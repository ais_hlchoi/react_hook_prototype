package hello;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

import reactor.core.publisher.Mono;

@Slf4j
@SpringBootApplication
@RestController
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
        log.info(String.format("done ... %s", "SpringApplication.run"));
	}

	@RequestMapping("/hello")
	public Mono<String> hello() {
		String a = String.valueOf(Math.random()).substring(2, 12);
		String b = String.valueOf(Math.random()).substring(2, 12);
		return Mono.just(String.format("{\"token\":\"%s.%s\"}", a, b));
	}

	@RequestMapping("/user")
	public Mono<RestEntity> user(HttpServletRequest request) {
		String[] names = new String[] { "admin" };
		User[] user = new User[] { new User(request.getParameter("username"), request.getParameter("password")) };
		List<UserProfile> users = Stream.of(user).map(UserInfo::new).collect(Collectors.toList());
		System.out.println(String.format("%s -> %s", ClassPathUtils.getName(), StringUtils.join(users, ", ")));
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("userInfo", users.stream().filter(o -> Stream.of(names).anyMatch(o.getUserCode()::equals)).collect(Collectors.toList()));
		return Mono.just(RestEntity.success(data));
	}

	@RequestMapping("/users")
	public Mono<RestEntity> users() {
		String[] names = new String[] { "admin", "user", "guest_" + String.valueOf(Math.random()).substring(2, 3) };
		List<UserProfile> users = Stream.of(names).map(User::new).map(UserInfo::new).collect(Collectors.toList());
		System.out.println(String.format("%s -> %s", ClassPathUtils.getName(), StringUtils.join(users, ", ")));
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("userInfo", users);
		return Mono.just(RestEntity.success(data));
	}
}

class User {

	private String username;
	private String password;

	public User(String username, String password) {
		setUsername(username);
		setPassword(password);
	}
	public User(String username) {
		this(username, username);
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (NullPointerException | JsonProcessingException e) {
			return String.format("{\"error\":\"%s\"}", e.getMessage());
		}
	}
}

class UserInfo implements UserProfile {

	private String userCode;
	private String pwd;
	private String[] roles;

	public UserInfo(User u) {
		setUserCode(u.getUsername());
		setPwd(u.getPassword());
		setRoles(null == u.getUsername() ? new String[] {} : new String[] { "*" });
	}
	public UserInfo() {
		this(new User(null));
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (NullPointerException | JsonProcessingException e) {
			return String.format("{\"error\":\"%s\"}", e.getMessage());
		}
	}
}

interface UserProfile {

	String getUserCode();
	String getPwd();
	String[] getRoles();
}

class RestEntity {

	private String responseCode;
	private String responseDesc;
	private Integer status;
	private Object object;
	private Integer totalRows;

	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public Integer getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}

	public RestEntity(Integer status, String responseCode, String msg, Object object) {
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
		this.object = object;
	}
	public static RestEntity success(Object object) {
		return new RestEntity(1, "M1000", null, object);
	}
}

class ClassPathUtils extends org.apache.commons.lang3.ClassPathUtils {

	public static String getName() {
		String className = Thread.currentThread().getStackTrace()[2].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		int index = className.lastIndexOf(".");
		if (index >= 0) {
			className = className.substring(index + 1);
		}
		return String.format("%s.%s", className, methodName);
	}
}