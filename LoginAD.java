/*      */ package com.dbsvhk.boss;
/*      */ 
/*      */ import java.io.UnsupportedEncodingException;
/*      */ import java.net.InetAddress;
/*      */ import java.net.UnknownHostException;
/*      */ import java.text.DateFormat;
/*      */ import java.util.GregorianCalendar;
/*      */ import java.util.Hashtable;
/*      */ import java.util.ResourceBundle;
/*      */ import java.util.TimeZone;
/*      */ import javax.naming.AuthenticationException;
/*      */ import javax.naming.NamingEnumeration;
/*      */ import javax.naming.NamingException;
/*      */ import javax.naming.OperationNotSupportedException;
/*      */ import javax.naming.directory.Attribute;
/*      */ import javax.naming.directory.Attributes;
/*      */ import javax.naming.directory.BasicAttribute;
/*      */ import javax.naming.directory.InvalidAttributeValueException;
/*      */ import javax.naming.directory.ModificationItem;
/*      */ import javax.naming.directory.SearchControls;
/*      */ import javax.naming.directory.SearchResult;
/*      */ import javax.naming.ldap.InitialLdapContext;
/*      */ import javax.naming.ldap.LdapContext;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class LoginAD {
/*   55 */   private static String log4jConfigPath = "";
/*      */   
/*   56 */   private static String enableSSL = "";
/*      */   
/*   57 */   private static String sslTrustStorePath = "";
/*      */   
/*   58 */   private static String winADServerURL = "";
/*      */   
/*   59 */   private static String security_authen = "";
/*      */   
/*   60 */   private static String winADServerDomain = "";
/*      */   
/*   61 */   private static String winADServerDC = "";
/*      */   
/*   62 */   private static String winADAuthenticateFlag = "";
/*      */   
/*   63 */   private static String enableChgnPswdFunct = "";
/*      */   
/*   64 */   private static String winADPswdWarningDay = "";
/*      */   
/*   66 */   private static String winADDomainList = "";
/*      */   
/*   69 */   private static String bimsProxyKeystore = "";
/*      */   
/*   70 */   private static String bimsProxyPswd = "";
/*      */   
/*   72 */   private static String LOGIN_AD_CONFIG = "LoginAD";
/*      */   
/*   73 */   private static String LUSUQ300_CONFIG = "LUSUQ300";
/*      */   
/*   74 */   private static String LOGINAD_LOG = "LOGINAD_LOG";
/*      */   
/*      */   private static ResourceBundle resourceBundle;
/*      */   
/*   78 */   private static String RC_SUCCESS = "SUCCESS";
/*      */   
/*   79 */   private static String RC_USERID_OR_PSWD_INCORRECT = "USERID_OR_PWD_INCORRECT";
/*      */   
/*   80 */   private static String RC_PSWD_WARNING = "PASSWORD_WARNING:";
/*      */   
/*   81 */   private static String RC_ACCOUNT_EXPIRED = "ACCOUNT_EXPIRED";
/*      */   
/*   82 */   private static String RC_PSWD_EXPIRED = "PASSWORD_EXPIRED";
/*      */   
/*   83 */   private static String RC_RESET_PSWD = "RESET_PASSWORD";
/*      */   
/*   84 */   private static String RC_USER_DISABLED = "USER_DISABLED";
/*      */   
/*   85 */   private static String RC_USER_LOCKOUT = "USER_LOCKOUT";
/*      */   
/*   86 */   private static String RC_LOGON_DENIED_AT_THIS_TIME = "LOGON_DENIED_AT_THIS_TIME";
/*      */   
/*   87 */   private static String RC_OTHER_ERROR = "OTHER_ERROR";
/*      */   
/*   88 */   private static String RC_AD_SERVER_ERROR = "AD_SERVER_ERROR";
/*      */   
/*   89 */   private static String RC_MUST_ENABLE_SSL = "MUST_ENABLE_SSL";
/*      */   
/*   90 */   private static String RC_PSWD_POLICY_VIOLATE = "PWD_POLICY_VIOLATE";
/*      */   
/*   91 */   private static String RC_NO_SUCH_USER = "NO_SUCH_USER";
/*      */   
/*      */   private static final int DONT_EXPIRE_PASSWORD = 65536;
/*      */   
/*      */   private static Logger logger;
/*      */   
/*      */   public void init() throws Exception {
/*      */     try {
/*  109 */       String hostname = getHostname();
/*  110 */       System.setProperty("hostname", hostname);
/*  113 */       resourceBundle = ResourceBundle.getBundle(LOGIN_AD_CONFIG);
/*  115 */       log4jConfigPath = resourceBundle.getString("log4jConfigPath");
/*  116 */       enableSSL = resourceBundle.getString("enableSSL");
/*  117 */       sslTrustStorePath = resourceBundle.getString("sslTrustStorePath");
/*  118 */       winADServerURL = resourceBundle.getString("winADServerURL");
/*  119 */       security_authen = resourceBundle.getString("security_authen");
/*  120 */       winADServerDomain = resourceBundle.getString("winADServerDomain");
/*  121 */       winADServerDC = resourceBundle.getString("winADServerDC");
/*  122 */       winADAuthenticateFlag = resourceBundle.getString("winADAuthenticateFlag");
/*  123 */       enableChgnPswdFunct = resourceBundle.getString("enableChgnPwdFunct");
/*  124 */       winADPswdWarningDay = resourceBundle.getString("winADPasswordWarningDay");
/*  126 */       winADDomainList = resourceBundle.getString("winADDomainList");
/*  130 */       resourceBundle = ResourceBundle.getBundle(LUSUQ300_CONFIG);
/*  132 */       bimsProxyKeystore = resourceBundle.getString("bims_proxy_keystore");
/*  133 */       bimsProxyPswd = resourceBundle.getString("bims_proxy_password");
/*  140 */       System.setProperty("log4j.configurationFile", log4jConfigPath);
/*  142 */       logger = LogManager.getLogger(LOGINAD_LOG);
/*  146 */       logger.debug("init done. enableSSL:" + enableSSL + 
/*  147 */           " sslTrustStorePath:" + sslTrustStorePath + 
/*  148 */           " winADServerURL:" + winADServerURL + 
/*  149 */           " security_authen:" + security_authen + 
/*  150 */           " winADServerDomain:" + winADServerDomain + 
/*  151 */           " winADServerDC:" + winADServerDC + 
/*  152 */           " winADAuthenticateFlag:" + winADAuthenticateFlag + 
/*  153 */           " enableChgnPswdFunct:" + enableChgnPswdFunct + 
/*  154 */           " winADPasswordWarningDay:" + winADPswdWarningDay + 
/*  155 */           " bimsProxyKeystore:" + bimsProxyKeystore + 
/*  156 */           " bimsProxyPassword:" + bimsProxyPswd);
/*  158 */     } catch (Exception e) {
/*  159 */       logger.error("Failed to read the config file:" + e);
/*  160 */       throw e;
/*      */     } 
/*      */   }
/*      */   
/*      */   private String getHostname() throws UnknownHostException {
/*  172 */     InetAddress localMachine = InetAddress.getLocalHost();
/*  173 */     return localMachine.getHostName();
/*      */   }
/*      */   
/*      */   public String login(String userID, String userPassword) throws Exception {
/*  187 */     if ("N".equalsIgnoreCase(winADAuthenticateFlag)) {
/*  189 */       logger.info("User:" + userID + " login. (No AD authentication)");
/*  190 */       return RC_SUCCESS;
/*      */     } 
/*  194 */     String[] adServerArray = winADServerURL.split(",");
/*  196 */     int index = 0;
/*  197 */     String[] adDomainArray = winADDomainList.split(",");
/*      */     byte b;
/*      */     int i;
/*      */     String[] arrayOfString1;
/*  199 */     for (i = (arrayOfString1 = adServerArray).length, b = 0; b < i; ) {
/*  199 */       String adServer = arrayOfString1[b];
/*  200 */       adServer = adServer.trim();
/*  201 */       logger.debug("Try to AD authenticate using server:" + adServer);
/*      */       try {
/*  206 */         String status = login(userID, userPassword, adServer, adDomainArray[index]);
/*  208 */         if (status.compareTo(RC_USERID_OR_PSWD_INCORRECT) == 0) {
/*  209 */           if (index < adDomainArray.length - 1)
/*  210 */             return login(userID, userPassword, adServer, adDomainArray[index + 1]); 
/*  212 */           return RC_USERID_OR_PSWD_INCORRECT;
/*      */         } 
/*  215 */         return status;
/*  219 */       } catch (NamingException ne) {
/*  220 */         logger.warn("loginAD - NamingException, will try another url");
/*  222 */       } catch (Exception e) {
/*  223 */         logger.warn("loginAD - Exception, will try another url");
/*      */       } 
/*  225 */       index++;
/*      */       b++;
/*      */     } 
/*  229 */     logger.error("AD Server error." + winADServerURL);
/*  230 */     return RC_AD_SERVER_ERROR;
/*      */   }
/*      */   
/*      */   public String login(String userID, String userPassword, String url) throws NamingException, Exception {
/*  245 */     String userDomainID = userID;
/*      */     try {
/*  247 */       if (!winADServerDomain.isEmpty())
/*  248 */         userDomainID = String.valueOf(winADServerDomain) + "\\" + userID; 
/*  250 */       logger.info("User " + userDomainID + " login attempt.");
/*  252 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  253 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  254 */       env.put("java.naming.provider.url", url);
/*  255 */       env.put("java.naming.security.authentication", security_authen);
/*  256 */       env.put("java.naming.security.principal", userDomainID);
/*  257 */       env.put("java.naming.security.credentials", userPassword);
/*  260 */       if ("Y".equalsIgnoreCase(enableSSL)) {
/*  262 */         env.put("java.naming.security.protocol", "ssl");
/*  264 */         System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*      */       } 
/*  268 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  271 */       if (!"".equals(winADPswdWarningDay) && !"-1".equals(winADPswdWarningDay)) {
/*  273 */         String remainExpiryDate = hasPasswordExpireWarning(ctx, userID);
/*  274 */         if (!"".equals(remainExpiryDate)) {
/*  275 */           logger.info("User " + userDomainID + " login warning (Password expire warning). Remain Expiry Date:" + remainExpiryDate);
/*  276 */           return String.valueOf(RC_PSWD_WARNING) + remainExpiryDate;
/*      */         } 
/*      */       } 
/*  280 */       ctx.close();
/*  282 */       logger.info("User " + userDomainID + " login success.");
/*  283 */       return RC_SUCCESS;
/*  285 */     } catch (AuthenticationException ae) {
/*  288 */       String errorMessage = ae.getMessage();
/*  289 */       logger.debug(errorMessage);
/*  290 */       return getErrorMsgCode(errorMessage, userDomainID);
/*  292 */     } catch (NamingException ne) {
/*  293 */       logger.warn("loginAD - NamingException: (user:" + userDomainID + ") (url:" + url + "): " + ne.toString());
/*  294 */       throw ne;
/*  296 */     } catch (Exception e) {
/*  297 */       logger.error("loginAD - Exception: (user:" + userDomainID + ") (url:" + url + "): " + e.toString());
/*  298 */       e.printStackTrace();
/*  299 */       throw e;
/*      */     } 
/*      */   }
/*      */   
/*      */   public String login(String userID, String userPassword, String url, String winDomain) throws NamingException, Exception {
/*  317 */     String userDomainID = userID;
/*      */     try {
/*  319 */       if (!winDomain.isEmpty())
/*  320 */         userDomainID = String.valueOf(winDomain) + "\\" + userID; 
/*  322 */       logger.info("User " + userDomainID + " login attempt.");
/*  324 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  325 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  326 */       env.put("java.naming.provider.url", url);
/*  327 */       env.put("java.naming.security.authentication", security_authen);
/*  328 */       env.put("java.naming.security.principal", userDomainID);
/*  329 */       env.put("java.naming.security.credentials", userPassword);
/*  332 */       if ("Y".equalsIgnoreCase(enableSSL)) {
/*  334 */         env.put("java.naming.security.protocol", "ssl");
/*  336 */         System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*      */       } 
/*  340 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  343 */       if (!"".equals(winADPswdWarningDay) && !"-1".equals(winADPswdWarningDay)) {
/*  345 */         String remainExpiryDate = hasPasswordExpireWarning(ctx, userID);
/*  346 */         if (!"".equals(remainExpiryDate)) {
/*  347 */           logger.info("User " + userDomainID + " login warning (Password expire warning). Remain Expiry Date:" + remainExpiryDate);
/*  348 */           return String.valueOf(RC_PSWD_WARNING) + remainExpiryDate;
/*      */         } 
/*      */       } 
/*  352 */       ctx.close();
/*  354 */       logger.info("User " + userDomainID + " login success.");
/*  355 */       return RC_SUCCESS;
/*  357 */     } catch (AuthenticationException ae) {
/*  360 */       String errorMessage = ae.getMessage();
/*  361 */       logger.debug(errorMessage);
/*  362 */       return getErrorMsgCode(errorMessage, userDomainID);
/*  364 */     } catch (NamingException ne) {
/*  365 */       logger.warn("loginAD - NamingException: (user:" + userDomainID + ") (url:" + url + "): " + ne.toString());
/*  366 */       throw ne;
/*  368 */     } catch (Exception e) {
/*  369 */       logger.error("loginAD - Exception: (user:" + userDomainID + ") (url:" + url + "): " + e.toString());
/*  370 */       e.printStackTrace();
/*  371 */       throw e;
/*      */     } 
/*      */   }
/*      */   
/*      */   public String changePassword(String userID, String userOldPassword, String userNewPassword) throws Exception {
/*  388 */     String returnCode = "";
/*  391 */     if (!"Y".equalsIgnoreCase(enableSSL))
/*  392 */       return RC_MUST_ENABLE_SSL; 
/*  395 */     String[] adServerArray = winADServerURL.split(",");
/*      */     byte b;
/*      */     int i;
/*      */     String[] arrayOfString1;
/*  396 */     for (i = (arrayOfString1 = adServerArray).length, b = 0; b < i; ) {
/*  396 */       String adServer = arrayOfString1[b];
/*  397 */       adServer = adServer.trim();
/*  398 */       logger.debug("Try to change password using server:" + adServer);
/*  400 */       returnCode = changePassword(userID, userOldPassword, userNewPassword, adServer);
/*  401 */       if (!RC_AD_SERVER_ERROR.equals(returnCode))
/*  402 */         return returnCode; 
/*      */       b++;
/*      */     } 
/*  406 */     return returnCode;
/*      */   }
/*      */   
/*      */   public String changePassword(String userID, String userOldPassword, String userNewPassword, String url) throws Exception {
/*  421 */     String userDomainID = userID;
/*      */     try {
/*  424 */       if (!winADServerDomain.isEmpty())
/*  425 */         userDomainID = String.valueOf(winADServerDomain) + "\\" + userID; 
/*  428 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  429 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  430 */       env.put("java.naming.provider.url", url);
/*  431 */       env.put("java.naming.security.authentication", security_authen);
/*  432 */       env.put("java.naming.security.principal", userDomainID);
/*  433 */       env.put("java.naming.security.credentials", userOldPassword);
/*  434 */       env.put("java.naming.security.protocol", "ssl");
/*  436 */       System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*  438 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  463 */       String distinguishedName = getDistinguishedName(ctx, userID);
/*  464 */       if (distinguishedName.isEmpty())
/*  465 */         return RC_NO_SUCH_USER; 
/*  468 */       ModificationItem[] mods = new ModificationItem[2];
/*  470 */       String oldQuotedPassword = "\"" + userOldPassword + "\"";
/*  471 */       byte[] oldUnicodePassword = oldQuotedPassword.getBytes("UTF-16LE");
/*  472 */       String newQuotedPassword = "\"" + userNewPassword + "\"";
/*  473 */       byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
/*  475 */       mods[0] = new ModificationItem(3, new BasicAttribute("unicodePwd", oldUnicodePassword));
/*  476 */       mods[1] = new ModificationItem(1, new BasicAttribute("unicodePwd", newUnicodePassword));
/*  478 */       ctx.modifyAttributes(distinguishedName, mods);
/*  480 */       ctx.close();
/*  483 */     } catch (AuthenticationException ae) {
/*  484 */       String errorMessage = ae.getMessage();
/*  485 */       return getErrorMsgCode(errorMessage, userDomainID);
/*  487 */     } catch (InvalidAttributeValueException iave) {
/*  488 */       String errorMessage = iave.getMessage();
/*  489 */       return getChangePwdErrorMsgCode(errorMessage, userDomainID);
/*  491 */     } catch (OperationNotSupportedException onse) {
/*  492 */       String errorMessage = onse.getMessage();
/*  493 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/*  495 */     } catch (NamingException ne) {
/*  496 */       logger.warn("changePassword - NamingException: (user:" + userDomainID + ") (url:" + url + "): " + ne);
/*  497 */       return RC_AD_SERVER_ERROR;
/*  499 */     } catch (UnsupportedEncodingException uee) {
/*  500 */       logger.warn("changePassword - UnsupportedEncodingException: (user:" + userDomainID + ") (url:" + url + "): " + uee);
/*  501 */       return RC_AD_SERVER_ERROR;
/*  503 */     } catch (Exception e) {
/*  504 */       logger.error("changePassword - Exception: (user:" + userDomainID + ") (url:" + url + "): " + e);
/*  505 */       e.printStackTrace();
/*  506 */       throw e;
/*      */     } 
/*  509 */     logger.info("password changed successfully for " + userDomainID);
/*  510 */     return RC_SUCCESS;
/*      */   }
/*      */   
/*      */   public String changePasswordByAdmin(String adminUserID, String adminPassword, String userID, String userNewPassword) throws Exception {
/*  524 */     String returnCode = "";
/*  527 */     if (!"Y".equalsIgnoreCase(enableSSL))
/*  528 */       return RC_MUST_ENABLE_SSL; 
/*  531 */     String[] adServerArray = winADServerURL.split(",");
/*      */     byte b;
/*      */     int i;
/*      */     String[] arrayOfString1;
/*  532 */     for (i = (arrayOfString1 = adServerArray).length, b = 0; b < i; ) {
/*  532 */       String adServer = arrayOfString1[b];
/*  533 */       adServer = adServer.trim();
/*  534 */       logger.debug("Try to change password using server:" + adServer);
/*  536 */       returnCode = changePasswordByAdmin(adminUserID, adminPassword, userID, userNewPassword, adServer);
/*  537 */       if (!RC_AD_SERVER_ERROR.equals(returnCode))
/*  538 */         return returnCode; 
/*      */       b++;
/*      */     } 
/*  542 */     return returnCode;
/*      */   }
/*      */   
/*      */   public String changePasswordByAdmin(String adminUserID, String adminPassword, String userID, String userNewPassword, String url) throws Exception {
/*  558 */     String adminUserDomainID = adminUserID;
/*      */     try {
/*  561 */       if (!winADServerDomain.isEmpty())
/*  562 */         adminUserDomainID = String.valueOf(winADServerDomain) + "\\" + adminUserID; 
/*  565 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  566 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  567 */       env.put("java.naming.provider.url", url);
/*  568 */       env.put("java.naming.security.authentication", security_authen);
/*  569 */       env.put("java.naming.security.principal", adminUserDomainID);
/*  570 */       env.put("java.naming.security.credentials", adminPassword);
/*  571 */       env.put("java.naming.security.protocol", "ssl");
/*  573 */       System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*  575 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  599 */       String distinguishedName = getDistinguishedName(ctx, userID);
/*  600 */       if (distinguishedName.isEmpty())
/*  601 */         return RC_NO_SUCH_USER; 
/*  604 */       ModificationItem[] mods = new ModificationItem[1];
/*  605 */       String newQuotedPassword = "\"" + userNewPassword + "\"";
/*  606 */       byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
/*  607 */       mods[0] = new ModificationItem(2, new BasicAttribute("unicodePwd", newUnicodePassword));
/*  608 */       ctx.modifyAttributes(distinguishedName, mods);
/*  610 */       ctx.close();
/*  613 */     } catch (AuthenticationException ae) {
/*  614 */       String errorMessage = ae.getMessage();
/*  615 */       return getErrorMsgCode(errorMessage, adminUserDomainID);
/*  617 */     } catch (InvalidAttributeValueException iave) {
/*  618 */       String errorMessage = iave.getMessage();
/*  619 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/*  621 */     } catch (OperationNotSupportedException onse) {
/*  622 */       String errorMessage = onse.getMessage();
/*  623 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/*  625 */     } catch (NamingException ne) {
/*  626 */       logger.warn("changePasswordByAdmin - NamingException: (admin:" + adminUserDomainID + ") (userID:" + userID + ") (url:" + url + "): " + ne);
/*  627 */       return RC_AD_SERVER_ERROR;
/*  629 */     } catch (UnsupportedEncodingException uee) {
/*  630 */       logger.warn("changePasswordByAdmin - UnsupportedEncodingException: (user:" + adminUserDomainID + ") (userID:" + userID + ") (url:" + url + "): " + uee);
/*  631 */       return RC_AD_SERVER_ERROR;
/*  633 */     } catch (Exception e) {
/*  634 */       logger.error("changePasswordByAdmin - Exception: (user:" + adminUserDomainID + ") (userID:" + userID + ") (url:" + url + "): " + e);
/*  635 */       e.printStackTrace();
/*  636 */       throw e;
/*      */     } 
/*  639 */     logger.info("password changed by admin successfully for " + userID);
/*  640 */     return RC_SUCCESS;
/*      */   }
/*      */   
/*      */   public String lockAccountByAdmin(String adminUserID, String adminPassword, String userID, String lockFlag) throws Exception {
/*  654 */     String returnCode = "";
/*  657 */     if (!"Y".equalsIgnoreCase(enableSSL))
/*  658 */       return RC_MUST_ENABLE_SSL; 
/*  661 */     String[] adServerArray = winADServerURL.split(",");
/*      */     byte b;
/*      */     int i;
/*      */     String[] arrayOfString1;
/*  662 */     for (i = (arrayOfString1 = adServerArray).length, b = 0; b < i; ) {
/*  662 */       String adServer = arrayOfString1[b];
/*  663 */       adServer = adServer.trim();
/*  664 */       logger.debug("Try to lock/unlock account using server:" + adServer);
/*  666 */       returnCode = lockAccountByAdmin(adminUserID, adminPassword, userID, lockFlag, adServer);
/*  667 */       if (!RC_AD_SERVER_ERROR.equals(returnCode))
/*  668 */         return returnCode; 
/*      */       b++;
/*      */     } 
/*  672 */     return returnCode;
/*      */   }
/*      */   
/*      */   public String lockAccountByAdmin(String adminUserID, String adminPassword, String userID, String lockFlag, String url) throws Exception {
/*  688 */     String adminUserDomainID = adminUserID;
/*      */     try {
/*  691 */       if (!winADServerDomain.isEmpty())
/*  692 */         adminUserDomainID = String.valueOf(winADServerDomain) + "\\" + adminUserID; 
/*  695 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  696 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  697 */       env.put("java.naming.provider.url", url);
/*  698 */       env.put("java.naming.security.authentication", security_authen);
/*  699 */       env.put("java.naming.security.principal", adminUserDomainID);
/*  700 */       env.put("java.naming.security.credentials", adminPassword);
/*  701 */       env.put("java.naming.security.protocol", "ssl");
/*  703 */       System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*  705 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  730 */       String distinguishedName = getDistinguishedName(ctx, userID);
/*  731 */       if (distinguishedName.isEmpty())
/*  732 */         return RC_NO_SUCH_USER; 
/*  735 */       ModificationItem[] mods = new ModificationItem[1];
/*  736 */       String lockoutTime = "";
/*  737 */       if ("Y".equals(lockFlag)) {
/*  738 */         lockoutTime = "1";
/*      */       } else {
/*  741 */         lockoutTime = "0";
/*      */       } 
/*  743 */       mods[0] = new ModificationItem(2, new BasicAttribute("lockoutTime", lockoutTime));
/*  744 */       ctx.modifyAttributes(distinguishedName, mods);
/*  746 */       ctx.close();
/*  749 */     } catch (AuthenticationException ae) {
/*  750 */       String errorMessage = ae.getMessage();
/*  751 */       return getErrorMsgCode(errorMessage, adminUserDomainID);
/*  753 */     } catch (InvalidAttributeValueException iave) {
/*  754 */       String errorMessage = iave.getMessage();
/*  755 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/*  757 */     } catch (OperationNotSupportedException onse) {
/*  758 */       String errorMessage = onse.getMessage();
/*  759 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/*  761 */     } catch (NamingException ne) {
/*  762 */       logger.warn("lockAccountByAdmin - NamingException: (admin:" + adminUserDomainID + ") (userID:" + userID + ") (url:" + url + "): " + ne);
/*  763 */       return RC_AD_SERVER_ERROR;
/*  765 */     } catch (Exception e) {
/*  766 */       logger.error("lockAccountByAdmin - Exception: (user:" + adminUserDomainID + ") (userID:" + userID + ") (url:" + url + "): " + e);
/*  767 */       e.printStackTrace();
/*  768 */       throw e;
/*      */     } 
/*  770 */     if ("Y".equals(lockFlag)) {
/*  771 */       logger.info("lock account by admin successfully for " + userID);
/*      */     } else {
/*  774 */       logger.info("unlock account by admin successfully for " + userID);
/*      */     } 
/*  776 */     return RC_SUCCESS;
/*      */   }
/*      */   
/*      */   private String getErrorMsgCode(String errorMessage, String userDomainID) {
/*  787 */     if (errorMessage.indexOf("525") != -1 || errorMessage.indexOf("successful bind must be completed") != -1 || 
/*  788 */       errorMessage.indexOf("52e") != -1) {
/*  789 */       logger.info("User " + userDomainID + " login failed (User ID or Password incorrect).");
/*  790 */       return RC_USERID_OR_PSWD_INCORRECT;
/*      */     } 
/*  791 */     if (errorMessage.indexOf("701") != -1) {
/*  792 */       logger.info("User " + userDomainID + " login failed (Account expired).");
/*  793 */       return RC_ACCOUNT_EXPIRED;
/*      */     } 
/*  794 */     if (errorMessage.indexOf("532") != -1) {
/*  795 */       logger.info("User " + userDomainID + " login failed (Password expired).");
/*  796 */       return RC_PSWD_EXPIRED;
/*      */     } 
/*  797 */     if (errorMessage.indexOf("773") != -1) {
/*  798 */       logger.info("User " + userDomainID + " login failed (Must reset password).");
/*  799 */       return RC_RESET_PSWD;
/*      */     } 
/*  800 */     if (errorMessage.indexOf("533") != -1) {
/*  801 */       logger.info("User " + userDomainID + " login failed (User disabled).");
/*  802 */       return RC_USER_DISABLED;
/*      */     } 
/*  803 */     if (errorMessage.indexOf("775") != -1) {
/*  804 */       logger.info("User " + userDomainID + " login failed (User lockout).");
/*  805 */       return RC_USER_LOCKOUT;
/*      */     } 
/*  806 */     if (errorMessage.indexOf("530") != -1) {
/*  807 */       logger.info("User " + userDomainID + " login failed (Logon denied at this time).");
/*  808 */       return RC_LOGON_DENIED_AT_THIS_TIME;
/*      */     } 
/*  811 */     logger.info("User " + userDomainID + " login failed (Other error).");
/*  812 */     logger.info("Return msg:" + errorMessage);
/*  813 */     return RC_OTHER_ERROR;
/*      */   }
/*      */   
/*      */   private String getChangePwdErrorMsgCode(String errorMessage, String userDomainID) {
/*  817 */     if (errorMessage.indexOf("error code 19") != -1 || errorMessage.indexOf("error code 53") != -1) {
/*  818 */       logger.info("User " + userDomainID + " change password failed (Password policy).");
/*  819 */       return RC_PSWD_POLICY_VIOLATE;
/*      */     } 
/*  822 */     logger.info("User " + userDomainID + " change password failed (Other error).");
/*  823 */     logger.info("Return msg:" + errorMessage);
/*  824 */     return RC_OTHER_ERROR;
/*      */   }
/*      */   
/*      */   private String hasPasswordExpireWarning(LdapContext ctx, String userID) throws NamingException {
/*  837 */     Long pwdAlertDateInMillis = new Long(winADPswdWarningDay);
/*  838 */     pwdAlertDateInMillis = Long.valueOf(pwdAlertDateInMillis.longValue() * 86400L * 10000000L);
/*  840 */     Attributes attrs = ctx.getAttributes(winADServerDC);
/*  843 */     Long pwdAge = new Long(attrs.get("maxPwdAge").get().toString());
/*  844 */     logger.debug("Password Age (Days): " + (pwdAge.longValue() / -86400L / 10000000L));
/*  847 */     GregorianCalendar win32Epoch = new GregorianCalendar(1601, 0, 1);
/*  848 */     Long win32EpochInMillis = Long.valueOf(win32Epoch.getTimeInMillis());
/*  851 */     GregorianCalendar currentDate = new GregorianCalendar();
/*  852 */     Long currentDateInMillis = Long.valueOf(currentDate.getTimeInMillis());
/*  856 */     Long todayDateInMillis = Long.valueOf((currentDateInMillis.longValue() - win32EpochInMillis.longValue()) * 10000L);
/*  858 */     logger.debug("today's Date: " + DisplayWin32Date(todayDateInMillis.toString()));
/*  861 */     SearchControls searchCtls = new SearchControls();
/*  862 */     String[] returnedAtts = { "pwdLastSet", "userAccountControl" };
/*  863 */     searchCtls.setReturningAttributes(returnedAtts);
/*  864 */     searchCtls.setSearchScope(2);
/*  866 */     String searchBase = winADServerDC;
/*  867 */     String searchFilter = "sAMAccountName=" + userID;
/*  868 */     NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
/*  869 */     while (answer.hasMoreElements()) {
/*  870 */       SearchResult sr = answer.next();
/*  871 */       logger.debug("AD search find:" + sr.getName());
/*  872 */       attrs = sr.getAttributes();
/*  873 */       if (attrs != null)
/*      */         try {
/*  876 */           TimeZone tz = TimeZone.getDefault();
/*  877 */           logger.debug("default timezone: " + tz.getDisplayName() + " offset:" + tz.getRawOffset());
/*  880 */           Long pwdLastSet = new Long(attrs.get("pwdLastSet").get().toString());
/*  881 */           logger.debug("-pwdLastSet: " + DisplayWin32Date(pwdLastSet.toString()));
/*  884 */           Long tzRawOffsetInMillis = new Long(tz.getRawOffset());
/*  885 */           tzRawOffsetInMillis = Long.valueOf(tzRawOffsetInMillis.longValue() * 10000L);
/*  886 */           pwdLastSet = Long.valueOf(pwdLastSet.longValue() + tzRawOffsetInMillis.longValue());
/*  887 */           logger.debug("-pwdLastSet(" + tz.getDisplayName() + "): " + DisplayWin32Date(pwdLastSet.toString()));
/*  890 */           Long expiryDateInMillis = Long.valueOf(pwdLastSet.longValue() + pwdAge.longValue() * -1L);
/*  891 */           logger.debug("-expiry Date: " + DisplayWin32Date(expiryDateInMillis.toString()));
/*  894 */           Long alertDateInMillis = Long.valueOf(expiryDateInMillis.longValue() - pwdAlertDateInMillis.longValue());
/*  895 */           logger.debug("-warning Date: " + DisplayWin32Date(alertDateInMillis.toString()));
/*  898 */           Long userAccountControl = new Long(attrs.get("userAccountControl").get().toString());
/*  899 */           boolean pwdNeverExpire = ((userAccountControl.longValue() & 0x10000L) == 65536L);
/*  900 */           logger.debug("-userAccountControl: " + userAccountControl + " pwdNeverExpire:" + pwdNeverExpire);
/*  902 */           if (pwdNeverExpire)
/*  903 */             return ""; 
/*  906 */           Long remainExpiryDateInMillis = Long.valueOf(expiryDateInMillis.longValue() - todayDateInMillis.longValue());
/*  908 */           Long remainExpiryDate = Long.valueOf(remainExpiryDateInMillis.longValue() / 86400L / 10000000L);
/*  909 */           logger.debug("remainExpiryDate:" + remainExpiryDate);
/*  911 */           if (todayDateInMillis.longValue() > alertDateInMillis.longValue())
/*  913 */             return remainExpiryDate.toString(); 
/*  916 */           return "";
/*  920 */         } catch (NullPointerException e) {
/*  921 */           logger.error("Unable to find attribute (NullPointerException): " + e);
/*      */         }  
/*      */     } 
/*  926 */     return "";
/*      */   }
/*      */   
/*      */   private String getDistinguishedName(LdapContext ctx, String userID) throws NamingException {
/*  930 */     String distinguishedName = "";
/*  932 */     SearchControls searchCtls = new SearchControls();
/*  933 */     String[] returnedAtts = { "distinguishedName" };
/*  934 */     searchCtls.setReturningAttributes(returnedAtts);
/*  935 */     searchCtls.setSearchScope(2);
/*  936 */     String searchBase = winADServerDC;
/*  937 */     String searchFilter = "sAMAccountName=" + userID;
/*  938 */     NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
/*  939 */     while (answer.hasMoreElements()) {
/*  940 */       SearchResult sr = answer.next();
/*  942 */       Attributes attrs = sr.getAttributes();
/*  943 */       if (attrs != null)
/*  944 */         distinguishedName = attrs.get("distinguishedName").get().toString(); 
/*      */     } 
/*  948 */     return distinguishedName;
/*      */   }
/*      */   
/*      */   public String listADValue(String userID, String userPassword) throws Exception {
/*  953 */     String userDomainID = userID;
/*      */     try {
/*  956 */       if (!winADServerDomain.isEmpty())
/*  957 */         userDomainID = String.valueOf(winADServerDomain) + "\\" + userID; 
/*  960 */       Hashtable<String, Object> env = new Hashtable<String, Object>();
/*  961 */       env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
/*  962 */       env.put("java.naming.provider.url", winADServerURL);
/*  963 */       env.put("java.naming.security.authentication", security_authen);
/*  964 */       env.put("java.naming.security.principal", userDomainID);
/*  965 */       env.put("java.naming.security.credentials", userPassword);
/*  967 */       if ("Y".equalsIgnoreCase(enableSSL)) {
/*  969 */         env.put("java.naming.security.protocol", "ssl");
/*  971 */         System.setProperty("javax.net.ssl.trustStore", sslTrustStorePath);
/*      */       } 
/*  974 */       LdapContext ctx = new InitialLdapContext(env, null);
/*  976 */       String distinguishedName = getDistinguishedName(ctx, userID);
/*  977 */       if (distinguishedName.isEmpty())
/*  978 */         return RC_NO_SUCH_USER; 
/*  981 */       SearchControls searchCtls = new SearchControls();
/*  985 */       searchCtls.setSearchScope(2);
/*  986 */       String searchBase = winADServerDC;
/*  987 */       String searchFilter = "sAMAccountName=" + userID;
/*  988 */       NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
/*  990 */       while (answer.hasMoreElements()) {
/*  992 */         SearchResult sr = answer.next();
/*  994 */         Attributes attrs = sr.getAttributes();
/*  995 */         if (attrs != null)
/*      */           try {
/*  999 */             NamingEnumeration<? extends Attribute> ne = attrs.getAll();
/* 1001 */             while (ne.hasMore())
/* 1002 */               logger.debug("AD Attr: " + ne.nextElement().toString()); 
/* 1005 */           } catch (NullPointerException e) {
/* 1006 */             logger.error("Unable to get the attribute distinguishedName for " + userDomainID + " :" + e);
/* 1007 */             return RC_AD_SERVER_ERROR;
/*      */           }  
/*      */       } 
/* 1011 */       ctx.close();
/* 1014 */     } catch (AuthenticationException ae) {
/* 1015 */       String errorMessage = ae.getMessage();
/* 1016 */       return getErrorMsgCode(errorMessage, userDomainID);
/* 1018 */     } catch (InvalidAttributeValueException iave) {
/* 1019 */       String errorMessage = iave.getMessage();
/* 1020 */       return getChangePwdErrorMsgCode(errorMessage, userDomainID);
/* 1022 */     } catch (OperationNotSupportedException onse) {
/* 1023 */       String errorMessage = onse.getMessage();
/* 1024 */       return getChangePwdErrorMsgCode(errorMessage, userID);
/* 1026 */     } catch (NamingException ne) {
/* 1027 */       logger.warn("listADValue - NamingException: (user:" + userDomainID + ") (url:" + winADServerURL + "): " + ne);
/* 1028 */       return RC_AD_SERVER_ERROR;
/* 1030 */     } catch (Exception e) {
/* 1031 */       logger.error("listADValue - Exception: (user:" + userDomainID + ") (url:" + winADServerURL + "): " + e);
/* 1032 */       e.printStackTrace();
/* 1033 */       throw e;
/*      */     } 
/* 1036 */     logger.info("listADValue successfully for " + userDomainID);
/* 1037 */     return RC_SUCCESS;
/*      */   }
/*      */   
/*      */   public String getBimsProxyKeystore() {
/* 1045 */     return bimsProxyKeystore;
/*      */   }
/*      */   
/*      */   public String getBimsProxyPassword() {
/* 1053 */     return bimsProxyPswd;
/*      */   }
/*      */   
/*      */   public String getWinADAuthenticateFlag() {
/* 1061 */     return winADAuthenticateFlag;
/*      */   }
/*      */   
/*      */   public String getEnableChgnPwdFunct() {
/* 1069 */     return enableChgnPswdFunct;
/*      */   }
/*      */   
/*      */   public void logMessage(String logLevel, String logMessage) {
/* 1080 */     if ("t".equalsIgnoreCase(logLevel)) {
/* 1081 */       logger.trace(logMessage);
/* 1083 */     } else if ("d".equalsIgnoreCase(logLevel)) {
/* 1084 */       logger.debug(logMessage);
/* 1086 */     } else if ("i".equalsIgnoreCase(logLevel)) {
/* 1087 */       logger.info(logMessage);
/* 1089 */     } else if ("e".equalsIgnoreCase(logLevel)) {
/* 1090 */       logger.error(logMessage);
/* 1092 */     } else if ("f".equalsIgnoreCase(logLevel)) {
/* 1093 */       logger.fatal(logMessage);
/*      */     } else {
/* 1096 */       logger.debug(logMessage);
/*      */     } 
/*      */   }
/*      */   
/*      */   private static String DisplayWin32Date(String Win32FileTime) {
/* 1107 */     GregorianCalendar Win32Epoch = new GregorianCalendar(1601, 0, 1);
/* 1108 */     Long lWin32Epoch = Long.valueOf(Win32Epoch.getTimeInMillis());
/* 1109 */     Long lWin32FileTime = new Long(Win32FileTime);
/* 1110 */     return DateFormat.getDateTimeInstance(0, 0).format(Long.valueOf(lWin32FileTime.longValue() / 10000L + lWin32Epoch.longValue()));
/*      */   }
/*      */   
/*      */   public static void main(String[] argv) throws Exception {
/* 1115 */     if (argv.length < 3) {
/* 1116 */       System.out.println("[usage] LoginAD login <userID> <password>");
/* 1117 */       System.out.println("[usage] LoginAD changePassword <userID> <oldPassword> <newPassword>");
/* 1118 */       System.out.println("[usage] LoginAD adminChangePassword <adminID> <adminPassword> <userID> <userPassword>");
/* 1119 */       System.out.println("[usage] LoginAD lockAccountByAdmin <adminID> <adminPassword> <userID> <lockFlag(Y:lock/N:unlock)>");
/* 1120 */       System.out.println("[usage] LoginAD list <userID> <password>");
/* 1121 */       System.exit(0);
/*      */     } 
/* 1123 */     String cmd = argv[0];
/* 1124 */     String parm1 = argv[1];
/* 1125 */     String parm2 = argv[2];
/* 1126 */     String parm3 = "";
/* 1127 */     if (argv.length >= 4)
/* 1128 */       parm3 = argv[3]; 
/* 1130 */     String parm4 = "";
/* 1131 */     if (argv.length >= 5)
/* 1132 */       parm4 = argv[4]; 
/* 1136 */     String result = "";
/* 1137 */     LoginAD loginAD = new LoginAD();
/* 1138 */     loginAD.init();
/* 1139 */     if ("login".equals(cmd)) {
/* 1140 */       result = loginAD.login(parm1, parm2);
/* 1142 */     } else if ("changePassword".equals(cmd)) {
/* 1143 */       result = loginAD.changePassword(parm1, parm2, parm3);
/* 1145 */     } else if ("changePasswordByAdmin".equals(cmd)) {
/* 1146 */       result = loginAD.changePasswordByAdmin(parm1, parm2, parm3, parm4);
/* 1148 */     } else if ("lockAccountByAdmin".equals(cmd)) {
/* 1149 */       result = loginAD.lockAccountByAdmin(parm1, parm2, parm3, parm4);
/* 1151 */     } else if ("list".equals(cmd)) {
/* 1152 */       result = loginAD.listADValue(parm1, parm2);
/*      */     } 
/* 1155 */     logger.info("main result:" + result);
/*      */   }
/*      */ }


/* Location:              C:\Users\AIS\Desktop\LoginAD\!\com\dbsvhk\boss\LoginAD.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */