/**
 *
 * (C) 2018 The Securities And Futures Commission of Hong Kong. All rights reserved.
 *
 */
package hk.sfc.eds.profilemaintenance.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hk.sfc.eds.common.security.GlobalMethodSecurityExpressionRoot;
import hk.sfc.eds.common.util.RedisCache;
import hk.sfc.eds.profilemaintenance.constants.Constants;
import hk.sfc.eds.profilemaintenance.constants.NotificationConstants;
import hk.sfc.eds.profilemaintenance.constants.RedisConstants;
import hk.sfc.eds.profilemaintenance.json.model.Corporation;
import hk.sfc.eds.profilemaintenance.json.model.Delegation;
import hk.sfc.eds.profilemaintenance.json.model.DelegationInt;
import hk.sfc.eds.profilemaintenance.json.model.PaginatedDelegationIntDataModel;
import hk.sfc.eds.profilemaintenance.json.model.PermissionRight;
import hk.sfc.eds.profilemaintenance.mapper.CommonMapper;
import hk.sfc.eds.profilemaintenance.mapper.LCMapper;
import hk.sfc.eds.profilemaintenance.mapper.UpdateMapper;
import hk.sfc.eds.profilemaintenance.mapper.UserMapper;
import hk.sfc.eds.profilemaintenance.mapper.WingsLoggerDao;
import hk.sfc.eds.profilemaintenance.model.AccreditedCorp;
import hk.sfc.eds.profilemaintenance.model.CadGroupEntity;
import hk.sfc.eds.profilemaintenance.model.CadGroupMembers;
import hk.sfc.eds.profilemaintenance.model.UpdateFormRequest;
import hk.sfc.eds.profilemaintenance.model.UserProfile;
import hk.sfc.eds.profilemaintenance.service.impl.CorpServiceImpl;
import hk.sfc.eds.profilemaintenance.service.impl.ProfileCommonServiceImpl;
import hk.sfc.eds.profilemaintenance.service.impl.SensitiveDataImpl;
import hk.sfc.eds.profilemaintenance.util.CustomApplicationContextHolder;
import hk.sfc.eds.profilemaintenance.util.JsonUtils;
import hk.sfc.eds.profilemaintenance.util.StaticWrapper;
import hk.sfc.eds.profilemaintenance.util.Utils;

public class CustomMethodSecurityExpressionRoot extends GlobalMethodSecurityExpressionRoot
    implements StaticWrapper {

  public CustomMethodSecurityExpressionRoot(Authentication authentication) {
    super(authentication);
  }

  public CustomMethodSecurityExpressionRoot(Authentication authentication, SqlSession sqlSession) {
    super(authentication, sqlSession);
  }

  @Override
  public boolean isTrustedClient() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    for (GrantedAuthority authority : authentication.getAuthorities()) {
      if (authority.getAuthority().equals("ROLE_TRUSTED_CLIENT")) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isUser() {
    for (GrantedAuthority authority : authentication.getAuthorities()) {
      if (authority.getAuthority().equals("ROLE_USER")) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean hasPermission(String permission, String permissionRight, String corpCeref) {
    return hasPermission(permission, Arrays.asList(permissionRight), corpCeref);
  }

  @Override
  public boolean hasPermission(String permission, List<String> permissionRights, String corpCeref) {
    return super.hasPermission(permission, permissionRights, corpCeref);
  }

  @SuppressWarnings({"unchecked", "unused"})
  public boolean isAllowed(String ceref, boolean isInitial, boolean checkLicensees,
      HttpServletRequest httpServletRequest) {
    ObjectMapper mapper = CustomApplicationContextHolder.getBean(ObjectMapper.class);
    UserMapper userMapper = CustomApplicationContextHolder.getBean(UserMapper.class);
    SensitiveDataImpl sensitiveData =
        CustomApplicationContextHolder.getBean(SensitiveDataImpl.class);
    WingsLoggerDao wingsLoggerDao = CustomApplicationContextHolder.getBean(WingsLoggerDao.class);
    ProfileCommonServiceImpl profileCommonService =
        CustomApplicationContextHolder.getBean(ProfileCommonServiceImpl.class);
    CorpServiceImpl corpService = CustomApplicationContextHolder.getBean(CorpServiceImpl.class);
    RedisCache redisHelper = CustomApplicationContextHolder.getBean(RedisCache.class);

    String initialCeref = null;
    String entryKey = null;
    String login = getAuthenticationName();
    String realCeref = null;
    String userType = userMapper.findUserTypeByLoginName(login);
    boolean hasPermission = false;
    boolean isCorporateAccount = false;
    boolean isOwnerProfile = false;
    if ((userType != null && userType.equalsIgnoreCase("LICCORP"))
        || getLiccorpByCeref(userMapper.findUserCerefByLoginName(login)) != null) {
      isCorporateAccount = true;
    }
    if (isInitial) {
      initialCeref = userMapper.findUserCerefByLoginName(login);
      UserProfile response = profileCommonService.getUserProfile("Y");
      if (response != null && login.equalsIgnoreCase(response.getUsername())) {
        isOwnerProfile = true;
      }
    } else {
      if (ceref == null || ceref.isEmpty()) {
        realCeref = userMapper.findUserCerefByLoginName(login);
        UserProfile response = profileCommonService.getUserProfile("Y");
        if (response != null && login != null && login.equalsIgnoreCase(response.getUsername())) {
          isOwnerProfile = true;
        }
      } else {
        realCeref = sensitiveData.show(ceref);
        String thisCeref = userMapper.findUserCerefByLoginName(login);
        if (thisCeref != null && realCeref != null && thisCeref.equalsIgnoreCase(realCeref)) {
          isOwnerProfile = true;
        }
      }
    }
    String localCeref = isInitial ? initialCeref : realCeref;
    List<Corporation> allCorps = null;

    if (redisHelper.containsInCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY)) {
      try {
        allCorps = mapper.readValue(
            redisHelper.getFromCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY,
                String.class),
            mapper.getTypeFactory().constructCollectionType(List.class, Corporation.class));
      } catch (IOException e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    } else {
      try {
        allCorps = corpService.getCorporationsByLoginName(null, null, false, "Y");
      } catch (Exception e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    }
    boolean isCorpAdmin = false;
    boolean skipCheckCorp = true;
    if (localCeref != null && !localCeref.isEmpty()) {
      skipCheckCorp = false;
    }
    boolean checkStrictPermission = false;
    boolean isDelegatingCorps = false;
    if (allCorps != null && !allCorps.isEmpty()) {
      for (Corporation c : allCorps) {
        if (c != null) {
          if (!skipCheckCorp && Objects.nonNull(c.getReferenceId())) {
            String realCorpCeref = sensitiveData.show(c.getReferenceId());
            if (localCeref.equals(realCorpCeref)) {
              checkStrictPermission = true;
            }
          }
          Delegation d = null;
          try {
            d = mapper.readValue(JsonUtils.getJsonStringFromObject(c.getDelegation()),
                Delegation.class);
          } catch (JsonParseException e) {
          } catch (JsonMappingException e) {
          } catch (IOException e) {
          }
          if (d != null) {
            if (d.getIsCorpAdmin()) {
              hasPermission = true;
            }
          }
          List<PermissionRight> permissionRights = c.getPermissionRights();
          if (permissionRights != null) {
            for (PermissionRight p : permissionRights) {
              if (p != null && p.getPermissionCode().equals("AILP")) {
                hasPermission = true;
                isDelegatingCorps = true;
              }
              if (p != null && p.getPermissionCode().equals("PM")
                  && (p.getKey().equalsIgnoreCase("Edit")
                      || p.getKey().equalsIgnoreCase("Submit"))) {
                hasPermission = true;
              }
            }
          }
          checkStrictPermission = false;
        }
      }
    } else {
      hasPermission = true;
      isDelegatingCorps = true;
    }
    if (hasPermission && isDelegatingCorps
        && ((userType != null && userType.equalsIgnoreCase("LICCORP"))
            || getLiccorpByCeref(userMapper.findUserCerefByLoginName(login)) != null)) {
      PaginatedDelegationIntDataModel model = profileCommonService.getPaginatedDelegatingInternals(
          realCeref, new Integer(0), new Integer(100), "N", null, null, "asc", null, false);
      List<DelegationInt> listDelegation = null;
      if (model != null && model.getDlgIntData() != null) {
        try {
          listDelegation = (List<DelegationInt>) mapper.readValue(
              mapper.writeValueAsString(model.getDlgIntData()),
              mapper.getTypeFactory().constructCollectionType(List.class, DelegationInt.class));
        } catch (IOException e) {
          wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
          return false;
        }
      }
      if (listDelegation != null && !listDelegation.isEmpty()) {
        for (DelegationInt delegation : listDelegation) {
          if (delegation != null) {
            String rCeref = sensitiveData.show(delegation.getReferenceId());
            if (rCeref.equalsIgnoreCase(localCeref)) {
              if (delegation.getIsCorpAdmin()) {
                isCorpAdmin = true;
              }
            }
          }
        }
      }
    }
    return hasPermission || isCorpAdmin || isCorporateAccount || isOwnerProfile;
  }

  public boolean isAccessibleRedis(String value) {
    StringRedisTemplate strRedisTemplate =
        CustomApplicationContextHolder.getBean(StringRedisTemplate.class);
    String redisKey = value;
    ValueOperations<String, String> val = strRedisTemplate.opsForValue();
    String raw = val.get(redisKey);
    if (StringUtils.isEmpty(raw)) {
      return false;
    }
    return true;
  }

  public boolean isReferenceAccessible(String refid) {
    UpdateMapper updateMapper = CustomApplicationContextHolder.getBean(UpdateMapper.class);
    Integer id = updateMapper.getDraftCountById(refid);
    if (id == null || id < 1) {
      return false;
    }
    return true;
  }

  public boolean isValidFormRequest(UpdateFormRequest updateFormRequest) {
    String refId = updateFormRequest.getRefno();
    UpdateMapper updateMapper = CustomApplicationContextHolder.getBean(UpdateMapper.class);
    Integer count = updateMapper.getDraftCountById(refId);
    if (count > 0) {
      if (updateFormRequest.getRefno() == null) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  public boolean isValidFromJson(String jsonForm, String ceref) {
    ObjectMapper mapper = CustomApplicationContextHolder.getBean(ObjectMapper.class);
    JsonNode rootNode;
    try {
      rootNode = mapper.readTree(jsonForm);
    } catch (IOException e) {
      return false;
    }
    String wingsUser = rootNode.findPath(NotificationConstants.METADATA)
        .findPath(NotificationConstants.WINGS_USER).asText();
    String cerefLocal = rootNode.findPath(Constants.METADATA).findPath(Constants.CEREF).asText();
    if (wingsUser != null && wingsUser.equalsIgnoreCase(getAuthenticationName())) {
      return true;
    }
    if (ceref != null && cerefLocal != null && cerefLocal.equalsIgnoreCase(ceref)) {
      return true;
    }
    return false;
  }

  public boolean isValidBundleIdSubmission(String bundleId) {
    UpdateMapper updateMapper = CustomApplicationContextHolder.getBean(UpdateMapper.class);
    List<UpdateFormRequest> listBundle = updateMapper.getComponentsByReferenceId(bundleId);
    UpdateFormRequest updateFormRequest = listBundle.get(0);
    if (updateFormRequest != null) {
      return true;
    }
    return false;
  }

  @SuppressWarnings({"unused", "unchecked"})
  public boolean isEditAllowed(String ceref) {
    ObjectMapper mapper = CustomApplicationContextHolder.getBean(ObjectMapper.class);
    WingsLoggerDao wingsLoggerDao = CustomApplicationContextHolder.getBean(WingsLoggerDao.class);
    ProfileCommonServiceImpl profileCommonService =
        CustomApplicationContextHolder.getBean(ProfileCommonServiceImpl.class);
    SensitiveDataImpl sensitiveData =
        CustomApplicationContextHolder.getBean(SensitiveDataImpl.class);
    CorpServiceImpl corpService = CustomApplicationContextHolder.getBean(CorpServiceImpl.class);
    RedisCache redisHelper = CustomApplicationContextHolder.getBean(RedisCache.class);
    String login = getAuthenticationName();
    boolean isCorpAdmin = false, isOwnerProfile = false;

    String localCeref = sensitiveData.show(ceref);

    UserProfile response = profileCommonService.getUserProfile("Y");
    if (response != null && localCeref.equalsIgnoreCase(response.getUserCeRef())) {
      isOwnerProfile = true;
    }

    PaginatedDelegationIntDataModel model = profileCommonService.getPaginatedDelegatingInternals(
        localCeref, new Integer(0), new Integer(100), "N", null, null, "asc", null, false);

    List<DelegationInt> listDelegation = null;
    try {
      if (model != null && model.getDlgIntData() != null) {
        listDelegation =
            (List<DelegationInt>) mapper.readValue(mapper.writeValueAsString(model.getDlgIntData()),
                mapper.getTypeFactory().constructCollectionType(List.class, DelegationInt.class));
      }
    } catch (IOException e) {
      wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
      return false;
    }
    if (listDelegation != null && !listDelegation.isEmpty()) {
      for (DelegationInt delegation : listDelegation) {
        if (delegation != null) {
          String rCeref = sensitiveData.show(delegation.getReferenceId());
          if (rCeref.equalsIgnoreCase(localCeref)) {
            if (delegation.getIsCorpAdmin()) {
              isCorpAdmin = true;
            }
          }
        }
      }
    }

    List<Corporation> allCorps = null;
    if (redisHelper.containsInCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY)) {
      try {
        allCorps = mapper.readValue(
            redisHelper.getFromCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY,
                String.class),
            mapper.getTypeFactory().constructCollectionType(List.class, Corporation.class));
      } catch (IOException e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    } else {
      try {
        allCorps = corpService.getCorporationsByLoginName(null, null, false, "Y");
      } catch (Exception e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    }

    boolean hasPermission = false;

    if (allCorps != null && !allCorps.isEmpty()) {
      for (Corporation c : allCorps) {
        if (c != null) {

          Delegation d = null;
          try {
            d = mapper.readValue(JsonUtils.getJsonStringFromObject(c.getDelegation()),
                Delegation.class);
          } catch (JsonParseException e) {
          } catch (JsonMappingException e) {
          } catch (IOException e) {
          }
          if (d != null) {
            if (d.getIsCorpAdmin()) {
              hasPermission = true;
            }
          }

          List<PermissionRight> permissionRights = c.getPermissionRights();
          for (PermissionRight p : permissionRights) {
            if (p != null && (p.getPermissionCode().equals("AILP")
                || (p.getPermissionCode().equals("PM") && p.getKey().equalsIgnoreCase("Edit")))) {
              hasPermission = true;
            }
          }
        }
      }
    } else {
      hasPermission = true;
    }

    return hasPermission || isCorpAdmin || isOwnerProfile;
  }

  @SuppressWarnings({"unused", "unchecked"})
  public boolean isSubmitAllowed(String ceref) {
    ObjectMapper mapper = CustomApplicationContextHolder.getBean(ObjectMapper.class);
    WingsLoggerDao wingsLoggerDao = CustomApplicationContextHolder.getBean(WingsLoggerDao.class);
    ProfileCommonServiceImpl profileCommonService =
        CustomApplicationContextHolder.getBean(ProfileCommonServiceImpl.class);
    SensitiveDataImpl sensitiveData =
        CustomApplicationContextHolder.getBean(SensitiveDataImpl.class);
    CorpServiceImpl corpService = CustomApplicationContextHolder.getBean(CorpServiceImpl.class);
    RedisCache redisHelper = CustomApplicationContextHolder.getBean(RedisCache.class);
    boolean isCorpAdmin = false, isOwnerProfile = false;

    String localCeref = sensitiveData.show(ceref);

    UserProfile response = profileCommonService.getUserProfile("Y");
    if (response != null && localCeref.equalsIgnoreCase(response.getUserCeRef())) {
      isOwnerProfile = true;
    }

    PaginatedDelegationIntDataModel model = profileCommonService.getPaginatedDelegatingInternals(
        localCeref, new Integer(0), new Integer(100), "N", null, null, "asc", null, false);

    List<DelegationInt> listDelegation = null;
    try {
      if (model != null && model.getDlgIntData() != null) {
        listDelegation =
            (List<DelegationInt>) mapper.readValue(mapper.writeValueAsString(model.getDlgIntData()),
                mapper.getTypeFactory().constructCollectionType(List.class, DelegationInt.class));
      }
    } catch (IOException e) {
      wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
      return false;
    }
    if (listDelegation != null && !listDelegation.isEmpty()) {
      for (DelegationInt delegation : listDelegation) {
        if (delegation != null) {
          String rCeref = sensitiveData.show(delegation.getReferenceId());
          if (rCeref.equalsIgnoreCase(localCeref)) {
            if (delegation.getIsCorpAdmin()) {
              isCorpAdmin = true;
            }
          }
        }
      }
    }

    boolean hasPermission = false;
    boolean isCorporateAccount = false;
    List<Corporation> allCorps = null;

    if (redisHelper.containsInCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY)) {
      try {
        allCorps = mapper.readValue(
            redisHelper.getFromCache(RedisConstants.PMDEL, RedisConstants.DELEGATION_KEY,
                String.class),
            mapper.getTypeFactory().constructCollectionType(List.class, Corporation.class));
      } catch (IOException e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    } else {
      try {
        allCorps = corpService.getCorporationsByLoginName(null, null, false, "Y");
      } catch (Exception e) {
        wingsLoggerDao.insertLogs(new Throwable(), ExceptionUtils.getStackTrace(e), localCeref);
        return false;
      }
    }

    if (allCorps != null && !allCorps.isEmpty()) {
      for (Corporation c : allCorps) {
        if (c != null) {
          List<PermissionRight> permissionRights = c.getPermissionRights();
          for (PermissionRight p : permissionRights) {
            if (p.getPermissionCode().equals("AILP")
                || (p.getPermissionCode().equals("PM") && p.getKey().equalsIgnoreCase("Submit"))) {
              hasPermission = true;
            }
          }
        }
      }
    } else {
      hasPermission = true;
    }

    return hasPermission || isCorporateAccount || isCorpAdmin || isOwnerProfile;
  }

  public boolean isAllowedToGroupId(String groupId) {
    LCMapper mapper = CustomApplicationContextHolder.getBean(LCMapper.class);
    CommonMapper commonMapper = CustomApplicationContextHolder.getBean(CommonMapper.class);
    List<CadGroupEntity> list = mapper.getCadGroupEntityByGroupId(groupId);

    UserMapper userMapper = CustomApplicationContextHolder.getBean(UserMapper.class);
    String localCeref = userMapper.findUserCerefByLoginName(getAuthenticationName());
    List<AccreditedCorp> coCeref = commonMapper.getAccreditedList(localCeref);
    String ceref = list.get(0).getCeref();
    for (AccreditedCorp accr : coCeref) {
      if (accr.getCoCeref().equals(ceref)) {
        return isAllowed(null, false, false, null);
      }
    }


    return false;
  }

  public boolean isAllowedListCeref(List<String> ref, HttpServletRequest request) {
    if (ref == null) {
      return false;
    }
    if (ref.isEmpty() || ref.size() == 0) {
      return false;
    }
    for (String ceref : ref) {
      if (isAllowed(ceref, false, false, request)) {
        return true;
      }

    }
    return false;
  }

}
