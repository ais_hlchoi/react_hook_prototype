package com.ais.conn.dao;

import java.util.List;

import com.ais.conn.model.*;

public interface PaymentOrderModelMapper {
	List<PaymentOrderModel> searchPaymentOrder(SearchPaymentOrderRecordCriteriaModel model);
	List<PaymentOrderStatModel> getDailyTotalQuantity(SearchPaymentOrderRecordCriteriaModel model);
	List<SkuQuantityModel> getTop5SkuQuantity(SearchPaymentOrderRecordCriteriaModel model);
	List<PaymentOrderStatModel> getDailyTotalAmt(SearchPaymentOrderRecordCriteriaModel model);
	List<SkuQuantityModel> getTop5SkuAmt(SearchPaymentOrderRecordCriteriaModel model);
}
