package com.ais.conn.model;

public class FcCsvFileUploadDataModel {
	private Integer id;
	private int fileUploadId;
	private String dynamicCols;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getFileUploadId() {
		return fileUploadId;
	}
	public void setFileUploadId(int fileUploadId) {
		this.fileUploadId = fileUploadId;
	}
	public String getDynamicCols() {
		return dynamicCols;
	}
	public void setDynamicCols(String dynamicCols) {
		this.dynamicCols = dynamicCols;
	}
}
