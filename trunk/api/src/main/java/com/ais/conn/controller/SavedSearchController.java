package com.ais.conn.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SavedSearchModel;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/savedSearch")
public class SavedSearchController extends CommonParent {

	@RequestMapping("/search")
	public Object search(@RequestBody SavedSearchModel savedSearchModel) {
		PageHelper.startPage(savedSearchModel.getPage(), savedSearchModel.getPageSize());
		List<SavedSearchModel> list = new ArrayList<SavedSearchModel>();
		return RestEntity.success(new PageInfo<>(list));
	}
	
	@RequestMapping("/searchAll")
	private Object searchAll(@RequestBody SavedSearchModel savedSearchModel) {
		List<SavedSearchModel> list = new ArrayList<SavedSearchModel>();
		return RestEntity.success(list);
	}

	@RequestMapping("/leftMenuSearch")
	public Object leftMenuSearch(@RequestBody SavedSearchModel savedSearchModel) {
		List<SavedSearchModel> list = new ArrayList<SavedSearchModel>();

		return RestEntity.success(list);
	}

	@RequestMapping("/searchByFuncId")
	public Object searchByFuncId(@RequestBody Integer funcId) {
		Map<String, Object> map=new HashMap<String, Object>();
		Integer roleCount = 0;
		Integer userPreferenceCount = 0;
		map.put("roleCount", roleCount);
		map.put("userPreferenceCount", userPreferenceCount);
		return RestEntity.success(map);
	}

	@RequestMapping("/insert")
	public Object insert(@RequestBody SavedSearchModel savedSearchModel) throws SysException {
		
		return RestEntity.success();
	}

	@RequestMapping("/update")
	public Object update(@RequestBody SavedSearchModel savedSearchModel) throws SysException {
		
		return RestEntity.success();
	}

	@RequestMapping("/delete")
	public Object delete(@RequestBody SavedSearchModel savedSearchModel) throws SysException {
		
		return RestEntity.success();
	}

	@RequestMapping("/searchFunc")
	public Object searchFunc(@RequestBody SavedSearchModel savedSearchModel) {
		List<SavedSearchModel> list = new ArrayList<SavedSearchModel>();
		return RestEntity.success(list);
	}
}
