package com.ais.conn.dao;

import java.util.List;

import com.ais.conn.model.FcCsvFileTypeHeaderColumnModel;
import com.ais.conn.model.FcCsvFileTypeHeaderModel;
import com.ais.conn.model.FcCsvFileUploadDataModel;
import com.ais.conn.model.FcCsvFileUploadModel;

public interface FcCsvDataMapper {

	FcCsvFileTypeHeaderModel findEffectiveFileTypeHeader(FcCsvFileTypeHeaderModel bean);

	List<FcCsvFileTypeHeaderColumnModel> findColumnList(FcCsvFileTypeHeaderColumnModel parm);

	int createFileTypeHeader(FcCsvFileTypeHeaderModel dbHeader);

	void createFileTypeHeaderColumn(FcCsvFileTypeHeaderColumnModel column);

	void createFileUpload(FcCsvFileUploadModel upd);

	void createFileUploadData(FcCsvFileUploadDataModel data);

	int getFileTypeIdByName(FcCsvFileTypeHeaderModel header);

}
