package com.ais.conn.model;

public class FcCsvFileTypeHeaderColumnModel {
	private Integer id;
	private int fileTypeHeaderId;
	private int columnIdx;
	private String columnName;
	private String staticInd;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getFileTypeHeaderId() {
		return fileTypeHeaderId;
	}
	public void setFileTypeHeaderId(int fileTypeHeaderId) {
		this.fileTypeHeaderId = fileTypeHeaderId;
	}
	public int getColumnIdx() {
		return columnIdx;
	}
	public void setColumnIdx(int columnIdx) {
		this.columnIdx = columnIdx;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getStaticInd() {
		return staticInd;
	}
	public void setStaticInd(String staticInd) {
		this.staticInd = staticInd;
	}
	
}
