package com.ais.conn.model;

public class PaymentOrderModel {
    private String id;
    private String orderDate;
    private String orderDateWithoutTime;
    private String subOrderNumber;
    private String skuId;
    private String skuDescription;
    private String orderNumber;
    private Integer consignmentOrder;
    private String completionDate;
    private Integer quantity;
    private Double unitCost;
    private Double discount;
    private Double totalCost;
    private Double storeCredit;
    private Double netCost;
    private Double commChargeRate;
    private Double commChargeAmt;
    private Double netAmount;
    private Double storageCharge;
    private String createdOrderUploadId;
    private String lastUpdatedOrderUploadId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubOrderNumber() {
        return subOrderNumber;
    }

    public void setSubOrderNumber(String subOrderNumber) {
        this.subOrderNumber = subOrderNumber;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuDescription() {
        return skuDescription;
    }

    public void setSkuDescription(String skuDescription) {
        this.skuDescription = skuDescription;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getConsignmentOrder() {
        return consignmentOrder;
    }

    public void setConsignmentOrder(Integer consignmentOrder) {
        this.consignmentOrder = consignmentOrder;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Double unitCost) {
        this.unitCost = unitCost;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getCommChargeRate() {
        return commChargeRate;
    }

    public void setCommChargeRate(Double commChargeRate) {
        this.commChargeRate = commChargeRate;
    }

    public Double getCommChargeAmt() {
        return commChargeAmt;
    }

    public void setCommChargeAmt(Double commChargeAmt) {
        this.commChargeAmt = commChargeAmt;
    }

    public String getCreatedOrderUploadId() {
        return createdOrderUploadId;
    }

    public void setCreatedOrderUploadId(String createdOrderUploadId) {
        this.createdOrderUploadId = createdOrderUploadId;
    }

    public String getLastUpdatedOrderUploadId() {
        return lastUpdatedOrderUploadId;
    }

    public void setLastUpdatedOrderUploadId(String lastUpdatedOrderUploadId) {
        this.lastUpdatedOrderUploadId = lastUpdatedOrderUploadId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getStoreCredit() {
        return storeCredit;
    }

    public void setStoreCredit(Double storeCredit) {
        this.storeCredit = storeCredit;
    }

    public Double getNetCost() {
        return netCost;
    }

    public void setNetCost(Double netCost) {
        this.netCost = netCost;
    }

    public Double getStorageCharge() {
        return storageCharge;
    }

    public void setStorageCharge(Double storageCharge) {
        this.storageCharge = storageCharge;
    }

    public String getOrderDateWithoutTime() {
        return orderDateWithoutTime;
    }

    public void setOrderDateWithoutTime(String orderDateWithoutTime) {
        this.orderDateWithoutTime = orderDateWithoutTime;
    }
}
