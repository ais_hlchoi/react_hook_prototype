package com.ais.conn.dao;

import java.util.List;

import com.ais.sys.model.SysParmModel;

public interface SysParmModelMapper {
	int batchInsert(List<SysParmModel> list);
	int batchDelete(List<SysParmModel> list);
	List<SysParmModel> getMasterData();
	String getContractByStore(String store);
}
