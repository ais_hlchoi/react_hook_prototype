package com.ais.conn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ais.conn.service.FileImportService;
import com.ais.conn.service.impl.FileImportServiceImpl;

@Component
public class ScheduleController {

	@Autowired
	private FileImportService fileImportService;

	@Scheduled(fixedDelay = 60000)
	public void impAllocation() {
		try {
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ALLOCATIONS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_EVENTS_EXCHANGE_ORDER);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_EVENTS_ORDER_AND_TRADE_FLOW);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_EVENTS_SOR_AUDIT);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_EVENTS_TRADE_REPORT);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_EVENTS_USER_LOGON);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ARC_SETTINGS_SERVICE_STRATEGY);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_BOOKS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_CHARGE_RULES);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_CLIENTS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_CLOSING_PRICE);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_EXCHANGE_CONNECTIONS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_FRONT_OFFICE_TRADES);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_GROUPS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_INSTRUMENTS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_LOGON_AUDIT_TRAIL);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_LOGON_AUDIT_TRAIL);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_MARKET_SIDE_COUNTERPARTIES);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_ORDER_PROGRESS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_STATIC_DATA_AUDIT);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_SUB_ACCOUNTS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_USER_EXCHANGE_CONNECTIONS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_USER_GROUP_RIGHTS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_USER_MARKET_ROUTES);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_USERS);
			fileImportService.imp(FileImportServiceImpl.FILE_TYPE_MIS_NEW_INSTRUMENTS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
