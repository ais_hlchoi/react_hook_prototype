package com.ais.conn.dao;

import java.util.List;

import com.ais.conn.model.DropDownListModel;

public interface DropDownListModelMapper {
	List<DropDownListModel> getContract();
	List<DropDownListModel> getStore();
}
