package com.ais.conn.model;

public class DynamicRptModel {
	private String datatype;
	private String table;
	private String criteria;
	private String outputColumn;
	private String groupBy;
	private String orderBy;
	private String exportOption;
	private String sumCol;
	private String sql;
	
	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getOutputColumn() {
		return outputColumn;
	}

	public void setOutputColumn(String outputColumn) {
		this.outputColumn = outputColumn;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getExportOption() {
		return exportOption;
	}

	public void setExportOption(String exportOption) {
		this.exportOption = exportOption;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getSumCol() {
		return sumCol;
	}

	public void setSumCol(String sumCol) {
		this.sumCol = sumCol;
	}

}
