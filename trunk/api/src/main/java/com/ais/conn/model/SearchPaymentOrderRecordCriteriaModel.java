package com.ais.conn.model;

import java.util.ArrayList;

public class SearchPaymentOrderRecordCriteriaModel {
    private String defaultSearchText; // SKU_ID / ORDER_NUMBER / SUB_ORDER_NUMBER / SKU_NAME_ENG
    private String orderDateFrom;
    private String orderDateTo;
    private String orderNumber;
    private String subOrderNumber;
    private String skuId;
    private String skuDesc;

    private ArrayList<String> skuIdList;

    public String getDefaultSearchText() {
        return defaultSearchText;
    }

    public void setDefaultSearchText(String defaultSearchText) {
        this.defaultSearchText = defaultSearchText;
    }

    public String getOrderDateFrom() {
        return orderDateFrom;
    }

    public void setOrderDateFrom(String orderDateFrom) {
        this.orderDateFrom = orderDateFrom;
    }

    public String getOrderDateTo() {
        return orderDateTo;
    }

    public void setOrderDateTo(String orderDateTo) {
        this.orderDateTo = orderDateTo;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getSubOrderNumber() {
        return subOrderNumber;
    }

    public void setSubOrderNumber(String subOrderNumber) {
        this.subOrderNumber = subOrderNumber;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuDesc() {
        return skuDesc;
    }

    public void setSkuDesc(String skuDesc) {
        this.skuDesc = skuDesc;
    }

    public ArrayList<String> getSkuIdList() {
        return skuIdList;
    }

    public void setSkuIdList(ArrayList<String> skuIdList) {
        this.skuIdList = skuIdList;
    }
}
