package com.ais.conn.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.OperationLogManageModel;
import com.ais.sys.service.OperationLogManageService;
import com.ais.util.OperationLogUtil;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ConnController {
		
	@Autowired
	private OperationLogManageService operationLogManageService;
		
	
	@RequestMapping(value = "/signOut", method = RequestMethod.POST)
	public RestEntity signOut(HttpServletRequest request, HttpServletResponse res, @RequestBody OperationLogManageModel model) throws Exception {
		RestEntity response = null;
		response = RestEntity.success();
		
		Integer logId = operationLogManageService.insert(OperationLogUtil.genLog(request, model, model.getUserId(), OperationLogUtil.getRealIp(request), "LOGOUT", "", "", null));
		operationLogManageService.update(OperationLogUtil.genLog(request, model, model.getUserId(), OperationLogUtil.getRealIp(request), request.getRequestURL().toString(), response.getResponseCode(), response.getResponseDesc(), logId));
		
		return response;
	}
}
