package com.ais.conn.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ais.conn.model.DynamicRptModel;
import com.ais.conn.model.PaymentOrderModel;
import com.ais.conn.model.PaymentOrderStatModel;
import com.ais.conn.model.SearchPaymentOrderRecordCriteriaModel;
import com.ais.conn.model.SkuQuantityModel;

public interface PaymentOrderService {
    public List<PaymentOrderModel> searchPaymentOrder(SearchPaymentOrderRecordCriteriaModel model) throws Exception;
    public List<PaymentOrderStatModel> getDailyTotalQuantity(SearchPaymentOrderRecordCriteriaModel model) throws Exception;
    public List<SkuQuantityModel> getTop5SkuQuantityModel(SearchPaymentOrderRecordCriteriaModel model) throws Exception;
    public List<PaymentOrderStatModel> getDailyTotalAmt(SearchPaymentOrderRecordCriteriaModel model) throws Exception;
    public List<SkuQuantityModel> getTop5SkuAmtModel(SearchPaymentOrderRecordCriteriaModel model) throws Exception;
    public XSSFWorkbook exportXls(DynamicRptModel model) throws SQLException, FileNotFoundException, IOException;
    public File exportCsv(DynamicRptModel model) throws SQLException, IOException;
    public File exportPdf(DynamicRptModel model) throws SQLException, IOException;
}