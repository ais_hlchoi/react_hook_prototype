package com.ais.conn.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ais.conn.model.DynamicRptModel;
import com.ais.conn.model.PaymentOrderModel;
import com.ais.conn.model.PaymentOrderStatModel;
import com.ais.conn.model.SearchPaymentOrderRecordCriteriaModel;
import com.ais.conn.model.SkuQuantityModel;
import com.ais.conn.service.PaymentOrderService;
import com.ais.util.FileUtil;

@RestController
@CrossOrigin
@RequestMapping("/api/paymentorder")
public class DataExportController {
	static Logger logger = LogManager.getLogger(DataExportController.class);

	@Autowired
	private PaymentOrderService paymentOrderService;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<PaymentOrderModel> searchPaymentOrderRecord(HttpServletRequest request,
			@RequestBody SearchPaymentOrderRecordCriteriaModel model) throws Exception {
		return paymentOrderService.searchPaymentOrder(model);
	}

	@RequestMapping(value = "/getDailyTotalQuantity", method = RequestMethod.POST)
	public List<PaymentOrderStatModel> getDailyTotalQuantity(HttpServletRequest request, HttpServletResponse res,
			@RequestBody SearchPaymentOrderRecordCriteriaModel model) throws Exception {
		return paymentOrderService.getDailyTotalQuantity(model);
	}

	@RequestMapping(value = "/getTop5SkuTotalQuantity", method = RequestMethod.POST)
	public List<SkuQuantityModel> getTop5SkuTotalQuantity(HttpServletRequest request, HttpServletResponse res,
			@RequestBody SearchPaymentOrderRecordCriteriaModel model) throws Exception {
		return paymentOrderService.getTop5SkuQuantityModel(model);
	}

	@RequestMapping(value = "/getDailyTotalAmt", method = RequestMethod.POST)
	public List<PaymentOrderStatModel> getDailyTotalAmt(HttpServletRequest request, HttpServletResponse res,
			@RequestBody SearchPaymentOrderRecordCriteriaModel model) throws Exception {
		return paymentOrderService.getDailyTotalAmt(model);
	}

	@RequestMapping(value = "/getTop5SkuTotalAmt", method = RequestMethod.POST)
	public List<SkuQuantityModel> getTop5SkuTotalAmt(HttpServletRequest request, HttpServletResponse res,
			@RequestBody SearchPaymentOrderRecordCriteriaModel model) throws Exception {
		return paymentOrderService.getTop5SkuAmtModel(model);
	}

	@ResponseBody
	@RequestMapping(value = "/generateDynamicRptXlsx", method = RequestMethod.POST, produces = { "application/xml",
			"application/json" })
	public void getDynamicRptXlsx(HttpServletRequest request, HttpServletResponse res,
			@RequestBody DynamicRptModel model) throws Exception {
		try {
			String fileName = "custom_report_xlsx_20201106.xlsx";
			XSSFWorkbook workbook2 = paymentOrderService.exportXls(model);
			FileUtil.downloadExcelFileUTF8(workbook2, fileName);
			workbook2.close();
		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/generateDynamicRptCsv", method = RequestMethod.POST, produces = { "text/csv" })
	public void getDynamicRptCsv(HttpServletRequest request, HttpServletResponse res,
			@RequestBody DynamicRptModel model) throws Exception {
		try {
			File file = paymentOrderService.exportCsv(model);
			FileUtil.downloadExcelFileUTF8(file, "custom_report_csv_20201106.csv", "text/csv");
		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/generateDynamicRptPdf", method = RequestMethod.POST, produces = { "application/pdf" })
	public void getDynamicRptPdf(HttpServletRequest request, HttpServletResponse res,
			@RequestBody DynamicRptModel model) throws Exception {
		try {
			File file = paymentOrderService.exportPdf(model);
			FileUtil.downloadExcelFileUTF8(file, "custom_report_pdf_20201106.pdf", "application/pdf");
		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
}
