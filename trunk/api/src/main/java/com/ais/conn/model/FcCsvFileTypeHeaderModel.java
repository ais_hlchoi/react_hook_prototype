package com.ais.conn.model;

import java.util.Date;

public class FcCsvFileTypeHeaderModel {
	private Integer id;
	private int fileTypeId;
	private Date effectiveDate;
	private String fileNameStart;
	private String headerName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getFileTypeId() {
		return fileTypeId;
	}
	public void setFileTypeId(int fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getFileNameStart() {
		return fileNameStart;
	}
	public void setFileNameStart(String fileNameStart) {
		this.fileNameStart = fileNameStart;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	
}
