package com.ais.conn.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.conn.dao.PaymentOrderModelMapper;
import com.ais.conn.model.DynamicRptModel;
import com.ais.conn.model.PaymentOrderModel;
import com.ais.conn.model.PaymentOrderStatModel;
import com.ais.conn.model.SearchPaymentOrderRecordCriteriaModel;
import com.ais.conn.model.SkuQuantityModel;
import com.ais.conn.service.PaymentOrderService;
import com.ais.util.ExcelUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.util.StringUtils;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.styledxmlparser.css.media.MediaDeviceDescription;
import com.itextpdf.styledxmlparser.css.media.MediaType;
import static com.itextpdf.html2pdf.css.CssConstants.LANDSCAPE;

@Service
public class PaymentOrderServiceImpl implements PaymentOrderService {
	static Logger logger = LogManager.getLogger(PaymentOrderServiceImpl.class);

	@Autowired
	private PaymentOrderModelMapper paymentOrderModelMapper;

	@Autowired
	private DruidDataSource dataSourceObj;

	public List<PaymentOrderModel> searchPaymentOrder(SearchPaymentOrderRecordCriteriaModel model) {
		return paymentOrderModelMapper.searchPaymentOrder(model);
	}

	public List<PaymentOrderStatModel> getDailyTotalQuantity(SearchPaymentOrderRecordCriteriaModel model) {
		List<SkuQuantityModel> top5Sku = paymentOrderModelMapper.getTop5SkuQuantity(model);
		ArrayList<String> skuIdList = null;
		if (top5Sku != null && top5Sku.size() > 0) {
			skuIdList = new ArrayList<String>();
			for (SkuQuantityModel sku : top5Sku) {
				skuIdList.add(sku.getSkuId());
			}
			model.setSkuIdList(skuIdList);
		}
		return paymentOrderModelMapper.getDailyTotalQuantity(model);
	}

	public List<SkuQuantityModel> getTop5SkuQuantityModel(SearchPaymentOrderRecordCriteriaModel model) {
		return paymentOrderModelMapper.getTop5SkuQuantity(model);
	}

	public List<PaymentOrderStatModel> getDailyTotalAmt(SearchPaymentOrderRecordCriteriaModel model) {
		List<SkuQuantityModel> top5Sku = paymentOrderModelMapper.getTop5SkuQuantity(model);
		ArrayList<String> skuIdList = null;
		if (top5Sku != null && top5Sku.size() > 0) {
			skuIdList = new ArrayList<String>();
			for (SkuQuantityModel sku : top5Sku) {
				skuIdList.add(sku.getSkuId());
			}
			model.setSkuIdList(skuIdList);
		}
		return paymentOrderModelMapper.getDailyTotalAmt(model);
	}

	public List<SkuQuantityModel> getTop5SkuAmtModel(SearchPaymentOrderRecordCriteriaModel model) {
		return paymentOrderModelMapper.getTop5SkuAmt(model);
	}

	private List<Map<String, Object>> executeSql(String sql) throws SQLException {
		logger.info(sql);
		Connection conn = dataSourceObj.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData metaData = rs.getMetaData();
		int columnCount = metaData.getColumnCount();
		List<Map<String, Object>> queryResult = new ArrayList<Map<String, Object>>();
		while (rs.next()) {
			Map<String, Object> columns = new LinkedHashMap<String, Object>();
			for (int i = 1; i <= columnCount; i++) {
				columns.put(metaData.getColumnLabel(i), rs.getObject(i));
			}
			queryResult.add(columns);
		}
		return queryResult;
	}

	public final static String XLSX_TEMPLATE_PATH = "C:/Users/hoilokchoi/Desktop/fidessa/custom_report_xlsx_template.xlsx";
	public final static String FILE_OUTPUT_PATH = "C:/Users/hoilokchoi/Desktop/fidessa/output/";
	private final int ROW_START = 7;

	public XSSFWorkbook exportXls(DynamicRptModel model) throws SQLException, FileNotFoundException, IOException {
		List<Map<String, Object>> queryResult = executeSql(StringUtils.isEmpty(model.getSql()) ? constructSql(model) : model.getSql());
		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(XLSX_TEMPLATE_PATH)));
		XSSFSheet sheet = workbook.getSheetAt(0);

		// no need style, we load from template, template got style already
		ExcelUtil.setCellValue(sheet, "B", 2, datetime, ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getOutputColumn()))
			ExcelUtil.setCellValue(sheet, "B", 4, model.getOutputColumn(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getTable()))
			ExcelUtil.setCellValue(sheet, "B", 5, model.getTable(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getCriteria()))
			ExcelUtil.setCellValue(sheet, "E", 4, model.getCriteria(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getOrderBy()))
			ExcelUtil.setCellValue(sheet, "G", 4, model.getOrderBy(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getGroupBy()))
			ExcelUtil.setCellValue(sheet, "G", 5, model.getGroupBy(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getSumCol()))
			ExcelUtil.setCellValue(sheet, "K", 4, model.getSumCol(), ExcelUtil.TEXT, null);
		if (!StringUtils.isEmpty(model.getSql()))
			ExcelUtil.setCellValue(sheet, "K", 5, model.getSql(), ExcelUtil.TEXT, null);

		int rowIdx = ROW_START;
		if (queryResult != null && !queryResult.isEmpty()) {
			int colIdx = 0;
			XSSFCellStyle style = workbook.createCellStyle();
			XSSFFont font = workbook.createFont();
			font.setBold(true);
			style.setFont(font);
			for (String colName : queryResult.get(0).keySet()) {
				ExcelUtil.setCellValue(sheet, colIdx, rowIdx, colName, ExcelUtil.TEXT, style);
				colIdx++;
			}

			rowIdx++;
			for (Map<String, Object> data : queryResult) {
				colIdx = 0;
				for (String colName : data.keySet()) {
					ExcelUtil.setCellValue(sheet, colIdx, rowIdx, data.get(colName) == null?"":data.get(colName), ExcelUtil.TEXT, null);
					colIdx++;
				}
				rowIdx++;
			}
		}

		FileOutputStream out = new FileOutputStream(new File(FILE_OUTPUT_PATH + "custom_report_xlsx_"
				+ new SimpleDateFormat("yyyyMMddHHmmss").format(now) + ".xlsx"));
		workbook.write(out);
		out.close();
		return workbook;
		/*
		 * return new XSSFWorkbook(new FileInputStream(new File(
		 * "C:\\Users\\hoilokchoi\\Desktop\\fidessa\\custom_report_xlsx_20201106.xlsx"))
		 * );
		 */
	}

	public File exportCsv(DynamicRptModel model) throws SQLException, IOException {
		List<Map<String, Object>> queryResult = executeSql(StringUtils.isEmpty(model.getSql()) ? constructSql(model) : model.getSql());

		File file = new File(FILE_OUTPUT_PATH + "custom_report_csv_"
				+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".csv");
		Writer writer = new FileWriter(file, true);
		CsvSchema schema = null;
		CsvSchema.Builder schemaBuilder = CsvSchema.builder();
		if (queryResult != null && !queryResult.isEmpty()) {
			for (String col : queryResult.get(0).keySet()) {
				schemaBuilder.addColumn(col);
			}
			schema = schemaBuilder.build().withLineSeparator(System.lineSeparator()).withHeader();
		}
		CsvMapper mapper = new CsvMapper();
		mapper.writer(schema).writeValues(writer).writeAll(queryResult);
		writer.flush();
		return file;
		/*
		 * return new File(
		 * "C:\\Users\\hoilokchoi\\Desktop\\fidessa\\custom_report_csv_20201106.csv");
		 */
	}

	public File exportPdf(DynamicRptModel model) throws SQLException, IOException {
		List<Map<String, Object>> queryResult = executeSql(StringUtils.isEmpty(model.getSql()) ? constructSql(model) : model.getSql());
		String destFile = FILE_OUTPUT_PATH + "custom_report_pdf_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".pdf";
		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		PdfDocument pdfDocument = new PdfDocument(new PdfWriter(new File(destFile)));
	    pdfDocument.setDefaultPageSize(PageSize.A4.rotate());
	    ConverterProperties properties = new ConverterProperties();
        MediaDeviceDescription med = new MediaDeviceDescription(MediaType.ALL);
        med.setOrientation(LANDSCAPE);
        properties.setMediaDeviceDescription(med);
        StringBuilder html = new StringBuilder();
        html.append("<h3 style=\"font-size:14px;\">Custom Report</h3><br/>");
        html.append("<table style=\"font-size:12px;\">");
        html.append("<tr><td>").append("<b>Export Date:</b>").append("</td><td>").append(datetime).append("</td></tr>");
        html.append("<tr><td>").append("<b>Search Criteria:</b>").append("</td></tr>");
        html.append("<tr><td>").append("<b>Search clause:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getOutputColumn())?"--":model.getOutputColumn())
        	.append("</td><td>").append("<b>Where clause:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getCriteria())?"--":model.getCriteria())
        	.append("</td><td>").append("<b>Order by:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getOrderBy())?"--":model.getOrderBy())
        	.append("</td><td>").append("<b>Sum column:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getSumCol())?"--":model.getSumCol())
        	.append("</td></tr>");
        html.append("<tr><td>").append("<b>From clause:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getTable())?"--":model.getTable())
	    	.append("</td><td>").append("<b></b>").append("</td><td>").append("")
	    	.append("</td><td>").append("<b>Group by:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getGroupBy())?"--":model.getGroupBy())
	    	.append("</td><td>").append("<b>sql:</b>").append("</td><td>").append(StringUtils.isEmpty(model.getSql())?"--":model.getSql())
	    	.append("</td></tr>");
        html.append("</table><br/><br/>");
                
		if (queryResult != null && !queryResult.isEmpty()) {
			html.append("<table style=\"font-size:12px;\">");
			html.append("<tr>");
			for (String colName : queryResult.get(0).keySet())
				 html.append("<td><b>").append(colName).append("</b></td>");
			html.append("</tr>");
			
			for (Map<String, Object> data : queryResult) {
				html.append("<tr>");
				for (String colName : data.keySet()) 
					html.append("<td>").append(data.get(colName) == null?"":data.get(colName)).append("</td>");
				html.append("</tr>");
			}
			html.append("</table>");
		}        
	    HtmlConverter.convertToPdf(html.toString(), pdfDocument, properties);
	    pdfDocument.close();
		return new File(destFile);
	}

	private final String WITH_TEMPLATE = ",%1$s_file_set as (select id from fc_csv_file_upload where file_type_id = %2$s)"
			+ ",%1$s_data_set as (select d.* from fc_csv_file_upload_data d, %1$s_file_set where d.file_upload_id = %1$s_file_set.id)";
	private final String SELECT_TEMPLATE = ",COLUMN_GET(%1$s.DYNAMIC_COLS, '%2$s' as char) %2$s";

	private String constructSql(DynamicRptModel model) {
		StringBuilder withClause = new StringBuilder();
		StringBuilder fromClause = new StringBuilder();
		StringBuilder selectClause = new StringBuilder();

		if (!StringUtils.isEmpty(model.getTable())) {
			String[] tableNames = model.getTable().replaceAll("\\s+", "").split(",");
			for (String tableName : tableNames) {
				if ("ALLOCATIONS".equals(tableName)) {
					String dataSet = "allo";
					String fileTypeId = "1";
					withClause.append(String.format(WITH_TEMPLATE, dataSet, fileTypeId));
					fromClause.append(",").append(dataSet).append("_data_set");
				}
			}
		}

		if (!StringUtils.isEmpty(model.getOutputColumn())) {
			String[] colNames = model.getOutputColumn().replaceAll("\\s+", "").split(",");
			for (String colName : colNames) {
				if (colName.startsWith("ALLOCATIONS")) {
					String tableAlias = "allo_data_set";
					String colAlias = colName.substring(colName.indexOf(".") + 1);
					selectClause.append(String.format(SELECT_TEMPLATE, tableAlias, colAlias));
				}
			}
		}

		StringBuilder sql = new StringBuilder();
		sql.append(" With ").append(withClause.toString().substring(1)).append(" Select ")
				.append(selectClause.toString().substring(1)).append(" from ")
				.append(fromClause.toString().substring(1));
		return sql.toString();
	}
}
