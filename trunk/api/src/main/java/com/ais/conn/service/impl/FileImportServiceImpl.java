package com.ais.conn.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.conn.dao.FcCsvDataMapper;
import com.ais.conn.model.FcCsvFileTypeHeaderColumnModel;
import com.ais.conn.model.FcCsvFileTypeHeaderModel;
import com.ais.conn.model.FcCsvFileUploadDataModel;
import com.ais.conn.model.FcCsvFileUploadModel;
import com.ais.conn.service.FileImportService;
import com.ais.sys.model.OperationLogManageModel;
import com.ais.sys.service.OperationLogManageService;
import com.alibaba.druid.util.StringUtils;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

@Service
public class FileImportServiceImpl implements FileImportService {
	static Logger logger = LogManager.getLogger(FileImportServiceImpl.class);

	@Autowired
	private FcCsvDataMapper fcCsvDataMapper;

	@Autowired
	private OperationLogManageService operationLogManageService;

	@Autowired
	private SqlSession sqlSession;

	private static String FOLDER_LOCATION = "C:/Users/hoilokchoi/Desktop/fidessa/robot/download/file/";
	private static String FOLDER_LOAD_PROCESSING = "process/";
	private static String FOLDER_LOAD_SUCESS = "success/";
	private static String FOLDER_LOAD_FAIL = "fail/";

	// Please verify with fc_csv_file_type
	public final static String FILE_TYPE_ALLOCATIONS = "ALLOCATIONS-";
	public final static String FILE_TYPE_ARC_EVENTS_EXCHANGE_ORDER = "ARC_EVENTS-Exchange_Order-";
	public final static String FILE_TYPE_ARC_EVENTS_ORDER_AND_TRADE_FLOW = "ARC_EVENTS-Order_and_Trade_Flow-";
	public final static String FILE_TYPE_ARC_EVENTS_SOR_AUDIT = "ARC_EVENTS-SOR_Audit-";
	public final static String FILE_TYPE_ARC_EVENTS_TRADE_REPORT = "ARC_EVENTS-Trade_Report-";
	public final static String FILE_TYPE_ARC_EVENTS_USER_LOGON = "ARC_EVENTS-User_Logon-";
	public final static String FILE_TYPE_ARC_SETTINGS_SERVICE_STRATEGY = "ARC_SETTINGS-Service_Strategy-";
	public final static String FILE_TYPE_BOOKS = "BOOKS-";
	public final static String FILE_TYPE_CHARGE_RULES = "CHARGE_RULES-";
	public final static String FILE_TYPE_CLIENTS = "CLIENTS-";
	public final static String FILE_TYPE_CLOSING_PRICE = "CLOSING_PRICE-";
	public final static String FILE_TYPE_EXCHANGE_CONNECTIONS = "EXCHANGE_CONNECTIONS-";
	public final static String FILE_TYPE_FRONT_OFFICE_TRADES = "FRONT_OFFICE_TRADES-";
	public final static String FILE_TYPE_GROUPS = "GROUPS-";
	public final static String FILE_TYPE_INSTRUMENTS = "INSTRUMENTS-";
	public final static String FILE_TYPE_LOGON_AUDIT_TRAIL = "LOGON_AUDIT_TRAIL-";
	public final static String FILE_TYPE_MARKET_SIDE_COUNTERPARTIES = "MARKET_SIDE_COUNTERPARTIES-";
	public final static String FILE_TYPE_MIS_NEW_INSTRUMENTS = "MIS_NEW_INSTRUMENTS-";
	public final static String FILE_TYPE_ORDER_PROGRESS = "ORDER_PROGRESS-";
	public final static String FILE_TYPE_STATIC_DATA_AUDIT = "STATIC_DATA_AUDIT-";
	public final static String FILE_TYPE_SUB_ACCOUNTS = "SUB_ACCOUNTS-";
	public final static String FILE_TYPE_USER_EXCHANGE_CONNECTIONS = "USER_EXCHANGE_CONNECTIONS-";
	public final static String FILE_TYPE_USER_GROUP_RIGHTS = "USER_GROUP_RIGHTS-";
	public final static String FILE_TYPE_USER_MARKET_ROUTES = "USER_MARKET_ROUTES-";
	public final static String FILE_TYPE_USERS = "USERS-";

	@SuppressWarnings("resource")
	public void imp(String action) throws Exception {
		String desc = null;
		File dir = new File(FOLDER_LOCATION);
		String[] directoryListing = dir.list();
		if (directoryListing != null) {
			for (String fileName : directoryListing) {
				if(new File(FOLDER_LOCATION + fileName).isFile() && fileName.indexOf(action) == 0) {
					OperationLogManageModel log = new OperationLogManageModel();
					log.setUserId(1);
					log.setUserCode("SCHEDULE");
					log.setOperationType("SCHEDULE");
					log.setStatus("PENDING");
					log.setServiceAddress("127.0.0.1");
					log.setLoginIpAddress("127.0.0.1");
					log.setOperationContent("Import file: " + fileName);
					log.setStartDate(new Date());
					Integer logId = operationLogManageService.insert(log);
					boolean result = false;
					try {
						desc = handleFiles(action, fileName);
						log.setOperationLogId(logId);
						log.setEndDate(new Date());
						log.setStatus("SUCCESS");
						log.setResponseCode("Success");
						log.setResponseDesc(desc);
						operationLogManageService.update(log);
						result = true;
					} catch (Exception e) {
						e.printStackTrace();
						log.setStatus("FAILED");
						log.setResponseCode("Error");
						log.setResponseDesc(e.getMessage());
					}
					if (!result)
						operationLogManageService.update(log);
				}
			}
		} else {
			throw new Exception("Setup error, folder not exists: " + FOLDER_LOCATION);
		}

	}

	private String handleFiles(String headerName, String fileName) throws Exception {
		if (new File(FOLDER_LOCATION + fileName).exists())
			moveToProcess(fileName);
		else
			return "File not found. No file imported";

		String csvName = FOLDER_LOCATION + FOLDER_LOAD_PROCESSING + fileName;
		CSVReader reader = null;
		List<String> fileColumnNameList = new ArrayList<String>();
		List<String> dynamicColumn = new ArrayList<String>();
		boolean loadSuccess = false;
		String errMsg = "";
		try {
			reader = new CSVReader(new FileReader(csvName));
			String[] line;
			int lineCnt = 1;
			while ((line = reader.readNext()) != null) {
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < line.length; i++) {
					if (StringUtils.isEmpty(line[i]))
						break;
					if (lineCnt == 1) {
						fileColumnNameList.add(line[i]);
					} else {
						builder.append(",'" + StringEscapeUtils.escapeSql(fileColumnNameList.get(i)) + "', '"
								+ StringEscapeUtils.escapeSql(line[i]) + "'");
					}
				}
				if (lineCnt > 1 && !StringUtils.isEmpty(builder.toString()))
					dynamicColumn.add(builder.toString().substring(1));
				lineCnt++;
			}
			FcCsvFileTypeHeaderModel dbHeader = findEffectiveFileTypeHeader(headerName);
			if (dbHeader == null) {
				FcCsvFileTypeHeaderModel newHeader = new FcCsvFileTypeHeaderModel();
				newHeader.setHeaderName(headerName);
				int newFileTypeId = (fcCsvDataMapper.getFileTypeIdByName(newHeader));
				createFileTypeHeader(newFileTypeId);
				dbHeader = findEffectiveFileTypeHeader(headerName);
				if (dbHeader == null) // still null then show error
					throw new Exception("Effective header for [" + headerName + "] not found.");
			}
			List<FcCsvFileTypeHeaderColumnModel> dbColumnList = findColumnList(dbHeader.getId());
			boolean compareColumnList = compareColumnList(dbColumnList, fileColumnNameList);
			int headerId = dbHeader.getId();
			if (!compareColumnList) {
				headerId = createFileTypeHeader(dbHeader.getFileTypeId());
				for (int i = 0; i < fileColumnNameList.size(); i++) {
					createFileTypeHeaderColumn(headerId, i, fileColumnNameList.get(i));
				}
			}
			int fileUploadId = createFcCsvFileUpload(dbHeader.getFileTypeId(), headerId, csvName);
			for (int i = 0; i < dynamicColumn.size(); i++) {
				createFcCsvFileData(fileUploadId, dynamicColumn.get(i));
			}
			loadSuccess = true;
		} catch (IOException e) {
			logger.error(e);
			errMsg = e.getMessage();
		} catch (CsvValidationException e) {
			e.printStackTrace();
			errMsg = e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			errMsg = e.getMessage();
		} finally {
			if (reader != null)
				reader.close();
		}
		if (loadSuccess) {
			moveToSuccess(fileName);
			return dynamicColumn.size() + " row(s) has bean imported.";
		} else {
			moveToFail(fileName);
			throw new Exception(errMsg);
		}
	}

	private void moveToFail(String fileName) throws IOException {
		Path from = Paths.get(FOLDER_LOCATION + FOLDER_LOAD_PROCESSING + fileName);
		Path to = Paths.get(FOLDER_LOCATION + FOLDER_LOAD_FAIL + fileName);
		Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
	}

	private void moveToSuccess(String fileName) throws IOException {
		Path from = Paths.get(FOLDER_LOCATION + FOLDER_LOAD_PROCESSING + fileName);
		Path to = Paths.get(FOLDER_LOCATION + FOLDER_LOAD_SUCESS + fileName);
		Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
	}

	private void moveToProcess(String fileName) throws IOException {
		Path from = Paths.get(FOLDER_LOCATION + fileName);
		Path to = Paths.get(FOLDER_LOCATION + FOLDER_LOAD_PROCESSING + fileName);
		Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
	}

	private void createFcCsvFileData(int fileUploadId, String dynamicCols) {
		FcCsvFileUploadDataModel data = new FcCsvFileUploadDataModel();
		data.setFileUploadId(fileUploadId);
		data.setDynamicCols(dynamicCols);
		fcCsvDataMapper.createFileUploadData(data);
	}

	private int createFcCsvFileUpload(int fileTypeId, int headerId, String csvName) {
		FcCsvFileUploadModel upd = new FcCsvFileUploadModel();
		upd.setFileTypeId(fileTypeId);
		upd.setFileTypeHeaderId(headerId);
		upd.setFileName(csvName);
		upd.setUploadDate(new Date());
		fcCsvDataMapper.createFileUpload(upd);
		return upd.getId();
	}

	private void createFileTypeHeaderColumn(int headerId, int idx, String columnName) {
		FcCsvFileTypeHeaderColumnModel column = new FcCsvFileTypeHeaderColumnModel();
		column.setFileTypeHeaderId(headerId);
		column.setColumnIdx(idx);
		column.setColumnName(columnName);
		column.setStaticInd("N");
		fcCsvDataMapper.createFileTypeHeaderColumn(column);
	}

	private int createFileTypeHeader(int fileTypeId) {
		FcCsvFileTypeHeaderModel header = new FcCsvFileTypeHeaderModel();
		header.setFileTypeId(fileTypeId);
		header.setEffectiveDate(new Date());
		fcCsvDataMapper.createFileTypeHeader(header);
		return header.getId();
	}

	private boolean compareColumnList(List<FcCsvFileTypeHeaderColumnModel> dbColumnList,
			List<String> fileColumnNameList) {
		if (dbColumnList.size() != fileColumnNameList.size())
			return false;
		for (FcCsvFileTypeHeaderColumnModel model : dbColumnList) {
			if (!fileColumnNameList.contains(model.getColumnName()))
				return false;
		}
		return true;
	}

	private List<FcCsvFileTypeHeaderColumnModel> findColumnList(int fileTypeHeaderId) {
		FcCsvFileTypeHeaderColumnModel parm = new FcCsvFileTypeHeaderColumnModel();
		parm.setFileTypeHeaderId(fileTypeHeaderId);
		return fcCsvDataMapper.findColumnList(parm);
	}

	private FcCsvFileTypeHeaderModel findEffectiveFileTypeHeader(String fileName) {
		FcCsvFileTypeHeaderModel parm = new FcCsvFileTypeHeaderModel();
		parm.setFileNameStart(fileName);
		return fcCsvDataMapper.findEffectiveFileTypeHeader(parm);
	}

}
