package com.ais.conn.model;

import java.util.Date;

public class FcCsvFileUploadModel {
	private Integer id;
	private int fileTypeId;
	private int fileTypeHeaderId;
	private String fileName;
	private Date uploadDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getFileTypeId() {
		return fileTypeId;
	}
	public void setFileTypeId(int fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	public int getFileTypeHeaderId() {
		return fileTypeHeaderId;
	}
	public void setFileTypeHeaderId(int fileTypeHeaderId) {
		this.fileTypeHeaderId = fileTypeHeaderId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	
}
