package com.ais.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;

import com.ais.sys.model.OperationLogManageModel;
//import com.alibaba.druid.util.StringUtils;

public class OperationLogUtil {
	public static OperationLogManageModel genLog(HttpServletRequest request, Object o, Integer userId, String clientIp, String apiName, String responseCode, String responseDesc, Integer logId) {
		OperationLogManageModel log = new OperationLogManageModel();
		String serverIpAddress = "";
    	InetAddress ip;
    	
    	//get server ip
    	try {
			ip = InetAddress.getLocalHost();		
	    	serverIpAddress = ip.getHostAddress();
	    	if(request.getParameter("userId") == null) {
	    		log.setUserId(userId);
	    	} else {
	    		log.setUserId(Integer.parseInt(request.getParameter("userId")));
	    	}
			log.setUserCode(request.getParameter("usercode"));
			log.setOperationType(apiName); //request.getRequestURL().toString()
			log.setStatus(responseCode==""?"PENDING":(responseCode=="M1000"?"SUCCESS":"FAILED"));
			log.setServiceAddress(serverIpAddress);
			log.setLoginIpAddress(clientIp);
			log.setResponseCode(responseCode);
			log.setResponseDesc(responseDesc);
			
			if(o instanceof List && o != null) {
				StringBuffer str = new StringBuffer();
				for(Object obj : ((List) o)) {
					str.append(obj!=null?obj.toString():"No request parameter found.");		
				}
				log.setOperationContent(str.toString());
			}
			else {
				log.setOperationContent(o!=null?o.toString():"No request parameter found.");
			}
			
			if(logId != null)
				log.setOperationLogId(logId);
			
			return log;
    	} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static String getRealIp(HttpServletRequest request) {
		String realIP = request.getHeader("x-forwarded-for");
		String ip = request.getRemoteAddr();
		return "0:0:0:0:0:0:0:1".equals(ip)? (StringUtils.isEmpty(realIP)?ip:realIP):ip;
	}
	
	/*
	public static ApiLogModel genLog(Object o, HttpServletRequest request) {
		ApiLogModel log = new ApiLogModel();
		log.setApiName(request.getRequestURL().toString());
		log.setClientIp(request.getRemoteAddr());
		log.setLogTime(new Date());
		log.setRequestHttpHeader(o!=null?o.toString():"no request parameter found");
		log.setResponseCode(((ApiLogModel)o).getResponseCode());
		log.setResponseDesc(((ApiLogModel)o).getResponseDesc());
		log.setUserId("SYSTEM");
		return log;
	}
	*/
}
