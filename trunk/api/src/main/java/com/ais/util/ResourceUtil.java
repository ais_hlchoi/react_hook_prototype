package com.ais.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility for read configuration file
 * 
 * @author AIS
 */
public class ResourceUtil {

	private static final Logger LOG = LoggerFactory.getLogger(ResourceUtil.class);

	private static final String WS_ENV = "WS_API_PROPERTIES";
	private static final String ENV_CONFIG_FILE = "com/ais/project/config/env.properties";
	private static final String SYSTEM_CONFIG_FILE = "com/ais/project/config/system.properties";
	private static final String WS_API_FILE = "com/ais/project/config/wsapi.json";

	private ResourceUtil() {
	}

	/**
	 * load system.properties file
	 * 
	 * @return
	 */
	public static Properties getSystemConfig() {
		String mmsSystemProperties = System.getenv((String)loadProperties(ENV_CONFIG_FILE).get("env"));
		if(StringUtils.isNotEmpty(mmsSystemProperties))
			return loadPropertiesByFile(mmsSystemProperties);
		else
			return loadProperties(SYSTEM_CONFIG_FILE);
	}

	private static Properties loadPropertiesByFile(String propertiesFile) {
		Properties properties = new Properties();
		try (
			FileInputStream in = new FileInputStream(new File(propertiesFile))){
			properties.load(in);
		} catch (IOException e) {
			LOG.error("get system config file error: " + e.getMessage(), e);
		}

		return properties;
	}
	
	/**
	 * load properties file object by file name
	 * 
	 * @param propertiesFile
	 * @return
	 */
	private static Properties loadProperties(String propertiesFile) {
		Properties properties = new Properties();
		ClassLoader cl = ResourceUtil.class.getClassLoader();
		URL url = cl.getResource(propertiesFile);

		try (InputStream in = url.openStream();) {
			properties.load(in);
		} catch (IOException e) {
			LOG.error("get system config file error: " + e.getMessage(), e);
		}

		return properties;
	}
	
	
	public static String getAdminPortal(){
		String adminPortalFlag = getSystemConfig().getProperty("web.adminPortal");
		
		return adminPortalFlag;
	}
	
	public static void checkDirFile(String dir) {
		try {
			if (StringUtils.isNotEmpty(dir)) {

				if (!new File(dir).exists()) {
					new File(dir).mkdirs();
				}
			}
		} catch (Exception e) {
			LOG.error("mkdir: " +dir +" fail"+ e.getMessage(), e);
		}

	}

	/*public static JSONObject getWsApiConfig(String wsName){
		try { 
			LOG.info("Get API." + wsName);
			JSONParser parser = new JSONParser();
			String file = "";
			String wsSystemProperties = System.getenv(WS_ENV);
			if(StringUtils.isNotEmpty(wsSystemProperties)){
				file = wsSystemProperties;
			}
			else{
				ClassLoader cl = ResourceUtil.class.getClassLoader();
				file = cl.getResource(WS_API_FILE).getFile();
			}
			Object obj = parser.parse(new FileReader(file));
	        JSONObject jsonObject =  (JSONObject) obj;
	        if(jsonObject != null && StringUtils.isNotEmpty(wsName) && jsonObject.get(wsName) != null){
				LOG.info("API Found.");
	        	return (JSONObject) jsonObject.get(wsName);
	        }
	        else{
				LOG.error("API Not Found.");
	        	return null;
	        }
		} catch (FileNotFoundException e) {
			LOG.error("Fail to read properties file.", e);
	    } catch (IOException e) {
			LOG.error("Fail to read properties file.", e);
	    } catch (ParseException e) {
			LOG.error("Fail to parse properties file.", e);
	    }
		return null;
	}*/
	
//	private static String HKTV_IMAGE_URL 		   = "images.hktvmall.com";
	//private static String IMAGE_HKTV_IMG_COM_URL   = "images.hktv-img.com";
	
	public static String getHKTVImageURL(){
		return getSystemConfig().getProperty("IMAGES_DOMAIN");
	}
	
//	public static String getImageHKTVImgComURL(){
//		return IMAGE_HKTV_IMG_COM_URL;
//	}
	
	public static boolean existsImageDomain(String filePath){
		String s = getHKTVImageURL();
		boolean flag= false;
		if(StringUtils.isNotEmpty(s)){
			String[] strs = s.split(",");
			
			if(strs!=null&& strs .length>0){
				for(String imageDomain :strs){
					if(StringUtils.isNotEmpty(imageDomain) && StringUtils.isNotEmpty(filePath) &&
							filePath.indexOf(imageDomain) > -1){
						flag= true;
						break;
					}
				}
			}
			
		}
		
		return flag;
	}
	
	
	/*public static String getVersionNo(){
		String version = getSystemConfig().getProperty("version");
		if(StringUtils.isEmpty(version))
			version = "v11"+DateUtils.formatDate(new Date());
		return version;
	}*/
	
	
	public static String getUploadTmp(){
		return getSystemConfig().getProperty("batch_upload_product_temporary_path");
	}

	public static int getProductUploadMaxSize(){
		try{
		String maxSize= getSystemConfig().getProperty("product_upload_max_size");
		if(StringUtils.isEmpty(maxSize))
			return ConstantUtil.PRODUCT_UPLOAD_MAX_SIZE;
		else
			return Integer.valueOf(maxSize);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ConstantUtil.PRODUCT_UPLOAD_MAX_SIZE;
		}
		
	}
	
	
}
