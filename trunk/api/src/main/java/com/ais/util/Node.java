package com.ais.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class Node{

    //private BuProductCategoryEntity cont;
    private Node  parent;
    public List<Node> children;
    public List<Node> nodeList;    
    
    private String id;  
    private String pid;  
    private String name;
    private String active;
    
    private String productCatCode ;
    private String productCatName ;
    
   
   // public Map<Node> childrens;

	public String getProductCatCode() {
		return productCatCode;
	}

	public void setProductCatCode(String productCatCode) {
		this.productCatCode = productCatCode;
	}

	public String getProductCatName() {
		return productCatName;
	}

	public void setProductCatName(String productCatName) {
		this.productCatName = productCatName;
	}

	private int level ;
    
    private boolean expend;
   
    public Node(){
    	
    }
    
    public Node(String id,String pid,String name){
    	this.id = id;
    	this.pid = pid;
    	this.name =name;
    }
    
    /*public Node(BuProductCategoryEntity entity){
    	 this.id = String.valueOf(entity.getId());
    	 
    	 if(entity.getParentId()==null || entity.getParentId().intValue()==0)
    		 this.pid = null;
    	 else
    		 this.pid =String.valueOf(entity.getParentId());
    	 
    	 if(StringUtils.isNotEmpty(entity.getProductCatName()))
    		 this.name = entity.getProductCatCode() +" - " +entity.getProductCatName();
    	 else
    		 this.name = entity.getProductCatCode() ;
    	 
    	 
    	this.nodeList = new ArrayList<Node>();
    	this.cont = entity;
    	this.active = entity.getActiveInd();
    	this.expend = true;
    	
    	this.productCatCode = entity.getProductCatCode();
    	this.productCatName  = entity.getProductCatName();
    		
    }
    public BuProductCategoryEntity getCont() {
        return cont;
    }
    public void setCont(BuProductCategoryEntity cont) {
        this.cont = cont;
    }*/
    public Node getParent() {
        return parent;
    }
    public void setParent(Node parent) {
        this.parent = parent;
    }

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getId() {
		return id;
	}
	public String getPid() {
		return pid;
	}
	public List<Node> getNodeList() {
		return nodeList;
	}
	public void setNodeList(List<Node> nodeList) {
		this.nodeList = nodeList;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public boolean isExpend() {
		return expend;
	}

	public void setExpend(boolean expend) {
		this.expend = expend;
	}
	
	 public String getActive() {
			return active;
		}

		public void setActive(String active) {
			this.active = active;
		}
		
		
		public boolean equals(Object obj) {   
	        if (obj instanceof Node) {   
	        	Node u = (Node) obj;   
	            return this.id.equals(u.id) ;   
	        }   
	        return super.equals(obj);
	        
		}
		
}
