package com.ais.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	public static final Map<String, Integer> COL_MAP = new HashMap<String,Integer>(){
        {
        	put("A",0);
        	put("B",1);
        	put("C",2);
        	put("D",3);
        	put("E",4);
        	put("F",5);
        	put("G",6);
        	put("H",7);
        	put("I",8);
        	put("J",9);
        	put("K",10);
        	put("L",11);
        	put("M",12);
        	put("N",13);
        	put("O",14);
        	put("P",15);
        	put("Q",16);
        	put("R",17);
        	put("S",18);
        	put("T",19);
        	put("U",20);
        	put("V",21);
        	put("W",22);
        	put("X",23);
        	put("Y",24);
        	put("Z",25);
        	put("AA",26);
        	put("AB",27);
        	put("AC",28);
        	put("AD",29);
        	put("AE",30);
        	put("AF",31);
        	put("AG",32);
        	put("AH",33);
        	put("AI",34);
        	put("AJ",35);
        	put("AK",36);
        	put("AL",37);
        	put("AM",38);
        	put("AN",39);
        	put("AO",40);
        	put("AP",41);
        	put("AQ",42);
        	put("AR",43);
        	put("AS",44);
        	put("AT",45);
        	put("AU",46);
        	put("AV",47);
        	put("AW",48);
        	put("AX",49);
        	put("AY",50);
        	put("AZ",51);
        	put("BA",52);
        	put("BB",53);
        	put("BC",54);
        	put("BD",55);
        	put("BE",56);
        	put("BF",57);
        	put("BG",58);
        	put("BH",59);
        	put("BI",60);
        	put("BJ",61);
        	put("BK",62);
        	put("BL",63);
        	put("BM",64);
        	put("BN",65);
        	put("BO",66);
        	put("BP",67);
        	put("BQ",68);
        	put("BR",69);
        	put("BS",70);
        	put("BT",71);
        	put("BU",72);
        	put("BV",73);
        	put("BW",74);
        	put("BX",75);
        	put("BY",76);
        	put("BZ",77);
        	put("CA",78);
        	put("CB",79);
        	put("CC",80);
        	put("CD",81);
        	put("CE",82);
        	put("CF",83);
        	put("CG",84);
        	put("CH",85);
        	put("CI",86);
        	put("CJ",87);
        	put("CK",88);
        	put("CL",89);
        	put("CM",90);
        	put("CN",91);
        	put("CO",92);
        	put("CP",93);
        	put("CQ",94);
        	put("CR",95);
        	put("CS",96);
        	put("CT",97);
        	put("CU",98);
        	put("CV",99);
        	put("CW",100);
        	put("CX",101);
        	put("CY",102);
        	put("CZ",103);
        	put("DA",104);
        	put("DB",105);
        	put("DC",106);
        	put("DD",107);
        	put("DE",108);
        	put("DF",109);
        	put("DG",110);
        	put("DH",111);
        	put("DI",112);
        	put("DJ",113);
        	put("DK",114);
        	put("DL",115);
        	put("DM",116);
        	put("DN",117);
        	put("DO",118);
        	put("DP",119);
        	put("DQ",120);
        	put("DR",121);
        	put("DS",122);
        	put("DT",123);
        	put("DU",124);
        	put("DV",125);
        	put("DW",126);
        	put("DX",127);
        	put("DY",128);
        	put("DZ",129);

        }
    };
    
    public static String FORMULA = "formula";
    public static String TEXT = "text";
    public static String NUMBER = "number";


    //for performance issues, we dont create style here, create style spend a lot of time
    public static void setCellValue(XSSFSheet sheet, int colNum, int rowNum, Object value, String dataType, XSSFCellStyle style){
    	if(value != null){
			rowNum = rowNum - 1; //rowNum = 1 = first Row, first Row is 0 in sheet object
			XSSFRow row = getRow(sheet, rowNum);
			Cell cell = getCell(row, colNum++);
			if(FORMULA.equals(dataType)){
		        cell.setCellType(CellType.FORMULA);
		        cell.setCellFormula(String.valueOf(value));
			}
			else if(NUMBER.equals(dataType)){
				cell.setCellType(CellType.NUMERIC);
				cell.setCellValue(Double.valueOf(String.valueOf(value)));
			}
			else{
		        cell.setCellType(CellType.STRING);
				cell.setCellValue("null".equals(String.valueOf(value))?"":String.valueOf(value));
			}
			if(style != null)
				cell.setCellStyle(style);
		}
    }
	public static void setCellValue(XSSFSheet sheet, String colName, int rowNum, Object value, String dataType, XSSFCellStyle style){
		setCellValue(sheet, ExcelUtil.COL_MAP.get(colName), rowNum, value, dataType, style);
	}
	
	public static XSSFRow getRow(XSSFSheet sheet, int rowNum) {
		return  (XSSFRow) (sheet.getRow(rowNum) == null ? sheet.createRow(rowNum) : sheet.getRow(rowNum));
	}
	
	public static Cell getCell(XSSFRow row, String cellIdx) {
		return  (Cell) getCell(row,COL_MAP.get(cellIdx));
	}
	
	public static Cell getCell(XSSFRow row, int cellIdx) {
		return (row.getCell(cellIdx) == null ? row.createCell(cellIdx) : row.getCell(cellIdx));
	}
}
