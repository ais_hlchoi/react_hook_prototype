/**
 *
 */
package com.ais.util;

/**
 * Define all constant for whole application
 *
 * @author AIS
 */
public class ConstantUtil {

	private ConstantUtil() {
	  
	}
	 
    //contant yes or no
    public static final String CONTANT_YES = "Y";
    public static final String CONTANT_NO = "N";

    //product status
    public static final String PRODUCT_STATUS_APPROVED = "APPROVED";

    // error  type
    public static final String ERROR_TYPE_DANGER = "danger";
    public static final String ERROR_TYPE_SUCCESS = "success";
    public static final String ERRORMESSAGE_SUCCESS = "Saved_Successfully";



    public static final String FAIL="FAIL";
    public static final String READY="READY";
    public static final String PROCESSING="PROCESSING";
    // version
    public static final String VERSION_STAGING="STAGING";
    public static final String VERSION_ONLINE="ONLINE";
    

    //user for _BARCODE or IMAGE
    //PRODUCT STATUS APPROVED_IN_RETEK--A UNAPPROVED_IN_RETEK--F
    public static final String PRODUCT_STATUS_APPROVED_IN_RETEK="APPROVED_IN_RETEK";
    public static final String PRODUCT_STATUS_UNAPPROVED_IN_RETEK="UNAPPROVED_IN_RETEK";
    //A Accepted; F Failed; P Processing
    public static final String PRODUCT_STATUS_RETEK_A="A";
    public static final String PRODUCT_STATUS_RETEK_F="F";
    public static final String PRODUCT_STATUS_RETEK_P="P";

    public static final String PRODUCT_IMAGE_TYPE="PRODUCT_IMAGE";
    //swatch 
    public static final String SWATCH_IMAGE_TYPE="SWATCH_IMAGE";
    public static final String JOB_USER_NAME="JOB";

    public static final String LOGIN_FAILURE_TYPE="LOGIN_FAILURE_TYPE";
    public static final String LOGIN_FAILURE_TYPE_DISABLED="DISABLED";
    public static final String LOGIN_FAILURE_TYPE_BADCREDENTIALS="BADCREDENTIALS";
    
    public static final String PROCESS    = "process";
    public static final String ARCHIVE    = "archive";
    public static final String ERROR    = "error";
    public static final String SUPPLIER_DIRECT_DELIVERY    = "SUPPLIER_DIRECT_DELIVERY";
    public static final String CONSIGNMENT_VIA_WAREHOUSE    = "CONSIGNMENT_VIA_WAREHOUSE";
    public static final String CONSIGNMENT   = "CONSIGNMENT";
    
    public static final String LOGIN_FAILURE_NAME="LOGIN_FAILURE_NAME";
    public static final String LOGIN_LANGUAGE="LOGIN_LANGUAGE";
    
    public static final String LOGIN_CAPTCHA="loginCaptcha";

    public static final String SYSTEM_SUPPLIER_LIST = "SYSTEM_SUPPLIER_LIST";
    public static final String SYSTEM_SUPPLIER_MAP = "SYSTEM_SUPPLIER_MAP";

    public static final String SYSTEM_SHOP_LIST = "SYSTEM_SHOP_LIST";
    public static final String SYSTEM_SHOP_MAP = "SYSTEM_SHOP_MAP";

    //OPP
    public static final String DRAFT = "D";
    public static final String SUBMITTED= "S";
    public static final String REJECT = "R";
    public static final String APPROVED = "A";
    public static final String ACKNOWLEDGE = "AC";
    public static final String COMPLETED = "C";
    public static final String NEW = "N";
    public static final String CANCEL = "CL";

    //role
    public static final String MERCHANT_ADMIN = "MERCHANT_ADMIN";
    public static final String MERCHANT = "MERCHANT";
    public static final String RMO = "RMO";
    public static final String RML = "RML";
    public static final String RM = "RM";
    public static final String EDITORIAL = "EDITORIAL";
    public static final String PM= "PM";
    public static final String MR = "MR";
    public static final String DEPT_HEAD = "DEPT_HEAD";
    public static final String ADMIN = "ADMIN";
    

    public static final String WORKFLOW_TYPE_OPP = "OPP";
    public static final String WORKFLOW_TYPE_PRODUCT = "PRODUCT";
    public static final String WORKFLOW_PRODUCT_APPROVAL = "Product Approval";

    /*
        Product ��
     */
    public static final String HKTV="HKTV";
    public static final String HKB="HKB";
    public static final String Y="Y";
    public static final String N="N";
    public static final String PRODUCT_CATEGORY_MMS = "MMS";
    public static final String PRODUCT_CATEGORY_STORE = "STORE";
    public static final String BATCH_UPLOAD_HKTV_CATEGORYIDS = "HKTVCATEGORYIDS";
    public static final String BATCH_UPLOAD_HKB_CATEGORYIDS = "HKBCATEGORYIDS";
    public static final String BATCH_UPLOAD_HKTV_PRIMARY_CATEGORYIDS = "HKTVPRIMARYCATEGORYIDS";
    public static final String BATCH_UPLOAD_HKB_PRIMARY_CATEGORYIDS = "HKBPRIMARYCATEGORYIDS";
    public static final String BATCH_UPLOAD_BRANDS= "BATCHUPLOADBRANDS";
    public static final String BATCH_UPLOAD_MANUCOUNTRIES= "BATCHUPLOADMANUCOUNTRIES";
    public static final int PRODUCT_UPLOAD_MAX_SIZE = 100000;
    
    //group
    public static final String GROUP_CATEGORY_TREE_PROUICT_TERMS_= "GROUP_CATEGORY_TREE_PROUICT_TERMS_" ;
    public static final String GROUP_TREE_LIST_FOR_STORE= "GROUP_TREE_LIST_FOR_STORE" ; 
    public static final String GROUP_TREE_LIST_FOR_PRODUCT       = "GROUP_TREE_LIST_FOR_PRODUCT" ;
    public static final String GROUP_BU_CATEGORY_LIST      = "GROUP_BU_CATEGORY_LIST" ;
    public static final String GROUP_CATEGORY_LIST      = "GROUP_CATEGORY_LIST" ;
    
    public static final String ADD ="ADD";
    public static final String EDIT="EDIT";

    public static int SEC_24_HOURS = 86400 ;
    public static int SEC_8_HOURS = 28800 ;
    public static int SEC_4_HOURS = 14400 ;
    public static int SEC_12_HOURS = 43200 ;

    public static int SEC_30_MINTUES= 1800 ;
    public static int MAX_LENGTH_CELL= 32767 ;
    
    public static final String GROUP_LOCK_PRODUCT_CODE= "GROUP_LOCK_PRODUCT_CODE" ;
    public static final String GROUP_PRODUCT_RUN_TIME= "GROUP_PRODUCT_RUN_TIME" ;

    
    
    //job
    public static final String MANAGE_SUPP_JOB = "MANAGE_SUPPLEMENTARY_JOB";
	public static final String MANAGE_RE_ASSIGNMENT = "MANAGE_RE_ASSIGNMENT";
	public static final String MESSAGE_SEND_JOB = "MESSAGE_SEND_JOB";
	public static final String MANAGE_RENEW_CONTRACT_JOB = "MANAGE_RENEW_CONTRACT_JOB";
	public static final String DAILY_ORDER_STATUS_REPORT_JOB = "DAILY_ORDER_STATUS_REPORT_JOB";
	public static final String COMMAND = "COMMAND";
	
	
	public static final String JOB_START = "S";
	public static final String JOB_FAIL = "F";
	public static final String JOB_COMPLETED = "C";
	public static final String JOB_SKIP = "SKIP";
	public static final String IMPORT_ORDER_JOB = "ImportOrderJob";   
	public static final String UPDATE_ORDER_JOB = "UpdateOrderJob";   
	public static final String PCR_JOB = "PCR_JOB";   
	public static final String PAYMENT_REGISTER_JOB = "PaymentRegisterJob";   
	public static final String PROMTIONAL_PRODUCT_JOB = "PromtionalProductJob";   
	public static final String MANAGE_COMMAND_JOB = "ManageCommandJob";   
	public static final String MANAGE_GENERATE_CONTACT_FOR_SALESFORCE = "GenerateContactForSalesforces";  
	public static final String MANAGE_GENERATE_REPORT_WITH_CUSTOM_PARAMETER = "ManageGenerateReportWithCustomParameter";   
	public static final String MANAGE_COMMAND  = "ManageCommand";   

	public static final String MERCHANT_BANK_STATUS_NEW  = "New";
	public static final String MERCHANT_BANK_STATUS_APPROVED  = "A";
	public static final String MERCHANT_BANK_STATUS_REJECTED  = "R";

	//additional offer
    public static final String ERROR_DATA_TYPE_BATCH = "BATCH";
    public static final String ERROR_DATA_TYPE_RECORD = "RECORD";

    public static final String SYNC_ACTION_DELETE = "DELETE";
    public static final String SYNC_ACTION_UPDATE = "UPDATE";
}
