package com.ais.util;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class PasswordHash {

	/**
	 * @param password
	 * @param saltValue
	 * @return passwordAfterHash
	 * */
	public static String getHashvalue(String password, String saltValue) {
		password = AesEncryptUtil.encrypt(password);
		String algorithmName = "md5";
		int hashIterations = 2;
		String passwordAfterHash = new SimpleHash(algorithmName, password, ByteSource.Util.bytes(saltValue),
				hashIterations).toHex();
		return passwordAfterHash;
	}

	/**
	 * @return saltvalue
	 * */
	public static String getSaltValue(String password) {
		return AesEncryptUtil.encrypt(password);
	}
}
