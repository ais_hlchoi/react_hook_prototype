package com.ais.util;

import java.util.Date;
import java.util.Random;

public class CreateIdUtil {

	/**
	 * @param prefix	
	 * @return id		
	 * @author HY
	 */
	public static String CreateId(String prefix){
		if(prefix == null || "".equals(prefix)) {
			return null;
		}
		String id = prefix+"_";
		
		id += new Date().getTime();
				
		id += randomGeneration();
		
		return id;
	}
	
	/**
	 * @return String
	 * @author HY
	 */
	public static String randomGeneration(){
		String str = "";  
		Random rand = new Random();  
		for(int i=0;i<4;i++){  
			int num = rand.nextInt(3);  
			switch(num){  
			case 0:  
				char c1 = (char)(rand.nextInt(26)+'a');   
				str += c1;  
				break;  
			case 1:  
				char c2 = (char)(rand.nextInt(26)+'A');   
				str += c2;  
				break;  
			case 2:  
				str += rand.nextInt(10);
			}  
		}
		return str;  
	}
	
	
	/**
	 * @return coe
	 * @author ZF
	 */
	public static String CreateMarketVoucherCode(){
		String randomStr = String.valueOf(Math.random());
        randomStr = randomStr.substring(4, 7);
        
		String dateStr =String.valueOf(new Date().getTime());
	
		String code = dateStr;
		
		code += randomStr;
		
		code += randomGeneration();
		
		return code;
	}
	
	
}
