package com.ais.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);
		
	private static FileUtil fileUtil;
	
	@Autowired
    private HttpServletResponse response;
	
	
    @PostConstruct  
    public void init() {  
    	fileUtil = this;
    	fileUtil.response = this.response;
    	
    }

    /**
     * Excel Export
     * 
     * @param workbook
     * @param fileName
     */
	public static void downloadExcelFile(XSSFWorkbook workbook, String fileName){
		if(workbook != null){
			ServletOutputStream out = null;
			try {
				String suffix = new SimpleDateFormat("_yyyyMMdd_HHmmss_SSS").format(new Date()) + ".xlsx";
				fileUtil.response.setContentType("multipart/form-data");
				fileUtil.response.setHeader("Content-Disposition", "attachment; filename=" + fileName + suffix);
				fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
				fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
				out = fileUtil.response.getOutputStream();
				workbook.write(out);
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * Excel Export
	 * 
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadExcelFile(SXSSFWorkbook workbook, String fileName){
		if(workbook != null){
			ServletOutputStream out = null;
			try {
				String suffix = new SimpleDateFormat("_yyyyMMdd_HHmmss_SSS").format(new Date()) + ".xlsx";
				fileUtil.response.setContentType("multipart/form-data");
				fileUtil.response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName,"UTF-8") + suffix);
				fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
				fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
				out = fileUtil.response.getOutputStream();
				workbook.write(out);
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * Excel Export
	 * 
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadExcelFileUTF8(XSSFWorkbook workbook, String fileName){
		if(workbook != null){
			ServletOutputStream out = null;
			try {
				fileUtil.response.setContentType("multipart/form-data");
				fileUtil.response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(fileName,"UTF-8"));
				fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
				fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
				out = fileUtil.response.getOutputStream();
				workbook.write(out);
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
	public static void downloadExcelFileUTF8(File f, String fileName, String contentType){
		if(f != null){
			ServletOutputStream out = null;
			try {
				fileUtil.response.setContentType(contentType);
				fileUtil.response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(fileName,"UTF-8"));
				fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
				fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
				out = fileUtil.response.getOutputStream();
				out.write(Files.readAllBytes(f.toPath()));
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * pdf导出
	 * 
	 * @param bytes
	 * @param fileName
	 */
	public static void downloadPdfFile(byte[] bytes, String fileName){
		if (bytes != null && bytes.length > 0) {
			ServletOutputStream out = null;
			try {
				fileUtil.response.setContentType("application/pdf;charset=UTF-8");
				fileUtil.response.setHeader("Content-Disposition", "attachment;filename*=UTF-8''" + URLEncoder.encode(fileName,"UTF-8"));
				fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
				out = fileUtil.response.getOutputStream();
				out.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * xlsx模板导出
	 * XSSFWorkbook:是操作Excel2007的版本，扩展名是.xlsx
	 * 
	 * @param wb
	 * @param fileName
	 */
	public static void downloadXlsxFile(XSSFWorkbook wb, String fileName) {
		OutputStream out = null;
		try {
			if (wb == null) {
				return;
			}
			fileUtil.response.setContentType("application/octet-stream");
			fileUtil.response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''"+ URLEncoder.encode(fileName,"UTF-8"));
			fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
			out = fileUtil.response.getOutputStream();
			wb.write(out);
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage(), e);
		} finally {
			try {
				if (out != null) {
					out.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * xls模板导出
	 * HSSFWorkbook:是操作Excel2003以前（包括2003）的版本，扩展名是.xls
	 * 
	 * @param wb
	 * @param fileName
	 */
	public static void downloadXlsFile(HSSFWorkbook wb, String fileName) {
		OutputStream out = null;
		try {
			if (wb == null) {
				return;
			}
			fileUtil.response.setContentType("application/octet-stream");
			fileUtil.response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(fileName,"UTF-8"));
			fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
			fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
			out = fileUtil.response.getOutputStream();
			wb.write(out);
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage(), e);
		} finally {
			try {
				if (out != null) {
					out.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
		}
	}
		
	public static void downloadZipFile(File file, String fileName) {
		BufferedOutputStream out=null;
		try {
			if (file == null) {
				return;
			}
			fileUtil.response.setContentType("multipart/form-data");
			fileUtil.response.setHeader("Content-Disposition", "attachment;filename*=UTF-8''" + URLEncoder.encode(fileName,"UTF-8"));
			fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
			ServletOutputStream sos = fileUtil.response.getOutputStream();
			FileInputStream in = new FileInputStream(file);
			out = new BufferedOutputStream(sos);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				out.write(b, 0, i);
			}
			out.flush();
			sos.close();
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage(), e);
		} finally {
			try {
				if (out != null) {
					out.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
		}
	}

    /**
     * 请求外部接口得到的文件流生成Excel
     * 
     * @param bin
     * @param fileName
     */
	public static void downloadExcelFile(BufferedInputStream bin, String fileName){
		ServletOutputStream out = null;
		try {
			fileUtil.response.setContentType("multipart/form-data");
			fileUtil.response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName,"UTF-8"));
			fileUtil.response.addHeader("Access-Control-Allow-Headers", "Content-Disposition");
			fileUtil.response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
			out = fileUtil.response.getOutputStream();
			int size = 0;
			byte[] buf = new byte[1024];
			while ((size = bin.read(buf)) != -1) {
			    out.write(buf, 0, size);
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage(), e);
		} finally {
			try {
				if (out != null){
					out.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
			try {
				if (out != null){
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
			try {
				if (bin != null){
					bin.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * 生成文件到指定路径
	 * 
	 * @param bytes
	 * @param filePath
	 */
	public static void downloadPdfFileSpecifiedPath(byte[] bytes, String filePath){
		if (bytes != null && bytes.length > 0) {
			FileOutputStream out = null;
			try {
				File file = new File(filePath);
				if(!file.exists()){
					file.createNewFile();
				}
				out = new FileOutputStream(file);
				out.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			} finally {
				try {
					if (out != null){
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}
	
}
