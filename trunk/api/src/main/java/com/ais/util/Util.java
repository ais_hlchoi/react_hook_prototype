package com.ais.util;

import java.net.CookieHandler;
import java.net.CookieManager;

public class Util {
	public void setCookies() throws Exception {
		/* The cookie store is initiated once, before the first request */
		CookieManager cookieManager = new CookieManager();
		CookieHandler.setDefault(cookieManager);
	}
}
