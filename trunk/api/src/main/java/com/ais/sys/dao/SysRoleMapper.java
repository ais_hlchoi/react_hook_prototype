package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysRoleModel;

public interface SysRoleMapper {

	List<SysRoleModel> search(SysRoleModel sysRoleModel);

	Integer insert(SysRoleModel sysRoleModel);

	Integer update(SysRoleModel sysRoleModel);

	Integer delete(SysRoleModel sysRoleModel);

	Integer onlyDispSeq(SysRoleModel sysRoleModel);

	Integer onlyRoleCode(SysRoleModel sysRoleModel);
}
