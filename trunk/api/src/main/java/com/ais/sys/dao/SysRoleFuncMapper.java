package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysRoleFuncModel;
import com.ais.sys.model.SysUserModel;

public interface SysRoleFuncMapper {

	Integer insertAll(SysRoleFuncModel sysRoleFuncModel);

	Integer updateAll(SysRoleFuncModel sysRoleFuncModel);

	Integer update(SysRoleFuncModel sysRoleFuncModel);

	Integer deleteBySysRoleId(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchRoleFunc(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchAllFunc(SysRoleFuncModel sysRoleFuncModel);
	
	List<SysRoleFuncModel> searchRoleFuncByUserCode(SysUserModel model);

	List<SysRoleFuncModel> searchFunc(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchUserPower(SysRoleFuncModel sysRoleFuncModel);

	Integer searchByFuncId(Integer funcId);

	Integer deleteByFuncId(Integer funcId);
}
