package com.ais.sys.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.OperationLogManageModel;
import com.ais.sys.service.OperationLogManageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/back/operationLogManage")
public class OperationLogManageController {

	@Autowired
	private OperationLogManageService operationLogManageService;
	
	@RequestMapping("/search")
	public Object search(@RequestBody OperationLogManageModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<OperationLogManageModel> list = operationLogManageService.search(model);
		return RestEntity.success(new PageInfo<>(list));
	}
	
	@RequestMapping("/getLoginActivity")
	public Object getLoginActivity(HttpServletRequest request, HttpServletResponse res) {
		OperationLogManageModel model = new OperationLogManageModel();
		List<OperationLogManageModel> list = operationLogManageService.getLoginActivity(model);
		return RestEntity.success(list);
	}

}
