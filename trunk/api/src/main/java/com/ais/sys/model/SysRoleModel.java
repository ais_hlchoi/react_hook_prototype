package com.ais.sys.model;

import java.util.List;

import com.ais.framework.model.CommonParentModel;

public class SysRoleModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private Integer roleId;

	private String roleCode;

	private String roleName;

	private String remark;

	private Integer dispSeq;

	private String activeInd;

	private List<SysRoleFuncModel> funcList;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDispSeq() {
		return dispSeq;
	}

	public void setDispSeq(Integer dispSeq) {
		this.dispSeq = dispSeq;
	}

	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}

	public List<SysRoleFuncModel> getFuncList() {
		return funcList;
	}

	public void setFuncList(List<SysRoleFuncModel> funcList) {
		this.funcList = funcList;
	}

	public String opeartionContent() {
		return "{roleId:" + roleId + "; roleCode:" + roleCode + "; roleName:" + roleName + "; remark:"
				+ remark + "; dispSeq:" + dispSeq + "; activeInd:" + activeInd + "; funcList:" + funcList + "}";
	}

}
