package com.ais.sys.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.OperationLogMapper;
import com.ais.sys.model.OperationLogModel;
import com.ais.sys.service.OperationLogService;

@Service
public class OperationLogServiceImpl extends CommonParent implements OperationLogService {

	@Autowired
	private OperationLogMapper operationLogMapper;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insert(OperationLogModel model) throws SysException {
		if(StringUtils.isBlank(model.getCreatedBy())) {
			model.setCreatedBy(getUserId());
		}
		int result = operationLogMapper.insert(model);
		if(result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
	}

}
