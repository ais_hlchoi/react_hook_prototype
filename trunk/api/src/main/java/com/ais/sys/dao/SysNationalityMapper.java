package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysNationalityModel;

public interface SysNationalityMapper {

	List<SysNationalityModel> getNationalityDownList();

}