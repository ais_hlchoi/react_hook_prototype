package com.ais.sys.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysFuncModel;
import com.ais.sys.service.SysFuncService;
import com.ais.sys.service.SysRoleService;
import com.ais.sys.service.SysUserPreferenceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/sysFunc")
public class SysFuncController extends CommonParent {

	@Autowired
	private SysFuncService sysFuncService;
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysUserPreferenceService userPreferenceService;

	/**
	 * Search Function
	 * @param sysFuncModel
	 * @return
	 */
	@RequestMapping("/search")
	public Object search(@RequestBody SysFuncModel sysFuncModel) {
		PageHelper.startPage(sysFuncModel.getPage(), sysFuncModel.getPageSize());
		List<SysFuncModel> list = sysFuncService.search(sysFuncModel);
		return RestEntity.success(new PageInfo<>(list));
	}
	
	@RequestMapping("/searchAll")
	private Object searchAll(@RequestBody SysFuncModel sysFuncModel) {
		List<SysFuncModel> list = sysFuncService.search(sysFuncModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/leftMenuSearch")
	public Object leftMenuSearch(@RequestBody SysFuncModel sysFuncModel) {
		List<SysFuncModel> list = sysFuncService.leftMenuSearch(sysFuncModel);
		
		//查询我的最爱
		List<SysFuncModel> preferenceMenuList = sysFuncService.searchPreferenceMenu(sysFuncModel);
		if(CollectionUtils.isNotEmpty(preferenceMenuList)) {
			list.addAll(preferenceMenuList);
		}
		
		return RestEntity.success(list);
	}
	
	/**
	 *  Search Function by ID
	 * 
	 * @param funcId
	 * @return
	 */
	@RequestMapping("/searchByFuncId")
	public Object searchByFuncId(@RequestBody Integer funcId) {
		Map<String, Object> map=new HashMap<String, Object>();
		Integer roleCount = sysRoleService.searchByFuncId(funcId);
		Integer userPreferenceCount = userPreferenceService.searchByFuncId(funcId);
		map.put("roleCount", roleCount);
		map.put("userPreferenceCount", userPreferenceCount);
		return RestEntity.success(map);
	}

	/**
	 * New Function
	 * @param sysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/insert")
	public Object insert(@RequestBody SysFuncModel sysFuncModel) throws SysException {
		sysFuncService.insert(sysFuncModel);
		return RestEntity.success();
	}

	/**
	 * Update Function
	 * @param sysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/update")
	public Object update(@RequestBody SysFuncModel sysFuncModel) throws SysException {
		sysFuncService.update(sysFuncModel);
		return RestEntity.success();
	}

	/**
	 * Delete Function
	 * @param sysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/delete")
	public Object delete(@RequestBody SysFuncModel sysFuncModel) throws SysException {
		if (sysFuncModel.getFuncId() == null) {
			return RestEntity.failed("ID_CANNOT_BE_EMPTY");
		}
		sysFuncService.delete(sysFuncModel);
		return RestEntity.success();
	}

	/**
	 * Search Function
	 * @param sysFuncModel
	 * @return
	 */
	@RequestMapping("/searchFunc")
	public Object searchFunc(@RequestBody SysFuncModel sysFuncModel) {
		List<SysFuncModel> list = sysFuncService.searchFunc(sysFuncModel);
		return RestEntity.success(list);
	}
}
