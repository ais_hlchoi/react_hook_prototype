package com.ais.sys.model;

import java.util.Date;

import com.ais.framework.model.CommonParentModel;

public class SysUserPwdHistModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private Integer userPwdHistId;
	
	private String pwd;
	
	private String salt;
	
	private Date pwdDate;
	
	private Integer times;

	public Integer getUserPwdHistId() {
		return userPwdHistId;
	}

	public void setUserPwdHistId(Integer userPwdHistId) {
		this.userPwdHistId = userPwdHistId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Date getPwdDate() {
		return pwdDate;
	}

	public void setPwdDate(Date pwdDate) {
		this.pwdDate = pwdDate;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

}
