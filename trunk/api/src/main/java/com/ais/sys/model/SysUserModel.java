package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SysUserModel extends CommonParentModel {

    private static final long serialVersionUID = 1L;

    private String userCode;

    private String userName;

    private String companyCode;

    private String companyName;

    private String pwd;

    private String salt;

    private String email;

    private String sex;

    private String activeInd;

    private String position;

    private String accPwGroup;

    private String desc;

    private String fax;

    private String address;

    private String phone;

    private String postalCode;

    private String qq;

    private String homePhone;

    private String officePhone;

    private String name;

    private String middleName;

    private String surname;

    private String countryCode;

    private String maritalStatus;

    private String birthPlace;

    private String birthday;

    private String token;
    
    private Integer roleId;

    private String defaultInd;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAccPwGroup() {
        return accPwGroup;
    }

    public void setAccPwGroup(String accPwGroup) {
        this.accPwGroup = accPwGroup;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getDefaultInd() {
		return defaultInd;
	}

	public void setDefaultInd(String defaultInd) {
		this.defaultInd = defaultInd;
	}

	public String opeartionContent() {
		return "{userCode:" + userCode + "; userName:" + userName + "; companyCode:" + companyCode
				+ "; companyName:" + companyName + "; pwd:" + pwd + "; salt:" + salt + "; email:" + email + "; sex:"
				+ sex + "; activeInd:" + activeInd + "; position:" + position + "; accPwGroup:" + accPwGroup + "; desc:"
				+ desc + "; fax:" + fax + "; address:" + address + "; phone:" + phone + "; postalCode:" + postalCode
				+ "; qq:" + qq + "; homePhone:" + homePhone + "; officePhone:" + officePhone + "; name:" + name
				+ "; middleName:" + middleName + "; surname:" + surname + "; countryCode:" + countryCode
				+ "; maritalStatus:" + maritalStatus + "; birthPlace:" + birthPlace + "; birthday:" + birthday
				+ "; token:" + token + "; roleId:" + roleId + "; defaultInd:" + defaultInd + "}";
	}
    
}