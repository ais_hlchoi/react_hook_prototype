package com.ais.sys.service;

import java.util.List;

import com.ais.sys.model.OperationLogManageModel;

public interface OperationLogManageService {
	
	List<OperationLogManageModel> search(OperationLogManageModel model);
	
	List<OperationLogManageModel> getLoginActivity(OperationLogManageModel model);
	
	int insert(OperationLogManageModel model);
	
	int update(OperationLogManageModel model);
}
