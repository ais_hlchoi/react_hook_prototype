package com.ais.sys.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.SysCompanyMapper;
import com.ais.sys.dao.SysUserMapper;
import com.ais.sys.dao.SysUserRoleMapper;
import com.ais.sys.model.SysCompanyModel;
import com.ais.sys.service.SysCompanyService;

/**
 */
@Service
public class SysCompanyServiceImpl extends CommonParent implements SysCompanyService {

	@Autowired
	private SysCompanyMapper sysCompanyMapper;

	@Autowired
	private SysUserMapper userMapper;

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	
	@Override
	public List<SysCompanyModel> search(SysCompanyModel sysCompanyModel) {
		return sysCompanyMapper.search(sysCompanyModel);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insert(SysCompanyModel sysCompanyModel) throws SysException {
		String userId = getUserId();
		sysCompanyModel.setCreatedBy(userId);
		sysCompanyModel.setLastUpdatedBy(userId);
		int result = sysCompanyMapper.insert(sysCompanyModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysCompanyModel sysCompanyModel) throws SysException {
		String userId = getUserId();
		sysCompanyModel.setLastUpdatedBy(userId);
		SysCompanyModel user = sysCompanyMapper.getLastUpdteDteById(sysCompanyModel.getCompanyCode());
		if(user ==null || !sysCompanyModel.getLastUpdatedDate().equals(user.getLastUpdatedDate())) {
			throw new SysException("DATA_UPDATE_TIMEOUT");
		}
		int result = sysCompanyMapper.update(sysCompanyModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(SysCompanyModel sysCompanyModel) throws SysException {
		int count = 0;
		if(StringUtils.isNotBlank(sysCompanyModel.getCompanyCode())) {
			count = userMapper.getCountByCompanyCode(sysCompanyModel.getCompanyCode());
			if(count ==0) {
				count = sysUserRoleMapper.getCountByCompanyCode(sysCompanyModel.getCompanyCode());
			}
			if(count>0) {
				throw new SysException("COMPANYCODE_IS_USED");
			}
		}
		int result = sysCompanyMapper.delete(sysCompanyModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_DELETE");
		}
	}

	@Override
	public List<SysCompanyModel> getCompanyDownList(SysCompanyModel sysCompanyModel) {
		return sysCompanyMapper.getCompanyDownList(sysCompanyModel);
	}

	@Override
	public Integer checkExistCount(SysCompanyModel sysCompanyModel) {
		return sysCompanyMapper.checkExistCount(sysCompanyModel);
	}

}
