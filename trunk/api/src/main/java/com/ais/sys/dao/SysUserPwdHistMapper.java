package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysUserModel;
import com.ais.sys.model.SysUserPwdHistModel;

public interface SysUserPwdHistMapper {

    int insert(SysUserPwdHistModel sysUserPwdHistModel);

	int deletePwdHistByUserId(SysUserModel userModel);

	List<SysUserPwdHistModel> searchFiveLimit(SysUserPwdHistModel sysUserPwdHistModel);

}