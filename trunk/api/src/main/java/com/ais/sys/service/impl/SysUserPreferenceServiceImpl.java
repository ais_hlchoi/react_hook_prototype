package com.ais.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.SysUserPreferenceMapper;
import com.ais.sys.model.SysUserPreferenceModel;
import com.ais.sys.service.SysUserPreferenceService;
import com.ais.util.CreateIdUtil;

@Service
public class SysUserPreferenceServiceImpl extends CommonParent implements SysUserPreferenceService {

	@Autowired
	private SysUserPreferenceMapper userPreferenceMapper;

	@Override
	public List<SysUserPreferenceModel> searchUserAllFunc(SysUserPreferenceModel userPreferenceModel) {
		return userPreferenceMapper.searchUserAllFunc(userPreferenceModel);
	}

	@Override
	public List<SysUserPreferenceModel> search(SysUserPreferenceModel userPreferenceModel) {
		return userPreferenceMapper.search(userPreferenceModel);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void setUp(SysUserPreferenceModel userPreferenceModel) {
		if (userPreferenceModel.getUserId() != null) {
			userPreferenceMapper.deletaAllPreference(userPreferenceModel);
			List<SysUserPreferenceModel> list = userPreferenceModel.getFuncList();
			if (list != null && list.size() > 0) {
				userPreferenceModel.setLastUpdatedBy(getUserId());
				userPreferenceModel.setCreatedBy(userPreferenceModel.getLastUpdatedBy());
				Integer seq = 1;
				for (SysUserPreferenceModel item : list) {
					item.setPreferenceId(CreateIdUtil.CreateId("PF"));
					item.setDispSeq(seq);
					seq++;
				}
				userPreferenceModel.setFuncList(list);
				userPreferenceMapper.insertAll(userPreferenceModel);
			}
		}

	}

	@Override
	public Integer searchByFuncId(Integer funcId) {
		return userPreferenceMapper.searchByFuncId(funcId);
	}

}
