package com.ais.sys.service;

import java.util.List;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.SysCompanyModel;

public interface SysCompanyService {
	
	List<SysCompanyModel> search(SysCompanyModel SysCompanyModel);

	void insert(SysCompanyModel SysCompanyModel) throws SysException;

	void update(SysCompanyModel SysCompanyModel) throws SysException;

	void delete(SysCompanyModel SysCompanyModel) throws SysException;

	List<SysCompanyModel> getCompanyDownList(SysCompanyModel sysCompanyModel);

	Integer checkExistCount(SysCompanyModel sysCompanyModel);
}
