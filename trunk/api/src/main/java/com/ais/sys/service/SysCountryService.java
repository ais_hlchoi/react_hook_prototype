package com.ais.sys.service;

import java.util.List;

import com.ais.sys.model.SysCountryModel;

public interface SysCountryService {

	List<SysCountryModel> getCountryDownList();

}
