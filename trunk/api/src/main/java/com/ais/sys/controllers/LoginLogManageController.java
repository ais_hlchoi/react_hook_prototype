package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.LoginLogManageModel;
import com.ais.sys.service.LoginLogManageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/back/loginLogManage")
public class LoginLogManageController {
	
	@Autowired
	private LoginLogManageService loginLogManageService;
	
	@RequestMapping("/search")
	public Object search(@RequestBody LoginLogManageModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<LoginLogManageModel> list = loginLogManageService.search(model);
		return RestEntity.success(new PageInfo<>(list));
	}

}
