package com.ais.sys.dao;

import com.ais.sys.model.OperationLogModel;

public interface OperationLogMapper {
	
	int insert(OperationLogModel model);

}
