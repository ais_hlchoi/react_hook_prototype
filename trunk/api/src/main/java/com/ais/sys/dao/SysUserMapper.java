package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysUserModel;

public interface SysUserMapper {

	List<SysUserModel> search(SysUserModel model);

	SysUserModel searchByUserId(Integer userId);

	List<SysUserModel> searchUserRoleByUserCode(String userCode);

	int insert(SysUserModel model);

	int update(SysUserModel model);

	int delete(SysUserModel model);

	int deleteAllByUserId(SysUserModel model);

	int updatePwd(SysUserModel userModel);

	Integer getCountByPhone(SysUserModel userModel);

	Integer getCountByEmail(SysUserModel userModel);

	Integer getCountByName(SysUserModel userModel);

	Integer getCountByCode(SysUserModel userModel);

	SysUserModel getLastUpdteDteById(Integer userId);

	Integer getCountByCompanyCode(String companyCode);

}