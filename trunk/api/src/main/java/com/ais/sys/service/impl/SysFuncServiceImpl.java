package com.ais.sys.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.constant.Constants;
import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.logAspect.annotation.OperationLog;
import com.ais.sys.dao.SysFuncMapper;
import com.ais.sys.dao.SysRoleFuncMapper;
import com.ais.sys.dao.SysUserPreferenceMapper;
import com.ais.sys.model.SysFuncModel;
import com.ais.sys.service.SysFuncService;

@Service
public class SysFuncServiceImpl extends CommonParent implements SysFuncService {

	@Autowired
	private SysFuncMapper sysFuncMapper;
	
	@Autowired
	private SysRoleFuncMapper sysRoleFuncMapper;
	
	@Autowired
	private SysUserPreferenceMapper userPreferenceMapper;

	@Override
	public List<SysFuncModel> search(SysFuncModel sysFuncModel) {
//		if(sysFuncModel.getFuncId() != null) {
//			sysFuncModel.setFlag("SUBFUNC");
//		}else if(sysFuncModel.getParentIdLevel2() != null) {
//			sysFuncModel.setFlag("FUNC");
//		}else if(sysFuncModel.getParentId() != null) {
//			sysFuncModel.setFlag("MENU");
//		}
		return sysFuncMapper.search(sysFuncModel);
	}
	
	@Override
	@OperationLog(operation = Constants.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insert(SysFuncModel sysFuncModel) throws SysException {
		sysFuncModel.setOperationContent("sysFunc-insert-sysFunc-insert-ADD-SYS_FUNC:" + sysFuncModel.opeartionContent());
		String msg = check(sysFuncModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		Integer result = 0;
		result = sysFuncMapper.checkFuncCode(sysFuncModel);
		if (result != 0) {
			throw new SysException("funcCode_cannot_be_repeated");
		}
		if ("MENU".equals(sysFuncModel.getType())) {
			result = sysFuncMapper.checkDispSeq1(sysFuncModel);
			if (result != 0) {
				throw new SysException("SEQ_CANNOT_BE_REPEATED");
			}
		} else {
			result = sysFuncMapper.checkDispSeq2(sysFuncModel);
			if (result != 0) {
				throw new SysException("SEQ_CANNOT_BE_REPEATED");
			}
		}
		result = sysFuncMapper.insert(sysFuncModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
		sysFuncModel.setOperationContent("sysFunc-insert-sysFunc-insert-ADD-SYS_FUNC:" + sysFuncModel.opeartionContent());
		return result;
	}

	@Override
	@OperationLog(operation = Constants.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object update(SysFuncModel sysFuncModel) throws SysException {
		sysFuncModel.setOperationContent(
				"sysFunc-update-sysFunc-update-MODIFY-SYS_FUNC:" + sysFuncModel.opeartionContent());
		String msg = check(sysFuncModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		Integer result = 0;
		if ("MENU".equals(sysFuncModel.getType())) {
			result = sysFuncMapper.checkDispSeq1(sysFuncModel);
			if (result != 0) {
				throw new SysException("SEQ_CANNOT_BE_REPEATED");
			}
		} else {
			result = sysFuncMapper.checkDispSeq2(sysFuncModel);
			if (result != 0) {
				throw new SysException("SEQ_CANNOT_BE_REPEATED");
			}
		}
		result = sysFuncMapper.update(sysFuncModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_UPDATE");
		}
		return result;
	}

	@Override
	@OperationLog(operation = Constants.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public Object delete(SysFuncModel sysFuncModel) throws SysException {
		sysFuncModel.setOperationContent(
				"sysFunc-delete-sysFunc-delete-DELETE-SYS_FUNC:" + sysFuncModel.opeartionContent());
		Integer result = 0;
		result = sysFuncMapper.searchSubRecord(sysFuncModel);
		if (result != 0) {
			throw new SysException("SUBRECORD_EXISTS");
		}
		int num = 0;
		num = userPreferenceMapper.searchByFuncId(sysFuncModel.getFuncId());
		result = userPreferenceMapper.deleteByFuncId(sysFuncModel.getFuncId());
		if(result != num) {
			throw new SysException("SQL_ERR_DELETE");
		}
		num = sysRoleFuncMapper.searchByFuncId(sysFuncModel.getFuncId());
		result = sysRoleFuncMapper.deleteByFuncId(sysFuncModel.getFuncId());
		if(result != num) {
			throw new SysException("SQL_ERR_DELETE");
		}
		result = sysFuncMapper.delete(sysFuncModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_DELETE");
		}
		return result;
	}

	@Override
	public List<SysFuncModel> searchFunc(SysFuncModel sysFuncModel) {
		return sysFuncMapper.searchFunc(sysFuncModel);
	}

	@Override
	public List<SysFuncModel> leftMenuSearch(SysFuncModel sysFuncModel) {
		return sysFuncMapper.leftMenuSearch(sysFuncModel);
	}

	@Override
	public List<SysFuncModel> searchPreferenceMenu(SysFuncModel sysFuncModel) {
		return sysFuncMapper.searchPreferenceMenu(sysFuncModel);
	}

	/**
	 * @param sysFuncModel
	 * @return
	 */
	private String check(SysFuncModel sysFuncModel) {
		String msg = "";
		if(StringUtils.isBlank(sysFuncModel.getFuncCode())) {
			msg += "FUNC_CODE_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysFuncModel.getFuncName())) {
			msg += "MODULE_NAME_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysFuncModel.getType())) {
			msg += "MODULE_TYPE_CANNOT_BE_EMPTY;";
		} else {
			if (sysFuncModel.getDispSeq() == null) {
				msg += "seq_cannot_be_empty;";
			}
			if ("FUNC".equals(sysFuncModel.getType()) && sysFuncModel.getParentId() == null) {
				msg += "LEVEL_1_MODULE_CANNOT_BE_EMPTY;";
			}
		}
		return msg;
	}

}
