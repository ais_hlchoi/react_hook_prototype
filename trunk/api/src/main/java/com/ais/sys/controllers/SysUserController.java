package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.model.SysUserRoleModel;
import com.ais.sys.service.SysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/user")
public class SysUserController extends CommonParent {

	@Autowired
	private SysUserService userService;

	/**
	 * Search User
	 * @param userModel
	 * @return
	 */
	@RequestMapping("/search")
	public Object search(@RequestBody SysUserModel userModel) {
		PageHelper.startPage(userModel.getPage(), userModel.getPageSize());
		List<SysUserModel> list = userService.search(userModel);
		return RestEntity.success(new PageInfo<>(list));
	}

	/**
	 * Search All User
	 * @param userModel
	 * @return
	 */
	@RequestMapping("/searchAll")
	public Object searchAll(@RequestBody SysUserModel userModel) {
		List<SysUserModel> list = userService.search(userModel);
		return RestEntity.success(list);
	}

	/**
	 * New User
	 * @param userModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/insert")
	public Object insert(@RequestBody SysUserModel userModel) throws Exception {
		Object object = userService.insert(userModel);
		return RestEntity.success(object);
	}

	/**
	 * Update User
	 * @param userModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/update")
	public Object update(@RequestBody SysUserModel userModel) throws Exception {
		Object object = userService.update(userModel);
		return RestEntity.success(object);
	}

	/**
	 * Delete User
	 * @param userModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/delete")
	public Object delete(@RequestBody SysUserModel userModel) throws SysException {
		userService.delete(userModel);
		return RestEntity.success();
	}

	/**
	 * Search User Role
	 * @param sysUserRoleModel
	 * @return
	 */
	@RequestMapping("/searchSysUserRole")
	public Object searchSysUserRole(@RequestBody SysUserRoleModel sysUserRoleModel) {
		List<SysUserRoleModel> list = userService.searchSysUserRole(sysUserRoleModel);
		return RestEntity.success(list);
	}
	
	/**
	 * Save User Role
	 * @param sysUserRoleModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveUserRole")
	public Object saveUserRole(@RequestBody SysUserRoleModel sysUserRoleModel) throws Exception {
		int count = userService.searchOnlyRole(sysUserRoleModel);
		if (count != 0) {
			return RestEntity.failed("ROLES_CANNOT_BE_REPEATED");
		}
		Object object = null;
		if (sysUserRoleModel.getUserRoleId() ==null) {
			object = userService.insertUserRole(sysUserRoleModel);//添加用户的角色
		} else {
			object = userService.updateUserRole(sysUserRoleModel);//修改用户的角色
		}
		return RestEntity.decide(object);
	}
	
	/**
	 * Delete User Role
	 * @param sysUserRoleModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/deleteUserRole")
	public Object deleteUserRole(@RequestBody SysUserRoleModel sysUserRoleModel) throws SysException {
		userService.deleteUserRole(sysUserRoleModel);
		return RestEntity.success();
	}
	
	/**
	 * Update User Password
	 * @param userModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/updatePwd")
	public Object updatePwd(@RequestBody SysUserModel userModel) throws Exception {
		userService.updatePwd(userModel);
		return RestEntity.success();
	}
	
}
