package com.ais.sys.service;

import java.util.List;

import com.ais.sys.model.SysNationalityModel;

public interface SysNationalityService {

	List<SysNationalityModel> getNationalityDownList();

}
