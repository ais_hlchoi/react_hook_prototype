package com.ais.sys.model;

import java.util.List;

import com.ais.framework.model.CommonParentModel;

public class SysFuncModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private Integer funcId;
	
	private Integer parentId;
	
	private String funcCode;

	private String funcName;

	private String type;

	private Integer dispSeq;

	private String funcUrl;
	
	private Integer parentIdLevel2;
	
	private List<SysFuncModel> children;

	private String flag;

	public Integer getFuncId() {
		return funcId;
	}

	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getFuncCode() {
		return funcCode;
	}

	public void setFuncCode(String funcCode) {
		this.funcCode = funcCode;
	}

	public Integer getDispSeq() {
		return dispSeq;
	}

	public void setDispSeq(Integer dispSeq) {
		this.dispSeq = dispSeq;
	}

	public String getFuncUrl() {
		return funcUrl;
	}

	public void setFuncUrl(String funcUrl) {
		this.funcUrl = funcUrl;
	}

	public List<SysFuncModel> getChildren() {
		return children;
	}

	public void setChildren(List<SysFuncModel> children) {
		this.children = children;
	}

	public Integer getParentIdLevel2() {
		return parentIdLevel2;
	}

	public void setParentIdLevel2(Integer parentIdLevel2) {
		this.parentIdLevel2 = parentIdLevel2;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public String opeartionContent() {
		return "{funcId:" + funcId + "; parentId:" + parentId + "; funcCode:" + funcCode + "; funcName:" + funcName
				+ "; type:" + type + "; dispSeq:" + dispSeq + "; funcUrl:" + funcUrl + "; parentIdLevel2:"
				+ parentIdLevel2 + "; children:" + children + "; flag:" + flag + "; canSelect:" + "}";
	}

}
