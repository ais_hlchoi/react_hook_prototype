package com.ais.sys.service;

import java.util.List;

import com.ais.sys.model.LoginLogManageModel;

public interface LoginLogManageService {
	
	List<LoginLogManageModel> search(LoginLogManageModel model);

}
