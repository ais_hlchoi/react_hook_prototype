package com.ais.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.SysNationalityMapper;
import com.ais.sys.model.SysNationalityModel;
import com.ais.sys.service.SysNationalityService;

/**
 */
@Service
public class SysNationalityServiceImpl extends CommonParent implements SysNationalityService {

	@Autowired
	private SysNationalityMapper nationalityMapper;

	@Override
	public List<SysNationalityModel> getNationalityDownList() {
		return nationalityMapper.getNationalityDownList();
	}


}
