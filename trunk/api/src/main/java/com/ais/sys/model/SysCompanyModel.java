package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SysCompanyModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private String companyCode;
	
	private String companyName;

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}