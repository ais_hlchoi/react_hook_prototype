package com.ais.sys.service;

import java.util.List;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.SysUserDetailModel;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.model.SysUserRoleModel;

public interface SysUserService {

	List<SysUserModel> search(SysUserModel userModel);

	List<SysUserModel> searchUserRoleByUserCode(String userCode);

	Object insert(SysUserModel userModel) throws SysException;

	Object update(SysUserModel userModel) throws SysException;

	void delete(SysUserModel userModel) throws SysException;

	List<SysUserRoleModel> searchSysUserRole(SysUserRoleModel sysUserRoleModel);

	int searchOnlyRole(SysUserRoleModel sysUserRoleModel);

	Object insertUserRole(SysUserRoleModel sysUserRoleModel) throws SysException;

	Object updateUserRole(SysUserRoleModel sysUserRoleModel) throws SysException;

	void deleteUserRole(SysUserRoleModel sysUserRoleModel) throws SysException;

	void updatePwd(SysUserModel userModel) throws SysException;
	
	List<SysUserDetailModel> getUserInfo(String userName);

	Integer getCountByPhone(SysUserModel userModel);

	Integer getCountByEmail(SysUserModel userModel);

	Integer getCountByName(SysUserModel userModel);

}
