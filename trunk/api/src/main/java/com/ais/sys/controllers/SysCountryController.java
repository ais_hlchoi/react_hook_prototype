package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysCountryModel;
import com.ais.sys.service.SysCountryService;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/country")
public class SysCountryController extends CommonParent {

	@Autowired
	private SysCountryService countryService;

	/**
	 *  Country Dropdown
	 * @param userModel
	 * @return
	 */
	@RequestMapping("/getCountryDownList")
	public Object getCountryDownList() {
		List<SysCountryModel> list = countryService.getCountryDownList();
		return RestEntity.success(list);
	}
	
}
