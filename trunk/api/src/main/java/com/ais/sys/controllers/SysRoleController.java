package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysRoleFuncModel;
import com.ais.sys.model.SysRoleModel;
import com.ais.sys.service.SysRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/sysRole")
public class SysRoleController extends CommonParent {

	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping("/search")
	public Object search(@RequestBody SysRoleModel sysRoleModel) {
		PageHelper.startPage(sysRoleModel.getPage(), sysRoleModel.getPageSize());
		List<SysRoleModel> list = sysRoleService.search(sysRoleModel);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchAll")
	public Object searchAll(@RequestBody SysRoleModel sysRoleModel) {
		List<SysRoleModel> list = sysRoleService.search(sysRoleModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchFunc")
	public Object searchFunc(@RequestBody SysRoleFuncModel sysRoleFuncModel) {
		List<SysRoleFuncModel> list = sysRoleService.searchFunc(sysRoleFuncModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchAllFunc")
	public Object searchAllFunc(@RequestBody SysRoleFuncModel sysRoleFuncModel) {
		List<SysRoleFuncModel> list = sysRoleService.searchAllFunc(sysRoleFuncModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchRoleFunc")
	public Object searchRoleFunc(@RequestBody SysRoleFuncModel sysRoleFuncModel) {
		List<SysRoleFuncModel> list = sysRoleService.searchRoleFunc(sysRoleFuncModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/insert")
	public Object insert(@RequestBody SysRoleModel sysRoleModel) throws SysException {
		sysRoleService.insert(sysRoleModel);
		return RestEntity.success();
	}

	@RequestMapping("/update")
	public Object update(@RequestBody SysRoleModel sysRoleModel) throws SysException {
		sysRoleService.update(sysRoleModel);
		return RestEntity.success();
	}

	@RequestMapping("/delete")
	public Object delete(@RequestBody SysRoleModel sysRoleModel) throws SysException {
		sysRoleService.delete(sysRoleModel);
		return RestEntity.success();
	}
}
