package com.ais.sys.model;

import java.util.List;

import com.ais.framework.model.CommonParentModel;

public class SysUserRoleModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private Integer userRoleId;

	private Integer roleId;

	private String roleCode;

	private String companyCode;

	private String companyName;

	private String roleName;

	private String defaultInd;
	
	private String activeInd;

	private List<SysUserRoleModel> sysUserRoleList;

	private List<String> list;

	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}

	public List<SysUserRoleModel> getSysUserRoleList() {
		return sysUserRoleList;
	}

	public void setSysUserRoleList(List<SysUserRoleModel> sysUserRoleList) {
		this.sysUserRoleList = sysUserRoleList;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getDefaultInd() {
		return defaultInd;
	}

	public void setDefaultInd(String defaultInd) {
		this.defaultInd = defaultInd;
	}

	public String opeartionContent() {
		return "{userRoleId:" + userRoleId + ";roleId:" + roleId + ";roleCode:" + roleCode + ";companyCode:" + companyCode
				+ ";companyName:" + companyName + ";roleName:" + roleName + ";defaultInd:" + defaultInd
				+ ";activeInd:" + activeInd + ";userId:" + userId + "}";
	}

}
