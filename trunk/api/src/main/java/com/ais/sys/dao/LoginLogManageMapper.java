package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.LoginLogManageModel;

public interface LoginLogManageMapper {
	
	List<LoginLogManageModel> search(LoginLogManageModel model);

}
