package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysParmModel;

public interface SysParmMapper {

	List<SysParmModel> search(SysParmModel sysParmModel);
	
	Integer insert(SysParmModel sysParmModel);
	
	Integer update(SysParmModel sysParmModel);
	
	Integer delete(SysParmModel sysParmModel);
	
	Integer onlyDispSeq(SysParmModel sysParmModel);
	
	Integer onlyCode(SysParmModel sysParmModel);
	
	List<SysParmModel> searchSysParmSegment(SysParmModel sysParmModel);

	List<SysParmModel> searchSysParmDownListBySegment(SysParmModel sysParmModel);
}
