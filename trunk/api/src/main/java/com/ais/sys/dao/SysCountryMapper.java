package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysCountryModel;

public interface SysCountryMapper {

	List<SysCountryModel> getCountryDownList();

}