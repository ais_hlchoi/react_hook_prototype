package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysParmModel;
import com.ais.sys.service.SysParamService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/sysParm")
public class SysParmController {
	
	@Autowired
	private SysParamService sysParamService;

	@RequestMapping("/search")
	public Object search(@RequestBody SysParmModel sysParmModel) {
		PageHelper.startPage(sysParmModel.getPage(), sysParmModel.getPageSize());
		List<SysParmModel> list = sysParamService.search(sysParmModel);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchAll")
	private Object searchAll(@RequestBody SysParmModel sysParmModel) {
		List<SysParmModel> list = sysParamService.search(sysParmModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/insert")
	public Object insert(@RequestBody SysParmModel sysParmModel) throws SysException {
		Object object = sysParamService.insert(sysParmModel);
		return RestEntity.decide(object);
	}

	@RequestMapping("/update")
	public Object update(@RequestBody SysParmModel sysParmModel) throws SysException {
		Object object = sysParamService.update(sysParmModel);
		return RestEntity.decide(object);
	}

	@RequestMapping("/delete")
	public Object delete(@RequestBody SysParmModel sysParmModel) throws SysException {
		Object object = sysParamService.delete(sysParmModel);
		return RestEntity.decide(object);
	}

	@RequestMapping("/searchSysParmSegment")
	public Object searchSysParmSegment(@RequestBody SysParmModel sysParmModel) {
		List<SysParmModel> list = sysParamService.searchSysParmSegment(sysParmModel);
		return RestEntity.success(list);
	}
	
	/**
	 * System Parameter Dropdown
	 * @param sysParmModel
	 * @return
	 */
	@RequestMapping("/searchSysParmDownListBySegment")
	public Object searchSysParmDownListBySegment(@RequestBody SysParmModel sysParmModel) {
		List<SysParmModel> list = sysParamService.searchSysParmDownListBySegment(sysParmModel);
		return RestEntity.success(list);
	}
}
