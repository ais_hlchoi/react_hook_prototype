package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SavedSearchModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private String savedSearch;
	
	private String accessGroup;
	
	private String selectClause;
	
	private String fromClause;
	
	private String whereClause;
	
	private String groupByClause;
	
	private String orderByClause;
	
	private String exportOpt;
	
	private String sql;
	
	public String getAccessGroup() {
		return accessGroup;
	}



	public void setAccessGroup(String accessGroup) {
		this.accessGroup = accessGroup;
	}



	public String getSelectClause() {
		return selectClause;
	}



	public void setSelectClause(String selectClause) {
		this.selectClause = selectClause;
	}



	public String getFromClause() {
		return fromClause;
	}



	public void setFromClause(String fromClause) {
		this.fromClause = fromClause;
	}



	public String getWhereClause() {
		return whereClause;
	}



	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}



	public String getGroupByClause() {
		return groupByClause;
	}



	public void setGroupByClause(String groupByClause) {
		this.groupByClause = groupByClause;
	}



	public String getOrderByClause() {
		return orderByClause;
	}



	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}



	public String getExportOpt() {
		return exportOpt;
	}



	public void setExportOpt(String exportOpt) {
		this.exportOpt = exportOpt;
	}



	public String getSql() {
		return sql;
	}



	public void setSql(String sql) {
		this.sql = sql;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String opeartionContent() {
		return "{accessGroup:" + accessGroup + "; selectClause:" + selectClause + "; fromClause:" + fromClause + "; whereClause:" + whereClause
				+ "; groupByClause:" + groupByClause + "; orderByClause:" + orderByClause + "; exportOpt:" + exportOpt + "}";
	}



	public String getSavedSearch() {
		return savedSearch;
	}



	public void setSavedSearch(String savedSearch) {
		this.savedSearch = savedSearch;
	}
}
