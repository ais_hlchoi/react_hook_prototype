package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysNationalityModel;
import com.ais.sys.service.SysNationalityService;

@RestController
@RequestMapping("/api/sys/nationality")
public class SysNationalityController extends CommonParent {

	@Autowired
	private SysNationalityService nationalityService;

	/**
	 * Nationality Dropdown
	 * @param userModel
	 * @return
	 */
	@RequestMapping("/getNationalityDownList")
	public Object getNationalityDownList() {
		List<SysNationalityModel> list = nationalityService.getNationalityDownList();
		return RestEntity.success(list);
	}
	
}
