package com.ais.sys.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.logAspect.annotation.OperationLog;
import com.ais.framework.constant.Constants;
import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.SysParmMapper;
import com.ais.sys.model.SysParmModel;
import com.ais.sys.service.SysParamService;

@Service
public class SysParmServiceImpl extends CommonParent implements SysParamService {

	@Autowired
	private SysParmMapper sysParamMapper;

	@Override
	public List<SysParmModel> search(SysParmModel sysParmModel) {
		return sysParamMapper.search(sysParmModel);
	}

	@Override
	@OperationLog(operation = Constants.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insert(SysParmModel sysParmModel) throws SysException {
		sysParmModel.setOperationContent("sysParam-insert-sysParam-insert-ADD-SYS_PARAM:" + sysParmModel.opeartionContent());
		String msg = check(sysParmModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		Integer result = sysParamMapper.onlyCode(sysParmModel);
		if (result != 0) {
			throw new SysException("CODE_CANNOT_BE_REPEATED");
		}
		result = sysParamMapper.onlyDispSeq(sysParmModel);
		if (result != 0) {
			throw new SysException("SEQ_CANNOT_BE_REPEATED");
		}
		result = sysParamMapper.insert(sysParmModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
		sysParmModel.setOperationContent("sysParam-insert-sysParam-insert-ADD-SYS_PARAM:" + sysParmModel.opeartionContent());
		return result;
	}

	@Override
	@OperationLog(operation = Constants.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object update(SysParmModel sysParmModel) throws SysException {
		sysParmModel.setOperationContent("sysParam-update-sysParam-update-MODIFY-SYS_PARAM:" + sysParmModel.opeartionContent());
		String msg = check(sysParmModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		Integer result = sysParamMapper.onlyCode(sysParmModel);
		if (result != 0) {
			throw new SysException("CODE_CANNOT_BE_REPEATED");
		}
		result = sysParamMapper.onlyDispSeq(sysParmModel);
		if (result != 0) {
			throw new SysException("SEQ_CANNOT_BE_REPEATED");
		}
		result = sysParamMapper.update(sysParmModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_UPDATE");
		}
		return result;
	}

	@Override
	@OperationLog(operation = Constants.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public Object delete(SysParmModel sysParmModel) throws SysException {
		sysParmModel.setOperationContent("sysParam-delete-sysParam-delete-DELETE-SYS_PARAM:" + sysParmModel.opeartionContent());
		if (sysParmModel.getId() == null) {
			throw new SysException("ID_CANNOT_BE_EMPTY");
		}
		Integer result = sysParamMapper.delete(sysParmModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_DELETE");
		}
		return result;
	}

	@Override
	public List<SysParmModel> searchSysParmSegment(SysParmModel sysParmModel) {
		return sysParamMapper.searchSysParmSegment(sysParmModel);
	}

	public String check(SysParmModel sysParmModel) {
		String msg = "";
		if (StringUtils.isBlank(sysParmModel.getSegment())) {
			msg += "PARAMETER_NAME_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysParmModel.getCode())) {
			msg += "CODE_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysParmModel.getShortDesc())) {
			msg += "DESCRIPTION_CANNOT_BE_EMPTY;";
		}
		/*if (StringUtils.isBlank(sysParmModel.getParmValue1())) {
			msg += "PARAMETER_VALUE_1_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysParmModel.getActiveInd())) {
			msg += "STATUS_CANNOT_BE_EMPTY;";
		}*/
		if (sysParmModel.getSegment() == null) {
			msg += "seq_cannot_be_empty;";
		}
		return msg;
	}

	@Override
	public List<SysParmModel> searchSysParmDownListBySegment(SysParmModel sysParmModel) {
		return sysParamMapper.searchSysParmDownListBySegment(sysParmModel);
	}

}
