package com.ais.sys.model;

import java.util.Date;

import com.ais.framework.model.CommonParentModel;

public class OperationLogModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;
	
	private Integer operationLogId;
	
	private String operationType;
	
	private String serviceAddress;
	
	private String status;
	
	private String loginIpAddress;
	
	private Date operationTime;
	
	private String responseCode;
	
	private String responseDesc;
	
	private Date responseTime;

	public Integer getOperationLogId() {
		return operationLogId;
	}

	public void setOperationLogId(Integer operationLogId) {
		this.operationLogId = operationLogId;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLoginIpAddress() {
		return loginIpAddress;
	}

	public void setLoginIpAddress(String loginIpAddress) {
		this.loginIpAddress = loginIpAddress;
	}

	public Date getOperationTime() {
		return operationTime;
	}

	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
	
	public OperationLogModel() {
		super();
	}
	
	public OperationLogModel(Integer userId, Date operationTime, String operationType, String loginIpAddress, String serviceAddress,
			String createdBy) {
		super();
		this.userId = userId;
		this.operationType = operationType;
		this.serviceAddress = serviceAddress;
		this.loginIpAddress = loginIpAddress;
		this.operationTime = operationTime;
		this.createdBy = createdBy;
	}
	
	public OperationLogModel(Date operationTime, String loginIpAddress) {
		super();
		this.loginIpAddress = loginIpAddress;
		this.operationTime = operationTime;
	}

}
