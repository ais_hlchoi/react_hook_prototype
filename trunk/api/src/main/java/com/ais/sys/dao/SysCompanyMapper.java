package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysCompanyModel;

public interface SysCompanyMapper {

	List<SysCompanyModel> search(SysCompanyModel model);

	int insert(SysCompanyModel model);

	int update(SysCompanyModel model);

	int delete(SysCompanyModel model);

	SysCompanyModel getLastUpdteDteById(String code);

	List<SysCompanyModel> getCompanyDownList(SysCompanyModel sysCompanyModel);

	Integer checkExistCount(SysCompanyModel sysCompanyModel);

}