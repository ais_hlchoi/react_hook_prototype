package com.ais.sys.model;

import java.util.List;

import com.ais.framework.model.CommonParentModel;

public class SysUserPreferenceModel extends CommonParentModel {
	private static final long serialVersionUID = 1L;

	private String preferenceId;

	private String funcId;

	private String funcName;

	private String parentId;

	private String type;

	private Integer dispSeq;

	private List<SysUserPreferenceModel> funcList;

	public String getPreferenceId() {
		return preferenceId;
	}

	public void setPreferenceId(String preferenceId) {
		this.preferenceId = preferenceId;
	}

	public String getFuncId() {
		return funcId;
	}

	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getDispSeq() {
		return dispSeq;
	}

	public void setDispSeq(Integer dispSeq) {
		this.dispSeq = dispSeq;
	}

	public List<SysUserPreferenceModel> getFuncList() {
		return funcList;
	}

	public void setFuncList(List<SysUserPreferenceModel> funcList) {
		this.funcList = funcList;
	}

}
