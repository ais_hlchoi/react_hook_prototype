package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysFuncModel;

public interface SysFuncMapper {

	List<SysFuncModel> search(SysFuncModel sysFuncModel);
	
	List<SysFuncModel> leftMenuSearch(SysFuncModel sysFuncModel);

	List<SysFuncModel> searchPreferenceMenu(SysFuncModel sysFuncModel);

	List<SysFuncModel> searchFunc(SysFuncModel sysFuncModel);

	SysFuncModel getById(SysFuncModel sysFuncModel);

	Integer insert(SysFuncModel sysFuncModel);

	Integer update(SysFuncModel sysFuncModel);

	Integer delete(SysFuncModel sysFuncModel);
	
	Integer checkFuncCode(SysFuncModel sysFuncModel);

	Integer checkDispSeq1(SysFuncModel sysFuncModel);

	Integer checkDispSeq2(SysFuncModel sysFuncModel);

	Integer searchSubRecord(SysFuncModel sysFuncModel);

}
