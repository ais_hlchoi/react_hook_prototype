package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SysCountryModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private String countryCode;
	
	private String countryName;
	
	private String countryNameZhs;
	
	private String countryNameEng ;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryNameZhs() {
		return countryNameZhs;
	}

	public void setCountryNameZhs(String countryNameZhs) {
		this.countryNameZhs = countryNameZhs;
	}

	public String getCountryNameEng() {
		return countryNameEng;
	}

	public void setCountryNameEng(String countryNameEng) {
		this.countryNameEng = countryNameEng;
	}
	
	
}