package com.ais.sys.controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysCompanyModel;
import com.ais.sys.service.SysCompanyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sys/company")
public class SysCompanyController extends CommonParent {

	@Autowired
	private SysCompanyService sysCompanyService;

	/**
	 * Search Company
	 * @param SysCompanyModel
	 * @return
	 */
	@RequestMapping("/search")
	public Object search(@RequestBody SysCompanyModel SysCompanyModel) {
		PageHelper.startPage(SysCompanyModel.getPage(), SysCompanyModel.getPageSize());
		List<SysCompanyModel> list = sysCompanyService.search(SysCompanyModel);
		return RestEntity.success(new PageInfo<>(list));
	}

	/**
	 * New Company
	 * @param SysCompanyModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/insert")
	public Object insert(@RequestBody SysCompanyModel SysCompanyModel) throws Exception {
		String msg = checkData(SysCompanyModel,"INSERT").toString();
		if (StringUtils.isNotBlank(msg)) {
			return RestEntity.failed(msg);
		}
		sysCompanyService.insert(SysCompanyModel);
		return RestEntity.success();
	}

	/**
	 *  Update Company
	 * @param SysCompanyModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/update")
	public Object update(@RequestBody SysCompanyModel SysCompanyModel) throws Exception {
		String msg = checkData(SysCompanyModel,"UPDATE").toString();
		if (StringUtils.isNotBlank(msg)) {
			return RestEntity.failed(msg);
		}
		sysCompanyService.update(SysCompanyModel);
		return RestEntity.success();
	}
	
	/**
	 *  Delete Company
	 * @param SysCompanyModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/delete")
	public Object delete(@RequestBody SysCompanyModel SysCompanyModel) throws SysException {
		sysCompanyService.delete(SysCompanyModel);
		return RestEntity.success();
	}
	
	/**
	 * Validate Company
	 * @param SysCompanyModel
	 * @return
	 */
	public StringBuffer checkData(SysCompanyModel sysCompanyModel,String type) {
		StringBuffer msg = new StringBuffer();
		SysCompanyModel model = new SysCompanyModel();
		model.setCreatedDate(sysCompanyModel.getCreatedDate());
		if(StringUtils.isBlank(sysCompanyModel.getCompanyCode())) {
			msg.append("CO_CODE_NOT_NULL_ERR;");
		}else {
			model.setCompanyCode(sysCompanyModel.getCompanyCode());
			Integer count = sysCompanyService.checkExistCount(model);
			if(count>0) {
				msg.append("CO_CODE_IS_EXIST;");
			}
		}
		if(StringUtils.isBlank(sysCompanyModel.getCompanyName())) {
			msg.append("CO_NAME_NOT_NULL_ERR;");
		}else {
			model.setCompanyCode(null);
			model.setCompanyName(sysCompanyModel.getCompanyName());
			Integer count = sysCompanyService.checkExistCount(model);
			if(count>0 ) {
				msg.append("CO_NAME_IS_EXIST;");
			}
		}
		return msg;
	}
	
	/**
	 * Company Dropdown
	 * @param SysCompanyModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getCompanyDownList")
	public Object getCompanyDownList(@RequestBody SysCompanyModel SysCompanyModel) throws Exception {
		List<SysCompanyModel> list = sysCompanyService.getCompanyDownList(SysCompanyModel);
		return RestEntity.success(list);
	}
	
}
