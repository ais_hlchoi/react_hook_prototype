package com.ais.sys.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.constant.Constants;
import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.logAspect.annotation.OperationLog;
import com.ais.sys.dao.SysUserMapper;
import com.ais.sys.dao.SysUserPreferenceMapper;
import com.ais.sys.dao.SysUserPwdHistMapper;
import com.ais.sys.dao.SysUserRoleMapper;
import com.ais.sys.model.SysUserDetailModel;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.model.SysUserPreferenceModel;
import com.ais.sys.model.SysUserPwdHistModel;
import com.ais.sys.model.SysUserRoleModel;
import com.ais.sys.service.SysUserService;
import com.ais.util.PBKDF2Encryption;

/**
 */
@Service
public class SysUserServiceImpl extends CommonParent implements SysUserService {

	@Autowired
	private SysUserMapper sysUserMapper;

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	
	@Autowired
	private SysUserPwdHistMapper sysUserPwdHistMapper;
	
	@Autowired
	private SysUserPreferenceMapper sysUserPreferenceMapper;

	@Override
	public List<SysUserModel> search(SysUserModel userModel) {
		return sysUserMapper.search(userModel);
	}

	/**
	 */
	@Override
	public List<SysUserModel> searchUserRoleByUserCode(String userCode) {
		return sysUserMapper.searchUserRoleByUserCode(userCode);
	}
	
	@Override
	@OperationLog(operation = Constants.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insert(SysUserModel userModel) throws SysException {
		userModel.setOperationContent("sysUser-insert-sysUser-insert-ADD-SYS_USER:" + userModel.opeartionContent());
		String msg = checkAddData(userModel).toString();
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		if (StringUtils.isNotBlank(userModel.getPwd())) {
//			userModel.setSalt(PasswordHash.getSaltValue(userModel.getPwd()));
//			userModel.setPwd(PasswordHash.getHashvalue(userModel.getPwd(), userModel.getSalt()));
			try {
				userModel.setSalt(PBKDF2Encryption.getSalt());
				userModel.setPwd(PBKDF2Encryption.getPBKDF2(userModel.getPwd(), userModel.getSalt()));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				throw new SysException("Invalid password.");
			}
		}
		int result = sysUserMapper.insert(userModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}else {
			SysUserPwdHistModel sysUserPwdHistModel = new SysUserPwdHistModel();
			sysUserPwdHistModel.setUserId(userModel.getUserId());
			sysUserPwdHistModel.setSalt(userModel.getSalt());
			sysUserPwdHistModel.setPwd(userModel.getPwd());
			sysUserPwdHistModel.setCreatedBy(userModel.getCreatedBy());
			sysUserPwdHistModel.setLastUpdatedBy(userModel.getCreatedBy());
			result = sysUserPwdHistMapper.insert(sysUserPwdHistModel);
			if (result != 1) {
				throw new SysException("SQL_ERR_INSERT");
			}
		}
		SysUserModel user = sysUserMapper.searchByUserId(userModel.getUserId());
		userModel.setOperationContent("sysUser-insert-sysUser-insert-ADD-SYS_USER:" + userModel.opeartionContent());
		return user;
	}
	
	private Object checkAddData(SysUserModel userModel) {
		StringBuffer msg = new StringBuffer();
		if(StringUtils.isBlank(userModel.getUserCode())) {
			msg.append("USER_CODE_NOT_NULL_ERR;");
		}else {
			Integer count = sysUserMapper.getCountByCode(userModel);
			if(count>0) {
				msg.append("USER_CODE_IS_EXIST;");
			}
		}
		if(StringUtils.isBlank(userModel.getUserName())) {
			msg.append("USER_NAME_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getAccPwGroup())) {
			msg.append("ACC_PW_GROUP_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getEmail())) {
			msg.append("EMAIL_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getHomePhone())) {
			msg.append("HOME_PHONE_NOT_NULL_ERR;");
		}
		if (StringUtils.isBlank(userModel.getPwd())) {
			msg.append("PWD_NOT_NULL_ERR;");
		}
		
//		String checkRule = PasswordCheckUtil.checkRule(userModel.getPwd());
//		if (!"ok".equals(checkRule)) {
//			msg.append(checkRule);
//		}
//		
//		String checkRepeat = PasswordCheckUtil.checkRepeat(userModel.getPwd());
//		if (!"ok".equals(checkRepeat)) {
//			msg.append(checkRepeat);
//		}
//		
//		String checkSame = PasswordCheckUtil.checkSame(userModel.getPwd());
//		if (!"ok".equals(checkSame)) {
//			msg.append(checkSame);
//		}
//		
//		String checkWrongful = PasswordCheckUtil.checkWrongful(userModel.getPwd());
//		if (!"ok".equals(checkWrongful)) {
//			msg.append(checkWrongful);
//		}
//		
//		String checkContainUserCode = PasswordCheckUtil.checkContainUserCode(userModel.getPwd(),userModel.getUserCode());
//		if (!"ok".equals(checkContainUserCode)) {
//			msg.append(checkContainUserCode);
//		}
//		
//		String checkContainName = PasswordCheckUtil.checkContainName(userModel.getPwd(),userModel.getUserName());
//		if (!"ok".equals(checkContainName)) {
//			msg.append(checkContainName);
//		}
//		
//		SysUserPwdHistModel pwdHistModel = new SysUserPwdHistModel();
//		pwdHistModel.setUserId(userModel.getUserId());
//		pwdHistModel.setTimes(5);
//		List<SysUserPwdHistModel> pwdList = sysUserPwdHistService.searchFiveLimit(pwdHistModel);//查询最近的5条密码记录
//		String checkPwdFive = PasswordCheckUtil.checkPwdFive(userModel.getUserId(),userModel.getPwd(),pwdList);
//		if (!"ok".equals(checkPwdFive)) { 
//			msg.append(checkPwdFive);
//		}
		return msg;
	}

	@Override
	@OperationLog(operation = Constants.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object update(SysUserModel userModel) throws SysException {
		userModel.setOperationContent("sysUser-update-sysUser-update-MODIFY-SYS_USER:" + userModel.opeartionContent());
		String msg = checkData(userModel).toString();
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		SysUserModel user = sysUserMapper.getLastUpdteDteById(userModel.getUserId());
		if (user == null || !userModel.getLastUpdatedDate().equals(user.getLastUpdatedDate())) {
			throw new SysException("DATA_UPDATE_TIMEOUT");
		}
		int result = sysUserMapper.update(userModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
		SysUserModel model = sysUserMapper.searchByUserId(userModel.getUserId());
		return model;
	}

	/**
	 * 修改時進行check
	 * @param userModel
	 * @return
	 */
	public StringBuffer checkData(SysUserModel userModel) {
		StringBuffer msg = new StringBuffer();
		if(StringUtils.isBlank(userModel.getUserCode())) {
			msg.append("USER_CODE_NOT_NULL_ERR;");
		}else {
			Integer count = sysUserMapper.getCountByCode(userModel);
			if(count>0 && userModel.getUserId() !=null) {
				msg.append("USER_CODE_IS_EXIST;");
			}
		}
		if(StringUtils.isBlank(userModel.getUserName())) {
			msg.append("USER_NAME_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getAccPwGroup())) {
			msg.append("ACC_PW_GROUP_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getEmail())) {
			msg.append("EMAIL_NOT_NULL_ERR;");
		}
		if(StringUtils.isBlank(userModel.getHomePhone())) {
			msg.append("HOME_PHONE_NOT_NULL_ERR;");
		}
//		if (StringUtils.isNotBlank(userModel.getTel())) {
//			if(CheckInformationUtil.checkNumber(userModel.getTel())) {
//				msg.append("PHONE_FORMAT_ERR;");
//			}else {
//				Integer count = sysUserService.getCountByPhone(userModel);
//				if(count>0) {
//					msg.append("PHONE_IS_EXIST;");
//				}
//			}
//		}
//		if (StringUtils.isNotBlank(userModel.getEmail())) {
//			Integer count = sysUserService.getCountByEmail(userModel);
//			if(count>0) {
//				msg.append("EMAIL_IS_EXIST;");
//			}
//		}
		return msg;
	}

	@Override
	@OperationLog(operation = Constants.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public void delete(SysUserModel userModel)  throws SysException{
		userModel.setOperationContent("sysUser-delete-sysUser-delete-DELETE-SYS_USER:" + userModel.opeartionContent());
		SysUserModel user = sysUserMapper.searchByUserId(userModel.getUserId());
		if(!"N".equals(user.getActiveInd())) {
			throw new SysException("ACTIVATED_CANNOT_BE_DELETE");
		}
		sysUserPwdHistMapper.deletePwdHistByUserId(userModel);//delete sys_user_pwd_hist
		sysUserMapper.deleteAllByUserId(userModel);//delete operation_log
		SysUserPreferenceModel userPreferenceModel = new SysUserPreferenceModel();
		userPreferenceModel.setUserId(userModel.getUserId());
		sysUserPreferenceMapper.deletaAllPreference(userPreferenceModel);//delete sys_user_func_preference
		SysUserRoleModel sysUserRoleModel = new SysUserRoleModel();
		sysUserRoleModel.setUserId(userModel.getUserId());
		sysUserRoleMapper.deleteByUserId(sysUserRoleModel);//delete sys_user_role
		int result = sysUserMapper.delete(userModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_DELETE");
		}
	}

	@Override
	public List<SysUserRoleModel> searchSysUserRole(SysUserRoleModel sysUserRoleModel) {
		return sysUserRoleMapper.searchSysUserRole(sysUserRoleModel);
	}

	@Override
	public int searchOnlyRole(SysUserRoleModel sysUserRoleModel) {
		return sysUserRoleMapper.searchOnlyRole(sysUserRoleModel);
	}

	@Override
	@OperationLog(operation = Constants.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insertUserRole(SysUserRoleModel sysUserRoleModel) throws SysException {
		sysUserRoleModel.setOperationContent("sysUser-saveUserRole-sysUser-insertUserRole-ADD-SYS_USER_ROLE:" 
				+ sysUserRoleModel.opeartionContent());
		if("Y".equals(sysUserRoleModel.getDefaultInd())) {
			List<SysUserRoleModel> list = sysUserRoleMapper.searchSysUserRole(sysUserRoleModel);
			for (int i = 0; i < list.size(); i++) {
				SysUserRoleModel itemModel = list.get(i);
				sysUserRoleMapper.updateDefaultIndById(itemModel);
			}
		}
		int result = sysUserRoleMapper.insertAll(sysUserRoleModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
		sysUserRoleModel.setOperationContent("sysUser-saveUserRole-sysUser-insertUserRole-ADD-SYS_USER_ROLE:" 
				+ sysUserRoleModel.opeartionContent());
		return sysUserRoleModel;
	}

	@Override
	@OperationLog(operation = Constants.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object updateUserRole(SysUserRoleModel sysUserRoleModel) throws SysException {
		SysUserRoleModel userRole = sysUserRoleMapper.getLastUpdteDteById(sysUserRoleModel.getUserRoleId());
		if (userRole == null || !sysUserRoleModel.getLastUpdatedDate().equals(userRole.getLastUpdatedDate())) {
			throw new SysException("DATA_UPDATE_TIMEOUT");
		}
		sysUserRoleModel.setOperationContent("sysUser-saveUserRole-sysUser-updateUserRole-MODIFY-SYS_USER_ROLE:" 
				+ sysUserRoleModel.opeartionContent());
		if("Y".equals(sysUserRoleModel.getDefaultInd())) {
			List<SysUserRoleModel> list = sysUserRoleMapper.searchSysUserRoleById(sysUserRoleModel);
			for (int i = 0; i < list.size(); i++) {
				SysUserRoleModel itemModel = list.get(i);
				itemModel.setLastUpdatedBy(sysUserRoleModel.getCreatedBy());
				sysUserRoleMapper.updateDefaultIndById(itemModel);
			}
		}
		int result = sysUserRoleMapper.updateUserRole(sysUserRoleModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_UPDATE");
		}
		return sysUserRoleModel;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteUserRole(SysUserRoleModel sysUserRoleModel) throws SysException {
		List<SysUserRoleModel> deleteList = sysUserRoleModel.getSysUserRoleList();
		for (int i = 0; i < deleteList.size(); i++) {
			SysUserRoleModel model = deleteList.get(i);
			int result = sysUserRoleMapper.deleteUserRole(model);
			if (result != 1) {
				throw new SysException("SQL_ERR_DELETE");
			}
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updatePwd(SysUserModel userModel) throws SysException {
		StringBuffer bf = new StringBuffer();
		if (userModel.getUserId() == null) {
			bf.append("USER_ID_NOT_NULL_ERR;");
		}
		if (StringUtils.isBlank(userModel.getPwd())) {
			bf.append("PWD_NOT_NULL_ERR;");
		}
		if (StringUtils.isNotBlank(bf.toString())) {
			throw new SysException(bf.toString());
		}
		if (StringUtils.isNotBlank(userModel.getPwd())) {
//			userModel.setSalt(PasswordHash.getSaltValue(userModel.getPwd()));
//			userModel.setPwd(PasswordHash.getHashvalue(userModel.getPwd(), userModel.getSalt()));
			try {
				userModel.setSalt(PBKDF2Encryption.getSalt());
				userModel.setPwd(PBKDF2Encryption.getPBKDF2(userModel.getPwd(), userModel.getSalt()));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				throw new SysException("Invalid password.");
			}
		}
		SysUserModel user = sysUserMapper.getLastUpdteDteById(userModel.getUserId());
		if (user == null || !userModel.getLastUpdatedDate().equals(user.getLastUpdatedDate())) {
			throw new SysException("DATA_UPDATE_TIMEOUT");
		}
		int result = sysUserMapper.updatePwd(userModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_UPDATE");
		}
	}
	
	@Override
	public List<SysUserDetailModel> getUserInfo(String userName) {
		return sysUserRoleMapper.getUserInfo(userName);
	}

	@Override
	public Integer getCountByPhone(SysUserModel userModel) {
		return sysUserMapper.getCountByPhone(userModel);
	}

	@Override
	public Integer getCountByEmail(SysUserModel userModel) {
		return sysUserMapper.getCountByEmail(userModel);
	}

	@Override
	public Integer getCountByName(SysUserModel userModel) {
		return sysUserMapper.getCountByName(userModel);
	}

}
