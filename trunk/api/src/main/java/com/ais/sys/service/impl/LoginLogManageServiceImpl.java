package com.ais.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.LoginLogManageMapper;
import com.ais.sys.model.LoginLogManageModel;
import com.ais.sys.service.LoginLogManageService;

@Service
public class LoginLogManageServiceImpl extends CommonParent implements LoginLogManageService {
	
	@Autowired
	private LoginLogManageMapper loginLogManageMapper;
	
	@Override
	public List<LoginLogManageModel> search(LoginLogManageModel model) {
		return loginLogManageMapper.search(model);
	}

}
