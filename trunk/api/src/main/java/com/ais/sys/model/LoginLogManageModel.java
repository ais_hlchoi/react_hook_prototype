package com.ais.sys.model;

import java.util.Date;

public class LoginLogManageModel extends OperationLogModel {

	private static final long serialVersionUID = 1L;
	
	private String userCode;

	private String userName;
	
	private Date startDate;
	
	private Date endDate;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
