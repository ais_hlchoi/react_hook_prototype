package com.ais.sys.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysUserPreferenceModel;
import com.ais.sys.service.SysUserPreferenceService;

@RestController
@RequestMapping("/api/sys/userPreference")
public class SysUserPreferenceController {

	@Autowired
	private SysUserPreferenceService userPreferenceService;

	@RequestMapping("/searchUserAllFunc")
	public Object searchUserAllFunc(@RequestBody SysUserPreferenceModel userPreferenceModel) {
		List<SysUserPreferenceModel> list = userPreferenceService.searchUserAllFunc(userPreferenceModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/search")
	public Object search(@RequestBody SysUserPreferenceModel userPreferenceModel) {
		List<SysUserPreferenceModel> list = userPreferenceService.search(userPreferenceModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/setUp")
	public Object setUp(@RequestBody SysUserPreferenceModel userPreferenceModel) {
		userPreferenceService.setUp(userPreferenceModel);
		return RestEntity.success();
	}
}
