package com.ais.sys.service;

import java.util.List;

import com.ais.sys.model.SysUserPreferenceModel;

public interface SysUserPreferenceService {
	
	List<SysUserPreferenceModel> searchUserAllFunc(SysUserPreferenceModel userPreferenceModel);

	List<SysUserPreferenceModel> search(SysUserPreferenceModel userPreferenceModel);

	void setUp(SysUserPreferenceModel userPreferenceModel);

	Integer searchByFuncId(Integer funcId);
}
