package com.ais.sys.service;

import java.util.List;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.SysRoleFuncModel;
import com.ais.sys.model.SysRoleModel;
import com.ais.sys.model.SysUserModel;

public interface SysRoleService {

	List<SysRoleModel> search(SysRoleModel sysRoleModel);

	Object insert(SysRoleModel sysRoleModel) throws SysException;

	Object update(SysRoleModel sysRoleModel) throws SysException;

	Object delete(SysRoleModel sysRoleModel) throws SysException;

	List<SysRoleFuncModel> searchRoleFunc(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchAllFunc(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchFunc(SysRoleFuncModel sysRoleFuncModel);

	List<SysRoleFuncModel> searchUserPower(SysRoleFuncModel sysRoleFuncModel);
	
	List<SysRoleFuncModel> searchRoleFuncByUserCode(SysUserModel model);

	Integer searchByFuncId(Integer funcId);
}
