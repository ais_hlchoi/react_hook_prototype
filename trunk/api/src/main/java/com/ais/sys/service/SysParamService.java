package com.ais.sys.service;

import java.util.List;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.SysParmModel;

public interface SysParamService {

	List<SysParmModel> search(SysParmModel SysParmModel);

	Object insert(SysParmModel SysParmModel) throws SysException;

	Object update(SysParmModel SysParmModel) throws SysException;

	Object delete(SysParmModel SysParmModel) throws SysException;
	
	List<SysParmModel> searchSysParmSegment(SysParmModel sysParmModel);

	List<SysParmModel> searchSysParmDownListBySegment(SysParmModel sysParmModel);
}
