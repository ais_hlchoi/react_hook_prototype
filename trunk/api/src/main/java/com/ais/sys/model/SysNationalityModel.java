package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SysNationalityModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private String nationalityCode;
	
	private String nationalityName;
	
	private String nationalityNameZhs;
	
	private String nationalityNameEng ;
	
	public String getNationalityCode() {
		return nationalityCode;
	}
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}
	public String getNationalityName() {
		return nationalityName;
	}
	public void setNationalityName(String nationalityName) {
		this.nationalityName = nationalityName;
	}
	public String getNationalityNameZhs() {
		return nationalityNameZhs;
	}
	public void setNationalityNameZhs(String nationalityNameZhs) {
		this.nationalityNameZhs = nationalityNameZhs;
	}
	public String getNationalityNameEng() {
		return nationalityNameEng;
	}
	public void setNationalityNameEng(String nationalityNameEng) {
		this.nationalityNameEng = nationalityNameEng;
	}

	
}