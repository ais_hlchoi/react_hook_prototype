package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysUserDetailModel;
import com.ais.sys.model.SysUserRoleModel;

public interface SysUserRoleMapper {

	List<SysUserRoleModel> searchSysUserRole(SysUserRoleModel sysUserRoleModel);
	
	List<SysUserDetailModel> getUserInfo(String userName);

	int insertAll(SysUserRoleModel sysUserRoleModel);

	int updateAll(SysUserRoleModel sysUserRoleModel);

	int deleteByUserId(SysUserRoleModel sysUserRoleModel);

	int deleteAll(SysUserRoleModel sysUserRoleModel);

	int searchBySysRoleId(SysUserRoleModel sysUserRoleModel);

	int searchOnlyRole(SysUserRoleModel sysUserRoleModel);

	int deleteUserRole(SysUserRoleModel sysUserRoleModel);

	int updateUserRole(SysUserRoleModel sysUserRoleModel);

	SysUserRoleModel getLastUpdteDteById(Integer userRoleId);

	Integer getCountByCompanyCode(String companyCode);

	void updateDefaultIndById(SysUserRoleModel sysUserRoleModel);

	List<SysUserRoleModel> searchSysUserRoleById(SysUserRoleModel sysUserRoleModel);
}
