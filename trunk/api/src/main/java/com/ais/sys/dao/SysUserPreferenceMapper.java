package com.ais.sys.dao;

import java.util.List;

import com.ais.sys.model.SysUserPreferenceModel;

public interface SysUserPreferenceMapper {
	List<SysUserPreferenceModel> searchUserAllFunc(SysUserPreferenceModel userPreferenceModel);

	List<SysUserPreferenceModel> search(SysUserPreferenceModel userPreferenceModel);

	Integer insertAll(SysUserPreferenceModel userPreferenceModel);

	Integer deletaAllPreference(SysUserPreferenceModel userPreferenceModel);

	Integer searchByFuncId(Integer funcId);

	Integer deleteByFuncId(Integer funcId);
}
