package com.ais.sys.service;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.OperationLogModel;

public interface OperationLogService {
	
	void insert(OperationLogModel model) throws SysException;

}
