package com.ais.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ais.framework.constant.Constants;
import com.ais.framework.exception.SysException;
import com.ais.framework.model.CommonParent;
import com.ais.logAspect.annotation.OperationLog;
import com.ais.sys.dao.SysRoleFuncMapper;
import com.ais.sys.dao.SysRoleMapper;
import com.ais.sys.dao.SysUserRoleMapper;
import com.ais.sys.model.SysRoleFuncModel;
import com.ais.sys.model.SysRoleModel;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.model.SysUserRoleModel;
import com.ais.sys.service.SysRoleService;

@Service
public class SysRoleServiceImpl extends CommonParent implements SysRoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;

	@Autowired
	private SysRoleFuncMapper sysRoleFuncMapper;

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public List<SysRoleModel> search(SysRoleModel sysRoleModel) {
		return sysRoleMapper.search(sysRoleModel);
	}

	@Override
	@OperationLog(operation = Constants.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insert(SysRoleModel sysRoleModel)  throws SysException{
		sysRoleModel.setOperationContent("sysRole-insert-sysRole-insert-ADD-SYS_ROLE:" + sysRoleModel.opeartionContent());
		String msg = check(sysRoleModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		Integer result = sysRoleMapper.onlyDispSeq(sysRoleModel);
		if (result != 0) {
			throw new SysException("SEQ_CANNOT_BE_REPEATED");
		}
		result = sysRoleMapper.onlyRoleCode(sysRoleModel);
		if (result != 0) {
			throw new SysException("ROLE_CODE_CANNOT_BE_DUPLICATE");
		}
		result = sysRoleMapper.insert(sysRoleModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_INSERT");
		}
		if (sysRoleModel.getFuncList() != null) {
			List<SysRoleFuncModel> list = sysRoleModel.getFuncList();
			SysRoleFuncModel sysRoleFuncModel = new SysRoleFuncModel();
			sysRoleFuncModel.setRoleId(sysRoleModel.getRoleId());
			sysRoleFuncModel.setCreatedBy(sysRoleModel.getCreatedBy());
			sysRoleFuncModel.setLastUpdatedBy(sysRoleModel.getLastUpdatedBy());
			sysRoleFuncModel.setFuncList(list);
			sysRoleFuncMapper.insertAll(sysRoleFuncModel);
		}
		sysRoleModel.setOperationContent("sysRole-insert-sysRole-insert-ADD-SYS_ROLE:" + sysRoleModel.opeartionContent());
		return result;
	}

	@Override
	@OperationLog(operation = Constants.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object update(SysRoleModel sysRoleModel) throws SysException {
		sysRoleModel.setOperationContent("sysRole-update-sysRole-update-MODIFY-SYS_ROLE:" + sysRoleModel.opeartionContent());
		String msg = check(sysRoleModel);
		if (StringUtils.isNotBlank(msg)) {
			throw new SysException(msg);
		}
		List<SysRoleFuncModel> funcList = sysRoleModel.getFuncList();
		Integer result = 0;
		if (funcList != null && funcList.size() > 0) {
			List<SysRoleFuncModel> insertList = new ArrayList<SysRoleFuncModel>();
			List<SysRoleFuncModel> updateList = new ArrayList<SysRoleFuncModel>();
			for (SysRoleFuncModel item : funcList) {
				if (item.getRoleId() == null) {
					insertList.add(item);
				} else {
					updateList.add(item);
				}
			}
			SysRoleFuncModel sysRoleFuncModel = new SysRoleFuncModel();
			sysRoleFuncModel.setLastUpdatedBy(sysRoleModel.getLastUpdatedBy());
			if (insertList != null && insertList.size() > 0) {
				sysRoleFuncModel.setCreatedBy(sysRoleModel.getCreatedBy());
				sysRoleFuncModel.setRoleId(sysRoleModel.getRoleId());
				sysRoleFuncModel.setFuncList(insertList);
				sysRoleFuncMapper.insertAll(sysRoleFuncModel);
			}

			if (updateList != null && updateList.size() > 0) {
//				sysRoleFuncModel.setFuncList(updateList);
//				sysRoleFuncMapper.updateAll(sysRoleFuncModel);
				for(SysRoleFuncModel model : updateList) {
					model.setLastUpdatedBy(getUserId());
					result = sysRoleFuncMapper.update(model);
					if (result != 1) {
						throw new SysException("SQL_ERR_UPDATE");
					}
				}
			}
		}
		result = sysRoleMapper.onlyDispSeq(sysRoleModel);
		if (result != 0) {
			throw new SysException("seq_cannot_be_repeated");
		}
		result = sysRoleMapper.onlyRoleCode(sysRoleModel);
		if (result != 0) {
			throw new SysException("ROLE_CODE_CANNOT_BE_DUPLICATE");
		}
		result = sysRoleMapper.update(sysRoleModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_UPDATE");
		}
		return result;
	}

	@Override
	@OperationLog(operation = Constants.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public Object delete(SysRoleModel sysRoleModel) throws SysException {
		sysRoleModel.setOperationContent("sysRole-delete-sysRole-delete-DELETE-SYS_ROLE:" + sysRoleModel.opeartionContent());
		SysUserRoleModel sysUserRoleModel = new SysUserRoleModel();
		sysUserRoleModel.setRoleId(sysRoleModel.getRoleId());
		Integer result = sysUserRoleMapper.searchBySysRoleId(sysUserRoleModel);
		if (result > 0) {
			throw new SysException("ROLES_CANNOT_BE_REPEATED");
		}
		SysRoleFuncModel sysRoleFuncModel = new SysRoleFuncModel();
		sysRoleFuncModel.setRoleId(sysRoleModel.getRoleId());
		sysRoleFuncMapper.deleteBySysRoleId(sysRoleFuncModel);
		result = sysRoleMapper.delete(sysRoleModel);
		if (result != 1) {
			throw new SysException("SQL_ERR_DELETE");
		}
		return result;
	}

	/**
	 * 
	 * @param funcList
	 * @return
	 */
	public List<SysRoleFuncModel> menuFormatData(List<SysRoleFuncModel> funcList, boolean addState) {
		List<SysRoleFuncModel> menuList = new ArrayList<SysRoleFuncModel>();
		if (CollectionUtils.isNotEmpty(funcList)) {
			List<SysRoleFuncModel> menuChildren = new ArrayList<SysRoleFuncModel>();
			List<SysRoleFuncModel> funcChildren = new ArrayList<SysRoleFuncModel>();
			for (SysRoleFuncModel menu : funcList) {
				if ("MENU".equals(menu.getType())) {
					menu.setIden(menu.getFuncId());
					menu.setActiveInd(addState ? "N" : menu.getActiveInd());
					menuChildren = new ArrayList<>();
					for (SysRoleFuncModel func : funcList) {
						if (menu.getFuncId().equals(func.getParentId()) && "FUNC".equals(func.getType())) {
							func.setIden(func.getFuncId());
							func.setActiveInd(addState ? "N" : func.getActiveInd());
							funcChildren = new ArrayList<>();
							for (SysRoleFuncModel subfunc : funcList) {
								if (func.getFuncId().equals(subfunc.getParentId())
										&& "SUBFUNC".equals(subfunc.getType())) {
									subfunc.setIden(subfunc.getFuncId());
									subfunc.setActiveInd(addState ? "N" : subfunc.getActiveInd());
									subfunc.setChildren(null);
									funcChildren.add(subfunc);
								}
							}
							if (CollectionUtils.isNotEmpty(funcChildren)) {
								func.setChildren(funcChildren);
							}
							menuChildren.add(func);
						}
					}
					if (CollectionUtils.isNotEmpty(menuChildren)) {
						menu.setChildren(menuChildren);
					}
					menuList.add(menu);
				}
			}
		}
		return menuList;
	}

	@Override
	public List<SysRoleFuncModel> searchRoleFunc(SysRoleFuncModel sysRoleFuncModel) {
		return menuFormatData(sysRoleFuncMapper.searchRoleFunc(sysRoleFuncModel), false);
	}

	@Override
	public List<SysRoleFuncModel> searchAllFunc(SysRoleFuncModel sysRoleFuncModel) {
		return menuFormatData(sysRoleFuncMapper.searchAllFunc(sysRoleFuncModel), true);
	}
	
	@Override
	public List<SysRoleFuncModel> searchRoleFuncByUserCode(SysUserModel model) {
		return sysRoleFuncMapper.searchRoleFuncByUserCode(model);
	}

	@Override
	public List<SysRoleFuncModel> searchFunc(SysRoleFuncModel sysRoleFuncModel) {
		return sysRoleFuncMapper.searchFunc(sysRoleFuncModel);
	}

	@Override
	public Integer searchByFuncId(Integer funcId) {
		return sysRoleFuncMapper.searchByFuncId(funcId);
	}

	@Override
	public List<SysRoleFuncModel> searchUserPower(SysRoleFuncModel sysRoleFuncModel) {
		return sysRoleFuncMapper.searchUserPower(sysRoleFuncModel);
	}

	public String check(SysRoleModel sysRoleModel) {
		String msg = "";
		// if (StringUtils.isBlank(sysRoleModel.getCompanyId())) {
		// msg += "COMPANY_CANNOT_BE_EMPTY;";
		// }
		if (StringUtils.isBlank(sysRoleModel.getRoleCode())) {
			msg += "ROLE_CODE_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysRoleModel.getRoleName())) {
			msg += "ROLE_NAME_CANNOT_BE_EMPTY;";
		}
		if (StringUtils.isBlank(sysRoleModel.getActiveInd())) {
			msg += "STATUS_CANNOT_BE_EMPTY;";
		}
		if (sysRoleModel.getDispSeq() == null) {
			msg += "seq_cannot_be_empty;";
		}
		return msg;
	}
}
