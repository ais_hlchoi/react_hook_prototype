package com.ais.sys.model;

import java.util.List;
import com.ais.framework.model.CommonParentModel;

public class SysRoleFuncModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	private Integer roleFuncId;

	private Integer roleId;

	private Integer funcId;

	private String canSelect;

	private String canInsert;

	private String canUpdate;

	private String canDelete;

	private String canAudit;

	private String canView;

	private String canUpload;

	private String canDownload;

	private String activeInd;

	private String funcName;

	private String funcCode;

	private String funcUrl;

	private String type;

	private Integer parentId;
	
	private int dispSeq;

	private List<SysRoleFuncModel> funcList;

	private Integer iden;
	
	private List<SysRoleFuncModel> children;

	public Integer getRoleFuncId() {
		return roleFuncId;
	}

	public void setRoleFuncId(Integer roleFuncId) {
		this.roleFuncId = roleFuncId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getFuncId() {
		return funcId;
	}

	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}

	public String getCanSelect() {
		return canSelect;
	}

	public void setCanSelect(String canSelect) {
		this.canSelect = canSelect;
	}

	public String getCanInsert() {
		return canInsert;
	}

	public void setCanInsert(String canInsert) {
		this.canInsert = canInsert;
	}

	public String getCanUpdate() {
		return canUpdate;
	}

	public void setCanUpdate(String canUpdate) {
		this.canUpdate = canUpdate;
	}

	public String getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(String canDelete) {
		this.canDelete = canDelete;
	}

	public String getCanAudit() {
		return canAudit;
	}

	public void setCanAudit(String canAudit) {
		this.canAudit = canAudit;
	}

	public String getCanView() {
		return canView;
	}

	public void setCanView(String canView) {
		this.canView = canView;
	}

	public String getCanUpload() {
		return canUpload;
	}

	public void setCanUpload(String canUpload) {
		this.canUpload = canUpload;
	}

	public String getCanDownload() {
		return canDownload;
	}

	public void setCanDownload(String canDownload) {
		this.canDownload = canDownload;
	}

	public String getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getDispSeq() {
		return dispSeq;
	}

	public void setDispSeq(int dispSeq) {
		this.dispSeq = dispSeq;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<SysRoleFuncModel> getFuncList() {
		return funcList;
	}

	public void setFuncList(List<SysRoleFuncModel> funcList) {
		this.funcList = funcList;
	}

	public String getFuncCode() {
		return funcCode;
	}

	public void setFuncCode(String funcCode) {
		this.funcCode = funcCode;
	}

	public String getFuncUrl() {
		return funcUrl;
	}

	public void setFuncUrl(String funcUrl) {
		this.funcUrl = funcUrl;
	}

	public Integer getIden() {
		return iden;
	}

	public void setIden(Integer iden) {
		this.iden = iden;
	}

	public List<SysRoleFuncModel> getChildren() {
		return children;
	}

	public void setChildren(List<SysRoleFuncModel> children) {
		this.children = children;
	}

}
