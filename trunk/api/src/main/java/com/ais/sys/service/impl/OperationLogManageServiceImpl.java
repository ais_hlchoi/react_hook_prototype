package com.ais.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.OperationLogManageMapper;
import com.ais.sys.model.OperationLogManageModel;
import com.ais.sys.service.OperationLogManageService;

@Service
public class OperationLogManageServiceImpl extends CommonParent implements OperationLogManageService {
	
	@Autowired
	private OperationLogManageMapper operationLogManageMapper;

	@Override
	public List<OperationLogManageModel> search(OperationLogManageModel model) {
		return operationLogManageMapper.search(model);
	}
	
	@Override
	public List<OperationLogManageModel> getLoginActivity(OperationLogManageModel model) {
		return operationLogManageMapper.getLoginActivity(model);
	}
	
	@Override
	public int insert(OperationLogManageModel model) {
		operationLogManageMapper.insert(model);
		return model.getOperationLogId();
	}
	
	@Override
	public int update(OperationLogManageModel model) {
		operationLogManageMapper.update(model);		
		return model.getOperationLogId();
	}

}
