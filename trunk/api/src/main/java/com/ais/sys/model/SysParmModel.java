package com.ais.sys.model;

import com.ais.framework.model.CommonParentModel;

public class SysParmModel extends CommonParentModel {
	private static final long serialVersionUID = 1L;
	private String id;
	private String segment;
	private String segmentName;
	private String code;
	private String shortDesc;
	private String longDesc;
	private String shortDescTc;
	private String longDescTc;
	private String parmValue;
	private int dispSeq;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getSegment() {
		return segment;
	}
	
	public void setSegment(String segment) {
		this.segment = segment;
	}
	
	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public String getParmValue() {
		return parmValue;
	}
	
	public void setParmValue(String parmValue) {
		this.parmValue = parmValue;
	}
	
	public int getDispSeq() {
		return dispSeq;
	}
	
	public void setDispSeq(int dispSeq) {
		this.dispSeq = dispSeq;
	}

	public String getShortDescTc() {
		return shortDescTc;
	}

	public String getLongDescTc() {
		return longDescTc;
	}

	public void setShortDescTc(String shortDescTc) {
		this.shortDescTc = shortDescTc;
	}

	public void setLongDescTc(String longDescTc) {
		this.longDescTc = longDescTc;
	}
	
	public String opeartionContent() {
		return "{id:" + id + "; segment:" + segment + "; code:" + code + "; shortDesc=" + shortDesc + "; longDesc:" + longDesc +
			"; dispSeq:" + dispSeq + "; shortDescTc:" + shortDescTc + "; longDescTc:" + longDescTc + "; parmValue:" + parmValue + "}";
	}
}
