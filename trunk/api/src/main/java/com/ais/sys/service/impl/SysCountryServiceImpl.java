package com.ais.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.framework.model.CommonParent;
import com.ais.sys.dao.SysCountryMapper;
import com.ais.sys.model.SysCountryModel;
import com.ais.sys.service.SysCountryService;

/**
 * 用户相关
 */
@Service
public class SysCountryServiceImpl extends CommonParent implements SysCountryService {

	@Autowired
	private SysCountryMapper countryMapper;

	@Override
	public List<SysCountryModel> getCountryDownList() {
		return countryMapper.getCountryDownList();
	}


}
