package com.ais.sys.service;

import java.util.List;

import com.ais.framework.exception.SysException;
import com.ais.sys.model.SysFuncModel;

public interface SysFuncService {

	List<SysFuncModel> search(SysFuncModel sysFuncModel);
	
	List<SysFuncModel> leftMenuSearch(SysFuncModel sysFuncModel);

	List<SysFuncModel> searchPreferenceMenu(SysFuncModel sysFuncModel);

	List<SysFuncModel> searchFunc(SysFuncModel sysFuncModel);

	Object insert(SysFuncModel sysFuncModel) throws SysException;

	Object update(SysFuncModel sysFuncModel) throws SysException;

	Object delete(SysFuncModel sysFuncModel) throws SysException;
}
