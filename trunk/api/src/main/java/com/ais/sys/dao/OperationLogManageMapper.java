package com.ais.sys.dao;

import java.util.List;


import com.ais.sys.model.OperationLogManageModel;

public interface OperationLogManageMapper {
	
	List<OperationLogManageModel> search(OperationLogManageModel model);
	
	List<OperationLogManageModel> getLoginActivity(OperationLogManageModel model);

	void insert(OperationLogManageModel model);
	
	void update(OperationLogManageModel model);
}
