package com.ais.framework.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

import io.jsonwebtoken.ExpiredJwtException;

@Configuration
@CrossOrigin
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationProvider provider;// Custom Define AuthenticationProvider
    
    @Autowired
    private JwtUserDetailsService UserDetailsService;
    
    @Autowired
    private JwtAuthenticationEntryPoint authenticationEntryPoint;

    @Bean
    public PasswordEncoder myPasswordEncoder() {
        return new JwtPasswordEncoder();//Custom Define Encryption Tool
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception, ExpiredJwtException {
        http
        .cors()
        .and()
        .formLogin().loginProcessingUrl("/login")
        .and()
        .csrf().disable()
        .authorizeRequests()
        .anyRequest().authenticated()
        .and()
        .addFilter(new JwtLoginFilter(authenticationManager()))
        .addFilter(new JwtAuthenticationFilter(authenticationManager()));
        //.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception, ExpiredJwtException {
        auth.authenticationProvider(provider);
        auth.userDetailsService(UserDetailsService);
    }
    /*@Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager iud = new InMemoryUserDetailsManager();
        Collection<GrantedAuthority> adminAuth = new ArrayList<>();
        
        List<SysUserDetailModel> userDetailModel = userService.getAllUserDetail();
        
        adminAuth.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        iud.createUser(new User("admin", "admin", adminAuth));
        return iud;
    }*/
}