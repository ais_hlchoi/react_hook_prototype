package com.ais.framework.core;  
  
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ais.util.InternationalizationUtil;
import com.alibaba.fastjson.support.spring.FastJsonJsonView;

/** 
 * 全局异常处理 
 */  
public class FrameworkExceptionHandler implements HandlerExceptionResolver {  
	
	private Logger logger = Logger.getLogger(getClass());
	
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception ex) {  
        ModelAndView mv = new ModelAndView();  
        FastJsonJsonView view = new FastJsonJsonView();  
        Map<String, Object> attributes = new HashMap<String, Object>();  
        if (ex instanceof UnauthenticatedException) {  
            attributes.put("code", "1000001");  
            attributes.put("msg", "token error");  
        } else if (ex instanceof UnauthorizedException) {  
            attributes.put("code", "1000002");  
            attributes.put("msg", "access control not found");  
        } else {
    		String err = ex.getMessage();
    		if (StringUtils.isBlank(err)) {
    			err = "SYS_ERR";
    		}
        	err = InternationalizationUtil.getMsg(err);
    		
        	if(StringUtils.isNotBlank(err)) {
    			int index = err.indexOf("org.hibernate.service.spi.ServiceException:");
    			if(index > 0) {
    				String oracleError = err.substring(index, err.length());
    				if(StringUtils.isNotBlank(oracleError)) {
    					index = oracleError.indexOf(":");
    					if(index > 0) {
    						oracleError = oracleError.substring(index, oracleError.length());
    					}
    					err = oracleError;
    				}
    			}
    		}
    		if(StringUtils.isNotBlank(err)) {
    			int index = err.indexOf("nested exception is");
    			if(index > 0) {
    				String oracleError = err.substring(index, err.length());
    				if(StringUtils.isNotBlank(oracleError)) {
    					index = oracleError.indexOf(":");
    					if(index > 0) {
    						oracleError = oracleError.substring(index+1, oracleError.length());
    					}
    					err = oracleError;
    				}
    			}
    		}

        	attributes.put("responseCode", "1000003"); 
    		attributes.put("responseDesc", err); 
            logger.error(err, ex);
            ex.printStackTrace();
        }  
        view.setAttributesMap(attributes);
        mv.setView(view);  
        return mv;  
    }  
}  