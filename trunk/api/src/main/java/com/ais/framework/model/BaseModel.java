package com.ais.framework.model;

import java.util.Date;
import java.util.List;

public class BaseModel {
	
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	private String createdBy;

    private Date createdDate;

    private String lastUpdatedBy;

    private Date lastUpdatedDate;
	
	private Integer startRow;
	
	private Integer endRow;
	
	private Integer dataTotal;
	
	private Integer excelRowIndex;
	private Integer limit;
	
	//page
	private List<? extends BaseModel>  totalItems;
	private Integer pageNum;
	private Integer pageSize;
	private Integer pageStart;
	private Integer totalCount;
   
	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	
	
    public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public Integer getDataTotal() {
		return dataTotal;
	}

	public void setDataTotal(Integer dataTotal) {
		this.dataTotal = dataTotal;
	}

	public Integer getExcelRowIndex() {
		return excelRowIndex;
	}

	public void setExcelRowIndex(Integer excelRowIndex) {
		this.excelRowIndex = excelRowIndex;
	}
	
	public Integer getPageStart() {
		//return pageStart;
    	if(pageNum!=null  && pageSize!=null)
    		return  (pageNum-1) * pageSize;
    
    	return pageStart;
	}

	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}

	public List<? extends BaseModel> getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(List<? extends BaseModel> totalItems) {
		this.totalItems = totalItems;
	}

}
