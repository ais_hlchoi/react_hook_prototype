package com.ais.framework.core;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.github.pagehelper.PageInterceptor;

@Configuration
public class DataSourceConfiguration {
	
	@SuppressWarnings("unchecked")
	protected <T> T createDataSource(DataSourceProperties properties, Class<? extends DataSource> type) {
		return (T) properties.initializeDataSourceBuilder().type(type).build();
	}

	/**
	 * @see org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration.Tomcat
	 * 
	 * @param properties content of application.properties
	 *            
	 * @return DruidDataSource
	 */
	@Bean(name = "dataSourceObj")
	@ConfigurationProperties("spring.datasource")
	public DruidDataSource dataSource(DataSourceProperties properties) {

		DruidDataSource dataSource = createDataSource(properties, DruidDataSource.class);

		DatabaseDriver databaseDriver = DatabaseDriver.fromJdbcUrl(properties.determineUrl());

		String validationQuery = databaseDriver.getValidationQuery();
		if (validationQuery != null) {
			dataSource.setTestOnBorrow(true);
			dataSource.setValidationQuery(validationQuery);
		}

		return dataSource;
	}

	@Value("${mybatis.mapper-locations}")
	String mapperLocations;
	
	@Bean(name = "sqlSessionFactoryObj")
	@Primary
	public SqlSessionFactory sqlSessionFactory(@Qualifier("dataSourceObj") DataSource dataSource) throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();

		bean.setDataSource(dataSource);
		bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));

		//set PageInterceptor as plugins for SqlSessionFactory
		Interceptor[] plugins =new Interceptor[1];
		plugins[0]=new PageInterceptor();
		
		
		Properties prop=new Properties();
		prop.setProperty("reasonable", "true");
		plugins[0].setProperties(prop);
		
		bean.setPlugins(plugins);
		
		return bean.getObject();
	}

	@Bean(name = "transactionManagerObj")
	@Primary
	public DataSourceTransactionManager transactionManager(@Qualifier("dataSourceObj") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "sqlSessionTemplateObj")
	@Primary
	public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryObj") SqlSessionFactory sqlSessionFactory)
			throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

}
