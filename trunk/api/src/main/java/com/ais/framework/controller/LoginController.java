package com.ais.framework.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.service.SysUserService;

@RestController
@CrossOrigin
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private SysUserService userService;

    @RequestMapping(value = "/checklogin", method = RequestMethod.POST)
    public Object checktoken(HttpServletRequest request, HttpServletResponse response,
                                          HttpSession session) throws Exception {
//        if (StringUtils.isBlank(userInfo.getUserCode())) {
//            return RestEntity.failed("PLEASE_USER_NUMBER;");
//        }
//        if (StringUtils.isBlank(userInfo.getPwd())) {
//            return RestEntity.failed("PLEASE_PASSWORD;");
//        }
//        UserModel userModel = userService.findByUserCode(userInfo.getUserCode());
//        if (userModel == null) {
//            return RestEntity.failed("USER_NOT_EXIST;");
//        }else if("N".equals(userModel.getActiveInd())) {
//            return RestEntity.failed("ACCOUNT_BEEN_FROZEN;");
//        }

//        HttpSession loginSession = session;
//        loginSession.setAttribute("userModel", userModel);
//        loginSession.setMaxInactiveInterval(60 * 60 * 4);// Session expired after 4 hours
//        MySessionContext.AddSession(loginSession);// 添加session
//        SysUserModel userModel = userService.searchByUserCode("admin");// Check User by Code

//        Map<String, Object> result = new HashMap<String, Object>();
//        result.put("userInfo", userModel);
//        result.put("token", session.getId());
//        return RestEntity.success(result);
//        Map<String, String> temp = new HashMap<String, String>();
//        temp.put("msg", "SUCCESS");
//        return temp;
    	return RestEntity.success();
    }

    @RequestMapping(value = "/checkToken")
    public Object checkToken() {
//        SysUserModel userModel = userService.searchByUserCode("admin");// 
//        return RestEntity.success(userModel);
//
//        if (getUserInfo() != null) {
//            UserModel userInfo = getUserInfo();
//            UserModel userModel = userService.searchByUserId(userInfo.getUserId());// Check User By ID
//            if (userModel == null) {
//                return RestEntity.failed();
//            }
//            return RestEntity.success(getUserInfo());
//        } else {
//            return RestEntity.failed();
//        }
        return RestEntity.success();
    }
//
//    public UserModel getUserInfo() {
//        String token = request.getHeader(TOKEN_KEY);
//        if(StringUtils.isNotBlank(token)) {
//            HttpSession session = MySessionContext.getSession(token);
//            if (session != null) {
//                return (UserModel) session.getAttribute("userModel");
//            }
//        }
//        return null;
//    }


}
