package com.ais.framework.jwt;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysUserDetailModel;
import com.ais.sys.service.OperationLogManageService;
import com.ais.sys.service.SysUserService;
import com.ais.util.OperationLogUtil;
import com.ais.util.PBKDF2Encryption;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

	@Autowired
    private OperationLogManageService operationLogManageService;
	
    @Autowired
    private JwtUserDetailsService userDetailsService;
    
    @Autowired
    private SysUserService userService;
    
    @Autowired
    private HttpServletRequest request;

    /**
     * custom authenticate method
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return userValidation(authentication);
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
    
    private Authentication userValidation(Authentication authentication) throws AuthenticationException{
    	RestEntity response = null;
    	Integer logId = operationLogManageService.insert(OperationLogUtil.genLog(request, null, null, OperationLogUtil.getRealIp(request), "LOGIN", "", "", null));
    	String username = authentication.getName();        
        List<SysUserDetailModel> userModel = userService.getUserInfo(username);
        
        if (userModel.get(0) == null) {
        	response = RestEntity.failed("User not found.");
        	operationLogManageService.update(OperationLogUtil.genLog(request, null, null, OperationLogUtil.getRealIp(request), request.getRequestURL().toString(), response.getResponseCode(), response.getResponseDesc(), logId));
        	return null;
        }
        	
        
        String password = "";
		try {
			password = PBKDF2Encryption.getPBKDF2((String) authentication.getCredentials(), userModel.get(0).getSalt());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			response = RestEntity.failed("Invalid password.");
			operationLogManageService.update(OperationLogUtil.genLog(request, null, null, OperationLogUtil.getRealIp(request), request.getRequestURL().toString(), response.getResponseCode(), response.getResponseDesc(), logId));
			return null;
		}        
        
        UserDetails user = userDetailsService.loadUserByUsername(username);
        
        if(user.getPassword().equals(password)) {
        	UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(username, null, user.getAuthorities());
            response = RestEntity.success(result);
            operationLogManageService.update(OperationLogUtil.genLog(request, null, null, OperationLogUtil.getRealIp(request), request.getRequestURL().toString(), response.getResponseCode(), response.getResponseDesc(), logId));
            return result;
        }
        
        response = RestEntity.failed("Invalid password.");
        operationLogManageService.update(OperationLogUtil.genLog(request, null, null, OperationLogUtil.getRealIp(request), request.getRequestURL().toString(), response.getResponseCode(), response.getResponseDesc(), logId));
        return null;
    }
}
