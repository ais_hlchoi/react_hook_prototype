package com.ais.framework.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysRoleFuncModel;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.service.SysRoleService;

@RestController
@CrossOrigin
@RequestMapping("/api/menu")
public class MenuController {
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@RequestMapping(value = "/getsidemenu", method = RequestMethod.POST)
	public RestEntity getSideMenu(HttpServletRequest request, HttpServletResponse res, @RequestBody SysUserModel model) throws Exception {
		RestEntity response = null;
		List<SysRoleFuncModel> resultModel = null;
		
		try {
			resultModel = sysRoleService.searchRoleFuncByUserCode(model);
			response = RestEntity.success(resultModel);
		}catch(Exception ex) {
			System.out.println(ex.toString());
		}
		
		return response;
	}
}
