package com.ais.framework.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.shiro.entity.RestEntity;
import com.ais.sys.model.SysUserModel;
import com.ais.sys.service.SysUserService;

@RestController
@CrossOrigin
@RequestMapping("/api/user/")
public class UserController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
    public Object getUserInfo(HttpServletRequest request, HttpServletResponse response, @RequestBody SysUserModel model) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        
    	List<SysUserModel> userRoleList = sysUserService.searchUserRoleByUserCode(model.getUserCode());
    	if(CollectionUtils.isNotEmpty(userRoleList)) {
    		boolean flag = true;
    		for(SysUserModel userRoleModel : userRoleList) {
    			if("Y".equals(userRoleModel.getDefaultInd())) {
    		        result.put("userInfo", userRoleModel);
    		        flag = false;
    		        break;
    			}
    		}
    		if(flag) {
		        result.put("userInfo", userRoleList.get(0));
    		}
    	}
    	
        return RestEntity.success(result);
    }

}
