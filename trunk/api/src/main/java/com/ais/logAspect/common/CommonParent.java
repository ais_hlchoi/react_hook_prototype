package com.ais.logAspect.common;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.ais.sys.exception.ZHException;
import com.ais.sys.model.SysUserModel;
import com.ais.framework.session.MySessionContext;

/**
 * @description:
 */
public class CommonParent {

	public final String TOKEN_KEY = "authorization";
	
	@Autowired
	public HttpSession session;

	@Autowired
	public HttpServletRequest request;

	@Autowired
	public HttpServletResponse response;

	/**
	 * 获取登录用户的userCode
	 * @return
	 */
	public String getUserId() {
		String result = null;
		SysUserModel userModel = getUserInfo();
		if (userModel != null) {
			result = userModel.getUserCode();
		}
		return result;
	}

	/**
	 * 获取用户信息，如果不存在，返回null
	 * @return
	 */
	public SysUserModel getUserInfo() {
		String token = request.getHeader(TOKEN_KEY);
		if(StringUtils.isNotBlank(token)) {
			HttpSession session = MySessionContext.getSession(token);
			if (session != null) {
				return (SysUserModel) session.getAttribute("userModel");
			}
		}
		return null;
	}
	
	/**
	 * 检查用户是否登录
	 * @return
	 */
	public SysUserModel checkLogin() {
		String token = request.getHeader(TOKEN_KEY);
		if(StringUtils.isNotBlank(token)) {
			HttpSession session = MySessionContext.getSession(token);
			if (session != null) {
				SysUserModel userModel = (SysUserModel) session.getAttribute("userModel");
				if(userModel != null) {
					return userModel;
				}
			}
		}
		throw new ZHException("LOGIN_INVALID");// 没有登录(登录失效，请重新登录)
	}
	
	/**
	 * 退出登录
	 */
	public void removeUserInfo() {
		// 清除session
		MySessionContext.DelSession(MySessionContext.getSession(request.getHeader(TOKEN_KEY)));
	}
	
	/**
	 * 獲取客戶端IP地址
	 */
    public static String getClientIPAddress(HttpServletRequest request) {
        String ip = null;
        //X-Forwarded-For：Squid 服务代理
        String ipAddresses = request.getHeader("X-Forwarded-For");
	    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            //Proxy-Client-IP：apache 服务代理
            ipAddresses = request.getHeader("Proxy-Client-IP");
        }
	    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            //WL-Proxy-Client-IP：weblogic 服务代理
            ipAddresses = request.getHeader("WL-Proxy-Client-IP");
        }
	    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            //HTTP_CLIENT_IP：有些代理服务器
            ipAddresses = request.getHeader("HTTP_CLIENT_IP");
        }
	    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            //HTTP_X_FORWARDED_FOR：有些代理服务器
            ipAddresses = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
	    if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            //X-Real-IP：nginx服务代理
            ipAddresses = request.getHeader("X-Real-IP");
        }
        //有些网络通过多层代理，那么获取到的ip就会有多个，一般都是通过逗号（,）分割开来，并且第一个ip为客户端的真实IP
        if (ipAddresses != null && ipAddresses.length() != 0) {
            ip = ipAddresses.split(",")[0];
        }
        //还是不能获取到，最后再通过request.getRemoteAddr();获取
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ip = request.getRemoteAddr();
        }
        return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
    }
    
    /**
     * 獲取服務端IP地址
     */
    public static String getServerIpAddress() {
    	String ip = "";
    	try {
			InetAddress locAdd = InetAddress.getByName("windows3.aishk.com");
			ip = locAdd.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
    	return ip;
    }

    /**
     * 根据方法和传入的参数获取请求参数
     */
    protected Object getParameter(Method method, Object[] args) {
        List<Object> argList = new ArrayList<>();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            //将RequestBody注解修饰的参数作为请求参数
            RequestBody requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (requestBody != null) {
                argList.add(args[i]);
            }
            //将RequestParam注解修饰的参数作为请求参数
            RequestParam requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (requestParam != null) {
                Map<String, Object> map = new HashMap<>();
                String key = parameters[i].getName();
                if (!StringUtils.isEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        if (argList.size() == 0) {
            return null;
        } else if (argList.size() == 1) {
            return argList.get(0);
        } else {
            return argList;
        }
    }

}
