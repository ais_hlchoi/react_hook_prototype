package com.ais.logAspect.common;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import com.ais.framework.model.CommonExceptionHandling;
import com.ais.framework.model.CommonParent;
import com.ais.logAspect.annotation.OperationLog;
import com.ais.sys.model.OperationLogModel;
import com.ais.sys.service.OperationLogService;
import com.alibaba.fastjson.JSON;

/**
 * Centralize user request, and log down all activity 
 * 
 * @Aspect： define interface
 * @Before： before controller method
 * @After： after controller method
 * @AfterReturning： after controller method success
 * @AfterThrowing： after controller throw exception
 * @Around： before and after
 * @Pointcut： define point cut expression
 */
@RestController
@Aspect
@Component
@Order(2)
public class OperationLogAspect extends CommonParent {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private OperationLogService operationLogService;

	@Pointcut(value = "@annotation(com.ais.logAspect.annotation.OperationLog)")
	public void operationLog() {
	}

	@Before("operationLog()")
	public void doBefore(JoinPoint joinPoint) throws Throwable {
	}

	@AfterReturning(value = "operationLog()", returning = "ret")
	public void doAfterReturning(Object ret) throws Throwable {
	}

	@Around(value = "operationLog() && @annotation(com.ais.logAspect.annotation.OperationLog)")
	public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
		OperationLogModel operationLogModel = new OperationLogModel(new Date(), getClientIPAddress(request));
//		if (getUserInfo() != null) {
//			SysUserModel userModel = getUserInfo();
//			operationLogModel.setUserId(userModel.getUserId()); 
//		}
		Signature signature = joinPoint.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		Method method = methodSignature.getMethod();
		if (method.isAnnotationPresent(OperationLog.class)) {
			OperationLog operationLog = method.getAnnotation(OperationLog.class);
			operationLogModel.setOperationType(operationLog.operation());
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			Object result = joinPoint.proceed();
			resultMap.put("result", result);
			resultMap.put("msg", "SUCCESS");
			operationLogModel.setStatus("SUCCESS");
		} catch (Exception e) {
			operationLogModel.setStatus("FAIL");
			operationLogModel.setResponseDesc(CommonExceptionHandling.formatError(e.getMessage()));
			resultMap.put("msg", operationLogModel.getResponseDesc());
		}

		Object object = joinPoint.getArgs()[0];
		Map<String, Object> map = JSON.parseObject(JSON.toJSONString(object));
		if (map.get("userId") != null) {
			operationLogModel.setUserId(Integer.parseInt(map.get("userId").toString()));
		}
		if (map.get("menuFuncId") != null) {
			operationLogModel.setMenuFuncId(Integer.parseInt(map.get("menuFuncId").toString()));
		}
		if (map.get("operationContent") != null) {
			operationLogModel.setOperationContent(map.get("operationContent").toString());
		}
		operationLogModel.setResponseTime(new Date());
		logger.info("---------AOP-operation---------" + JSON.toJSONString(operationLogModel));
		operationLogService.insert(operationLogModel);
		return resultMap;
	}

}
