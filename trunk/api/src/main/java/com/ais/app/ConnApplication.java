package com.ais.app;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@EnableTransactionManagement
@EnableAutoConfiguration 
@MapperScan({"com.ais.conn.dao","com.ais.sys.dao"})//packages mybatis will scan for xml-java mapper
@ComponentScan(basePackages = {"com.ais"})//packages spring will map scan for DI
@SpringBootApplication
@Configuration
public class ConnApplication {
	static Logger logger = LogManager.getLogger(ConnApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ConnApplication.class, args);
		logger.info("ヾ(◍°∇°◍)ﾉﾞ    bootdo startup     ヾ(◍°∇°◍)ﾉﾞ\n" +
                " ______                    _   ______            \n" +
                "|_   _ \\                  / |_|_   _ `.          \n" +
                "  | |_) |   .--.    .--. `| |-' | | `. \\  .--.   \n" +
                "  |  __'. / .'`\\ \\/ .'`\\ \\| |   | |  | |/ .'`\\ \\ \n" +
                " _| |__) || \\__. || \\__. || |, _| |_.' /| \\__. | \n" +
                "|_______/  '.__.'  '.__.' \\__/|______.'  '.__.'  ");
	}

}
