<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.ais.conn.dao.PaymentOrderModelMapper">

    <resultMap id="PaymentOrderModel" type="com.ais.conn.model.PaymentOrderModel">
        <result column="ID" 							property="id" />
        <result column="ORDER_DATE" 					property="orderDate" />
        <result column="ORDER_DATE_NO_TIME" 			property="orderDateWithoutTime" />
        <result column="SUB_ORDER_NUMBER" 				property="subOrderNumber" />
        <result column="SKU_ID" 						property="skuId" />
        <result column="SKU_DESC" 						property="skuDescription" />
        <result column="ORDER_NUMBER" 					property="orderNumber" />
        <result column="CONSIGNMENT_ORDER" 				property="consignmentOrder" />
        <result column="COMPLETION_DATE"				property="completionDate" />
        <result column="QUANTITY" 						property="quantity" />
        <result column="UNIT_COST" 						property="unitCost" />
        <result column="DISCOUNT" 						property="discount" />
        <result column="TOTAL_COST" 					property="totalCost" />
        <result column="STORE_CREDIT" 					property="storeCredit" />
        <result column="NET_COST" 						property="netCost" />
        <result column="NET_AMOUNT" 					property="netAmount" />
        <result column="COMM_CHARGE_RATE" 				property="commChargeRate" />
        <result column="COMM_CHARGE_AMT" 				property="commChargeAmt" />
        <result column="STORAGE_CHARGE" 				property="storageCharge" />
        <result column="CREATED_ORDER_UPLOAD_ID" 		property="createdOrderUploadId" />
        <result column="LAST_UPDATED_ORDER_UPLOAD_ID" 	property="lastUpdatedOrderUploadId" />
    </resultMap>

    <resultMap id="PaymentOrderStatModel" type="com.ais.conn.model.PaymentOrderStatModel">
        <result column="DATE" 							property="date" />
        <result column="TOTAL_QUANTITY" 				property="totalQuantity" />
        <result column="TOTAL_AMT" 						property="totalAmt" />        
        <result column="SKU_ID" 						property="skuId" />
        <result column="SKU_DESC" 						property="skuDesc" />
    </resultMap>

    <resultMap id="SkuQuantityModel" type="com.ais.conn.model.SkuQuantityModel">
        <result column="SKU_ID" 						property="skuId" />
        <result column="SKU_DESC" 						property="skuDesc" />
        <result column="TOTAL_QUANTITY" 				property="totalQuantity" />
        <result column="TOTAL_AMT" 						property="totalAmt" />
    </resultMap>

	

    <select id="searchPaymentOrder" resultMap="PaymentOrderModel" parameterType="com.ais.conn.model.SearchPaymentOrderRecordCriteriaModel">
        SELECT DATE(PAYMENT_ORDER.ORDER_DATE) AS ORDER_DATE_NO_TIME, PAYMENT_ORDER.* FROM PAYMENT_ORDER
        WHERE 1
        <if test="defaultSearchText != null and defaultSearchText != ''">
            AND (ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SUB_ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SKU_ID LIKE '%${defaultSearchText}%' OR
            SKU_DESC LIKE '%${defaultSearchText}%')
        </if>
        <if test="orderDateFrom != null and orderDateFrom != ''">
            AND ORDER_DATE >= #{orderDateFrom}
        </if>
        <if test="orderDateTo != null and orderDateTo != ''">
            AND ORDER_DATE &lt;= #{orderDateTo}
        </if>
        <if test="orderNumber != null and orderNumber != ''">
            AND ORDER_NUMBER LIKE '%${orderNumber}%'
        </if>
        <if test="subOrderNumber != null and subOrderNumber != ''">
            AND SUB_ORDER_NUMBER LIKE '%${subOrderNumber}%'
        </if>
        <if test="skuId != null and skuId != ''">
            AND SKU_ID LIKE '%${skuId}%'
        </if>
        <if test="skuDesc != null and skuDesc != ''">
            AND SKU_DESC LIKE '%${skuDesc}%'
        </if>
    </select>

    <select id="getDailyTotalQuantity" resultMap="PaymentOrderStatModel" parameterType="com.ais.conn.model.PaymentOrderStatModel">
        SELECT b.date as DATE, b.sku_id as SKU_ID, b.sku_desc as SKU_DESC, IFNULL(SUM(c.quantity), 0) AS TOTAL_QUANTITY, IFNULL(SUM(c.net_amount), 0) AS TOTAL_AMT
        FROM
            (select date, sku_id, sku_desc
                FROM PAYMENT_ORDER
                CROSS JOIN (SELECT DATE(order_date) as date
                    FROM payment_order
                    <if test="skuIdList != null">
                	    WHERE SKU_ID IN (<foreach collection="skuIdList" item="skuId" separator=",">#{skuId}</foreach>)
                	</if>
                    GROUP BY DATE(order_date), SKU_ID, SKU_DESC) as a
            <if test="skuIdList != null">
            	WHERE SKU_ID in (<foreach collection="skuIdList" item="skuId" separator=",">#{skuId}</foreach>)
            </if>
            GROUP BY DATE, SKU_ID, SKU_DESC) as b
            -- b is combination of all available date and sku_id for top 5 quantity sku_id
        LEFT JOIN payment_order c ON b.sku_id = c.sku_id AND b.date = DATE(c.order_date)
        GROUP BY b.date, b.sku_id, b.sku_desc
        HAVING 1
        <if test="orderDateFrom != null and orderDateFrom != ''">
            AND b.date >= #{orderDateFrom}
        </if>
        <if test="orderDateTo != null and orderDateTo != ''">
            AND b.date &lt;= #{orderDateTo}
        </if>
        ORDER BY b.date
    </select>
    
    <select id="getDailyTotalAmt" resultMap="PaymentOrderStatModel" parameterType="com.ais.conn.model.PaymentOrderStatModel">
        SELECT b.date as DATE, b.sku_id as SKU_ID, b.sku_desc as SKU_DESC, IFNULL(SUM(c.net_amount), 0) AS TOTAL_AMT
        FROM
            (select date, sku_id, sku_desc
                FROM PAYMENT_ORDER
                CROSS JOIN (SELECT DATE(order_date) as date
                    FROM payment_order
                    <if test="skuIdList != null">
                	    WHERE SKU_ID IN (<foreach collection="skuIdList" item="skuId" separator=",">#{skuId}</foreach>)
                	</if>
                    GROUP BY DATE(order_date), SKU_ID, SKU_DESC) as a
            <if test="skuIdList != null">
            	WHERE SKU_ID in (<foreach collection="skuIdList" item="skuId" separator=",">#{skuId}</foreach>)
            </if>
            GROUP BY DATE, SKU_ID, SKU_DESC) as b
            -- b is combination of all available date and sku_id for top 5 quantity sku_id
        LEFT JOIN payment_order c ON b.sku_id = c.sku_id AND b.date = DATE(c.order_date)
        GROUP BY b.date, b.sku_id, b.sku_desc
        HAVING 1
        <if test="orderDateFrom != null and orderDateFrom != ''">
            AND b.date >= #{orderDateFrom}
        </if>
        <if test="orderDateTo != null and orderDateTo != ''">
            AND b.date &lt;= #{orderDateTo}
        </if>
        ORDER BY b.date
    </select>

    <select id="getTop5SkuQuantity" resultMap="SkuQuantityModel" parameterType="com.ais.conn.model.SkuQuantityModel">
        SELECT SKU_ID, SKU_DESC, SUM(QUANTITY) AS TOTAL_QUANTITY FROM PAYMENT_ORDER
        WHERE 1
        <if test="defaultSearchText != null and defaultSearchText != ''">
            AND (ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SUB_ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SKU_ID LIKE '%${defaultSearchText}%' OR
            SKU_DESC LIKE '%${defaultSearchText}%')
        </if>
        <if test="orderDateFrom != null and orderDateFrom != ''">
            AND ORDER_DATE >= #{orderDateFrom}
        </if>
        <if test="orderDateTo != null and orderDateTo != ''">
            AND ORDER_DATE &lt;= #{orderDateTo}
        </if>
        <if test="orderNumber != null and orderNumber != ''">
            AND ORDER_NUMBER LIKE '%${orderNumber}%'
        </if>
        <if test="subOrderNumber != null and subOrderNumber != ''">
            AND SUB_ORDER_NUMBER LIKE '%${subOrderNumber}%'
        </if>
        <if test="skuId != null and skuId != ''">
            AND SKU_ID LIKE '%${skuId}%'
        </if>
        <if test="skuDesc != null and skuDesc != ''">
            AND SKU_DESC LIKE '%${skuDesc}%'
        </if>
		GROUP BY SKU_ID, SKU_DESC
        ORDER BY TOTAL_QUANTITY DESC
        LIMIT 5
    </select>
    
    
    <select id="getTop5SkuAmt" resultMap="SkuQuantityModel" parameterType="com.ais.conn.model.SkuQuantityModel">
        SELECT SKU_ID, SKU_DESC, SUM(NET_AMOUNT) AS TOTAL_AMT FROM PAYMENT_ORDER
        WHERE 1
        <if test="defaultSearchText != null and defaultSearchText != ''">
            AND (ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SUB_ORDER_NUMBER LIKE '%${defaultSearchText}%' OR
            SKU_ID LIKE '%${defaultSearchText}%' OR
            SKU_DESC LIKE '%${defaultSearchText}%')
        </if>
        <if test="orderDateFrom != null and orderDateFrom != ''">
            AND ORDER_DATE >= #{orderDateFrom}
        </if>
        <if test="orderDateTo != null and orderDateTo != ''">
            AND ORDER_DATE &lt;= #{orderDateTo}
        </if>
        <if test="orderNumber != null and orderNumber != ''">
            AND ORDER_NUMBER LIKE '%${orderNumber}%'
        </if>
        <if test="subOrderNumber != null and subOrderNumber != ''">
            AND SUB_ORDER_NUMBER LIKE '%${subOrderNumber}%'
        </if>
        <if test="skuId != null and skuId != ''">
            AND SKU_ID LIKE '%${skuId}%'
        </if>
        <if test="skuDesc != null and skuDesc != ''">
            AND SKU_DESC LIKE '%${skuDesc}%'
        </if>
		GROUP BY SKU_ID, SKU_DESC
        ORDER BY TOTAL_AMT DESC
        LIMIT 5
    </select>
</mapper>