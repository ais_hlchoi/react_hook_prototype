/**
 *
 * (C) 2018 The Securities And Futures Commission of Hong Kong. All rights reserved.
 *
 */
package hk.sfc.eds.profilemaintenance.security;

import org.aopalliance.intercept.MethodInvocation;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import hk.sfc.eds.common.security.GlobalMethodSecurityExpressionHandler;
import hk.sfc.eds.profilemaintenance.util.CustomApplicationContextHolder;

public class CustomMethodSecurityExpressionHandler extends GlobalMethodSecurityExpressionHandler {

  @Override
  protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
      Authentication authentication, MethodInvocation invocation) {
    CustomMethodSecurityExpressionRoot root = new CustomMethodSecurityExpressionRoot(authentication,
        CustomApplicationContextHolder.getBean(SqlSessionTemplate.class));
    root.setPermissionEvaluator(getPermissionEvaluator());
    root.setTrustResolver(this.trustResolver);
    root.setRoleHierarchy(getRoleHierarchy());
    return root;
  }

}
