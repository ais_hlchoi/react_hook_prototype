package com.dbs.test;

import com.dbs.vtsp.service.CommandConsoleService;
import com.dbs.vtsp.service.ResultFileService;
import com.dbs.vtsp.service.SourceFileService;
import com.dbs.vtsp.service.TransferFileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataExportUnitTest {

	@Autowired
	ResultFileService resultFileService;

	@Autowired
	SourceFileService sourceFileService;


	@Autowired
	TransferFileService transferFileService;

	@Value("${sh.fds.upld.txn}")
	String shDownloadFtp;


	@Autowired
	CommandConsoleService commandConsoleService;

	@Value("${folder.import}")
	String folderImport;
	

	@Test
	public void transferFile() throws Exception{
		//FTP to BOSU
		//Integer exitVal = (commandConsoleService.executeShell(shDownloadFtp));

		//BOSU to AmazonS3
		List<String> lines = Arrays.asList("1st line", "2nd line");
		File file = new File(folderImport);
		file.mkdirs();
		Files.write(Paths.get(folderImport+"S3UPLOADFILET.csv"), lines,
				StandardCharsets.UTF_8);
		transferFileService.transferFile();
	}
	


}