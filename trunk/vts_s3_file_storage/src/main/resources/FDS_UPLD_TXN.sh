#!/bin/sh
###############################################################################
# Program id : FDS_UPLD_TXN.sh
# Function   : Download Fidessa BOI files and create Operation Txn
# Written by : Tony Hui
# Date       : May 2020
#
# Parameter  : $1 = "ONLINE" if submitted via FORMS, else "DUMMY" (optional), "ALERT" : skip program will alert
#              $2 = Trade Date YYYYMMDD (optional)
# Modification history :
# --------------------
# Date         Author          Description
#20210107		Leo			concurrent running handling
# -----------------------------------------------------------------------------
###############################################################################
echo HOME=$HOME
##. $HOME/bims_env.sh
. /u05/bims/bmprod/login1/bims_env.sh

#
#
SSH_USER=svohkin1
SSH_SERVER=10.91.79.106
SSH_PATH=D:\\FidessaSFT_ATP_SG\\UAT\\MISFiles\\
#SFTP_PATH=/D:/FidessaSFT_ATP_SG/UAT/MISFiles
SFTP_PATH=/INBOUNDMIS
SAVE_FTP_PATH=/u05/bims/bmprod/exe/vts/bash
SUCCESS_FTP_PATH=/u05/bims/bmprod/exe/vts/success
#echo $FTP_ALLOC
#sshg3 ${SSH_USER}@${SSH_SERVER} "D: & cd D:/FidessaSFT_ATP_SG/UAT/MISFiles & dir" >> /u05/bims/bmprod/exe/s3_file_storage_test/abc.txt
#sshg3 ${SSH_USER}@${SSH_SERVER} "D: & cd D:/FidessaSFT_ATP_SG/UAT/MISFiles & dir USER_MARKET_ROUTES-202106021902.csv " >> /u05/bims/bmprod/exe/s3_file_storage_test/abcd.txt
#
TODAY_DATETIME=`date +%Y%m%d-%H%M%S`
TODAY_DATE=`date +%Y%m%d`
TIMESTAMP=`date "+%Y%m%d%H%M%S"`
CURRENT_TIME=$(date +%H)
#
#concurrent running handling start
DATE_TIME=`date +%H:%M:%`
#


 IMPORT_PATTERN=""
  IMPORT_PATTERN="${IMPORT_PATTERN} ALLOCATIONS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_EVENTS_EXCHANGE_ORDER-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_EVENTS_ORDER_AND_TRADE_FLOW-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_EVENTS_SOR_AUDIT-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_EVENTS_TRADE_REPORT-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_EVENTS_USER_LOGON-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ARC_SETTINGS_SERVICE_STRATEGY-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} BOOKS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} CHARGE_RULES-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} CLIENTS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} CLOSING_PRICE-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} EXCHANGE_CONNECTIONS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} FRONT_OFFICE_TRADES-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} GROUPS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} INSTRUMENTS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} LOGON_AUDIT_TRAIL-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} MARKET_SIDE_COUNTERPARTIES-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} MIS_NEW_INSTRUMENTS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} ORDER_PROGRESS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} STATIC_DATA_AUDIT-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} SUB_ACCOUNTS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} USER_EXCHANGE_CONNECTIONS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} USER_GROUP_RIGHTS-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} USER_MARKET_ROUTES-${TODAY_DATE}*.csv"
  IMPORT_PATTERN="${IMPORT_PATTERN} USERS-${TODAY_DATE}*.csv"
  #IMPORT_PATTERN="${IMPORT_PATTERN} THEIRNAME"
#  for NAME in ${IMPORT_PATTERN}; do
#    echo ${NAME};
#   done
#  echo ${IMPORT_PATTERN}
if [  "$@" != "" ] ; then
 IMPORT_PATTERN="$@" 
fi

echo $IMPORT_PATTERN





echo "Start time ($PROG) : `date`"

cd $SAVE_FTP_PATH
echo "Ftp get files" >> $LOGFILE

for NAME in ${IMPORT_PATTERN}; do

echo ${NAME};
TEMPFILE=`mktemp -p /tmp FDS_DOWNLOADVTSPXXXXXX`
cat > $TEMPFILE <<!
cd $SFTP_PATH
get ${NAME}
quit
!
echo "sftpg3" >> $LOGFILE
sftpg3 -B $TEMPFILE ${SSH_USER}@${SSH_SERVER} 2>> ${LOGFILE}
retval=$?
retval=0
if [ $retval != 0 ]; then
   echo "FDS_UPLD_TXN: Fail to sftp Fidessa files: return $retval" >> ${LOGFILE}
   echo "sftp data file has error. (FDS_UPLD_TXN.sh)"
   #send_email ${RECIPIENT} "BOSV - Fidessa MIS File Download Failed" ${LOGFILE}
   echo Remove $TEMPFILE >> ${LOGFILE}
   rm $TEMPFILE
   exit 1
fi

rm $TEMPFILE

echo "Pervious Date files" >> $LOGFILE
echo "ALLOCATIONS-${PREV_DATE}*.csv" >> $LOGFILE
#####################################################################
# File change permission
#####################################################################
chmod a+rw ${NAME}

done


#####################################################################
# File housekeeping
#####################################################################
HOUSE_KEEP_PATTERN=""
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ALLOCATIONS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_EVENTS_EXCHANGE_ORDER-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_EVENTS_ORDER_AND_TRADE_FLOW-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_EVENTS_SOR_AUDIT-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_EVENTS_TRADE_REPORT-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_EVENTS_USER_LOGON-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ARC_SETTINGS_SERVICE_STRATEGY-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} BOOKS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} CHARGE_RULES-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} CLIENTS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} CLOSING_PRICE-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} EXCHANGE_CONNECTIONS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} FRONT_OFFICE_TRADES-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} GROUPS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} INSTRUMENTS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} LOGON_AUDIT_TRAIL-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} MARKET_SIDE_COUNTERPARTIES-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} MIS_NEW_INSTRUMENTS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} ORDER_PROGRESS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} STATIC_DATA_AUDIT-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} SUB_ACCOUNTS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} USER_EXCHANGE_CONNECTIONS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} USER_GROUP_RIGHTS-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} USER_MARKET_ROUTES-${TODAY_DATE}*.csv"
 HOUSE_KEEP_PATTERN="${HOUSE_KEEP_PATTERN} USERS-${TODAY_DATE}*.csv"
  for NAME in ${HOUSE_KEEP_PATTERN}; do
    find ${SAVE_FTP_PATH} -name ${NAME} -mtime 60 -exec rm {} \;
    find ${SUCCESS_FTP_PATH} -name ${NAME} -mtime 60 -exec rm {} \;
#    find ${SAVE_FTP_PATH} -name ${NAME} -amin +30 -exec rm {} \;
#    find ${SUCCESS_FTP_PATH} -name ${NAME} -amin +30 -exec rm {} \;
   done




