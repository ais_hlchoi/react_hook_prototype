package com.dbs.vtsp.controller.external;


import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.vtsp.service.CommandConsoleService;
import com.dbs.vtsp.service.ResultFileService;
import com.dbs.vtsp.service.TransferFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());


//	@Scheduled(fixedDelay = 60000)
//	public void impAllocation() {
//	}
	@Autowired
	TransferFileService transferFileService;


	@Autowired
	CommandConsoleService commandConsoleService;

	@Value("${sh.fds.upld.txn}")
	String shDownloadFtp;


//
	@Scheduled(cron = "0 0 9,12,14,17 * * ?")
	//@Scheduled(cron = "0 */20 * * * ?") //20mins
	//@Scheduled(cron = "0 0 */1 * * ?") //every hour
	public void transferFileJob() throws Exception{

		MDC.put("cronjob","[transferFileJob]");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");


		//FTP to BOSU
		String startTime = formatter.format(new Date());
		logger.info(String.format("Start transferFileJob from Fidessa FTP to BOSU at :%s" , startTime) );
		Integer exitVal = (commandConsoleService.executeShell(shDownloadFtp));
		String endTime = formatter.format(new Date());
		logger.info(String.format("End transferFileJob from Fidessa FTP to BOSU at :%s" , endTime));

		//BOSU to AmazonS3
		startTime = formatter.format(new Date());
		logger.info(String.format("Start transferFileJob from BOSU to AmazonS3 at :%s" , startTime) );
		Integer count = transferFileService.transferFile();
		endTime = formatter.format(new Date());
		if(count==null || count<1){
			logger.info(String.format("End transferFileJob from BOSU to AmazonS3 at :%s" , endTime));
			throw new SysRuntimeException(ResponseCode.E1999,"Cannot transfer any file in the directory");
		}else{
			logger.info(String.format("End transferFileJob at :%s" , endTime));
		}






	}
}
