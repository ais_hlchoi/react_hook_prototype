package com.dbs.vtsp.service.impl;


import com.dbs.module.file.config.RetrieveConfig;
import com.dbs.module.file.util.LocalStorageUtils;
import com.dbs.module.model.FileModel;
import com.dbs.vtsp.service.SourceFileService;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SourceFileServiceImpl extends AbstractServiceImpl implements SourceFileService {

	private final Logger logger = LoggerFactory.getLogger(SourceFileServiceImpl.class);

	@Autowired
	private RetrieveConfig retrieveConfig;


	public FileModel[] getFiles() {

		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
		FileModel[] files = localStorageUtils.getFileModel();
		return files;
	}

	public void moveFilesToSuccess(FileModel[] fileModels) {

		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
		for (FileModel fileModel : fileModels) {
			localStorageUtils.move(fileModel.getFullPathNameExtension(),retrieveConfig.getLoadSuccessPath());
		}

	}

	public void moveFilesToSuccess(FileModel fileModel) {

		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
		localStorageUtils.move(fileModel.getFullPathNameExtension(),retrieveConfig.getLoadSuccessPath()+fileModel.getFileName()+"."+fileModel.getFileExtension());

	}

	public boolean checkExist(FileModel fileModel) {

		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
		return localStorageUtils.exist(retrieveConfig.getLoadSuccessPath()+fileModel.getFileName()+"."+fileModel.getFileExtension());

	}

	public boolean deleteBash(FileModel fileModel) {

		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
		return localStorageUtils.delete(fileModel.getFullPathNameExtension());

	}


//	public FileModel[] getFile(String path) {
//
//		LocalStorageUtils localStorageUtils = new LocalStorageUtils(retrieveConfig);
//		FileModel[] files = localStorageUtils.getFile(path);
//		try {
//			JSONArray jsonArray = new JSONArray(files);
//			System.out.println(jsonArray.toString());
//		}
//		catch (Exception e){
//
//		}
//		return files;
//	}



}
