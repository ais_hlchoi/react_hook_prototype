package com.dbs.vtsp.service.impl;


import com.amazonaws.services.s3.model.PutObjectResult;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.file.FileImport;
import com.dbs.module.model.FileModel;
import com.dbs.vtsp.service.ResultFileService;
import com.dbs.vtsp.service.SourceFileService;
import com.dbs.vtsp.service.TransferFileService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TransferFileServiceImpl extends AbstractServiceImpl implements TransferFileService {

	private final Logger logger = LoggerFactory.getLogger(TransferFileServiceImpl.class);


	@Autowired
	SourceFileService sourceFileService;

	@Autowired
	ResultFileService resultFileService;

	public Integer transferFile() {
		FileModel[] sourceFileServiceFiles  = sourceFileService.getFiles();

		if(sourceFileServiceFiles==null || sourceFileServiceFiles.length<1){
			throw new SysRuntimeException(ResponseCode.E1999,"Cannot find any file in the directory");
		}

		int errorCount = 0;
		String errorFileName = "";
		logger.info("Transfer file list\n"+ Arrays.stream(sourceFileServiceFiles)
				.map(sourceFile->{return sourceFile.getFileName();})
				.collect(Collectors.joining("\n"))
		);
		int totalFileCount = sourceFileServiceFiles.length;
		int duplicateCount = 0;
		int transferCount = 0;
		for (FileModel sourceFile : sourceFileServiceFiles) {
			try {
				logger.info("Start checking file"+ sourceFile.getFullPathNameExtension());
				Object result = null;
				if(sourceFileService.checkExist(sourceFile)){
					sourceFileService.deleteBash(sourceFile);
					//TODO remove
					duplicateCount++;
				}
				else {
					logger.info("Start transfer file"+ sourceFile.getFullPathNameExtension());
					result = resultFileService.saveFile(sourceFile);
					sourceFileService.moveFilesToSuccess(sourceFile);
					transferCount++;
					logger.info("Success transfer file" + sourceFile.getFullPathNameExtension());
				}



			}
			catch (Exception e){
				logger.error("Failed transfer file"+ sourceFile.getFullPathNameExtension());
				errorFileName+= sourceFile.getFullPathNameExtension()+"\n";
				errorCount++;
				e.printStackTrace();

			}
		}

		logger.info("There are "+ totalFileCount+" files found in FTP");
		logger.info("There are "+ duplicateCount+" duplicate file already transfer before ,  won't transfer again");
		logger.info("There are "+ transferCount+" transfer successfully");
		if(errorCount==0){
			logger.info("There are no files transfer failed.");
		}
		else{
			logger.error("There are "+ errorCount+" files transfer failed. \nThey are:\n"+errorFileName);
		}


		return totalFileCount;
	}




}
