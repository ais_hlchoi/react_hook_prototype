package com.dbs.vtsp.service;


import com.dbs.module.model.FileModel;

import java.io.File;

public interface ResultFileService {
    public FileModel[] getFiles();
    public Object saveFile(FileModel newFile);
    public Object saveFile(String fileName, String fileExtension,String content);

}
