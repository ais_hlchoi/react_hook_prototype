package com.dbs.vtsp.controller.internal;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.file.FileImport;
import com.dbs.module.model.FileModel;
import com.dbs.vtsp.service.CommandConsoleService;
import com.dbs.vtsp.service.ResultFileService;
import com.dbs.vtsp.service.SourceFileService;
import com.dbs.vtsp.service.TransferFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.io.File;

@RestController
@CrossOrigin
@RequestMapping("/api/s3")
public class DataExportController {


	@Autowired
	ResultFileService resultFileService;

	@Autowired
	SourceFileService sourceFileService;


	@Autowired
	TransferFileService transferFileService;

	@Value("${sh.fds.upld.txn}")
	String shDownloadFtp;


	@Autowired
	CommandConsoleService commandConsoleService;

	@RequestMapping(value = "/getOutcomeFiles", method = RequestMethod.POST)
	public RestEntity getOutcomeFiles() {
		return RestEntity.success(resultFileService.getFiles());
	}

	@RequestMapping(value = "/getIncomeFiles", method = RequestMethod.POST)
	public RestEntity getIncomeFiles() {
		return RestEntity.success(sourceFileService.getFiles());
	}

	@RequestMapping(value = "/moveFiles", method = RequestMethod.POST)
	public RestEntity moveFiles() {
		for (FileModel file : sourceFileService.getFiles()) {
			sourceFileService.moveFilesToSuccess(file);
		}
		return RestEntity.success();
	}
//
//	@RequestMapping(value = "/saveFile", method = RequestMethod.POST)
//	public RestEntity saveFile() {
//		return RestEntity.success(resultFileService.saveFile(new File("123")));
//	}


	@RequestMapping(value = "/saveFileWithContent", method = RequestMethod.POST)
	public RestEntity saveFileWithContent() {
		return RestEntity.success(resultFileService.saveFile("testName","txt","123"));
	}

	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	public RestEntity transferFile() throws Exception{
		//FTP to BOSU
		Integer exitVal = (commandConsoleService.executeShell(shDownloadFtp));

		//BOSU to AmazonS3
		return RestEntity.success(transferFileService.transferFile());
	}


	@RequestMapping(value = "/donwloadFileFromFTPToBOSU", method = RequestMethod.POST)
	public RestEntity donwloadFilePattern() throws Exception {
		return RestEntity.success(commandConsoleService.executeShell(shDownloadFtp));
	}


	@RequestMapping(value = "/transferFileFromBOSUToS3", method = RequestMethod.POST)
	public RestEntity transferFileToS3() throws Exception {
		return RestEntity.success(transferFileService.transferFile());
	}
//	public RestEntity connectFtp() {
//		String server = "10.91.79.106";
//		int port = 21;
//		String user = "svohkin1";
//		String pass = "";
//
//		System.out.println("server: " + server+ " user: "+user);
//		SFTP ftpClient = new FTPClient();
//		try {
//			ftpClient.connect(server, port);
//			showServerReply(ftpClient);
//			int replyCode = ftpClient.getReplyCode();
//			if (!FTPReply.isPositiveCompletion(replyCode)) {
//				System.out.println("Operation failed. Server reply code: " + replyCode);
//				return RestEntity.failed("Operation failed. Server reply code: " + replyCode);
//			}
//			boolean success = ftpClient.login(user, pass);
//			showServerReply(ftpClient);
//			if (!success) {
//				System.out.println("Could not login to the server");
//				return RestEntity.failed("Could not login to the server");
//			} else {
//				return RestEntity.success("LOGGED IN SERVER");
//			}
//		} catch (IOException ex) {
//			System.out.println("Oops! Something wrong happened");
//			ex.printStackTrace();
//			return RestEntity.failed(ex.getMessage());
//		}
//
//	}

//	private static void showServerReply(FTPClient ftpClient) {
//		String[] replies = ftpClient.getReplyStrings();
//		if (replies != null && replies.length > 0) {
//			for (String aReply : replies) {
//				System.out.println("SERVER: " + aReply);
//			}
//		}
//	}
//
//	@RequestMapping(value = "/connectFtpSSHJ", method = RequestMethod.POST)
//	public RestEntity connectFtp(){
//		SSHClient client = new SSHClient();
//		try {
//			client.addHostKeyVerifier(new PromiscuousVerifier());
//			client.connect("10.91.79.106");
//			System.out.println("client.isConnected()"+client.isConnected());
////			client.auth("svohkin1",);
////			client.authPublickey("svohkin1","/u05/bims/bmprod/exe/s3_file_storage_test/id_rsa.pub","/u05/bims/bmprod/exe/s3_file_storage_test/id_rsa");
//			client.authPublickey("svohkin1", new KeyPairWrapper(
//					KeyPairGenerator.getInstance("DSA", "SUN").generateKeyPair()));
//
//
//			System.out.println("client.getUserAuth().getAllowedMethods()"+client.getUserAuth().getAllowedMethods());
//
//
//
//
//
//			SFTPClient sftpClient = client.newSFTPClient();
//			List<RemoteResourceInfo> list =  sftpClient.ls("D:\\\\FidessaSFT_ATP_SG\\\\PROD\\\\MISFiles");
//			System.out.println("list.size()"+list.size());
//
//			//client.authPassword("svohkin1","svohkin1");
//				}
//		catch (Exception e){
//			e.printStackTrace();
//			return RestEntity.failed(e.getMessage());
//		}
//
//
//		System.out.println("client.isConnected()"+client.isConnected());
//		return RestEntity.success(client.isConnected());
//
//	}



}
