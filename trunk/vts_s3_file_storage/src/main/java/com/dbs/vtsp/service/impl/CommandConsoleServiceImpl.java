package com.dbs.vtsp.service.impl;


import com.amazonaws.services.s3.model.PutObjectResult;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.model.FileModel;
import com.dbs.vtsp.service.CommandConsoleService;
import com.dbs.vtsp.service.ResultFileService;
import com.dbs.vtsp.service.SourceFileService;
import com.dbs.vtsp.service.TransferFileService;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class CommandConsoleServiceImpl extends AbstractServiceImpl implements CommandConsoleService {

	private final Logger logger = LoggerFactory.getLogger(CommandConsoleServiceImpl.class);



	public Integer executeShell(String command) throws Exception {
		Integer exitVal = null;
		try {
		ProcessBuilder processBuilder = new ProcessBuilder();

		// -- Linux --
		// Run a shell command
		//processBuilder.command("bash", "-c", "ls /u05/bims/bmprod/exe/s3_file_storage_test/");
		// Run a shell script
//		logger.info("Run command : "+command + args.toString());
//		String[] commandsWithArgs;
//		if(args!=null || args.length>0) {
//			commandsWithArgs = (String[]) ArrayUtils.addAll(new String[]{command}, args);
//		}
//		else{
//			commandsWithArgs = new String[]{command};
//		}

		logger.info("Run command : "+command );
		processBuilder.command(command);

		// -- Windows --
		// Run a command
		//processBuilder.command("cmd.exe", "/c", "dir C:\\Users\\mkyong");
		// Run a bat file
		//processBuilder.command("C:\\Users\\mkyong\\hello.bat");

			Process process = processBuilder.start();
			StringBuilder output = new StringBuilder();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
			exitVal = process.waitFor();
			if (exitVal == 0) {
				System.out.println("Success!");


			}
			System.out.println(output);
			return exitVal;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return exitVal;
	}




}
