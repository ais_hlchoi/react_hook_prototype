package com.dbs.vtsp.service;


import com.dbs.module.file.util.LocalStorageUtils;
import com.dbs.module.model.FileModel;

import java.io.File;

public interface SourceFileService {
    public FileModel[] getFiles();

    public void moveFilesToSuccess(FileModel[] fileModels);
    public void moveFilesToSuccess(FileModel fileModel);
    public boolean checkExist(FileModel fileModel);
    public boolean deleteBash(FileModel fileModel);
}
