package com.dbs.vtsp.service.impl;


import com.dbs.module.file.FileImport;
import com.dbs.module.model.FileModel;
import com.dbs.vtsp.service.ResultFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ResultFileServiceImpl extends AbstractServiceImpl implements ResultFileService {

	private final Logger logger = LoggerFactory.getLogger(ResultFileServiceImpl.class);


	@Autowired
	private FileImport s3StorageUtils;


	public FileModel[] getFiles() {
		FileModel[] files  = s3StorageUtils.getFileModel();
		return files;
	}


	public Object saveFile(FileModel newFile) {
		Object resultObject = s3StorageUtils.saveFile(newFile);
		return resultObject;
	}

	public Object saveFile(String fileName,String fileExtension,String content) {
		Object resultObject = s3StorageUtils.saveFile(fileName,fileExtension,content);
		return resultObject;
	}


}
