package com.dbs.module.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter
@Setter
public class FileModel {


	private String filePath;


	private String fileName;


	private String fileExtension;

	private String fullPathNameExtension;

	@JsonIgnore
	private File file;


//	public String getFileFullPathNameExtension(){
//	return this.filePath+this.fileName+"."+fileExtension;
//	}


}
