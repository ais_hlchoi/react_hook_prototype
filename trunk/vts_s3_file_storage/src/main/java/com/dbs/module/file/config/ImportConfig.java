package com.dbs.module.file.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.dbs.module.file.FileImportProps;

import lombok.Getter;

@Configuration
@Getter
public class ImportConfig extends AbstractImportConfig implements FileImportProps {

	@Value("${folder.root}")
	private String rootPath;

	@Value("${folder.import}")
	private String loadPath;

	@Value("${folder.import.process}")
	private String loadProcessPath;

	@Value("${folder.import.success}")
	private String loadSuccessPath;

	@Value("${folder.import.fail}")
	private String loadFailPath;

	@Value("${file.import.pattern.name}")
	private String fileImportPatternName;

	@Value("${file.import.pattern.ext}")
	private String fileImportPatternExt;
}
