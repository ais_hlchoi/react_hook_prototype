package com.dbs.module.aws.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;

public class S3Service {

	private final AmazonS3 s3Client;

	public S3Service(AmazonS3 s3Client) {
		this.s3Client = s3Client;
	}

	public boolean isReadyS3Client() {
		return null != s3Client;
	}

	public List<Bucket> listBuckets() {
		return s3Client.listBuckets();
	}

	public void deleteBucket(String bucketName) {
		s3Client.deleteBucket(bucketName);
	}

	public PutObjectResult putObject(String bucketName, String key, File file) {
		return s3Client.putObject(bucketName, key, file);
	}

	public PutObjectResult putObject(String bucketName, String key, String content) {
		return s3Client.putObject(bucketName, key, content);
	}


	public boolean doesObjectExist(String bucketName, String key) {
		return s3Client.doesObjectExist(bucketName,key);
	}

	public ObjectListing listObjects(String bucketName) {
		return s3Client.listObjects(bucketName);
	}

	public ListObjectsV2Result listObjectsV2(String bucketName, Map<ListObjectsV2RequestProps, String> config) {
		ListObjectsV2Request req = new ListObjectsV2Request();
		if (MapUtils.isNotEmpty(config)) {
			config.entrySet().stream().forEach(o -> setListObjectsV2RequestParam(req, o.getKey(), o.getValue()));
		}
		req.withBucketName(bucketName);
		return s3Client.listObjectsV2(req);
	}

	public S3Object getObject(String bucketName, String objectKey) {
		return s3Client.getObject(bucketName, objectKey);
	}

	public CopyObjectResult copyObject(String sourceBucketName, String sourceKey, String destinationBucketName, String destinationKey) {
		return s3Client.copyObject(sourceBucketName, sourceKey, destinationBucketName, destinationKey);
	}

	public void deleteObject(String bucketName, String objectKey) {
		s3Client.deleteObject(bucketName, objectKey);
	}

	public DeleteObjectsResult deleteObjects(DeleteObjectsRequest delObjReq) {
		return s3Client.deleteObjects(delObjReq);
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	private void setListObjectsV2RequestParam(ListObjectsV2Request req, ListObjectsV2RequestProps attr, String text) {
		Object value = ListObjectsV2RequestProps.getValueOrElse(text, attr.cls);
		if (null == value)
			return;
		Stream.of(ListObjectsV2Request.class.getDeclaredMethods()).filter(method -> method.getName().equals(getMethodName(attr.key, "with"))).forEach(method -> invoke(method, req, value));
	}

	private String getMethodName(String name, String prefix) {
		// no check any more since it is internal function
		return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private void invoke(Method method, Object object, Object value) {
		try {
			method.invoke(object, value);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// no error any more since it is internal function
		}
	}

	public enum ListObjectsV2RequestProps {
		BUCKET_NAME("bucketName")
		, CONTINUATION_TOKEN("continuationToken")
		, DELIMITER("delimiter")
		, ENCODING_TYPE("encodingType")
		, EXPECTED_BUCKET_OWNER("expectedBucketOwner")
		, FETCH_OWNER("fetchOwner", Boolean.class)
		, MAX_KEYS("maxKeys", Integer.class)
		, PREFIX("prefix")
		, REQUESTER_PAYS("requesterPays", Boolean.class)
		, START_AFTER("startAfter")
		;
		public final String key;
		public final Class<?> cls;
		private ListObjectsV2RequestProps(String key) {
			this(key, String.class);
		}
		private ListObjectsV2RequestProps(String key, Class<?> cls) {
			this.key = key;
			this.cls = cls;
		}
		@SuppressWarnings("unchecked")
		public static <T> T getValueOrElse(String value, Class<T> cls) {
			if (null == value || null == cls)
				return null;
			else if (cls == Integer.class)
				return (T) (NumberUtils.isCreatable(value) ? NumberUtils.createInteger(value) : null);
			else if (cls == Boolean.class)
				return (T) BooleanUtils.toBooleanObject(value);
			return (T) String.valueOf(value);
		}
	}
}
