package com.dbs.module.file.util;

import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.config.ImportConfig;
import com.dbs.module.file.config.ImportS3Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class FileImportUtils extends AbstractFileImportUtils {

	@Autowired
	private ImportConfig importConfig;

	@Autowired
	private ImportS3Config importS3Config;

	private FileImport fileImport;

	@Value("${aws.s3client.service}")
	private boolean isS3Service;

	protected FileImport getService() {
		if (null == fileImport) {
			fileImport = isS3Service ? new S3StorageUtils(importS3Config) : new LocalStorageUtils(importConfig);
		}
		return fileImport;
	}


}
