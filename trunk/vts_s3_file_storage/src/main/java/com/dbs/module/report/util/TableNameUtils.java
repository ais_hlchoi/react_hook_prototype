package com.dbs.module.report.util;

public class TableNameUtils {

	private TableNameUtils() {
		// for SonarQube scanning purpose
	}

	public static String parse(String name) {
		if (null == name)
			return "";

		return name.trim().replaceAll("\\s+|-", "_").toUpperCase();
	}
}
