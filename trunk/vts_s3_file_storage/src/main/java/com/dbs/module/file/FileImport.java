package com.dbs.module.file;

import com.dbs.module.model.FileModel;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface FileImport {

	FileImportProps getProps();

	String[] list();

	String[] list(String path);

	String[] list(String path, String prefix);

	void copy(String source, String destination);

	void move(String source, String destination);

	File getFile(String name);

	InputStream getStream(String name);

	CSVReader getCsvReader(String name);

	boolean isFile(String name);

	boolean isMatch(String name, String pattern, String ext, String parent);

	List<String> prepare(String... paths);

	Object saveFile(FileModel file);

	Object saveFile(String fileName,String fileExtension,String content);

	public FileModel[] getFileModel();

	public boolean exist(String destination);

	public boolean delete(String destination) ;
}
