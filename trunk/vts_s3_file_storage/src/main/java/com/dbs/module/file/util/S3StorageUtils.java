package com.dbs.module.file.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.dbs.module.model.FileModel;
import com.dbs.util.FilenameUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.aws.util.S3Service;
import com.dbs.module.aws.util.S3Service.ListObjectsV2RequestProps;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.FileImportProps;
import com.opencsv.CSVReader;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Service
public class S3StorageUtils implements FileImport {

	private S3Service service;
	private String bucket;
	private FileImportProps props;

	public FileImportProps getProps() {
		return props;
	}

	protected S3StorageUtils() {
		// for SonarQube scanning purpose
	}

	public S3StorageUtils(FileImportProps props) {
		this.service = S3ClientUtils.getS3Service();
		this.bucket = S3ClientUtils.getBucketName();
		this.props = props;
	}

	public String[] list() {
		return list(getProps().getLoadPath(), null);
	}

	public String[] list(String path) {
		return list(path, null);
	}

	public String[] list(String path, String prefix) {
		String pathAndPrefix = (StringUtils.isBlank(path) ? "" : path) + (StringUtils.isBlank(prefix) ? "" : prefix);
		return service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().map(S3ObjectSummary::getKey).toArray(String[]::new);
	}

	public FileModel[] getFileModel() {
		return getFileModel(getProps().getLoadPath(), null);
	}
	public FileModel[] getFileModel(String path, String prefix) {
		String pathAndPrefix = (StringUtils.isBlank(path) ? "" : path) + (StringUtils.isBlank(prefix) ? "" : prefix);
		List<S3ObjectSummary> s3ObjectSummaries = service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().collect(Collectors.toList());
		return service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().map( s3ObjectSummary -> {
			String key = s3ObjectSummary.getKey();
			String fileName = String.format("%s.%s", org.apache.commons.io.FilenameUtils.getBaseName(key), org.apache.commons.io.FilenameUtils.getExtension(key));
			FileModel fileModel = new FileModel();
			fileModel.setFilePath(FilenameUtils.getFullPath(key));
			fileModel.setFileName(FilenameUtils.getBaseName(fileName));
			fileModel.setFileExtension(FilenameUtils.getExtension(fileName));
			fileModel.setFullPathNameExtension(key);
			//S3Object s3Object = service.getObject(bucket, key);
			return fileModel;
		}).toArray(FileModel[]::new);

	}


	public void copy(String source, String destination) {
		service.copyObject(bucket, FilenameUtils.toUriString(source), bucket, FilenameUtils.toUriString(destination));
	}

	public void move(String source, String destination) {
		service.copyObject(bucket, FilenameUtils.toUriString(source), bucket, FilenameUtils.toUriString(destination));
		service.deleteObject(bucket, FilenameUtils.toUriString(source));
	}

	public File getFile(String name) {
		return null;
	}

	public InputStream getStream(String name) {
		return service.getObject(bucket, FilenameUtils.toUriString(name)).getObjectContent();
	}

	public CSVReader getCsvReader(String name) {
		return new CSVReader(new InputStreamReader(getStream(name)));
	}

	public boolean isFile(String name) {
		return StringUtils.isNotBlank(FilenameUtils.getExtension(name));
	}

	public boolean isMatch(String name, String pattern, String ext, String parent) {
		return isFile(name) && FilenameUtils.match(name, pattern, ext) && FilenameUtils.matchParent(name, parent);
	}

	public List<String> prepare(String... paths) {
		return IntStream.range(0, paths.length).boxed().filter(n -> StringUtils.isBlank(paths[n])).map(n -> String.format("Path is empty in prepare.path[%d]", n)).collect(Collectors.toList());
	}

	public Object saveFile(FileModel file) {
		String storageKey = saveFileGetStorageKey(file);
		PutObjectResult putObjectResult = service.putObject(bucket, storageKey,file.getFile());
		return putObjectResult;

	}

	public Object saveFile(String fileName,String fileExtension,String content) {
		FileModel fileModel = new FileModel();
		fileModel.setFileName(fileName);
		fileModel.setFileExtension(fileExtension);
		String storageKey = saveFileGetStorageKey(fileModel); //No specific FileName
		PutObjectResult putObjectResult = service.putObject(bucket, storageKey,content);
		return putObjectResult;

	}

	private String saveFileGetStorageKey(FileModel fileModel){
		//String generatedString = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy_HHmm");
		Date date = new Date();
		String dateString = formatter.format(date);
		return  getProps().getLoadPath() + fileModel.getFileName() + "_" + dateString + "." + fileModel.getFileExtension();

	}


	@Override
	public boolean exist(String destination) {
		throw new NotImplementedException();//TODO
	}

	public boolean delete(String destination) {
		throw new NotImplementedException();//TODO
	}
}
