package com.dbs.module.file.config;

public abstract class AbstractImportConfig {

	abstract String getLoadPath();

	public String getLoadImportPath(String fileName) {
		return getLoadPath() + fileName;
	}
}
