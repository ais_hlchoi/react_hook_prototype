package com.dbs.module.file.config;

import com.dbs.module.file.FileImportProps;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class RetrieveConfig extends AbstractImportConfig implements FileImportProps {

	@Value("${folder.retrieve.root}")
	private String rootPath;

	@Value("${folder.retrieve}")
	private String loadPath;

	@Value("${folder.retrieve.process}")
	private String loadProcessPath;

	@Value("${folder.retrieve.success}")
	private String loadSuccessPath;

	@Value("${folder.retrieve.fail}")
	private String loadFailPath;

	@Value("${file.import.pattern.name}")
	private String fileImportPatternName;

	@Value("${file.import.pattern.ext}")
	private String fileImportPatternExt;


}
