package com.dbs.module.file.config;

import com.dbs.module.file.config.AbstractImportConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.dbs.module.file.FileImportProps;

import lombok.Getter;

@Configuration
@Getter
public class ImportS3Config extends AbstractImportConfig implements FileImportProps {

	@Value("${folder.s3.root}")
	private String rootPath;

	@Value("${folder.s3.import}")
	private String loadPath;

	@Value("${folder.s3.import.process}")
	private String loadProcessPath;

	@Value("${folder.s3.import.success}")
	private String loadSuccessPath;

	@Value("${folder.s3.import.fail}")
	private String loadFailPath;

	@Value("${file.import.pattern.name}")
	private String fileImportPatternName;

	@Value("${file.import.pattern.ext}")
	private String fileImportPatternExt;


}