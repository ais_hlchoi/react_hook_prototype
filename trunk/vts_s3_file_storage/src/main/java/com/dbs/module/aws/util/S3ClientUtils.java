package com.dbs.module.aws.util;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.dbs.module.aws.config.S3Config;
import com.dbs.module.aws.config.TrustCertHelper;

@Component
public class S3ClientUtils {

	private static AmazonS3 s3Client;
	private static String bucketname;

	@Autowired
	private S3Config s3Config;



	@PostConstruct
	private void init() {
		if (BooleanUtils.isNotTrue(BooleanUtils.toBoolean(s3Config.getService()))) {
			s3Client = null;
			bucketname = null;
			return;
		}

		ClientConfiguration client = new ClientConfiguration();
		client.getApacheHttpClientConfig().setSslSocketFactory(TrustCertHelper.getSSLSocketFactory());
		s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Config.getAccessId(), s3Config.getSecretKey())))
				.withEndpointConfiguration(new EndpointConfiguration(s3Config.getEndPoint(), s3Config.getRegion()))
				.withClientConfiguration(client)
				.build();
		bucketname = s3Config.getBucketname();
	}

	public static S3Service getS3Service() {
		return new S3Service(s3Client);
	}

	public static String getBucketName() {
		return bucketname;
	}


}
