package com.dbs.module.aws.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.http.conn.ssl.TrustStrategy;

public class TrustCertHelper {

	protected static TrustStrategy getTrustCertificate() {
		return new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				return true;
			}
		};
	}

	@SuppressWarnings("deprecation")
	protected static org.apache.http.conn.ssl.AllowAllHostnameVerifier getTrustVerifier() {
		return new org.apache.http.conn.ssl.AllowAllHostnameVerifier();
	}

	@SuppressWarnings("deprecation")
	public static org.apache.http.conn.ssl.SSLSocketFactory getSSLSocketFactory() {
		org.apache.http.conn.ssl.SSLSocketFactory socketFactory = null;
		try {
			socketFactory = new org.apache.http.conn.ssl.SSLSocketFactory(getTrustCertificate(), getTrustVerifier());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return socketFactory;
	}
}
