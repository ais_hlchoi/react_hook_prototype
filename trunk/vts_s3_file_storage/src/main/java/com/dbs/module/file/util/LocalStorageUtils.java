package com.dbs.module.file.util;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.dbs.module.model.FileModel;
import com.dbs.util.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.dbs.module.file.FileImport;
import com.dbs.module.file.FileImportProps;
import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.opencsv.CSVReader;

@Service
public class LocalStorageUtils implements FileImport {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private FileImportProps props;

	public FileImportProps getProps() {
		return props;
	}

	protected LocalStorageUtils() {
		// for SonarQube scanning purpose
	}

	public LocalStorageUtils(FileImportProps props) {
		this.props = props;
	}

	public String[] list() {
		return list(getProps().getLoadPath(), null);
	}

	public String[] list(String path) {
		return list(path, null);
	}

	public String[] list(String path, String prefix) {
		if (StringUtils.isBlank(path))
			return new String[] {};

		File folder = getFile(path);
		String[] files = folder.list();
		if (ArrayUtils.isEmpty(files))
			return files;

		return (StringUtils.isBlank(prefix) ? Stream.of(files) : Stream.of(files).filter(o -> o.startsWith(prefix))).map(baseName -> FilenameUtils.concat(folder.getAbsolutePath(), baseName)).toArray(String[]::new);
	}


	public FileModel[] getFileModel(){
		return getFileModel(getProps().getLoadPath());
	}

	public FileModel[] getFileModel(String path){
		if (StringUtils.isBlank(path)) {
			logger.error("path is empty");
			return new FileModel[]{};
		}

		File folder = getFile(path);
		logger.info(path);
		logger.info(folder.getName());
		if(!folder.exists()){
			logger.error("Folder cannot be found");
		}
		String[] files = folder.list();
		if(files==null || files.length<1){
			logger.error("There are no file found in this folder "+folder.getName());
		}
		else{
			logger.info("There are "+files.length +" file found in this folder "+folder.getName());
		}

		return Arrays.stream(files).map(file->{
			FileModel fileModel = new FileModel();
			fileModel.setFilePath(path);
			fileModel.setFileExtension(FilenameUtils.getExtension(file));
			fileModel.setFileName(FilenameUtils.getBaseName(file));
			fileModel.setFullPathNameExtension(path+file);
			fileModel.setFile(getFile(path+file));
			return fileModel;
		}).toArray(FileModel[]::new);

	}

	public void copy(String source, String destination) {
		LocalStorageUtils.moveFile(source, destination, StandardCopyOption.COPY_ATTRIBUTES);
	}

	public void move(String source, String destination) {
		LocalStorageUtils.moveFile(source, destination, StandardCopyOption.REPLACE_EXISTING);
	}

	public File getFile(String name) {
		return LocalStorageUtils.openFile(name);
	}

	public InputStream getStream(String name) {
		try {
			return new FileInputStream(getFile(name));
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	public CSVReader getCsvReader(String name) {
		try {
			return new CSVReader(new FileReader(getFile(name)));
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	public boolean isFile(String name) {
		return getFile(name).isFile();
	}

	public boolean isMatch(String name, String pattern, String ext, String parent) {
		return isFile(name) && FilenameUtils.match(name, pattern, ext) && FilenameUtils.matchParent(name, parent);
	}

	public List<String> prepare(String... paths) {
		List<String> logger = new ArrayList<>();
		Stream.of(paths).map(o -> FilenameUtils.getFullPath(FortifyStrategyUtils.toPathString(o))).map(Paths::get).forEach(path -> {
			try {
				Files.createDirectories(path);
			} catch (FileAlreadyExistsException e) {
				logger.add(String.format("Path is already existed [ %s ]", path.toString()));
			} catch (IOException e) {
				logger.add(FortifyStrategyUtils.toErrString(e));
			}
		});
		return logger;
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected static File openFile(String name) {
		return new File(FortifyStrategyUtils.toPathString(name));
	}

	protected static void moveFile(String source, String destination, StandardCopyOption option) {
		Assert.isTrue(new File(FortifyStrategyUtils.toPathString(source)).exists(), "File not found.");
		String msg = null;
		try {
			Path from = Paths.get(FortifyStrategyUtils.toPathString(source));
			Path to = Paths.get(destination);
			Files.move(from, to, option);
		} catch (IOException e) {
			msg = FortifyStrategyUtils.toErrString(e);
		}
		Assert.isNull(msg, msg);
	}



	public Object saveFile(FileModel fileModel) {
		return new NotImplementedException();//TODO
	}

	public Object saveFile(String fileName,String fileExtension,String content) {
		return new NotImplementedException();//TODO
	}

	public boolean exist(String destination) {
		return getFile(destination).exists();
	}


	public boolean delete(String destination) {
		return getFile(destination).delete();
	}


}
