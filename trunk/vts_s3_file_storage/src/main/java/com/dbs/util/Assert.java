package com.dbs.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

public class Assert extends org.springframework.util.Assert {

	public static void notBlank(@Nullable String text, String message) {
		if (StringUtils.isBlank(text)) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isFalse(boolean expression, String message) {
		if (expression) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void equals(@Nullable Object a, Object b, String message) {
		if ((a == null && b != null) || (a != null && !a.equals(b))) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isTrue(boolean expression, String message, List<String> list) {
		if (!expression && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void isFalse(boolean expression, String message, List<String> list) {
		if (expression && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void isNull(@Nullable Object object, String message, List<String> list) {
		if (object != null && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void notNull(@Nullable Object object, String message, List<String> list) {
		if (object == null && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void notBlank(@Nullable String text, String message, List<String> list) {
		if (StringUtils.isBlank(text) && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}
}
