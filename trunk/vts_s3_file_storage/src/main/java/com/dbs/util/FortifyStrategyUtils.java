package com.dbs.util;

import java.util.Map;

public class FortifyStrategyUtils {

	private FortifyStrategyUtils() {
		// for SonarQube scanning purpose
	}

	public static String toMaskString(String text) {
		return ClassPathUtils.toMaskString(text);
	}

	public static String toCharString(String text) {
		return ClassPathUtils.toCharString(text, 32, 127);
	}

	public static String toPathString(String text) {
		return ClassPathUtils.toCharString(text);
	}

	public static String toLogString(String... args) {
		return ClassPathUtils.toLogString(args);
	}

	public static String toErrString(Throwable cause) {
		return ClassPathUtils.toLogString(cause.getMessage());
	}

	public static <T> T fromParamMap(Map<String, Object> object, Class<T> cls) {
		return ClassPathUtils.fromObjectMap(object, cls);
	}

	public static Map<String, Object> toParamMap(Object object) {
		return ClassPathUtils.toObjectMap(object);
	}

	public static Object toParamVar(Object object) {
		return ClassPathUtils.toObjectVar(object);
	}
}
