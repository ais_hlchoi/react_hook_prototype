package com.dbs.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dbs.module.report.util.TableNameUtils;
import org.apache.commons.lang3.StringUtils;
public class FilenameUtils extends org.apache.commons.io.FilenameUtils {

	public static boolean match(String name, String pattern, String ext) {
		if (StringUtils.isBlank(name))
			return false;

		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(TableNameUtils.parse(FilenameUtils.getBaseName(name)));
		if (!m.find())
			return false;

		if (StringUtils.isNotBlank(ext)) {
			p = Pattern.compile(ext);
			m = p.matcher(FilenameUtils.getExtension(name));
			if (!m.find())
				return false;
		}

		return true;
	}

	public static boolean matchParent(String path, String parent) {
		if (StringUtils.isBlank(path))
			return false;

		return StringUtils.isBlank(parent) || FilenameUtils.concat(FilenameUtils.getFullPath(parent), FilenameUtils.getBaseName(parent)).equals(FilenameUtils.concat(FilenameUtils.getFullPath(path), ""));
	}

	public static String toUriString(String path) {
		if (StringUtils.isBlank(path))
			return "";

		String a = FilenameUtils.getFullPath(path);
		String b = FilenameUtils.getBaseName(path);
		String c = FilenameUtils.getExtension(path);
		return FilenameUtils.concat(getOrElse(a, ""), getOrElse(b, getOrElse(c, String.format("%s.%s", b, c), b), "")).replaceAll("\\\\", "/");
	}

	private static String getOrElse(String value, String valueNotNull, String valueIfNull) {
		return StringUtils.isBlank(value) ? valueIfNull : valueNotNull;
	}

	private static String getOrElse(String value, String valueIfNull) {
		return getOrElse(value, value, valueIfNull);
	}
}
