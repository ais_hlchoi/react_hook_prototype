package com.dbs.module.export.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.module.util.AbstractModuleUtils;

public class DataParserUtils extends AbstractModuleUtils {

	private DataParserUtils() {
		// for SonarQube scanning purpose
	}

	public static List<Map<String, Object>> transpose(final List<Map<String, Object>> list) {
		if (CollectionUtils.isEmpty(list))
			return list;

		List<String> headList = new ArrayList<>(list.get(0).keySet());
		int index = 0;
		List<Map<String, Object>> bodyList = new ArrayList<>();
		for (int i = 0; i < headList.size(); i++) {
			String header = headList.get(i);
			if (index < headList.size()) {
				bodyList.add(new LinkedHashMap<>());
				bodyList.get(i).put("header", header);
				for (int j = 0; j < list.size(); j++) {
					List<Object> objects = new ArrayList<>(list.get(j).values());
					bodyList.get(i).put(String.format("col_%d", j), objects.get(i));
				}
				index++;
			}
		}
		return bodyList;
	}

	public static String toString(final Object object) {
		String text = null;
		if (null == object) {
			text = "";
		} else if (object instanceof String) {
			text = (String) object;
		} else if (object instanceof Integer) {
			text = String.valueOf((Integer) object);
		} else if (object instanceof Long) {
			text = String.valueOf((Long) object);
		} else if (object instanceof Double) {
			text = String.valueOf((Double) object);
		} else if (object instanceof BigDecimal) {
			text = ((BigDecimal) object).toPlainString();
		} else if (object instanceof Date) {
			text = new SimpleDateFormat("yyyyMMdd HHmmss").format((Date) object);
		}
		return getOrElse(text, String.valueOf(object));
	}

	public static Map<Integer, String> toMap(final List<String> list) {
		return IntStream.range(0, list.size()).boxed().collect(Collectors.toMap(n -> n, n -> list.get(0)));
	}
}
