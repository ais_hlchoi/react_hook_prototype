package com.dbs.module.download.config;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.dbs.util.FilenameUtils;

import lombok.AccessLevel;
import lombok.Getter;

@Configuration
@Getter
public class DownloadConfig {

	@Value("${aws.s3client.service}")
	@Getter(AccessLevel.NONE)
	private String service;

	@Value("${folder.root}")
	@Getter(AccessLevel.NONE)
	private String localFolderRoot;

	@Value("${folder.output}")
	@Getter(AccessLevel.NONE)
	private String localFolderOutput;

	@Value("${folder.s3.root}")
	@Getter(AccessLevel.NONE)
	private String s3FolderRoot;

	@Value("${folder.s3.output}")
	@Getter(AccessLevel.NONE)
	private String s3FolderOutput;

	@Value("${folder.s3.temp}")
	@Getter(AccessLevel.NONE)
	private String s3FolderTemp;

	@Value("${file.download.file-path-prefix}")
	private String fileDownloadPathPrefix;

	@Value("${file.download.report-name}")
	private String fileDownloadReportName;

	public String getFolderRoot() {
		return FilenameUtils.toPathString(BooleanUtils.toBoolean(service) ? s3FolderRoot : localFolderRoot);
	}

	public String getFolderOutput() {
		return FilenameUtils.toPathString(BooleanUtils.toBoolean(service) ? s3FolderOutput : localFolderOutput);
	}

	public String getFolderTemp() {
		return FilenameUtils.toPathString(BooleanUtils.toBoolean(service) ? s3FolderTemp : localFolderOutput);
	}
}
