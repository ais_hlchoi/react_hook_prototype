package com.dbs.module.export.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.export.Grid;
import com.dbs.module.report.constant.ReportLayoutType;

public class GridCsvData implements Grid {

	private List<List<String>> data;
	private String layout;

	public GridCsvData(List<List<String>> data) {
		this(data, ReportLayoutType.H.name());
	}

	public GridCsvData(List<List<String>> data, String layout) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
		this.layout = ReportLayoutType.getOrElse(layout, ReportLayoutType.H).name();
	}

	protected List<List<String>> getData() {
		return data;
	}

	protected void setData(List<List<String>> data) {
		this.data = data;
	}

	public String getLayout() {
		return layout;
	}

	protected void setLayout(String layout) {
		this.layout = layout;
	}

	public List<String> getHead() {
		return isEmpty() ? new ArrayList<>() : getData().get(0);
	}

	public List<List<String>> getBody() {
		return size() < 2 ? new ArrayList<>() : getData().subList(1, size());
	}

	public int size() {
		return CollectionUtils.isEmpty(getData()) ? 0 : getData().size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public boolean isNotEmpty() {
		return !isEmpty();
	}
}
