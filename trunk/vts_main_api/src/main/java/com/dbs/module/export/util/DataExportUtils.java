package com.dbs.module.export.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.export.DataExport;
import com.dbs.module.export.model.DataExportModel;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportPropsParameter;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.ReportParamUtils;

public class DataExportUtils {

	private DataExportUtils() {
		// for SonarQube scanning purpose
	}

	public static DataExport parse(final ReportTemplate model) {
		return parse(model, null, null);
	}

	public static DataExport parse(final ReportTemplate model, final String sql) {
		return parse(model, null, sql);
	}

	public static DataExport parse(final ReportTemplate model, final List<? extends ReportPropsParameter> list) {
		return parse(model, list, null);
	}

	public static DataExport parse(final ReportTemplate model, final List<? extends ReportPropsParameter> list, final String sql) {
		DataExportModel o = new DataExportModel();
		if (null == model)
			return o;

		List<ReportPropsParameter> parameterList = new ArrayList<>();
		if (null == model.getOption() && CollectionUtils.isNotEmpty(model.getOption().getParameter()))
			parameterList.addAll(model.getOption().getParameter());
		if (CollectionUtils.isNotEmpty(list))
			parameterList.addAll(list);

		// this part as reference only [ e.g. list out fields in Excel or PDF only ]
		if (null != model.getBasic()) {
			o.setSelect(CollectionUtils.isEmpty(model.getBasic().getSelect()) ? null : DynQuerySqlUtils.parse(model.getBasic().getSelect(), model.getBasic().getFrom()).stream().collect(Collectors.joining(", ")));
			o.setFrom(CollectionUtils.isEmpty(model.getBasic().getFrom()) ? null : model.getBasic().getFrom().stream().map(ReportPropsTable::getChildren).map(StringUtils::upperCase).collect(Collectors.joining(", ")));
			o.setWhere(CollectionUtils.isEmpty(model.getBasic().getWhere()) ? null : model.getBasic().getWhere().stream().map(ReportPropsFilter::getValue).collect(Collectors.joining(" ")));
			o.setOrderBy(CollectionUtils.isEmpty(model.getBasic().getOrder()) ? null : model.getBasic().getOrder().stream().map(ReportPropsSort::getValue).collect(Collectors.joining(" ")));
			o.setGroupBy(null);
			o.setGroupAs(null);
		}
		// this part as use predefined sql in first priority
		if (StringUtils.isNotBlank(sql)) {
			o.setSql(sql);
		}
		// this part as major query statement which is inserted into table for generating result set
		else if (null != model.getAdvance()) {
			o.setSql(ReportParamUtils.parse(model.getAdvance().getSql(), parameterList));
		}
		o.setLayout(model.getLayout());
		return o;
	}
}
