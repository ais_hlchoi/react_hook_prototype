package com.dbs.module.schedule.model;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.schedule.FileExport;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileExportModel implements FileExport {

	private String status;

	private String executionTime;

	public static FileExportModel of(String executionTime) {
		return FileExportModel.of(executionTime, null);
	}

	public static FileExportModel of(String executionTime, DownloadStatus status) {
		FileExportModel o = new FileExportModel();
		o.setExecutionTime(executionTime);
		o.setStatus((null == status ? DownloadStatus.PENDING : status).index());
		return o;
	}
}
