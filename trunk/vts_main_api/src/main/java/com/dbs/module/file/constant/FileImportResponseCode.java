package com.dbs.module.file.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.dbs.framework.constant.ResponseCode;

public enum FileImportResponseCode implements ResponseCode {

	E2001("No source file in queue.", 1),
	E2002("Service is stopped."),
	E2003("No data is processed."),
	E2004("Empty folder.",1),
	E2005("Service is running.", 1),
	E2006("File import with error."),
	E2011("File header is empty."),
	/*
	 * Effective header for ${headerName} not found
	 */
	E2012,
	/*
	 * Exception when createFileUploadData
	 */
	E2013,
	/*
	 * Exception when createFileUploadDataStatic
	 */
	E2014,
	/*
	 * Exception when updateFileUpdateDataIndex
	 */
	E2015,
	/*
	 * Count of field and column not match
	 */
	E2016,
	/*
	 * Skip to insert row data
	 */
	E2017,
	/*
	 * IOException when insertFileUploadData and reading file
	 */
	E2101,
	/*
	 * CsvValidationException when insertFileUploadData and reading file
	 */
	E2102,
	/*
	 * IllegalArgumentException when insertFileUploadData and reading file
	 */
	E2103,
	/*
	 * Error
	 */
	E2999,
	;

	private String code;
	private String desc;
	private Integer status;

	private FileImportResponseCode(String code, String desc, Integer status) {
		this.code = code;
		this.desc = desc;
		this.status = status;
	}

	private FileImportResponseCode(String desc, Integer status) {
		this.code = null;
		this.desc = desc;
		this.status = status;
	}

	private FileImportResponseCode(String desc) {
		this.code = null;
		this.desc = desc;
		this.status = null;
	}

	private FileImportResponseCode() {
		this.code = null;
		this.desc = null;
		this.status = null;
	}

	public String getCode() {
		return null == code ? name() : code;
	}

	public String getDesc() {
		return desc;
	}

	public Integer getStatus() {
		return null == status ? name().startsWith("E") ? -1 : 1 : status;
	}

	public static FileImportResponseCode getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static FileImportResponseCode getOrElse(String text, FileImportResponseCode o) {
		if (null == o)
			o = E2999;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return FileImportResponseCode.valueOf(text);
		} catch (IllegalArgumentException e) {
			return Stream.of(FileImportResponseCode.values()).filter(c -> c.getCode().equals(text)).findFirst().orElse(o);
		}
	}
}
