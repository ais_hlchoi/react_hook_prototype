package com.dbs.module.file;

public interface FileImportProps {

	String getRootPath();

	String getLoadPath();

	String getLoadProcessPath();

	String getLoadSuccessPath();

	String getLoadFailPath();

	String getFileImportPatternName();

	String getFileImportPatternExt();

	String getLoadImportPath(String fileName);
}
