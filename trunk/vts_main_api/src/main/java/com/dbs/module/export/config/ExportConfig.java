package com.dbs.module.export.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.dbs.module.download.config.DownloadConfig;
import com.dbs.util.FilenameUtils;

import lombok.AccessLevel;
import lombok.Getter;

@Configuration
@Getter
public class ExportConfig {

	@Autowired
	private DownloadConfig downloadConfig;

	@Value("${data.export.custom.report.xlsx-template}")
	@Getter(AccessLevel.NONE)
	private String excelTemplatePath;

	@Value("${data.export.custom.report.row-start}")
	private Integer rowStart;

	public String getExcelTemplatePath() {
		return FilenameUtils.toPathString(downloadConfig.getFolderRoot() + excelTemplatePath);
	}
}
