package com.dbs.module.export.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.export.Grid;
import com.dbs.module.export.util.DataParserUtils;
import com.dbs.module.report.constant.ReportLayoutType;

public class GridTableData implements Grid {

	private List<Map<String, Object>> data;
	private String layout;

	public GridTableData(List<Map<String, Object>> data) {
		this(data, ReportLayoutType.H.name());
	}

	public GridTableData(List<Map<String, Object>> data, String layout) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
		this.layout = ReportLayoutType.getOrElse(layout, ReportLayoutType.H).name();
	}

	protected List<Map<String, Object>> getData() {
		return data;
	}

	protected void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public String getLayout() {
		return layout;
	}

	protected void setLayout(String layout) {
		this.layout = layout;
	}

	public List<String> getHead() {
		if (isEmpty())
			return new ArrayList<>();

		return ReportLayoutType.V.name().equals(getLayout()) ? new ArrayList<>() : new ArrayList<>(getData().get(0).keySet());
	}

	public List<List<String>> getBody() {
		if (isEmpty())
			return new ArrayList<>();

		return (ReportLayoutType.V.name().equals(getLayout()) ? DataParserUtils.transpose(getData()) : getData()).stream().map(row -> row.values().stream().map(DataParserUtils::toString).collect(Collectors.toList())).collect(Collectors.toList());
	}

	public int size() {
		return CollectionUtils.isEmpty(getData()) ? 0 : getData().size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public boolean isNotEmpty() {
		return !isEmpty();
	}
}
