package com.dbs.module.export.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.dbs.framework.constant.ResponseCode;

public enum FileExportResponseCode implements ResponseCode {

	E3001("No outstanding report in queue.", 1),
	E3002("Service is stopped."),
	E3003("No data is processed."),
	E3004("Folder not exists."),
	/*
	 * Report template not found
	 */
	E3011,
	/*
	 * Report category not existed
	 */
	E3012,
	/*
	 * Reports in category not existed
	 */
	E3013,
	/*
	 * Exception when exporting data in XLSX
	 */
	E3101,
	/*
	 * Exception when exporting data in CSV
	 */
	E3102,
	/*
	 * Exception when exporting data in PDF
	 */
	E3103,
	/*
	 * Exception when merging data in XLSX
	 */
	E3111,
	/*
	 * Exception when merging data in CSV
	 */
	E3112,
	/*
	 * Exception when merging data in PDF
	 */
	E3113,
	/*
	 * Error
	 */
	E3999,
	;

	private String code;
	private String desc;
	private Integer status;

	private FileExportResponseCode(String code, String desc, Integer status) {
		this.code = code;
		this.desc = desc;
		this.status = status;
	}

	private FileExportResponseCode(String desc, Integer status) {
		this.code = null;
		this.desc = desc;
		this.status = status;
	}

	private FileExportResponseCode(String desc) {
		this.code = null;
		this.desc = desc;
		this.status = null;
	}

	private FileExportResponseCode() {
		this.code = null;
		this.desc = null;
		this.status = null;
	}

	public String getCode() {
		return null == code ? name() : code;
	}

	public String getDesc() {
		return desc;
	}

	public Integer getStatus() {
		return null == status ? name().startsWith("E") ? -1 : 1 : status;
	}

	public static FileExportResponseCode getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static FileExportResponseCode getOrElse(String text, FileExportResponseCode o) {
		if (null == o)
			o = E3999;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return FileExportResponseCode.valueOf(text);
		} catch (IllegalArgumentException e) {
			return Stream.of(FileExportResponseCode.values()).filter(c -> c.getCode().equals(text)).findFirst().orElse(o);
		}
	}
}
