package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.UserForm;

public interface ReportProps extends UserForm {

	ReportPropsFile getFile();

	List<ReportPropsTable> getTable();

	List<ReportPropsColumn> getColumn();

	List<ReportPropsFilter> getFilter();

	List<ReportPropsSort> getSort();

	List<ReportPropsCriteria> getAdvance();

	String getSql();

	String getLayout();

	String getName();

	Integer getId();

	String getActiveInd();
}
