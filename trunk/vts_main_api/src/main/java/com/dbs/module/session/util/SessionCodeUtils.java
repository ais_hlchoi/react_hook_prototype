package com.dbs.module.session.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;

import com.dbs.module.session.SessionCodeHelper;

public class SessionCodeUtils implements SessionCodeHelper {

	private SessionCodeUtils() {
		// for SonarQube scanning purpose
	}

	public static SessionCodeHelper getFactory() {
		return new SessionCodeUtils();
	}

	public String create() {
		return generate();
	}

	public String create(final int len) {
		int m = BLOCK;
		int n = Math.max(m, len);
		List<String> list = new ArrayList<>();
		while (n >= m) {
			list.add(generate());
			n -= m;
		}
		if (n > 0) {
			return String.format("%s%s%s", gen(n / 2), StringUtils.join(list, ""), gen(n / 2 + n % 2));
		}
		return StringUtils.join(list, "");
	}

	public boolean test(final String text) {
		return validate(text);
	}

	private static final int BLOCK = 6;

	public static String generate() {
		final int[] a = Stream.of(gen(BLOCK - 3).split("|")).mapToInt(Integer::parseInt).toArray();
		final int b = Integer.parseInt(gen(1));
		final int c = b % 5;
		final int x = b * 7 % 10;
		final int y = (10 + x - IntStream.of(a).sum() % 10) % 10;
		return IntStream.range(0, BLOCK).boxed().map(n -> String.valueOf(get(n, c, x, y, b, a))).collect(Collectors.joining());
	}

	private static String gen(final int n) {
		final int m = Math.min(Math.max(n, 1), 10);
		StringBuilder u = new StringBuilder();
		while (u.length() < n) {
			u.append(new StringBuilder(UUID.randomUUID().toString()).reverse().toString().replaceAll("\\D", ""));
		}
		return u.substring(0, m);
	}

	private static int get(final int n, final int c, final int x, final int y, final int b, final int[] a) {
		if (n == c)
			return x;
		if (n == BLOCK - 1)
			return b;
		final int i = n - (n < c ? 0 : 1);
		return i < a.length ? a[i] : y;
	}

	private static boolean verify(String text) {
		final int[] a = Stream.of(text.substring(0, BLOCK - 1).split("|")).mapToInt(Integer::parseInt).toArray();
		final int b = Integer.parseInt(text.substring(BLOCK - 1, BLOCK));
		final int c = b % 5;
		final int x = IntStream.range(0, a.length).map(j -> j == c ? 0 : a[j]).sum();
		return x % 10 == a[c];
	}

	public static boolean validate(final String text) {
		if (StringUtils.isBlank(text) || text.length() < BLOCK)
			return false;

		final int n = text.length() % BLOCK;
		final String t = text.substring(n / 2, text.length() - (n / 2 + n % 2));
		return IntStream.range(0, t.length() / BLOCK).boxed().map(i -> t.substring(BLOCK * i, BLOCK * (i + 1))).allMatch(SessionCodeUtils::verify);
	}
}
