package com.dbs.module.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportTemplateNameModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String name;

	@JsonInclude
	private String type;
}
