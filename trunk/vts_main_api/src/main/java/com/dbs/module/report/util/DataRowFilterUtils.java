package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.DataRowFilter;
import com.dbs.vtsp.model.SysParmModel;

public class DataRowFilterUtils extends MatcherUtils {

	private DataRowFilterUtils() {
		// for SonarQube scanning purpose
	}

	public static final String SEGMENT = "DATA_ROW_FILTER";

	private static final String INDEX = "INDEX";
	private static final String SP = ",";
	private static final String SP_BY_CHAR = "[" + SP + "]";

	public static DataRowFilter parse(final List<SysParmModel> list) {
		DataRowFilterProps o = new DataRowFilterProps();
		if (CollectionUtils.isEmpty(list))
			return o;
		SysParmModel index = list.stream().filter(item -> INDEX.equalsIgnoreCase(item.getCode())).findFirst().orElse(null);
		if (null == index || StringUtils.isBlank(index.getParmValue()))
			return o;
		parseFilter(index.getParmValue(), SP_BY_CHAR).stream().forEach(filter -> {
			o.addFilter(Stream.of(filter.split(SP))
					.map(code -> list.stream().filter(item -> code.equalsIgnoreCase(item.getCode())).findFirst().orElse(null))
					.filter(Objects::nonNull)
					.map(SysParmModel::getParmValue)
					.filter(StringUtils::isNotBlank)
					.map(MatcherUtils::parseFilterProps)
					.collect(Collectors.toList()));
		});
		return o;
	}

	private static List<String> parseFilter(final String text, final String format) {
		return new MatcherUtils().parseByChar(text, format);
	}

	protected static String parseColumnIndex(String[] args) {
		return args[ParmValueIndex.NAME.value];
	}

	protected static Object[] parseFilterIndex(final String[] columns, final String[] args) {
		return new Object[] { index(columns, args[FilterIndex.INDEX.value]), pattern(args[FilterIndex.PATTERN.value]), match(args[FilterIndex.MATCH.value]) };
	}
}

class DataRowFilterProps implements DataRowFilter {

	private List<List<String[]>> filters = new ArrayList<>();
	private Map<String, Object[]> index = new LinkedHashMap<>();
	private List<String> error = new ArrayList<>();

	public void addFilter(List<String[]> filter) {
		filters.add(filter);
	}

	public List<String> getErrors() {
		return error.stream().collect(Collectors.toList());
	}

	public String getError() {
		return hasError() ? error.get(0) : "";
	}

	public int getErrorCount() {
		return error.size();
	}

	public boolean hasError() {
		return CollectionUtils.isNotEmpty(error);
	}

	public void build(String[] columns) {
		index.clear();
		error.clear();
		error.addAll(DataRowFilterUtils.validate(filters));
		if (hasError())
			return;
		filters.stream().flatMap(f -> f.stream()).forEach(c -> index.put(DataRowFilterUtils.parseColumnIndex(c), DataRowFilterUtils.parseFilterIndex(columns, c) ));
	}

	public boolean test(String[] line) {
		return DataRowFilterUtils.test(line, filters, index);
	}

	public String toFilterString() {
		return filters.stream().flatMap(f -> f.stream().map(c -> StringUtils.join(c, ", "))).collect(Collectors.joining("\n"));
	}
}