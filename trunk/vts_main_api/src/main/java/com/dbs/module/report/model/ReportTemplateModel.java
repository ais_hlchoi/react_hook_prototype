package com.dbs.module.report.model;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportTemplateModel extends AbstractUserModel implements ReportTemplate {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private QueryBasicModel basic;

	@JsonInclude
	private QueryAdvanceModel advance;

	@JsonInclude
	private ReportOptionModel option;

	@JsonInclude
	private String name;

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String activeInd;

	public String getOutput() {
		return null == option || CollectionUtils.isEmpty(option.getOutput()) ? null : option.getOutput().get(0);
	}

	public String getLayout() {
		return null == option || StringUtils.isBlank(option.getLayout()) ? null : option.getLayout();
	}

	public String getType() {
		return null == option ? null : option.getType();
	}
}
