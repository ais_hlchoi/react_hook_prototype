package com.dbs.module.schedule.constant;

public class ReportScheduleConstants {

	public static final String EXECUTION_TIME_DEFAULT = "";

	public static final String EXECUTION_TIME_FOMRAT = "yyyy-MM-dd HH:mm:ss"; // date format in MariaDb as %Y-%m-%d %H:%i:%s
}
