package com.dbs.module.report;

public interface ReportPropsFilter {

	Integer getSeq();

	String getValue();
}
