package com.dbs.module.report;

public interface ReportPropsFile {

	Integer getId();

	String getLabel();
}
