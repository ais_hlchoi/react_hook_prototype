package com.dbs.module.download.model;

import java.util.List;

import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportDownloadObjectModel extends RptFileDnldModel {

	@JsonInclude
	private RptFileDnldDataModel data;

	@JsonInclude
	private String filePathPrefix;

	@JsonInclude
	private String parentStatus;

	@JsonInclude
	private List<ReportDownloadObjectModel> children;

	public ReportDownloadObjectModel() {
		super();
	}
}
