package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsSort;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputSortModel implements ReportPropsSort {

	@JsonInclude
	private Integer seq;

	@JsonInclude
	private String value;
}
