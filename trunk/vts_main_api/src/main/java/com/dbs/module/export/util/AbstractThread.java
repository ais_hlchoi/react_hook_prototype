package com.dbs.module.export.util;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.util.Assert;

import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.model.CommonExceptionHandling;
import com.dbs.module.export.DataExport;
import com.dbs.module.export.DataExportHelper;
import com.dbs.module.export.DataMergeHelper;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.EmailParamUtil;
import com.dbs.util.EmailRefTable;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.OperationLogUtils;
import com.dbs.util.OperationLogUtils.OperationLogStatus;
import com.dbs.util.RequestHeaderUtils;
import com.dbs.util.ResponseCodeUtils;
import com.dbs.vtsp.model.OperationLogModel;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;
import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class AbstractThread {

	abstract DataExportHelper getDataService();

	abstract SysEmailRequestService getEmailService();

	abstract SysEmailTmplService getEmailTmplService();

	abstract Logger getLogger();

	abstract String getFileDnldId();

	abstract String getProcessId();

	abstract void run();

	protected static final String MSG_DOWNLOAD_FORMAT = "%s -> [ fileDnldId: %s, processId: %s ]";

	protected void export(DataExportHelper service, ReportTemplate model, String pid, Integer id) throws SQLException, IOException {
		Assert.notNull(service, "Service cannot be null");
		Assert.notNull(model, "Data cannot be null");
		Assert.notNull(id, "Id cannot be null");

		RptFileDnldModel dnload = service.getFileDownload(id);
		Assert.notNull(dnload, "Download task cannot be null");

		RptFileDnldDataModel data = service.getFileDownloadData(id);
		Assert.notNull(data, "Download task cannot be null");

		ExportOption output = ExportOption.getOrElse(dnload.getFileExtension());
		DataExport bean = DataExportUtils.parse(model, data.getRptSql());
		boolean file = false;
		switch (output) {
		case XLSX:
			file = service.exportXlsx(bean, pid, id);
			break;
		case CSV:
			file = service.exportCsv(bean, pid, id);
			break;
		case PDF:
			file = service.exportPdf(bean, pid, id);
			break;
		}
		Assert.isTrue(file, "Output file cannot be null");
	}

	protected void merge(DataMergeHelper service, String fileExtension, String pid, Integer id) throws IOException {
		Assert.notNull(service, "Service cannot be null");
		Assert.notNull(fileExtension, "File Extension cannot be null");
		Assert.notNull(id, "Id cannot be null");

		ExportOption output = ExportOption.getOrElse(fileExtension);
		boolean file = false;
		switch (output) {
		case XLSX:
			file = service.exportXlsx(pid, id);
			break;
		case CSV:
			file = service.exportCsv(pid, id);
			break;
		case PDF:
			file = service.exportPdf(pid, id);
			break;
		}
		Assert.isTrue(file, "Output file cannot be null");
	}

	protected void sendOperationLogFail(String api, String info, String error) {
		if (StringUtils.isBlank(api))
			return;

		OperationLogModel log = new OperationLogModel(new Date(), RequestHeaderUtils.getClientIPAddress(null));
		log.setUserId(CurrentUserUtils.getOneBankId());
		log.setOperationType(OperationLogAction.DOWNLOAD.name());
		log.setResponseCode(ResponseCodeUtils.getFailure());
		log.setResponseDesc(CommonExceptionHandling.formatError(error));

		Map<String, Object> content = new LinkedHashMap<>();
		content.put("service", info);
		content.put("error", log.getResponseCode());
		content.put("result", log.getResponseDesc());

		log.setOperationContentObject(content);
		log.setStatus(OperationLogStatus.getOrElse(log.getResponseCode()).name());
		log.setResponseTime(new Date());

		try {
			OperationLogUtils.insert(log, api);
		} catch (JsonProcessingException e) {
			getLogger().error(FortifyStrategyUtils.toErrString(e));
		}
	}

	protected void sendEmailAlert(EmailAlertEnum type, Integer id, String name, String msg, String... attach) {
		if (null == getDataService() || null == getEmailService() || null == getEmailTmplService())
			return;

		RptFileDnldDataModel data = getDataService().getFileDownloadData(id);
		if (null == data || null == data.getRefSched())
			return;

		SysEmailTmplModel email = getEmailTmplService().getSysEmailTmpl(data.getRefSched(), EmailRefTable.RPT_SETUP_SCHED.name());
		List<String> attachment = null == attach ? new ArrayList<>() : Arrays.asList(attach);

		HashMap<EmailParamUtil, String> map = EmailParamUtil.getDefaultHashMap();
		map.put(EmailParamUtil.RPT_NAME, name);
		map.put(EmailParamUtil.ERR_MSG, msg);
		map.put(EmailParamUtil.LAST_UPDATEED_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		getEmailService().requestEmail(map, email, attachment);
	}
}
