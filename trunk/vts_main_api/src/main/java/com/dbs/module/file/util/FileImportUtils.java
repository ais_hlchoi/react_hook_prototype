package com.dbs.module.file.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.config.ImportConfig;
import com.dbs.module.file.config.ImportS3Config;

@Primary
@Service
public class FileImportUtils extends AbstractFileImportUtils {

	@Autowired
	private ImportConfig importConfig;

	@Autowired
	private ImportS3Config importS3Config;

	private FileImport fileImport;

	protected FileImport getService() {
		if (null == fileImport) {
			fileImport = S3ClientUtils.isService() ? new S3StorageUtils(importS3Config) : new LocalStorageUtils(importConfig);
		}
		return fileImport;
	}
}
