package com.dbs.module.report;

import com.dbs.module.model.UserForm;

public interface ReportTemplate extends UserForm {

	ReportPropsBasic getBasic();

	ReportPropsAdvance getAdvance();

	ReportPropsOption getOption();

	String getOutput();

	String getLayout();

	String getType();

	String getName();

	Integer getId();

	String getActiveInd();
}
