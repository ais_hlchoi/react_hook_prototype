package com.dbs.module.file.util;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.dbs.module.file.FileImport;
import com.dbs.module.file.FileImportProps;
import com.dbs.module.model.FileModel;
import com.dbs.module.model.FileProps;
import com.opencsv.CSVReader;

public abstract class AbstractFileImportUtils implements FileImport {

	protected AbstractFileImportUtils() {
		// for SonarQube scanning purpose
	}

	abstract FileImport getService();

	public FileProps[] list() {
		return getService().list(getProps().getLoadPath(), null);
	}

	public FileProps[] list(String path) {
		return getService().list(path, null);
	}

	public FileProps[] list(String path, String prefix) {
		return getService().list(path, prefix);
	}

	public void copy(String source, String destination) {
		getService().copy(source, destination);
	}

	public void move(String source, String destination) {
		getService().move(source, destination);
	}

	public File getFile(String name) {
		return getService().getFile(name);
	}

	public byte[] getFileByte(String name) {
		return getService().getFileByte(name);
	}

	public InputStream getStream(String name) {
		return getService().getStream(name);
	}

	public CSVReader getCsvReader(String name) {
		return getService().getCsvReader(name);
	}

	public boolean isFile(String name) {
		return getService().isFile(name);
	}

	public boolean isMatch(String name, String pattern, String ext, String parent) {
		return getService().isMatch(name, pattern, ext, parent);
	}

	public List<String> prepare(String... paths) {
		return getService().prepare(paths);
	}

	public FileImportProps getProps() {
		return getService().getProps();
	}

	public Object saveFile(FileModel fileModel) {
		return getService().saveFile(fileModel);
	}

	public Object saveFile(FileModel fileModel, String storageKey) {
		return getService().saveFile(fileModel, storageKey);
	}

	public Object saveFile(String fileName, String fileExtension, String content) {
		return getService().saveFile(fileName, fileExtension, content);
	}

	public FileModel[] getFileModel() {
		return getService().getFileModel();
	}

	public FileModel[] getFileModel(String path, String prefix) {
		return getService().getFileModel(path, prefix);
	}

	public boolean exist(String destination) {
		return getService().exist(destination);
	}

	public boolean delete(String destination) {
		return getService().delete(destination);
	}
}
