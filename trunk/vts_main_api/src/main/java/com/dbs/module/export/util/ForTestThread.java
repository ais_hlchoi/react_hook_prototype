package com.dbs.module.export.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import com.dbs.module.export.DataExportHelper;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;

public class ForTestThread extends AbstractThread implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	public ForTestThread() {
		
	}

	public void run() {
		StopWatch timer = new StopWatch();
		timer.start();
		try {			
			logger.info("Started.");
			Thread.sleep(3000);
			logger.info("Ended");
			
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
		} catch (InterruptedException e) {
			logger.error("interrupted:" + e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		timer.stop();
		logger.info("end agai.");
	}

	@Override
	DataExportHelper getDataService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	SysEmailRequestService getEmailService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	SysEmailTmplService getEmailTmplService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	String getFileDnldId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	String getProcessId() {
		// TODO Auto-generated method stub
		return null;
	}

}
