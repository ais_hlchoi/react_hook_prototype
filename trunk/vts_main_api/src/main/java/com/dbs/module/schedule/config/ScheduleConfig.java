package com.dbs.module.schedule.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class ScheduleConfig {

	@Value("${schedule.data.export}")
	private String scheduleDataExportInterval;

	@Value("${schedule.data.import}")
	private String scheduleDataImportInterval;
}
