package com.dbs.module.file.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.aws.util.S3Service;
import com.dbs.module.aws.util.S3Service.ListObjectsV2RequestProps;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.FileImportProps;
import com.dbs.module.model.FileModel;
import com.dbs.module.model.FileProps;
import com.dbs.util.Assert;
import com.dbs.util.FilenameUtils;
import com.opencsv.CSVReader;

@Service
public class S3StorageUtils implements FileImport {

	private S3Service service;
	private String bucket;
	private FileImportProps props;

	public FileImportProps getProps() {
		return props;
	}

	protected S3StorageUtils() {
		// for SonarQube scanning purpose
	}

	public S3StorageUtils(FileImportProps props) {
		this.service = S3ClientUtils.getS3Service();
		this.bucket = S3ClientUtils.getBucketName();
		this.props = props;
	}

	public FileProps[] list() {
		return list(getProps().getLoadPath(), null);
	}

	public FileProps[] list(String path) {
		return list(path, null);
	}

	public FileProps[] list(String path, String prefix) {
		String pathAndPrefix = (StringUtils.isBlank(path) ? "" : path) + (StringUtils.isBlank(prefix) ? "" : prefix);
		return service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().map(o -> FileProps.of(o.getKey(), o.getSize())).toArray(FileProps[]::new);
	}

	public FileModel[] getFileModel() {
		return getFileModel(getProps().getLoadPath(), null);
	}

	public FileModel[] getFileModel(String path, String prefix) {
		String pathAndPrefix = (StringUtils.isBlank(path) ? "" : path) + (StringUtils.isBlank(prefix) ? "" : prefix);
		return service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().map(s3ObjectSummary -> {
			String key = s3ObjectSummary.getKey();
			String fileName = String.format("%s.%s", FilenameUtils.getBaseName(key), FilenameUtils.getExtension(key));
			return FileModel.of(FilenameUtils.getFullPath(key), FilenameUtils.getBaseName(fileName), FilenameUtils.getExtension(fileName), key);
		}).toArray(FileModel[]::new);
	}

	public void copy(String source, String destination) {
		service.copyObject(bucket, FilenameUtils.toUriString(source), bucket, FilenameUtils.toUriString(destination));
	}

	public void move(String source, String destination) {
		service.copyObject(bucket, FilenameUtils.toUriString(source), bucket, FilenameUtils.toUriString(destination));
		service.deleteObject(bucket, FilenameUtils.toUriString(source));
	}

	public File getFile(String name) {
		// TODO
		throw new NotImplementedException();
	}

	public byte[] getFileByte(String name) {
		FileModel[] models = service.listObjectsV2(bucket, Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(name))).getObjectSummaries().stream().map(s3ObjectSummary -> {
			String key = s3ObjectSummary.getKey();
			String fileName = String.format("%s.%s", FilenameUtils.getBaseName(key), FilenameUtils.getExtension(key));
			FileModel fileModel = new FileModel();
			fileModel.setFilePath(FilenameUtils.getFullPath(key));
			fileModel.setFileName(FilenameUtils.getBaseName(fileName));
			fileModel.setFileExtension(FilenameUtils.getExtension(fileName));
			fileModel.setFullPathNameExtension(key);
			S3Object s3Object = service.getObject(bucket, key);
			try {
				byte[] byteArray = IOUtils.toByteArray(s3Object.getObjectContent());
				fileModel.setBytes(byteArray);
			} catch (IOException e) {
				// no action
			}
			return fileModel;
		}).toArray(FileModel[]::new);
		Assert.isTrue(models.length == 1, String.format("Found more than one file with this extension"));
		Assert.isTrue(models[0].getBytes() != null, String.format("File not existed"));
		return models[0].getBytes();
	}

	public InputStream getStream(String name) {
		return service.getObject(bucket, FilenameUtils.toUriString(name)).getObjectContent();
	}

	public CSVReader getCsvReader(String name) {
		return new CSVReader(new InputStreamReader(getStream(name)));
	}

	public boolean isFile(String name) {
		return StringUtils.isNotBlank(FilenameUtils.getExtension(name));
	}

	public boolean isMatch(String name, String pattern, String ext, String parent) {
		return isFile(name) && FilenameUtils.match(name, pattern, ext) && FilenameUtils.matchParent(name, parent);
	}

	public List<String> prepare(String... paths) {
		return IntStream.range(0, paths.length).boxed().filter(n -> StringUtils.isBlank(paths[n])).map(n -> String.format("Path is empty in prepare.path[%d]", n)).collect(Collectors.toList());
	}

	public Object saveFile(FileModel file) {
		String storageKey = saveFileGetStorageKey(file);
		return service.putObject(bucket, storageKey, file.getFile());
	}

	public Object saveFile(FileModel file, String storageKey) {
		return service.putObject(bucket, storageKey, file.getFile());
	}

	public Object saveFile(String fileName, String fileExtension, String content) {
		FileModel fileModel = new FileModel();
		fileModel.setFileName(fileName);
		fileModel.setFileExtension(fileExtension);
		String storageKey = saveFileGetStorageKey(fileModel); // No specific FileName
		return service.putObject(bucket, storageKey, content);
	}

	private String saveFileGetStorageKey(FileModel fileModel) {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy_HHmm");
		Date date = new Date();
		String dateString = formatter.format(date);
		return getProps().getLoadPath() + fileModel.getFileName() + "_" + dateString + "." + fileModel.getFileExtension();
	}

	public boolean exist(String name) {
		return service.doesObjectExist(bucket, name);
	}

	public boolean delete(String name) {
		service.deleteObject(bucket, name);
		return true;
	}
}
