package com.dbs.module.model;

import java.sql.Timestamp;
import java.util.List;

public interface UserForm {

	String getOneBankId();

	void setOneBankId(String oneBankId);

	String getOneBankDisplayName();

	void setOneBankDisplayName(String oneBankDisplayName);

	List<String> getOneBankRoleList();

	void setOneBankRoleList(List<String> oneBankRoleList);

	Timestamp getLastUpdatedDate();

	void setLastUpdatedTime(Long... lastUpdatedTime);
}
