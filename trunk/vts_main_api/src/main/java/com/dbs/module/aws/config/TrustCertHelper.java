package com.dbs.module.aws.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.http.conn.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.util.FortifyStrategyUtils;

public class TrustCertHelper {

	private static final Logger logger = LoggerFactory.getLogger(TrustCertHelper.class);

	private TrustCertHelper() {
		// for SonarQube scanning purpose
	}

	protected static TrustStrategy getTrustCertificate() {
		return new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				return true;
			}
		};
	}

	@SuppressWarnings("deprecation")
	protected static org.apache.http.conn.ssl.AllowAllHostnameVerifier getTrustVerifier() {
		return new org.apache.http.conn.ssl.AllowAllHostnameVerifier();
	}

	@SuppressWarnings("deprecation")
	public static org.apache.http.conn.ssl.SSLSocketFactory getSSLSocketFactory() {
		org.apache.http.conn.ssl.SSLSocketFactory socketFactory = null;
		try {
			socketFactory = new org.apache.http.conn.ssl.SSLSocketFactory(getTrustCertificate(), getTrustVerifier());
		} catch (KeyManagementException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}
		return socketFactory;
	}
}
