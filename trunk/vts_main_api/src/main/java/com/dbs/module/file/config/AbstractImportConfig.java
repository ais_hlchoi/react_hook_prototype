package com.dbs.module.file.config;

import com.dbs.util.FilenameUtils;

public abstract class AbstractImportConfig {

	abstract String getLoadPath();

	public String getLoadImportPath(String fileName) {
		return FilenameUtils.normalize(getLoadPath() + fileName);
	}
}
