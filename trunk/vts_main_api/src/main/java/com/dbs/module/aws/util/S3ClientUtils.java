package com.dbs.module.aws.util;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.dbs.module.aws.config.S3Config;
import com.dbs.module.aws.config.TrustCertHelper;

@Component
public class S3ClientUtils {

	private static AmazonS3 s3Client = null;
	private static String bucketname = null;
	private static boolean service = false;

	@Autowired
	private S3Config s3Config;

	@PostConstruct
	private void init() {
		if (BooleanUtils.isNotTrue(BooleanUtils.toBoolean(s3Config.getService())))
			return;

		ClientConfiguration client = new ClientConfiguration();
		client.getApacheHttpClientConfig().setSslSocketFactory(TrustCertHelper.getSSLSocketFactory());
		S3ClientUtils.s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Config.getAccessId(), s3Config.getSecretKey())))
				.withEndpointConfiguration(new EndpointConfiguration(s3Config.getEndPoint(), s3Config.getRegion()))
				.withClientConfiguration(client)
				.build();
		S3ClientUtils.bucketname = s3Config.getBucketname();
		S3ClientUtils.service = true;
	}

	public static S3Service getS3Service() {
		return new S3Service(s3Client);
	}

	public static String getBucketName() {
		return bucketname;
	}

	public static boolean isService() {
		return service;
	}
}
