package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.util.FortifyStrategyUtils;

public class MatcherUtils {

	protected MatcherUtils() {
		// for SonarQube scanning purpose
	}

	protected final static String ANY = "*";
	protected final static String EMPTY = "";
	protected final static String TRUE = "TRUE";
	protected final static String FALSE = "FALSE";
	protected final static String TESTING_VALUE = "1234HK-123";

	protected final static int INDEX_IGNORE = -1;
	protected final static int INDEX_ALWAYS = -2;

	public static List<String> match(final String text, final String format) {
		return new MatcherUtils().parseByPattern(text, format);
	}

	protected static String[] parseFilterProps(final String text) {
		if (TRUE.equalsIgnoreCase(text))
			return new String[] { ANY, TRUE, EMPTY };
		if (FALSE.equalsIgnoreCase(text))
			return new String[] { ANY, FALSE, EMPTY };
		String[] arg = text.split(":");
		String column = arg[0].toUpperCase();
		String filter = arg.length == 1 ? EMPTY : arg[1];
		if (StringUtils.isBlank(filter))
			return new String[] { column, EMPTY, EMPTY };
		Pattern p = Pattern.compile("^(!)?(.+)$");
		Matcher m = p.matcher(filter);
		if (m.find()) {
			return new String[] { column, m.group(2), m.group(1) };
		}
		return new String[] { column, filter, EMPTY };
	}

	protected static String[] parseOutputProps(final String text) {
		String[] arg = text.split(":");
		return new String[] { arg[0].toUpperCase(), arg.length == 1 ? EMPTY : arg[1] };
	}

	// simple way to parse text by pattern
	protected List<String> parseByPattern(String text, String format) {
		List<String> list = new ArrayList<>();
		if (StringUtils.isBlank(text) || StringUtils.isBlank(format))
			return list;
		Pattern p = Pattern.compile(format);
		Matcher m = p.matcher(text);
		while (m.find()) {
			list.add(m.group(1));
		}
		return list;
	}

	// simple way to parse text excluding any escape char handling
	protected List<String> parseByChar(String text, String format) {
		List<String> list = new ArrayList<>();
		if (StringUtils.isBlank(text) || StringUtils.isBlank(format))
			return list;
		char[] sp = format.toCharArray();
		char[] ca = text.toCharArray();
		boolean w = true; // writing char
		int x = 0; // inner-array
		int m = 0; // start char position from text
		String c = String.valueOf(sp[1]);
		for (int n = 0; n < ca.length; n++) {
			// next is an array
			if (w && ca[n] == sp[0]) {
				add(list, text, m, n, w, c);
				m = n;
				w = false;
			}
			// push value into list
			else if (w && ca[n] == sp[1]) {
				add(list, text, m, n, w, c);
				m = n + 1;
			}
			// next is an inner-array in array
			else if (!w && ca[n] == sp[0]) {
				x++;
			}
			else if (!w && ca[n] == sp[2] && x > 0) {
				x--;
			}
			// push array into list
			else if (!w && ca[n] == sp[2]) {
				add(list, text, m, n, w, c);
				m = n + 1;
				w = true;
			}
		}
		add(list, text, m, ca.length, w, c);
		return list;
	}

	private void add(List<String> list, String text, int m, int n, boolean w, String c) {
		if (m == n)
			return;
		String t = text.substring(m, n + (w ? 0 : 1)).replaceAll("\\[|\\]", c).replaceAll(c + "+", c).replaceAll("^" + c + "|" + c + "$", EMPTY).trim();
		if (StringUtils.isNotBlank(t))
			list.add(t);
	}

	protected static Integer index(final String[] columns, final String column) {
		if (null == columns || columns.length < 1 || StringUtils.isBlank(column))
			return INDEX_IGNORE;
		if (ANY.equals(column))
			return INDEX_ALWAYS;
		return IntStream.range(0, columns.length).boxed().filter(n -> columns[n].equalsIgnoreCase(column)).findFirst().orElse(INDEX_IGNORE);
	}

	protected static Pattern pattern(final String text) {
		return StringUtils.isBlank(text) ? null : Pattern.compile(text);
	}

	protected static Boolean match(final String text) {
		return StringUtils.isBlank(text) || !"!".equals(text);
	}

	protected static String output(final String text) {
		return StringUtils.isBlank(text) ? EMPTY : text;
	}

	protected static boolean test(final String[] line, final List<List<String[]>> filters, final Map<String, Object[]> index) {
		if (null == line || line.length < 1)
			return false;
		if (CollectionUtils.isEmpty(filters) || MapUtils.isEmpty(index))
			return true;
		return filters.stream().allMatch(and -> and.stream().anyMatch(or -> {
			if (null == or || or.length < 1)
				return false;
			Object[] arg = index.get(or[ParmValueIndex.NAME.value]);
			Integer n = null == arg || arg.length < FilterIndex.INDEX.value ? null : (Integer) arg[FilterIndex.INDEX.value];
			Pattern p = null == arg || arg.length < FilterIndex.PATTERN.value ? null : (Pattern) arg[FilterIndex.PATTERN.value];
			Boolean b = null == arg || arg.length < FilterIndex.MATCH.value ? null : (Boolean) arg[FilterIndex.MATCH.value];
			boolean m = check(line, n, p);
			return b ? m : !m;
		}));
	}

	private static boolean check(final String[] line, final Integer n, final Pattern p) {
		if (null == n || INDEX_IGNORE == n)
			return null == p;
		if (null == p)
			return StringUtils.isBlank(line[n]);
		if (INDEX_ALWAYS == n)
			return TRUE.equals(p.toString());
		return p.matcher(line[n]).matches();
	}

	protected static String[] apply(final String[] line, final List<List<String[]>> filters, final Map<String, Object[]> index, final Map<Integer, String> delta) {
		if (null == line || line.length < 1)
			return new String[] {};
		if (CollectionUtils.isEmpty(filters) || MapUtils.isEmpty(index) || !test(line, filters, index))
			return line;
		return IntStream.range(0, line.length).boxed().map(n -> delta.containsKey(n) ? delta.get(n) : line[n]).toArray(String[]::new);
	}

	protected static List<String> validate(final List<List<String[]>> filters) {
		List<String> error = new ArrayList<>();
		if (CollectionUtils.isEmpty(filters))
			return error;
		filters.stream().flatMap(f -> f.stream()).forEach(c -> {
			if (null == c)
				return;
			String name = c.length < ParmValueIndex.NAME.value ? null : c[ParmValueIndex.NAME.value];
			String regex = c.length < ParmValueIndex.REGEX.value ? null : c[ParmValueIndex.REGEX.value];
			if (StringUtils.isBlank(regex))
				return;
			try {
				Pattern.compile(regex).matcher(TESTING_VALUE);
			} catch (Exception e) {
				error.add(String.format("Error found at %s caused by %s", FortifyStrategyUtils.toLogString(name, regex), FortifyStrategyUtils.toErrString(e)));
			}
		});
		return error;
	}

	protected enum ParmValueIndex {
		  NAME(0)
		, REGEX(1)
		, MATCH(2)
		;
		public final int value;
		private ParmValueIndex(int value) {
			this.value = value;
		}
	}

	protected enum FilterIndex {
		  INDEX(0)
		, PATTERN(1)
		, MATCH(2)
		;
		public final int value;
		private FilterIndex(int value) {
			this.value = value;
		}
	}

	protected enum OutputIndex {
		  INDEX(0)
		, VALUE(1)
		;
		public final int value;
		private OutputIndex(int value) {
			this.value = value;
		}
	}
}
