package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsAdvance;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryAdvanceModel implements ReportPropsAdvance {

	@JsonInclude(Include.NON_NULL)
	private String sql;
}
