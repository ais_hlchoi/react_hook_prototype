package com.dbs.module.export.util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.data.ReportDownloadCategoryData;
import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.export.DataExportHelper;
import com.dbs.module.file.constant.UploadStatus;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.util.ReportTemplateUtils;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.service.DataMergeService;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;

public class DataMergeThread extends AbstractThread implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final List<Integer> MERGING = new ArrayList<Integer>();

	private DataMergeService dataMergeService;
	private SysEmailRequestService sysEmailRequestService;
	private SysEmailTmplService sysEmailTmplService;
	private String sysLogApi;

	private ReportDownloadCategoryData model;
	private Integer childId;
	private Integer id;
	private String pid;

	public DataMergeThread sysLogApi(String sysLogApi) {
		this.sysLogApi = sysLogApi;
		return this;
	}

	public DataMergeThread pid(String pid) {
		this.pid = pid;
		logger.info(String.format("%s export [ assign ]", String.format(MSG_DOWNLOAD_FORMAT, ClassPathUtils.getName(), getFileDnldId(), getProcessId())));
		return this;
	}

	protected DataExportHelper getDataService() {
		return dataMergeService;
	}

	protected SysEmailRequestService getEmailService() {
		return sysEmailRequestService;
	}

	protected SysEmailTmplService getEmailTmplService() {
		return sysEmailTmplService;
	}

	protected Logger getLogger() {
		return logger;
	}

	protected String getFileDnldId() {
		return String.format("%s -> %s", model.getId(), childId);
	}

	protected String getProcessId() {
		return pid;
	}

	public DataMergeThread(DataMergeService dataMergeService, SysEmailRequestService sysEmailRequestService, SysEmailTmplService sysEmailTmplService, ReportDownloadCategoryData model, Integer childId, Integer id) {
		Assert.notNull(dataMergeService, "Service cannot be null");
		Assert.notNull(model, "Data cannot be null");
		Assert.notNull(model.getId(), "Id cannot be null");
		Assert.isTrue(isValidId(childId, model), "Report not found");
		this.dataMergeService = dataMergeService;
		this.sysEmailRequestService = sysEmailRequestService;
		this.model = model;
		this.childId = childId;
		this.id = id;
	}

	public void run() {
		if (UploadStatus.COMPLETE == UploadStatus.getOrElse(model.getStatus(), UploadStatus.PENDING)) {
			logger.info(String.format(MSG_DOWNLOAD_FORMAT + " done already", ClassPathUtils.getName(), model.getId(), getProcessId()));
			return;
		}
		StopWatch timer = new StopWatch();
		String info = String.format(MSG_DOWNLOAD_FORMAT, ClassPathUtils.getName(), getFileDnldId(), getProcessId());
		String error = null;
		boolean done = false;
		boolean fail = false;
		boolean merge = false;
		logger.info(String.format("%s start", info));
		timer.start();
		try {
			dataMergeService.updateFileDownloadStatus(DownloadStatus.PREPARE, getProcessId(), id);
			int m = model.getChildren().size();
			int n = 0;
			// export file by file sequentially
			if (isPrintAll(childId)) {
				List<String> errors = new ArrayList<>();
				for (ReportDownloadObjectModel child : model.getChildren()) {
					export(child, getProcessId(), info, m, n, errors);
					childId = child.getId();
					n++;
				}
				errors.stream().forEach(logger::warn);
			}
			// export specified file
			else if (!isEmptyId(childId)) {
				n = getIndex(childId, model);
				export(model, childId, getProcessId(), info, m, n);
				done = true;
			}
			// merge files
			if (isCompleted(childId) && !MERGING.contains(model.getId())) {
				MERGING.add(model.getId());
				logger.info(String.format("Outstanding %s item(s) -> [ %s ]", MERGING.size(), StringUtils.join(MERGING, ", ")));
				logger.info(String.format("%s merge from %s", info, m));
				dataMergeService.updateFileDownloadStatus(DownloadStatus.PROCESS, getProcessId(), id);
				merge(dataMergeService, model.getFileExtension(), getProcessId(), id);
				done = true;
				merge = true;
			}
		} catch (IllegalArgumentException e) {
			logger.warn(String.format("%s merge [ warn ] caused by %s", info, FortifyStrategyUtils.toErrString(e)));
			error = e.getMessage();
			fail = true;
		} catch (Exception e) {
			logger.error(String.format("%s merge [ error ] caused by %s", info, FortifyStrategyUtils.toErrString(e)));
			error = e.getMessage();
			fail = true;
		} finally {
			if (merge)
				MERGING.remove(model.getId());

			if (CollectionUtils.isEmpty(MERGING))
				logger.info("Outstanding 0 item");
			else
				logger.info(String.format("Outstanding %s item(s) -> [ %s ]", MERGING.size(), StringUtils.join(MERGING, ", ")));
		}
		timer.stop();
		// case when error found
		if (fail) {
			logger.info(String.format("%s fail (%s ms)", info, timer.getTotalTimeMillis()));
			dataMergeService.updateFileDownloadStatus(DownloadStatus.FAILURE, getProcessId(), id);
			sendOperationLogFail(sysLogApi, info, error);
		}
		// case when run success
		else if (done) {
			logger.info(String.format("%s done (%s ms)", info, timer.getTotalTimeMillis()));

			if (merge)
				sendEmailAlert(EmailAlertEnum.EMAIL_ALERT_DONE_SCHED_RPT, id, model.getRptName(), "Export group report successfully");
		}
	}

	private void export(ReportDownloadCategoryData model, Integer childId, String pid, String info, int m, int n) throws SQLException, IOException {
		logger.info(String.format("%s export %s of %s", info, n + 1, m));
		dataMergeService.updateFileDownloadStatus(DownloadStatus.PREPARE, getProcessId(), childId);
		export(dataMergeService, getChild(childId, model), getProcessId(), childId);
	}

	private void export(ReportDownloadObjectModel child, String pid, String info, int m, int n, List<String> errors) throws SQLException, IOException {
		try {
			logger.info(String.format("%s export %s of %s [ async ]", info, n + 1, m));
			dataMergeService.updateFileDownloadStatus(DownloadStatus.PREPARE, pid, child.getId());
			export(dataMergeService, ReportTemplateUtils.parse(child), pid, child.getId());
		} catch (IllegalArgumentException e) {
			errors.add(String.format("% export %s of %s caused by %s", info, n + 1, m, FortifyStrategyUtils.toErrString(e)));
		}
	}

	protected boolean isEmptyId(Integer id) {
		return null == id || id < 0;
	}

	protected boolean isValidId(Integer id, ReportDownloadCategoryData model) {
		return isEmptyId(id) || CollectionUtils.isNotEmpty(model.getChildren()) && model.getChildren().stream().anyMatch(o -> id.equals(o.getId()));
	}

	protected boolean isPrintAll(Integer id) {
		return null == id;
	}

	protected boolean isCompleted(Integer id) {
		List<ReportDownloadStatusData> list = dataMergeService.getReportDownloadStatusList(id);
		if (CollectionUtils.isEmpty(list))
			return false;

		return list.stream().allMatch(ReportDownloadStatusData::isCompleted);
	}

	protected Integer getIndex(Integer id, ReportDownloadCategoryData model) {
		return isEmptyId(id) ? -1 : IntStream.range(0, model.getChildren().size()).boxed().filter(o -> id.equals(model.getChildren().get(o).getId())).findFirst().orElse(-1);
	}

	protected ReportTemplate getChild(Integer id, ReportDownloadCategoryData model) {
		if (null == model || null == id)
			return null;

		ReportDownloadObjectModel child = isEmptyId(id) ? null : model.getChildren().stream().filter(o -> id.equals(o.getId())).findFirst().orElse(null);
		return null == child ? null : ReportTemplateUtils.parse(child);
	}
}
