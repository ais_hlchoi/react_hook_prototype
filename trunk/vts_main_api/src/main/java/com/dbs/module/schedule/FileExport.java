package com.dbs.module.schedule;

public interface FileExport {

	String getStatus();

	String getExecutionTime();
}
