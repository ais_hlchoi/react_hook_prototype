package com.dbs.module.report;

import java.util.Map;

public interface ReportPropsParameter {

	String getName();

	String getDatatype();

	String getLabel();

	String getFormat();

	String getValue();

	Integer getRptTmplId();

	ReportPropsComparator getComparator();

	String getMandatory();

	Map<String, String> getAttributes();

	String getSegment();

	String getText();
}
