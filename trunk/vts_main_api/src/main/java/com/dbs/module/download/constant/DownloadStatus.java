package com.dbs.module.download.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum DownloadStatus {

	/**
	 * Status = 'I'
	 */
	INITIAL ("I", "Initial"),
	/**
	 * Status = 'P'
	 */
	PENDING ("P", "Pending"),
	/**
	 * Status = 'A'
	 */
	PREPARE ("A", "Preparing"),
	/**
	 * Status = 'R'
	 */
	PROCESS ("R", "Processing"),
	/**
	 * Status = 'F'
	 */
	FAILURE ("F", "Failure"),
	/**
	 * Status = 'C'
	 */
	COMPLETE("C", "Completed"),
	/**
	 * Status = 'U'
	 */
	UNKNOWN ("U", null),
	;

	private String index;
	private String label;

	private DownloadStatus(String index, String label) {
		this.index = index;
		this.label = label;
	}

	public String index() {
		return this.index;
	}

	public String label() {
		return this.label;
	}

	public static DownloadStatus getOrElse(String text) {
		return getOrElse(text, UNKNOWN);
	}

	public static DownloadStatus getOrElse(String text, DownloadStatus o) {
		if (null == o)
			o = UNKNOWN;

		if (StringUtils.isBlank(text))
			return o;

		try {
			if (text.length() == 1)
				return Stream.of(DownloadStatus.values()).filter(c -> text.equalsIgnoreCase(c.index())).findFirst().orElse(o);
			else
				return DownloadStatus.valueOf(text);
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
