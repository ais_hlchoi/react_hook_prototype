package com.dbs.module.export.util;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.dbs.module.download.util.FileDownloadUtils;
import com.dbs.util.FilenameUtils;
import com.dbs.util.FortifyStrategyUtils;

@Component
public class ReportFilePathUtils {

	private ReportFilePathUtils() {
		// for SonarQube scanning purpose
	}

	public static final String ATTACH_FILE_NAME_PREFIX = "custom_report";
	public static final String ATTACH_FILE_NAME_SUBFIX = "_%1$s_%2$s";

	public static String getAttachFileFullPath(final String folderOutput, final String ext, final Date date, final Integer seq) {
		return getAttachFileFullPath(folderOutput, ATTACH_FILE_NAME_PREFIX, ext, date, seq);
	}

	public static String getAttachFileFullPath(final String folderOutput, final String name, final String ext, final Date date, final Integer seq) {
		return folderOutput + getAttachFileName(name, ext, date, seq);
	}

	public static String getAttachFileName(final String ext) {
		return getAttachFileName(ATTACH_FILE_NAME_PREFIX, ext, new Date(), null);
	}

	public static String getAttachFileName(final String name, final String ext, final Date date, final Integer seq) {
		String pattern = name + ATTACH_FILE_NAME_SUBFIX + ".%s";
		String datetime = new SimpleDateFormat("yyyyMMddHHmmss").format(date) + (null == seq ? "" : String.format("_%03d", seq % 1000));
		String type = ext.toLowerCase();
		return String.format(pattern, type, datetime, type);
	}

	public static void preparePath(final String file) throws IOException {
		if (StringUtils.isBlank(file))
			return;

		// not support s3Service
		try {
			Files.createDirectories(Paths.get(FilenameUtils.getFullPath(FortifyStrategyUtils.toPathString(file))));
		} catch (FileAlreadyExistsException e) {
			// suggest not throw exception and just print out to console only
			Logger logger = LoggerFactory.getLogger(ReportFilePathUtils.class);
			logger.info(String.format("Path is already existed [ %s ]", FilenameUtils.getFullPath(file)));
		}
	}

	public static String replaceFileOutputPath(final String folderOutput, final String path) {
		String a = FilenameUtils.toPathString(path);
		String b = FilenameUtils.toPathString(folderOutput);
		return FilenameUtils.toPathString(StringUtils.replace(a, b, FileDownloadUtils.FOLDER_OUTPUT));
	}

	public static String restoreFileOutputPath(final String folderOutput, final String path) {
		String a = FilenameUtils.toPathString(path);
		String b = FilenameUtils.toPathString(folderOutput);
		return FilenameUtils.toPathString(StringUtils.replace(a, FileDownloadUtils.FOLDER_OUTPUT, b));
	}
}
