package com.dbs.module.model;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractUserModel extends AbstractFortifyStrategyModel implements UserForm {

	@JsonIgnore
	private String oneBankId;

	@JsonIgnore
	private String oneBankDisplayName;

	@JsonIgnore
	private List<String> oneBankRoleList;

	@JsonIgnore
	@Setter(AccessLevel.PROTECTED)
	private Timestamp lastUpdatedDate;

	public void setLastUpdatedTime(Long... args) {
		if (null != args) {
			if (args.length > 0 && null != args[0])
				this.lastUpdatedDate = new Timestamp(args[0]);
			if (args.length > 1 && null != args[1])
				this.lastUpdatedDate.setNanos(args[1].intValue());
		}
	}
}
