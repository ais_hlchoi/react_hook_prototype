package com.dbs.module.report.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportPropsColumn;
import com.dbs.module.report.ReportPropsCriteria;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportProps;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportPropsModel extends AbstractUserModel implements ReportProps {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private OutputFileModel file;

	@JsonInclude
	private List<OutputTableModel> table;

	@JsonInclude
	private List<OutputColumnModel> column;

	@JsonInclude
	private List<OutputFilterModel> filter;

	@JsonInclude
	private List<OutputSortModel> sort;

	@JsonInclude
	private List<OutputCriteriaModel> advance;

	@JsonInclude
	private String sql;

	@JsonInclude
	private String name;

	@JsonInclude
	private String layout;

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String activeInd;

	public List<ReportPropsTable> getTable() {
		return CollectionUtils.isEmpty(table) ? new ArrayList<>() : table.stream().map(o -> (ReportPropsTable) o).collect(Collectors.toList());
	}

	public List<ReportPropsColumn> getColumn() {
		return CollectionUtils.isEmpty(column) ? new ArrayList<>() : column.stream().map(o -> (ReportPropsColumn) o).collect(Collectors.toList());
	}

	public List<ReportPropsFilter> getFilter() {
		return CollectionUtils.isEmpty(filter) ? new ArrayList<>() : filter.stream().map(o -> (ReportPropsFilter) o).collect(Collectors.toList());
	}

	public List<ReportPropsSort> getSort() {
		return CollectionUtils.isEmpty(sort) ? new ArrayList<>() : sort.stream().map(o -> (ReportPropsSort) o).collect(Collectors.toList());
	}

	public List<ReportPropsCriteria> getAdvance() {
		return CollectionUtils.isEmpty(advance) ? new ArrayList<>() : advance.stream().map(o -> (ReportPropsCriteria) o).collect(Collectors.toList());
	}
}
