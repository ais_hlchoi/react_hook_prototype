package com.dbs.module.export;

import java.io.IOException;
import java.sql.SQLException;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;

public interface DataExportHelper {

	boolean exportXlsx(DataExport model, String pid, Integer id) throws SQLException, IOException;

	boolean exportCsv(DataExport model, String pid, Integer id) throws SQLException, IOException;

	boolean exportPdf(DataExport model, String pid, Integer id) throws SQLException, IOException;

	void updateFileDownloadStatus(DownloadStatus status, String pid, Integer id);

	RptFileDnldModel getFileDownload(Integer id);

	RptFileDnldDataModel getFileDownloadData(Integer id);
}
