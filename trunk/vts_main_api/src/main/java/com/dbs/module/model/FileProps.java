package com.dbs.module.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor(staticName = "of")
public class FileProps {

	@JsonInclude
	private String path;

	@JsonInclude
	private Long size;
}
