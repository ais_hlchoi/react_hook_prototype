package com.dbs.module.report.constant;

import org.apache.commons.lang3.StringUtils;

public enum ReportExportType {

	  R("Report")
	, S("Category")
	, C("Child")
	;

	private String desc;

	private ReportExportType(String desc) {
		this.desc = desc;
	}

	public String desc() {
		return desc;
	}

	public static ReportExportType getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ReportExportType getOrElse(String text, ReportExportType o) {
		if (null == o)
			o = R;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ReportExportType.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
