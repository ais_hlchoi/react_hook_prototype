package com.dbs.module.export.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.export.DataExportHelper;
import com.dbs.module.report.ReportTemplate;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.service.DataExportService;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;

public class DataExportThread extends AbstractThread implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private DataExportService dataExportService;
	private SysEmailRequestService sysEmailRequestService;
	private SysEmailTmplService sysEmailTmplService;
	private String sysLogApi;

	private ReportTemplate model;
	private Integer id;
	private String pid;

	public DataExportThread(DataExportService dataExportService, SysEmailRequestService sysEmailRequestService, SysEmailTmplService sysEmailTmplService, ReportTemplate model, Integer id) {
		Assert.notNull(dataExportService, "Service cannot be null");
		Assert.notNull(model, "Data cannot be null");
		Assert.notNull(id, "Id cannot be null");
		this.dataExportService = dataExportService;
		this.sysEmailRequestService = sysEmailRequestService;
		this.model = model;
		this.id = id;
	}

	public DataExportThread sysLogApi(String sysLogApi) {
		this.sysLogApi = sysLogApi;
		return this;
	}

	public DataExportThread pid(String pid) {
		this.pid = pid;
		logger.info(String.format("%s export [ assign ]", String.format(MSG_DOWNLOAD_FORMAT, ClassPathUtils.getName(), getFileDnldId(), getProcessId())));
		return this;
	}

	protected DataExportHelper getDataService() {
		return dataExportService;
	}

	protected SysEmailRequestService getEmailService() {
		return sysEmailRequestService;
	}

	protected SysEmailTmplService getEmailTmplService() {
		return sysEmailTmplService;
	}

	protected Logger getLogger() {
		return logger;
	}

	protected String getFileDnldId() {
		return String.valueOf(id);
	}

	protected String getProcessId() {
		return pid;
	}

	public void run() {
		StopWatch timer = new StopWatch();
		String info = String.format(MSG_DOWNLOAD_FORMAT, ClassPathUtils.getName(), getFileDnldId(), getProcessId());
		String error = null;
		boolean done = false;
		boolean fail = false;
		timer.start();
		try {
			dataExportService.updateFileDownloadStatus(DownloadStatus.PREPARE, getProcessId(), id);
			export(dataExportService, model, pid, id);
			done = true;
		} catch (IllegalArgumentException e) {
			logger.warn(String.format("%s export [ warn ] caused by %s ", info, FortifyStrategyUtils.toErrString(e)));
			error = e.getMessage();
			fail = true;
		} catch (Exception e) {
			logger.error(String.format("%s export [ error ] caused by %s", info, FortifyStrategyUtils.toErrString(e)));
			error = e.getMessage();
			fail = true;
		}
		timer.stop();
		// case when error found
		if (fail) {
			logger.info(String.format("%s fail (%s ms)", info, timer.getTotalTimeMillis()));
			dataExportService.updateFileDownloadStatus(DownloadStatus.FAILURE, getProcessId(), id);
			sendOperationLogFail(sysLogApi, info, error);
		}
		// case when run success
		else if (done) {
			logger.info(String.format("%s done (%s ms)", info, timer.getTotalTimeMillis()));
			sendEmailAlert(EmailAlertEnum.EMAIL_ALERT_DONE_SCHED_RPT, id, model.getName(), "Export report successfully");
		}
	}
}
