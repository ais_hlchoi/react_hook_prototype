package com.dbs.module.report;

public interface ReportPropsCriteria {

	Integer getSeq();

	ReportPropsTable getTable();

	ReportPropsColumn getColumn();

	ReportPropsComparator getComparator();

	String getValue();

	String getMandatory();
}
