package com.dbs.module.download.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportDownloadCategoryData extends AbstractModuleData {

	private ReportDownloadObjectModel data;

	public ReportDownloadCategoryData(ReportDownloadObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportDownloadObjectModel getData() {
		return data;
	}

	protected void setData(ReportDownloadObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getRptName() {
		return getData().getRptName();
	}

	public String getFileName() {
		return getData().getFileName();
	}

	public String getFilePath() {
		return getOrElse(getData().getFilePathPrefix(), "") + getData().getFilePath();
	}

	public String getFileExtension() {
		return getData().getFileExtension();
	}

	public String getStatus() {
		return getData().getStatus();
	}

	public List<ReportDownloadObjectModel> getChildren() {
		return CollectionUtils.isEmpty(getData().getChildren()) ? new ArrayList<>() : getData().getChildren();
	}
}
