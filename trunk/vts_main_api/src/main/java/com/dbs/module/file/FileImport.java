package com.dbs.module.file;

import com.dbs.module.model.FileModel;
import com.dbs.module.model.FileProps;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface FileImport {

	FileImportProps getProps();

	FileProps[] list();

	FileProps[] list(String path);

	FileProps[] list(String path, String prefix);

	void copy(String source, String destination);

	void move(String source, String destination);

	File getFile(String name);

	byte[] getFileByte(String name);

	InputStream getStream(String name);

	CSVReader getCsvReader(String name);

	boolean isFile(String name);

	boolean isMatch(String name, String pattern, String ext, String parent);

	List<String> prepare(String... paths);

	Object saveFile(FileModel file);

	Object saveFile(FileModel file, String fileStorageKey);

	Object saveFile(String fileName, String fileExtension, String content);

	FileModel[] getFileModel();

	FileModel[] getFileModel(String path, String prefix);

	boolean exist(String name);

	boolean delete(String name);
}
