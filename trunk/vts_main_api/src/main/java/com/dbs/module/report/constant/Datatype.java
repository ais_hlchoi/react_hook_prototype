package com.dbs.module.report.constant;

import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum Datatype {

	  STRING
	, NUMBER
	, DATE
	, TEXT
	, ARRAY ("^\\[(.+)\\]$")
	;

	private String pattern;

	private Datatype(String pattern) {
		this.pattern = pattern;
	}

	private Datatype() {
		this(null);
	}

	/**
	 * this value affects ReportParamUtils.parse 
	 */
	public String pattern() {
		return pattern;
	}

	public String value() {
		return this.name().toLowerCase();
	}

	public static Datatype getOrElse(String text) {
		return getOrElse(text, STRING);
	}

	public static Datatype getOrElse(String text, Datatype o) {
		if (null == o)
			o = STRING;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return Datatype.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			Optional<Datatype> t = Stream.of(Datatype.values()).filter(c -> null != c.pattern() && text.matches(c.pattern())).findFirst();
			return t.isPresent() ? t.get(): o;
		}
	}
}
