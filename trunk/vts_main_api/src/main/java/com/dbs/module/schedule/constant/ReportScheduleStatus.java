package com.dbs.module.schedule.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum ReportScheduleStatus {

	  INITIAL ("I", "Initial")
	, AMEND   ("A", "Amended")
	, SCHEDULE("S", "Scheduled")
	, ERROR   ("E", "Error")
	, UNKNOWN ("U", null)
	;

	private String index;
	private String label;

	private ReportScheduleStatus(String index, String label) {
		this.index = index;
		this.label = label;
	}

	public String index() {
		return this.index;
	}

	public String label() {
		return this.label;
	}

	public static ReportScheduleStatus getOrElse(String text) {
		return getOrElse(text, UNKNOWN);
	}

	public static ReportScheduleStatus getOrElse(String text, ReportScheduleStatus o) {
		if (null == o)
			o = UNKNOWN;

		if (StringUtils.isBlank(text))
			return o;

		try {
			if (text.length() == 1)
				return Stream.of(ReportScheduleStatus.values()).filter(c -> text.equalsIgnoreCase(c.index())).findFirst().orElse(o);
			else
				return ReportScheduleStatus.valueOf(text);
		} catch (IllegalArgumentException e) {
			return o;
		}
	}

	public static boolean isError(String text) {
		return ERROR == getOrElse(text, ERROR);
	}
}
