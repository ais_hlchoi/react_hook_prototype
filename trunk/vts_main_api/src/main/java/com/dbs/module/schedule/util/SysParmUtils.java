package com.dbs.module.schedule.util;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.vtsp.model.SysParmModel;

public class SysParmUtils {

	private SysParmUtils() {
		// for SonarQube scanning purpose
	}

	public static SysParmModel parse(String segment) {
		SysParmModel o = new SysParmModel();
		o.setSegment(segment);
		return o;
	}

	public static SysParmModel getFirst(List<SysParmModel> list) {
		return CollectionUtils.isEmpty(list) ? new SysParmModel() : list.get(0);
	}
}
