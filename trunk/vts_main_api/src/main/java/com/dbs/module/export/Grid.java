package com.dbs.module.export;

import java.util.List;

public interface Grid {

	String getLayout();

	List<String> getHead();

	List<List<String>> getBody();

	int size();

	boolean isEmpty();

	boolean isNotEmpty();
}
