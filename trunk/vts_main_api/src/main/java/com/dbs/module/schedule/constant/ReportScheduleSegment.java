package com.dbs.module.schedule.constant;

public enum ReportScheduleSegment {

	  SCHED_FILE_EXPORT
	, SCHED_FILE_IMPORT
	, SCHED_USER_REPORT
	;

	private String index;
	private ReportScheduleCode[] code;

	private ReportScheduleSegment() {
		this.index = name();
		this.code = ReportScheduleCode.values();
	}

	private ReportScheduleSegment(String index) {
		this.index = index;
		this.code = ReportScheduleCode.values();
	}

	private ReportScheduleSegment(String index, ReportScheduleCode... code) {
		this.index = index;
		this.code = code;
	}

	public String index() {
		return index;
	}

	public ReportScheduleCode[] code() {
		return code;
	}

	public enum ReportScheduleCode {
		  STATUS
		, LIMIT
		, ASYNC
		, OVERWRITE
		, HOSTNAME
		, BULK_INSERT_SIZE
		, LOG_INVALID_FILE
	}
}
