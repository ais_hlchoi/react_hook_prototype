package com.dbs.module.aws.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class S3Config {

	@Value("${aws.s3client.bucketname}")
	private String bucketname;

	@Value("${aws.s3client.end-point}")
	private String endPoint;

	@Value("${aws.s3client.access-id}")
	private String accessId;

	@Value("${aws.s3client.secret-key}")
	private String secretKey;

	@Value("${aws.s3client.region}")
	private String region;

	@Value("${aws.s3client.service}")
	private String service;
}
