package com.dbs.module.export.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.util.FortifyStrategyUtils;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

public class CSVReaderUtils {

	private CSVReaderUtils() {
		// for SonarQube scanning purpose
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());	

	public static final String COMMA_DELIMITER = ";";

	private static final String MSG_LINE_FORMAT = "Error found at line %d when parse file %s <- %s";

	public static List<List<String>> readValue(final String filePath) throws IOException {
		return new CSVReaderUtils().parse(filePath, null);
	}

	public static List<String> readLine(final String filePath) throws IOException {
		return new CSVReaderUtils().parse(filePath, String.class);
	}

	@SuppressWarnings("unchecked")
	protected <T> List<T> parse(String filePath, Class<T> cls) throws IOException {
		List<T> list = new ArrayList<>();
		if (StringUtils.isBlank(filePath) || !Files.exists(Paths.get(FortifyStrategyUtils.toPathString(filePath)))) {
			logger.error(String.format("No file is located at %s", FortifyStrategyUtils.toLogString(filePath)));
		} else if (null == cls) {
			parseFileCsvReader(filePath, (List<List<String>>) list);
		} else if (cls.equals(String.class)) {
			parseFile(filePath, (List<String>) list);
		}
		return list;
	}

	protected void parseFileCsvReader(String filePath, List<List<String>> list) throws IOException {
		try (CSVReader csvReader = new CSVReader(new FileReader(FortifyStrategyUtils.toPathString(filePath)))) {
		    String[] values = null;
		    while ((values = csvReader.readNext()) != null) {
		        list.add(Arrays.asList(values));
		    }
		} catch (CsvValidationException e) {
			logger.error(String.format(MSG_LINE_FORMAT, list.size(), FortifyStrategyUtils.toLogString(filePath), FortifyStrategyUtils.toErrString(e)));
			throw new IOException(e.getMessage(), e.getCause());
		} catch (IOException e) {
			logger.error(String.format(MSG_LINE_FORMAT, list.size(), FortifyStrategyUtils.toLogString(filePath), FortifyStrategyUtils.toErrString(e)));
			throw e;
		}
	}

	protected void parseFile(String filePath, List<String> list) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(FortifyStrategyUtils.toPathString(filePath)))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				list.add(line);
			}
		} catch (IOException e) {
			logger.error(String.format(MSG_LINE_FORMAT, list.size(), FortifyStrategyUtils.toLogString(filePath), FortifyStrategyUtils.toErrString(e)));
			throw e;
		}
	}

	protected List<String> parseLine(String line) {
		return StringUtils.isBlank(line) ? new ArrayList<>() : Arrays.asList(line.split(COMMA_DELIMITER));
	}
}
