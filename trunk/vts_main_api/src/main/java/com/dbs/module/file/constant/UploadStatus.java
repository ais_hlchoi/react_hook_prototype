package com.dbs.module.file.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum UploadStatus {

	/**
	 * Status = 'I'
	 */
	INITIAL ("I", "Initial"),
	/**
	 * Status = 'P'
	 */
	PENDING ("P", "Pending"),
	/**
	 * Status = 'R'
	 */
	PROCESS ("R", "Processing"),
	/**
	 * Status = 'D'
	 */
	INDEX   ("D", "Indexing"),
	/**
	 * Status = 'F'
	 */
	FAILURE ("F", "Failure"),
	/**
	 * Status = 'C'
	 */
	COMPLETE("C", "Completed"),
	/**
	 * Status = 'X'
	 */
	CANCEL("X", "Cancelled"),
	/**
	 * Status = 'U'
	 */
	UNKNOWN ("U", null),
	;

	private String index;
	private String label;

	private UploadStatus(String index, String label) {
		this.index = index;
		this.label = label;
	}

	public String index() {
		return this.index;
	}

	public String label() {
		return this.label;
	}

	public static UploadStatus getOrElse(String text) {
		return getOrElse(text, UNKNOWN);
	}

	public static UploadStatus getOrElse(String text, UploadStatus o) {
		if (null == o)
			o = UNKNOWN;

		if (StringUtils.isBlank(text))
			return o;

		try {
			if (text.length() == 1)
				return Stream.of(UploadStatus.values()).filter(c -> text.equalsIgnoreCase(c.index())).findFirst().orElse(o);
			else
				return UploadStatus.valueOf(text);
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
