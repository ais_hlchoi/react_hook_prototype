package com.dbs.module.download.data;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportDownloadStatusData extends AbstractModuleData {

	private ReportDownloadObjectModel data;

	public ReportDownloadStatusData(ReportDownloadObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportDownloadObjectModel getData() {
		return data;
	}

	protected void setData(ReportDownloadObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getRptType() {
		return getData().getRptType();
	}

	public String getStatus() {
		return getData().getStatus();
	}

	public String getRefType() {
		return null == getData().getData() ? null : getData().getData().getRefType();
	}

	public Integer getRefId() {
		return null == getData().getData() ? null : getData().getData().getRefId();
	}

	@JsonInclude(Include.NON_NULL)
	public List<ReportDownloadStatusData> getChildren() {
		return CollectionUtils.isEmpty(getData().getChildren()) ? null : getData().getChildren().stream().map(ReportDownloadStatusData::new).collect(Collectors.toList());
	}

	public boolean isCompleted() {
		return DownloadStatus.COMPLETE.index().equals(getStatus()) && DownloadStatus.PREPARE.index().equals(getData().getParentStatus());
	}
}
