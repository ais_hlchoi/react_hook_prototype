package com.dbs.module.schedule.util;

import java.util.Date;

import com.dbs.vtsp.model.RptSetupSchedModel;

public class RptSetupSchedUtils {

	private RptSetupSchedUtils() {
		// for SonarQube scanning purpose
	}

	public RptSetupSchedModel parse(Date date, Integer id) {
		RptSetupSchedModel o = new RptSetupSchedModel();
		o.setId(id);
		o.setNextExecutionTime(date);
		return o;
	}
}
