package com.dbs.module.report;

import java.util.List;

public interface DataRowFilter {

	List<String> getErrors();

	String getError();

	int getErrorCount();

	boolean hasError();

	void build(String[] column);

	boolean test(String[] line);
}
