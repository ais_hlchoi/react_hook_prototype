package com.dbs.module.export;

import java.io.IOException;

public interface DataMergeHelper extends DataExportHelper {

	boolean exportXlsx(String pid, Integer id) throws IOException;

	boolean exportCsv(String pid, Integer id) throws IOException;

	boolean exportPdf(String pid, Integer id) throws IOException;
}
