package com.dbs.vtsp.controller.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dbs.framework.model.LocalHostModel;
import com.dbs.module.session.util.SessionCodeUtils;
import com.dbs.vtsp.service.FileImportService;

@Component
public class DataImportScheduleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileImportService fileImportService;

	@Scheduled(cron = "${schedule.data.import}")
	public void process() {
		logger.info("Scheduled data import start");
    	fileImportService.process(LocalHostModel.of(SessionCodeUtils.getFactory().create(8)));
		logger.info("Scheduled data import end");
	}
}
