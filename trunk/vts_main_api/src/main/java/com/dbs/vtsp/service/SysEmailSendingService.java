package com.dbs.vtsp.service;


import com.dbs.vtsp.model.SysEmailAttachInputStreamModel;
import com.dbs.vtsp.model.SysEmailModel;
import org.springframework.core.io.InputStreamSource;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface SysEmailSendingService {

	public boolean sendEmail(SysEmailModel sysEmailModel);

	public boolean fakeSendEmail(SysEmailModel sysEmailModel);

}
