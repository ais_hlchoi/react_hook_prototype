package com.dbs.vtsp.model;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.InputStreamSource;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
public class SysEmailAttachInputStreamModel{

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose

	@JsonInclude
	private String fileName;

	@JsonInclude
	private InputStreamSource inputStreamSource;

	
	@JsonInclude
	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("fileName", fileName);
		args.put("inputStreamSource", inputStreamSource);
		return ClassPathUtils.toJSONString(args);
	}
}
