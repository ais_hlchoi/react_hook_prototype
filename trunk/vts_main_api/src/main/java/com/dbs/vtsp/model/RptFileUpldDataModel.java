package com.dbs.vtsp.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileUpldDataModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer fileUpldId;

	@JsonInclude
	private Integer lineSeq;

	@JsonInclude
	private String dynamicTable;

	@JsonInclude
	private List<String> dynamicCols;

	public RptFileUpldDataModel(String dynamicTable, List<String> dynamicCols, Integer lineSeq) {
		setDynamicTable(dynamicTable);
		setDynamicCols(dynamicCols);
		setLineSeq(lineSeq);
	}

	public RptFileUpldDataModel() {
	}

	public String getSql() {
		List<String> args = new ArrayList<>();
		args.add("insert into " + FortifyStrategyUtils.toPathString(getDynamicTable()) + " (");
		args.add("  file_upld_id");
		args.add(", line_seq");
		args.add(", dynamic_cols");
		args.add(", partition_ind");
		args.add(", created_by");
		args.add(", created_date");
		args.add(", last_updated_by");
		args.add(", last_updated_date");
		args.add(") values (");
		args.add("  " + FortifyStrategyUtils.toPathString(String.valueOf(getFileUpldId())));
		args.add(", " + FortifyStrategyUtils.toPathString(String.valueOf(getLineSeq())));
		args.add(", COLUMN_CREATE(");
		args.add(getDynamicCols().stream().map(StringEscapeUtils::escapeSql).map(item -> String.format("'%s'", item)).collect(Collectors.joining(", ")));
		args.add(")");
		args.add(", date_format(now(), '" + DynQuerySqlUtils.PARTITION_BY_FORMAT + "')");
		args.add(", " + (StringUtils.isBlank(getCreatedBy()) ? "null" : FortifyStrategyUtils.toPathString(getCreatedBy())));
		args.add(", current_timestamp(6)");
		args.add(", " + (StringUtils.isBlank(getCreatedBy()) ? "null" : FortifyStrategyUtils.toPathString(getCreatedBy())));
		args.add(", current_timestamp(6)");
		args.add(")");
		return StringUtils.join(args, "");
	}
}
