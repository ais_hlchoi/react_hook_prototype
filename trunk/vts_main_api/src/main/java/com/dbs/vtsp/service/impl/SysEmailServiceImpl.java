package com.dbs.vtsp.service.impl;


import com.dbs.framework.aspect.annotation.CurrentUser;
import com.dbs.util.Assert;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.EmailStatusEnum;
import com.dbs.vtsp.dao.SysEmailMapper;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.service.SysEmailService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysEmailServiceImpl extends AbstractServiceImpl implements SysEmailService {
	@Autowired
	private SysEmailMapper sysEmailMapper;


	public List<SysEmailModel> getEmailListPending() {

		List<SysEmailModel> sysEmailModelList = sysEmailMapper.getEmailListPending();

		return sysEmailModelList;
	}


	@Override
	public int updateEmailListStatus(SysEmailModel model, EmailStatusEnum emailStatusEnum) {

		model.setStatus(emailStatusEnum.value());
		model.setLastUpdatedBy(CurrentUserUtils.getOneBankId());
		Assert.isTrue(sysEmailMapper.updateStatus(model) == 1, SQL_ERR_UPDATE);
		return model.getEmailId();
	}

	@Override
	public List<SysEmailModel> updateEmailListStatus(List<SysEmailModel> sysEmailModelList, EmailStatusEnum emailStatusEnum) {
		List<Integer> ids = sysEmailModelList.stream().map(it->it.getEmailId()).collect(Collectors.toList());
		Assert.isTrue(sysEmailMapper.updateStatusList(ids,emailStatusEnum.value(),CurrentUserUtils.getOneBankId()) >= 1, SQL_ERR_UPDATE);
		return sysEmailModelList;
	}
}
