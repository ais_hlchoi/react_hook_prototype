package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSrcFileHdrColModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer srcFileHdrId;

	@JsonInclude
	private Integer columnIdx;

	@JsonInclude
	private String columnName;

	@JsonInclude
	private String columnDesc;

	@JsonInclude
	private String dataType;

	@JsonInclude
	private String dataFunc;

	@JsonInclude
	private String staticInd;
}
