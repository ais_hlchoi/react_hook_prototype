package com.dbs.vtsp.service;


import com.dbs.util.Assert;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.EmailParamUtil;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.impl.AbstractServiceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface SysEmailRequestService {

	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum, String yyyymmddhhmmss);
	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum, Date date);
	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum);

	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap,SysEmailModel sysEmailModel);
	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap, Integer refId, String refTable,List<String> attachmentPaths );
	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap, SysEmailTmplModel tmpl , List<String> attachmentPaths );
}
