package com.dbs.vtsp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSrcFileHdrModel extends AbstractAuditModel {


	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer srcFileId;

	@JsonInclude
	private Date effectiveDate;
}
