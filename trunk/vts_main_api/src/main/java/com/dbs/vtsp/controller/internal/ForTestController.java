package com.dbs.vtsp.controller.internal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.dbs.framework.model.LocalHostModel;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.aws.util.S3Service;
import com.dbs.module.aws.util.S3Service.ListObjectsV2RequestProps;
import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.export.util.ForTestThread;
import com.dbs.module.file.config.ImportS3Config;
import com.dbs.module.schedule.constant.ReportScheduleConstants;
import com.dbs.module.schedule.model.FileExportModel;
import com.dbs.module.session.util.SessionCodeUtils;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.EmailParamUtil;
import com.dbs.util.EmailRefTable;
import com.dbs.util.FilenameUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.LocalHostUtils;
import com.dbs.vtsp.model.RptSetupSchedModel;
import com.dbs.vtsp.model.RptSrcFileModel;
import com.dbs.vtsp.model.SysEmailAttachModel;
import com.dbs.vtsp.model.SysEmailDataModel;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import com.dbs.vtsp.service.FileExportService;
import com.dbs.vtsp.service.FileImportService;
import com.dbs.vtsp.service.ReportScheduleService;
import com.dbs.vtsp.service.SysEmailProcessService;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailSendingService;

@RestController
@CrossOrigin
@RequestMapping("/internal/t")
public class ForTestController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileExportService fileExportService;

	@Autowired
	private FileImportService fileImportService;

	@Autowired
	private ReportScheduleService reportScheduleService;

	@Autowired
	private SysEmailRequestService sysEmailRequestService;

	@Autowired
	private SysEmailSendingService sysEmailSendingService;

	@Autowired
	private SysEmailProcessService sysEmailProcessService;

	@Autowired
	private ImportS3Config importS3Config;

	private static final String MSG_START = "%s -> start";

	@RequestMapping("/get")
	public String get() {
		return "hello world";
	}

	@RequestMapping("/host")
	public RestEntity host() {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));
		return RestEntity.success(LocalHostUtils.getHostConfig());
	}
	
	@RequestMapping("/encode")
	public String encode() {
		Codec ORACLE_CODEC = new OracleCodec();
		return ESAPI.encoder().encodeForSQL(ORACLE_CODEC, "abc");
	}

	@RequestMapping("/datasource1")
	public String datasource1() throws Exception {
		try {
			reportScheduleService.update();
		} catch (Exception e) {
			throw e;
		}
		return "update success";
	}

	@RequestMapping("/datasource2")
	public String datasource2() throws Exception {
		try {
			reportScheduleService.updateReadOnly();
		} catch (Exception e) {
			throw e;
		}
		return "update success";
	}

	@RequestMapping(value = "/file/import", method = RequestMethod.GET)
	public RestEntity fileImport(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));

		String fileNameStart = StringUtils.isBlank(args) ? "NONE" : args.equalsIgnoreCase("ALL") ? null : args;

		if (StringUtils.isNotBlank(fileNameStart)) {
			logger.info(String.format("Import file %s", FortifyStrategyUtils.toLogString(fileNameStart)));
			List<RptSrcFileModel> list = fileImportService.getSrcFileList(fileNameStart);

			if (CollectionUtils.isEmpty(list))
				return RestEntity.success();

			for (RptSrcFileModel o : list) {
				try {
					fileImportService.upload(o);
				} catch (Exception e) {
					return RestEntity.failed(e.getMessage());
				}
			}
			return RestEntity.success();
		}

		fileImportService.process(LocalHostModel.of(new SimpleDateFormat("MMddHHmm").format(new Date())));
		return RestEntity.success();
	}

	@RequestMapping(value = "/file/reload", method = RequestMethod.GET)
	public RestEntity fileReload(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));

		String fileNameStart = StringUtils.isBlank(args) ? "NONE" : args.equalsIgnoreCase("ALL") ? null : args;

		try {
			fileImportService.reload(fileNameStart);
		} catch (Exception e) {
			return RestEntity.failed(e.getMessage());
		}
		return RestEntity.success();
	}

	@RequestMapping(value = "/file/index", method = RequestMethod.GET)
	public RestEntity fileIndex(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));

		String fileNameStart = StringUtils.isBlank(args) ? "NONE" : args.equalsIgnoreCase("ALL") ? null : args;

		try {
			fileImportService.index(fileNameStart);
		} catch (Exception e) {
			return RestEntity.failed(e.getMessage());
		}
		return RestEntity.success();
	}

	@RequestMapping(value = "/file/export", method = RequestMethod.GET)
	public RestEntity fileExport(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));

		String executeTimeStr = StringUtils.isBlank(args) ? "" : args;

		if (StringUtils.isNotBlank(executeTimeStr)) {
			logger.info(String.format("Export file %s", FortifyStrategyUtils.toLogString(executeTimeStr)));
			List<ReportDownloadStatusData> list = fileExportService.getReportDownloadPendingList(FileExportModel.of(ReportScheduleConstants.EXECUTION_TIME_DEFAULT));

			if (CollectionUtils.isEmpty(list))
				return RestEntity.success();

			for (ReportDownloadStatusData o : list) {
				try {
					fileExportService.export(o);
				} catch (Exception e) {
					return RestEntity.failed(e.getMessage());
				}
			}
			return RestEntity.success();
		}

		fileExportService.process(LocalHostModel.of(new SimpleDateFormat("MMddHHmm").format(new Date())));
		return RestEntity.success();
	}

	@RequestMapping(value = "/report/schedule", method = RequestMethod.GET)
	public RestEntity reportSchedule(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));

		String executeTimeStr = StringUtils.isBlank(args) ? "" : args;

		if (StringUtils.isNotBlank(executeTimeStr)) {
			logger.info(String.format("Check report schedule %s", FortifyStrategyUtils.toLogString(executeTimeStr)));
			List<RptSetupSchedModel> list = reportScheduleService.getScheduledReportList(executeTimeStr);

			if (CollectionUtils.isEmpty(list))
				return RestEntity.success();

			for (RptSetupSchedModel o : list) {
				try {
					reportScheduleService.update(o);
				} catch (Exception e) {
					return RestEntity.failed(e.getMessage());
				}
			}
			return RestEntity.success();
		}

		reportScheduleService.process(LocalHostModel.of(new SimpleDateFormat("MMddHHmm").format(new Date())));
		return RestEntity.success();
	}

	@RequestMapping(value = "/s3/list", method = RequestMethod.GET)
	public RestEntity list(@RequestParam String args) {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));
		S3Service service = S3ClientUtils.getS3Service();
		String pathAndPrefix = getOrElse(args, importS3Config.getLoadPath());
		String[] list = service.listObjectsV2(S3ClientUtils.getBucketName(), Collections.singletonMap(ListObjectsV2RequestProps.PREFIX, FilenameUtils.toUriString(pathAndPrefix))).getObjectSummaries().stream().map(S3ObjectSummary::getKey).toArray(String[]::new);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/email/sendEmail", method = RequestMethod.GET)
	public RestEntity emailSendEmail() {
		sysEmailSendingService.sendEmail(new SysEmailModel());
		return RestEntity.success();
	}

	@RequestMapping(value = "/email/process", method = RequestMethod.GET)
	public RestEntity emailProcess() {
		sysEmailProcessService.process();
		return RestEntity.success();
	}

	@RequestMapping(value = "/email/sendError", method = RequestMethod.GET)
	public RestEntity emailError() {
		HashMap<EmailParamUtil, String> enumStringHashMap = EmailParamUtil.getDefaultHashMap();
		enumStringHashMap.put(EmailParamUtil.ERR_MSG, "paul9394");
		enumStringHashMap.put(EmailParamUtil.RPT_NAME, "paulsReport");
		enumStringHashMap.put(EmailParamUtil.LAST_UPDATEED_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		sysEmailRequestService.requestEmailAlert(enumStringHashMap, EmailAlertEnum.EMAIL_ALERT_FAIL_SCHED_RPT);
		return RestEntity.success();
	}

	@RequestMapping(value = "/email/sendSuccess", method = RequestMethod.GET)
	public RestEntity sendSuccess() {
		SysEmailTmplModel tmpl = new SysEmailTmplModel();
		tmpl.setRecipient("paulpochichan@dbs.com");
		tmpl.setRecipientCc("paulpochichan@dbs.com");
		tmpl.setRecipientBcc("paulpochichan@dbs.com");
		tmpl.setSubject("Testing from Spring Boot ${rptName} ${today} ${lastUpdatedDate}");
		tmpl.setContent("Hello World \n Spring Boot Email");
		List<String> attachmentPaths = new ArrayList<>();
		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");
//		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");
		String rptName = "paul9394";

		// SysEmailModel builder
		SysEmailModel sysEmailModel = new SysEmailModel();
		sysEmailModel.setType(EmailAlertEnum.EMAIL_ALERT_DONE_SCHED_RPT.value());
		sysEmailModel.setRecipient(tmpl.getRecipient());
		sysEmailModel.setRecipientCc(tmpl.getRecipientCc());
		sysEmailModel.setRecipientBcc(tmpl.getRecipientBcc());
		sysEmailModel.setSubject(tmpl.getSubject());
		SysEmailDataModel sysEmailDataModel = new SysEmailDataModel();
		sysEmailDataModel.setContent(tmpl.getContent());
		sysEmailModel.setSysEmailDataModel(sysEmailDataModel);
		sysEmailModel.setSendDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		sysEmailModel.setRemark(null);
		List<SysEmailAttachModel> sysEmailAttachModels = attachmentPaths.stream().map(path -> {
			SysEmailAttachModel s = new SysEmailAttachModel();
			s.setFilePath(path);
			return s;
		}).collect(Collectors.toList());
		sysEmailModel.setSysEmailAttachModelList(sysEmailAttachModels);

		// map builder
		HashMap<EmailParamUtil, String> map = EmailParamUtil.getDefaultHashMap();
		map.put(EmailParamUtil.RPT_NAME, rptName);
		map.put(EmailParamUtil.ERR_MSG, null);
		map.put(EmailParamUtil.LAST_UPDATEED_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

		sysEmailRequestService.requestEmail(map, sysEmailModel);
		return RestEntity.success();
	}

	@RequestMapping(value = "/email/sendSuccessByTmplId", method = RequestMethod.GET)
	public RestEntity sendSuccessByTmplId() {
		String rptName = "paul9394";
		List<String> attachmentPaths = new ArrayList<>();
		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");
		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");

		// map builder
		HashMap<EmailParamUtil, String> map = EmailParamUtil.getDefaultHashMap();
		map.put(EmailParamUtil.RPT_NAME, rptName);
		map.put(EmailParamUtil.ERR_MSG, null);
		map.put(EmailParamUtil.LAST_UPDATEED_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

		sysEmailRequestService.requestEmail(map, 6, EmailRefTable.RPT_SETUP_SCHED.name(), attachmentPaths);
		return RestEntity.success();
	}

	@RequestMapping(value = "/reportSchedule/process", method = RequestMethod.GET)
	public RestEntity reportScheduleProcess() {
		logger.info("Scheduled report export start");
		reportScheduleService.process(LocalHostModel.of(SessionCodeUtils.getFactory().create(8)));
		logger.info("Scheduled report export end");
		return RestEntity.success();
	}

	@Autowired
	private TaskExecutor taskExecutor;

	@RequestMapping(value = "/threadtask", method = RequestMethod.GET)
	public RestEntity threadtask() {
		taskExecutor.execute(new ForTestThread());
		return RestEntity.success();
	}

	protected <T> T getOrElse(T value, T valueIfNull) {
		return ClassPathUtils.getOrElse(value, valueIfNull);
	}
}
