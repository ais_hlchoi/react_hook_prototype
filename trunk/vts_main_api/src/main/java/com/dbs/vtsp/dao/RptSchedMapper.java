package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptSetupSchedModel;

public interface RptSchedMapper {

	List<RptSetupSchedModel> getScheduledReportList(String text);
}
