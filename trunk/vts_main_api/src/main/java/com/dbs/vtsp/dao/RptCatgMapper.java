package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.model.ReportCategoryTemplateModel;

public interface RptCatgMapper {

	List<ReportCategoryTemplateModel> getUserReportCategoryTemplateNameList(Integer id);
}
