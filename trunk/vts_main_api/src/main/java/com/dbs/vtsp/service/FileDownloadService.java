package com.dbs.vtsp.service;

import com.dbs.framework.model.LocalHost;
import com.dbs.module.download.ReportDownload;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;

public interface FileDownloadService {

	RptFileDnldModel getFileDownload(ReportDownload model);

	RptFileDnldDataModel getFileDownloadData(ReportDownload model);

	int createFileDownloadCheckCount(ReportDownload model);

	int createFileDownload(ReportDownload model);

	int updateFileDownloadStatus(ReportDownload model, LocalHost host);

	int updateFileDownloadStart(ReportDownload model, LocalHost host);

	int updateFileDownloadProcess(ReportDownload model, LocalHost host);

	int updateFileDownloadFailure(ReportDownload model, LocalHost host);

	int updateFileDownloadComplete(ReportDownload model, LocalHost host);

	int updateFileDownloadStatusPending(ReportDownload model);
}
