package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptSrcFileHdrColModel;
import com.dbs.vtsp.model.RptSrcFileHdrModel;
import com.dbs.vtsp.model.RptSrcFileModel;

public interface SrcFileMapper {

	List<RptSrcFileModel> getSrcFileList(RptSrcFileModel model);

	List<RptSrcFileModel> getSrcFileByName(List<String> list);

	int getSrcFileIdByName(String table);

	List<RptSrcFileHdrModel> getSrcFileHeaderList(String table);

	List<RptSrcFileHdrColModel> getSrcFileColumnList(RptSrcFileHdrColModel model);
	
	int createSrcFileHeader(RptSrcFileHdrModel model);

	int createSrcFileHeaderColumn(RptSrcFileHdrColModel model);

	int getSrcFileRunningCnt(Integer id);
}
