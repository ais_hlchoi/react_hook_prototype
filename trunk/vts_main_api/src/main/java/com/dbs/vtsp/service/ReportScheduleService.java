package com.dbs.vtsp.service;

import java.sql.SQLException;
import java.util.List;

import com.dbs.framework.model.LocalHost;
import com.dbs.vtsp.model.RptSetupSchedModel;

public interface ReportScheduleService {

	void process(LocalHost host);

	void update(RptSetupSchedModel model);

	List<RptSetupSchedModel> getScheduledReportList(String text);

	void update() throws SQLException;

	void updateReadOnly() throws SQLException;
}
