package com.dbs.vtsp.model;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SysEmailAttachModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer emailId;

	@JsonInclude
	private String filePath;

	@JsonInclude
	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("id", id);
		args.put("emailId", emailId);
		args.put("filePath", filePath);
		return ClassPathUtils.toJSONString(args);
	}
}
