package com.dbs.vtsp.service;


import com.dbs.util.Assert;
import com.dbs.util.EmailStatusEnum;
import com.dbs.vtsp.model.SysEmailModel;

import java.util.List;

public interface SysEmailService {

	public List<SysEmailModel> getEmailListPending();

	public int updateEmailListStatus(SysEmailModel model,EmailStatusEnum emailStatusEnum);
	public List<SysEmailModel> updateEmailListStatus(List<SysEmailModel> sysEmailModelList,EmailStatusEnum emailStatusEnum);
}
