package com.dbs.vtsp.controller.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dbs.framework.model.LocalHostModel;
import com.dbs.module.session.util.SessionCodeUtils;
import com.dbs.vtsp.service.FileExportService;

@Component
public class DataExportScheduleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileExportService fileExportService;

	@Scheduled(cron = "${schedule.data.export}")
	public void process() {
		logger.info("Scheduled data export start");
		fileExportService.process(LocalHostModel.of(SessionCodeUtils.getFactory().create(8)));
		logger.info("Scheduled data export end");
	}
}
