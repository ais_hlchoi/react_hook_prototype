package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.schedule.FileExport;

public interface DataExportMapper {

	ReportDownloadObjectModel getReportDownloadCategory(Integer id);

	List<ReportDownloadObjectModel> getReportDownloadPendingList(FileExport model);

	List<ReportDownloadObjectModel> getReportDownloadOutstandingList(List<String> list);

	List<ReportDownloadObjectModel> getReportDownloadStatusList(Integer id);
}
