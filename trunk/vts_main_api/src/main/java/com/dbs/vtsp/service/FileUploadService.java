package com.dbs.vtsp.service;

import java.sql.Connection;
import java.util.List;

import com.dbs.module.file.constant.UploadStatus;
import com.dbs.vtsp.model.SysParmModel;

public interface FileUploadService {

	String insertFileUploadData(String headerName, String fileName, String tableName, Integer fileUploadId, List<SysParmModel> attrList);

	int updateFileUploadStatus(int fileUploadId, UploadStatus status);

	void updateFileUpdateDataIndex(Connection conn, int fileUploadId);
}
