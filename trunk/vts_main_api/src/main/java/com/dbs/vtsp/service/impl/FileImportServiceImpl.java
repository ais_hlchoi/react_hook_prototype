package com.dbs.vtsp.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.druid.pool.DruidDataSource;
import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.model.LocalHost;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.constant.FileImportResponseCode;
import com.dbs.module.file.constant.UploadStatus;
import com.dbs.module.model.FileProps;
import com.dbs.module.schedule.constant.ReportScheduleSegment;
import com.dbs.module.schedule.constant.ReportScheduleSegment.ReportScheduleCode;
import com.dbs.module.schedule.constant.ReportScheduleStatus;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.FileUpldMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.RptFileUpldModel;
import com.dbs.vtsp.model.RptSrcFileModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.FileImportService;
import com.dbs.vtsp.service.FileUploadService;

@Service
public class FileImportServiceImpl extends AbstractScheduleServiceImpl implements FileImportService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private SysParmMapper sysParmMapper;

	@Autowired
	private FileUpldMapper fileUpldMapper;

	@Autowired
	private FileImport fileImport;

	@Autowired
	private FileImportService fileImportService;

	@Autowired
	private FileUploadService fileUploadService;

	@Value("${folder.root}")
	private String rootPath;

	private static final String MSG_UPLOAD_FORMAT = "%s -> %s -> %s";

	@Autowired
	@Qualifier("dataSourceObj")
	private DruidDataSource dataSourceObj;

	protected SysParmMapper getSysParmMapper() {
		return sysParmMapper;
	}

	@OperationLog(operation = OperationLogAction.FILE_UPLOAD, logErrorOnly = true)
	public void process(LocalHost host) {
		String schedHost = getSchedAttrValue(ReportScheduleCode.HOSTNAME, ReportScheduleSegment.SCHED_FILE_IMPORT);

		if (StringUtils.isNotBlank(schedHost) && Stream.of(schedHost.split("\\s*,\\s*")).noneMatch(c -> c.equalsIgnoreCase(host.getHostName()))) {
			logger.info(String.format("Host not match <- %s", FortifyStrategyUtils.toLogString(host.getHostName(), schedHost)));
			return;
		}

		List<RptSrcFileModel> list = getSrcFileList(null);
		Assert.isFalse(CollectionUtils.isEmpty(list), FileImportResponseCode.E2001);

		logger.info(String.format("%d source file%s in queue.", list.size(), list.size() > 1 ? "s" : ""));

		List<String> errorList = new ArrayList<>();
		int successCount = 0;

		for (RptSrcFileModel o : list) {
			// try-catch because remaining items still go ahead even through error found
			try {
				fileImportService.upload(o);
				successCount++;
			} catch (Exception e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
				String error = String.format("FileNameStart: %s, Error: %s", o.getFileNameStart(), FortifyStrategyUtils.toErrString(e));
				errorList.add(error);
			}
		}

		if (CollectionUtils.isNotEmpty(errorList)) {
			List<String> msgList = new ArrayList<>();
			int errorCount = errorList.size();
			msgList.add(String.format("There are %s files pattern execute without error", successCount));
			msgList.add(String.format("There are %s files pattern execute with error, display in below list:", errorCount));
			msgList.addAll(errorList);
			Assert.isTrue(errorCount == 0, "\n" + StringUtils.join(msgList, "\n"), FileImportResponseCode.E2006);
		}
	}

	@OperationLog(operation = OperationLogAction.FILE_UPLOAD, logErrorOnly = true, whenErrorSendEmail = false)
	public void upload(RptSrcFileModel model) {
		Assert.notNull(model, "Action cannot be null");
		Assert.notNull(model.getFileNameStart(), "FileNameStart cannot be null");
		Assert.notNull(rootPath, "Folder root path cannot be null");

		String action = model.getFileNameStart();
		String tableName = getOrElse(model.getTableName(), action);

		Assert.isFalse(isSrcFileRunning(model), String.format(MSG_WORK_IN_PROGRESS, "File type " + FortifyStrategyUtils.toLogString(action)), FileImportResponseCode.E2005);

		ReportScheduleSegment schedSegment = ReportScheduleSegment.SCHED_FILE_IMPORT;
		List<SysParmModel> attrList = getSchedAttrList(schedSegment);

		String schedStatus = getSchedAttrValue(ReportScheduleCode.STATUS, attrList);
		Assert.isTrue(BooleanUtils.toBoolean(schedStatus), String.format(MSG_OUT_OF_SERVICE, schedSegment.index(), FortifyStrategyUtils.toLogString(schedStatus, action)), FileImportResponseCode.E2002);

		int schedLimit = getIntValue(getSchedAttrValue(ReportScheduleCode.LIMIT, attrList), -1);
		Assert.isTrue(schedLimit > 0, String.format(MSG_EMPTY_LIMIT, schedSegment.index(), FortifyStrategyUtils.toLogString(schedLimit, action)), FileImportResponseCode.E2003);

		// set schedLimit to number instead of infinitive
		if (schedLimit < 0)
			schedLimit = 1000;

		prepare();

		logger.info(String.format("List out files at %s", FortifyStrategyUtils.toLogString(fileImport.getProps().getLoadPath())));

		FileProps[] fileFullPaths = fileImport.list(fileImport.getProps().getLoadPath(), action);
		Assert.isFalse(ArrayUtils.isEmpty(fileFullPaths), String.format(MSG_FOLDER_NOT_FOUND, FortifyStrategyUtils.toLogString(fileImport.getProps().getLoadPath())), FileImportResponseCode.E2004);

		logger.info(String.format("%d item%s found", fileFullPaths.length, fileFullPaths.length > 1 ? "s" : ""));

		List<String> whiteList = getFileUploadInitList().stream().map(o -> o.getFileName()).collect(Collectors.toList());
		boolean isLogInvalidFile = BooleanUtils.toBoolean(getSchedAttrValue(ReportScheduleCode.LOG_INVALID_FILE, attrList));

		Map<Integer, String> fileUploadList = new LinkedHashMap<Integer, String>();
		for (FileProps file : fileFullPaths) {
			String filePath = file.getPath();
			if (isMatch(filePath, model.getFileNameStart(), model.getFilePattRegex(), fileImport.getProps().getLoadPath())) {
				String fileName = String.format("%s.%s", FilenameUtils.getBaseName(filePath), FilenameUtils.getExtension(filePath));
				String csvName = fileImport.getProps().getLoadPath() + fileName;
				if (whiteList.contains(csvName))
					continue;
				int key = createFileUpload(model.getId(), csvName, getFileDate(fileName.substring(action.length())), file.getSize());
				fileUploadList.put(key, fileName);
				if (fileUploadList.size() >= schedLimit) {
					logger.info(String.format("Trim down list from %d to %d", fileFullPaths.length, schedLimit));
					break;
				}
			}
			// optional - log files in directory
			else if (isLogInvalidFile && fileImport.isFile(filePath)) {
				logger.info(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(action, filePath), "not in list"));
			}
		}

		handleFileList(action, tableName, fileUploadList, attrList);
	}

	private boolean isSrcFileRunning(RptSrcFileModel model) {
		int cnt = srcFileMapper.getSrcFileRunningCnt(model.getId());
		return cnt > 0;
	}

	@OperationLog(operation = OperationLogAction.FILE_UPLOAD, logErrorOnly = true)
	public void reload(String action) {
		List<RptFileUpldModel> list = getFileUploadInitList();
		Assert.isFalse(CollectionUtils.isEmpty(list), FileImportResponseCode.E2001);

		logger.info(String.format("%d pending file%s in list.", list.size(), list.size() > 1 ? "s" : ""));

		ReportScheduleSegment schedSegment = ReportScheduleSegment.SCHED_FILE_IMPORT;
		List<SysParmModel> attrList = getSchedAttrList(schedSegment);

		String schedStatus = getSchedAttrValue(ReportScheduleCode.STATUS, attrList);
		Assert.isTrue(BooleanUtils.toBoolean(schedStatus), String.format(MSG_OUT_OF_SERVICE, schedSegment.index(), FortifyStrategyUtils.toLogString(schedStatus, action)), FileImportResponseCode.E2002);

		boolean isAll = StringUtils.isBlank(action);
		List<String> done = new ArrayList<String>();

		for (RptFileUpldModel o : list) {
			if (StringUtils.isBlank(o.getAction()) || done.contains(o.getAction()))
				continue;
			if (!(isAll || action.equals(o.getAction())))
				continue;
			Map<Integer, String> fileUploadList = list.stream().filter(c -> o.getAction().equals(c.getAction())).collect(Collectors.toMap(c -> c.getId(), c -> c.getFileName(), (a, b) -> a));
			if (MapUtils.isEmpty(fileUploadList))
				continue;
			RptSrcFileModel model = getSrcFileList(o.getAction()).stream().filter(c -> o.getAction().equalsIgnoreCase(c.getFileNameStart())).findFirst().orElse(null);
			if (null == model)
				continue;
			done.add(o.getAction());
			handleFileList(o.getAction(), model.getTableName(), fileUploadList, attrList);
		}
	}

	@OperationLog(operation = OperationLogAction.FILE_UPLOAD)
	public void index(String action) throws SQLException {
		List<RptSrcFileModel> list = getSrcFileList(action);
		Assert.isFalse(CollectionUtils.isEmpty(list), FileImportResponseCode.E2001);

		ReportScheduleSegment schedSegment = ReportScheduleSegment.SCHED_FILE_IMPORT;
		List<SysParmModel> attrList = getSchedAttrList(schedSegment);

		String schedStatus = getSchedAttrValue(ReportScheduleCode.STATUS, attrList);
		Assert.isTrue(BooleanUtils.toBoolean(schedStatus), String.format(MSG_OUT_OF_SERVICE, schedSegment.index(), FortifyStrategyUtils.toLogString(schedStatus, action)), FileImportResponseCode.E2002);

		try (
				Connection conn = dataSourceObj.getConnection();
			) {
			for (RptSrcFileModel o : list) {
				Integer fileUploadId = fileUpldMapper.getLastFileUploadId(o.getFileNameStart());
				if (null == fileUploadId) {
					logger.info(String.format("No file is uploaded %s", FortifyStrategyUtils.toLogString(action)));
					continue;
				}
				fileUploadService.updateFileUpdateDataIndex(conn, fileUploadId);
			}
		}
	}

	private void handleFileList(String action, String tableName, Map<Integer, String> fileUploadList, List<SysParmModel> attrList) {
		String methodName = ClassPathUtils.getName();
		boolean checked = false;
		for (Entry<Integer, String> o : fileUploadList.entrySet()) {
			Integer fileUploadId = o.getKey();
			String filePath = o.getValue();
			String fileName = String.format("%s.%s", FilenameUtils.getBaseName(filePath), FilenameUtils.getExtension(filePath));
			logger.info(String.format(MSG_UPLOAD_FORMAT, methodName, fileName, "start"));
			// try-catch because remaining items still go ahead even through error found
			try {
				fileUploadService.insertFileUploadData(action, fileName, tableName, fileUploadId, attrList);
				logger.info(String.format(MSG_UPLOAD_FORMAT, methodName, fileName, "processed"));
				checked = true;
			} catch (Exception e) {
				fileUploadService.updateFileUploadStatus(fileUploadId, UploadStatus.FAILURE);
				logger.error(String.format(MSG_UPLOAD_FORMAT, methodName, fileName, "failed"));
				logger.error(FortifyStrategyUtils.toErrString(e));
			}
		}
		logger.info(String.format(MSG_UPLOAD_FORMAT, methodName, FortifyStrategyUtils.toLogString(action), checked ? "done" : "no action"));
	}

	public List<RptSrcFileModel> getSrcFileList(String fileNameStart) {
		RptSrcFileModel filter = new RptSrcFileModel();
		filter.setFileNameStart(fileNameStart);
		List<RptSrcFileModel> list = srcFileMapper.getSrcFileList(filter);
		return null == list ? new ArrayList<>() : list;
	}

	private List<RptFileUpldModel> getFileUploadInitList() {
		List<RptFileUpldModel> list = fileUpldMapper.getFileUploadStatusList(ReportScheduleStatus.INITIAL.index());
		return CollectionUtils.isEmpty(list) ? new ArrayList<>() : list;
	}

	private int createFileUpload(int fileId, String csvFileName, String fileDate, Long fileSize) {
		RptFileUpldModel model = new RptFileUpldModel();
		model.setSrcFileId(fileId);
		model.setFileName(csvFileName);
		model.setFileDate(fileDate);
		model.setFileSize(fileSize);
		model.setUpldDate(new Date());
		model.setStatus(UploadStatus.INITIAL.index());
		fileUpldMapper.createFileUpload(model);
		return model.getId();
	}

	private String getFileDate(String fileName) {
		Matcher m = Pattern.compile("\\D+(\\d{10,})_\\d+").matcher(fileName);
		return m.find() ? m.group(1) : null;
	}

	private boolean isMatch(String filePath, String fileName, String filePattRegex, String parent) {
		String regex = getOrElse(filePattRegex, fileImport.getProps().getFileImportPatternName());
		String pattern = StringUtils.isBlank(regex) ? fileName : String.format(regex, fileName);
		String ext = fileImport.getProps().getFileImportPatternExt();
		return fileImport.isMatch(filePath, pattern, ext, parent);
	}

	private void prepare() {
		fileImport.prepare(fileImport.getProps().getLoadProcessPath(), fileImport.getProps().getLoadSuccessPath(), fileImport.getProps().getLoadFailPath()).forEach(msg -> logger.info(msg));
	}
}
