package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupTmplSortModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer sortSeq;

	@JsonInclude
	private String sortText;
}
