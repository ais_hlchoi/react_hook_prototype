package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.model.LocalHost;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.data.ReportDownloadCategoryData;
import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.export.constant.FileExportResponseCode;
import com.dbs.module.export.util.DataExportThread;
import com.dbs.module.export.util.DataMergeThread;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.constant.ReportExportType;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.module.report.util.ReportPropsUtils;
import com.dbs.module.report.util.ReportTemplateUtils;
import com.dbs.module.schedule.FileExport;
import com.dbs.module.schedule.constant.ReportScheduleConstants;
import com.dbs.module.schedule.constant.ReportScheduleSegment;
import com.dbs.module.schedule.constant.ReportScheduleSegment.ReportScheduleCode;
import com.dbs.module.schedule.model.FileExportModel;
import com.dbs.module.session.util.SessionCodeUtils;
import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.DataExportMapper;
import com.dbs.vtsp.dao.FileDnldMapper;
import com.dbs.vtsp.dao.InformationSchemaMapper;
import com.dbs.vtsp.dao.RptTmplMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.DataExportService;
import com.dbs.vtsp.service.DataMergeService;
import com.dbs.vtsp.service.FileExportService;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;

@Service
public class FileExportServiceImpl extends AbstractScheduleServiceImpl implements FileExportService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DataExportMapper dataExportMapper;

	@Autowired
	private FileDnldMapper fileDnldMapper;

	@Autowired
	private RptTmplMapper rptTmplMapper;

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private SysParmMapper sysParmMapper;

	@Autowired
	private InformationSchemaMapper informationSchemaMapper;

	@Autowired
	private DataExportService dataExportService;

	@Autowired
	private DataMergeService dataMergeService;

	@Autowired
	private FileExportService fileExportService;

	@Autowired
	private SysEmailRequestService sysEmailRequestService;

	@Autowired
	private SysEmailTmplService sysEmailTmplService;

	@Autowired
	private Executor taskExecutor;

	@Value("${datasource.tableSchema}")
	private String tableSchema;

	@Value("${sys.log.url}")
	private String sysLogApi;

	protected SysParmMapper getSysParmMapper() {
		return sysParmMapper;
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD, logErrorOnly = true)
	public void process(LocalHost host) {
		String schedHost = getSchedAttrValue(ReportScheduleCode.HOSTNAME, ReportScheduleSegment.SCHED_FILE_EXPORT);

		if (StringUtils.isNotBlank(schedHost) && Stream.of(schedHost.split("\\s*,\\s*")).noneMatch(c -> c.equalsIgnoreCase(host.getHostName()))) {
			logger.info(String.format("Host not match <- %s", FortifyStrategyUtils.toLogString(host.getHostName(), schedHost)));
			return;
		}

		List<ReportDownloadStatusData> list = getReportDownloadPendingList(FileExportModel.of(ReportScheduleConstants.EXECUTION_TIME_DEFAULT));
		Assert.isFalse(CollectionUtils.isEmpty(list), FileExportResponseCode.E3001);

		logger.info(String.format("%d outstanding report%s in queue.", list.size(), list.size() > 1 ? "s" : ""));

		for (ReportDownloadStatusData o : list) {
			// try-catch because remaining items still go ahead even through error found
			try {
				fileExportService.export(o);
			} catch (Exception e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
			}
		}
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD, logErrorOnly = true)
	public void export(ReportDownloadStatusData model) {
		Assert.notNull(model, "Data cannot be null");
		logger.info(String.format("Report file download %s start", FortifyStrategyUtils.toLogString(model.getId(), model.getRefType(), model.getRefType(), model.getRefId())));

		ReportScheduleSegment schedSegment = ReportScheduleSegment.SCHED_FILE_EXPORT;
		List<SysParmModel> attrList = getSchedAttrList(schedSegment);

		String schedStatus = getSchedAttrValue(ReportScheduleCode.STATUS, attrList);
		Assert.isTrue(BooleanUtils.toBoolean(schedStatus), String.format(MSG_OUT_OF_SERVICE, schedSegment.index(), FortifyStrategyUtils.toLogString(schedStatus, model.getId())), FileExportResponseCode.E3002);

		String schedAsync = getSchedAttrValue(ReportScheduleCode.ASYNC, attrList);
		int asyncLimit = NumberUtils.isCreatable(schedAsync) ? Math.max(0, NumberUtils.createInteger(schedAsync)) : BooleanUtils.toBoolean(schedAsync) ? 0 : 1;

		try {
			switch (ReportExportType.getOrElse(model.getRptType())) {
			case R:
				Assert.notNull(model.getRefId(), "Id cannot be null");
				ReportTemplateObjectModel rpt = getReportDownloadItem(model.getRefId());
				Assert.notNull(rpt, "Report is not existed");
				taskExecutor.execute(new DataExportThread(dataExportService, sysEmailRequestService, sysEmailTmplService, ReportTemplateUtils.parse(rpt), model.getId()).pid(SessionCodeUtils.getFactory().create(8)).sysLogApi(sysLogApi));
				break;
			case S:
				Assert.notNull(model.getId(), "Id cannot be null");
				ReportDownloadCategoryData cat = getReportDownloadCategory(model.getId());
				Assert.notNull(cat, "Report set is not existed");
				Assert.notEmpty(cat.getChildren(), "No report is found");
				if (asyncLimit > 0 && cat.getChildren().size() > asyncLimit)
					taskExecutor.execute(new DataMergeThread(dataMergeService, sysEmailRequestService, sysEmailTmplService, cat, null, model.getId()).pid(SessionCodeUtils.getFactory().create(8)).sysLogApi(sysLogApi));
				else
					cat.getChildren().stream().forEach(child -> taskExecutor.execute(new DataMergeThread(dataMergeService, sysEmailRequestService, sysEmailTmplService, cat, child.getId(), model.getId()).pid(SessionCodeUtils.getFactory().create(8)).sysLogApi(sysLogApi)));
				break;
			default:
				break;
			}
			logger.info(String.format("Report file download %s assigned", FortifyStrategyUtils.toLogString(model.getId())));

		} catch (IllegalArgumentException e) {
			updateFileDownloadStatus(model.getId(), DownloadStatus.FAILURE);
			logger.error(String.format("Report file download %s failed", FortifyStrategyUtils.toLogString(model.getId())));
			logger.error(FortifyStrategyUtils.toErrString(e));
		}
	}

	// get data

	public List<ReportDownloadStatusData> getReportDownloadPendingList(FileExport model) {
		List<ReportDownloadObjectModel> list = dataExportMapper.getReportDownloadPendingList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportDownloadStatusData::new).collect(Collectors.toList());
	}

	public List<ReportDownloadStatusData> getReportDownloadOutstandingList(ReportDownload model) {
		List<String> filter = StringUtils.isBlank(model.getStatus()) ? null : Arrays.asList(model.getStatus().split("\\s*,\\s*"));
		List<ReportDownloadObjectModel> list = dataExportMapper.getReportDownloadOutstandingList(filter);
		return null == list ? new ArrayList<>() : list.stream().map(ReportDownloadStatusData::new).collect(Collectors.toList());
	}

	public ReportDownloadCategoryData getReportDownloadCategory(Integer id) {
		ReportDownloadObjectModel item = dataExportMapper.getReportDownloadCategory(id);
		return null == item ? null : new ReportDownloadCategoryData(item);
	}

	protected ReportTemplateObjectModel getReportDownloadItem(Integer id) {
		return rptTmplMapper.getUserReportTemplateObjectNoPrivs(ReportPropsUtils.parse(id));
	}

	// set data

	private int updateFileDownloadStatus(int fileDownloadId, DownloadStatus status) {
		RptFileDnldModel model = new RptFileDnldModel();
		model.setId(fileDownloadId);
		model.setStatus(null == status ? null : status.index());
		fileDnldMapper.updateFileDownloadStatus(model);
		return model.getId();
	}

	protected String getQuerySql(ReportTemplate model) {
		String sql = DynQuerySqlUtils.render(model, srcFileMapper, informationSchemaMapper, tableSchema);
		return StringUtils.isBlank(sql) || null == model.getOption() ? sql : ReportParamUtils.parse(sql, model.getOption().getParameter());
	}
}
