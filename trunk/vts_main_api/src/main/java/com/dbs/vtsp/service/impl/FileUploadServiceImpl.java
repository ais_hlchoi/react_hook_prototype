package com.dbs.vtsp.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.druid.pool.DruidDataSource;
import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.constant.FileImportResponseCode;
import com.dbs.module.file.constant.UploadStatus;
import com.dbs.module.model.FileProps;
import com.dbs.module.report.DataColOutput;
import com.dbs.module.report.DataRowFilter;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.util.DataColOutputUtils;
import com.dbs.module.report.util.DataRowFilterUtils;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.DynQuerySqlUtils.FieldDataType;
import com.dbs.module.schedule.constant.ReportScheduleSegment.ReportScheduleCode;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.EmailParamUtil;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.TableNameUtils;
import com.dbs.vtsp.dao.FileUpldMapper;
import com.dbs.vtsp.dao.InformationSchemaMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.RptFileUpldDataModel;
import com.dbs.vtsp.model.RptFileUpldDataStaticModel;
import com.dbs.vtsp.model.RptFileUpldModel;
import com.dbs.vtsp.model.RptSrcFileHdrColModel;
import com.dbs.vtsp.model.RptSrcFileHdrModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.model.UserTabColsModel;
import com.dbs.vtsp.service.FileUploadService;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

@Service
public class FileUploadServiceImpl extends AbstractScheduleServiceImpl implements FileUploadService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileUpldMapper fileUpldMapper;

	@Autowired
	private InformationSchemaMapper informationSchemaMapper;

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private SysParmMapper sysParmMapper;

	@Autowired
	private FileImport fileImport;

	@Autowired
	@Qualifier("dataSourceObj")
	private DruidDataSource dataSourceObj;

	@Value("${datasource.tableSchema}")
	private String tableSchema;

	private static final String MSG_UPLOAD_FORMAT = "%s -> %s -> %s";
	private static final String MSG_INSERT_FORMAT = "Import %s %d ...";
	private static final String MSG_LINE_FORMAT = "%s found at line %d in %s - %s";

	private static final String ALERT = "Alert";
	private static final String ERROR = "Error";

	private static final int BULK_INSERT_SIZE = 500;

	protected SysParmMapper getSysParmMapper() {
		return sysParmMapper;
	}

	@OperationLog(operation = OperationLogAction.FILE_UPLOAD, logDescReturn = true)
	public String insertFileUploadData(String headerName, String fileName, String tableName, Integer fileUploadId, List<SysParmModel> attrList) {
		Map<String, String> map = new HashMap<>();
		map.put(EmailParamUtil.RPT_NAME.name(), fileName);

		String schedOverwrite = getSchedAttrValue(ReportScheduleCode.OVERWRITE, attrList);
		boolean isOverwrite = StringUtils.isBlank(schedOverwrite) || BooleanUtils.toBoolean(schedOverwrite);

		String schedBulkInsertSize = getSchedAttrValue(ReportScheduleCode.BULK_INSERT_SIZE, attrList);
		int bulkInsertSize = getIntValue(schedBulkInsertSize, BULK_INSERT_SIZE);

		moveToProcess(fileName, isOverwrite);

		String csvName = fileImport.getProps().getLoadProcessPath() + fileName;
		SrcFileProps props = new SrcFileProps(headerName).put(tableName);
		int lineCnt = 0;
		int failCnt = 0;
		int skipCnt = 0;
		logger.info(String.format("Start to read csv %s", csvName));		
		try (
				CSVReader reader = fileImport.getCsvReader(csvName);
			) {
			// part 1 - get parameter
			props.put(DataRowFilterUtils.parse(getSysParamListBySegmentValue(DataRowFilterUtils.SEGMENT, props.getHeaderName())));
			props.put(DataColOutputUtils.parse(getSysParamListBySegmentValue(DataColOutputUtils.SEGMENT, props.getHeaderName())));

			// part 2 - read file
			logger.info(String.format("Get table column list %s", props.getTableName()));
			props.put(getUserTabColumnList(props.getTableName()));
			String[] line;
			while ((line = reader.readNext()) != null) {
				lineCnt++;
				// header
				if (lineCnt == 1) {
					final String[] lineFinal = line;
					props.put(IntStream.range(0, line.length).mapToObj(i -> parseColumnModel(lineFinal[i], i)).collect(Collectors.toList()));
					props.put(props.getFileColumnNameList().stream().map(RptSrcFileHdrColModel::getColumnName).toArray(String[]::new));
					Assert.isFalse(CollectionUtils.isEmpty(props.getFileColumnNameList()), FileImportResponseCode.E2011);
					props.getDataRowFilter().build(props.getFileColumnNames());
					Assert.isFalse(props.getDataRowFilter().hasError(), String.format("%d error(s) found - %s ...", props.getDataRowFilter().getErrorCount(), props.getDataRowFilter().getError()));
					props.getDataColOutput().build(props.getFileColumnNames());
					Assert.isFalse(props.getDataColOutput().hasError(), String.format("%d error(s) found - %s ...", props.getDataColOutput().getErrorCount(), props.getDataColOutput().getError()));
				}
				// content
				else {
					if (line.length < 1 || (line.length == 1 && StringUtils.isBlank(line[0])))
						continue;
					// try-catch because remaining items still go ahead even through error found
					try {
						Assert.isTrue(line.length == props.getFileColumnNames().length, String.format("count of field and column not match [ %d != %d ].", line.length, props.getFileColumnNames().length), FileImportResponseCode.E2016);
						Assert.isTrue(props.getDataRowFilter().test(line), "skip to insert row data", FileImportResponseCode.E2017);
						handleRows(props, props.getDataColOutput().apply(line), lineCnt);
					} catch (SysRuntimeException e) {
						logger.info(String.format(MSG_LINE_FORMAT, ALERT, lineCnt, FortifyStrategyUtils.toLogString(props.getHeaderName()), FortifyStrategyUtils.toErrString(e)));
						if (FileImportResponseCode.E2016.name().equals(e.getResponseCode()))
							failCnt++;
						else if (FileImportResponseCode.E2017.name().equals(e.getResponseCode()))
							skipCnt++;
					}
				}
			}

			updateFileUploadRowCount(fileUploadId, lineCnt, lineCnt - skipCnt);
			Assert.isFalse(failCnt > 0, String.format("%d error(s) found - count of field and column not match", failCnt), FileImportResponseCode.E2016);

			// part 3 - insert data
			RptSrcFileHdrModel dbHeader = findEffectiveHeader(props.getHeaderName());
			boolean isDbHeaderCreated = false;
			if (dbHeader == null) {
				int newFileTypeId = srcFileMapper.getSrcFileIdByName(props.getHeaderName());
				if (!isDbHeaderCreated) {
					createSrcFileHeader(newFileTypeId);
					isDbHeaderCreated = true;
				}
				dbHeader = findEffectiveHeader(props.getHeaderName());
				Assert.notNull(dbHeader, String.format("Effective header for [ %s ] not found.", props.getHeaderName()), FileImportResponseCode.E2012);
			}
			int headerId = dbHeader.getId();
			List<RptSrcFileHdrColModel> dbColumnList = findColumnList(headerId);
			boolean isNewDbColumn = isDbHeaderCreated || !compareColumnList(dbColumnList, props.getFileColumnNameList());
			if (isNewDbColumn) {
				if (!isDbHeaderCreated) {
					headerId = createSrcFileHeader(dbHeader.getSrcFileId());
					isDbHeaderCreated = true;
				}
				final int headerId_final = headerId;
				props.getFileColumnNameList().stream().forEach(data -> createSrcFileHeaderColumn(headerId_final, data, dbColumnList));
			}
			try (
					Connection conn = dataSourceObj.getConnection();
				) {
				updateFileUploadHeader(fileUploadId, headerId);
				logger.info("Done - FileUploadHeader");
//				props.getDynamicColumnList().stream().forEach(data -> createFileUploadData(fileUploadId, data));
				batchCreateFileUploadData(conn, props.getDynamicColumnList(), bulkInsertSize, fileUploadId);
				updateFileUploadStatus(fileUploadId, UploadStatus.PROCESS);
				logger.info("Done - FileUploadData > DynamicColumn");
//				props.getStaticColumnList().stream().forEach(data -> createFileUploadDataStatic(fileUploadId, data));
				batchCreateFileUploadStaticData(conn, props.getStaticColumnList(), bulkInsertSize, fileUploadId);
				updateFileUploadStatus(fileUploadId, UploadStatus.INDEX);
				logger.info("Done - FileUploadData > StaticColumn");
				updateFileUpdateDataIndex(conn, fileUploadId);
				logger.info("Done - FileUpdateDataIndex");
				updateFileUploadStatus(fileUploadId, UploadStatus.COMPLETE);
				logger.info("Done - FileUploadStatus");
			}
		} catch (IOException e) {
			logger.error(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(props.getTableName()), IOException.class.getSimpleName()));
			moveToFail(fileName, isOverwrite);
			throw new SysRuntimeException(FileImportResponseCode.E2101, FortifyStrategyUtils.toErrString(e), e).putAll(map);
		} catch (CsvValidationException e) {
			logger.error(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(props.getTableName()), CsvValidationException.class.getSimpleName()));
			moveToFail(fileName, isOverwrite);
			throw new SysRuntimeException(FileImportResponseCode.E2102, FortifyStrategyUtils.toErrString(e), e).putAll(map);
		} catch (IllegalArgumentException e) {
			logger.error(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(props.getTableName()), FortifyStrategyUtils.toErrString(e)));
			moveToFail(fileName, isOverwrite);
			throw new SysRuntimeException(FileImportResponseCode.E2103, FortifyStrategyUtils.toErrString(e), e).putAll(map);
		} catch (SysRuntimeException e) {
			logger.error(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(props.getTableName()), FortifyStrategyUtils.toErrString(e)));
			moveToFail(fileName, isOverwrite);
			throw new SysRuntimeException(e.getExceptionCause(), FortifyStrategyUtils.toErrString(e), e).putAll(map);
		} catch (Exception e) {
			logger.error(String.format(MSG_UPLOAD_FORMAT, ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(props.getTableName()), FortifyStrategyUtils.toErrString(e)));
			moveToFail(fileName, isOverwrite);
			throw new SysRuntimeException(FileImportResponseCode.E2999, FortifyStrategyUtils.toErrString(e), e).putAll(map);
		}

		moveToSuccess(fileName, isOverwrite);
		return String.format("%d row(s) imported. %d row(s) failed. %d row(s) skipped.", props.getDynamicColumnList().size(), failCnt, skipCnt);
	}

	private void batchCreateFileUploadStaticData(Connection conn, List<RptFileUpldDataStaticModel> staticColumnList, Integer bulkInsertSize, Integer fileUploadId) {
		StringBuilder query = new StringBuilder();
		int size = null == bulkInsertSize || bulkInsertSize < 0 ? BULK_INSERT_SIZE : bulkInsertSize;
		int index = 0;
		RptFileUpldDataStaticModel lastModel = null;
		for (RptFileUpldDataStaticModel data : staticColumnList) {
			data.setFileUpldId(fileUploadId);
			index++;
			if (index == 1)
				query.append(data.getSql());
			else
				query.append(",").append(data.getSql().substring(data.getSql().indexOf(") values ") + 9));
			if (index == size) {
				index = 0;
				createFileUploadDataStatic(conn, fileUploadId, data, query.toString());
				query = new StringBuilder();
			}
			lastModel = data;
		}
		if (query.toString().length() > 1)
			createFileUploadDataStatic(conn, fileUploadId, lastModel, query.toString());
	}

	private void batchCreateFileUploadData(Connection conn, List<RptFileUpldDataModel> dynamicColumnList, Integer bulkInsertSize, Integer fileUploadId) {
		StringBuilder query = new StringBuilder();
		int size = null == bulkInsertSize || bulkInsertSize < 0 ? BULK_INSERT_SIZE : bulkInsertSize;
		int index = 0;
		RptFileUpldDataModel lastModel = null;
		for (RptFileUpldDataModel data : dynamicColumnList) {
			data.setFileUpldId(fileUploadId);
			index++;
			if (index == 1)
				query.append(data.getSql());
			else
				query.append(",").append(data.getSql().substring(data.getSql().indexOf(") values ") + 9));
			if (index == size) {
				index = 0;
				createFileUploadData(conn, fileUploadId, data, query.toString());
				query = new StringBuilder();
			}
			lastModel = data;
		}
		if (query.toString().length() > 1)
			createFileUploadData(conn, fileUploadId, lastModel, query.toString());
	}

	private void handleRows(SrcFileProps props, String[] line, int lineCnt) {
		StringBuilder tableColumnNameBuilder = new StringBuilder();
		StringBuilder tableColumnValueBuilder = new StringBuilder();
		List<String> dynamicColumnBuilder = new ArrayList<>();
		int headerColumnCount = props.getFileColumnNames().length;
		for (int i = 0; i < line.length; i++) {
			// for bulk insert align all line, no check column count not matched
			if (props.getUserTabColsList().containsKey(props.getFileColumnNames()[i])) {
				String value = line[i];
				Integer maxLength = props.getUserTabColsList().get(props.getFileColumnNames()[i]);
				if (null != maxLength && value.length() > maxLength)
					value = value.substring(0, maxLength);
				tableColumnNameBuilder.append(", " + props.getFileColumnNames()[i]);
				tableColumnValueBuilder.append(",'" + StringEscapeUtils.escapeSql(value) + "'");
			}
			// no insert to dynamic column table if column is found in static table
			else {
				if (StringUtils.isEmpty(line[i]) || !(i < headerColumnCount)) 
					continue;
				dynamicColumnBuilder.add(props.getFileColumnNameList().get(i).getColumnName());
				dynamicColumnBuilder.add(line[i]);
			}
		}
		if (tableColumnNameBuilder.length() > 0)
			props.getStaticColumnList().add(new RptFileUpldDataStaticModel(props.getTableName(), tableColumnNameBuilder.toString().substring(1), tableColumnValueBuilder.toString().substring(1), lineCnt));
		if (CollectionUtils.isNotEmpty(dynamicColumnBuilder))
			props.getDynamicColumnList().add(new RptFileUpldDataModel(props.getDynmaicTable(), dynamicColumnBuilder, lineCnt));
	}

	private void moveToFail(String fileName, boolean isOverwrite) {
		String path = fileImport.getProps().getLoadFailPath();
		FileProps[] arr = fileImport.list(path, fileName);
		if (ArrayUtils.isNotEmpty(arr)) {
			if (isOverwrite)
				Stream.of(arr).forEach(file -> fileImport.delete(file.getPath()));
			else
				Stream.of(arr).forEach(file -> fileImport.move(file.getPath(), path + appendDateStr(file.getPath())));
		}
		fileImport.move(fileImport.getProps().getLoadProcessPath() + fileName, path + fileName);
	}

	private void moveToSuccess(String fileName, boolean isOverwrite) {
		String path = fileImport.getProps().getLoadSuccessPath();
		FileProps[] arr = fileImport.list(path, fileName);
		if (ArrayUtils.isNotEmpty(arr)) {
			if (isOverwrite)
				Stream.of(arr).forEach(file -> fileImport.delete(file.getPath()));
			else
				Stream.of(arr).forEach(file -> fileImport.move(file.getPath(), path + appendDateStr(file.getPath())));
		}
		fileImport.move(fileImport.getProps().getLoadProcessPath() + fileName, path + fileName);
	}

	private void moveToProcess(String fileName, boolean isOverwrite) {
		String path = fileImport.getProps().getLoadProcessPath();
		FileProps[] arr = fileImport.list(path, fileName);
		if (ArrayUtils.isNotEmpty(arr)) {
			if (isOverwrite)
				Stream.of(arr).forEach(file -> fileImport.delete(file.getPath()));
			else
				Stream.of(arr).forEach(file -> fileImport.move(file.getPath(), path + appendDateStr(file.getPath())));
		}
		fileImport.move(fileImport.getProps().getLoadImportPath(fileName), path + fileName);
	}

	private String appendDateStr(String fileName) {
		return String.format("%s_%s.%s", FilenameUtils.getBaseName(fileName), new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()), FilenameUtils.getExtension(fileName));
	}

	private boolean compareColumnList(List<RptSrcFileHdrColModel> dbColumnList, List<RptSrcFileHdrColModel> fileColumnNameList) {
		if (CollectionUtils.isEmpty(dbColumnList) && CollectionUtils.isEmpty(fileColumnNameList))
			return true;
		if (CollectionUtils.isEmpty(fileColumnNameList))
			return false;
		if (dbColumnList.size() != fileColumnNameList.size())
			return false;

		List<String> list = fileColumnNameList.stream().map(RptSrcFileHdrColModel::getColumnName).collect(Collectors.toList());
		return dbColumnList.stream().allMatch(o -> list.contains(o.getColumnName()));
	}

	private RptSrcFileHdrColModel parseColumnModel(String columnDesc, int columnIdx) {
		RptSrcFileHdrColModel model = new RptSrcFileHdrColModel();
		model.setColumnIdx(columnIdx);
		model.setColumnName(TableNameUtils.parse(columnDesc));
		model.setColumnDesc(columnDesc);
		return model;
	}

	// get data

	private Map<String, Integer> getUserTabColumnList(String tableName) {
		UserTabColsModel filter = new UserTabColsModel();
		filter.setTableSchema(tableSchema);
		filter.setTableName(tableName);
		List<UserTabColsModel> list = informationSchemaMapper.getUserTabColsList(filter);
		if (CollectionUtils.isEmpty(list))
			return new HashMap<>();
		List<String> excludes = Stream.of(UserTabColumnExcl.values()).map(Enum::name).collect(Collectors.toList());
		return list.stream().filter(o -> !excludes.contains(o.getColumnName())).collect(Collectors.toMap(UserTabColsModel::getColumnName, UserTabColsModel::getMaxLength));
	}

	private List<RptSrcFileHdrColModel> findColumnList(int headerId) {
		RptSrcFileHdrColModel filter = new RptSrcFileHdrColModel();
		filter.setSrcFileHdrId(headerId);
		List<RptSrcFileHdrColModel> list = srcFileMapper.getSrcFileColumnList(filter);
		return null == list ? new ArrayList<>() : list;
	}

	private RptSrcFileHdrModel findEffectiveHeader(String fileNameStart) {
		List<RptSrcFileHdrModel> list = srcFileMapper.getSrcFileHeaderList(fileNameStart);
		return CollectionUtils.isEmpty(list) ? null : list.get(0);
	}

	// set data

	private int createSrcFileHeader(int fileId) {
		RptSrcFileHdrModel model = new RptSrcFileHdrModel();
		model.setSrcFileId(fileId);
		model.setEffectiveDate(new Date());
		srcFileMapper.createSrcFileHeader(model);
		return model.getId();
	}

	private void createSrcFileHeaderColumn(int headerId, RptSrcFileHdrColModel model, List<RptSrcFileHdrColModel> list) {
		model.setSrcFileHdrId(headerId);
		model.setDataType(FieldDataType.VARCHAR.name());
		list.stream().filter(c -> c.getColumnName().equals(model.getColumnName())).findFirst().ifPresent(src -> {
			model.setDataType(getOrElse(src.getDataType(), FieldDataType.VARCHAR.name()));
			model.setDataFunc(src.getDataFunc());
		});
		srcFileMapper.createSrcFileHeaderColumn(model);
	}

	private void createFileUploadData(Connection conn, int fileUploadId, RptFileUpldDataModel model, String query) {
		try {
			/* model.setFileUpldId(fileUploadId); */
			/* executeSql(conn, model.getSql()); */
			executeSql(conn, query);
			/* if (model.getLineSeq() % 500 == 0) */
			logger.info(String.format(MSG_INSERT_FORMAT, FortifyStrategyUtils.toLogString(model.getDynamicTable()), model.getLineSeq()));
		} catch (Exception e) {
			String errMsg = String.format(MSG_LINE_FORMAT, ERROR, model.getLineSeq(), FortifyStrategyUtils.toLogString(model.getDynamicTable()), FortifyStrategyUtils.toErrString(e));
			logger.error(errMsg);
			throw new SysRuntimeException(FileImportResponseCode.E2013, errMsg, e);
		}
	}

	private void createFileUploadDataStatic(Connection conn, int fileUploadId, RptFileUpldDataStaticModel model, String query) {
		try {
			/* model.setFileUpldId(fileUploadId);*/
			/* executeSql(conn, model.getSql()); */
			executeSql(conn, query);
			/* if (model.getLineSeq() % 500 == 0) */
			logger.info(String.format(MSG_INSERT_FORMAT, FortifyStrategyUtils.toLogString(model.getTableName()), model.getLineSeq()));
		} catch (Exception e) {
			String errMsg = String.format(MSG_LINE_FORMAT, ERROR, model.getLineSeq(), FortifyStrategyUtils.toLogString(model.getTableName()), FortifyStrategyUtils.toErrString(e));
			logger.error(errMsg);
			throw new SysRuntimeException(FileImportResponseCode.E2014, errMsg, e);
		}
	}

	private int updateFileUploadHeader(int fileUploadId, int headerId) {
		RptFileUpldModel model = new RptFileUpldModel();
		model.setId(fileUploadId);
		model.setSrcFileHdrId(headerId);
		model.setUpldDate(new Date());
		model.setStatus(UploadStatus.PENDING.index());
		fileUpldMapper.updateFileUploadHeader(model);
		return model.getId();
	}

	public int updateFileUploadRowCount(int fileUploadId, int lineCnt, int lineUpld) {
		RptFileUpldModel model = new RptFileUpldModel();
		model.setId(fileUploadId);
		model.setFileLineCount(lineCnt);
		model.setFileLineUpld(lineUpld);
		fileUpldMapper.updateFileUploadRowCount(model);
		return model.getId();
	}

	public int updateFileUploadStatus(int fileUploadId, UploadStatus status) {
		RptFileUpldModel model = new RptFileUpldModel();
		model.setId(fileUploadId);
		model.setStatus(null == status ? null : status.index());
		fileUpldMapper.updateFileUploadStatus(model);
		return model.getId();
	}

	public void updateFileUpdateDataIndex(Connection conn, int fileUploadId) {
		int step = 0;
		String sql = null;
		try {
			// step 1: eliminate old version in current upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep1(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
			// step 1: update latest version in current upload without any conditions
			else {
				sql = fileUpldMapper.getSqlFileUploadDataIndexUpdate(fileUploadId);
				logger.info(sql);
				executeSql(conn, sql);
				return;
			}
			// step 2: eliminate old line_seq in current upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep2(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
			// step 3: update first occurrence in current upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep3(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
			// step 4: update not latest version in current upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep4(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
			// step 5: update not latest version in past upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep5(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
			// step 6: update latest version in current upload
			step++;
			sql = fileUpldMapper.getSqlFileUploadDataIndexUpdateStep6(fileUploadId);
			if (StringUtils.isNotBlank(sql)) {
				logger.info(sql);
				executeSql(conn, sql);
			}
		} catch (Exception e) {
			String errMsg = String.format("Fail to run at step %d -> %s %s", step, sql, FortifyStrategyUtils.toErrString(e));
			logger.error(errMsg);
			throw new SysRuntimeException(FileImportResponseCode.E2015, errMsg, e);
		}
	}

	private void executeSql(Connection conn, String sql) throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(ClassPathUtils.toCharString(sql, 32, 256));
			ps.execute();
		} finally {
			if(ps!=null)
				ps.close();
		}
	}

	// local properties

	private class SrcFileProps {

		private String headerName;
		private String tableName;
		private List<RptSrcFileHdrColModel> fileColumnNameList = new ArrayList<>();
		private List<RptFileUpldDataModel> dynamicColumnList = new ArrayList<>();
		private List<RptFileUpldDataStaticModel> staticColumnList = new ArrayList<>();
		private Map<String, Integer> userTabColsList = new LinkedHashMap<>();
		private DataRowFilter dataRowFilter = null;
		private DataColOutput dataColMask = null;
		private String[] fileColumnNames = null;
		private boolean initFileColumnNameList = true;

		public SrcFileProps(String headerName) {
			this.headerName = headerName;
		}
		public SrcFileProps put(String tableName) {
			this.tableName = tableName;
			return this;
		}
		public SrcFileProps put(List<RptSrcFileHdrColModel> fileColumnNameList) {
			this.fileColumnNameList = null == fileColumnNameList ? new ArrayList<>() : fileColumnNameList;
			this.initFileColumnNameList = true;
			return this;
		}
		public SrcFileProps put(Map<String, Integer> userTabColsList) {
			this.userTabColsList = null == userTabColsList ? new LinkedHashMap<>() : userTabColsList;
			this.initFileColumnNameList = true;
			return this;
		}
		public SrcFileProps put(DataRowFilter dataRowFilter) {
			this.dataRowFilter = dataRowFilter;
			return this;
		}
		public SrcFileProps put(DataColOutput dataColMask) {
			this.dataColMask = dataColMask;
			return this;
		}
		public SrcFileProps put(String[] fileColumnNames) {
			this.fileColumnNames = fileColumnNames;
			return this;
		}
		public String getHeaderName() {
			return headerName;
		}
		public String getTableName() {
			return tableName;
		}
		public String getDynmaicTable() {
			return getTableName() + DynQuerySqlUtils.DATA_SUBFIX;
		}
		public List<RptSrcFileHdrColModel> getFileColumnNameList() {
			if (initFileColumnNameList) {
				if (CollectionUtils.isNotEmpty(fileColumnNameList) && MapUtils.isNotEmpty(userTabColsList)) {
					fileColumnNameList.stream().forEach(o -> o.setStaticInd(Indicator.toString(userTabColsList.keySet().contains(o.getColumnName()))));
				}
				initFileColumnNameList = false;
			}
			return fileColumnNameList;
		}
		public List<RptFileUpldDataModel> getDynamicColumnList() {
			return dynamicColumnList;
		}
		public List<RptFileUpldDataStaticModel> getStaticColumnList() {
			return staticColumnList;
		}
		public Map<String, Integer> getUserTabColsList() {
			return userTabColsList;
		}
		public DataRowFilter getDataRowFilter() {
			return dataRowFilter;
		}
		public DataColOutput getDataColOutput() {
			return dataColMask;
		}
		public String[] getFileColumnNames() {
			return fileColumnNames;
		}
	}

	private enum UserTabColumnExcl {
		  FILE_UPLD_ID
		, INE_SEQ
		, CREATED_DATE
	}
}
