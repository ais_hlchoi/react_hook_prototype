package com.dbs.vtsp.dao;

import com.dbs.framework.aspect.annotation.CurrentUser;
import com.dbs.framework.aspect.annotation.CurrentUser.Action;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldLogModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.RptFileDnldUserGrpModel;

public interface FileDnldMapper {

	RptFileDnldModel getFileDownload(Integer id);

	RptFileDnldDataModel getFileDownloadData(Integer id);

	int createFileDownload(RptFileDnldModel model);

	int createFileDownloadData(RptFileDnldDataModel model);

	int createFileDownloadUserGroup(RptFileDnldUserGrpModel model);

	int createFileDownloadLog(RptFileDnldLogModel model);

	int updateFileDownloadStatus(@CurrentUser(value = Action.IGNORE, ignore = { "lastUpdatedBy" }) RptFileDnldModel model);

	int updateFileDownloadStart(@CurrentUser(value = Action.IGNORE, ignore = { "lastUpdatedBy" }) RptFileDnldModel model);

	int updateFileDownloadProcess(@CurrentUser(value = Action.IGNORE, ignore = { "lastUpdatedBy" }) RptFileDnldModel model);

	int updateFileDownloadFailure(@CurrentUser(value = Action.IGNORE, ignore = { "lastUpdatedBy" }) RptFileDnldModel model);

	int updateFileDownloadComplete(@CurrentUser(value = Action.IGNORE, ignore = { "lastUpdatedBy" }) RptFileDnldModel model);

	int updateFileDownloadStatusPending(Integer id);

	int updateFileDownloadLogAged(Integer id);
}
