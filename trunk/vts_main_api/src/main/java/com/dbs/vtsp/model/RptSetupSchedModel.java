package com.dbs.vtsp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupSchedModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String refTable;

	@JsonInclude
	private Integer refId;

	@JsonInclude
	private String name;

	@JsonInclude
	private String cronExpression;

	@JsonInclude
	private String exportOption;

	@JsonInclude
	private Date lastExecutionTime;

	@JsonInclude
	private Date nextExecutionTime;

	@JsonInclude
	private Date startDate;

	@JsonInclude
	private Date expiredDate;

	@JsonInclude
	private String status;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;
}
