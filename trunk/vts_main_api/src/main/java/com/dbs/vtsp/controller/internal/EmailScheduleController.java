package com.dbs.vtsp.controller.internal;

import com.dbs.util.EmailStatusEnum;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.service.ReportScheduleService;
import com.dbs.vtsp.service.SysEmailProcessService;
import com.dbs.vtsp.service.SysEmailSendingService;
import com.dbs.vtsp.service.SysEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmailScheduleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SysEmailProcessService sysEmailProcessService;

	@Scheduled(cron = "${schedule.email.sending}")
	public void process() {
		logger.info("Scheduled report export start");
		sysEmailProcessService.process();
		logger.info("Scheduled report export end");
	}
}
