package com.dbs.vtsp.security.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.dbs.vtsp.security.auth.jwt.JwtCustomVerifier;
import com.dbs.vtsp.security.auth.jwt.UsernamePasswordAuthenticationBearer;

public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	public JwtAuthenticationFilter() {
		super("/**");
	}

	@Override
	protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
		return true;
	}

	private JwtCustomVerifier jwtVerifier = new JwtCustomVerifier();

	@Value("${internal.access.path}")
	private String internalAccessPath;

	@Value("${swagger.bypass}")
	private String byPassSwagger;

	@Value("${swagger.isEnable}")
	private Boolean swaggerIsEnable;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		String header = request.getHeader("Authorization");

		String path = request.getRequestURI().substring(request.getContextPath().length());
		if (swaggerIsEnable && Arrays.stream(byPassSwagger.split(",")).anyMatch(it -> path.startsWith(it))) {
			return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken("InternalAccess", null));
		}

		if (header == null || !header.startsWith("Bearer ")) {
			if (header == null && StringUtils.isNotEmpty(internalAccessPath) && request.getRequestURI().startsWith(internalAccessPath))
				return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken("InternalAccess", null));
			else
				throw new SecurityException("No JWT token found in request headers");
		}
		String authToken = header.substring(7);
		return getAuthenticationManager().authenticate(UsernamePasswordAuthenticationBearer.create(jwtVerifier.check(authToken).block()).block());
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		// As this authentication is in HTTP header, after success we need to continue the request normally
		// and return the response as if the resource was not secured at all
		chain.doFilter(request, response);
	}
}
