package com.dbs.vtsp.service.impl;


import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.config.ImportConfig;
import com.dbs.util.Assert;
import com.dbs.util.EmailConfigEnum;
import com.dbs.util.EmailStatusEnum;
import com.dbs.util.FilenameUtils;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.SysEmailAttachInputStreamModel;
import com.dbs.vtsp.model.SysEmailAttachModel;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.SysEmailProcessService;
import com.dbs.vtsp.service.SysEmailSendingService;
import com.dbs.vtsp.service.SysEmailService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysEmailProcessServiceImpl extends AbstractServiceImpl implements SysEmailProcessService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SysEmailService sysEmailService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SysEmailSendingService sysEmailSendingService;


    @Value("${sys.email.enable}")
    private Boolean sysEmailEnable;

    @Autowired
    private SysParmMapper sysParmMapper;


    public int process() {
        SysParmModel sysParmModel= new SysParmModel();
        sysParmModel.setSegment(EmailConfigEnum.EMAIL_CONFIG.value());
        List<SysParmModel> sysParmModelList = sysParmMapper.searchSysParmValueBySegment(sysParmModel);
        Assert.isFalse(CollectionUtils.isEmpty(sysParmModelList), AssertResponseCode.NOT_NULL_DATA.getMsg());


        if(!sysEmailEnable) {
            logger.info(String.format("Disabled SysEmail Function in application.properties , cannot send email"));
            return 0;
        }
        List<SysEmailModel> sysEmailPendingList = sysEmailService.getEmailListPending();
        if(sysEmailPendingList == null || sysEmailPendingList.size() < 1){
            logger.info( String.format("No email were found Function , cannot send email"));
            return 0;
        }


        List<SysEmailModel> sysEmailRunningList = sysEmailService.updateEmailListStatus(sysEmailPendingList, EmailStatusEnum.RUNNING);

        List<SysEmailModel> successList = new ArrayList<>();
        List<SysEmailModel> failedList = new ArrayList<>();
        for (SysEmailModel sysEmailModel : sysEmailRunningList) {
            //boolean sendResult = sysEmailSendingService.fakeSendEmail(sysEmailModel);
            boolean sendResult;
            try {
               sendResult = sysEmailSendingService.sendEmail(sysEmailModel);
            }
            catch (RuntimeException re){
                sendResult = false;
            }
            catch (Exception e){
                sendResult = false;
            }
            if (sendResult) {
                successList.add(sysEmailModel);
            } else {
                failedList.add(sysEmailModel);
            }
        }
        logger.info("successList :" + successList.size());
        logger.info("failedList :" + failedList.size());
        if (successList.size() > 0) {
            sysEmailService.updateEmailListStatus(successList, EmailStatusEnum.SENT);
        }
        if (failedList.size() > 0) {
            sysEmailService.updateEmailListStatus(failedList, EmailStatusEnum.FAIL);
        }

        return 0;
    }


}
