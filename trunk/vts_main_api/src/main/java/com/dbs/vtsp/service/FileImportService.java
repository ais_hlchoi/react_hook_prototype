package com.dbs.vtsp.service;

import java.sql.SQLException;
import java.util.List;

import com.dbs.framework.model.LocalHost;
import com.dbs.vtsp.model.RptSrcFileModel;

public interface FileImportService {

	void process(LocalHost host);

	void upload(RptSrcFileModel model);

	void reload(String fileNameStart);

	void index(String fileNameStart) throws SQLException;

	List<RptSrcFileModel> getSrcFileList(String fileNameStart);
}