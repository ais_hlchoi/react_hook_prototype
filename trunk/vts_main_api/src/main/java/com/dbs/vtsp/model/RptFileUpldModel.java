package com.dbs.vtsp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileUpldModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer srcFileId;

	@JsonInclude
	private Integer srcFileHdrId;

	@JsonInclude
	private String fileName;

	@JsonInclude
	private String fileDate;

	@JsonInclude
	private Integer fileLineCount;

	@JsonInclude
	private Integer fileLineUpld;

	@JsonInclude
	private Long fileSize;

	@JsonInclude
	private Date upldDate;

	@JsonInclude
	private String status;

	@JsonInclude
	private String action;
}
