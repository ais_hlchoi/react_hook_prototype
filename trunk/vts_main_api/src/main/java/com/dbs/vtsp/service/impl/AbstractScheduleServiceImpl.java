package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.dbs.module.schedule.constant.ReportScheduleSegment;
import com.dbs.module.schedule.constant.ReportScheduleSegment.ReportScheduleCode;
import com.dbs.module.schedule.util.SysParmUtils;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.SysParmModel;

public abstract class AbstractScheduleServiceImpl extends AbstractServiceImpl {

	protected static final String MSG_OUT_OF_SERVICE = "Service is stopped since segment %s status is %s";
	protected static final String MSG_EMPTY_LIMIT = "No data is processed since segment %s limit is %s";
	protected static final String MSG_FOLDER_NOT_FOUND = "Setup error, folder not exists %s";
	protected static final String MSG_WORK_IN_PROGRESS = "%s still in processing in last scheduler";

	protected abstract SysParmMapper getSysParmMapper();

	protected List<SysParmModel> getSchedAttrList(ReportScheduleSegment segment) {
		List<SysParmModel> list = getSysParmMapper().searchSysParmValueBySegment(SysParmUtils.parse(segment.index()));
		return CollectionUtils.isEmpty(list) ? new ArrayList<>() : list;
	}

	protected String getSchedAttrValue(ReportScheduleCode code, ReportScheduleSegment segment) {
		return getSchedAttrValue(code, getSchedAttrList(segment));
	}

	protected String getSchedAttrValue(ReportScheduleCode code, List<SysParmModel> list) {
		return CollectionUtils.isEmpty(list) ? null : list.stream().filter(o -> code.name().equals(o.getCode())).map(o -> o.getParmValue()).findFirst().orElse(null);
	}

	protected int getSchedAttrIntValue(ReportScheduleCode code, ReportScheduleSegment segment) {
		return getSchedAttrIntValue(code, getSchedAttrList(segment));
	}

	protected int getSchedAttrIntValue(ReportScheduleCode code, List<SysParmModel> list) {
		return getIntValue(getSchedAttrValue(code, list), 0);
	}

	protected int getIntValue(String value, int valueIfNull) {
		return StringUtils.isBlank(value) || !NumberUtils.isCreatable(value) ? valueIfNull : NumberUtils.createInteger(value);
	}

	protected List<SysParmModel> getSysParamListBySegmentValue(String segment, String parmValue) {
		if (StringUtils.isBlank(segment) || StringUtils.isBlank(parmValue))
			return new ArrayList<>();

		SysParmModel model = new SysParmModel();
		model.setSegment(segment);

		List<SysParmModel> list = getSysParmMapper().searchSysParmSegment(model);
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		model = list.stream().filter(o -> parmValue.equalsIgnoreCase(o.getParmValue())).findFirst().orElse(null);
		if (null == model)
			return new ArrayList<>();

		list = getSysParmMapper().searchSysParmValueBySegment(model);
		return null == list ? new ArrayList<>() : list;
	}
}
