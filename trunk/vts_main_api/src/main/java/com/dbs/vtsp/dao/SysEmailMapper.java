package com.dbs.vtsp.dao;



import com.dbs.vtsp.model.SysEmailDataModel;
import com.dbs.vtsp.model.SysEmailModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysEmailMapper {

	List<SysEmailModel> getEmailListPending();
	SysEmailDataModel getEmailContent(Integer id);

	Integer updateStatus(SysEmailModel sysEmailModel);
	Integer updateStatusList(@Param("ids") List<Integer> ids, @Param("status") String status,@Param("lastUpdatedBy") String lastUpdatedBy);

}
