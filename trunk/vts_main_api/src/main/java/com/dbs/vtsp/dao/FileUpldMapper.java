package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptFileUpldDataModel;
import com.dbs.vtsp.model.RptFileUpldDataStaticModel;
import com.dbs.vtsp.model.RptFileUpldModel;

public interface FileUpldMapper {

	int createFileUpload(RptFileUpldModel model);

	int createFileUploadData(RptFileUpldDataModel model);

	int createFileUploadDataStatic(RptFileUpldDataStaticModel model);

	int updateFileUploadHeader(RptFileUpldModel model);

	int updateFileUploadRowCount(RptFileUpldModel model);

	int updateFileUploadStatus(RptFileUpldModel model);

	List<RptFileUpldModel> getFileUploadStatusList(String status);

	Integer getLastFileUploadId(String action);

	String getSqlFileUploadDataIndexUpdate(Integer id);

	String getSqlFileUploadDataIndexUpdateStep1(Integer id);

	String getSqlFileUploadDataIndexUpdateStep2(Integer id);

	String getSqlFileUploadDataIndexUpdateStep3(Integer id);

	String getSqlFileUploadDataIndexUpdateStep4(Integer id);

	String getSqlFileUploadDataIndexUpdateStep5(Integer id);

	String getSqlFileUploadDataIndexUpdateStep6(Integer id);
}
