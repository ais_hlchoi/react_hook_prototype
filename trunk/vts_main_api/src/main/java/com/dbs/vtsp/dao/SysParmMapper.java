package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.SysParmModel;

public interface SysParmMapper {

	List<SysParmModel> searchSysParmSegment(SysParmModel model);

	List<SysParmModel> searchSysParmValueBySegment(SysParmModel model);
}
