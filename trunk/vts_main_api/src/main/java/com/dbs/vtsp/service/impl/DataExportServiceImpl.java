package com.dbs.vtsp.service.impl;

import static com.itextpdf.html2pdf.css.CssConstants.LANDSCAPE;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.alibaba.druid.pool.DruidDataSource;
import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.framework.model.LocalHost;
import com.dbs.framework.model.LocalHostModel;
import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.util.ReportDownloadUtils;
import com.dbs.module.export.DataExport;
import com.dbs.module.export.Grid;
import com.dbs.module.export.config.ExportConfig;
import com.dbs.module.export.constant.FileExportResponseCode;
import com.dbs.module.export.data.GridTableData;
import com.dbs.module.export.util.DataParserUtils;
import com.dbs.module.export.util.ReportFilePathUtils;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.config.ImportConfig;
import com.dbs.module.file.util.LocalStorageUtils;
import com.dbs.module.model.FileModel;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.module.report.constant.ReportLayoutType;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.ExcelUtil;
import com.dbs.util.FilenameUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.ResponseCodeUtils;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.service.DataExportService;
import com.dbs.vtsp.service.FileDownloadService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.styledxmlparser.css.media.MediaDeviceDescription;
import com.itextpdf.styledxmlparser.css.media.MediaType;

@Service
public class DataExportServiceImpl extends AbstractServiceImpl implements DataExportService {

	private final Logger logger = LoggerFactory.getLogger(DataExportServiceImpl.class);

	@Autowired
	@Qualifier("dataSourceObj2")
	private DruidDataSource dataSourceObj2;

	@Autowired
	private DownloadConfig downloadConfig;

	@Autowired
	private ExportConfig exportConfig;

	@Autowired
	private ImportConfig importConfig;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private FileImport fileImport;

	private static final int ROW_LOGGING_INTERVAL = 2500;
	private static final int ROW_LOGGING_MIN_STAGE = 4;

	private static final String MSG_LINE_FORMAT = "Error found at line %d when parse file %s caused by %s";

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportXlsx(DataExport model, String pid, Integer id) throws SQLException, IOException {
		String templatePath = exportConfig.getExcelTemplatePath();
		XSSFWorkbook workbook = getWorkbook(templatePath);
		Assert.notNull(workbook, String.format("Report template not found at %s", FortifyStrategyUtils.toLogString(templatePath)), FileExportResponseCode.E3011);

		LocalHost host = LocalHostModel.of(pid);
		List<Map<String, Object>> list = executeSql(model);
		int rowCount = list.size();
		int rowProcess = 0;
		boolean rowLogging = (rowCount / ROW_LOGGING_INTERVAL) >= ROW_LOGGING_MIN_STAGE;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		XSSFSheet metadataSheet = workbook.getSheetAt(0);
		workbook.setSheetName(0 , "Metadata");
		XSSFSheet dataSheet = workbook.createSheet("Data");

		XSSFCellStyle styleHeader = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		styleHeader.setFont(font);

		// no need style, we load from template, template got style already
		String valueIfNull = "--";
		ExcelUtil.setCellValue(metadataSheet, "B2", datetime, ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "B4", getOrElse(model.getSelect(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "B5", getOrElse(model.getFrom(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "E4", getOrElse(model.getWhere(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "G4", getOrElse(model.getOrderBy(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "G5", getOrElse(model.getGroupBy(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "K4", getOrElse(model.getGroupAs(), valueIfNull), ExcelUtil.TEXT, null);
		// replace this cell to blank
//		ExcelUtil.setCellValue(metadataSheet, "K5", getOrElse(model.getSql(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "J5", "", ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "K5", "", ExcelUtil.TEXT, null);
		// end

		Grid data = new GridTableData(list, model.getLayout());

		if (data.isNotEmpty()) {
			try {
				int rowIdx = ExcelUtil.FIRST_INDEX - 1;
				int colIdx = ExcelUtil.FIRST_INDEX;

				// header
				for (String head : data.getHead()) {
					ExcelUtil.setCellValue(dataSheet, colIdx, rowIdx, head, ExcelUtil.TEXT, styleHeader);
					colIdx++;
				}
				rowIdx++;

				// body
				for (List<String> body : data.getBody()) {
					colIdx = ExcelUtil.FIRST_INDEX;
					for (String row : body) {
						ExcelUtil.setCellValue(dataSheet, colIdx, rowIdx, row, ExcelUtil.TEXT, null);
						colIdx++;
					}
					rowIdx++;
					rowProcess++;
					if (rowLogging && rowProcess % ROW_LOGGING_INTERVAL == 0)
						fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
				}

				fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
			} catch (Exception e) {
				logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
				fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
				throw new SysRuntimeException(FileExportResponseCode.E3101, FortifyStrategyUtils.toErrString(e), e);
			}
		}

		String destFile = getAttachFileTempPath(ExportOption.XLSX.ext, now, id);
		logger.info(String.format("Excel file is located at %s", destFile));

		FileOutputStream out = new FileOutputStream(FortifyStrategyUtils.toPathString(destFile));
		workbook.write(out);
		out.close();
		workbook.close();

		File file = createTempFile(destFile);
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.XLSX.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("Excel file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportCsv(DataExport model, String pid, Integer id) throws SQLException, IOException {
		LocalHost host = LocalHostModel.of(pid);
		List<Map<String, Object>> list = executeSql(model);
		int rowCount = list.size();
		int rowProcess = 0;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();

		String destFile = getAttachFileTempPath(ExportOption.CSV.ext, now, id);
		logger.info(String.format("CSV file is located at %s", destFile));

		File file = createTempFile(destFile);
		if (CollectionUtils.isEmpty(list)) {
			String s3File = getAttachFileFullPath(ExportOption.CSV.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("CSV file is saved at %s", s3File));
			destFile = s3File;
			fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
			fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
			return deleteTempFile(file);
		}

		try (
				Writer writer = new FileWriter(createTempFile(destFile), true);
			) {
			CsvSchema schema = null;
			CsvSchema.Builder schemaBuilder = CsvSchema.builder();
			try {
				switch (ReportLayoutType.getOrElse(model.getLayout())) {
				case H:
					list.get(0).keySet().stream().forEach(schemaBuilder::addColumn);
					schema = schemaBuilder.build().withLineSeparator(System.lineSeparator()).withHeader();
					break;
				case V:
					list = DataParserUtils.transpose(list);
					list.get(0).keySet().stream().forEach(schemaBuilder::addColumn);
					schema = schemaBuilder.build().withLineSeparator(System.lineSeparator());
					break;
				default:
					break;
				}

				CsvMapper mapper = new CsvMapper();
				mapper.writer(schema).writeValues(writer).writeAll(list);
				fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
			} catch (Exception e) {
				logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
				fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
				throw new SysRuntimeException(FileExportResponseCode.E3102, FortifyStrategyUtils.toErrString(e), e);
			}
			writer.flush();
		}
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.CSV.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("CSV file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportPdf(DataExport model, String pid, Integer id) throws SQLException, IOException {
		LocalHost host = LocalHostModel.of(pid);
		List<Map<String, Object>> list = executeSql(model);
		int rowCount = list.size();
		int rowProcess = 0;
		boolean rowLogging = (rowCount / ROW_LOGGING_INTERVAL) >= ROW_LOGGING_MIN_STAGE;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		String destFile = getAttachFileTempPath(ExportOption.PDF.ext, now, id);
		logger.info(String.format("PDF file is located at %s", destFile));

		PdfDocument pdfDocument = new PdfDocument(new PdfWriter(createTempFile(destFile)));
		pdfDocument.setDefaultPageSize(PageSize.A4.rotate());

		ConverterProperties properties = new ConverterProperties();
		MediaDeviceDescription med = new MediaDeviceDescription(MediaType.ALL);
		med.setOrientation(LANDSCAPE);
		properties.setMediaDeviceDescription(med);

		String opnTr = "<tr><td>";
		String clsTr = "</td></tr>";
		String nxtTd = "</td><td>";
		String valueIfNull = "--";

		StringBuilder html = new StringBuilder();
		html.append("<h3 style=\"font-size:14px;\">Custom Report</h3><br/>");
		html.append("<table style=\"font-size:12px;\">");
		html.append(opnTr).append("<b>Export Date:</b>").append(nxtTd).append(datetime).append(clsTr);
		html.append(opnTr).append("<b>Search Criteria:</b>").append(clsTr);
		html.append(opnTr)
			.append("<b>Search clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getSelect(), valueIfNull))
			.append(nxtTd)
			.append("<b>Where clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getWhere(), valueIfNull))
			.append(nxtTd)
			.append("<b>Order by:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getOrderBy(), valueIfNull))
			.append(nxtTd)
			.append("<b>Sum column:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getGroupAs(), valueIfNull))
			.append(clsTr);
		html.append(opnTr)
			.append("<b>From clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getFrom(), valueIfNull))
			.append(nxtTd)
			.append("<b></b>")
			.append(nxtTd)
			.append("")
			.append(nxtTd)
			.append("<b>Group by:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getGroupBy(), valueIfNull))
			.append(nxtTd)
			.append("<b></b>")
			.append(nxtTd)
			.append("")
			.append(clsTr);
		html.append("</table><br/><br/>");
		html.append("<div style=\"page-break-after: always;\"></div>");

		Grid data = new GridTableData(list, model.getLayout());

		if (data.isNotEmpty()) {
			try {
				html.append("<table style=\"font-size:12px;\">");
				html.append("<tr>");

				// header
				for (String head : data.getHead()) {
					html.append("<td><b>").append(head).append("</b></td>");
				}

				// body
				for (List<String> body : data.getBody()) {
					html.append("<tr>");
					for (String row : body) {
						html.append("<td>").append(row).append("</td>");
					}
					html.append("</tr>");
					rowProcess++;
					if (rowLogging && rowProcess % ROW_LOGGING_INTERVAL == 0)
						fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
				}

				html.append("</table>");
				fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
			} catch (Exception e) {
				logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
				fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
				throw new SysRuntimeException(FileExportResponseCode.E3103, FortifyStrategyUtils.toErrString(e), e);
			}
		}

		HtmlConverter.convertToPdf(html.toString(), pdfDocument, properties);
		pdfDocument.close();

		File file = createTempFile(destFile);
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.PDF.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("PDF file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	public void updateFileDownloadStatus(DownloadStatus status, String pid, Integer id) {
		LocalHost host = LocalHostModel.of(pid);
		fileDownloadService.updateFileDownloadStatus(ReportDownloadUtils.parse(status, id), host);
	}

	protected List<Map<String, Object>> executeSql(DataExport model) {
		return null == model ? new ArrayList<>() : executeSql(getSql(model));
	}

	protected List<Map<String, Object>> executeSql(String sql) {
		logger.info(String.format("Execute query -> %s", FortifyStrategyUtils.toLogString(sql)));
		List<Map<String, Object>> list = new ArrayList<>();
		try (
				Connection conn = dataSourceObj2.getConnection();
				PreparedStatement ps = conn.prepareStatement(ClassPathUtils.toCharString(sql, 32, 256));
				ResultSet rs = ps.executeQuery();
			) {
			ResultSetMetaData metaData = rs.getMetaData();
			int count = metaData.getColumnCount();
			while (rs.next()) {
				Map<String, Object> columns = new LinkedHashMap<>();
				for (int i = 1; i <= count; i++) {
					columns.put(metaData.getColumnLabel(i), rs.getObject(i));
				}
				list.add(columns);
			}
		} catch (Exception e) {
			String msg = String.format("Errors caused by %s when execute [ %s ]", FortifyStrategyUtils.toErrString(e), sql);
			logger.error(msg);
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), msg);
		}
		return list;
	}

	private String getSql(DataExport model) {
		return model.getSql();
	}

	private XSSFWorkbook getWorkbook(String file) throws IOException {
		XSSFWorkbook workbook = null;
		if (S3ClientUtils.isService()) {
			workbook = new XSSFWorkbook(new ByteArrayInputStream(fileImport.getFileByte(file)));
		} else {
			workbook = new XSSFWorkbook(new FileInputStream(createTempFile(file)));
		}
		return workbook;
	}

	private File createTempFile(String file) throws IOException {
		ReportFilePathUtils.preparePath(file);
		return new LocalStorageUtils(importConfig).getFile(file);
	}

	private boolean deleteTempFile(File file) {
		if (null == file)
			return false;
		if (S3ClientUtils.isService())
			new LocalStorageUtils(importConfig).delete(file.getAbsolutePath());
		file = null;
		return true;
	}

	private String getAttachFileTempPath(String ext, Date date, Integer seq) {
		logger.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(downloadConfig.getFolderTemp(), ext, seq)));
		return ReportFilePathUtils.getAttachFileFullPath(downloadConfig.getFolderTemp(), ext, date, seq);
	}

	private String getAttachFileFullPath(String ext, Date date, Integer seq) {
		logger.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(downloadConfig.getFolderOutput(), ext, seq)));
		return ReportFilePathUtils.getAttachFileFullPath(downloadConfig.getFolderOutput(), ext, date, seq);
	}

	private String replaceFileOutputPath(String path) {
		return ReportFilePathUtils.replaceFileOutputPath(downloadConfig.getFolderOutput(), path);
	}

	public RptFileDnldModel getFileDownload(Integer id) {
		return fileDownloadService.getFileDownload(ReportDownloadUtils.parse(id));
	}

	public RptFileDnldDataModel getFileDownloadData(Integer id) {
		return fileDownloadService.getFileDownloadData(ReportDownloadUtils.parse(id));
	}
}
