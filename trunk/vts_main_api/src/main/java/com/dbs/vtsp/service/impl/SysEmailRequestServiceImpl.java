package com.dbs.vtsp.service.impl;


import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.constant.RestEntity;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.util.*;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.*;
import com.dbs.vtsp.service.SysEmailRequestService;
import com.dbs.vtsp.service.SysEmailTmplService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SysEmailRequestServiceImpl extends AbstractServiceImpl implements SysEmailRequestService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${sys.email.url}")
	private String sysEmailUrl;

	@Autowired
	private SysParmMapper sysParmMapper;

	@Autowired
	private SysEmailTmplService sysEmailTmplService;


	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum, String yyyymmddhhmmss){
		SysParmModel sysParmModel= new SysParmModel();
		sysParmModel.setSegment(emailAlertEnum.value());
		List<SysParmModel> sysParmModelList = sysParmMapper.searchSysParmValueBySegment(sysParmModel);
		Assert.isFalse(CollectionUtils.isEmpty(sysParmModelList), AssertResponseCode.NOT_NULL_DATA.getMsg());

		SysParmModel subjectModel = sysParmModelList.stream().filter(it->EmailAlertEnum.SUBJECT.equals(it.getCode())).findFirst().orElse(null);
		Assert.notNull(subjectModel, AssertResponseCode.NOT_NULL_DATA.getMsg());

		SysParmModel recipientModel = sysParmModelList.stream().filter(it->EmailAlertEnum.RECIPIENT.equals(it.getCode())).findFirst().orElse(null);
		Assert.notNull(recipientModel, AssertResponseCode.NOT_NULL_DATA.getMsg());

		SysParmModel contentModel = sysParmModelList.stream().filter(it->EmailAlertEnum.CONTENT.equals(it.getCode())).findFirst().orElse(null);
		Assert.notNull(contentModel, AssertResponseCode.NOT_NULL_DATA.getMsg());
		SysEmailModel sysEmailModelTmp = new SysEmailModel();
		sysEmailModelTmp.setType(emailAlertEnum.value());
		sysEmailModelTmp.setRecipient(recipientModel.getParmValue());
		sysEmailModelTmp.setRecipientCc(null);
		sysEmailModelTmp.setRecipientBcc(null);
		sysEmailModelTmp.setSubject(subjectModel.getParmValue());
		SysEmailDataModel sysEmailDataModel = new SysEmailDataModel();
		sysEmailDataModel.setContent(contentModel.getParmValue());
		sysEmailModelTmp.setSysEmailDataModel(sysEmailDataModel);
		sysEmailModelTmp.setSendDate(yyyymmddhhmmss);
		sysEmailModelTmp.setRemark(null);
		sysEmailModelTmp.setSysEmailAttachModelList(null);

		//Object reference will change sysEmailModel
		return requestEmail(enumStringHashMap,sysEmailModelTmp);

	}
	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum, Date date){
		return requestEmailAlert(enumStringHashMap,emailAlertEnum, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
	}
	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmailAlert(HashMap<EmailParamUtil,String> enumStringHashMap, EmailAlertEnum emailAlertEnum) {
		return requestEmailAlert(enumStringHashMap,emailAlertEnum, new Date());

	}

	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap, Integer refId, String refTable,List<String> attachmentPaths ){
		SysEmailTmplModel sysEmailTmplModel = sysEmailTmplService.getSysEmailTmpl(refId,refTable);
		return requestEmail(enumStringHashMap,sysEmailTmplModel,attachmentPaths);
	}

	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap, SysEmailTmplModel tmpl ,List<String> attachmentPaths ){
		SysEmailModel sysEmailModel = new SysEmailModel();
		sysEmailModel.setType(EmailAlertEnum.EMAIL_ALERT_DONE_SCHED_RPT.value());
		sysEmailModel.setRecipient(tmpl.getRecipient());
		sysEmailModel.setRecipientCc(tmpl.getRecipientCc());
		sysEmailModel.setRecipientBcc(tmpl.getRecipientBcc());
		sysEmailModel.setSubject(tmpl.getSubject());
		SysEmailDataModel sysEmailDataModel = new SysEmailDataModel();
		sysEmailDataModel.setContent(tmpl.getContent());
		sysEmailModel.setSysEmailDataModel(sysEmailDataModel);
		sysEmailModel.setSendDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		sysEmailModel.setRemark(null);
		List<SysEmailAttachModel>  sysEmailAttachModels  = attachmentPaths
				.stream()
				.map(path-> { SysEmailAttachModel s = new SysEmailAttachModel(); s.setFilePath(path); return s;})
				.collect(Collectors.toList());
		sysEmailModel.setSysEmailAttachModelList(sysEmailAttachModels);

		return requestEmail(enumStringHashMap,sysEmailModel);
	}
	@OperationLog(operation = OperationLogAction.CREATE_EMAIL)
	public boolean requestEmail(HashMap<EmailParamUtil,String> enumStringHashMap,SysEmailModel sysEmailModel){
		try {
			//Temp object to insert
			SysEmailModel sysEmailModelResult = new SysEmailModel();
			sysEmailModelResult.setType(sysEmailModel.getType());
			sysEmailModelResult.setRecipient(sysEmailModel.getRecipient());
			sysEmailModelResult.setRecipientCc(sysEmailModel.getRecipientCc());
			sysEmailModelResult.setRecipientBcc(sysEmailModel.getRecipientBcc());
			sysEmailModelResult.setSubject(sysEmailModel.getSubject());
			SysEmailDataModel sysEmailDataModel = new SysEmailDataModel();
			sysEmailDataModel.setContent(sysEmailModel.getSysEmailDataModel().getContent());
			sysEmailModelResult.setSysEmailDataModel(sysEmailDataModel);
			sysEmailModelResult.setSendDate(sysEmailModel.getSendDate());
			sysEmailModelResult.setRemark(sysEmailModel.getRemark());
			sysEmailModelResult.setSysEmailAttachModelList(sysEmailModel.getSysEmailAttachModelList());

			//Object reference will change sysEmailModel
			EmailParamUtil.executeKeyReplacement(sysEmailModelResult,enumStringHashMap);
			insert(sysEmailModelResult, sysEmailUrl);

		}
		catch (Exception e){

			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), String.format("Error found when %s send Email %s", ClassPathUtils.getName().split(".")[1], FortifyStrategyUtils.toLogString(sysEmailUrl)));

		}
		return true;
	}


	private Integer insert(final SysEmailModel model, final String url) throws JsonProcessingException {
		if (StringUtils.isBlank(url) || null == model)
			return null;
		return save(model, url);
	}


	private Integer save(SysEmailModel model, String sysEmailUrl) {
		Integer id = null;
		try {
			JsonNode node = ApiUtils.post(sysEmailUrl, model, null);
			ApiUtils.validate(node);
			JsonNode object = node.get(RestEntity.OBJECT.key);
			if (null == object || !NumberUtils.isCreatable(object.asText()))
				return id;
			id = NumberUtils.createInteger(object.asText());
		} catch (JsonProcessingException e) {
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), String.format("Error found when %s send Email %s", ClassPathUtils.getName().split(".")[1], FortifyStrategyUtils.toLogString(sysEmailUrl)));
		}
		return id;
	}





}
