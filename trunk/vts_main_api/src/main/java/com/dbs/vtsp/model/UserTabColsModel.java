package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTabColsModel {

	@JsonInclude
	private String tableSchema;

	@JsonInclude
	private String tableName;

	@JsonInclude
	private String columnName;

	@JsonInclude
	private Integer columnId;

	@JsonInclude
	private String dataType;

	@JsonInclude
	private Integer maxLength;

	@JsonInclude
	private Integer dataPrecision;

	@JsonInclude
	private String nullable;

	@JsonInclude
	private String columnDefault;
}
