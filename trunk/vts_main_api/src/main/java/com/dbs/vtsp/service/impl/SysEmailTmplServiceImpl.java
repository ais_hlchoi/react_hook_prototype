package com.dbs.vtsp.service.impl;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.report.*;
import com.dbs.module.report.constant.Datatype;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.data.*;
import com.dbs.module.report.model.*;
import com.dbs.module.report.util.*;
import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.*;
import com.dbs.vtsp.model.*;
import com.dbs.vtsp.service.SysEmailTmplService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SysEmailTmplServiceImpl extends AbstractServiceImpl implements SysEmailTmplService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());



	@Autowired
	private SysEmailTmplMapper sysEmailTmplMapper;




	@Override
	public SysEmailTmplModel getSysEmailTmpl(SysEmailTmplModel model) {
		return getSysEmailTmpl(model.getRefId(),model.getRefTable());
	}


	@Override
	public SysEmailTmplModel getSysEmailTmpl(Integer refId, String refTable) {
		SysEmailTmplModel existEmailTmplModel =  sysEmailTmplMapper.getSysEmailTmpl(refId,refTable);
		return existEmailTmplModel;
	}



}
