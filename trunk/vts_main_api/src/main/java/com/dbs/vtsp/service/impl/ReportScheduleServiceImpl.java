package com.dbs.vtsp.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.druid.pool.DruidDataSource;
import com.dbs.framework.model.LocalHost;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.download.util.ReportDownloadUtils;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.module.report.util.ReportPropsUtils;
import com.dbs.module.report.util.ReportTemplateUtils;
import com.dbs.module.schedule.constant.ReportScheduleConstants;
import com.dbs.module.schedule.constant.ReportScheduleSegment;
import com.dbs.module.schedule.constant.ReportScheduleSegment.ReportScheduleCode;
import com.dbs.module.schedule.constant.ReportScheduleStatus;
import com.dbs.module.schedule.util.CronExpressionUtils;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.InformationSchemaMapper;
import com.dbs.vtsp.dao.RptSchedMapper;
import com.dbs.vtsp.dao.RptSetupSchedMapper;
import com.dbs.vtsp.dao.RptTmplMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.RptSetupSchedModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.FileDownloadService;
import com.dbs.vtsp.service.ReportScheduleService;

@Service
public class ReportScheduleServiceImpl extends AbstractScheduleServiceImpl implements ReportScheduleService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RptSchedMapper rptSchedMapper;

	@Autowired
	private RptSetupSchedMapper rptSetupSchedMapper;

	@Autowired
	private RptTmplMapper rptTmplMapper;

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private SysParmMapper sysParmMapper;

	@Autowired
	private InformationSchemaMapper informationSchemaMapper;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private DownloadConfig downloadConfig;

	@Value("${datasource.tableSchema}")
	private String tableSchema;

	protected SysParmMapper getSysParmMapper() {
		return sysParmMapper;
	}

	@Autowired
	@Qualifier("dataSourceObj2")
	private DruidDataSource dataSourceObj2;

	@Autowired
	@Qualifier("dataSourceObj")
	private DruidDataSource dataSourceObj1;

	private final static String TEST_UPDATE_STMT = "update sys_parm set short_desc_tc = '1' where 1 = 2";

	public void update() throws SQLException  {
		try (
				Connection conn = dataSourceObj1.getConnection();
				PreparedStatement ps = conn.prepareStatement(TEST_UPDATE_STMT);
			) {
				ps.executeUpdate();
		}
	}

	public void updateReadOnly() throws SQLException {
		try (
				Connection conn = dataSourceObj2.getConnection();
				PreparedStatement ps = conn.prepareStatement(TEST_UPDATE_STMT);
			) {
				ps.executeUpdate();
		}
	}

	public void process(LocalHost host) {
		String schedHost = getSchedAttrValue(ReportScheduleCode.HOSTNAME, ReportScheduleSegment.SCHED_USER_REPORT);

		if (StringUtils.isNotBlank(schedHost) && Stream.of(schedHost.split("\\s*,\\s*")).noneMatch(c -> c.equalsIgnoreCase(host.getHostName()))) {
			logger.info(String.format("Host not match <- %s", FortifyStrategyUtils.toLogString(host.getHostName(), schedHost)));
			return;
		}

		String sysDate = new SimpleDateFormat(ReportScheduleConstants.EXECUTION_TIME_FOMRAT).format(new Date());
		logger.info(String.format("Check scheduled report at %s (%s)", sysDate, ReportScheduleConstants.EXECUTION_TIME_FOMRAT));

		List<RptSetupSchedModel> list = getScheduledReportList(sysDate);
		if (CollectionUtils.isEmpty(list)) {
			logger.info("No scheduled report in queue.");
			return;
		}
		logger.info(String.format("%d scheduled report%s in queue.", list.size(), list.size() > 1 ? "s" : ""));
		for (RptSetupSchedModel o : list) {
			try {
				update(o);
			} catch (Exception e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
			}
		}
	}

	public void update(RptSetupSchedModel model) {
		if (ReportScheduleStatus.isError(model.getStatus())) {
			logger.info(String.format("Skip to update next execution time %s", FortifyStrategyUtils.toLogString(model.getCronExpression(), model.getRefId())));
			return;
		}

		ReportScheduleSegment schedSegment = ReportScheduleSegment.SCHED_USER_REPORT;
		List<SysParmModel> attrList = getSchedAttrList(schedSegment);

		String schedStatus = getSchedAttrValue(ReportScheduleCode.STATUS, attrList);
		if (!BooleanUtils.toBoolean(schedStatus)) {
			logger.info(String.format("Service is stopped since segment %s status is %s", schedSegment.index(), FortifyStrategyUtils.toLogString(schedStatus, model.getId())));
			return;
		}

		if (null != model.getNextExecutionTime()) {
			ReportTemplateObjectModel rpt = getReportDownloadItem(model.getRefId());
			rpt.getUserReportTemplate().setAdvanceSql(getQuerySql(ReportTemplateUtils.parse(rpt)));
			ReportDownload bean = ReportDownloadUtils.parsePending(ReportTemplateUtils.parse(rpt), ClassPathUtils.getOrElse(model.getName(), downloadConfig.getFileDownloadReportName()), model.getId());
			Assert.isTrue(fileDownloadService.createFileDownload(bean) > 0, String.format("Fail to create file download %s", FortifyStrategyUtils.toLogString(model.getRefTable(), model.getRefId())));
		}
		Date nextExecTime = CronExpressionUtils.getNextValidTimeBetweenStartDateEndDate(model.getCronExpression(),model.getStartDate(),model.getExpiredDate()) ;
		model.setNextExecutionTime(nextExecTime);
		//if (null == model.getNextExecutionTime()) {
			//logger.info(String.format("Fail to parse cron expression %s", FortifyStrategyUtils.toLogString(model.getCronExpression())));
			//model.setStatus(ReportScheduleStatus.ERROR.index());
		//} else
		if (ReportScheduleStatus.INITIAL.index().equals(model.getStatus())) {
			logger.info(String.format("Schedule to new cron expression %s", FortifyStrategyUtils.toLogString(model.getCronExpression())));
			model.setStatus(ReportScheduleStatus.SCHEDULE.index());
		}
		Assert.isTrue(rptSetupSchedMapper.updateNextExecution(model) == 1, String.format("Fail to update report schedule %s", FortifyStrategyUtils.toLogString(model.getRefTable(), model.getRefId())));
		logger.info(String.format("Done to update next execution time %s", FortifyStrategyUtils.toLogString(model.getCronExpression(), model.getRefTable(), model.getRefId())));
	}

	public List<RptSetupSchedModel> getScheduledReportList(String text) {
		List<RptSetupSchedModel> list = rptSchedMapper.getScheduledReportList(text);
		return null == list ? new ArrayList<>() : list;
	}

	protected ReportTemplateObjectModel getReportDownloadItem(Integer id) {
		return rptTmplMapper.getUserReportTemplateObjectNoPrivs(ReportPropsUtils.parse(id));
	}

	protected String getQuerySql(ReportTemplate model) {
		String sql = DynQuerySqlUtils.render(model, srcFileMapper, informationSchemaMapper, tableSchema);
		return StringUtils.isBlank(sql) || null == model.getOption() ? sql : ReportParamUtils.parse(sql, model.getOption().getParameter());
	}
}
