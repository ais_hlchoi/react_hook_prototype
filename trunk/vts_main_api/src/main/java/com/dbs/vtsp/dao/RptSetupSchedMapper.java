package com.dbs.vtsp.dao;

import com.dbs.vtsp.model.RptSetupSchedModel;

public interface RptSetupSchedMapper {

	int updateLastExecution(RptSetupSchedModel model);

	int updateNextExecution(RptSetupSchedModel model);
}
