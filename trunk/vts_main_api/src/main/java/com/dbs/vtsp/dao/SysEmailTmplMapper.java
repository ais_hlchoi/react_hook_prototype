package com.dbs.vtsp.dao;

import com.dbs.vtsp.model.RptSetupSchedModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysEmailTmplMapper {


	SysEmailTmplModel getSysEmailTmpl(@Param("refId") Integer refId , @Param("refTable") String refTable);

	int insert(SysEmailTmplModel model);

	int delete(@Param("refId") Integer refId , @Param("refTable") String refTable);

	int update(SysEmailTmplModel model);



}
