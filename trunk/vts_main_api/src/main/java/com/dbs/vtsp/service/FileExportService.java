package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.framework.model.LocalHost;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.data.ReportDownloadCategoryData;
import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.schedule.FileExport;

public interface FileExportService {

	void process(LocalHost host);

	void export(ReportDownloadStatusData model);

	List<ReportDownloadStatusData> getReportDownloadPendingList(FileExport model);

	List<ReportDownloadStatusData> getReportDownloadOutstandingList(ReportDownload model);

	ReportDownloadCategoryData getReportDownloadCategory(Integer id);
}
