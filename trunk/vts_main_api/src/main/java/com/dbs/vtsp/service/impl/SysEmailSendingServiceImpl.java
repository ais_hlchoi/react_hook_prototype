package com.dbs.vtsp.service.impl;


import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.file.FileImport;
import com.dbs.util.*;
import com.dbs.vtsp.dao.SysEmailMapper;
import com.dbs.vtsp.dao.SysParmMapper;
import com.dbs.vtsp.model.SysEmailAttachInputStreamModel;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.model.SysParmModel;
import com.dbs.vtsp.service.SysEmailSendingService;
import com.dbs.vtsp.service.SysEmailService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysEmailSendingServiceImpl extends AbstractServiceImpl implements SysEmailSendingService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private FileImport fileImport;

	@Value("${sys.email.enable}")
	private Boolean sysEmailEnable;

	@Autowired
	DownloadConfig downloadConfig;

	@Autowired
	private SysParmMapper sysParmMapper;


	public static final String FOLDER_OUTPUT = "${folder.output}/";






	@OperationLog(operation = OperationLogAction.SEND_EMAIL)
	public boolean fakeSendEmail(SysEmailModel sysEmailModel){
		try{
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setFrom("hoilokchoi@dbs.com");
			msg.setTo("hoilokchoi@dbs.com");
			msg.setSubject("Testing from Spring Boot");
			msg.setText("Hello World \n Spring Boot Email");
			//javaMailSender.send(msg);
			return Math.floor((Math.random()*100))%2 == 1;
//			if(true) {
//				return true;
//			}
//			else{
//				return false;
//			}
//

		}
		catch (Exception e){
			return false;
		}

	}

	@OperationLog(operation = OperationLogAction.SEND_EMAIL)
	public boolean sendEmail(SysEmailModel sysEmailModel) {

		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			SysParmModel sysParmModel= new SysParmModel();
			sysParmModel.setSegment(EmailConfigEnum.EMAIL_CONFIG.value());
			List<SysParmModel> sysParmModelList = sysParmMapper.searchSysParmValueBySegment(sysParmModel);
			Assert.isFalse(CollectionUtils.isEmpty(sysParmModelList), AssertResponseCode.NOT_NULL_DATA.getMsg());

			SysParmModel subjectModel = sysParmModelList.stream().filter(it-> EmailConfigEnum.SENDER.equals(it.getCode())).findFirst().orElse(null);
			Assert.notNull(subjectModel, AssertResponseCode.NOT_NULL_DATA.getMsg());
			helper.setFrom(subjectModel.getParmValue());
			//helper.setFrom("paulpochichan@dbs.com");

			String[] recipents = sysEmailModel.getRecipient().split(";");
			//helper.setTo("paulpochichan@dbs.com");
			helper.setTo(recipents);//TODO

			//helper.setTo(sysEmailModel.getRecipient());
			if(sysEmailModel.getRecipientCc()!=null) {
				helper.setCc(sysEmailModel.getRecipientCc());
			}
			if(sysEmailModel.getRecipientBcc()!=null) {
				helper.setBcc(sysEmailModel.getRecipientBcc());
			}

			helper.setSubject(sysEmailModel.getSubject());
			if(sysEmailModel.getSysEmailDataModel().getContent()!=null) {
				helper.setText(sysEmailModel.getSysEmailDataModel().getContent());
			}

			if( sysEmailModel.getSysEmailAttachModelList()!=null) {
				List<SysEmailAttachInputStreamModel> inputStreams = sysEmailModel.getSysEmailAttachModelList().stream()
						.map(attachModel -> {
							String path = attachModel.getFilePath().replace(FOLDER_OUTPUT, downloadConfig.getFolderOutput());
							try {

								byte[] fileByte = fileImport.getFileByte(path);
								final InputStreamSource fileStreamSource = new ByteArrayResource(fileByte);
								SysEmailAttachInputStreamModel model = new SysEmailAttachInputStreamModel();
								model.setFileName(FilenameUtils.getBaseName(path) + "." + FilenameUtils.getExtension(path));
								model.setInputStreamSource(fileStreamSource);
								return model;
							} catch (Exception e) {
								logger.error("Cannot get the related file " + path + " from s3 , skip this file");
								return null;
							}

						})
						.collect(Collectors.toList());


				for (SysEmailAttachInputStreamModel inputStreamModel : inputStreams) {
					if (inputStreamModel != null && inputStreamModel.getInputStreamSource() != null) {
						helper.addAttachment(inputStreamModel.getFileName(), inputStreamModel.getInputStreamSource());
					}
				}
			}

			SysParmModel enableModel = sysParmModelList.stream().filter(it-> EmailConfigEnum.ENABLED.equals(it.getCode())).findFirst().orElse(null);
			Assert.notNull(enableModel, AssertResponseCode.NOT_NULL_DATA.getMsg());
			if(enableModel.getParmValue().equalsIgnoreCase("true")){

				javaMailSender.send(msg);
				return true;
			}
			else {
				logger.info(String.format("Disabled SysEmail Function in sysParam , cannot send email %s", FortifyStrategyUtils.toLogString(sysEmailModel.getEmailId())));
				return false;
			}

		}
		catch (Exception e){
			logger.error(String.format("Sys email send failed %s", FortifyStrategyUtils.toLogString(sysEmailModel.getEmailId())));
			throw new RuntimeException(String.format("Sys email send failed %s", FortifyStrategyUtils.toLogString(sysEmailModel.getEmailId())));
		}





	}


}
