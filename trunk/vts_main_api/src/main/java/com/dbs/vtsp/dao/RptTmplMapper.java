package com.dbs.vtsp.dao;

import com.dbs.module.report.ReportProps;
import com.dbs.module.report.model.ReportTemplateObjectModel;

public interface RptTmplMapper {

	ReportTemplateObjectModel getUserReportTemplateObjectNoPrivs(ReportProps model);
}
