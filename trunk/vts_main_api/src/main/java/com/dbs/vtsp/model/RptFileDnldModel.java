package com.dbs.vtsp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String rptName;

	@JsonInclude
	private Date rptDate;

	@JsonInclude
	private String rptType;

	@JsonInclude
	private Integer parentId;

	@JsonInclude
	private String fileName;

	@JsonInclude
	private String filePath;

	@JsonInclude
	private String fileExtension;

	@JsonInclude
	private Integer fileLineCount;

	@JsonInclude
	private Integer fileLineProcess;

	@JsonInclude
	private Long fileSize;

	@JsonInclude
	private String printCode;

	@JsonInclude
	private Integer printCount;

	@JsonInclude
	private Date lastPrintedDate;

	@JsonInclude
	private String status;
}
