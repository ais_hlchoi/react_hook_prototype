package com.dbs.vtsp.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileUpldDataStaticModel {

	@JsonInclude
	private String tableName;

	@JsonInclude
	private String columnNames;

	@JsonInclude
	private String columnValues;

	@JsonInclude
	private Integer fileUpldId;

	@JsonInclude
	private Integer lineSeq;

	public RptFileUpldDataStaticModel(String tableName, String columnNames, String columnValues, int lineSeq) {
		setTableName(tableName);
		setColumnNames(columnNames);
		setColumnValues(columnValues);
		setLineSeq(lineSeq);
	}

	public RptFileUpldDataStaticModel() {
	}

	public String getSql() {
		List<String> args = new ArrayList<>();
		args.add("insert into " + FortifyStrategyUtils.toPathString(getTableName()) + " (");
		args.add("  file_upld_id");
		args.add(", line_seq");
		args.add(", latest_version_ind");
		args.add(", partition_ind");
		args.add(", created_date");
		args.add(", " + FortifyStrategyUtils.toPathString(getColumnNames()));
		args.add(") values (");
		args.add("  " + FortifyStrategyUtils.toPathString(String.valueOf(getFileUpldId())));
		args.add(", " + FortifyStrategyUtils.toPathString(String.valueOf(getLineSeq())));
		args.add(", 'D'");
		args.add(", date_format(now(), '" + DynQuerySqlUtils.PARTITION_BY_FORMAT + "')");
		args.add(", current_timestamp(6)");
		args.add(", " + FortifyStrategyUtils.toPathString(getColumnValues()));
		args.add(")");
		return StringUtils.join(args, "");
	}
}
