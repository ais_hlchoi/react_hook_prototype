package com.dbs.vtsp.service;

import java.io.IOException;
import java.util.List;

import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.export.DataMergeHelper;

public interface DataMergeService extends DataMergeHelper {

	List<ReportDownloadStatusData> getReportDownloadStatusList(Integer id);

	void preview(Integer id) throws IOException;
}
