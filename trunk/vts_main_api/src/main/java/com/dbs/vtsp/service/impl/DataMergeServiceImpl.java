package com.dbs.vtsp.service.impl;

import static com.itextpdf.html2pdf.css.CssConstants.LANDSCAPE;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.framework.model.LocalHost;
import com.dbs.framework.model.LocalHostModel;
import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.data.ReportDownloadStatusData;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.download.util.ReportDownloadUtils;
import com.dbs.module.export.DataExport;
import com.dbs.module.export.Grid;
import com.dbs.module.export.config.ExportConfig;
import com.dbs.module.export.constant.FileExportResponseCode;
import com.dbs.module.export.data.GridCsvData;
import com.dbs.module.export.util.CSVReaderUtils;
import com.dbs.module.export.util.DataExportUtils;
import com.dbs.module.export.util.ReportFilePathUtils;
import com.dbs.module.file.FileImport;
import com.dbs.module.file.config.ImportConfig;
import com.dbs.module.file.util.LocalStorageUtils;
import com.dbs.module.model.FileModel;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.module.report.constant.ReportLayoutType;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.ExcelUtil;
import com.dbs.util.FilenameUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.DataExportMapper;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.service.DataExportService;
import com.dbs.vtsp.service.DataMergeService;
import com.dbs.vtsp.service.FileDownloadService;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.styledxmlparser.css.media.MediaDeviceDescription;
import com.itextpdf.styledxmlparser.css.media.MediaType;

@Service
public class DataMergeServiceImpl extends AbstractServiceImpl implements DataMergeService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DataExportMapper dataExportMapper;

	@Autowired
	private DownloadConfig downloadConfig;

	@Autowired
	private ExportConfig exportConfig;

	@Autowired
	private ImportConfig importConfig;

	@Autowired
	private DataExportService dataExportService;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private FileImport fileImport;

	private static final int ROW_LOGGING_INTERVAL = 2500;
	private static final int ROW_LOGGING_MIN_STAGE = 4;

	private static final String REPORT = "Report";
	private static final String MSG_LINE_FORMAT = "Error found at line %d when parse file %s caused by %s";

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportXlsx(String pid, Integer id) throws IOException {
		String templatePath = exportConfig.getExcelTemplatePath();
		XSSFWorkbook workbook = getWorkbook(templatePath);
		Assert.notNull(workbook, String.format("Report template not found at %s", FortifyStrategyUtils.toLogString(templatePath)), FileExportResponseCode.E3011);

		LocalHost host = LocalHostModel.of(pid);
		ReportDownloadObjectModel cat = dataExportMapper.getReportDownloadCategory(id);
		Assert.notNull(cat, AssertResponseCode.NOT_NULL_DATA.getMsg(), FileExportResponseCode.E3012);
		Assert.isTrue(CollectionUtils.isNotEmpty(cat.getChildren()), AssertResponseCode.NOT_EMPTY.getMsg(REPORT), FileExportResponseCode.E3013);
		DataExport model = DataExportUtils.parse(null);
		int rowCount = cat.getChildren().stream().mapToInt(child -> getOrElse(child.getFileLineCount(), 0)).sum();
		int rowProcess = 0;
		boolean rowLogging = (rowCount / ROW_LOGGING_INTERVAL) >= ROW_LOGGING_MIN_STAGE;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		XSSFSheet metadataSheet = workbook.getSheetAt(0);
		workbook.setSheetName(0 , "Metadata");
		XSSFSheet dataSheet = workbook.createSheet("Data");

		XSSFCellStyle styleHeader = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		styleHeader.setFont(font);

		// no need style, we load from template, template got style already
		String valueIfNull = "--";
		ExcelUtil.setCellValue(metadataSheet, "B2", datetime, ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "B4", getOrElse(model.getSelect(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "B5", getOrElse(model.getFrom(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "E4", getOrElse(model.getWhere(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "G4", getOrElse(model.getOrderBy(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "G5", getOrElse(model.getGroupBy(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "K4", getOrElse(model.getGroupAs(), valueIfNull), ExcelUtil.TEXT, null);
		// replace this cell to blank
//		ExcelUtil.setCellValue(metadataSheet, "K5", getOrElse(model.getSql(), valueIfNull), ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "J5", "", ExcelUtil.TEXT, null);
		ExcelUtil.setCellValue(metadataSheet, "K5", "", ExcelUtil.TEXT, null);
		// end

		int rowIdx = ExcelUtil.FIRST_INDEX;
		int colIdx = ExcelUtil.FIRST_INDEX;

		for (int i = 0; i < cat.getChildren().size(); i++) {
			ReportDownloadObjectModel child = cat.getChildren().get(i);
			String filePath = restoreFileOutputPath(child.getFilePath());
			List<List<String>> lines = CSVReaderUtils.readValue(filePath);

			if (CollectionUtils.isEmpty(lines))
				continue;

			try {
				Grid data = new GridCsvData(lines);

				if (i > 0)
					rowIdx++;

				colIdx = ExcelUtil.FIRST_INDEX;

				// header
				XSSFCellStyle headerStyle = ReportLayoutType.H == ReportLayoutType.getOrElse(child.getData().getRptLayout()) ? styleHeader : null;
				for (String head : data.getHead()) {
					ExcelUtil.setCellValue(dataSheet, colIdx, rowIdx, head, ExcelUtil.TEXT, headerStyle);
					colIdx++;
				}
				rowIdx++;

				// body
				for (List<String> body : data.getBody()) {
					colIdx = ExcelUtil.FIRST_INDEX;
					for (String row : body) {
						ExcelUtil.setCellValue(dataSheet, colIdx, rowIdx, row, ExcelUtil.TEXT, null);
						colIdx++;
					}
					rowIdx++;
					rowProcess++;
					if (rowLogging && rowProcess % ROW_LOGGING_INTERVAL == 0)
						fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
				}

				if (i < cat.getChildren().size() - 1)
					fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
			} catch (Exception e) {
				logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
				fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
				throw new SysRuntimeException(FileExportResponseCode.E3111, FortifyStrategyUtils.toErrString(e), e);
			}
		}

		String destFile = getAttachFileTempPath(ExportOption.XLSX.ext, now, id);
		logger.info(String.format("Excel file is located at %s", destFile));

		FileOutputStream out = new FileOutputStream(FortifyStrategyUtils.toPathString(destFile));
		workbook.write(out);
		out.close();
		workbook.close();

		File file = createTempFile(destFile);
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.XLSX.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("Excel file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportCsv(String pid, Integer id) throws IOException {
		LocalHost host = LocalHostModel.of(pid);
		ReportDownloadObjectModel cat = dataExportMapper.getReportDownloadCategory(id);
		Assert.notNull(cat, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isTrue(CollectionUtils.isNotEmpty(cat.getChildren()), AssertResponseCode.NOT_EMPTY.getMsg(REPORT));
		int rowCount = cat.getChildren().stream().mapToInt(child -> getOrElse(child.getFileLineCount(), 0)).sum();
		int rowProcess = 0;
		boolean rowLogging = (rowCount / ROW_LOGGING_INTERVAL) >= ROW_LOGGING_MIN_STAGE;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();
		String destFile = getAttachFileTempPath(ExportOption.CSV.ext, now, id);
		logger.info(String.format("CSV file is located at %s", destFile));

		File file = createTempFile(destFile);
		try (
				BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
			) {
			for (int i = 0; i < cat.getChildren().size(); i++) {
				ReportDownloadObjectModel child = cat.getChildren().get(i);
				String filePath = restoreFileOutputPath(child.getFilePath());
				List<String> lines = CSVReaderUtils.readLine(filePath);

				if (CollectionUtils.isEmpty(lines))
					continue;

				try {
					if (i > 0)
						writer.newLine();

					if (ReportLayoutType.H == ReportLayoutType.getOrElse(child.getData().getRptLayout()))
						rowProcess -= 1;

					Iterator<String> it = lines.iterator();
					while (it.hasNext()) {
						writer.write(it.next());
						writer.newLine();
						rowProcess++;
						if (rowLogging && rowProcess % ROW_LOGGING_INTERVAL == 0)
							fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
					}

					if (i < cat.getChildren().size() - 1)
						fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
				} catch (Exception e) {
					logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
					fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
					throw new SysRuntimeException(FileExportResponseCode.E3112, FortifyStrategyUtils.toErrString(e), e);
				}
				writer.flush();
			}
		}
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.CSV.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("CSV file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public boolean exportPdf(String pid, Integer id) throws IOException {
		LocalHost host = LocalHostModel.of(pid);
		ReportDownloadObjectModel cat = dataExportMapper.getReportDownloadCategory(id);
		Assert.notNull(cat, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isTrue(CollectionUtils.isNotEmpty(cat.getChildren()), AssertResponseCode.NOT_EMPTY.getMsg(REPORT));
		DataExport model = DataExportUtils.parse(null);
		int rowCount = cat.getChildren().stream().mapToInt(child -> getOrElse(child.getFileLineCount(), 0)).sum();
		int rowProcess = 0;
		boolean rowLogging = (rowCount / ROW_LOGGING_INTERVAL) >= ROW_LOGGING_MIN_STAGE;
		fileDownloadService.updateFileDownloadStart(ReportDownloadUtils.parseStart(rowCount, id), host);

		Date now = new Date();
		String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		String destFile = getAttachFileTempPath(ExportOption.PDF.ext, now, id);
		logger.info(String.format("PDF file is located at %s", destFile));

		PdfDocument pdfDocument = new PdfDocument(new PdfWriter(createTempFile(destFile)));
		pdfDocument.setDefaultPageSize(PageSize.A4.rotate());

		ConverterProperties properties = new ConverterProperties();
		MediaDeviceDescription med = new MediaDeviceDescription(MediaType.ALL);
		med.setOrientation(LANDSCAPE);
		properties.setMediaDeviceDescription(med);

		String opnTr = "<tr><td>";
		String clsTr = "</td></tr>";
		String nxtTd = "</td><td>";
		String valueIfNull = "--";

		StringBuilder html = new StringBuilder();
		html.append("<h3 style=\"font-size:14px;\">Custom Report</h3><br/>");
		html.append("<table style=\"font-size:12px;\">");
		html.append(opnTr).append("<b>Export Date:</b>").append(nxtTd).append(datetime).append(clsTr);
		html.append(opnTr).append("<b>Search Criteria:</b>").append(clsTr);
		html.append(opnTr)
			.append("<b>Search clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getSelect(), valueIfNull))
			.append(nxtTd)
			.append("<b>Where clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getWhere(), valueIfNull))
			.append(nxtTd)
			.append("<b>Order by:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getOrderBy(), valueIfNull))
			.append(nxtTd)
			.append("<b>Sum column:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getGroupAs(), valueIfNull))
			.append(clsTr);
		html.append(opnTr)
			.append("<b>From clause:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getFrom(), valueIfNull))
			.append(nxtTd)
			.append("<b></b>")
			.append(nxtTd)
			.append("")
			.append(nxtTd)
			.append("<b>Group by:</b>")
			.append(nxtTd)
			.append(getOrElse(model.getGroupBy(), valueIfNull))
			.append(nxtTd)
			.append("<b></b>")
			.append(nxtTd)
			.append("")
			.append(clsTr);
		html.append("</table><br/><br/>");
		html.append("<div style=\"page-break-after: always;\"></div>");

		for (int i = 0; i < cat.getChildren().size(); i++) {
			ReportDownloadObjectModel child = cat.getChildren().get(i);
			String filePath = restoreFileOutputPath(child.getFilePath());
			List<List<String>> lines = CSVReaderUtils.readValue(filePath);

			if (CollectionUtils.isEmpty(lines))
				continue;

			try {
				Grid data = new GridCsvData(lines);

				if (i > 0)
					html.append("<br/>");

				// header
				html.append("<table style=\"font-size:12px;\">");
				html.append("<tr>");
				for (String head : data.getHead()) {
					html.append("<td><b>").append(head).append("</b></td>");
				}

				// body
				for (List<String> body : data.getBody()) {
					html.append("<tr>");
					for (String row : body) {
						html.append("<td>").append(row).append("</td>");
					}
					html.append("</tr>");
					rowProcess++;
					if (rowLogging && rowProcess % ROW_LOGGING_INTERVAL == 0)
						fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowProcess, id), host);
				}

				html.append("</table>");

				if (i < cat.getChildren().size() - 1)
					fileDownloadService.updateFileDownloadProcess(ReportDownloadUtils.parseProcess(rowCount, id), host);
			} catch (Exception e) {
				logger.error(String.format(MSG_LINE_FORMAT, rowProcess, FortifyStrategyUtils.toLogString(String.valueOf(id)), FortifyStrategyUtils.toErrString(e)));
				fileDownloadService.updateFileDownloadFailure(ReportDownloadUtils.parseFailure(rowCount, e.getMessage(), id), host);
				throw new SysRuntimeException(FileExportResponseCode.E3113, FortifyStrategyUtils.toErrString(e), e);
			}
		}

		HtmlConverter.convertToPdf(html.toString(), pdfDocument, properties);
		pdfDocument.close();

		File file = createTempFile(destFile);
		if (S3ClientUtils.isService()) {
			String s3File = getAttachFileFullPath(ExportOption.PDF.ext, now, id);
			fileImport.saveFile(FileModel.of(FilenameUtils.getBaseName(destFile), FilenameUtils.getExtension(destFile), file), s3File);
			logger.info(String.format("PDF file is saved at %s", s3File));
			destFile = s3File;
		}
		fileDownloadService.updateFileDownloadComplete(ReportDownloadUtils.parseComplete(replaceFileOutputPath(destFile), file.length(), now, id), host);
		return deleteTempFile(file);
	}

	public void preview(Integer id) throws IOException {
		ReportDownloadObjectModel cat = dataExportMapper.getReportDownloadCategory(id);
		Assert.notNull(cat, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isTrue(CollectionUtils.isNotEmpty(cat.getChildren()), AssertResponseCode.NOT_EMPTY.getMsg(REPORT));
		int rowCount = cat.getChildren().stream().mapToInt(child -> getOrElse(child.getFileLineCount(), 0)).sum();
		int rowProcess = 0;

		for (int i = 0; i < cat.getChildren().size(); i++) {
			ReportDownloadObjectModel child = cat.getChildren().get(i);
			String filePath = restoreFileOutputPath(child.getFilePath());
			List<List<String>> lines = CSVReaderUtils.readValue(filePath);
			logger.info(String.format("%s -> [ dnloadId: %s ] read %s", ClassPathUtils.getName(), id, FortifyStrategyUtils.toLogString(filePath, String.valueOf(CollectionUtils.isEmpty(lines) ? 0 : lines.size()))));
			for (int n = 0; n < Math.min(lines.size(), 10); n++) {
				logger.info(String.format("%s", FortifyStrategyUtils.toLogString(StringUtils.join(lines.get(n), ", "))));
			}
			if (lines.size() > 10) {
				logger.info(String.format("[ ... %d more ]", lines.size() - 10));
			}
			logger.info(StringUtils.repeat("-", 20));
			rowProcess += lines.size();
			if (ReportLayoutType.H == ReportLayoutType.getOrElse(child.getData().getRptLayout()))
				rowProcess -= 1;
		}

		if (rowCount != rowProcess) {
			logger.info(String.format("All rows are R/W ... [ total: %d, process: %d, diff: %d ]", rowCount, rowProcess, rowCount - rowProcess));
		}
	}

	public void updateFileDownloadStatus(DownloadStatus status, String pid, Integer id) {
		LocalHost host = LocalHostModel.of(pid);
		fileDownloadService.updateFileDownloadStatus(ReportDownloadUtils.parse(status, id), host);
	}

	private XSSFWorkbook getWorkbook(String file) throws IOException {
		XSSFWorkbook workbook = null;
		if (S3ClientUtils.isService()) {
			workbook = new XSSFWorkbook(new ByteArrayInputStream(fileImport.getFileByte(file)));
		} else {
			workbook = new XSSFWorkbook(new FileInputStream(createTempFile(file)));
		}
		return workbook;
	}

	private File createTempFile(String file) throws IOException {
		ReportFilePathUtils.preparePath(file);
		return new LocalStorageUtils(importConfig).getFile(file);
	}

	private boolean deleteTempFile(File file) {
		if (null == file)
			return false;
		if (S3ClientUtils.isService())
			new LocalStorageUtils(importConfig).delete(file.getAbsolutePath());
		return true;
	}

	private String getAttachFileTempPath(String ext, Date date, Integer seq) {
		logger.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(downloadConfig.getFolderTemp(), ext, seq)));
		return ReportFilePathUtils.getAttachFileFullPath(downloadConfig.getFolderTemp(), ext, date, seq);
	}

	private String getAttachFileFullPath(String ext, Date date, Integer seq) {
		logger.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(downloadConfig.getFolderOutput(), ext, seq)));
		return ReportFilePathUtils.getAttachFileFullPath(downloadConfig.getFolderOutput(), ext, date, seq);
	}

	private String replaceFileOutputPath(String path) {
		return ReportFilePathUtils.replaceFileOutputPath(downloadConfig.getFolderOutput(), path);
	}

	private String restoreFileOutputPath(String path) {
		return ReportFilePathUtils.restoreFileOutputPath(downloadConfig.getFolderOutput(), path);
	}

	public List<ReportDownloadStatusData> getReportDownloadStatusList(Integer id) {
		List<ReportDownloadObjectModel> list = dataExportMapper.getReportDownloadStatusList(id);
		return CollectionUtils.isEmpty(list) ? new ArrayList<>() : list.stream().map(ReportDownloadStatusData::new).collect(Collectors.toList());
	}

	public boolean exportXlsx(DataExport model, String pid, Integer id) throws SQLException, IOException {
		return dataExportService.exportXlsx(model, pid, id);
	}

	public boolean exportCsv(DataExport model, String pid, Integer id) throws SQLException, IOException {
		return dataExportService.exportCsv(model, pid, id);
	}

	public boolean exportPdf(DataExport model, String pid, Integer id) throws SQLException, IOException {
		return dataExportService.exportPdf(model, pid, id);
	}

	public RptFileDnldModel getFileDownload(Integer id) {
		return dataExportService.getFileDownload(id);
	}

	public RptFileDnldDataModel getFileDownloadData(Integer id) {
		return dataExportService.getFileDownloadData(id);
	}

	public File getFile(String name){
		return fileImport.getFile(name);
	}
}
