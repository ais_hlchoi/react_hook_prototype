package com.dbs.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.dbs.app.Application;
import com.dbs.framework.aspect.annotation.LocalHostConfig;
import com.dbs.framework.aspect.annotation.LocalHostConfig.Action;
import com.dbs.framework.aspect.annotation.LocalHostConfig.Attribute;
import com.dbs.framework.model.LocalHost;
import com.dbs.framework.model.LocalHostModel;
import com.dbs.module.util.AbstractModuleUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalHostUtils extends AbstractModuleUtils {

	private LocalHostUtils() {
		// for SonarQube scanning purpose
	}

	public static void bind(Object obj) {
		if (null == obj)
			return;

		boolean isLocalHostConfig = obj.getClass().isAnnotationPresent(LocalHostConfig.class);
		boolean isActionIgnore = isLocalHostConfig && Action.IGNORE.equals(obj.getClass().getAnnotation(LocalHostConfig.class).value());

		if (isActionIgnore)
			return;

		if (obj instanceof LocalHost) {
			LocalHostModel o = ((LocalHostModel) obj);
			bindData(Attribute.HOST_NAME.key, o, getHostName());
			bindData(Attribute.HOST_ADDRESS.key, o, getHostAddress());
			bindData("serviceCode", o, getServiceCode());
		}
	}

	public static String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			log.info("Fail to get host name");
		}
		return null;
	}

	public static String getHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.info("Fail to get host address");
		}
		return null;
	}

	public static String getServiceCode() {
		return Application.UID;
	}

	public static LocalHost getHostConfig() {
		LocalHostModel o = new LocalHostModel();
		o.setHostName(getHostName());
		o.setHostAddress(getHostAddress());
		o.setServiceCode(getServiceCode());
		return o;
	}
}
