package com.dbs.util;

import java.util.stream.Stream;

public enum HttpHeader {

	  AUTHORIZATION("Authorization", HttpHeaderType.AUTHN)
    , AUTHENTICATION_TYPE_BASIC("Basic", HttpHeaderType.AUTHN)
    , X_AUTH_TOKEN("X-Auth-Token", HttpHeaderType.AUTHN)
    , WWW_AUTHENTICATION("WWW-Authenticate", HttpHeaderType.AUTHN)

    , X_FORWARDED_FOR("X-Forwarded-For", HttpHeaderType.IPADDR)
    , PROXY_CLIENT_IP("Proxy-Client-IP", HttpHeaderType.IPADDR)
    , WL_PROXY_CLIENT_IP("WL-Proxy-Client-IP", HttpHeaderType.IPADDR)
    , HTTP_CLIENT_IP("HTTP_CLIENT_IP", HttpHeaderType.IPADDR)
    , HTTP_X_FORWARDED_FOR("HTTP_X_FORWARDED_FOR", HttpHeaderType.IPADDR)
    , X_REAL_IP("X-Real-IP", HttpHeaderType.IPADDR)
    ;

	private String key;
	private HttpHeaderType type;

	private HttpHeader(String key, HttpHeaderType type) {
		this.key = key;
		this.type = type;
	}

	public String key() {
		return this.key;
	}

	public static String[] getHttpHeaderIpAddress() {
		return Stream.of(HttpHeader.values()).filter(o -> o.type == HttpHeaderType.IPADDR).map(Enum::name).toArray(String[]::new);
	}

	private enum HttpHeaderType {
		  AUTHN
		, IPADDR
	}
}
