package com.dbs.util;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.dbs.framework.aspect.annotation.CurrentUser;
import com.dbs.framework.aspect.annotation.CurrentUser.Action;
import com.dbs.framework.aspect.annotation.CurrentUser.Attribute;
import com.dbs.module.model.UserForm;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.vtsp.model.AbstractAuditModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CurrentUserUtils extends AbstractModuleUtils {

	private CurrentUserUtils() {
		// for SonarQube scanning purpose
	}

	public static void bind(Object obj) {
		new CurrentUserUtils().setOneBankId(obj, null);
	}

	public static void bind(Object obj, String[] ignore) {
		new CurrentUserUtils().setOneBankId(obj, ignore);
	}

	protected void setOneBankId(Object obj, String[] ignore) {
		if (null == obj || isIgnoreAll(obj))
			return;

		String[] ignoreArr = getIgnore(obj, ignore);
		List<String> ignoreList = null == ignoreArr ? null : Arrays.asList(ignoreArr);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = null == auth ? "schedule" : auth.getName();
		List<String> list = null == auth ? new ArrayList<>() : auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

		boolean match = false;
		if (obj instanceof Map || obj instanceof AbstractAuditModel) {
			bindData(Attribute.ONE_BANK_ID, ignoreList, obj, name, true);
			bindData(Attribute.ONE_BANK_ROLE_LIST, ignoreList, obj, list);
			bindData(Attribute.CREATED_BY, ignoreList, obj, name);
			bindData(Attribute.LAST_UPDATED_BY, ignoreList, obj, name);
			match = true;
		} else if (obj instanceof UserForm) {
			bindData(Attribute.ONE_BANK_ID, ignoreList, obj, name, true);
			bindData(Attribute.ONE_BANK_ROLE_LIST, ignoreList, obj, list);
			match = true;
		}
		if (match) {
			log.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(ClassPathUtils.toJSONString(obj))));
		}
	}

	private static void bindData(final Attribute item, final List<String> list, Object model, final Object value) {
		bindData(item, list, model, value, false);
	}

	private static void bindData(final Attribute item, final List<String> list, Object model, final Object value, final boolean check) {
		if (null == item || null == model)
			return;

		boolean isActionIgnore = null != list;
		boolean isIgnoreAll = isActionIgnore && list.size() == 0;
		boolean isIgnoreOne = isActionIgnore && list.contains(item.key);

		if (isActionIgnore && (isIgnoreAll || isIgnoreOne))
			return;

		bindData(item.key, model, value, check);
	}

	public static boolean isCurrentUser(final Object obj) {
		if (obj instanceof Parameter)
			return ((Parameter) obj).isAnnotationPresent(CurrentUser.class);
		else if (obj instanceof Method)
			return ((Method) obj).isAnnotationPresent(CurrentUser.class);
		else
			return obj.getClass().isAnnotationPresent(CurrentUser.class);
	}

	public static boolean isActionIgnore(final Object obj) {
		if (isCurrentUser(obj)) {
			if (obj instanceof Parameter)
				return Action.IGNORE.equals(((Parameter) obj).getAnnotation(CurrentUser.class).value());
			else if (obj instanceof Method)
				return Action.IGNORE.equals(((Method) obj).getAnnotation(CurrentUser.class).value());
			else
				return Action.IGNORE.equals(obj.getClass().getAnnotation(CurrentUser.class).value());
		}
		return false;
	}

	public static boolean isIgnoreAll(final Object obj) {
		return isActionIgnore(obj) && ArrayUtils.isEmpty(getIgnore(obj));
	}

	public static String[] getIgnore(Object obj) {
		if (isActionIgnore(obj)) {
			if (obj instanceof Parameter)
				return ((Parameter) obj).getAnnotation(CurrentUser.class).ignore();
			else if (obj instanceof Method)
				return ((Method) obj).getAnnotation(CurrentUser.class).ignore();
			else
				return obj.getClass().getAnnotation(CurrentUser.class).ignore();
		}
		return null;
	}

	public static String[] getIgnore(final Object obj, final String[] parent) {
		String[] child = getIgnore(obj);

		if (null == parent)
			return child;

		if (parent.length == 0 || null == child)
			return parent;

		List<String> list = Arrays.asList(parent);
		list.addAll(Stream.of(child).filter(c -> !list.contains(c)).collect(Collectors.toList()));
		return list.toArray(new String[list.size()]);
	}

	public static String getOneBankId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return null == auth ? "schedule" : auth.getName();
	}

	@SuppressWarnings("unchecked")
	public static String getSessionCode() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (null == auth || null == auth.getDetails() || !(auth.getDetails() instanceof Map))
			return null;

		return (String) ((Map<String, Object>) auth.getDetails()).get("sessCode");
	}

	@SuppressWarnings("unchecked")
	public static Long getLoginTimeInMillis() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (null == auth || null == auth.getDetails() || !(auth.getDetails() instanceof Map))
			return null;

		return (Long) ((Map<String, Object>) auth.getDetails()).get("dateTime");
	}

	public static String getAuthToken(final HttpServletRequest request) {
		return RequestHeaderUtils.getAuthToken(request);
	}

	public static String getClientIPAddress(final HttpServletRequest request) {
		return RequestHeaderUtils.getClientIPAddress(request);
	}

	public static String maskToken(final String text) {
		String[] arr = StringUtils.isBlank(text) ? new String[] {} : text.split("\\.");
		return arr.length < 3 ? "*.*.*" : String.format("%s.%s.%s", arr[0], "*", arr[2]);
	}
}
