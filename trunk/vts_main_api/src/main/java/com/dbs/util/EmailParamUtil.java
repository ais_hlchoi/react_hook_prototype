package com.dbs.util;

import com.dbs.vtsp.model.SysEmailDataModel;
import com.dbs.vtsp.model.SysEmailModel;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public enum EmailParamUtil {

	LAST_UPDATEED_DATE("lastUpdatedDate", "lastUpdatedDate", (key,sysEmailModel,paramVal) -> {
		return commonContentAndSubjectReplaceKey(key,sysEmailModel,paramVal) ;
	})

	, RPT_NAME("rptName", "rptName", (key,sysEmailModel,paramVal) -> {
		return commonContentAndSubjectReplaceKey(key,sysEmailModel,paramVal) ;
	})
    , ERR_MSG("errmsg", "errmsg", (key,sysEmailModel,paramVal) -> {
		return commonContentAndSubjectReplaceKey(key,sysEmailModel,paramVal) ;
	})
	, TODAY("today", "today", (key,sysEmailModel,paramVal) -> {
		return commonContentAndSubjectReplaceKey(key,sysEmailModel,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())) ;
	})

    ;


	private String key;
	private String value;
	private EmailParamInterface emailParamInterface;


	private EmailParamUtil(String key, String value, EmailParamInterface emailParamInterface) {
		this.key = key;
		this.value = value;
		this.emailParamInterface = emailParamInterface;
	}


	public static HashMap<EmailParamUtil,String> getDefaultHashMap(){
		HashMap<EmailParamUtil,String> enumStringHashMap = new HashMap<>();
		enumStringHashMap.put(TODAY,null);
		return enumStringHashMap;
	}

	public String key() {
		return this.key;
	}

	public String value() {
		return this.value;
	}


	public static SysEmailModel executeKeyReplacement(SysEmailModel sysEmailModel,HashMap<EmailParamUtil,String> enumStringHashMap){
		for (EmailParamUtil entry : enumStringHashMap.keySet()) {
			//Find related key to replace which hashmap has that value match the enum list function
			Arrays.stream(EmailParamUtil.values()).filter(emailParamEnum->emailParamEnum.key.equals(entry.key())).findFirst().ifPresent(emailParamEnum->{
				emailParamEnum.emailParamInterface.execute(emailParamEnum.key(),sysEmailModel,enumStringHashMap.get(emailParamEnum));
			});

		}
		return sysEmailModel;
	}

//	privae SysEmailModel executeKeyReplacement(SysEmailModel sysEmailModel,String paramVal){
//		return this.emailParamInterface.execute(this.key,sysEmailModel,paramVal);
//	}

	private static String toPattern(String key){
		return "${"+key+"}";
	}



	private static SysEmailModel commonContentAndSubjectReplaceKey(String key,SysEmailModel sysEmailModel,String paramVal){
		if(paramVal!=null) {
			if(sysEmailModel.getSysEmailDataModel()!=null) {
				SysEmailDataModel tmp = sysEmailModel.getSysEmailDataModel();
				String content = tmp.getContent();
				tmp.setContent(content.replace(toPattern(key), paramVal));
				sysEmailModel.setSysEmailDataModel(tmp);
			}
			String subject = sysEmailModel.getSubject();
			sysEmailModel.setSubject(subject.replace(toPattern(key), paramVal));

		}
		return sysEmailModel;
	}






}
