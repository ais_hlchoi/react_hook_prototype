package com.dbs.util;

import com.dbs.vtsp.model.SysEmailModel;

public interface EmailParamInterface {

	public SysEmailModel execute(String key,SysEmailModel sysEmailModel,String paramVal);
}
