package com.dbs.util;

import org.apache.commons.lang3.StringUtils;

public enum EmailRefTable {

	  RPT_SETUP_SCHED
	;

	public static EmailRefTable getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static EmailRefTable getOrElse(String text, EmailRefTable o) {
		if (null == o)
			o = RPT_SETUP_SCHED;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return EmailRefTable.valueOf(text);
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
