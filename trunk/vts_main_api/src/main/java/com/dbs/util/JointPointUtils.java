package com.dbs.util;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.util.Assert;

import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.util.AbstractModuleUtils;

public class JointPointUtils extends AbstractModuleUtils {

	private JointPointUtils() {
		// for SonarQube scanning purpose
	}

	/**
	 * SonarQube not allows try-catch or throws Throwable.
	 * This is a function to centralize all joint point processing action.

	 * @param joinPoint
	 * @return
	 */
	public static Object proceedJoinPoint(ProceedingJoinPoint joinPoint) {
		// for SonarQube scanning purpose
		try {
			return joinPoint.proceed();
		} catch (SysRuntimeException e) {
			throw new SysRuntimeException(e.getExceptionCause(), e.getResponseDesc(), e.getCause()).putAll(e.getParam());
		} catch (Throwable e) {
			throw new SysRuntimeException(e);
		}
	}

	public static Map<String, Object> getContent(ProceedingJoinPoint joinPoint) {
		JointPointProps o = new JointPointProps(joinPoint);
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("args", o.getArgs());
		map.put("service", o.getService());
		return map;
	}

	public static class JointPointProps {
		private ProceedingJoinPoint data;

		public JointPointProps(ProceedingJoinPoint data) {
			Assert.notNull(data, "Data cannot be null");
			this.data = data;
		}
		public ProceedingJoinPoint getData() {
			return data;
		}
		protected void setData(ProceedingJoinPoint data) {
			this.data = data;
		}
		public Signature getSignature() {
			return getData().getSignature();
		}
		public Method getMethod() {
			return ((MethodSignature) getSignature()).getMethod();
		}
		public List<String> getArgs() {
			return Stream.of(getData().getArgs()).map(ClassPathUtils::toJSONString).collect(Collectors.toList());
		}
		public String getService() {
			return String.format("%s.%s", ClassPathUtils.getShortName(getSignature().getDeclaringTypeName()), getMethod().getName());
		}
	}
}
