package com.dbs.util;

public enum EmailConfigEnum {

	EMAIL_CONFIG("EMAIL_CONFIG", "EMAIL_CONFIG")
    ;

	public static String SENDER = "SENDER";
	public static String ENABLED = "ENABLED";
	private String key;
	private String value;

	private EmailConfigEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String key() {
		return this.key;
	}

	public String value() {
		return this.value;
	}




}
