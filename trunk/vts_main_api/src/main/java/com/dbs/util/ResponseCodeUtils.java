package com.dbs.util;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.constant.SystemResponseCode;
import com.dbs.module.export.constant.FileExportResponseCode;
import com.dbs.module.file.constant.FileImportResponseCode;

public class ResponseCodeUtils {

	private ResponseCodeUtils() {
		// for SonarQube scanning purpose
	}

	public static ResponseCode getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ResponseCode getOrElse(String text, ResponseCode o) {
		if (null == o)
			o = getFailureCode();

		if (StringUtils.isBlank(text))
			return o;

		return Stream.of(ResponseCodePrefix.getOrElse(text).getValues()).filter(c -> c.getCode().equals(text)).findFirst().orElse(o);
	}

	public static String getFailure() {
		return getFailureCode().name();
	}

	public static ResponseCode getFailureCode() {
		return SystemResponseCode.E1999;
	}

	public static String getSuccess() {
		return getSuccessCode().name();
	}

	public static ResponseCode getSuccessCode() {
		return SystemResponseCode.M1000;
	}

	public static boolean isSuccess(String text) {
		return isSuccess(text);
	}

	public static boolean isSuccess(ResponseCode code) {
		return getSuccessCode() == code;
	}

	public static boolean isNotSuccess(String text) {
		return !isSuccess(text);
	}

	public static boolean isNotSuccess(ResponseCode code) {
		return !isSuccess(code);
	}

	private enum ResponseCodePrefix {

		  M1(SystemResponseCode.values())
		, E1(SystemResponseCode.values())
		, M2(FileImportResponseCode.values())
		, E2(FileImportResponseCode.values())
		, M3(FileExportResponseCode.values())
		, E3(FileExportResponseCode.values())
		;

		private ResponseCode[] values;

		private ResponseCodePrefix(ResponseCode[] values) {
			this.values = values;
		}

		public ResponseCode[] getValues() {
			return values;
		}

		public static ResponseCodePrefix getOrElse(String text) {
			return getOrElse(text, null);
		}

		public static ResponseCodePrefix getOrElse(String text, ResponseCodePrefix o) {
			if (null == o)
				o = E1;

			if (StringUtils.isBlank(text) || text.trim().length() < 2)
				return o;

			try {
				return ResponseCodePrefix.valueOf(text.trim().substring(0, 2).toUpperCase());
			} catch (IllegalArgumentException e) {
				return o;
			}
		}
	}
}
