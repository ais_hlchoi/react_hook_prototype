package com.dbs.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ExcelUtil {

	private ExcelUtil() {
		// for SonarQube scanning purpose
	}

	public static final String FORMULA = "formula";
	public static final String TEXT    = "text";
	public static final String NUMBER  = "number";

	public static final int FIRST_INDEX = 0; // first index of sheet, row, cell
	public static final int FIRST_LINE  = 1; // first line of sheet
	public static final String FIRST_COLUMN = "A"; // first column of row

	@SuppressWarnings("deprecation")
	public static void setCellValue(XSSFSheet sheet, int colNum, int rowNum, Object value, String dataType, XSSFCellStyle style) {
		// for SonarQube scanning purpose, then avoid that removing useless assignment to local variable "colNum"
		if (null == value || colNum < FIRST_INDEX || rowNum < FIRST_INDEX)
			return;

		XSSFRow row = getRow(sheet, rowNum);
		Cell cell = getCell(row, colNum);

		if (FORMULA.equals(dataType)) {
			cell.setCellType(CellType.FORMULA);
			cell.setCellFormula(String.valueOf(value)); // TODO - Fortify: Formula Injection
		} else if (NUMBER.equals(dataType)) {
			cell.setCellType(CellType.NUMERIC);
			cell.setCellValue(Double.parseDouble(String.valueOf(value)));
		} else {
			cell.setCellType(CellType.STRING);
			cell.setCellValue("null".equals(String.valueOf(value)) ? "" : String.valueOf(value));
		}

		// for performance issues, we do not create style here, create style spend a lot of time
		if (null != style)
			cell.setCellStyle(style);
	}

	public static void setCellValue(XSSFSheet sheet, String colName, int rowNum, Object value, String dataType, XSSFCellStyle style) {
		setCellValue(sheet, ExcelUtil.getColumnIndex(colName), rowNum, value, dataType, style);
	}

	public static void setCellValue(XSSFSheet sheet, String cellInfo, Object value, String dataType, XSSFCellStyle style) {
		Matcher m = Pattern.compile("^(\\w+)(\\d+)$").matcher(cellInfo);
		if (m.find())
			setCellValue(sheet, ExcelUtil.getColumnIndex(m.group(1)), getRowIndex(Integer.parseInt(m.group(2))), value, dataType, style);
	}

	protected static XSSFRow getRow(XSSFSheet sheet, int rowNum) {
		return (XSSFRow) (sheet.getRow(rowNum) == null ? sheet.createRow(rowNum) : sheet.getRow(rowNum));
	}

	protected static Cell getCell(XSSFRow row, String cellIdx) {
		return (Cell) getCell(row, ExcelUtil.getColumnIndex(cellIdx));
	}

	protected static Cell getCell(XSSFRow row, int cellIdx) {
		return (row.getCell(cellIdx) == null ? row.createCell(cellIdx) : row.getCell(cellIdx));
	}

	public static int getLineNumber(final int rowNum) {
		return rowNum + FIRST_LINE;
	}

	public static int getRowIndex(final int line) {
		return line - FIRST_LINE;
	}

	public static int getColumnIndex(final String text) {
		if (StringUtils.isBlank(text))
			return FIRST_INDEX;

		String[] arr = text.trim().toUpperCase().split("|");
		int len = arr.length - 1;
		Double sum = IntStream.rangeClosed(0, len).boxed().mapToDouble(n -> (((short) arr[n].charAt(0) - 64) * Math.pow(26, (len - n.intValue())))).sum();
		return null == sum ? 0 : sum.intValue() - 1;
	}
}
