package com.dbs.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;

public class InternationalizationUtil {

	private InternationalizationUtil() {
		// for SonarQube scanning purpose
	}

	public enum localeCde {
		  ZH_CN // China
		, ZH_HK // Hong Kong
		, EN_US // USA
		;

		public String getLang() {
			String[] arr = name().split("_", 2);
			return arr.length == 1 ? arr[0].toLowerCase() : arr[0].toLowerCase() + "_" + arr[1];
		}
	}

	private static Map<String, ResourceBundle> budleMap = new HashMap<>();

	/**
	 * @param languageEnv
	 * @return
	 * @throws Exception
	 */
	public static ResourceBundle getBundleByCurrentEnv(String languageEnv) {
		Locale locale = null;
		ResourceBundle bundle = null;
		if (StringUtils.isBlank(languageEnv)) {
			locale = Locale.getDefault();

			// Fail to get language file
			if (locale == null) {
				return null;
			}

			languageEnv = locale.toString();
		}

		if (budleMap.containsKey(languageEnv)) {
			bundle = budleMap.get(languageEnv);
		} else {
			try {
				bundle = ResourceBundle.getBundle("internationalization/messages-" + languageEnv);
				budleMap.put(languageEnv, bundle);
			} catch (MissingResourceException e) {
				return null;
			}
		}

		return bundle;
	}

	/**
	 * @param code
	 * @param languageEnv
	 * @return
	 * @throws Exception
	 */
	public static String getMsg(String code, String languageEnv) {
		if (StringUtils.isBlank(code)) {
			return code;
		}

		ResourceBundle bundle = getBundleByCurrentEnv(languageEnv);
		ResourceBundle bundleDefault = getBundleByCurrentEnv(localeCde.ZH_HK.getLang());

		if (bundleDefault == null) {
			return code;
		}

		if (bundle == null) {
			bundle = bundleDefault;
		}

		List<String> resuleMsg = new ArrayList<>();
		String[] codes = code.split(";");
		for (String key : codes) {
			if (StringUtils.isNotBlank(key)) {
				try {
					resuleMsg.add(bundle.getString(key));
				} catch (MissingResourceException e) {
					try {
						resuleMsg.add(bundleDefault.getString(key));
					} catch (MissingResourceException ex) {
						resuleMsg.add(key);
					}
				}
			}
		}

		return CollectionUtils.isEmpty(resuleMsg) ? "" : StringUtils.join(resuleMsg, ";");
	}

	/**
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static String getMsg(String code) {
		return getMsg(code, null);
	}

	/**
	 * @param code
	 * @param languageEnv
	 * @return
	 * @throws Exception
	 */
	public static String getMsgTwo(String code, String languageEnv) {
		if (StringUtils.isBlank(code)) {
			return code;
		}

		ResourceBundle bundle = getBundleByCurrentEnv(languageEnv);
		ResourceBundle bundleDefault = getBundleByCurrentEnv(localeCde.ZH_CN.getLang());

		if (bundleDefault == null) {
			return code;
		}

		if (bundle == null) {
			bundle = bundleDefault;
		}

		List<String> resuleMsg = new ArrayList<>();
		String[] codes = code.split(";");
		for (String key : codes) {
			if (StringUtils.isNotBlank(key)) {
				try {
					resuleMsg.add(bundle.getString(key));
				} catch (MissingResourceException e) {
					try {
						resuleMsg.add(bundleDefault.getString(key));
					} catch (MissingResourceException ex) {
						resuleMsg.add(key);
					}
				}
			}
		}

		return CollectionUtils.isEmpty(resuleMsg) ? "" : StringUtils.join(resuleMsg, ";");
	}

	/**
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static String getMsgTwo(String code) {
		return getMsgTwo(code, null);
	}

	/**
	 * @param map
	 * @param code
	 * @return
	 */
	public static String getMsg(Map<String, String> map, String code) {
		String msg = getMsg(code, null);
		if (MapUtils.isEmpty(map) || StringUtils.isBlank(msg))
			return msg;

		for (Entry<String, String> entry : map.entrySet()) {
			msg = msg.replaceAll("{" + entry.getKey() + "}", entry.getValue());
		}
		return msg;
	}

	/**
	 * @param code
	 * @param result
	 * @return
	 */
	public static String getMsg(String code, int result) {
		return getMsg(code, 1, result);
	}

	/**
	 * @param code
	 * @param size
	 * @param result
	 * @return
	 */
	public static String getMsg(String code, int size, int result) {
		Map<String, String> map = new HashMap<>();
		map.put("size", String.valueOf(size));
		map.put("result", String.valueOf(result));
		return getMsg(map, code);
	}
}