package com.dbs.util;

public enum EmailAlertEnum {

	EMAIL_ALERT_DONE_SCHED_RPT("EMAIL_ALERT_DONE_SCHED_RPT", "EMAIL_ALERT_DONE_SCHED_RPT")
	, EMAIL_ALERT_FAIL_SCHED_RPT("EMAIL_ALERT_FAIL_SCHED_RPT", "EMAIL_ALERT_FAIL_SCHED_RPT")
    , EMAIL_ALERT_FAIL_FILE_UPLD("EMAIL_ALERT_FAIL_FILE_UPLD", "EMAIL_ALERT_FAIL_FILE_UPLD")

    ;

	public static String SUBJECT = "SUBJECT";
	public static String CONTENT = "CONTENT";
	public static String RECIPIENT = "RECIPIENT";
	private String key;
	private String value;

	private EmailAlertEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String key() {
		return this.key;
	}

	public String value() {
		return this.value;
	}




}
