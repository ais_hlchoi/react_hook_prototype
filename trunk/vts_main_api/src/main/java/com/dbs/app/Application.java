package com.dbs.app;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.dbs.executor.TimeOutThreadPoolExecutor;
import com.dbs.module.session.util.SessionCodeUtils;

@EnableScheduling
@EnableTransactionManagement
@EnableAutoConfiguration 
@ComponentScan(basePackages = {"com.dbs"})//packages spring will map scan for DI
@SpringBootApplication
@Configuration
@EnableAsync
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static final String UID = SessionCodeUtils.getFactory().create(6);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		logger.info("ヾ(◍°∇°◍)ﾉﾞ    bootdo startup     ヾ(◍°∇°◍)ﾉﾞ\n" +
                " ______                    _   ______            \n" +
                "|_   _ \\                  / |_|_   _ `.          \n" +
                "  | |_) |   .--.    .--. `| |-' | | `. \\  .--.   \n" +
                "  |  __'. / .'`\\ \\/ .'`\\ \\| |   | |  | |/ .'`\\ \\ \n" +
                " _| |__) || \\__. || \\__. || |, _| |_.' /| \\__. | \n" +
                "|_______/  '.__.'  '.__.' \\__/|______.'  '.__.'  ");
	}

	@Value("${threadCorePoolSize}")
	private int corePoolSize;
	
	@Value("${threadMaxPoolSize}")
	private int maxPoolSize;
	
	@Value("${threadQueueCapacity}")
	private int queueCapacity;
	
	@Value("${threadTimeout}")
	private int threadTimeout;
	
	/*
	 * @Bean public Executor taskExecutor() { ThreadPoolTaskExecutor executor = new
	 * ThreadPoolTaskExecutor(); executor.setCorePoolSize(corePoolSize);
	 * executor.setMaxPoolSize(maxPoolSize);
	 * executor.setQueueCapacity(queueCapacity);
	 * executor.setThreadNamePrefix("DataExportController-");
	 * executor.setWaitForTasksToCompleteOnShutdown(true); executor.initialize();
	 * return executor; }
	 */	
	
	
	@Bean("taskExecutor")
    public TaskExecutor taskExecutor() {
        final TimeOutThreadPoolExecutor executor = new TimeOutThreadPoolExecutor();
        executor.setCorePoolSize(corePoolSize);
	    executor.setMaxPoolSize(maxPoolSize);
	    executor.setQueueCapacity(queueCapacity);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setThreadNamePrefix("DataExportController-");
        executor.setAllowCoreThreadTimeOut(true);
        executor.setDaemon(true);
		executor.setTimeOut(threadTimeout);
		executor.setTimeUnit(TimeUnit.SECONDS);
        executor.init();
        return executor;
    }
	
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (throwable, method, params) -> logger.error(String.format("Async uncaught exception, method: %s params: %s", method, Arrays.asList(params)), throwable);
    }
}
