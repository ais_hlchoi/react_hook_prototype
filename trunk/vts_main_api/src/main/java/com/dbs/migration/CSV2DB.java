package com.dbs.migration;


import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;

public class CSV2DB {
	private final Logger logger = LoggerFactory.getLogger(CSV2DB.class);
	
	final List<String> PARTITION_COLUMN_LIST = Arrays.asList("LAST_MOD_TME","SYS_DTE","SYS_TME","TXN_DTE","TRADE_DTE","EVENT_DTE","PROC_DTE",
			"TRADE_LIMIT_EFF_TO_DTE", "FNA_EXPRY_DTE", "PRINT_TME", "MSG_TME",
			"TXN_ACCT_NBR");
	final int cntToInsert = 500;
	
	final Codec ORACLE_CODEC = new OracleCodec();
	
	public CSV2DB() {
		
	}

	public void migrateFile2Table(Connection conn1, CSVReader reader, String tableName) {
		PreparedStatement stmt1 = null;
		try {
			if(reader==null)
				return;
			String[] line;
			int lineCnt = 1;
			StringBuilder sb = new StringBuilder();
			StringBuilder columns = new StringBuilder();
			int partitionCol = -1;
			while ((line = reader.readNext()) != null) {
				StringBuilder values = new StringBuilder();
				if (lineCnt == 1) {
					int colIdx = 0;
					for(String col:line) {
						columns.append(",").append(ESAPI.encoder().encodeForSQL(ORACLE_CODEC, col));
						//find first match column to use as partition column
						if(PARTITION_COLUMN_LIST.contains(col.toUpperCase()) && partitionCol == -1)
							partitionCol = colIdx;
						colIdx++;
					}
				}
				else {
					int colIdx = 0;
					String partitionTxt = "";
					for(String data:line) {
						String value = data;
						values.append(",'").append(ESAPI.encoder().encodeForSQL(ORACLE_CODEC, value)).append("'");						
						if(partitionCol == colIdx)
							partitionTxt = StringUtils.isNotEmpty(value)?(value.indexOf("-") > -1 ? value.substring(3,9).replaceAll("\\-", "") : value.substring(0,2)):"EMPTY";
						
						colIdx++;
					}	
					if(StringUtils.isEmpty(partitionTxt))
						throw new Exception("Partition column not found.");	
					sb.append(addInsert(conn1, values, tableName, (lineCnt-1), partitionTxt));
				}
				if(partitionCol == -1)
					throw new Exception("Partition column not found.");
	        	logger.info("Read line: " + lineCnt);		
	        	if(lineCnt==-1)
		        	logger.info("debug here.");	        		
				lineCnt++;
				if(lineCnt%cntToInsert==0) {
					insertRcd(conn1, tableName, columns, sb, (lineCnt-1));
					sb = new StringBuilder();
				}
			}
			if(sb.toString().length()>0)
				insertRcd(conn1, tableName, columns, sb, (lineCnt-1));
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if(reader!=null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
			if(stmt1!=null) {
				try {
					stmt1.close();
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
		}
	}
	
	private String addInsert(Connection conn, StringBuilder values, String tableName, int rcdCnt, String partitionTxt) throws SQLException {
		String insertSql = ",((select a.id from rpt_file_upld a,rpt_src_file b where a.file_name like '%s' and a.src_file_id = b.id and b.file_name_start = '%s'), NOW(), 'Y', '%s', %s%s)";
		return String.format(insertSql,"migrate%",ESAPI.encoder().encodeForSQL(ORACLE_CODEC, tableName),ESAPI.encoder().encodeForSQL(ORACLE_CODEC, partitionTxt),ESAPI.encoder().encodeForSQL(ORACLE_CODEC, String.valueOf(rcdCnt)),values.toString());		
		//return ",((select a.id from rpt_file_upld a,rpt_src_file b where a.file_name like 'migrate%' and a.src_file_id = b.id and b.file_name_start = '"+tableName+"'), NOW(), 'Y', '"+partitionTxt+"', " + rcdCnt + values.toString() + ")";
	}
	
	private void insertRcd(Connection conn, String tableName, StringBuilder columns,
			StringBuilder values, int rcdCnt) throws SQLException {
		//String sql = "INSERT INTO " + tableName + "(FILE_UPLD_ID, CREATED_DATE, LATEST_VERSION_IND, PARTITION_IND, LINE_SEQ" + columns.toString() + ") VALUES " + values.toString().substring(1) ;
		String sql = String.format("INSERT INTO %s(FILE_UPLD_ID, CREATED_DATE, LATEST_VERSION_IND, PARTITION_IND, LINE_SEQ%s) VALUES %s" ,ESAPI.encoder().encodeForSQL(ORACLE_CODEC, tableName),columns.toString(),values.toString().substring(1));
		logger.info("Moving " + tableName +" record up to...: " + rcdCnt + "\t" + sql);
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sql);
			stmt.executeUpdate();
			conn.commit();
		}
		catch(Exception e) {
			logger.error(e.getMessage());
		}
		finally {
			if(stmt!=null)
				stmt.close();
		}
	}
}