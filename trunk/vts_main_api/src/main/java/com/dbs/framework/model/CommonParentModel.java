package com.dbs.framework.model;

import java.io.Serializable;
import java.sql.Timestamp;

import com.dbs.framework.constant.RecordStatus;
import com.dbs.vtsp.model.AbstractAuditModel;
import com.dbs.util.ClassPathUtils;

/**
 * @description: Common Parent Model
 */
public class CommonParentModel extends AbstractAuditModel implements Serializable {

	private static final long serialVersionUID = -2008509668784883326L;

	// for react table component
	protected int key;

	protected Integer page = 1;
	protected Integer pageSize = 10;
	protected Integer startRow;
	protected Integer totalPage;

	protected String userId;
	protected Integer menuFuncId;
	protected String operationContent;

	protected String defaultSearchText;

	protected String statInd;
	protected String amendInd;
	protected String lastInitId;
	protected Timestamp lastInitDate;
	protected String lastPostId;
	protected Timestamp lastPostDate;

	public void calculationStartRow() {
		if (page != null && page > 0 && pageSize != null && pageSize > 0) {
			startRow = (page - 1) * pageSize;
		}
	}

	public void calculationStartRow(boolean isDefaultData) {
		if (isDefaultData) {
			if (page == null || page < 1) {
				page = 1;
			}
			if (pageSize == null || pageSize < 1) {
				pageSize = 6;
			}
		}
		calculationStartRow();
	}

	public void pageCountHandler(Integer totalNumber) {
		if (pageSize > 0 && totalNumber > 0) {
			float totalFloat = (float) totalNumber / pageSize;
			totalPage = (int) totalFloat;
			if (totalFloat - totalPage > 0) {
				totalPage++;
			}
		} else {
			totalPage = 0;
		}
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getMenuFuncId() {
		return menuFuncId;
	}

	public void setMenuFuncId(Integer menuFuncId) {
		this.menuFuncId = menuFuncId;
	}

	public String getOperationContent() {
		return operationContent;
	}

	public void setOperationContent(String operationContent) {
		this.operationContent = operationContent;
	}

	public String getDefaultSearchText() {
		return defaultSearchText;
	}

	public void setDefaultSearchText(String defaultSearchText) {
		this.defaultSearchText = defaultSearchText;
	}

	public String getStatInd() {
		return statInd;
	}

	public void setStatInd(String statInd) {
		this.statInd = statInd;
	}

	public String getAmendInd() {
		return amendInd;
	}

	public void setAmendInd(String amendInd) {
		this.amendInd = amendInd;
	}

	public String getLastInitId() {
		return lastInitId;
	}

	public void setLastInitId(String lastInitId) {
		this.lastInitId = lastInitId;
	}

	public Timestamp getLastInitDate() {
		return lastInitDate;
	}

	public void setLastInitDate(Timestamp lastInitDate) {
		this.lastInitDate = lastInitDate;
	}

	public String getLastPostId() {
		return lastPostId;
	}

	public void setLastPostId(String lastPostId) {
		this.lastPostId = lastPostId;
	}

	public Timestamp getLastPostDate() {
		return lastPostDate;
	}

	public void setLastPostDate(Timestamp lastPostDate) {
		this.lastPostDate = lastPostDate;
	}

	public String getMakChkStatus() {
		return RecordStatus.getOrElse(ClassPathUtils.getOrElse(getStatInd(), "") + ClassPathUtils.getOrElse(getAmendInd(), "")).name();
	}
}