package com.dbs.framework.constant;

public enum OperationLogAction {

	/*
	 * user login
	 */
	  LOGIN
	/*
	 * user logout
	 */
	, LOGOUT
	/*
	 * insert new data into database
	 */
	, ADD
	/*
	 * update existing data in database
	 */
	, MODIFY
	/*
	 * delete existing data from database
	 */
	, DELETE
	/*
	 * insert new data to 'Checker' tables and update status in existing 'Maker' tables
	 */
	, AUTHORIZE
	/*
	 * read data from files and insert data into database
	 * e.g. csv from s3 -> mariadb
	 */
	, FILE_UPLOAD
	/*
	 * get data from database and generate files
	 * e.g. query data and output to files then save in s3
	 */
	, DOWNLOAD
	/*
	 * send email
	 */
	, SEND_EMAIL
	/*
	 * create email
	 */
	, CREATE_EMAIL
	;
}
