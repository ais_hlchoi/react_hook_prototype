package com.dbs.framework.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface LocalHostConfig {

	Action value() default Action.ALWAYS;

	String[] ignore() default {};

	public enum Action {
		IGNORE,
		ALWAYS,
	}

	public enum Attribute {
		HOST_NAME("hostName"),
		HOST_ADDRESS("hostAddress"),
		;
		public final String key;
		private Attribute(String key) {
			this.key = key;
		}
	}
}
