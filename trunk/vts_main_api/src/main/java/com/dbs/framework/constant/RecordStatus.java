package com.dbs.framework.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum RecordStatus {

	  INIT("IZ")
	, AUTH("AZ")
	, AMEND("AA")
	, DELETE("AD")
	, NA
	;

	private String code;

	private RecordStatus(String code) {
		this.code = code;
	}

	private RecordStatus() {
		this.code = name();
	}

	public String getCode() {
		return code;
	}

	public boolean equalsStatus(String text) {
		return StringUtils.isNotBlank(text) && name().equalsIgnoreCase(text);
	}

	public static RecordStatus getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static RecordStatus getOrElse(String text, RecordStatus o) {
		if (null == o)
			o = NA;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return RecordStatus.valueOf(text);
		} catch (IllegalArgumentException e) {
			return Stream.of(RecordStatus.values()).filter(c -> c.getCode().equalsIgnoreCase(text)).findFirst().orElse(o);
		}
	}
}
