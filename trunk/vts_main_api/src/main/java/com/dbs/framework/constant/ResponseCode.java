package com.dbs.framework.constant;

public interface ResponseCode {

	String name();

	String getCode();

	String getDesc();

	Integer getStatus();
}
