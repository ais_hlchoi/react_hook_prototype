package com.dbs.framework.core;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import com.dbs.framework.constant.SystemResponseCode;
import com.dbs.framework.constant.RestEntity;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.framework.model.CommonExceptionHandling;

/**
 * Global exception handler
 */
public class FrameworkExceptionHandler implements HandlerExceptionResolver {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {
		Map<String, Object> attributes = new HashMap<>();

		if (e instanceof SysRuntimeException) {
			SysRuntimeException t = (SysRuntimeException) e;
			putAttributes(attributes, t.getResponseCode(), t.getResponseDesc(), t.getStatus(), t.getObject());
		} else {
			putAttributes(attributes, SystemResponseCode.E1003.getCode(), CommonExceptionHandling.formatError(e.getMessage()), SystemResponseCode.E1003.getStatus(), null);
			logger.error(String.valueOf(attributes.get(RestEntity.RESPONSE_DESC.key)));
		}

		FastJsonJsonView view = new FastJsonJsonView();
		view.setAttributesMap(attributes);

		ModelAndView mv = new ModelAndView();
		mv.setView(view);
		return mv;
	}

	protected void putAttributes(Map<String, Object> attributes, String responseCode, String responseDesc, Integer status, Object object) {
		if (null == attributes)
			return;

		attributes.put(RestEntity.RESPONSE_CODE.key, responseCode);
		attributes.put(RestEntity.RESPONSE_DESC.key, responseDesc);
		attributes.put(RestEntity.STATUS.key, status);
		if (null != object) {
			attributes.put(RestEntity.OBJECT.key, object);
		}
	}

	protected void putAttributes(Map<String, Object> attributes, SystemResponseCode responseCode, Object object) {
		if (null == responseCode)
			return;

		putAttributes(attributes, responseCode.getCode(), responseCode.getDesc(), responseCode.getStatus(), object);
	}
}