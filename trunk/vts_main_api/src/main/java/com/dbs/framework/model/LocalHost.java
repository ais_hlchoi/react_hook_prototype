package com.dbs.framework.model;

public interface LocalHost {

	String getHostName();

	void setHostName(String hostName);

	String getHostAddress();

	void setHostAddress(String hostAddress);

	String getServiceCode();

	void setServiceCode(String serviceCode);

	String getPid();

	String getName();
}
