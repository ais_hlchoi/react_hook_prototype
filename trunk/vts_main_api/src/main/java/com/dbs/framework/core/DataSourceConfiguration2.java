package com.dbs.framework.core;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.dbs.framework.security.BossPlaceHolderConfigurerHelper;
import com.dbs.framework.security.CipherUtil;
import com.dbs.util.FortifyStrategyUtils;
import com.github.pagehelper.PageInterceptor;

@Configuration
@MapperScan(basePackages = "com.dbs.readonly.dao", sqlSessionTemplateRef  = "sqlSessionTemplateObj2")
public class DataSourceConfiguration2 {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${datasource.jdbcUsername2}")
	private String jdbcUsername;

	@Value("${datasource.jdbcSecret2}")
	private String jdbcSecret;

	@SuppressWarnings("unchecked")
	protected <T> T createDataSource(DataSourceProperties properties, Class<? extends DataSource> type) {
		String secret = null;
		if (BossPlaceHolderConfigurerHelper.isEncryptedValue(jdbcSecret)) {
			secret = CipherUtil.decrypt(BossPlaceHolderConfigurerHelper.getInnerValue(jdbcSecret));
		} else {
			secret = jdbcSecret;
		}
		properties.setUsername(jdbcUsername);
		properties.setPassword(secret);
		return (T) properties.initializeDataSourceBuilder().type(type).build();
	}

	/**
	 * @see org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration.Tomcat
	 * @param properties content of application.properties
	 * @return DruidDataSource
	 */
	@Bean(name = "dataSourceObj2")
	@ConfigurationProperties("spring.datasource2")
	public DruidDataSource dataSource(DataSourceProperties properties) {

		DruidDataSource dataSource = createDataSource(properties, DruidDataSource.class);

		DatabaseDriver databaseDriver = DatabaseDriver.fromJdbcUrl(properties.determineUrl());

		String validationQuery = databaseDriver.getValidationQuery();
		if (validationQuery != null) {
			dataSource.setTestOnBorrow(true);
			dataSource.setValidationQuery(validationQuery);
		}

		return dataSource;
	}

	@Value("${mybatis.mapper-locations2}")
	String mapperLocations;

	@Bean(name = "sqlSessionFactoryObj2")
	public SqlSessionFactory sqlSessionFactory(@Qualifier("dataSourceObj2") DataSource dataSource) throws IOException {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();

		bean.setDataSource(dataSource);
		bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));

		// set PageInterceptor as plugins for SqlSessionFactory
		Interceptor[] plugins = new Interceptor[1];
		plugins[0] = new PageInterceptor();

		Properties prop = new Properties();
		prop.setProperty("reasonable", "true");
		plugins[0].setProperties(prop);

		bean.setPlugins(plugins);

		try {
			return bean.getObject();
		} catch (Exception e) {
			logger.error("Fail to connect sql session caused by " + FortifyStrategyUtils.toErrString(e));
		}
		return null;
	}

	@Bean(name = "transactionManagerObj2")
	public DataSourceTransactionManager transactionManager(@Qualifier("dataSourceObj2") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "sqlSessionTemplateObj2")
	public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryObj2") SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
