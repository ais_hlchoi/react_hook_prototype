package com.dbs.framework.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocalHostModel implements LocalHost {

	@JsonIgnore
	private String hostName;

	@JsonIgnore
	private String hostAddress;

	@JsonInclude
	private String serviceCode;

	@JsonInclude
	private String pid;

	@JsonIgnore
	public String getName() {
		List<String> list = new ArrayList<String>();
		list.add(getHostName());
		list.add(getServiceCode());
		list.add(getPid());
		return list.stream().filter(StringUtils::isNotBlank).collect(Collectors.joining("."));
	}

	public static LocalHost of(String pid) {
		LocalHostModel o = new LocalHostModel();
		o.setPid(pid);
		return o;
	}
}
