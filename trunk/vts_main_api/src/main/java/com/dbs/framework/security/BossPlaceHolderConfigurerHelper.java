package com.dbs.framework.security;

/**
 * 
 * @author Ben Cheng
 *
 */
public class BossPlaceHolderConfigurerHelper {

	private static final String ENCRYPTED_VALUE_PREFIX = "ENC(";
	private static final String ENCRYPTED_VALUE_SUFFIX = ")";

	private BossPlaceHolderConfigurerHelper() {
		// for SonarQube scanning purpose
	}

	public static String getInnerValue(String value) {
		return getInnerEncyptedValue(value);
	}

	public static boolean isEncryptedValue(String value) {
		return value.startsWith(ENCRYPTED_VALUE_PREFIX) && value.endsWith(ENCRYPTED_VALUE_SUFFIX);
	}

	public static String getInnerEncyptedValue(String value) {
		return value.substring(ENCRYPTED_VALUE_PREFIX.length(), value.length() - ENCRYPTED_VALUE_SUFFIX.length());
	}
}
