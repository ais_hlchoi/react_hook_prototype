package com.dbs.framework.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;

import javax.crypto.SecretKey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dbs.framework.core.ApplicationConfig;
import com.dbs.util.FortifyStrategyUtils;

public class KeyStoreUtil {

	private KeyStoreUtil() {
		// for SonarQube scanning purpose
	}

	private Logger logger = LogManager.getLogger(this.getClass());

	public static SecretKey getSecretKey() {
		if (ApplicationConfig.getKeystorePath().equals("classpath"))
			return new KeyStoreUtil().getSecretKeyByClassPath();

		return new KeyStoreUtil().getSecretKeyByApplicationConfig();
	}

	private SecretKey getSecretKeyByClassPath() {
		logger.info("DECRYPTION - loading keystore:: " + ApplicationConfig.getKeystoreName() + " from classpath");
		try (
				InputStream storeInputStream = KeyStoreUtil.class.getClassLoader().getResourceAsStream(ApplicationConfig.getKeystoreName());
			) {
			KeyStore keystore = KeyStore.getInstance(ApplicationConfig.getKeystoreType());
			keystore.load(storeInputStream, ApplicationConfig.getKeystoreSecret().toCharArray());
			Key key = keystore.getKey(ApplicationConfig.getKeystoreAlias(), ApplicationConfig.getKeystoreAliasSecret().toCharArray());
			if (key instanceof SecretKey) {
				logger.debug("DECRYPTION - returning Secret Key");
				return (SecretKey) key;
			}
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}

		logger.debug("DECRYPTION - Secret Key not found");
		return null;
	}

	private SecretKey getSecretKeyByApplicationConfig() {
		File keystoreFile = new File(FortifyStrategyUtils.toPathString(ApplicationConfig.getKeystorePath()), FortifyStrategyUtils.toPathString(ApplicationConfig.getKeystoreName()));
		logger.info("DECRYPTION - loading keystore:: " + ApplicationConfig.getKeystoreName() + " from " + keystoreFile.getAbsolutePath());
		try (
				InputStream storeInputStream = new FileInputStream(keystoreFile);
			) {
			KeyStore keystore = KeyStore.getInstance(ApplicationConfig.getKeystoreType());
			keystore.load(storeInputStream, ApplicationConfig.getKeystoreSecret().toCharArray());
			Key key = keystore.getKey(ApplicationConfig.getKeystoreAlias(), ApplicationConfig.getKeystoreAliasSecret().toCharArray());
			if (key instanceof SecretKey) {
				logger.debug("DECRYPTION - returning Secret Key");
				return (SecretKey) key;
			}
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}

		logger.debug("DECRYPTION - Secret Key not found");
		return null;
	}
}
