package com.dbs.framework.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CurrentUser {

	Action value() default Action.ALWAYS;

	String[] ignore() default {};

	public enum Action {
		IGNORE,
		ALWAYS,
	}

	public enum Attribute {
		ONE_BANK_ID("oneBankId"),
		ONE_BANK_ROLE_LIST("oneBankRoleList"),
		CREATED_BY("createdBy"),
		LAST_UPDATED_BY("lastUpdatedBy"),
		;
		public final String key;
		private Attribute(String key) {
			this.key = key;
		}
	}
}
