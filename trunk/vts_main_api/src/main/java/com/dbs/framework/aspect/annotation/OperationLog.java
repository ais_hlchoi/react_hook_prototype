package com.dbs.framework.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.dbs.framework.constant.OperationLogAction;

import java.lang.annotation.RetentionPolicy;

@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface OperationLog {

	OperationLogAction operation();

	boolean logDescReturn() default false;

	boolean logErrorOnly() default false;

	boolean whenErrorSendEmail() default true;
}
