package com.dbs.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.file.FileImport;
import com.dbs.util.EmailAlertEnum;
import com.dbs.util.EmailParamUtil;
import com.dbs.vtsp.model.RptSrcFileModel;
import com.dbs.vtsp.model.SysEmailAttachModel;
import com.dbs.vtsp.model.SysEmailDataModel;
import com.dbs.vtsp.model.SysEmailModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import com.dbs.vtsp.service.FileImportService;
import com.dbs.vtsp.service.SysEmailProcessService;
import com.dbs.vtsp.service.SysEmailRequestService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ScheduleUnitTest {

	@Value("${test.demo.schedule}")
	private String fileName;

	@Autowired
	private FileImportService fileImportService;

	@Autowired
	private FileImport fileImport;

	@Autowired
	private SysEmailRequestService sysEmailRequestService;

	@Autowired
	private SysEmailProcessService sysEmailProcessService;

	@Test
	public void test() {
		processTest(1, "ALLOCATIONS");
		moveToImport("ALLOCATIONS-202107051230_05072021_1619.csv");
	}

	public void processTest(Integer id, String fileStart) {
		RptSrcFileModel o = new RptSrcFileModel();
		o.setId(id);
		o.setFileNameStart(fileStart);
		fileImportService.upload(o);
	}

	public void moveToImport(String fileName) {
		try {
			fileImport.move(fileImport.getProps().getLoadFailPath() + fileName, fileImport.getProps().getLoadImportPath(fileName));
		} catch (Exception e) {

		}
		try {
			fileImport.move(fileImport.getProps().getLoadSuccessPath() + fileName, fileImport.getProps().getLoadImportPath(fileName));
		} catch (Exception e) {

		}
		try {
			fileImport.move(fileImport.getProps().getLoadProcessPath() + fileName, fileImport.getProps().getLoadImportPath(fileName));
		} catch (Exception e) {

		}
	}

	@Test
	public void emailError() {
		HashMap<EmailParamUtil,String> enumStringHashMap = EmailParamUtil.getDefaultHashMap();
		enumStringHashMap.put(EmailParamUtil.ERR_MSG,"Unit Test Email test warning email error message");
		enumStringHashMap.put(EmailParamUtil.RPT_NAME,"Unit Test Email test warning email rpt Name");
		enumStringHashMap.put(EmailParamUtil.LAST_UPDATEED_DATE,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

		sysEmailRequestService.requestEmailAlert(enumStringHashMap, EmailAlertEnum.EMAIL_ALERT_FAIL_SCHED_RPT);
		sysEmailProcessService.process();
	}

	@Test
	public void emailSuccess() {
		SysEmailTmplModel tmpl = new SysEmailTmplModel();
		tmpl.setRecipient("paulpochichan@dbs.com");
		tmpl.setRecipientCc("paulpochichan@dbs.com");
		tmpl.setRecipientBcc("paulpochichan@dbs.com");
		tmpl.setSubject("Unit Test Email success Rptname: ${rptName} ${today} ${lastUpdatedDate}");
		tmpl.setContent("Unit Test Email success  \n rptName: ${rptName} \n today: ${today} \n lastUpdatedDate: ${lastUpdatedDate}");
		List<String> attachmentPaths = new ArrayList<>();
		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");
//		attachmentPaths.add("${folder.output}/custom_report_csv_20210809161900_277.csv");
		String rptName = "Unit Test Email";

		// SysEmailModel builder
		SysEmailModel sysEmailModel = new SysEmailModel();
		sysEmailModel.setType(EmailAlertEnum.EMAIL_ALERT_DONE_SCHED_RPT.value());
		sysEmailModel.setRecipient(tmpl.getRecipient());
		sysEmailModel.setRecipientCc(tmpl.getRecipientCc());
		sysEmailModel.setRecipientBcc(tmpl.getRecipientBcc());
		sysEmailModel.setSubject(tmpl.getSubject());
		SysEmailDataModel sysEmailDataModel = new SysEmailDataModel();
		sysEmailDataModel.setContent(tmpl.getContent());
		sysEmailModel.setSysEmailDataModel(sysEmailDataModel);
		sysEmailModel.setSendDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		sysEmailModel.setRemark(null);
		List<SysEmailAttachModel> sysEmailAttachModels = attachmentPaths.stream().map(path -> {
			SysEmailAttachModel s = new SysEmailAttachModel();
			s.setFilePath(path);
			return s;
		}).collect(Collectors.toList());
		sysEmailModel.setSysEmailAttachModelList(sysEmailAttachModels);

		// map builder
		HashMap<EmailParamUtil, String> map = EmailParamUtil.getDefaultHashMap();
		map.put(EmailParamUtil.RPT_NAME, rptName);
		map.put(EmailParamUtil.ERR_MSG, null);
		map.put(EmailParamUtil.LAST_UPDATEED_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

		sysEmailRequestService.requestEmail(map, sysEmailModel);
		sysEmailProcessService.process();
	}
}
