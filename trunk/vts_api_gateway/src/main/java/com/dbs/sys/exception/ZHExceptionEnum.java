package com.dbs.sys.exception;

import com.dbs.util.InternationalizationUtil;

/**
 * @author GX
 * @date 2019年6月3日 上午11:30:03
 * @description:错误码枚举，规范错误集合
 */
public enum ZHExceptionEnum {

	/** 数据库插入异常 */
	INSERT("SQL_ERR_INSERT"),
	/** 数据库删除异常 */
	DELETE("SQL_ERR_DELETE"),
	/** 数据库修改异常 */
	UPDATE("SQL_ERR_UPDATE"),
	/** 系统异常 */
	SYSTEM_ERROR("SYS_ERR");

	private String errorCode;
	private String errorMsg;
	ZHExceptionEnum(String errorCode) {
		this.errorCode = errorCode;
		this.errorMsg = InternationalizationUtil.getMsg(errorCode);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}