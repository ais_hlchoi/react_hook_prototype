package com.dbs.sys.exception;

/**
 * @author GX
 * @description:Custom Execption
 */
public class ZHException extends RuntimeException {

	private static final long serialVersionUID = 1411474328346767893L;

	public ZHException() {
		super();
	}

	public ZHException(String msg) {
		super(msg);
	}

	public ZHException(Throwable t) {
		super(t);
	}

	public ZHException(String msg, Throwable t) {
		super(msg, t);
	}

	public ZHException(ZHExceptionEnum zhExceptionEnum, Throwable t) {
		super(zhExceptionEnum.getErrorMsg(), t);
	}

	public ZHException(ZHExceptionEnum zhExceptionEnum) {
		super(zhExceptionEnum.getErrorMsg());
	}
}
