package com.dbs.framework.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: Common Parent Model
 */
public class CommonParentModel implements Serializable, Cloneable {

	private static final long serialVersionUID = -2008509668784883326L;

	//for react table component
	protected int key;
	protected String createdBy;
	protected Date createdDate;
	protected String lastUpdatedBy;
	protected Date lastUpdatedDate;

	protected Integer page;

	protected Integer pageSize;

	protected Integer startRow;

	protected Integer totalPage;

	protected Integer userId;

	protected Integer menuFuncId;

	protected String operationContent;

	protected String defaultSearchText;

	/**
	 *  
	 * @author GX
	 */
	public void calculationStartRow() {
		if (page != null && page > 0 && pageSize != null && pageSize > 0) {
			startRow = (page - 1) * pageSize;
		}
	}

	/**
	 * 
	 * @param isDefaultData
	 * @author GX
	 */
	public void calculationStartRow(boolean isDefaultData) {
		if (isDefaultData) {
			if (page == null || page < 1) {
				page = 1;
			}
			if (pageSize == null || pageSize < 1) {
				pageSize = 6;
			}
		}
		calculationStartRow();
	}

	/**
	 * 
	 * @param totalNumber
	 * @author GX
	 */
	public void pageCountHandler(Integer totalNumber) {
		if (pageSize > 0 && totalNumber > 0) {
			float totalFloat = (float) totalNumber / pageSize;
			totalPage = (int) totalFloat;
			if (totalFloat - totalPage > 0) {
				totalPage++;
			}
		} else {
			totalPage = 0;
		}
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getMenuFuncId() {
		return menuFuncId;
	}

	public void setMenuFuncId(Integer menuFuncId) {
		this.menuFuncId = menuFuncId;
	}

	public String getOperationContent() {
		return operationContent;
	}

	public void setOperationContent(String operationContent) {
		this.operationContent = operationContent;
	}

	public String getDefaultSearchText() {
		return defaultSearchText;
	}

	public void setDefaultSearchText(String defaultSearchText) {
		this.defaultSearchText = defaultSearchText;
	}

}