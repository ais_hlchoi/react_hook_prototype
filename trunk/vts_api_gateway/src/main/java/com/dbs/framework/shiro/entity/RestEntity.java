/**
 * 
 */
package com.dbs.framework.shiro.entity;

import java.util.Map;

import com.alibaba.fastjson.JSON;

/*import org.apache.commons.lang.StringUtils;*/

/*import com.dbs.pd.exception.PdException;
import com.dbs.pd.internationalization.MessagesHandler;
import com.dbs.pd.model.custom.CreateWIPPackModel;*/

/**
 * @author Administrator
 */
public class RestEntity implements EntityBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 267755863298736996L;

	private String responseCode;
	
	private String responseDesc;
	
	private Integer status;

	private Object object;
	
	private Integer totalRows;

	public RestEntity() {
	}

	public RestEntity(Integer status, String responseCode, String msg, Object object, Integer totalRows) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
		this.object = object;
		this.totalRows = totalRows;
	}
	
	public RestEntity(Integer status, String responseCode, String msg, Object object) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
		this.object = object;
	}

	public RestEntity(Integer status, String responseCode, String msg) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
	}

	public RestEntity(Integer status, String responseCode) {
		super();
		this.status = status;
		this.responseCode = responseCode;
	}
	
	public static RestEntity success(Object object, Integer totalRows) {
		return new RestEntity(1, "M1000", null, object, totalRows);
	}

	public static RestEntity success(Object object) {
		return new RestEntity(1, "M1000", null, object);
	}

	public static RestEntity success() {
		return new RestEntity(1, "M1000");
	}

	public static RestEntity failed() {
		return new RestEntity(-1, "FAILED");
	}

	public static RestEntity failed(String msg) {
		return new RestEntity(-1, "E1999", msg);
	}

	//public static RestEntity failed(String msg, String errorCode) {
		//msg = InternationalizationUtil.getMsg(msg);
		//return new RestEntity(-1, msg, errorCode);
	//}
	
	public static RestEntity decide(Object object) {
		Map<String, Object> map = JSON.parseObject(JSON.toJSONString(object));
		if("SUCCESS".equals(map.get("msg"))) {
			return new RestEntity(1, "M1000", null, map.get("result"));
		} else {
			return new RestEntity(-1, "E1999", map.get("msg").toString());
		}
	}

	/*
	 * public static RestEntity failed(String msg) { if(StringUtils.isNotEmpty(msg)
	 * && msg.startsWith(PdException.EXCEPTION_PREFIX)){ //Expected Sample Value:
	 * PED1234-Error //errorCode: PED1234, msg: Error String errorCode =
	 * msg.substring(0,PdException.EXCEPTION_PREFIX.length()+4); msg =
	 * msg.substring(PdException.EXCEPTION_PREFIX.length()+5); return new
	 * RestEntity(-1, errorCode, MessagesHandler.getMsg(msg)); } else{ return new
	 * RestEntity(-1, PdException.UNKNOWN_EXCEPTION_CODE,
	 * MessagesHandler.getMsg(msg)); } }
	 */
	
	public boolean isFailed(){
		return -1 == this.status;
	}

	/*
	 * public static RestEntity failed(Map<String, String> map, String code) {
	 * String msg = MessagesHandler.getMsg(code); if (map != null && msg != null) {
	 * for (String key : map.keySet()) { msg = msg.replace("{" + key + "}",
	 * map.get(key)); } } return new RestEntity(-1, msg); }
	 */
	
	/*
	 * public static RestEntity failed(String msg, String errorCode) {
	 * if(StringUtils.isNotEmpty(msg)){ if(msg.indexOf("ORA-20001") > 0){ errorCode
	 * = PdException.DEFAULT_EXCEPTION_CODE; msg =
	 * msg.substring(msg.indexOf("ORA-")).split("\n")[0]; } else
	 * if(msg.indexOf("ORA-") > 0){ msg =
	 * msg.substring(msg.indexOf("ORA-")).split("\n")[0]; } } return new
	 * RestEntity(-1, errorCode, msg); }
	 */
	
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public Integer getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}

	public static RestEntity sqlInsertSuccess(String msg) {
		return success(msg);
	}

	public static RestEntity process_success(Object result) {
		return success(result);
	}
}
