package com.dbs.framework.jwt;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.dbs.util.PBKDF2Encryption;

/*import com.dbs.util.PBKDF2Encryption;*/

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private JwtUserDetailsService userDetailsService;

	private final String SALT = "abcde12345";

	/**
	 * custom authenticate method
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		return userValidation(authentication);
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	private Authentication userValidation(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		System.out.println(String.format("%s.%s -> %s", new Object() {
		}.getClass().getEnclosingClass().getSimpleName(), new Object() {
		}.getClass().getEnclosingMethod().getName(), username));
		System.out.println(StringUtils.repeat("-", 50));

		String password = "";
		try {
			password = PBKDF2Encryption.getPBKDF2((String) authentication.getCredentials(), SALT);
			// userModel.get(0).getSalt());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			return null;
		}

		try {
			UserDetails user = userDetailsService.loadUserByUsername(username);
			if (user.getPassword().equals("admin")) {
				UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(username, null,
						user.getAuthorities());
				System.out.println(String.format("%s.%s -> %s(%s, %s, %s)", new Object() {
				}.getClass().getEnclosingClass().getSimpleName(), new Object() {
				}.getClass().getEnclosingMethod().getName(), "UsernamePasswordAuthenticationToken", username, null,
						user.getAuthorities()));
				System.out.println(StringUtils.repeat("-", 50));
				return result;
			}
		} catch (UsernameNotFoundException e) {
			System.out.println(String.format("%s.%s -> %s", new Object() {
			}.getClass().getEnclosingClass().getSimpleName(), new Object() {
			}.getClass().getEnclosingMethod().getName(), e.getMessage()));
			System.out.println(StringUtils.repeat("-", 50));
			// if return null, then
			// -> auto-run loadUserByUsername again (don't know why and where to call this
			// again)
			// -> throw org.springframework.security.authentication.BadCredentialsException:
			// Bad credentials
			return null;
		}
		System.out.println(String.format("%s.%s -> %s", new Object() {
		}.getClass().getEnclosingClass().getSimpleName(), new Object() {
		}.getClass().getEnclosingMethod().getName(), "NoChange"));
		System.out.println(StringUtils.repeat("-", 50));
		return authentication;
	}
}
