package com.dbs.framework.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dbs.util.UserInfoUtils;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Value("${gateway.route.auth.url}")
    private String AUTH_API;
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	
		/* RestTemplate rest = call (AUTH_API+"/auth"); */
        System.out.println(String.format("%s.%s(%s) -> %s", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName(), username, AUTH_API));
        String[] userCode = UserInfoUtils.getUserCode(AUTH_API, username);
        return new User(userCode[0], userCode[1], createAuthority(userCode[2]));
    }

    private List<SimpleGrantedAuthority> createAuthority(String roles) {
        String[] roleArray = roles.split(",");
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        for (String role : roleArray) {
            authorityList.add(new SimpleGrantedAuthority(role));
        }
        return authorityList;
    }
}