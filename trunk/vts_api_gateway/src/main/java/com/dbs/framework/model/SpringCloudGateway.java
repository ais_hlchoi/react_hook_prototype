package com.dbs.framework.model;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.cloud.gateway")
public class SpringCloudGateway {

	private List<SpringCloudGatewayRoute> routes;

	public List<SpringCloudGatewayRoute> getRoutes() {
		return routes;
	}

	public void setRoutes(List<SpringCloudGatewayRoute> routes) {
		this.routes = routes;
	}
}
