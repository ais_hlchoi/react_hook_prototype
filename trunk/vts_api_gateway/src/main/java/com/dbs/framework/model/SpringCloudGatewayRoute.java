package com.dbs.framework.model;

import java.util.List;

public class SpringCloudGatewayRoute {

	private String id;
	private String uri;
	private List<String> predicates;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<String> getPredicates() {
		return predicates;
	}

	public void setPredicates(List<String> predicates) {
		this.predicates = predicates;
	}
}
