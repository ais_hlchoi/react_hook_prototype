package com.dbs.framework.model;

import org.apache.commons.lang.StringUtils;

import com.dbs.util.InternationalizationUtil;

public class CommonExceptionHandling {
	
	public static String formatError(String err) {
		
		if (StringUtils.isBlank(err)) {
			err = "SYS_ERR";
		}
		
    	err = InternationalizationUtil.getMsg(err);
    	
		if(StringUtils.isNotBlank(err)) {
			int index = err.indexOf("org.hibernate.service.spi.ServiceException:");
			if(index > -1) {
				String oracleError = err.substring(index, err.length());
				if(StringUtils.isNotBlank(oracleError)) {
					index = oracleError.indexOf(":");
					if(index > 0) {
						oracleError = oracleError.substring(index+1, oracleError.length());
					}
					err = oracleError;
				}
			}
		}
		if(StringUtils.isNotBlank(err)) {
			int index = err.indexOf("nested exception is");
			if(index > 0) {
				String oracleError = err.substring(index, err.length());
				if(StringUtils.isNotBlank(oracleError)) {
					index = oracleError.indexOf(":");
					if(index > 0) {
						oracleError = oracleError.substring(index+1, oracleError.length());
					}
					err = oracleError;
				}
			}
		}
		
		return err;
	}
	
}
