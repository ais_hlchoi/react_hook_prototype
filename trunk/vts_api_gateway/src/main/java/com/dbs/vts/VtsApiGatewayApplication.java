package com.dbs.vts;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.dbs.framework.model.SpringCloudGateway;

import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.dbs", "com.dbs.framework.model" })
@RestController
public class VtsApiGatewayApplication {

	@Autowired
	private SpringCloudGateway gateway;

	public static void main(String[] args) {
		SpringApplication.run(VtsApiGatewayApplication.class, args);
	}
	
	//due to incompatible issues spring-cloud-starter-gateway vs spring-boot-starter-security
	//need to custom define this, TODO need to resolve later
	@Bean
	public ServerCodecConfigurer serverCodecConfigurer() {
	   return ServerCodecConfigurer.create();
	}

	@Bean
	public RouteLocator readRoutes(RouteLocatorBuilder builder) {
		if (null == gateway)
			return builder.routes().build();

		System.out.println(String.format("%s.%s:", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName()));
		gateway.getRoutes().stream().forEach(r -> builder.routes().route(r.getId(), p -> {
			System.out.println(String.format("%-7s -> uri(%s), path(%s)", r.getId(), r.getUri(), r.getPredicates().stream().filter(s -> s.startsWith("Path=")).map(s -> s.replaceAll("Path=\s*", "")).collect(Collectors.joining(", "))));
			r.getPredicates().stream().filter(s -> s.startsWith("Path=")).map(s -> s.replaceAll("Path=\s*", "")).forEach(s -> p.path(s));
			return p.uri(r.getUri());
		}));

		return builder.routes().build();
	}

	@RequestMapping("/test")
	public Mono<String> test() {
		System.out.println(String.format("%s.%s", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName()));
		Map<String, String> map = new HashMap<String, String>();
		map.put("token", "abc.123");
		return Mono.just(JSON.toJSONString(map));
	}
}
