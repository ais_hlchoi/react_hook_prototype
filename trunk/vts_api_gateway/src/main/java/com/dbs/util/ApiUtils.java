package com.dbs.util;

import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ApiUtils {

	private ApiUtils() {
		// for SonarQube scanning purpose
	}

	public static JsonNode post(final String url, final Object params, final Map<String, String> customHeaders) throws JsonProcessingException {
		if (StringUtils.isBlank(url))
			return null;

		return new ApiUtils().send(url, params, customHeaders);
	}

	protected JsonNode send(final String url, final Object params, final Map<String, String> customHeaders) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String stringifyParams = mapper.writeValueAsString(params);
		HttpHeaders headers = new HttpHeaders();
		if (MapUtils.isNotEmpty(customHeaders))
			customHeaders.entrySet().stream().forEach(o -> headers.add(o.getKey(), o.getValue()));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(stringifyParams, headers);
		ResponseEntity<String> result = new RestTemplate().postForEntity(url, request, String.class);
		return mapper.readTree(result.getBody());
	}
}
