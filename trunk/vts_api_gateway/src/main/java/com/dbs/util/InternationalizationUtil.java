package com.dbs.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @description:
 */
public class InternationalizationUtil {

	protected static Logger logger = Logger.getLogger(InternationalizationUtil.class);

	public static enum localeCde {
		zh_CN, // China
		zh_HK, // Hong Kong
		en_US,// USA
	}

	private static Map<String, ResourceBundle> budleMap = new HashMap<>();

	/**
	 * @param languageEnv 
	 * @return
	 * @throws Exception
	 */
	public static ResourceBundle getBundleByCurrentEnv(String languageEnv) {
		Locale locale = null;
		ResourceBundle bundle = null;
		if (StringUtils.isBlank(languageEnv)) {
			try {
				locale = Locale.getDefault();
			} finally {
				if (locale == null) {// Fail to get language file
					logger.error(
							"\u83b7\u53d6\u7cfb\u7edf\u9ed8\u8ba4\u7684\u56fd\u5bb6\u002f\u8bed\u8a00\u73af\u5883\u5931\u8d25");
					return null;
				}
			}
			languageEnv = locale.toString();
		}
		if (budleMap.containsKey(languageEnv)) {
			bundle = budleMap.get(languageEnv);
		} else {
			try {
				bundle = ResourceBundle.getBundle("internationalization/messages-" + languageEnv);
				budleMap.put(languageEnv, bundle);
			} catch (MissingResourceException e) {
				logger.error("\u7f3a\u5931\u5bf9\u5e94\u8bed\u8a00\u5305\u003a" + languageEnv);
			}
		}
		return bundle;
	}

	/**
	 * @param code
	 * @param languageEnv
	 * @return
	 * @throws Exception
	 */
	public static String getMsg(String code, String languageEnv) {
		if (StringUtils.isBlank(code)) {
			return code;
		}
		ResourceBundle bundle = getBundleByCurrentEnv(languageEnv);
		ResourceBundle CNBundle = getBundleByCurrentEnv(localeCde.zh_HK.name());
		if (CNBundle == null) {
			return code;
		}
		if (bundle == null) {
			bundle = CNBundle;
		}
		String resuleMsg = "";
		String[] codes = code.split(";");
		for (String key : codes) {
			if (StringUtils.isNotBlank(key)) {
				try {
					resuleMsg += bundle.getString(key) + ";";
				} catch (MissingResourceException e) {
					try {
						resuleMsg += CNBundle.getString(key) + ";";
					} catch (MissingResourceException ex) {
						resuleMsg += key + ";";
					}
				}
			}
		}
		 if (resuleMsg.endsWith(";")) {
			 resuleMsg = resuleMsg.substring(0, resuleMsg.length() - 1);
		 }
		return resuleMsg;
	}

	/**
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static String getMsg(String code) {
		return getMsg(code, null);
	}
	
	/**
	 * @param code
	 * @param languageEnv
	 * @return
	 * @throws Exception
	 */
	public static String getMsgTwo(String code, String languageEnv) {
		if (StringUtils.isBlank(code)) {
			return code;
		}
		ResourceBundle bundle = getBundleByCurrentEnv(languageEnv);
		ResourceBundle CNBundle = getBundleByCurrentEnv(localeCde.zh_CN.name());
		if (CNBundle == null) {
			return code;
		}
		if (bundle == null) {
			bundle = CNBundle;
		}
		String resuleMsg = "";
		String[] codes = code.split(";");
		for (String key : codes) {
			if (StringUtils.isNotBlank(key)) {
				try {
					resuleMsg += bundle.getString(key);
				} catch (MissingResourceException e) {
					try {
						resuleMsg += CNBundle.getString(key);
					} catch (MissingResourceException ex) {
						resuleMsg += key;
					}
				}
			}
		}
		return resuleMsg;
	}

	/**
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static String getMsgTwo(String code) {
		return getMsgTwo(code, null);
	}

	/**
	 * @param map
	 * @param code
	 * @return
	 */
	public static String getMsg(Map<String, String> map, String code) {
		String msg = getMsg(code, null);
		if (map != null && msg != null) {
			for (String key : map.keySet()) {
				msg = msg.replace("{" + key + "}", map.get(key));
			}
		}
		return msg;
	}

	/**
	 * @param code
	 * @param result
	 * @return
	 */
	public static String getMsg(String code, int result) {
		return getMsg(code, 1, result);
	}

	/**
	 * @param code
	 * @param size
	 * @param result
	 * @return
	 */
	public static String getMsg(String code, int size, int result) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("size", String.valueOf(size));
		map.put("result", String.valueOf(result));
		return getMsg(map, code);
	}
}