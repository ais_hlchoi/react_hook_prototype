package com.dbs.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class UserInfoUtils {

	public static String[] getUserCode(String path, String username) throws UsernameNotFoundException {
		String[] userCode = new String[3];
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("username", encodeValue(username));
			String url = path + (MapUtils.isEmpty(params) ? "" : ((path.indexOf("?") < 0 ? "?" : "&") + params.entrySet().stream().map(o -> String.format("%s=%s", o.getKey(), o.getValue())).collect(Collectors.joining("&"))));
			System.out.println(String.format("%s.%s -> %s", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName(), url));
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
			if (HttpStatus.OK != response.getStatusCode()) {
				throw new Exception(String.format("No response from %s", path));
			}
			System.out.println(String.format("%s.%s -> %s", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName(), response.getStatusCode()));
			userCode = parseUserCodeFromJsonNode(path, username, response.getBody());
			System.out.println(String.format("%s.%s -> %s", new Object() {}.getClass().getEnclosingClass().getSimpleName(), new Object() {}.getClass().getEnclosingMethod().getName(), userCode[0]));
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage(), e.getCause());
		}
		return userCode;
	}

	protected static String encodeValue(String value) throws UnsupportedEncodingException {
		return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
	}

	protected static String[] parseUserCodeFromJsonNode(String path, String username, String responseBody) throws Exception {
		String[] userCode = new String[3];
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(responseBody);
		JsonNode object = root.path("object");
		ArrayNode userInfoList = (ArrayNode) object.get("userInfo");
		if (null == userInfoList) {
			throw new Exception(String.format("No userInfo from %s%n%s", path, responseBody));
		}
		Iterator<JsonNode> it = userInfoList.elements();
		if (!it.hasNext()) {
			throw new Exception(String.format("Empty userInfo from %s", path));
		}
		JsonNode userInfo = it.next();
		userCode[0] = userInfo.path("userCode").asText();
		if (StringUtils.isBlank(userCode[0])) {
			throw new Exception(String.format("Empty userCode where username=%s", username));
		}
		userCode[1] = userInfo.path("pwd").asText();
		ArrayNode userRoleList = (ArrayNode) userInfo.get("roles");
		if (null == userRoleList) {
			throw new Exception(String.format("No userRole where username=%s%n%s", username, responseBody));
		}
		Iterator<JsonNode> itr = userRoleList.elements();
		if (!itr.hasNext()) {
			throw new Exception(String.format("Empty userRole where username=%s%n%s", username, responseBody));
		}
		userCode[2] = StreamSupport.stream(Spliterators.spliteratorUnknownSize(itr, Spliterator.ORDERED), false).map(o -> o.asText()).collect(Collectors.joining(","));
		return userCode;
	}
}
