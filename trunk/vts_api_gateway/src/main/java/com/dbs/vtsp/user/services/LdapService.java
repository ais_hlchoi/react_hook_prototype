package com.dbs.vtsp.user.services;

import com.fasterxml.jackson.databind.JsonNode;

public interface LdapService {

	JsonNode login(String username, String password);
}
