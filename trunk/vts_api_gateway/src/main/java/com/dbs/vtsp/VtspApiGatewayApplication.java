package com.dbs.vtsp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.web.server.WebFilter;

import com.dbs.util.CustomPasswordEncoder;
import com.dbs.vtsp.model.SpringCloudGateway;
import com.dbs.vtsp.model.SpringCloudGatewayRoute;
import com.dbs.vtsp.security.auth.CustomLoginReactiveAuthenticationManager;
import com.dbs.vtsp.security.auth.bearer.BearerTokenReactiveAuthenticationManager;
import com.dbs.vtsp.security.auth.bearer.ServerHttpBearerAuthenticationConverter;
import com.dbs.vtsp.security.auth.filter.CorsWebFilter;
import com.dbs.vtsp.security.auth.filter.CustomAuthenticationWebFilter;
import com.dbs.vtsp.security.auth.handler.CustomAuthenticationFailureHandler;
import com.dbs.vtsp.security.auth.handler.CustomAuthenticationSuccessHandler;
import com.dbs.vtsp.user.services.impl.LdapRepositoryServiceImpl;
import com.dbs.vtsp.user.services.impl.UserRepositoryServiceImpl;

@SpringBootApplication
@ComponentScan(basePackages = {"com.dbs"})//packages spring will map scan for DI
public class VtspApiGatewayApplication {

	@Autowired
	private SpringCloudGateway gateway;

	@Value("${user.auth.url}")
	private String userAuthUrl;

	public static void main(String[] args) {
		SpringApplication.run(VtspApiGatewayApplication.class, args);
	}

	/**
	 * A custom UserDetailsService to provide quick user rights for Spring Security,
	 * more formal implementations may be added as separated files and annotated as
	 * a Spring stereotype.
	 *
	 * @return MapReactiveUserDetailsService an InMemory implementation of user details
	 */
//	@Bean
	public ReactiveUserDetailsService userDetailsRepository() {
		UserDetails user = User.builder().passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode).username("user").password("user").roles("USER", "ADMIN").build();
		return new MapReactiveUserDetailsService(user);
	}

	/**
	 * For Spring Security webflux, a chain of filters will provide user authentication
	 * and authorization, we add custom filters to enable JWT token approach.
	 *
	 * @param http An initial object to build common filter scenarios.
	 *             Customized filters are added here.
	 * @return SecurityWebFilterChain A filter chain for web exchanges that will provide security
	 **/
	@Bean
	public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
		http
			.cors().disable()
			.csrf().disable()
			.httpBasic()
			.and()
			.formLogin()
			.authenticationManager(loginReactiveAuthenticationManager())
			.authenticationSuccessHandler(new CustomAuthenticationSuccessHandler("formLogin"))
			.authenticationFailureHandler(new CustomAuthenticationFailureHandler())
			.and()
			.authorizeExchange()
			.pathMatchers("/login/**").permitAll()
			.and()
			.addFilterAt(basicAuthenticationFilter(), SecurityWebFiltersOrder.HTTP_BASIC)
			.authorizeExchange()
			.pathMatchers(getRoutePaths())
			.authenticated()
	        .and()
	        .addFilterAt(bearerAuthenticationFilter(getRoutePaths()), SecurityWebFiltersOrder.AUTHENTICATION)
	        .addFilterAt(corsWebFilter("cors"), SecurityWebFiltersOrder.CORS)
			;
		return http.build();
	}
	
	private WebFilter corsWebFilter(String name) {
		return new CorsWebFilter(name);
	}
	
	private ReactiveAuthenticationManager loginReactiveAuthenticationManager() {
		return new CustomLoginReactiveAuthenticationManager(new UserRepositoryServiceImpl()).setLdapService(new LdapRepositoryServiceImpl(userAuthUrl));
	}

	private String[] getRoutePaths() {
		if (null == gateway)
			return new String[] {};

		return gateway.getRoutes().stream().map(this::getRoutePredicates).filter(CollectionUtils::isNotEmpty).flatMap(this::getRouteStream).toArray(String[]::new);
	}

	private List<String> getRoutePredicates(SpringCloudGatewayRoute route) {
		// for SonarQube scanning purpose
		if (null == route || CollectionUtils.isEmpty(route.getPredicates()))
			return new ArrayList<>();

		return route.getPredicates().stream().filter(s -> s.startsWith("Path=")).flatMap(s -> Stream.of(s.replaceAll("Path=\\s*", "").split("\\s*,\\s*"))).collect(Collectors.toList());
	}

	private Stream<String> getRouteStream(List<String> list) {
		// for SonarQube scanning purpose
		return list.stream();
	}

	/**
	 * Use the already implemented logic in AuthenticationWebFilter and set a custom
	 * SuccessHandler that will return a JWT when a user is authenticated
	 * Create an AuthenticationManager using the UserDetailsService defined above
	 *
	 * @return AuthenticationWebFilter
	 */
	private WebFilter basicAuthenticationFilter() {
		UserDetailsRepositoryReactiveAuthenticationManager manager = new UserDetailsRepositoryReactiveAuthenticationManager(new UserRepositoryServiceImpl());
		manager.setPasswordEncoder(myPasswordEncoder());
		CustomAuthenticationWebFilter authenticationFilter = new CustomAuthenticationWebFilter(manager, "Auth");
		authenticationFilter.setAuthenticationSuccessHandler(new CustomAuthenticationSuccessHandler("basic"));
		authenticationFilter.setRequiresAuthenticationMatcher(ServerWebExchangeMatchers.pathMatchers("/login/**"));
		return authenticationFilter;
	}

	/**
	 * Use the already implemented logic by AuthenticationWebFilter and set a custom
	 * converter that will handle requests containing a Bearer token inside the HTTP
	 * Authorization header. Set a dummy authentication manager to this filter, it's
	 * not needed because the converter handles this.
	 *
	 * @return bearerAuthenticationFilter that will authorize requests containing a JWT
	 */
	private WebFilter bearerAuthenticationFilter(String[] path) {
		AuthenticationWebFilter authenticationFilter = new AuthenticationWebFilter(new BearerTokenReactiveAuthenticationManager());
		authenticationFilter.setServerAuthenticationConverter(new ServerHttpBearerAuthenticationConverter());
		authenticationFilter.setRequiresAuthenticationMatcher(ServerWebExchangeMatchers.pathMatchers(path));
		return authenticationFilter;
	}

	@Bean
    public PasswordEncoder myPasswordEncoder() {
        return new CustomPasswordEncoder();
    }

	/**
	 * SecurityWebFiltersOrder
	 * - FIRST
	 * - HTTP_HEADERS_WRITER
	 * - HTTPS_REDIRECT
	 * - CORS
	 * - CSRF
	 * - REACTOR_CONTEXT
	 * - HTTP_BASIC
	 * - FORM_LOGIN
	 * - AUTHENTICATION
	 * - ANONYMOUS_AUTHENTICATION
	 * - OAUTH2_AUTHORIZATION_CODE
	 * - LOGIN_PAGE_GENERATING
	 * - LOGOUT_PAGE_GENERATING
	 * - SECURITY_CONTEXT_SERVER_WEB_EXCHANGE
	 * - SERVER_REQUEST_CACHE
	 * - LOGOUT
	 * - EXCEPTION_TRANSLATION
	 * - AUTHORIZATION
	 * - LAST
	 */
}
