package com.dbs.vtsp.security.auth.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;

public class CorsWebFilter implements WebFilter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String name;

	public CorsWebFilter(String name) {
		this.name = name;
	}

	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		logger.info("Process in CORS web filter " + name + " ...");
		exchange.getResponse().getHeaders().add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
		exchange.getResponse().getHeaders().add(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "POST,GET,OPTIONS,DELETE,PUT");
		exchange.getResponse().getHeaders().add(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "3600");
		exchange.getResponse().getHeaders().add(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "*");
		return chain.filter(exchange);
	}
}
