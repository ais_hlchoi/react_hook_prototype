package com.dbs.vtsp.user.services.impl;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.dbs.vtsp.model.UserInfo;
import com.dbs.vtsp.model.UserProfile;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Mono;

@Component
public class UserRepositoryServiceImpl implements ReactiveUserDetailsService {

	@Override
	public Mono<UserDetails> findByUsername(String userInfo) {
		UserDetails user = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			user = parseUser(mapper.readValue(userInfo, UserInfo.class));
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage(), e.getCause());
		}
		return Mono.justOrEmpty(user);
	}

	protected String[] getRoles(String roleCode) {
		return StringUtils.isBlank(roleCode) ? new String[] {} : Stream.of(roleCode.split("\\s*,\\s*")).filter(StringUtils::isNotBlank).toArray(String[]::new);
	}

	protected UserDetails parseUser(UserProfile profile) {
		return new User(profile.getUserCode(), profile.getPassCode(), Stream.of(getRoles(profile.getRoleCode())).map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
	}
}
