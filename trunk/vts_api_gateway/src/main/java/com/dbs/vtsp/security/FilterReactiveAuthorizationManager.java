package com.dbs.vtsp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authorization.AuthorizationContext;

import reactor.core.publisher.Mono;

public class FilterReactiveAuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String name;

	public FilterReactiveAuthorizationManager(String name) {
		this.name = name;
	}

	public Mono<AuthorizationDecision> check(Mono<Authentication> authentication, AuthorizationContext object) {
		logger.info("Process in Filter reactive authorization manager " + name + " ...");
		return Mono.just(new AuthorizationDecision(true));
	}
}
