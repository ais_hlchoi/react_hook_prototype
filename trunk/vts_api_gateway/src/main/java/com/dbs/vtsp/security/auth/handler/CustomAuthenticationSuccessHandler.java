package com.dbs.vtsp.security.auth.handler;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;

import com.dbs.util.AesEncryptUtil;
import com.dbs.vtsp.security.auth.jwt.JwtTokenService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class CustomAuthenticationSuccessHandler implements ServerAuthenticationSuccessHandler {

	private static final String PATTERN = "{\"token\":\"%s\", \"key\":\"%s\"}";

	private final String name;

	@Value("${user.auth.algorithm}")
	private String algorithm;

	@Value("${user.auth.key}")
	private String key;

	@Value("${user.auth.iv}")
	private String iv;

	public CustomAuthenticationSuccessHandler(String name) {
		Assert.notNull(name, "name is required");
		this.name = name;
	}

	public CustomAuthenticationSuccessHandler() {
		this.name = "null";
	}

	@Override
	public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
		ServerWebExchange exchange = webFilterExchange.getExchange();

		if ("formLogin".equals(name)) {
			byte[] bytes = String.format(PATTERN, tokenFromAuthentication(authentication), AesEncryptUtil.encrypt(authentication.getName(), key, iv, algorithm)).getBytes(StandardCharsets.UTF_8);
			DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
			return exchange.getResponse().writeWith(Flux.just(buffer));
		}

		return webFilterExchange.getChain().filter(exchange);
	}

	private String tokenFromAuthentication(Authentication authentication) {
		return JwtTokenService.generateToken(authentication.getName(), authentication.getCredentials(), authentication.getAuthorities(), authentication.getDetails());
	}
}
