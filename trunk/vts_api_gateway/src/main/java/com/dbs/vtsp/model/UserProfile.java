package com.dbs.vtsp.model;

public interface UserProfile {

	String getUserCode();
	String getPassCode();
	String getRoleCode();
	String getSessCode();
}
