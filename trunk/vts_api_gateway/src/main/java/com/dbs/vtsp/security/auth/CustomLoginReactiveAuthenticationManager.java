package com.dbs.vtsp.security.auth;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.util.Assert;

import com.dbs.vtsp.security.auth.jwt.CustomAuthenticationToken;
import com.dbs.vtsp.user.services.LdapService;
import com.fasterxml.jackson.databind.JsonNode;

import reactor.core.publisher.Mono;

public class CustomLoginReactiveAuthenticationManager implements ReactiveAuthenticationManager {

	private ReactiveUserDetailsService userDetailsService;
	private LdapService ldapService;

	public CustomLoginReactiveAuthenticationManager(ReactiveUserDetailsService userDetailsService) {
		Assert.notNull(userDetailsService, "userDetailsService is required");
		this.userDetailsService = userDetailsService;
	}

	public CustomLoginReactiveAuthenticationManager setLdapService(LdapService ldapService) {
		Assert.notNull(ldapService, "ldapService is required");
		this.ldapService = ldapService;
		return this;
	}

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();

		JsonNode node = ldapService.login(username, password);
		JsonNode object = ((JsonNode) node.get("object"));
		String userInfo = "{"
				+ "\"userCode\":\"" + username + "\","
				+ "\"passCode\":\"" + password + "\","
				+ "\"roleCode\":\"" + object.get("roleList").toString().replaceAll("\"", "") + "\"}";
		String sessCode = object.get("sessCode").asText();
		String dateTime = object.get("dateTime").asText();
		// we use userInfo direct to generate userDetail instead of using username to query from API
		Map<String, Object> details = new HashMap<>();
		details.put("sessCode", sessCode);
		details.put("dateTime", StringUtils.isBlank(dateTime) || !NumberUtils.isCreatable(dateTime) ? null : NumberUtils.createLong(dateTime));
		return userDetailsService.findByUsername(userInfo).flatMap(user -> Mono.just(new CustomAuthenticationToken(user, details)));
	}
}
