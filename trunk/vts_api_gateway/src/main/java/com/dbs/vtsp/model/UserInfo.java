package com.dbs.vtsp.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserInfo implements UserProfile {

	private String userCode;
	private String passCode;
	private String roleCode;
	private String sessCode;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPassCode() {
		return passCode;
	}

	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getSessCode() {
		return sessCode;
	}

	public void setSessCode(String sessCode) {
		this.sessCode = sessCode;
	}

	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (NullPointerException | JsonProcessingException e) {
			return String.format("{\"error\":\"%s\"}", e.getMessage());
		}
	}
}
