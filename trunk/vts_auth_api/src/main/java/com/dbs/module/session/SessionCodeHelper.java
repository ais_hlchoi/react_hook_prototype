package com.dbs.module.session;

public interface SessionCodeHelper {

	String create();

	String create(int index);

	boolean test(String text);
}
