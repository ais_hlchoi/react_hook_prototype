package com.dbs.framework.ldap.util;

import org.apache.commons.lang.StringUtils;

import com.dbs.framework.ldap.common.LoginConstants;

public class LoginUtil {
	
	private LoginUtil(){}


	public static String validate(String username, String password) {
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			return "Please input the Logon ID and Password.";
		}
		return "" ;
	}

	public static boolean loginSuccess(String userid, String oneBankId) {
		return StringUtils.equalsIgnoreCase(userid, oneBankId);
	}

	public static String getLoginADErrorMessage(String code) {
		String message = "";

		if ("SUCCESS".equals(code)) {
			message = "Success";
		} else if (LoginConstants.RC_USERID_OR_SECRET_INCORRECT.equals(code)) {
			message = "Incorrect Username and Password.";
		} else if (LoginConstants.RC_ACCOUNT_EXPIRED.equals(code)) {
			message = "Account Expired.";
		} else if (LoginConstants.RC_SECRET_EXPIRED.equals(code)) {
			message = "Password Expired.";
		} else if (LoginConstants.RC_USER_DISABLED.equals(code)) {
			message = "User Disabled.";
		} else if (LoginConstants.RC_USER_LOCKOUT.equals(code)) {
			message = "Login Account is Locked";
		} else if (LoginConstants.RC_LOGON_DENIED_AT_THIS_TIME.equals(code)) {
			message = "Unable to logon at current time range";
		} else if (LoginConstants.RC_OTHER_ERROR.equals(code)) {
			message = "Other error. Please contact Administrator";
		} else if (LoginConstants.RC_AD_SERVER_ERROR.equals(code)) {
			message = "AD Server error. Please contact Administrator";
		} else if (LoginConstants.RC_MUST_ENABLE_SSL.equals(code)) {
			message = "Must enable SSL. Please contact Administrator";
		} else if (LoginConstants.RC_SECRET_POLICY_VIOLATE.equals(code)) {
			message = "New password not comply with password policy";
		} else if (LoginConstants.RC_NO_SUCH_USER.equals(code)) {
			message = "No such user";
		} else if (LoginConstants.RC_ROLE_NOT_EXISTS.equals(code)) {
			message = "VTSP role not found.";
		} else if (code.startsWith(LoginConstants.RC_SECRET_WARNING)) {
			Integer day = Integer.valueOf(code.split(":")[1]) + 1;
			message = "Your password will expire in " + day + " day(s). Please change your password.";
		} else {
			message = "Invalid error code: " + code + ". Please contact Administrator";
		}

		return message;
	}
}
