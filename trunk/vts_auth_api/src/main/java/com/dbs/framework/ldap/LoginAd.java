package com.dbs.framework.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.filter.EqualsFilter;

import com.dbs.framework.exception.SysException;
import com.dbs.framework.ldap.common.LoginConstants;
import com.dbs.framework.ldap.config.LoginConfig;
import com.dbs.framework.ldap.helper.LoginHelper;
import com.dbs.util.AesEncryptUtil;
import com.dbs.util.FortifyStrategyUtils;

/**
 * LoginAD - Java module to provide a login Windows Active Directory function call for Oracle form
 *           Oracle form source file (*.fmb) import this java class, and call "login(userId, credentials)" function to check the login is correct or not
 *           
 * @author steve_shum
 * @date   2014-03-19
 *
 * Modification history
 * -------------------------------------------------------------------------------------------------------------------
 * 2015-03-16	Steve Shum		Provide a new function "logMessage(logLevel, logMessage)" to log a message via log4j 
 * 2016-02-23   Steve Shum      1. Able to return credentials expire warning return code
 *                              2. Able to use SSL connection (LDAPS)
 *                              3. Able to change AD user credentials
 *                              4. Able to change AD user credentials by Admin
 *                              5. Able to unlock AD user account (lock AD user a/c is not work)
 * 2018-01-22   Steve Shum      Handle Log Forging issue (change to use Log4j2)                              
 * 2018-02-06	Ben Cheng		BOSS use this file directly instead of using jar file.
 * 2019-06-19	Matthew Ho		6. Support both reg1 and reg3 login * 
 * 2021-05-12	Joe Lee			7. Solve Fortify and SonarQube issues
 */

public class LoginAd {

	private static Logger logger = LoggerFactory.getLogger(LoginAd.class);

	private static final String MSG_USER_FORMAT = "User %s %s";
	private static final String MSG_LOGIN_FORMAT = "loginAD - %s, user: %s caused by %s";
	private static final String MSG_RETRY_FORMAT = "loginAD - %s, will try another url";

	/**
	 * Make the Constructor private
	 */
	private LoginAd() {
	}

	/**
	 * login
	 * @param userID
	 * @param userPassword
	 * @return SUCCESS - if login success
	 * 		   Other error code - if login failed (with reason)
	 * @throws Exception - other problem
	 */
	public static Map<String, String> login(LoginConfig lc, String userID, String userPassword) throws NamingException {
		// Check AD authentication is need or not
		if (lc.isWinADAuthenticate()) {
			// No need to do the AD authentication
			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userID), " login. (No AD authentication)"));
			Map<String, String> noADMap = new HashMap<>();
			noADMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_SUCCESS);
			noADMap.put(LoginConstants.RK_ROLE_LIST, "ADMIN");
			return noADMap;
		}

		// AD authentication (support multiple AD servers)
		// 20190619 Matthew Ho
		int index = 0;
		String[] adServerArray = lc.getWinADServerURL().split(",");
		String[] adDomainArray = lc.getWinADDomainList().split(",");
		String[] searchBase = lc.getWinADServerDC().split("\\|");
		// <- 
		
		if(adServerArray.length!=adDomainArray.length || adServerArray.length != searchBase.length) {
			Map<String, String> failMap = new HashMap<>();
			failMap.put(LoginConstants.RK_RESULT, "Incorrect Ldap Config.");
			return failMap;			
		}

		Map<String, String> failMap = new HashMap<>();
		for (String adServer : adServerArray) {
			adServer = adServer.trim();
			logger.info("Try to AD authenticate using server:" + FortifyStrategyUtils.toLogString(adServer));

			try {
				// 20190619 Matthew Ho
				Map<String, String> resultMap = login(lc, userID, userPassword, adServer, adDomainArray[index], searchBase[index]);

				if (LoginConstants.RC_USERID_OR_SECRET_INCORRECT.equals(resultMap.get(LoginConstants.RK_RESULT))) {
					if (index >= (adDomainArray.length - 1)) {
						failMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_USERID_OR_SECRET_INCORRECT);
						return failMap;
					}
				} else {
					return resultMap;
				}
				// <-
			} catch (NamingException ne) {
				logger.warn(String.format(MSG_RETRY_FORMAT, NamingException.class.getSimpleName()));
			} catch(SysException e) {
				logger.warn(String.format(MSG_RETRY_FORMAT, SysException.class.getSimpleName()));
				failMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_ROLE_NOT_EXISTS);
			}
			catch (Exception e) {
				logger.warn(String.format(MSG_RETRY_FORMAT, Exception.class.getSimpleName()));
			}
			index++;
		}
		// all the AD servers failed
		logger.error("AD Server error." + FortifyStrategyUtils.toLogString(lc.getWinADServerURL()));
		if(failMap.get(LoginConstants.RK_RESULT) == null)
			failMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_AD_SERVER_ERROR);
		return failMap;
	}

	/**
	 * login authentication by AD server
	 * @param userID
	 * @param userPassword
	 * @param url
	 * @return SUCCESS - if login success
	 * 		   Other error code - if login failed (with reason)
	 * @throws NamingException - AD server not available
	 * @throws Exception - other problem
	 */
	public static String login(LoginConfig lc, String userID, String userPassword, String url, String winADServerDomain) throws Exception {
		String userDomainID = userID;
		try {
			if (StringUtils.isNotEmpty(winADServerDomain)) {
				userDomainID = winADServerDomain + "\\" + userID;
			}
			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.START.msg));

			// use Properties instead of Hashtable
			Properties env = new Properties();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, url);
			env.put(Context.SECURITY_AUTHENTICATION, lc.getSecurityAuthen());
			env.put(Context.SECURITY_PRINCIPAL, userDomainID);
			env.put(Context.SECURITY_CREDENTIALS, AesEncryptUtil.decode(userPassword));

			logger.info("SSL: " + lc.isEnableSSL());
			// enableSSL = Y
			if (lc.isEnableSSL()) {
				// specify use of ssl
				env.put(Context.SECURITY_PROTOCOL, "ssl");
				logger.info(LoginConstants.SYS_SSL_TRUST_STORE + ": " + lc.getSslTrustStorePath());
				System.setProperty(LoginConstants.SYS_SSL_TRUST_STORE, lc.getSslTrustStorePath());
				logger.info(System.getProperty(LoginConstants.SYS_SSL_TRUST_STORE));
			}

			LdapContext ctx = new InitialLdapContext(env, null);

			// for SonarQube scanning purpose
			String remainExpiryDate = getRemainExpiryDate(lc, ctx, userID);
			ctx.close();

			if (StringUtils.isNotBlank(remainExpiryDate)) {
				logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.EXPIRED.msg + remainExpiryDate));
				return LoginConstants.RC_SECRET_WARNING + remainExpiryDate;
			}

			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.SUCCESS.msg));
			return LoginConstants.RC_SUCCESS;
		} catch (AuthenticationException ae) {
			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.FAILED.msg));
			String errorMessage = ae.getMessage();
			return getErrorMsgCode(errorMessage, userDomainID);
		} catch (NamingException ne) {
			logger.warn(String.format(MSG_LOGIN_FORMAT, NamingException.class.getSimpleName(), FortifyStrategyUtils.toLogString(userDomainID, url), FortifyStrategyUtils.toErrString(ne)));
			throw ne;
		} catch (Exception e) {
			logger.error(String.format(MSG_LOGIN_FORMAT, Exception.class.getSimpleName(), FortifyStrategyUtils.toLogString(userDomainID, url), FortifyStrategyUtils.toErrString(e)));
			throw e;
		}
	}

	/**
	 * copy from function login and return role list
	 * @param userID
	 * @param userPassword
	 * @param url
	 * @return Map with role list and SUCCESS - if login success
	 * 		   Other error code - if login failed (with reason)
	 * @throws NamingException - AD server not available
	 * @throws Exception - other problem
	 */
	public static Map<String, String> login(LoginConfig lc, String userID, String userPassword, String url, String winDomain, String searchBase) throws Exception {
		Map<String, String> resultMap = new HashMap<>();
		String userDomainID = userID;
		try {
			if (!winDomain.isEmpty()) {
				userDomainID = winDomain + "\\" + userID;
			}
			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.START.msg));

			// use Properties instead of Hashtable
			Properties env = new Properties();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, url);
			env.put(Context.SECURITY_AUTHENTICATION, lc.getSecurityAuthen());
			env.put(Context.SECURITY_PRINCIPAL, userDomainID);
			env.put(Context.SECURITY_CREDENTIALS, AesEncryptUtil.decode(userPassword));
//			env.put(Context.SECURITY_CREDENTIALS, userPassword);

			logger.info("SSL: " + lc.isEnableSSL());
			// enableSSL = Y
			if (lc.isEnableSSL()) {
				// specify use of ssl
				env.put(Context.SECURITY_PROTOCOL, "ssl");
				logger.info(LoginConstants.SYS_SSL_TRUST_STORE + ": " + lc.getSslTrustStorePath());
				System.setProperty(LoginConstants.SYS_SSL_TRUST_STORE, lc.getSslTrustStorePath());
				logger.info(System.getProperty(LoginConstants.SYS_SSL_TRUST_STORE));
			}

			logger.info("start to connect: " + url);
			LdapContext ctx = new InitialLdapContext(env, null);
			logger.info("connect success.");
			
			EqualsFilter filter = new EqualsFilter("sAMAccountName", userID /* + "@dbs.com" */);
			SearchControls searchCtls = new SearchControls();
			String[] returnedAtts = { "sAMAccountName", "uid", "sn", "mail", "cn", "givenName", "telephoneNumber", "manager", "memberOf" };
			searchCtls.setReturningAttributes(returnedAtts);
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String searchFilter = filter.toString();
			logger.info("search user...");
			NamingEnumeration<?> answer = ctx.search(searchBase, searchFilter, searchCtls);

			// for SonarQube scanning purpose
			StringBuilder roleList = getRoleList(answer);
			String remainExpiryDate = getRemainExpiryDate(lc, ctx, userID);
			ctx.close();

			if (StringUtils.isNotBlank(remainExpiryDate)) {
				logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.EXPIRED.msg + remainExpiryDate));
				resultMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_SECRET_WARNING);
				resultMap.put(LoginConstants.RK_EXPIRY_DATE, remainExpiryDate);
				return resultMap;
			}

			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.SUCCESS.msg));
			resultMap.put(LoginConstants.RK_ROLE_LIST, roleList.length() > 0 ? roleList.substring(1) : "");
			resultMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_SUCCESS);
			return resultMap;
		} catch (AuthenticationException ae) {
			logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), LoginMsgCode.FAILED.msg));
			String errorMessage = ae.getMessage();
			resultMap.put(LoginConstants.RK_RESULT, LoginConstants.RC_USERID_OR_SECRET_INCORRECT);
			resultMap.put(LoginConstants.RK_ERROR, getErrorMsgCode(errorMessage, userDomainID));
			return resultMap;
		} catch (NamingException ne) {
			logger.warn(String.format(MSG_LOGIN_FORMAT, NamingException.class.getSimpleName(), FortifyStrategyUtils.toLogString(userDomainID, url), FortifyStrategyUtils.toLogString(ne.getMessage())));
			throw ne;
		} catch(SysException e) { 
			logger.warn(String.format(MSG_LOGIN_FORMAT, NamingException.class.getSimpleName(), FortifyStrategyUtils.toLogString(userDomainID, url), FortifyStrategyUtils.toLogString(e.getMessage())));
			throw e;
		}
		catch (Exception e) {
			logger.error(String.format(MSG_LOGIN_FORMAT, Exception.class.getSimpleName(), FortifyStrategyUtils.toLogString(userDomainID, url), FortifyStrategyUtils.toErrString(e)));
			throw e;
		}
	}
    // <-

	// 20210512 Joe Lee
	private static StringBuilder getRoleList(NamingEnumeration<?> answer) throws NamingException, SysException {
		StringBuilder roleList = new StringBuilder();
		while (answer.hasMoreElements()) {
			SearchResult sr = (SearchResult) answer.next();
			Attributes attrs = sr.getAttributes();
			if (null == attrs)
				continue;
			try {
				appendMemberRoles(roleList, attrs.get("memberOf").getAll());
			}
			catch(Exception e) {
				throw new SysException("VTSP Role not found.");
			}
		}
		return roleList;
	}

	private static void appendMemberRoles(StringBuilder roleList, NamingEnumeration<?> memberOf) throws NamingException {
		final String eq = "=";
		final String sp = ",";
		while (memberOf.hasMoreElements()) {
			String member = (String) memberOf.next();
			int a = member.indexOf(eq);
			int b = member.indexOf(sp);
			roleList.append("," + member.substring(a + eq.length(), b > -1 ? b : member.length()));
		}
	}

	private static String getRemainExpiryDate(LoginConfig lc, LdapContext ctx, String userID) throws NamingException {
		// winADPswdWarningDay is empty or -1 will ignore the credentials expire warning checking
		if (StringUtils.isBlank(lc.getWinADPasswordWarningDay()) || "-1".equals(lc.getWinADPasswordWarningDay()))
			return null;

		return LoginHelper.hasPasswordExpireWarning(lc, ctx, userID);
	}

	private static String getErrorMsgCode(String errorMessage, String userDomainID) {
		ErrorMsgCode errorMsgCode = ErrorMsgCode.test(errorMessage);
		logger.info(String.format(MSG_USER_FORMAT, FortifyStrategyUtils.toLogString(userDomainID), " login failed (" + errorMsgCode.value + ")."));

		if (ErrorMsgCode.LE999 == errorMsgCode)
			logger.info("Return msg: " + FortifyStrategyUtils.toLogString(errorMessage));

		return errorMsgCode.key;
	}

	private enum ErrorMsgCode {
		  LE000(LoginConstants.RC_USERID_OR_SECRET_INCORRECT, "User ID or Password incorrect", "525", "52e", "successful bind must be completed")
		, LE001(LoginConstants.RC_ACCOUNT_EXPIRED, "Account expired", "701")
		, LE002(LoginConstants.RC_SECRET_EXPIRED, "Password expired", "532")
		, LE003(LoginConstants.RC_RESET_SECRET, "Must reset password", "773")
		, LE004(LoginConstants.RC_USER_DISABLED, "User disabled", "533")
		, LE005(LoginConstants.RC_USER_LOCKOUT, "User lockout", "775")
		, LE006(LoginConstants.RC_LOGON_DENIED_AT_THIS_TIME, "Logon denied at this time", "530")
		, LE999(LoginConstants.RC_OTHER_ERROR, "Other error")
		;

		public final String key;
		public final String value;
		private String[] args;

		private ErrorMsgCode(String key, String value, String... args) {
			this.key = key;
			this.value = value;
			this.args = args;
		}

		public static ErrorMsgCode test(final String text) {
			return Stream.of(ErrorMsgCode.values()).filter(o -> null != o.args).filter(o -> Stream.of(o.args).anyMatch(s -> text.indexOf(s) > -1)).findFirst().orElse(LE999);
		}
	}

	private enum LoginMsgCode {
		  START("login attempt.")
		, SUCCESS("login success.")
		, EXPIRED("login warning (Password expire warning). Remain Expiry Date: ")
		, FAILED("login failed (authentication failed).")
		;

		public final String msg;

		private LoginMsgCode(String msg) {
			this.msg = msg;
		}
	}
    // <-
}
