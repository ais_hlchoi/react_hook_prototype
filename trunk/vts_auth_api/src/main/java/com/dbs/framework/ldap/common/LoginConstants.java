package com.dbs.framework.ldap.common;

public class LoginConstants {

	private LoginConstants() {
	}

	// Return codes
	public static final String RC_SUCCESS = "SUCCESS";
	public static final String RC_USERID_OR_SECRET_INCORRECT = "RC_USERID_OR_SECRET_INCORRECT";
	public static final String RC_SECRET_WARNING = "SECRET_WARNING:";
	public static final String RC_ACCOUNT_EXPIRED = "ACCOUNT_EXPIRED";
	public static final String RC_SECRET_EXPIRED = "SECRET_EXPIRED";
	public static final String RC_RESET_SECRET = "RESET_SECRET";
	public static final String RC_USER_DISABLED = "USER_DISABLED";
	public static final String RC_USER_LOCKOUT = "USER_LOCKOUT";
	public static final String RC_LOGON_DENIED_AT_THIS_TIME = "LOGON_DENIED_AT_THIS_TIME";
	public static final String RC_OTHER_ERROR = "OTHER_ERROR";
	public static final String RC_AD_SERVER_ERROR = "AD_SERVER_ERROR";
	public static final String RC_MUST_ENABLE_SSL = "MUST_ENABLE_SSL";
	public static final String RC_SECRET_POLICY_VIOLATE = "SECRET_POLICY_VIOLATE";
	public static final String RC_NO_SUCH_USER = "NO_SUCH_USER";
	public static final String RC_ROLE_NOT_EXISTS = "NO_ROLE";

	// Return keys
	public static final String RK_ERROR = "err";
	public static final String RK_RESULT = "result";
	public static final String RK_ROLE_LIST = "roleList";
	public static final String RK_EXPIRY_DATE = "remainExpiryDate";

	// System variables
	public static final String SYS_SSL_TRUST_STORE = "javax.net.ssl.trustStore";
}
