package com.dbs.framework.ldap.helper;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.filter.EqualsFilter;

import com.dbs.framework.ldap.config.LoginConfig;
import com.dbs.util.FortifyStrategyUtils;

public class LoginHelper {

	private LoginHelper() {
		// for SonarQube scanning purpose
	}

	private static Logger logger = LoggerFactory.getLogger(LoginHelper.class);

	// AD attribute: userAccountControl (in hex)
	private static final int DONT_EXPIRE_PASSWORD = 0x10000; // 65536

	/**
	 * has credentials expire warning
	 * @param ctx
	 * @param userID
	 * @return <no. of remain expire date> if has warning, otherwise "" (empty string)
	 * @throws NamingException
	 */
	public static String hasPasswordExpireWarning(LoginConfig lc, LdapContext ctx, String userID) throws NamingException {

		// Convert the winADSecretWarningDay in mills second
		Long pwdAlertDateInMillis = Long.parseLong(lc.getWinADPasswordWarningDay());
		pwdAlertDateInMillis = pwdAlertDateInMillis * 86400 * 10000000; // 86400=24*60*60 ; 10000000=in millis second

		Attributes attrs = ctx.getAttributes(lc.getWinADServerDC());

		// Get the number of days of credentials expire from AD server
		Long pwdAge = Long.parseLong(attrs.get("maxPwdAge").get().toString());
		logger.debug("Password Age (Days): " + ((pwdAge / -86400) / 10000000)); // stored as -ve number of hundred nanosecond intervals Eg. 10^7

		GregorianCalendar win32Epoch = new GregorianCalendar(1601, Calendar.JANUARY, 1);
		Long win32EpochInMillis = win32Epoch.getTimeInMillis();

		GregorianCalendar currentDate = new GregorianCalendar();
		Long currentDateInMillis = currentDate.getTimeInMillis();

		Long todayDateInMillis = (currentDateInMillis - win32EpochInMillis) * 10000;
		logger.debug("today's Date: " + displayWin32Date(todayDateInMillis.toString()));

		SearchControls searchCtls = new SearchControls();
		String[] returnedAtts = { "pwdLastSet", "userAccountControl" };
		searchCtls.setReturningAttributes(returnedAtts);
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		EqualsFilter filter = new EqualsFilter("sAMAccountName", userID);
		String searchBase = lc.getWinADServerDC();
		String searchFilter = filter.toString();
		NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
		while (answer.hasMoreElements()) {
			SearchResult sr = answer.next();
			logger.debug("AD search find:" + sr.getName());
			attrs = sr.getAttributes();
			if (attrs != null) {
				try {
					TimeZone tz = TimeZone.getDefault();
					logger.debug("default timezone: " + tz.getDisplayName() + " offset:" + tz.getRawOffset());

					// get AD user attribute: last change credentials date time
					Long pwdLastSet = Long.parseLong(attrs.get("pwdLastSet").get().toString());
					logger.debug("-pwdLastSet: " + displayWin32Date(pwdLastSet.toString()));

					// add the timezone to the pwdLastSet
					Long tzRawOffsetInMillis = Long.valueOf(tz.getRawOffset());
					tzRawOffsetInMillis *= 10000;
					pwdLastSet += tzRawOffsetInMillis;
					logger.debug("-pwdLastSet(" + tz.getDisplayName() + "): " + displayWin32Date(pwdLastSet.toString()));

					// Calculate expire date for login user
					Long expiryDateInMillis = pwdLastSet + (pwdAge * -1);
					logger.debug("-expiry Date: " + displayWin32Date(expiryDateInMillis.toString()));

					// Calculate the credentials warning date for user
					Long alertDateInMillis = expiryDateInMillis - pwdAlertDateInMillis;
					logger.debug("-warning Date: " + displayWin32Date(alertDateInMillis.toString()));

					// get AD user attribute: userAccountControl (check is credentials never expire)
					Long userAccountControl = Long.parseLong(attrs.get("userAccountControl").get().toString());
					boolean pwdNeverExpire = (userAccountControl & DONT_EXPIRE_PASSWORD) == DONT_EXPIRE_PASSWORD;
					logger.debug("-userAccountControl: " + userAccountControl + " pwdNeverExpire:" + pwdNeverExpire);

					if (pwdNeverExpire) {
						return "";
					} else {
						Long remainExpiryDateInMillis = expiryDateInMillis - todayDateInMillis;
						Long remainExpiryDate = remainExpiryDateInMillis / 86400 / 10000000; // in day
						logger.debug("remainExpiryDate:" + remainExpiryDate);

						if (todayDateInMillis > alertDateInMillis) {
							// current date later than warning date
							return remainExpiryDate.toString();
						} else {
							return "";
						}
					}
				} catch (NullPointerException e) {
					logger.error("Unable to find attribute (NullPointerException): " + FortifyStrategyUtils.toErrString(e));
				}
			}
		}
		return "";
	}

	/**
	 * display win32 date by adding 1601-01-01
	 * @param Win32FileTime
	 * @return
	 */
	private static String displayWin32Date(String win32FileTime) {
		GregorianCalendar win32Epoch = new GregorianCalendar(1601, Calendar.JANUARY, 1);
		Long lWin32Epoch = win32Epoch.getTimeInMillis();
		Long lWin32FileTime = Long.parseLong(win32FileTime);
		return (DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format((lWin32FileTime / 10000) + lWin32Epoch));
	}
}
