package com.dbs.framework.exception;

public class SysException extends Exception {

	private static final long serialVersionUID = 1L;

	public SysException() {
		super();
	}

	public SysException(String message) {
		super(message);
	}

	public SysException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public SysException(Exception e) {
		super(e);
	}

}