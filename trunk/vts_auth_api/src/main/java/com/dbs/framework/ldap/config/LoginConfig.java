package com.dbs.framework.ldap.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.util.FortifyStrategyUtils;

public class LoginConfig {

	private Logger logger = LoggerFactory.getLogger(LoginConfig.class);

	public final String LOGIN_CONFIG_FILE = "LoginAD_%s.properties";
	private String enableSSL;
	private String sslTrustStorePath;
	private String winADServerURL;
	private String winADDomainList;
	private String securityAuthen;
	private String winADServerDomain;
	private String winADServerDC;
	private String winADAuthenticateFlag;
	private String enableChgnPwdFunct;
	private String winADSecretWarningDay;
	private String bimsProxyKeystore;
	
	public LoginConfig(String env) {
		Properties loginConfigProperties = new Properties();
		InputStream loginConfigInputStream = null;
		
		loginConfigInputStream = LoginConfig.class.getClassLoader().getResourceAsStream("config/" + String.format(LOGIN_CONFIG_FILE,env));
		if (loginConfigInputStream != null) {
			try {
				loginConfigProperties.load(loginConfigInputStream);
			} catch (IOException e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
			} finally {
				try {
					loginConfigInputStream.close();
				} catch (IOException e) {
					logger.error(FortifyStrategyUtils.toErrString(e));
				}
			}
		} else {
			logger.error("cannot find property file for ApplicationConfig: " + FortifyStrategyUtils.toLogString(LOGIN_CONFIG_FILE));
		}

		enableSSL = loginConfigProperties.getProperty("enableSSL", "");
		sslTrustStorePath = loginConfigProperties.getProperty("sslTrustStorePath", "");
		logger.error("sslTrustStorePath: " + FortifyStrategyUtils.toLogString(sslTrustStorePath));
		winADServerURL = loginConfigProperties.getProperty("winADServerURL", "");
		winADDomainList = loginConfigProperties.getProperty("winADDomainList", "");
		securityAuthen = loginConfigProperties.getProperty("security_authen", "");
		winADServerDomain = loginConfigProperties.getProperty("winADServerDomain", "");
		winADServerDC = loginConfigProperties.getProperty("winADServerDC", "");
		winADAuthenticateFlag = loginConfigProperties.getProperty("winADAuthenticateFlag", "");
		enableChgnPwdFunct = loginConfigProperties.getProperty("enableChgnPwdFunct", "");
		winADSecretWarningDay = loginConfigProperties.getProperty("winADSecretWarningDay", "");
		bimsProxyKeystore = loginConfigProperties.getProperty("bims_proxy_keystore");

		Map<String, String> map = new LinkedHashMap<>();
		map.put("enableSSL", enableSSL);
		map.put("sslTrustStorePath", sslTrustStorePath);
		map.put("winADServerURL", winADServerURL);
		map.put("security_authen", securityAuthen);
		map.put("winADServerDomain", winADServerDomain);
		map.put("winADServerDC", winADServerDC);
		map.put("winADAuthenticateFlag", winADAuthenticateFlag);
		map.put("enableChgnPwdFunct", enableChgnPwdFunct);
		map.put("winADSecretWarningDay", winADSecretWarningDay);
		map.put("bimsProxyKeystore", bimsProxyKeystore);

		logger.info("init done. " + ("prod".equalsIgnoreCase(env) ? "" : FortifyStrategyUtils.toLogString(map.entrySet().stream().map(o -> String.format("%s: %s", o.getKey(), o.getValue())).toArray())));
	}

	public boolean isEnableSSL() {
		return "Y".equalsIgnoreCase(enableSSL);
	}

	public String getSslTrustStorePath() {
		return sslTrustStorePath;
	}

	public String getWinADServerURL() {
		return winADServerURL;
	}

	public String getWinADDomainList() {
		return winADDomainList;
	}

	public String getSecurityAuthen() {
		return securityAuthen;
	}

	public String getWinADServerDomain() {
		return winADServerDomain;
	}

	public String getWinADServerDC() {
		return winADServerDC;
	}

	public boolean isWinADAuthenticate() {
		return "N".equalsIgnoreCase(winADAuthenticateFlag);
	}

	public String getWinADPasswordWarningDay() {
		return winADSecretWarningDay;
	}

	public String getBimsProxyKeystore() {
		return bimsProxyKeystore;
	}
}
