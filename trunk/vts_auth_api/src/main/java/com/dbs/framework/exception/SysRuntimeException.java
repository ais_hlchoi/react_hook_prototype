package com.dbs.framework.exception;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.framework.constant.ResponseCode;

public class SysRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -463494052776277179L;

	private final ResponseCode responseCode;
	private final String responseDesc;
	private final Map<String, String> param = new HashMap<>();

	public SysRuntimeException(ResponseCode responseCode) {
		this.responseCode = responseCode;
		this.responseDesc = null;
	}

	public SysRuntimeException(ResponseCode responseCode, String responseDesc) {
		super(responseDesc);
		this.responseCode = responseCode;
		this.responseDesc = responseDesc;
	}

	public SysRuntimeException(ResponseCode responseCode, Throwable cause) {
		super(cause);
		this.responseCode = responseCode;
		this.responseDesc = null;
	}

	public SysRuntimeException(ResponseCode responseCode, String responseDesc, Throwable cause) {
		super(responseDesc, cause);
		this.responseCode = responseCode;
		this.responseDesc = responseDesc;
	}

	public SysRuntimeException(Throwable cause) {
		super(cause);
		if (cause instanceof SysRuntimeException) {
			SysRuntimeException t = (SysRuntimeException) cause;
			this.responseCode = ResponseCode.getOrElse(t.getResponseCode());
			this.responseDesc = t.getResponseDesc();
		} else {
			this.responseCode = ResponseCode.E1999;
			this.responseDesc = cause.getMessage();
		}
	}

	public SysRuntimeException put(String key, String value) {
		if (StringUtils.isNotBlank(key))
			param.put(key, value);
		return this;
	}

	public SysRuntimeException putAll(Map<String, String> map) {
		if (MapUtils.isNotEmpty(map))
			param.putAll(map);
		return this;
	}

	public String getResponseCode() {
		return responseCode.name();
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public Map<String, String> getParam() {
		return param;
	}

	public Integer getStatus() {
		return -1;
	}

	public Object getObject() {
		return MapUtils.isEmpty(getParam()) ? null : getParam().get(StringUtils.uncapitalize(SysRuntimeException.class.getEnclosingMethod().getName().substring(3)));
	}
}
