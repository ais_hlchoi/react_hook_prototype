package com.dbs.framework.constant;

public enum RestEntity {

	  RESPONSE_CODE("responseCode")
	, RESPONSE_DESC("responseDesc")
	, STATUS("status")
	, OBJECT("object")
	;

	public final String key;

	private RestEntity(String key) {
		this.key = key;
	}
}
