package com.dbs.framework.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.dbs.framework.aspect.annotation.Login;
import com.dbs.framework.aspect.annotation.Logout;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.OperationLogModel;
import com.dbs.sys.model.UserForm;
import com.dbs.sys.model.UserLoginModel;
import com.dbs.sys.model.UserLogoutModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.JointPointUtils;
import com.dbs.util.JointPointUtils.JointPointProps;
import com.dbs.util.OperationLogUtils;
import com.dbs.util.OperationLogUtils.OperationLogStatus;
import com.dbs.util.RequestHeaderUtils;

@Aspect
@ConfigurationProperties("interceptor")
@Component
public class LoginAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HttpServletRequest request;

	@Value("${sys.log.url}")
	private String sysLogApi;

	@Pointcut("execution(* com.dbs.*..controller.*..*(..))")
	public void controllerPoint() {
		// do nothing because of point cut
	}

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.Login)")
	public void loginPoint() {
		// do nothing because of point cut
	}

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.Logout)")
	public void logoutPoint() {
		// do nothing because of point cut
	}

	@Around("controllerPoint() && (loginPoint() || logoutPoint())")
	public Object intercept(ProceedingJoinPoint joinPoint) {
		JointPointProps props = new JointPointProps(joinPoint);

		Action action = Action.getOrElse(props.getMethod());
		if (Action.UNKNOWN == action) {
			return JointPointUtils.proceedJoinPoint(joinPoint);
		}

		UserForm form = null;
		String userId = null;

		for (Object o : props.getData().getArgs()) {
			if (o != null && (o instanceof UserLoginModel || o instanceof UserLogoutModel)) {
				form = (UserForm) o;
				userId = form.getOneBankId();
				break;
			}
		}

		OperationLogModel log = null;

		try {
			log = OperationLogUtils.create(getClearUserForm(form), userId, RequestHeaderUtils.getClientIPAddress(request), action.name());
			log.setOperationLogId(OperationLogUtils.insert(log, sysLogApi));
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}

		Object result = JointPointUtils.proceedJoinPoint(joinPoint);

		try {
			if (null == log) {
				logger.info("No operation log is created.");
			} else if (null == log.getOperationLogId()) {
				logger.info("Operation log is not existed.");
			} else {
				if (null == result) {
					log.setResponseCode(ResponseCode.M1000.name());
					log.setResponseDesc(null);
					log.setStatus(OperationLogStatus.SUCCESS.name());
					logger.info("No result is returned.");
				} else {
					log.setResponseCode(getOrElse(((RestEntity) result).getResponseCode()));
					log.setResponseDesc(getOrElse(((RestEntity) result).getResponseDesc()));
					log.setStatus(OperationLogStatus.getOrElse(log.getResponseCode()).name());
				}
				log.setResponseTime(new Date());
				OperationLogUtils.update(log, sysLogApi);
			}
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}

		return result;
	}

	private Map<String, Object> getClearUserForm(Object o) {
		Map<String, Object> map = ClassPathUtils.toObjectMap(o);
		if (MapUtils.isEmpty(map))
			return map;
		String[] excludes = new String[] { "password" };
		Stream.of(excludes).filter(key -> map.containsKey(key)).forEach(key -> map.remove(key));
		return map;
	}

	private String getOrElse(String value) {
		return getOrElse(value, null);
	}

	private String getOrElse(String value, String valueIfNull) {
		return StringUtils.isBlank(value) ? valueIfNull : value;
	}

	private enum Action {
		  LOGIN(Login.class)
		, LOGOUT(Logout.class)
		, UNKNOWN(null)
		;
		private Class<? extends Annotation> cls;
		private Action(Class<? extends Annotation> cls) {
			this.cls = cls;
		}
		public Class<? extends Annotation> getCls() {
			return cls;
		}
		public static Action getOrElse(Method method) {
			return Stream.of(Action.values()).filter(o -> null != o.getCls()).filter(o -> method.isAnnotationPresent(o.getCls())).findFirst().orElse(UNKNOWN);
		}
	}

	@EventListener
	public void startup(ContextRefreshedEvent event) {
		logger.info(String.format("%s", ClassPathUtils.getName()));
	}
}
