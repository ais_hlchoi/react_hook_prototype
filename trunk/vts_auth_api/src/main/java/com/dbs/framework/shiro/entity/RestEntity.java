package com.dbs.framework.shiro.entity;

import com.dbs.framework.constant.ResponseCode;

public class RestEntity implements EntityBase {

	private String responseCode;
	private String responseDesc;
	private Integer status;
	private Object object;
	private Integer totalRows;

	public RestEntity() {
	}

	public RestEntity(Integer status, String responseCode, String msg, Object object, Integer totalRows) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
		this.object = object;
		this.totalRows = totalRows;
	}

	public RestEntity(Integer status, String responseCode, String msg, Object object) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
		this.object = object;
	}

	public RestEntity(Integer status, String responseCode, String msg) {
		super();
		this.status = status;
		this.responseDesc = msg;
		this.responseCode = responseCode;
	}

	public RestEntity(Integer status, String responseCode) {
		super();
		this.status = status;
		this.responseCode = responseCode;
	}

	public static RestEntity success(Object object, Integer totalRows) {
		return new RestEntity(1, ResponseCode.M1000.getCode(), null, object, totalRows);
	}

	public static RestEntity success(Object object) {
		return new RestEntity(1, ResponseCode.M1000.getCode(), null, object);
	}

	public static RestEntity success() {
		return new RestEntity(1, ResponseCode.M1000.getCode());
	}

	public static RestEntity failed() {
		return new RestEntity(-1, "FAILED");
	}

	public static RestEntity failed(String msg) {
		return new RestEntity(-1, ResponseCode.E1999.getCode(), msg);
	}

	public boolean isFailed() {
		return -1 == this.status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public Integer getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}

	public static RestEntity sqlInsertSuccess(String msg) {
		return success(msg);
	}

	public static RestEntity processSuccess(Object result) {
		return success(result);
	}
}
