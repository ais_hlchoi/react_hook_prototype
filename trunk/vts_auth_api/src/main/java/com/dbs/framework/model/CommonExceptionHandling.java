package com.dbs.framework.model;

import org.apache.commons.lang.StringUtils;

import com.dbs.util.InternationalizationUtil;

public class CommonExceptionHandling {

	private CommonExceptionHandling() {
		// for SonarQube scanning purpose
	}

	public static String formatError(String err) {
		if (StringUtils.isBlank(err)) {
			err = "SYS_ERR";
		}
		err = InternationalizationUtil.getMsg(err);
		err = getOracleError(err, "org.hibernate.service.spi.ServiceException:");
		err = getOracleError(err, "nested exception is");
		return err;
	}

	public static String getOracleError(String text, String search) {
		if (StringUtils.isBlank(text) || StringUtils.isBlank(search))
			return text;

		int index = text.indexOf(search);
		if (index < 0)
			return text;

		String sp = ":";
		String oracleError = text.substring(index, text.length());
		if (StringUtils.isNotBlank(oracleError)) {
			index = oracleError.indexOf(sp);
			if (index > 0) {
				oracleError = oracleError.substring(index + sp.length(), oracleError.length());
			}
			return oracleError;
		}

		return text;
	}
}
