package com.dbs.sys.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dbs.framework.ldap.config.LoginConfig;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.session.util.SessionCodeUtils;
import com.dbs.sys.model.UserLoginModel;
import com.dbs.sys.model.UserLogoutModel;
import com.dbs.sys.service.UserService;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.LdapUserUtils;
import com.dbs.util.LdapUserUtils.UserPropsRequired;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserServiceImpl implements UserService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public HttpServletRequest request;

	@Value("${ldapOn}")
	private String ldapOn;

	@Value("${local.user}")
	private String localUser;

	@Value("${local.code}")
	private String localCode;

	@Value("${env}")
	private String env;
	
	private static LoginConfig lc;

	public void login(UserLoginModel model, RestEntity rest) {
		if (lc == null)
			lc = new LoginConfig(env);
		List<String> msg = new ArrayList<>();
		Map<String, String> resultMap = LdapUserUtils.checkUser(lc, model, localUser, localCode, ldapOn, msg);
		resultMap.put("sessCode", SessionCodeUtils.getFactory().create(16));
		resultMap.put("dateTime", String.valueOf(Calendar.getInstance().getTimeInMillis()));
		logger.info(String.format("Success to check user %s", FortifyStrategyUtils.toLogString(resultMap.get("sessCode"), resultMap.get(UserPropsRequired.ROLE_LIST.name()), resultMap.get(UserPropsRequired.RESULT.name()))));

		if (null == rest)
			return;

		rest.setResponseDesc(StringUtils.join(msg, ""));
		rest.setObject(new ObjectMapper().convertValue(resultMap, JsonNode.class));
	}

	public void logout(UserLogoutModel model, RestEntity rest) {
		// do nothing when logout
	}
}
