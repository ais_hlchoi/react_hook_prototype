package com.dbs.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.Login;
import com.dbs.framework.aspect.annotation.Logout;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.UserLoginModel;
import com.dbs.sys.model.UserLogoutModel;
import com.dbs.sys.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AuthController {

	@Autowired
	private UserService userService;

	@Login
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public RestEntity login(@RequestBody UserLoginModel model) {
		RestEntity result = RestEntity.success();
		userService.login(model, result);
		return result;
	}

	@Logout
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public RestEntity logout(@RequestBody UserLogoutModel model) {
		RestEntity result = RestEntity.success();
		userService.logout(model, result);
		return result;
	}
}
