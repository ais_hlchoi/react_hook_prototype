package com.dbs.sys.service;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.UserLoginModel;
import com.dbs.sys.model.UserLogoutModel;

public interface UserService {

	void login(UserLoginModel model, RestEntity rest);

	void logout(UserLogoutModel model, RestEntity rest);
}
