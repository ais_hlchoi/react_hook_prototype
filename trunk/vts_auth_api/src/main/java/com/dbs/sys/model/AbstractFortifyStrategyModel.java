package com.dbs.sys.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractFortifyStrategyModel {

	@JsonIgnore
	private String applCde;

	@JsonIgnore
	private String lvCde;

	@JsonIgnore
	private String officCde;
}
