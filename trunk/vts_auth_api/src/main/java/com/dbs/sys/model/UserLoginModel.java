package com.dbs.sys.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoginModel extends AbstractFortifyStrategyModel implements UserForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String username;

	@JsonInclude
	private String password;

	public String getOneBankId() {
		return getUsername();
	}
}
