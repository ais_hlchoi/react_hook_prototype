package com.dbs.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.framework.ldap.LoginAd;
import com.dbs.framework.ldap.common.LoginConstants;
import com.dbs.framework.ldap.config.LoginConfig;
import com.dbs.framework.ldap.util.LoginUtil;
import com.dbs.sys.model.UserLoginModel;

public class LdapUserUtils {

	private LdapUserUtils() {
		// for SonarQube scanning purpose
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String MSG_FAIL_FORMAT = "%s%s";
	
	private static final String USER_LOGIN_RESULT = LoginConstants.RC_OTHER_ERROR;
	private static final String USER_ROLE_ANONYMOUS = "ANONYMOUS";

	public static Map<String, String> checkUser(final LoginConfig lc, final UserLoginModel model, final String localUser, final String localCode, final String ldapOn, final List<String> log) {
		LdapUserUtils u = new LdapUserUtils();
		UserProps o = BooleanUtils.toBoolean(ldapOn) ? u.checkLdapUser(lc, model.getUsername(), model.getPassword()) : u.checkLocalUser(model.getUsername(), model.getPassword(), localUser, localCode);

		if (CollectionUtils.isNotEmpty(o.getLog()) && null != log)
			log.addAll(o.getLog());

		return o.getData();
	}

	protected UserProps checkLdapUser(LoginConfig lc, String user, String code) {
		String msg = nvl(LoginUtil.validate(user, code), null);
		Assert.isNull(msg, msg);

		Map<String, String> data = null;
		try {
			data = LoginAd.login(lc, user, code);
			Assert.notNull(data, "Empty returned");
		} catch (Exception e) {
			msg = String.format(MSG_FAIL_FORMAT, "", FortifyStrategyUtils.toErrString(e));
			logger.error(msg);
			Assert.isNull(msg, msg);
		}

		UserProps o = new UserProps(data);
		if (LoginConstants.RC_SUCCESS.equalsIgnoreCase(o.getResult()))
			return o;

		o.add(LoginUtil.getLoginADErrorMessage(o.getResult()));
		if (LoginConstants.RC_SECRET_WARNING.equals(o.getResult())) {
			logger.warn(String.format(MSG_FAIL_FORMAT, "Password Warning:", FortifyStrategyUtils.toLogString(o.getMsg())));
			o.put("pwdwarn", "Y");
			return o;
		}
		msg = String.format(MSG_FAIL_FORMAT, "", o.getMsg());
		logger.error(FortifyStrategyUtils.toLogString(msg));
		Assert.isNull(msg, msg);
		return o;
	}

	protected UserProps checkLocalUser(String user, String code, String localUser, String localCode) {
		logger.info(String.format("Check to login to Local Properties %s", FortifyStrategyUtils.toLogString(localUser, user)));
		UserProps o = new UserProps(LoginConstants.RC_SUCCESS);
		Assert.notBlank(localUser, "No user in list.");
		Assert.isTrue(StringUtils.isBlank(localCode) || localCode.equals(code), "Login Failed.");
		String sp = "|";
		for (String conf : localUser.split(",")) {
			if (conf.startsWith(user + sp)) {
				o.put("roleList", conf.substring(conf.indexOf(sp) + sp.length()));
				logger.info(String.format("User is found %s", FortifyStrategyUtils.toLogString(conf.substring(0, conf.indexOf(sp)), o.getRoleList())));
				break;
			}
			logger.info(String.format("User not match %s", FortifyStrategyUtils.toLogString(conf)));
		}
		return o;
	}

	protected String nvl(String value, String valueIfNull) {
		return StringUtils.isBlank(value) ? valueIfNull : value;
	}

	public enum UserPropsRequired {
		  RESULT("result", USER_LOGIN_RESULT)
		, ROLE_LIST("roleList", USER_ROLE_ANONYMOUS)
		;
		private final String key;
		private final String value;
		private UserPropsRequired(String key, String value) {
			this.key = key;
			this.value = value;
		}
	}

	private class UserProps {
		private Map<String, String> data = new HashMap<>();
		private List<String> log = new ArrayList<>();

		public UserProps(Map<String, String> data) {
			put(data);
		}
		public UserProps(String result) {
			put("result", result);
		}
		public Map<String, String> getData() {
			Stream.of(UserPropsRequired.values()).filter(o -> !data.containsKey(o.key) || StringUtils.isBlank(data.get(o.key))).forEach(o -> put(o.key, o.value));
			return data;
		}
		public List<String> getLog() {
			return log;
		}
		public String getMsg() {
			return StringUtils.join(getLog(), "");
		}
		public void put(Map<String, String> data) {
			if (null != data)
				this.data = data;
		}
		public void put(String key, String value) {
			data.put(key, value);
		}
		public void add(String msg) {
			log.add(msg);
		}
		public String get(String key) {
			return MapUtils.isEmpty(getData()) ? null : getData().get(key);
		}
		public String getResult() {
			return get("result");
		}
		public String getRoleList() {
			return get("roleList");
		}
	}
}
