package com.dbs.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class FortifyStrategyUtils {

	private FortifyStrategyUtils() {
		// for SonarQube scanning purpose
	}

	public static String toMaskString(String text) {
		return ClassPathUtils.toMaskString(text);
	}

	public static String toCharString(String text) {
		return ClassPathUtils.toCharString(text, 32, 127);
	}

	public static String toPathString(String text) {
		return ClassPathUtils.toCharString(text);
	}

	public static String toLogString(Object... args) {
		List<String> list = new ArrayList<String>();
		for (Object o : args) {
			if (null == o)
				list.add("");
			else if (o instanceof String)
				list.add((String) o);
			else if (o instanceof BigDecimal)
				list.add(((BigDecimal) o).toString());
			else if (o instanceof Integer || o instanceof Long || o instanceof Double)
				list.add(String.valueOf(o));
			else if (o instanceof Date)
				list.add(new SimpleDateFormat("yyyy-MM-dd HH:mi:ss").format(o));
			else
				list.add(ClassPathUtils.toJSONString(o));
		}
		return ClassPathUtils.toLogString(list.toArray(new String[list.size()]));
	}

	public static String toErrString(Throwable cause) {
		return ClassPathUtils.toLogString(StringUtils.isBlank(cause.getMessage()) ? cause.toString() : cause.getMessage());
	}

	public static <T> T fromParamMap(Map<String, Object> object, Class<T> cls) {
		return ClassPathUtils.fromObjectMap(object, cls);
	}

	public static Map<String, Object> toParamMap(Object object) {
		return ClassPathUtils.toObjectMap(object);
	}

	public static Object toParamVar(Object object) {
		return ClassPathUtils.toObjectVar(object);
	}
}
