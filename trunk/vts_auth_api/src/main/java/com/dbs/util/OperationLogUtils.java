package com.dbs.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.LoggerFactory;

import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.constant.RestEntity;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.sys.model.OperationLogModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public class OperationLogUtils {

	private OperationLogUtils() {
		// for SonarQube scanning purpose
	}

	public static OperationLogModel create(Object object, String userId, String clientIp, String apiName) {
		String hostIp = null;
		try {
			hostIp = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			LoggerFactory.getLogger(OperationLogUtils.class).error(String.format("Fail to get host address when %s operation log", ClassPathUtils.getName().split(".")[1]));
		}

		OperationLogModel log = new OperationLogModel();
		log.setUserId(userId);
		log.setOperationType(apiName);
		log.setStatus(OperationLogStatus.PENDING.name());
		log.setServiceAddress(hostIp);
		log.setLoginIpAddress(clientIp);
		log.setResponseCode(null);
		log.setResponseDesc(null);
		log.setOperationLogId(null);
		log.setOperationContentObject(object);
		return log;
	}

	public static OperationLogModel parse(OperationLogModel model) {
		if (null == model)
			return model;

		if (StringUtils.isBlank(model.getOperationContent()) && null != model.getOperationContentObject())
			model.setOperationContent(ClassPathUtils.toJSONString(Collections.singletonMap("args", model.getOperationContentObject())));

		return model;
	}

	public static Integer insert(final OperationLogModel model, final String url) throws JsonProcessingException {
		if (StringUtils.isBlank(url) || null == model)
			return null;

		if (null != model.getOperationLogId())
			return update(model, url);

		String action = ClassPathUtils.getName().split("\\.")[1];
		String api = String.format("%s/%s", url, action);
		return new OperationLogUtils().save(model, api, action);
	}

	public static Integer update(final OperationLogModel model, final String url) throws JsonProcessingException {
		if (StringUtils.isBlank(url) || null == model)
			return null;

		if (null == model.getOperationLogId())
			return insert(model, url);

		String action = ClassPathUtils.getName().split("\\.")[1];
		String api = String.format("%s/%s", url, action);
		return new OperationLogUtils().save(model, api, action);
	}

	protected Integer save(OperationLogModel model, String api, String action) {
		Integer id = null;
		try {
			JsonNode node = ApiUtils.post(api, model, null);
			ApiUtils.validate(node);
			JsonNode object = node.get(RestEntity.OBJECT.key);
			if (null == object || !NumberUtils.isCreatable(object.asText()))
				return id;
			id = NumberUtils.createInteger(object.asText());
		} catch (JsonProcessingException e) {
			throw new SysRuntimeException(ResponseCode.E1999, String.format("Error found when %s operation log %s", ClassPathUtils.getName().split(".")[1], FortifyStrategyUtils.toLogString(api, action)));
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getValue(Object object, String key, Class<T> cls) {
		if (null == object || StringUtils.isBlank(key) || null == cls)
			return null;

		Object value = object instanceof String ? object : ClassPathUtils.toObjectMap(object).get(key);

		if (null == value)
			return null;

		if (cls.isAssignableFrom(Integer.class))
			return object instanceof Integer ? (T) object : NumberUtils.isCreatable(String.valueOf(value)) ? (T) NumberUtils.createInteger(String.valueOf(value)) : null;

		return (T) value;
	}

	public enum OperationLogStatus {
		  PENDING
		, SUCCESS
		, FAILED
		;

		public static OperationLogStatus getOrElse(String text) {
			if (StringUtils.isBlank(text))
				return PENDING;

			if (ResponseCode.M1000.name().equalsIgnoreCase(text))
				return SUCCESS;

			try {
				return OperationLogStatus.valueOf(text.toUpperCase());
			} catch (IllegalArgumentException e) {
				return FAILED;
			}
		}
	}
}
