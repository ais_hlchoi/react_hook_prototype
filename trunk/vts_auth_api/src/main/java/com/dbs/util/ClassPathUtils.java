package com.dbs.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClassPathUtils extends org.apache.commons.lang3.ClassPathUtils {

	public static String getName() {
		String sp = ".";
		String className = Thread.currentThread().getStackTrace()[2].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		int index = className.lastIndexOf(sp);
		if (index >= 0) {
			className = className.substring(index + sp.length());
		}
		return String.format("%s.%s", className, methodName);
	}

	public static String getShortName(final String path) {
		if (StringUtils.isBlank(path))
			return "";

		String[] args = path.split("\\.");
		int n = args.length;
		return IntStream.range(0, n).boxed().collect(Collectors.toMap(o -> o, o -> args[o])).entrySet().stream().map(o -> o.getKey() < n - 1 ? o.getValue().substring(0, 1).toLowerCase() : o.getValue()).collect(Collectors.joining("."));
	}

	public static String toJSONString(final Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			return e.getMessage();
		}
	}

	public static String toMaskString(final String text) {
		final String s = "*";
		switch (StringUtils.isBlank(text) ? 1 : text.length()) {
		case 1:
			return s;
		case 2:
			return text.substring(0, 1) + s;
		default:
			int l = text.length();
			int m = text.length() / 2;
			int n = Math.min(m / 2, 3);
			return text.substring(0, Math.min(m, 9) - n) + s + text.substring(l - n, l);
		}
	}

	public static String toLogString(final String... args) {
		return String.format("[ %s ]", (null == args || args.length < 1) ? "null" : Stream.of(args).map(ClassPathUtils::toCharString).map(s -> s.replaceAll("(\\r|\\n)+", " ").replaceAll("^\\[\\s+|\\s+\\]$", "")).collect(Collectors.joining(", ")));
	}

	public static String toCharString(final String s) {
		return toCharString(s, 32, 127);
	}

	public static String toCharString(final String s, final int a, final int b) {
		return StringUtils.isBlank(s) ? "" : IntStream.range(0, s.length()).boxed().map(n -> String.valueOf(cleanChar(s.charAt(n), a, b))).collect(Collectors.joining(""));
	}

	private static char cleanChar(final char c, final int a, final int b) {
		return (c >= a || c < b) ? c : '%';
	}

	public static <T> T fromObjectMap(final Map<String, Object> object, final Class<T> cls) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			String json = mapper.writeValueAsString(object);

			ObjectMapper reader = new ObjectMapper();
			reader.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return reader.readValue(json, cls);
		} catch (JsonProcessingException e) {
			return getClassInstance(cls);
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T getClassInstance(Class<T> cls) {
		try {
			return (T) cls.getConstructor(String.class);
		} catch (NoSuchMethodException | SecurityException e1) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toObjectMap(final Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			String json = mapper.writeValueAsString(object);

			ObjectMapper reader = new ObjectMapper();
			reader.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return reader.readValue(json, Map.class);
		} catch (JsonProcessingException e) {
			return new HashMap<>();
		}
	}

	public static Object toObjectVar(final Object object) {
		try {
			Map<String, Object> objectMap = Collections.singletonMap("object", object);
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			String json = mapper.writeValueAsString(objectMap);

			ObjectMapper reader = new ObjectMapper();
			reader.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return reader.readValue(json, Map.class).get("object");
		} catch (JsonProcessingException e) {
			return object;
		}
	}

	public static <T> T getOrElse(T value, T valueIfNull) {
		if (value instanceof String)
			return StringUtils.isBlank((String) value) ? valueIfNull : value;

		return null == value ? valueIfNull : value;
	}
}
