package com.dbs.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RequestHeaderUtils {

	private RequestHeaderUtils() {
		// for SonarQube scanning purpose
	}

	private static final String LOCALHOST_IPV4 = "127.0.0.1";
	private static final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";

	public static String getAuthorization(HttpServletRequest req) {
		HttpServletRequest request = null == req ? getCurrentRequest() : req;
		if (null == request)
			return null;

		return request.getHeader("Authorization");
	}

	public static String getAuthToken(HttpServletRequest req) {
		HttpServletRequest request = null == req ? getCurrentRequest() : req;
		if (null == request)
			return null;

		String header = getAuthorization(request);
		if (header == null || !header.startsWith("Bearer ")) {
			return "";
		}
		return header.substring(7);
	}

	public static String getClientIPAddress(HttpServletRequest req) {
		HttpServletRequest request = null == req ? getCurrentRequest() : req;
		if (null == request)
			return null;

		String ip = null;
		try {
			ip = request.getRemoteAddr();
			for (String name : HttpHeader.getHttpHeaderIpAddress()) {
				String header = request.getHeader(name);
				if (StringUtils.isNotBlank(header) && !"unknown".equalsIgnoreCase(header)) {
					ip = header;
					break;
				}
			}
		} catch (Exception e) {
			// throw java.lang.reflect.InvocationTargetException when run scheduler
		}
		return null == ip || ip.equals(LOCALHOST_IPV6) ? LOCALHOST_IPV4 : ip;
	}

	public static boolean isNotEmptyRequest() {
		return null == getCurrentRequest();
	}

	public static HttpServletRequest getCurrentRequest() {
		HttpServletRequest request = null;
		try {
			RequestAttributes attr = RequestContextHolder.currentRequestAttributes();
			if (attr instanceof NativeWebRequest) {
				request = (HttpServletRequest) ((NativeWebRequest) attr).getNativeRequest();
			} else {
				request = ((ServletRequestAttributes) attr).getRequest();
			}
		} catch (Exception e) {
			// return empty if any exception
		}
		return request;
	}

	public static HttpServletResponse getCurrentResponse() {
		HttpServletResponse response = null;
		try {
			RequestAttributes attr = RequestContextHolder.getRequestAttributes();
			if (attr instanceof NativeWebRequest) {
				response = (HttpServletResponse) ((NativeWebRequest) attr).getNativeResponse();
			}
		} catch (Exception e) {
			// return empty if any exception
		}
		return response;
	}
}
