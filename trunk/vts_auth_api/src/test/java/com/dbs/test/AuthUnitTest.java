package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.sys.model.UserLoginModel;
import com.dbs.sys.model.UserLogoutModel;
import com.dbs.sys.service.UserService;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class AuthUnitTest {

	@Autowired
	private UserService userService;

	@Value("${test.auth.user-code}")
	private String userCode;

	@Value("${test.auth.pass-code}")
	private String passCode;

	@Test
	public void login() {
		UserLoginModel model = new UserLoginModel();
		model.setUsername(userCode);
		model.setPassword(passCode);
		userService.login(model, null);
		System.out.println(String.format("%s: %d <- [ %s ]", ClassPathUtils.getName(), 1, userCode));
	}

	@Test
	public void logout() {
		UserLogoutModel model = new UserLogoutModel();
		model.setUserId(userCode);
		userService.logout(model, null);
		System.out.println(String.format("%s: %d <- [ %s ]", ClassPathUtils.getName(), 1, userCode));
	}
}