Create database and users
=========================
CREATE DATABASE FCDB;
show databases;
CREATE USER 'fcdb' IDENTIFIED BY 'fcdb';
SELECT User FROM mysql.user;
GRANT ALL PRIVILEGES ON *.* TO 'fcdb' IDENTIFIED BY 'fcdb';
GRANT ALL PRIVILEGES ON FCDB.* TO 'fcdb';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'fcdb';


CREATE DATABASE FCDB_READ_ONLY;
show databases;
CREATE USER 'fcdb_read_only' IDENTIFIED BY 'fcdb_read_only';
GRANT SELECT ON fcdb.data_allocations TO 'fcdb_read_only' IDENTIFIED BY 'fcdb_read_only';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'fcdb_read_only';

