/**
 *
 * (C) 2018 The Securities And Futures Commission of Hong Kong. All rights reserved.
 *
 */
package hk.sfc.eds.profilemaintenance.config;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import hk.sfc.eds.profilemaintenance.security.CustomMethodSecurityExpressionHandler;

@Configuration
public class EdsProfileMaintenanceServiceConfiguration {

  /**
   * clientCredentialsResourceDetails.
   *
   * @return ClientCredentialsResourceDetails
   */
  @Bean
  @ConfigurationProperties("security.oauth2.client")
  protected ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
    return new ClientCredentialsResourceDetails();
  }

  /**
   * asSystemRestTemplate.
   *
   * @return RestTemplate
   */
  @Bean
  @Qualifier("asSystem")
  protected RestTemplate asSystemRestTemplate() {
    return new OAuth2RestTemplate(clientCredentialsResourceDetails());
  }

  /**
   * asUserRestTemplate.
   *
   * @param resource OAuth2ProtectedResourceDetails
   * @param context OAuth2ClientContext
   * @return RestTemplate
   */
  @Bean
  @Qualifier("asUser")
  protected RestTemplate asUserRestTemplate(OAuth2ProtectedResourceDetails resource,
      OAuth2ClientContext context) {
    return new OAuth2RestTemplate(resource, context);
  }

  @Primary
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  protected class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
      return new CustomMethodSecurityExpressionHandler();
    }
  }
  
  @Bean
  public LocaleResolver sessionLocaleResolver() {
    return new CustomLocaleResolver();
  }
  
  public class CustomLocaleResolver extends AcceptHeaderLocaleResolver {
    private List<Locale> locales = Arrays.asList(new Locale("en"),new Locale("zh"),new Locale("fr"));
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
      if (StringUtils.isEmpty(request.getHeader("Accept-Language"))) {
        return new Locale("en");
      }
      List<Locale.LanguageRange> list =
          Locale.LanguageRange.parse(request.getHeader("Accept-Language"));
      return Locale.lookup(list, locales);
    }
  } 
 @Bean
 public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
   ObjectMapper mapper = new ObjectMapper();
   mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
   return new MappingJackson2HttpMessageConverter(mapper);
 }
}
