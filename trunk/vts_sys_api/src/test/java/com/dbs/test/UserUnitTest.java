package com.dbs.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.sys.controller.external.UserController;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.security.auth.jwt.CustomAuthenticationToken;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class UserUnitTest {

	@Value("${test.user.user-code}")
	private String userCode;

	@Value("${test.user.pass-code}")
	private String passCode;

	@Value("${test.user.role-code}")
	private String roleCode;

	@Value("${test.user.sess-code}")
	private String sessCode;

	@Autowired
	private UserController userController;

	@Test
	public void getUserInfo() throws Exception {
		UserDetails user = new User(userCode, passCode, Stream.of(getRoles(roleCode)).map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		Map<String, Object> details = new HashMap<>();
		details.put("sessCode", sessCode);
		details.put("dateTime", new Date().getTime());
		String key = null;
		Authentication authentication = new CustomAuthenticationToken(user, details);

		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(authentication);
		SecurityContextHolder.setContext(securityContext);

		userController.getUserInfo(key, authentication);
		System.out.println(ClassPathUtils.getName());
	}

	protected String[] getRoles(String roleCode) {
		return StringUtils.isBlank(roleCode) ? new String[] {} : Stream.of(roleCode.split("\\s*,\\s*")).filter(StringUtils::isNotBlank).toArray(String[]::new);
	}
}
