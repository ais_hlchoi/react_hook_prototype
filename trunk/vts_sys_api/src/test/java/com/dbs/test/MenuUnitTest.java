package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.role.SysRoleForm;
import com.dbs.module.role.model.SysRoleFormModel;
import com.dbs.sys.service.SysRoleService;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class MenuUnitTest {

	@Autowired
	private SysRoleService sysRoleService;

	@Test
	public void getSideMenu() {
		SysRoleForm model = TestUtil.getTestObject(SysRoleFormModel.class);
		sysRoleService.getRoleFuncList(model);
		System.out.println(ClassPathUtils.getName());
	}
}
