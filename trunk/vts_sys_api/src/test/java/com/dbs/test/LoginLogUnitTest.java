package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.log.model.OperationLogFormModel;
import com.dbs.sys.controller.external.LoginLogController;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class LoginLogUnitTest {

	@Autowired
	private LoginLogController loginLogController;
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	private OperationLogFormModel getTempModel() {
		OperationLogFormModel model = TestUtil.getTestObject(OperationLogFormModel.class);
		return model;
	}
	
	@Test
	public void search() {
		loginLogController.search(getTempModel());
		System.out.println(ClassPathUtils.getName());
	}
}
