package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.log.model.OperationLogFormModel;
import com.dbs.sys.controller.external.OperationLogController;
import com.dbs.util.ClassPathUtils;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class OperationLogUnitTest {

	@Autowired
	private OperationLogController operationLogController;
	
	OperationLogFormModel operationLogFormModel = TestUtil.getTestObject(OperationLogFormModel.class);
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void search() {
		operationLogController.search(operationLogFormModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	@Test
	public void searchUpload() {
		operationLogController.searchUpload(operationLogFormModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	@Test
	public void getLoginActivity() {
		operationLogController.searchLogin(operationLogFormModel);
		System.out.println(ClassPathUtils.getName());
	}
}