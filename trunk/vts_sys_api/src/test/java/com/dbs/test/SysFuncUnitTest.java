package com.dbs.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.exception.SysException;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.func.data.SysFuncData;
import com.dbs.module.model.PageProps;
import com.dbs.sys.controller.external.SysFuncController;
import com.dbs.sys.model.SysFuncModel;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SysFuncUnitTest {

	@Autowired
	private SysFuncController sysFuncController;

	private SysFuncModel sysFuncModel = TestUtil.getTestObject(SysFuncModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnitTest", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void searchAll() {
		sysFuncController.searchAll(new SysFuncModel());
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	@Transactional
	public void insertUpdateDelete() throws SysException {
		insert();
		search();
		searchByFuncId();
		update();
		delete();
	}

	@SuppressWarnings("unchecked")
	private void search() {
		RestEntity rs = (RestEntity) sysFuncController.search(sysFuncModel);
		PageProps<SysFuncData> pi = (PageProps<SysFuncData>) rs.getObject();
		List<SysFuncData> sysFuncModelList = pi.getList();
		System.out.println(ClassPathUtils.getName());
	}

	private void searchByFuncId() {
		sysFuncController.searchByFuncId(sysFuncModel.getFuncId());
		System.out.println(ClassPathUtils.getName());
	}

	private void insert() throws SysException {
		sysFuncController.insert(sysFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() throws SysException {
		sysFuncModel.setFuncName("JUnit_Update");
		sysFuncController.update(sysFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() throws SysException {
		sysFuncController.delete(sysFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	private SysFuncModel parse(SysFuncData model) {
		SysFuncModel o = new SysFuncModel();
		o.setFuncId(model.getFuncId());
		o.setFuncCode(model.getFuncCode());
		o.setFuncName(model.getFuncName());
		o.setFuncUrl(model.getFuncUrl());
		o.setType(model.getType());
		o.setDispSeq(model.getDispSeq());
		o.setParentId(model.getParentId());
		return o;
	}
}
