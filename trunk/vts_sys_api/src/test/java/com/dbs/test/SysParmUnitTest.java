package com.dbs.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.exception.SysException;
import com.dbs.sys.model.SysParmModel;
import com.dbs.sys.service.SysParamService;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SysParmUnitTest {

	@Autowired
	private SysParamService sysParamService;

	SysParmModel sysParmModel = TestUtil.getTestObject(SysParmModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnitTest", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void search() {
		sysParamService.search(sysParmModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchAll() {
		sysParamService.search(new SysParmModel());
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchSysParmSegment() {
		sysParamService.searchSysParmSegment(sysParmModel);
		System.out.println(ClassPathUtils.getName());
	}

	private SysParmModel searchSysParmValueBySegment() {
		List<SysParmModel> list = sysParamService.searchSysParmValueBySegment(sysParmModel);
		if (null != list && !list.isEmpty()) {
			System.out.println(ClassPathUtils.getName());
			return list.get(0);
		} else {
			return null;
		}
	}

	@Test
	@Transactional
	public void insertUpdateDelete() throws SysException {
		insert();
		sysParmModel = searchSysParmValueBySegment();
		update();
		delete();
	}

	private void insert() throws SysException {
		sysParamService.insert(sysParmModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() throws SysException {
		sysParamService.update(sysParmModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() throws SysException {
		sysParamService.delete(sysParmModel);
		System.out.println(ClassPathUtils.getName());
	}
}