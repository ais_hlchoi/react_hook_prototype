package com.dbs.test;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

public class TestUtil {
	private static Map<String, Map<String, Object>> TEST_DATA;
	
	static {
		InputStream testDataInputStream = null;
		testDataInputStream = TestUtil.class.getClassLoader().getResourceAsStream("testData.json");
		String jsonStr = "";
		try {
			jsonStr = IOUtils.toString(testDataInputStream, Charset.defaultCharset());
			ObjectMapper objectMapper = new ObjectMapper();
			TEST_DATA = objectMapper.readValue(jsonStr, new TypeReference<Map<String, Map<String, Object>>>(){});
		} catch (Exception e) {
			TEST_DATA = new HashMap<>();
		}
	}
	@SuppressWarnings("deprecation")
	public static <T> T getTestObject(Class<T> className){
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> obj = null;
		T returnObj = null;
		try {
			obj = TEST_DATA.get(className.getSimpleName());
			returnObj = objectMapper.readValue(objectMapper.writeValueAsString(obj),className);
			if(null == returnObj) {
				returnObj = className.newInstance();
			}
			return returnObj;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return returnObj;
	}
}


