package com.dbs.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.exception.SysException;
import com.dbs.sys.controller.external.SysRoleController;
import com.dbs.sys.model.SysRoleFuncModel;
import com.dbs.sys.model.SysRoleModel;
import com.dbs.sys.service.SysRoleService;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SysRoleUnitTest {

	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysRoleController sysRoleController;

	SysRoleModel sysRoleModel = TestUtil.getTestObject(SysRoleModel.class);
	SysRoleFuncModel sysRoleFuncModel = TestUtil.getTestObject(SysRoleFuncModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnitTest", null));
		SecurityContextHolder.setContext(securityContext);
	}

	private SysRoleModel getTempRecord() {
		SysRoleModel model = TestUtil.getTestObject(SysRoleModel.class);
		List<SysRoleModel> list = sysRoleService.search(model);
		return list.get(0);
	}

	@Test
	public void search() {
		sysRoleController.search(sysRoleModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchAll() {
		sysRoleController.searchAll(sysRoleModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchFunc() {
		sysRoleController.searchFunc(sysRoleFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchAllFunc() {
		sysRoleController.searchAllFunc(sysRoleFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchRoleFunc() {
		sysRoleController.searchRoleFunc(sysRoleFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	@Transactional
	public void insertUpdateDeleteAuth() throws SysException {
		insert();
		update();
		authorize();
		selectChk();
		searchRoleFuncChk();
		delete();
	}

	private void insert() throws SysException {
		sysRoleController.insert(sysRoleModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() throws SysException {
		SysRoleModel tempRecord = getTempRecord();
		sysRoleController.update(tempRecord);
		System.out.println(ClassPathUtils.getName());
	}

	private void authorize() throws SysException {
		sysRoleController.authorize(getTempRecord());
		System.out.println(ClassPathUtils.getName());
	}

	private void selectChk() throws SysException {
		sysRoleController.selectChk(getTempRecord());
		System.out.println(ClassPathUtils.getName());
	}

	private void searchRoleFuncChk() {
		sysRoleController.searchRoleFuncChk(sysRoleFuncModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() throws SysException {
		sysRoleController.delete(getTempRecord());
		sysRoleController.authorize(getTempRecord());
		System.out.println(ClassPathUtils.getName());
	}
}
