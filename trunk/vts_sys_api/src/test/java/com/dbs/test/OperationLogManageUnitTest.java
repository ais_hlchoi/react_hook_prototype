package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.controller.internal.OperationLogManageController;
import com.dbs.sys.model.OperationLogDataModel;
import com.dbs.util.ClassPathUtils;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class OperationLogManageUnitTest {

	@Autowired
	private OperationLogManageController operationLogManageController;
	
	private OperationLogDataModel operationLogManageModel = TestUtil.getTestObject(OperationLogDataModel.class);

	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}
	
	@Test
	@Transactional
	public void insertUpdate() {
		insert();
		update();
	}
	
	private void insert() {
		int id = (int)((RestEntity)operationLogManageController.insert(operationLogManageModel)).getObject();
		operationLogManageModel.setOperationLogId(id);
		System.out.println(ClassPathUtils.getName());
	}
	
	private void update() {
		operationLogManageModel.setOperationContent(ClassPathUtils.getName());
		operationLogManageController.update(operationLogManageModel);		
		System.out.println(ClassPathUtils.getName());
	}
}