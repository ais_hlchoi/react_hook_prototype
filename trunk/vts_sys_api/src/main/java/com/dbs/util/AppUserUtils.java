package com.dbs.util;

import org.apache.commons.lang3.StringUtils;

import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.session.UserSessionForm;
import com.fasterxml.jackson.core.JsonProcessingException;

public class AppUserUtils {

	public static void disconnect(final UserSessionForm model, final String url) {
		if (StringUtils.isBlank(url))
			return;

		String action = ClassPathUtils.getName().split("\\.")[1];
		new AppUserUtils().save(model, url, action);
	}

	public void save(UserSessionForm model, String api, String action) {
		try {
			ApiUtils.post(api, model, null);
		} catch (JsonProcessingException e) {
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), String.format("Error found when %s user session", ClassPathUtils.getName().split(".")[1]));
		}
	}
}
