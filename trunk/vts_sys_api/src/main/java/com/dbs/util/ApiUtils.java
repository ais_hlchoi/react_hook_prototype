package com.dbs.util;

import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.constant.RestEntity;
import com.dbs.framework.exception.SysRuntimeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ApiUtils {

	private ApiUtils() {
		// for SonarQube scanning purpose
	}

	public static JsonNode post(final String url, final Object params, final Map<String, String> customHeaders) throws JsonProcessingException {
		if (StringUtils.isBlank(url))
			return null;

		return new ApiUtils().send(url, params, customHeaders);
	}

	protected JsonNode send(final String url, final Object params, final Map<String, String> customHeaders) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String stringifyParams = mapper.writeValueAsString(params);
		HttpHeaders headers = new HttpHeaders();
		if (MapUtils.isNotEmpty(customHeaders))
			customHeaders.entrySet().stream().forEach(o -> headers.add(o.getKey(), o.getValue()));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(stringifyParams, headers);
		ResponseEntity<String> result = new RestTemplate().postForEntity(url, request, String.class);
		return mapper.readTree(result.getBody());
	}

	public static void validate(final JsonNode root) {
		if (null == root.get(RestEntity.RESPONSE_CODE.key)) {
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), "Unknown Exception");
		}
		ResponseCode responseCode = ResponseCodeUtils.getOrElse(root.get(RestEntity.RESPONSE_CODE.key).asText());
		if (ResponseCodeUtils.isNotSuccess(responseCode)) {
			throw new SysRuntimeException(responseCode, null == root.get(RestEntity.RESPONSE_DESC.key) ? "Unknown Exception" : root.get(RestEntity.RESPONSE_DESC.key).asText());
		}
	}
}
