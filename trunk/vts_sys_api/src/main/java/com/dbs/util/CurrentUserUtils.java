package com.dbs.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.dbs.framework.aspect.annotation.CurrentUser;
import com.dbs.module.model.UserForm;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.sys.model.AbstractAuditModel;

public class CurrentUserUtils extends AbstractModuleUtils {

	private CurrentUserUtils() {
		// for SonarQube scanning purpose
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void bind(Object obj) {
		new CurrentUserUtils().setOneBankId(obj);
	}

	@SuppressWarnings("unchecked")
	protected void setOneBankId(Object obj) {
		if (obj.getClass().isAnnotationPresent(CurrentUser.class) && CurrentUser.Action.IGNORE.equals(obj.getClass().getAnnotation(CurrentUser.class).value()))
			return;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = null == auth ? "schedule" : auth.getName();
		List<String> list = null == auth ? new ArrayList<>() : auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

		boolean match = false;
		if (obj instanceof Map) {
			Map<String, Object> o = (Map<String, Object>) obj;
			o.put("oneBankId", getOrElse(o.get("oneBankId"), name));
			o.put("oneBankRoleList", list);
			o.put("createdBy", name);
			o.put("lastUpdatedBy", name);
			match = true;
		} else {
			if (obj instanceof AbstractAuditModel) {
				AbstractAuditModel o = (AbstractAuditModel) obj;
				o.setOneBankId(getOrElse(o.getOneBankId(), name));
				o.setOneBankRoleList(list);
				o.setCreatedBy(name);
				o.setLastUpdatedBy(name);
				match = true;
			}
			if (obj instanceof UserForm) {
				UserForm o = (UserForm) obj;
				o.setOneBankId(getOrElse(o.getOneBankId(), name));
				o.setOneBankRoleList(list);
				match = true;
			}
		}
		if (match) {
			logger.info(String.format("%s -> %s", ClassPathUtils.getName(), FortifyStrategyUtils.toLogString(ClassPathUtils.toJSONString(obj))));
		}
	}

	public static String getOneBankId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return null == auth ? "schedule" : auth.getName();
	}

	@SuppressWarnings("unchecked")
	public static String getSessionCode() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (null == auth || null == auth.getDetails() || !(auth.getDetails() instanceof Map))
			return null;

		return (String) ((Map<String, Object>) auth.getDetails()).get("sessCode");
	}

	@SuppressWarnings("unchecked")
	public static Long getLoginTimeInMillis() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (null == auth || null == auth.getDetails() || !(auth.getDetails() instanceof Map))
			return null;

		return (Long) ((Map<String, Object>) auth.getDetails()).get("dateTime");
	}

	public static String getAuthToken(final HttpServletRequest request) {
		return RequestHeaderUtils.getAuthToken(request);
	}

	public static String getClientIPAddress(final HttpServletRequest request) {
		return RequestHeaderUtils.getClientIPAddress(request);
	}

	public static String maskToken(final String text) {
		String[] arr = StringUtils.isBlank(text) ? new String[] {} : text.split("\\.");
		return arr.length < 3 ? "*.*.*" : String.format("%s.%s.%s", arr[0], "*", arr[2]);
	}
}
