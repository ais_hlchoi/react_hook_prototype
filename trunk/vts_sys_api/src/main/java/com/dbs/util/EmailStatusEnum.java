package com.dbs.util;

public enum EmailStatusEnum {
	  PENDING("Pending", "P")
    , SENT("Sent", "S")
    , FAIL("Fail","F")
    ;

	private String key;
	private String value;

	private EmailStatusEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String key() {
		return this.key;
	}

	public String value() {
		return this.value;
	}



}
