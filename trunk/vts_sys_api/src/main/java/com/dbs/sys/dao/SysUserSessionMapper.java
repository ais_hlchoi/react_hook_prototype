package com.dbs.sys.dao;

import java.util.List;

import com.dbs.module.session.SysUserSession;
import com.dbs.module.session.model.UserSessionStatusModel;
import com.dbs.sys.model.SysUserSessionAudModel;
import com.dbs.sys.model.SysUserSessionModel;

public interface SysUserSessionMapper {

	List<UserSessionStatusModel> getUserSessionList(SysUserSession model);

	int getUserSessionCount(SysUserSession model);

	int insert(SysUserSessionModel model);

	int update(SysUserSession model);

	int delete(SysUserSession model);

	int insertAudit(SysUserSessionAudModel model);
}
