package com.dbs.sys.dao;

import java.util.List;

import com.dbs.module.role.SysRoleForm;
import com.dbs.sys.model.SysRoleFuncModel;

public interface SysRoleFuncMapper {

	List<SysRoleFuncModel> getRoleFuncList(SysRoleForm model);

	Integer insertAll(SysRoleFuncModel model);

	Integer updateAll(SysRoleFuncModel model);

	Integer update(SysRoleFuncModel model);

	Integer updateDeleteByRoleId(SysRoleFuncModel model);

	List<SysRoleFuncModel> searchRoleFunc(SysRoleFuncModel model);

	List<SysRoleFuncModel> searchRoleFuncChk(SysRoleFuncModel model);

	List<SysRoleFuncModel> searchAllFunc(SysRoleFuncModel model);

	List<SysRoleFuncModel> searchFunc(SysRoleFuncModel model);

	Integer searchByFuncId(Integer id);

	Integer searchByFuncIdChk(Integer id);

	Integer deleteByFuncId(Integer id);

	Integer deleteByFuncIdChk(Integer id);

	Integer deleteMakByRoleId(Integer id);

	Integer deleteMakByFuncId(Integer id);

	Integer deleteChkByRoleId(Integer id);

	Integer deleteChkByFuncId(Integer id);

	Integer makToChk(SysRoleFuncModel model);

	Integer chkToAud(SysRoleFuncModel model);

	Integer makAuth(SysRoleFuncModel model);
}
