package com.dbs.sys.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer roleId;

	@JsonInclude
	private String roleCode;

	@JsonInclude
	private String roleName;

	@JsonInclude
	private String remark;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;

	@JsonInclude
	private List<SysRoleFuncModel> funcList;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("roleId", roleId);
		args.put("roleCode", roleCode);
		args.put("roleName", roleName);
		args.put("remark", remark);
		args.put("dispSeq", dispSeq);
		args.put("activeInd", activeInd);
		args.put("funcList", CollectionUtils.isEmpty(funcList) ? null : funcList.stream().map(SysRoleFuncModel::operationContent).collect(Collectors.toList()));
		return ClassPathUtils.toJSONString(args);
	}
}
