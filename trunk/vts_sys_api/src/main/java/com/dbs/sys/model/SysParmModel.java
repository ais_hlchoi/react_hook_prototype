package com.dbs.sys.model;

import java.util.LinkedHashMap;
import java.util.Map;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysParmModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String segment;

	@JsonInclude
	private String segmentValue;

	@JsonInclude
	private String segmentName;

	@JsonInclude
	private String code;

	@JsonInclude
	private String shortDesc;

	@JsonInclude
	private String longDesc;

	@JsonInclude
	private String shortDescTc;

	@JsonInclude
	private String longDescTc;

	@JsonInclude
	private String parmValue;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("id", id);
		args.put("segment", segment);
		args.put("segmentValue", segmentValue);
		args.put("segmentName", segmentName);
		args.put("code", code);
		args.put("shortDesc", shortDesc);
		args.put("longDesc", longDesc);
		args.put("shortDescTc", shortDescTc);
		args.put("longDescTc", longDescTc);
		args.put("parmValue", parmValue);
		args.put("dispSeq", dispSeq);
		args.put("activeInd", activeInd);
		return ClassPathUtils.toJSONString(args);
	}
}
