package com.dbs.sys.model;

import java.sql.Timestamp;

import com.dbs.module.session.SysUserSession;
import com.fasterxml.jackson.annotation.JsonInclude;

public class SysUserSessionModel extends AbstractAuditModel implements SysUserSession {

	@JsonInclude
	private String userId;

	@JsonInclude
	private String sid;

	@JsonInclude
	private String ip;

	@JsonInclude
	private Timestamp loginTime;

	@JsonInclude
	private Timestamp lastActiveTime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Timestamp getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	public Timestamp getLastActiveTime() {
		return lastActiveTime;
	}

	public void setLastActiveTime(Timestamp lastActiveTime) {
		this.lastActiveTime = lastActiveTime;
	}
}
