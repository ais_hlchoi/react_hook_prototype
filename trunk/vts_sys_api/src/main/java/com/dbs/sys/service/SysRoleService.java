package com.dbs.sys.service;

import java.util.List;

import com.dbs.module.role.SysRoleForm;
import com.dbs.module.role.data.SysRoleData;
import com.dbs.module.role.data.SysRoleFuncData;
import com.dbs.sys.model.SysRoleFuncModel;
import com.dbs.sys.model.SysRoleModel;

public interface SysRoleService {

	List<SysRoleFuncData> getRoleFuncList(SysRoleForm model);

	List<SysRoleModel> search(SysRoleModel model);

	List<SysRoleData> searchAll(SysRoleModel model);

	List<SysRoleFuncData> searchRoleFunc(SysRoleFuncModel model);

	List<SysRoleFuncData> searchRoleFuncChk(SysRoleFuncModel model);

	List<SysRoleFuncData> searchAllFunc(SysRoleFuncModel model);

	List<SysRoleFuncData> searchFunc(SysRoleFuncModel model);

	Integer searchByFuncId(Integer id);

	SysRoleData searchChk(SysRoleModel model);

	int insert(SysRoleModel model);

	int update(SysRoleModel model);

	int reverse(SysRoleModel model);

	int delete(SysRoleModel model);

	int authorize(SysRoleModel model);
}
