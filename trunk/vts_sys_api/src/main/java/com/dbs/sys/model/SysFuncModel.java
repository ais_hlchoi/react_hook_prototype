package com.dbs.sys.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysFuncModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer funcId;

	@JsonInclude
	private Integer parentId;

	@JsonInclude
	private String funcCode;

	@JsonInclude
	private String funcName;

	@JsonInclude
	private String type;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String funcUrl;

	@JsonInclude
	private Integer parentIdLevel2;

	@JsonInclude
	private String flag;

	@JsonInclude
	private List<SysFuncModel> children;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("funcId", funcId);
		args.put("parentId", parentId);
		args.put("funcCode", funcCode);
		args.put("funcName", funcName);
		args.put("type", type);
		args.put("dispSeq", dispSeq);
		args.put("funcUrl", funcUrl);
		args.put("parentIdLevel2", parentIdLevel2);
		args.put("flag", flag);
		args.put("children", CollectionUtils.isEmpty(children) ? null : children.stream().map(SysFuncModel::operationContent).collect(Collectors.toList()));
		return ClassPathUtils.toJSONString(args);
	}
}
