package com.dbs.sys.dao;

import java.util.List;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.model.OperationLogDataModel;
import com.dbs.sys.model.OperationLogModel;

public interface OperationLogMapper {

	List<OperationLogDataModel> search(OperationLogForm model);

	List<OperationLogDataModel> searchUpload(OperationLogForm model);

	List<OperationLogDataModel> searchLogin(OperationLogForm model);

	int insert(OperationLogModel model);

	int update(OperationLogModel model);
}
