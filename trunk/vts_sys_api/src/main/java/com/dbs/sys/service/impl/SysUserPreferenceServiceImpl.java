package com.dbs.sys.service.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.sys.dao.SysUserFuncPrefMapper;
import com.dbs.sys.model.SysUserPreferenceModel;
import com.dbs.sys.service.SysUserPreferenceService;
import com.dbs.util.CurrentUserUtils;

@Service
public class SysUserPreferenceServiceImpl implements SysUserPreferenceService {

	@Autowired
	private SysUserFuncPrefMapper sysUserFuncPrefMapper;

	@Override
	public List<SysUserPreferenceModel> search(SysUserPreferenceModel model) {
		return sysUserFuncPrefMapper.search(model);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void setUp(SysUserPreferenceModel model) {
		if (model.getUserId() != null) {
			sysUserFuncPrefMapper.deleteByUserId(model.getUserId());
			List<SysUserPreferenceModel> list = model.getFuncList();
			if (CollectionUtils.isNotEmpty(list)) {
				model.setLastUpdatedBy(CurrentUserUtils.getOneBankId());
				model.setCreatedBy(model.getLastUpdatedBy());
				Integer seq = 1;
				for (SysUserPreferenceModel item : list) {
					item.setDispSeq(seq);
					seq++;
				}
				model.setFuncList(list);
				sysUserFuncPrefMapper.insertAll(model);
			}
		}
	}

	@Override
	public Integer searchByFuncId(Integer funcId) {
		return sysUserFuncPrefMapper.searchByFuncId(funcId);
	}
}
