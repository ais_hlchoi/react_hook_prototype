package com.dbs.sys.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.log.data.SysLogData;
import com.dbs.module.log.model.OperationLogFormModel;
import com.dbs.sys.model.OperationLogDataModel;
import com.dbs.sys.service.OperationLogService;
import com.dbs.util.PageInfoUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/operationLog")
public class OperationLogController {

	@Autowired
	private OperationLogService operationLogManageService;

	@RequestMapping("/search")
	public Object search(@RequestBody OperationLogFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<OperationLogDataModel> list = operationLogManageService.search(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysLogData.class));
	}

	@RequestMapping("/searchUpload")
	public Object searchUpload(@RequestBody OperationLogFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<OperationLogDataModel> list = operationLogManageService.searchUpload(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysLogData.class));
	}

	@RequestMapping("/searchLogin")
	public Object searchLogin(@RequestBody OperationLogFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<OperationLogDataModel> list = operationLogManageService.searchLogin(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysLogData.class));
	}
}
