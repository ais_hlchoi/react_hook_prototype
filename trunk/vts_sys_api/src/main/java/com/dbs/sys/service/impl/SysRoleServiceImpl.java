package com.dbs.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.framework.constant.RecordStatus;
import com.dbs.framework.constant.SystemResponseCode;
import com.dbs.module.role.SysRoleForm;
import com.dbs.module.role.data.SysRoleData;
import com.dbs.module.role.data.SysRoleFuncData;
import com.dbs.sys.dao.SysRoleFuncMapper;
import com.dbs.sys.dao.SysRoleMapper;
import com.dbs.sys.model.SysRoleFuncModel;
import com.dbs.sys.model.SysRoleModel;
import com.dbs.sys.service.SysRoleService;
import com.dbs.sys.service.impl.AbstractServiceImpl.AssertResponseCode;
import com.dbs.util.Assert;

@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;

	@Autowired
	private SysRoleFuncMapper sysRoleFuncMapper;

	private static final String SQL_ERR_INSERT = "SQL_ERR_INSERT";
	private static final String SQL_ERR_UPDATE = "SQL_ERR_UPDATE";
	private static final String SQL_ERR_DELETE = "SQL_ERR_DELETE";

	private static final String ROLE_CODE = "RoleCode";
	private static final String SEQ = "Seq";

	@Override
	public List<SysRoleFuncData> getRoleFuncList(SysRoleForm model) {
		List<SysRoleFuncModel> list = sysRoleFuncMapper.getRoleFuncList(model);
		Assert.isTrue(CollectionUtils.isNotEmpty(list), "User role not found.", SystemResponseCode.M9000);
		return list.stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysRoleModel> search(SysRoleModel model) {
		return sysRoleMapper.search(model);
	}

	@Override
	public List<SysRoleData> searchAll(SysRoleModel model) {
		List<SysRoleModel> list = sysRoleMapper.search(model);
		return null == list ? new ArrayList<>() : list.stream().map(SysRoleData::new).collect(Collectors.toList());
	}

	@Override
	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional(rollbackFor = Exception.class)
	public int insert(SysRoleModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		Assert.isTrue(sysRoleMapper.onlyDispSeq(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		Assert.isTrue(sysRoleMapper.onlyRoleCode(model) == 0, AssertResponseCode.EXISTS.getMsg(ROLE_CODE));
		Assert.isTrue(sysRoleMapper.insert(model) == 1, SQL_ERR_INSERT);
		if (CollectionUtils.isNotEmpty(model.getFuncList())) {
			SysRoleFuncModel bean = new SysRoleFuncModel();
			bean.setRoleId(model.getRoleId());
			bean.setCreatedBy(model.getCreatedBy());
			bean.setLastUpdatedBy(model.getLastUpdatedBy());
			bean.setFuncList(model.getFuncList());
			sysRoleFuncMapper.insertAll(bean);
		}
		return model.getRoleId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public int update(SysRoleModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		if (CollectionUtils.isNotEmpty(model.getFuncList())) {
			insertRoleFunc(model);
			updateRoleFunc(model);
		}
		Assert.isTrue(sysRoleMapper.onlyDispSeq(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		Assert.isTrue(sysRoleMapper.onlyRoleCode(model) == 0, AssertResponseCode.EXISTS.getMsg(ROLE_CODE));
		Assert.isTrue(sysRoleMapper.update(model) == 1, SQL_ERR_UPDATE);
		return model.getRoleId();
	}

	private void insertRoleFunc(SysRoleModel model) {
		List<SysRoleFuncModel> list = model.getFuncList().stream().filter(o -> null == o.getRoleId()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(list))
			return;

		SysRoleFuncModel bean = new SysRoleFuncModel();
		bean.setRoleId(model.getRoleId());
		bean.setFuncList(list);
		Assert.isTrue(sysRoleFuncMapper.insertAll(bean) > 0, SQL_ERR_UPDATE);
	}

	private void updateRoleFunc(SysRoleModel model) {
		List<SysRoleFuncModel> list = model.getFuncList().stream().filter(o -> null != o.getRoleId()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(list))
			return;

		for (SysRoleFuncModel item : list) {
			Assert.isTrue(sysRoleFuncMapper.update(item) == 1, SQL_ERR_UPDATE);
		}
	}

	@Override
	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public int reverse(SysRoleModel model) {
		Assert.isTrue(sysRoleMapper.reverse(model) == 1, SQL_ERR_UPDATE);
		return model.getRoleId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public int delete(SysRoleModel model) {
		if (RecordStatus.INIT.equalsStatus(model.getMakChkStatus())) {
			sysRoleFuncMapper.deleteMakByRoleId(model.getRoleId());
			Assert.isTrue(sysRoleMapper.deleteMak(model) == 1, SQL_ERR_DELETE);
		} else {
			SysRoleFuncModel bean = new SysRoleFuncModel();
			bean.setRoleId(model.getRoleId());
			sysRoleFuncMapper.updateDeleteByRoleId(bean);
			Assert.isTrue(sysRoleMapper.delete(model) == 1, SQL_ERR_DELETE);
		}
		return model.getRoleId();
	}

	/**
	 * 
	 * @param funcList
	 * @return
	 */
	protected List<SysRoleFuncModel> menuFormatData(List<SysRoleFuncModel> funcList, boolean addState) {
		List<SysRoleFuncModel> menuList = new ArrayList<>();

		if (CollectionUtils.isEmpty(funcList))
			return menuList;

		for (SysRoleFuncModel menu : funcList) {
			if ("MENU".equals(menu.getType())) {
				menu.setIden(menu.getFuncId());
				menu.setActiveInd(addState ? "N" : menu.getActiveInd());
				List<SysRoleFuncModel> menuChildren = new ArrayList<>();
				for (SysRoleFuncModel func : funcList) {
					if (menu.getFuncId().equals(func.getParentId()) && "FUNC".equals(func.getType())) {
						func.setIden(func.getFuncId());
						func.setActiveInd(addState ? "N" : func.getActiveInd());
						List<SysRoleFuncModel> funcChildren = new ArrayList<>();
						for (SysRoleFuncModel subfunc : funcList) {
							if (func.getFuncId().equals(subfunc.getParentId()) && "SUBFUNC".equals(subfunc.getType())) {
								subfunc.setIden(subfunc.getFuncId());
								subfunc.setActiveInd(addState ? "N" : subfunc.getActiveInd());
								subfunc.setChildren(null);
								funcChildren.add(subfunc);
							}
						}
						if (CollectionUtils.isNotEmpty(funcChildren)) {
							func.setChildren(funcChildren);
						}
						menuChildren.add(func);
					}
				}
				if (CollectionUtils.isNotEmpty(menuChildren)) {
					menu.setChildren(menuChildren);
				}
				menuList.add(menu);
			}
		}
		return menuList;
	}

	@Override
	public List<SysRoleFuncData> searchRoleFunc(SysRoleFuncModel model) {
		return menuFormatData(sysRoleFuncMapper.searchRoleFunc(model), false).stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysRoleFuncData> searchRoleFuncChk(SysRoleFuncModel model) {
		return menuFormatData(sysRoleFuncMapper.searchRoleFuncChk(model), true).stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysRoleFuncData> searchAllFunc(SysRoleFuncModel model) {
		return menuFormatData(sysRoleFuncMapper.searchAllFunc(model), true).stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysRoleFuncData> searchFunc(SysRoleFuncModel model) {
		List<SysRoleFuncModel> list = sysRoleFuncMapper.searchFunc(model);
		return null == list ? new ArrayList<>() : list.stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@Override
	public Integer searchByFuncId(Integer id) {
		return sysRoleFuncMapper.searchByFuncId(id);
	}
	
	@Override
	public SysRoleData searchChk(SysRoleModel model) {
		SysRoleModel item = sysRoleMapper.searchChk(model);
		return null == item ? null : new SysRoleData(item);
	}

	public String check(SysRoleModel model) {
		List<String> msg = new ArrayList<>();
		Assert.notBlank(model.getRoleCode(), AssertResponseCode.NOT_EMPTY.getMsg(ROLE_CODE), msg);
		Assert.notBlank(model.getRoleName(), AssertResponseCode.NOT_EMPTY.getMsg("RoleName"), msg);
		Assert.notBlank(model.getActiveInd(), AssertResponseCode.NOT_EMPTY.getMsg("Status"), msg);
		Assert.notNull(model.getDispSeq(), AssertResponseCode.NOT_EMPTY.getMsg(SEQ), msg);
		return CollectionUtils.isEmpty(msg) ? null : StringUtils.join(msg, ";");
	}

	@Override
	@OperationLog(operation = OperationLogAction.AUTHORIZE)
	@Transactional(rollbackFor = Exception.class)
	public int authorize(SysRoleModel model) {
		List<SysRoleModel> modelMakList = sysRoleMapper.search(model);
		Assert.isTrue(CollectionUtils.isNotEmpty(modelMakList) && modelMakList.size() == 1, "Role Model error.");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysRoleModel modelMak = modelMakList.get(0);
		boolean isJUnitTest = auth.getName().equalsIgnoreCase("JUnitTest");
		boolean isOtherUser = !auth.getName().equals(modelMak.getLastInitId());
		Assert.isTrue(isJUnitTest || isOtherUser, "You are not allow to authorize this record.");

		modelMak.setCreatedBy(model.getCreatedBy());
		modelMak.setLastUpdatedBy(model.getLastUpdatedBy());
		modelMak.setStatInd(modelMak.getStatInd());
		modelMak.setAmendInd(modelMak.getAmendInd());

		SysRoleFuncModel bean = new SysRoleFuncModel();
		bean.setRoleId(modelMak.getRoleId());
		bean.setCreatedBy(model.getCreatedBy());
		bean.setLastUpdatedBy(model.getLastUpdatedBy());
		bean.setStatInd(modelMak.getStatInd());
		bean.setAmendInd(modelMak.getAmendInd());

		int done = 0;
		if (RecordStatus.INIT.equalsStatus(modelMak.getMakChkStatus())) {
			sysRoleMapper.makAuth(modelMak);
			sysRoleMapper.makToChk(modelMak);
			sysRoleMapper.chkToAud(modelMak);
			sysRoleFuncMapper.makAuth(bean);
			sysRoleFuncMapper.makAuth(bean);
			sysRoleFuncMapper.chkToAud(bean);
			done = 1;
		} else if (RecordStatus.AMEND.equalsStatus(modelMak.getMakChkStatus())) {
			sysRoleFuncMapper.chkToAud(bean);
			sysRoleFuncMapper.deleteChkByRoleId(bean.getRoleId());
			sysRoleFuncMapper.makAuth(bean);
			sysRoleFuncMapper.makToChk(bean);
			sysRoleMapper.chkToAud(modelMak);
			sysRoleMapper.makAuth(modelMak);
			sysRoleMapper.makToChk(modelMak);
			done = 3;
		} else if (RecordStatus.DELETE.equalsStatus(modelMak.getMakChkStatus())) {
			sysRoleFuncMapper.chkToAud(bean);
			sysRoleFuncMapper.deleteChkByRoleId(bean.getRoleId());
			sysRoleFuncMapper.deleteMakByRoleId(bean.getRoleId());
			sysRoleMapper.chkToAud(modelMak);
			sysRoleMapper.deleteChk(modelMak);
			sysRoleMapper.deleteMak(modelMak);
			done = 2;
		}
		Assert.isTrue(done > 0, "Role Model Status has error.");

		return model.getRoleId();
	}
}
