package com.dbs.sys.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.log.data.SysLogData;
import com.dbs.module.log.model.OperationLogFormModel;
import com.dbs.sys.model.LoginLogDataModel;
import com.dbs.sys.service.LoginLogService;
import com.dbs.util.PageInfoUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/loginLog")
public class LoginLogController {

	@Autowired
	private LoginLogService loginLogManageService;

	@RequestMapping("/search")
	public Object search(@RequestBody OperationLogFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<LoginLogDataModel> list = loginLogManageService.search(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysLogData.class));
	}
}
