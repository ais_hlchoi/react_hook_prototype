package com.dbs.sys.controller.external;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.role.SysRoleForm;
import com.dbs.module.role.data.SysRoleFuncData;
import com.dbs.module.role.model.SysRoleFormModel;
import com.dbs.sys.service.SysRoleService;

@RestController
@CrossOrigin
@RequestMapping("/api/menu")
public class MenuController {

	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping(value = "/getSideMenu", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getSideMenu(@AuthenticationPrincipal Authentication authentication) {
		SysRoleForm bean = new SysRoleFormModel(authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
		List<SysRoleFuncData> list = sysRoleService.getRoleFuncList(bean);
		return RestEntity.success(list);
	}
}
