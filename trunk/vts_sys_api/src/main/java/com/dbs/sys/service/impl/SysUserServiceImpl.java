package com.dbs.sys.service.impl;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.dbs.module.role.data.UserInfoData;
import com.dbs.sys.service.SysUserService;

@Service
public class SysUserServiceImpl implements SysUserService {

	public UserInfoData getUserInfo(Authentication model) {
		return null == model ? null : new UserInfoData(model);
	}
}
