package com.dbs.sys.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

public class SysUserPreferenceModel extends AbstractAuditModel {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String userId;

	@JsonInclude
	private Integer funcId;

	@JsonInclude
	private Integer dispSeq;

	// extends

	@JsonInclude
	private String funcName;

	@JsonInclude
	private Integer parentId;

	@JsonInclude
	private String type;

	@JsonInclude
	private List<SysUserPreferenceModel> funcList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getFuncId() {
		return funcId;
	}

	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}

	public Integer getDispSeq() {
		return dispSeq;
	}

	public void setDispSeq(Integer dispSeq) {
		this.dispSeq = dispSeq;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<SysUserPreferenceModel> getFuncList() {
		return funcList;
	}

	public void setFuncList(List<SysUserPreferenceModel> funcList) {
		this.funcList = funcList;
	}
}
