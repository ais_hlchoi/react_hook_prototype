package com.dbs.sys.controller.external;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.exception.SysException;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.func.data.SysFuncData;
import com.dbs.sys.model.SysFuncModel;
import com.dbs.sys.service.SysFuncService;
import com.dbs.sys.service.SysRoleService;
import com.dbs.sys.service.SysUserPreferenceService;
import com.dbs.util.PageInfoUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sysFunc")
public class SysFuncController {

	@Autowired
	private SysFuncService sysFuncService;

	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysUserPreferenceService userPreferenceService;

	/**
	 * Search Function
	 * @param SysFuncModel
	 * @return
	 */
	@RequestMapping("/search")
	public Object search(@RequestBody SysFuncModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<SysFuncModel> list = sysFuncService.search(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysFuncData.class));
	}

	@RequestMapping("/searchAll")
	public Object searchAll(@RequestBody SysFuncModel model) {
		List<SysFuncModel> list = sysFuncService.search(model);
		return RestEntity.success(list);
	}

	/**
	 *  Search Function by ID
	 * @param funcId
	 * @return
	 */
	@RequestMapping("/searchByFuncId")
	public Object searchByFuncId(@RequestBody Integer funcId) {
		Map<String, Object> map = new HashMap<>();
		Integer roleCount = sysRoleService.searchByFuncId(funcId);
		Integer userPreferenceCount = userPreferenceService.searchByFuncId(funcId);
		map.put("roleCount", roleCount);
		map.put("userPreferenceCount", userPreferenceCount);
		return RestEntity.success(map);
	}

	/**
	 * New Function
	 * @param SysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/insert")
	public Object insert(@RequestBody SysFuncModel model) {
		int id = sysFuncService.insert(model);
		return RestEntity.success(id);
	}

	/**
	 * Update Function
	 * @param SysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/update")
	public Object update(@RequestBody SysFuncModel model) {
		int id = sysFuncService.update(model);
		return RestEntity.success(id);
	}

	/**
	 * Delete Function
	 * @param SysFuncModel
	 * @return
	 * @throws SysException 
	 */
	@RequestMapping("/delete")
	public Object delete(@RequestBody SysFuncModel model) {
		if (model.getFuncId() == null) {
			return RestEntity.failed("ID_CANNOT_BE_EMPTY");
		}
		int id = sysFuncService.delete(model);
		return RestEntity.success(id);
	}

	/**
	 * Search Function
	 * @param SysFuncModel
	 * @return
	 */
	@RequestMapping("/searchFunc")
	public Object searchFunc(@RequestBody SysFuncModel model) {
		List<SysFuncData> list = sysFuncService.searchFunc(model);
		return RestEntity.success(list);
	}
}
