package com.dbs.sys.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.SysUserPreferenceModel;
import com.dbs.sys.service.SysUserPreferenceService;

@RestController
@RequestMapping("/api/userPreference")
public class SysUserPreferenceController {

	@Autowired
	private SysUserPreferenceService userPreferenceService;

	@RequestMapping("/search")
	public Object search(@RequestBody SysUserPreferenceModel userPreferenceModel) {
		List<SysUserPreferenceModel> list = userPreferenceService.search(userPreferenceModel);
		return RestEntity.success(list);
	}

	@RequestMapping("/setUp")
	public Object setUp(@RequestBody SysUserPreferenceModel userPreferenceModel) {
		userPreferenceService.setUp(userPreferenceModel);
		return RestEntity.success();
	}
}
