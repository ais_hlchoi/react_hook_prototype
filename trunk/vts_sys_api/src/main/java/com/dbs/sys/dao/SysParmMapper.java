package com.dbs.sys.dao;

import java.util.List;

import com.dbs.sys.model.SysParmModel;

public interface SysParmMapper {

	List<SysParmModel> search(SysParmModel model);

	List<SysParmModel> searchSysParmSegment(SysParmModel model);

	List<SysParmModel> searchSysParmValueBySegment(SysParmModel model);

	Integer onlyDispSeq(SysParmModel model);

	Integer onlyCode(SysParmModel model);

	int insert(SysParmModel model);

	int update(SysParmModel model);

	int delete(SysParmModel model);
}
