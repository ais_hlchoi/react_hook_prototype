package com.dbs.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.sys.dao.SysParmMapper;
import com.dbs.sys.model.SysParmModel;
import com.dbs.sys.service.SysParamService;
import com.dbs.util.Assert;

@Service
public class SysParmServiceImpl extends AbstractServiceImpl implements SysParamService {

	@Autowired
	private SysParmMapper sysParmMapper;

	private static final String SQL_ERR_INSERT = "SQL_ERR_INSERT";
	private static final String SQL_ERR_UPDATE = "SQL_ERR_UPDATE";
	private static final String SQL_ERR_DELETE = "SQL_ERR_DELETE";

	private static final String SEGMENT = "Segment";
	private static final String CODE = "Code";
	private static final String SEQ = "Seq";

	@Override
	public List<SysParmModel> search(SysParmModel model) {
		return sysParmMapper.search(model);
	}

	@Override
	public List<SysParmModel> searchAll(SysParmModel model) {
		return sysParmMapper.search(model);
	}

	@Override
	public List<SysParmModel> searchSysParmSegment(SysParmModel model) {
		return sysParmMapper.searchSysParmSegment(model);
	}

	@Override
	public List<SysParmModel> searchSysParmValueBySegment(SysParmModel model) {
		return sysParmMapper.searchSysParmValueBySegment(model);
	}

	@Override
	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional(rollbackFor = Exception.class)
	public Object insert(SysParmModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		Assert.isTrue(sysParmMapper.onlyCode(model) == 0, AssertResponseCode.EXISTS.getMsg(CODE));
		Assert.isTrue(sysParmMapper.onlyDispSeq(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		Assert.isTrue(sysParmMapper.insert(model) == 1, SQL_ERR_INSERT);
		return model.getId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public Object update(SysParmModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		Assert.isTrue(sysParmMapper.onlyCode(model) == 0, AssertResponseCode.EXISTS.getMsg(CODE));
		Assert.isTrue(sysParmMapper.onlyDispSeq(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		Assert.isTrue(sysParmMapper.update(model) == 1, SQL_ERR_UPDATE);
		return model.getId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public Object delete(SysParmModel model) {
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.isTrue(sysParmMapper.delete(model) == 1, SQL_ERR_DELETE);
		return model.getId();
	}

	protected String check(SysParmModel model) {
		List<String> msg = new ArrayList<>();
		Assert.notBlank(model.getSegment(), AssertResponseCode.NOT_EMPTY.getMsg(SEGMENT), msg);
		Assert.notBlank(model.getCode(), AssertResponseCode.NOT_EMPTY.getMsg(CODE), msg);
		Assert.notBlank(model.getShortDesc(), AssertResponseCode.NOT_EMPTY.getMsg("Description"), msg);
		Assert.notBlank(model.getParmValue(), AssertResponseCode.NOT_EMPTY.getMsg("Parameter Value"), msg);
		Assert.notBlank(model.getActiveInd(), AssertResponseCode.NOT_EMPTY.getMsg("Status"), msg);
		return CollectionUtils.isEmpty(msg) ? null : StringUtils.join(msg, ";");
	}
}
