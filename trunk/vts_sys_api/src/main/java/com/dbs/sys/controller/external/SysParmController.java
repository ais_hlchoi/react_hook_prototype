package com.dbs.sys.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.SysParmModel;
import com.dbs.sys.service.SysParamService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sysParm")
public class SysParmController {

	@Autowired
	private SysParamService sysParamService;

	@RequestMapping("/search")
	public Object search(@RequestBody SysParmModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<SysParmModel> list = sysParamService.search(model);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchAll")
	public Object searchAll(@RequestBody SysParmModel model) {
		List<SysParmModel> list = sysParamService.searchAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/insert")
	public Object insert(@RequestBody SysParmModel model) {
		Object object = sysParamService.insert(model);
		return RestEntity.success(object);
	}

	@RequestMapping("/update")
	public Object update(@RequestBody SysParmModel model) {
		Object object = sysParamService.update(model);
		return RestEntity.success(object);
	}

	@RequestMapping("/delete")
	public Object delete(@RequestBody SysParmModel model) {
		Object object = sysParamService.delete(model);
		return RestEntity.success(object);
	}

	@RequestMapping("/searchSysParmSegment")
	public Object searchSysParmSegment(@RequestBody SysParmModel model) {
		List<SysParmModel> list = sysParamService.searchSysParmSegment(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchSysParmValueBySegment")
	public Object searchSysParmValueBySegment(@RequestBody SysParmModel model) {
		List<SysParmModel> list = sysParamService.searchSysParmValueBySegment(model);
		return RestEntity.success(list);
	}
}
