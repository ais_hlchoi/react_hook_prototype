package com.dbs.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.dao.OperationLogMapper;
import com.dbs.sys.model.OperationLogDataModel;
import com.dbs.sys.model.OperationLogModel;
import com.dbs.sys.service.OperationLogService;
import com.dbs.util.OperationLogUtils;

@Service
public class OperationLogServiceImpl extends AbstractServiceImpl implements OperationLogService {

	@Autowired
	private OperationLogMapper operationLogMapper;

	public List<OperationLogDataModel> search(OperationLogForm model) {
		return operationLogMapper.search(model);
	}

	public List<OperationLogDataModel> searchUpload(OperationLogForm model) {
		return operationLogMapper.searchUpload(model);
	}

	public List<OperationLogDataModel> searchLogin(OperationLogForm model) {
		return operationLogMapper.searchLogin(model);
	}

	@Transactional
	public int insert(OperationLogModel model) {
		Assert.isTrue(operationLogMapper.insert(OperationLogUtils.parse(model)) == 1, String.format("Fail to insert operation log %s", getOrElse(model.getOperationType(), "")));
		return model.getOperationLogId();
	}

	@Transactional
	public int update(OperationLogModel model) {
		Assert.isTrue(operationLogMapper.update(OperationLogUtils.parse(model)) == 1, String.format("Fail to update operation log %s", getOrElse(model.getOperationType(), "")));
		return model.getOperationLogId();
	}
}
