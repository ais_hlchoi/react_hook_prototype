package com.dbs.sys.controller.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.sys.model.OperationLogModel;
import com.dbs.sys.service.OperationLogService;

@RestController
@CrossOrigin
@RequestMapping("/internal/operationLogManage")
public class OperationLogManageController {

	@Autowired
	private OperationLogService operationLogService;

	@RequestMapping("/insert")
	public Object insert(@RequestBody OperationLogModel model) {
		int id = operationLogService.insert(model);
		return RestEntity.success(id);
	}

	@RequestMapping("/update")
	public Object update(@RequestBody OperationLogModel model) {
		int id = operationLogService.update(model);
		return RestEntity.success(id);
	}
}
