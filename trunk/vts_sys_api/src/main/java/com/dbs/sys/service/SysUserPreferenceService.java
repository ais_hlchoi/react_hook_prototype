package com.dbs.sys.service;

import java.util.List;

import com.dbs.sys.model.SysUserPreferenceModel;

public interface SysUserPreferenceService {

	List<SysUserPreferenceModel> search(SysUserPreferenceModel model);

	void setUp(SysUserPreferenceModel model);

	Integer searchByFuncId(Integer funcId);
}
