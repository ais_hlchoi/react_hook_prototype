package com.dbs.sys.model;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class SysEmailModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer emailId;

	@JsonInclude
	private String type;

	@JsonInclude
	private String recipient;

	@JsonInclude
	private String recipientCc;

	@JsonInclude
	private String recipientBcc;

	@JsonInclude
	private String subject;


	@JsonInclude
	private String status;

	@JsonInclude
	private String sendDate;

	@JsonInclude
	private String remark;

	@JsonInclude
	private SysEmailDataModel sysEmailDataModel;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("emailId", emailId);
		args.put("type", type);
		args.put("recipient", recipient);
		args.put("recipientCc", recipientCc);
		args.put("recipientBcc", recipientBcc);
		args.put("subject", subject);
		args.put("status", status);
		args.put("sendDate", sendDate);
		args.put("remark", remark);
		args.put("sysEmailDataModel", sysEmailDataModel.getOperationContent());
		return ClassPathUtils.toJSONString(args);
	}
}
