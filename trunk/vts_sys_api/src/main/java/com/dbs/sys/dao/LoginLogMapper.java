package com.dbs.sys.dao;

import java.util.List;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.model.LoginLogDataModel;

public interface LoginLogMapper {

	List<LoginLogDataModel> search(OperationLogForm model);
}
