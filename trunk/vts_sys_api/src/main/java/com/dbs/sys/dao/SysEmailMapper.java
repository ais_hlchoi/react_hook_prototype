package com.dbs.sys.dao;

import com.dbs.sys.model.SysEmailDataModel;
import com.dbs.sys.model.SysEmailModel;
import com.dbs.sys.model.SysFuncModel;

import java.util.List;

public interface SysEmailMapper {



	Integer insert(SysEmailModel sysEmailModel);
	Integer insertContent(SysEmailDataModel sysEmailDataModel);

	Integer update(SysEmailModel sysEmailModel);
	Integer updateStatus(SysEmailModel sysEmailModel);

}
