package com.dbs.sys.controller.internal;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.log.model.OperationLogFormModel;
import com.dbs.module.role.data.SysRoleFuncData;
import com.dbs.module.role.model.SysRoleFormModel;
import com.dbs.sys.model.LoginLogDataModel;
import com.dbs.sys.service.LoginLogService;
import com.dbs.sys.service.SysRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/internal/t")
public class ForTestController {

	@RequestMapping("/get")
	public String update() {
		return "hello world";
	}

	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private LoginLogService loginLogManageService;

	@RequestMapping(value = "/getSideMenu")
	public RestEntity getSideMenu() {
		List<String> roleList = new ArrayList<String>();
		roleList.add("01UREG3_VTSP_ROLE_ADMIN");
		SysRoleFormModel bean = new SysRoleFormModel();
		List<SysRoleFuncData> list = sysRoleService.getRoleFuncList(bean);
		return RestEntity.success(list);
	}

	@RequestMapping("/search")
	public Object search() {
		OperationLogFormModel model = new OperationLogFormModel();
		model.setPage(1);
		model.setPageSize(10);
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<LoginLogDataModel> list = loginLogManageService.search(model);
		return RestEntity.success(new PageInfo<>(list));
	}
}
