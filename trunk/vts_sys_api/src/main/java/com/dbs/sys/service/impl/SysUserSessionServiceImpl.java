package com.dbs.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.dbs.framework.constant.SystemResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.session.SysUserSession;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.constant.UserSessionMode;
import com.dbs.module.session.constant.UserSessionStatus;
import com.dbs.module.session.data.UserSessionData;
import com.dbs.module.session.model.UserSessionStatusModel;
import com.dbs.module.session.util.SysUserSessionUtils;
import com.dbs.module.session.util.UserSessionFormUtils;
import com.dbs.module.session.util.UserSessionStatusUtils;
import com.dbs.sys.dao.SysUserSessionMapper;
import com.dbs.sys.model.SysUserSessionAudModel;
import com.dbs.sys.model.SysUserSessionModel;
import com.dbs.sys.service.SysUserSessionService;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.FortifyStrategyUtils;

@Service
public class SysUserSessionServiceImpl extends AbstractServiceImpl implements SysUserSessionService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SysUserSessionMapper sysUserSessionMapper;

	@Transactional
	public UserSessionData checkAndSaveUserSession(UserSessionForm model) {
		UserSessionData check = checkUserSessionStatus(model);
		UserSessionStatus status = UserSessionStatus.getOrElse(check.getStatus());
		logger.info(String.format("----- Check User Session ----- %s %s", FortifyStrategyUtils.toLogString(model.getUserId(), model.getIp(), model.getSid(), check.getStatus(), status.desc()), FortifyStrategyUtils.toLogString(CurrentUserUtils.maskToken(model.getToken()))));
		switch (status) {
		case A:
			createUserSession(model);
			break;
		case O:
			updateUserSession(model);
			break;
		case E:
			throw new SysRuntimeException(SystemResponseCode.M9000, "Session Expired. Please login again.");
		case F:
			if (UserSessionMode.K == UserSessionMode.getOrElse(model.getMode())) {
				createOrReplaceUserSession(UserSessionFormUtils.parse(model, UserSessionMode.K));
				break;
			}
			throw new SysRuntimeException(SystemResponseCode.M9001, "User is currently online.\nAre you sure to login?");
		case R:
			createOrReplaceUserSession(UserSessionFormUtils.parse(model, UserSessionMode.E));
			break;
		case K:
			updateAndDeleteUserSession(UserSessionFormUtils.parse(model, UserSessionMode.K));
			break;
		case T:
			createUserSessionAudit(UserSessionFormUtils.parse(model, UserSessionMode.U));
			break;
		default:
			break;
		}
		return check;
	}

	public UserSessionData checkUserSessionStatus(UserSessionForm model) {
		return new UserSessionData(getUserSessionList(SysUserSessionUtils.parse(model)), model.getConnect());
	}

	public int getUserSessionCount(SysUserSession model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getUserId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		return sysUserSessionMapper.getUserSessionCount(model);
	}

	@Transactional
	public int createUserSession(UserSessionForm model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getUserId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.notNull(model.getSid(), AssertResponseCode.NOT_NULL.getMsg("Session Id"));
		Assert.notNull(model.getIp(), AssertResponseCode.NOT_NULL.getMsg("IP address"));
		SysUserSessionModel bean = SysUserSessionUtils.parse(model);
		if (isUserFoundInSession(bean)) {
			logger.info(String.format("----- User Session Found ----- %s", FortifyStrategyUtils.toLogString(model.getUserId(), model.getSid())));
			return 1;
		}
		Assert.isTrue(sysUserSessionMapper.insert(bean) == 1, String.format("Fail to insert user session %s", getOrElse(model.getUserId(), "")));
		return 1;
	}

	@Transactional
	public int createOrReplaceUserSession(UserSessionForm model) {
		deleteUserSession(model);
		return createUserSession(model);
	}

	@Transactional
	public int deleteUserSession(UserSessionForm model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (StringUtils.isNotBlank(model.getMode())) {
			List<UserSessionStatusModel> list = getUserSessionList(SysUserSessionUtils.parse(model));
			list.stream().filter(UserSessionStatusUtils.notEqualsSession()).forEach(o -> createUserSessionAudit(UserSessionFormUtils.parse(o.getUserId(), null, model.getIp(), model.getSid(), model.getMode()), o.getSid(), o.getIp()));
		}
		return 1;
	}

	@Transactional
	public int updateUserSession(UserSessionForm model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getUserId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		SysUserSession bean = SysUserSessionUtils.parse(model);
		Assert.isTrue(sysUserSessionMapper.update(bean) == 1, String.format("Fail to update user session %s", getOrElse(model.getUserId(), "")));
		return 1;
	}

	@Transactional
	public int updateAndDeleteUserSession(UserSessionForm model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (StringUtils.isNotBlank(model.getMode())) {
			List<UserSessionStatusModel> list = getUserSessionList(SysUserSessionUtils.parse(model));
			list.stream().filter(UserSessionStatusUtils.notEqualsSession()).forEach(o -> createUserSessionAudit(UserSessionFormUtils.parse(o.getUserId(), null, model.getIp(), model.getSid(), model.getMode()), o.getSid(), o.getIp()));
		}
		return updateUserSession(model);
	}

	@Transactional
	public int createUserSessionAudit(UserSessionForm model) {
		return createUserSessionAudit(model, model.getSid(), model.getIp());
	}

	@Transactional
	public int createUserSessionAudit(UserSessionForm model, String sid, String ip) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getUserId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.notNull(model.getSid(), AssertResponseCode.NOT_NULL.getMsg("Session Id"));
		Assert.notNull(model.getIp(), AssertResponseCode.NOT_NULL.getMsg("IP address"));
		List<UserSessionStatusModel> list = getUserSessionList(SysUserSessionUtils.parse(model));
		if (CollectionUtils.isEmpty(list))
			return 0;
		SysUserSessionAudModel bean = null;
		UserSessionMode mode = UserSessionMode.getOrElse(model.getMode(), UserSessionMode.N);
		switch (mode) {
		case N:
		case E:
		case K:
			bean = SysUserSessionUtils.parse(model, sid, ip, mode.name());
			break;
		case U:
			bean = SysUserSessionUtils.parse(model, UserSessionMode.E.name());
			break;
		default:
		}
		Assert.notNull(bean, String.format("Fail to insert user session audit %s", getOrElse(model.getUserId(), "")));
		Assert.isTrue(sysUserSessionMapper.insertAudit(bean) > 0, String.format("Fail to insert user session audit %s", getOrElse(model.getUserId(), "")));
		Assert.isTrue(sysUserSessionMapper.delete(bean) > 0, String.format("Fail to delete user session %s", getOrElse(model.getUserId(), "")));
		return 1;
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected List<UserSessionStatusModel> getUserSessionList(SysUserSession model) {
		List<UserSessionStatusModel> list = sysUserSessionMapper.getUserSessionList(model);
		return CollectionUtils.isEmpty(list) ? new ArrayList<>() : list;
	}

	protected boolean isUserFoundInSession(SysUserSession model) {
		return getUserSessionList(model).size() > 0;
	}
}
