package com.dbs.sys.model;

import java.util.Date;

import com.dbs.util.OperationLogUtils.OperationLogStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationLogModel extends AbstractAuditModel {

	@JsonInclude
	private String userId;

	@JsonInclude
	private Integer funcId;

	@JsonInclude
	private Integer operationLogId;

	@JsonInclude
	private String operationType;

	@JsonInclude
	private String serviceAddress;

	@JsonInclude
	private String status;

	@JsonInclude
	private String loginIpAddress;

	@JsonInclude
	private String operationContent;

	@JsonInclude
	private Object operationContentObject;

	@JsonInclude
	private Date operationTime;

	@JsonInclude
	private String responseCode;

	@JsonInclude
	private String responseDesc;

	@JsonInclude
	private Date responseTime;

	public OperationLogModel() {
		super();
	}

	public OperationLogModel(String userId, Date operationTime, String operationType, String loginIpAddress, String serviceAddress) {
		super();
		setUserId(userId);
		setOperationType(operationType);
		setServiceAddress(serviceAddress);
		setLoginIpAddress(loginIpAddress);
		setOperationTime(operationTime);
	}

	public OperationLogModel(Date operationTime, String loginIpAddress) {
		super();
		setLoginIpAddress(loginIpAddress);
		setOperationTime(operationTime);
	}

	public OperationLogModel put(String responseCode, String responseDesc) {
		setResponseCode(responseCode);
		setResponseDesc(responseDesc);
		setStatus(null == responseCode ? null : OperationLogStatus.getOrElse(responseCode).name());
		return this;
	}
}
