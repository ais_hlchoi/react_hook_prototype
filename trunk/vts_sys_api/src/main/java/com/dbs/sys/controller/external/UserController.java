package com.dbs.sys.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.role.data.UserInfoData;
import com.dbs.sys.service.SysUserService;

@RestController
@CrossOrigin
@RequestMapping("/api/user/")
public class UserController {

	@Autowired
	private SysUserService sysUserService;

	@RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public Object getUserInfo(@RequestHeader("key") String key, @AuthenticationPrincipal Authentication authentication) {
		UserInfoData item = sysUserService.getUserInfo(authentication);
		return RestEntity.success(item);
	}
}
