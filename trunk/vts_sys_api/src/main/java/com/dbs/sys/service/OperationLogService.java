package com.dbs.sys.service;

import java.util.List;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.model.OperationLogDataModel;
import com.dbs.sys.model.OperationLogModel;

public interface OperationLogService {

	List<OperationLogDataModel> search(OperationLogForm model);

	List<OperationLogDataModel> searchUpload(OperationLogForm model);

	List<OperationLogDataModel> searchLogin(OperationLogForm model);

	int insert(OperationLogModel model);

	int update(OperationLogModel model);
}
