package com.dbs.sys.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleFuncModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer roleFuncId;

	@JsonInclude
	private Integer roleId;

	@JsonInclude
	private Integer funcId;

	@JsonInclude
	private String canSelect;

	@JsonInclude
	private String canInsert;

	@JsonInclude
	private String canUpdate;

	@JsonInclude
	private String canDelete;

	@JsonInclude
	private String canAudit;

	@JsonInclude
	private String canView;

	@JsonInclude
	private String canUpload;

	@JsonInclude
	private String canDownload;

	@JsonInclude
	private String activeInd;

	// extends

	@JsonInclude
	private String funcName;

	@JsonInclude
	private String funcCode;

	@JsonInclude
	private String funcUrl;

	@JsonInclude
	private String type;

	@JsonInclude
	private Integer parentId;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private List<SysRoleFuncModel> funcList;

	@JsonInclude
	private Integer iden;

	@JsonInclude
	private List<SysRoleFuncModel> children;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("roleFuncId", roleFuncId);
		args.put("roleId", roleId);
		args.put("funcId", funcId);
		args.put("canSelect", canSelect);
		args.put("canInsert", canInsert);
		args.put("canUpdate", canUpdate);
		args.put("canDelete", canDelete);
		args.put("canAudit", canAudit);
		args.put("canView", canView);
		args.put("canUpload", canUpload);
		args.put("canDownload", canDownload);
		args.put("activeInd", activeInd);
		args.put("funcName", funcName);
		args.put("funcCode", funcCode);
		args.put("funcUrl", funcUrl);
		args.put("type", type);
		args.put("parentId", parentId);
		args.put("dispSeq", dispSeq);
		args.put("funcList", CollectionUtils.isEmpty(funcList) ? null : funcList.stream().map(SysRoleFuncModel::operationContent).collect(Collectors.toList()));
		args.put("iden", iden);
		args.put("children", CollectionUtils.isEmpty(children) ? null : children.stream().map(SysRoleFuncModel::operationContent).collect(Collectors.toList()));
		return ClassPathUtils.toJSONString(args);
	}
}
