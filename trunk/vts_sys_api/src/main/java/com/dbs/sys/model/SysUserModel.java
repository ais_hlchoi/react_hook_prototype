package com.dbs.sys.model;

import java.util.LinkedHashMap;
import java.util.Map;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysUserModel extends CommonParentModel {

    private static final long serialVersionUID = 1L;

    private String userCode;
    private String userName;
    private String companyCode;
    private String companyName;
    private String pwd;
    private String salt;
    private String email;
    private String sex;
    private String activeInd;
    private String position;
    private String accPwGroup;
    private String desc;
    private String fax;
    private String address;
    private String phone;
    private String postalCode;
    private String qq;
    private String homePhone;
    private String officePhone;
    private String name;
    private String middleName;
    private String surname;
    private String countryCode;
    private String maritalStatus;
    private String birthPlace;
    private String birthday;
    private String token;
    private Integer roleId;
    private String defaultInd;

	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("userCode", userCode);
		args.put("userName", userName);
		args.put("roleId", roleId);
		args.put("activeInd", activeInd);
		args.put("defaultInd", defaultInd);
		return ClassPathUtils.toJSONString(args);
	}
}