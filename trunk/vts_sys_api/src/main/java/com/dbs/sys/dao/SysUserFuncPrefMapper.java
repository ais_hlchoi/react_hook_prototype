package com.dbs.sys.dao;

import java.util.List;

import com.dbs.sys.model.SysUserPreferenceModel;

public interface SysUserFuncPrefMapper {

	List<SysUserPreferenceModel> search(SysUserPreferenceModel userPreferenceModel);

	Integer searchByFuncId(Integer funcId);

	int insertAll(SysUserPreferenceModel userPreferenceModel);

	int deleteByUserId(String userId);

	int deleteByFuncId(Integer funcId);
}
