package com.dbs.sys.service;

import org.springframework.security.core.Authentication;

import com.dbs.module.role.data.UserInfoData;

public interface SysUserService {

	UserInfoData getUserInfo(Authentication model);
}
