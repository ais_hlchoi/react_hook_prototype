package com.dbs.sys.model;

import com.dbs.module.log.SysLog;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginLogDataModel extends OperationLogModel implements SysLog {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String userCode;

	@JsonInclude
	private String userName;
}
