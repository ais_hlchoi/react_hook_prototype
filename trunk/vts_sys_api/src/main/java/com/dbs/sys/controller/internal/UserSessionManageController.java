package com.dbs.sys.controller.internal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.constant.UserSessionConnect.ClientStatus;
import com.dbs.module.session.constant.UserSessionMode;
import com.dbs.module.session.data.UserSessionData;
import com.dbs.module.session.util.UserSessionFormUtils;
import com.dbs.sys.service.SysUserSessionService;
import com.dbs.util.CurrentUserUtils;

@RestController
@CrossOrigin
@RequestMapping("/internal/userSessionManage")
public class UserSessionManageController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private SysUserSessionService sysUserSessionService;

	@RequestMapping("/open")
	public RestEntity open() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis()).connect(ClientStatus.CREATE);
		UserSessionData item = sysUserSessionService.checkAndSaveUserSession(model);
		return RestEntity.success(item);
	}

	@RequestMapping("/connect")
	public RestEntity connect() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis()).connect(ClientStatus.UPDATE);
		UserSessionData item = sysUserSessionService.checkAndSaveUserSession(model);
		return RestEntity.success(item);
	}

	@RequestMapping("/reconnect")
	public RestEntity reconnect() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis()).connect(ClientStatus.UPDATE);
		sysUserSessionService.createOrReplaceUserSession(UserSessionFormUtils.parse(model, UserSessionMode.K));
		return RestEntity.success();
	}

	@RequestMapping("/disconnect")
	public RestEntity disconnect() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis()).connect(ClientStatus.DELETE);
		sysUserSessionService.createUserSessionAudit(UserSessionFormUtils.parse(model, UserSessionMode.N));
		return RestEntity.success();
	}

	@RequestMapping("/reset")
	public RestEntity reset() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis());
		sysUserSessionService.deleteUserSession(UserSessionFormUtils.parse(model, UserSessionMode.K));
		return RestEntity.success();
	}
}
