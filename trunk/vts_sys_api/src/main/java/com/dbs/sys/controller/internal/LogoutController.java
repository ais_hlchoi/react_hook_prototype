package com.dbs.sys.controller.internal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.UserSession.UserSessionAction;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.util.UserSessionFormUtils;
import com.dbs.module.session.util.UserSessionManageUtils;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.RequestHeaderUtils;
import com.dbs.util.AppUserUtils;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LogoutController {

	@Autowired
	private HttpServletRequest request;

	@Value("${sys.sid.url}")
	private String sysSidApi;

	@Value("${user.logout.url}")
	private String userLogoutUrl;

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public RestEntity logout() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis());
		AppUserUtils.disconnect(model, userLogoutUrl);
		UserSessionManageUtils.reconnect(model, sysSidApi, RequestHeaderUtils.getAuthorization(request), UserSessionAction.DISCONNECT);
		return RestEntity.success();
	}

	@RequestMapping(value = "/reconnect", method = RequestMethod.POST)
	public RestEntity reconnect() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis());
		UserSessionManageUtils.reconnect(model, sysSidApi, RequestHeaderUtils.getAuthorization(request), UserSessionAction.RECONNECT);
		return RestEntity.success();
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public RestEntity reset() {
		UserSessionForm model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis());
		UserSessionManageUtils.reconnect(model, sysSidApi, RequestHeaderUtils.getAuthorization(request), UserSessionAction.RESET);
		return RestEntity.success();
	}
}
