package com.dbs.sys.controller.internal;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.role.data.UserInfoData;
import com.dbs.sys.model.SysEmailModel;
import com.dbs.sys.model.SysFuncModel;
import com.dbs.sys.service.SysEmailService;
import com.dbs.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/internal/sysEmail")
public class SysEmailController {

	@Autowired
	private SysEmailService sysEmailService;

	@RequestMapping(value = "/createSysEmail", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public Object createSysEmail(@RequestBody SysEmailModel model) {
		int id = sysEmailService.createSysEmail(model);
		return RestEntity.success(id);
	}
}
