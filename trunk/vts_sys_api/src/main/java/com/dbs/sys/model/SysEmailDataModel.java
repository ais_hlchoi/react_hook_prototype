package com.dbs.sys.model;

import com.dbs.framework.model.CommonParentModel;
import com.dbs.util.ClassPathUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
public class SysEmailDataModel extends CommonParentModel {

	private static final long serialVersionUID = 1L;

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer emailId;

	@JsonInclude
	private String content;

	@JsonInclude
	private String partitionInd;



	public String operationContent() {
		Map<String, Object> args = new LinkedHashMap<>();
		args.put("emailId", emailId);
		args.put("content", content);
		args.put("partitionInd", partitionInd);
		return ClassPathUtils.toJSONString(args);
	}
}
