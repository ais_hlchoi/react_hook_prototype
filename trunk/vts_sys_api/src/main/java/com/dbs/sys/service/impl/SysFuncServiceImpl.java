package com.dbs.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.func.data.SysFuncData;
import com.dbs.sys.dao.SysFuncMapper;
import com.dbs.sys.dao.SysRoleFuncMapper;
import com.dbs.sys.dao.SysUserFuncPrefMapper;
import com.dbs.sys.model.SysFuncModel;
import com.dbs.sys.service.SysFuncService;
import com.dbs.util.Assert;

@Service
public class SysFuncServiceImpl extends AbstractServiceImpl implements SysFuncService {

	@Autowired
	private SysFuncMapper sysFuncMapper;

	@Autowired
	private SysRoleFuncMapper sysRoleFuncMapper;

	@Autowired
	private SysUserFuncPrefMapper sysUserFuncPrefMapper;

	private static final String SQL_ERR_INSERT = "SQL_ERR_INSERT";
	private static final String SQL_ERR_UPDATE = "SQL_ERR_UPDATE";
	private static final String SQL_ERR_DELETE = "SQL_ERR_DELETE";

	private static final String FUNC_CODE = "FuncCode";
	private static final String SEQ = "Seq";

	@Override
	public List<SysFuncModel> search(SysFuncModel model) {
		return sysFuncMapper.search(model);
	}

	@Override
	public List<SysFuncData> searchAll(SysFuncModel model) {
		List<SysFuncModel> list = sysFuncMapper.search(model);
		return null == list ? new ArrayList<>() : list.stream().map(SysFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysFuncData> searchFunc(SysFuncModel model) {
		List<SysFuncModel> list = sysFuncMapper.searchFunc(model);
		return null == list ? new ArrayList<>() : list.stream().map(SysFuncData::new).collect(Collectors.toList());
	}

	@Override
	public List<SysFuncData> searchPreferenceMenu(SysFuncModel model) {
		List<SysFuncModel> list = sysFuncMapper.searchPreferenceMenu(model);
		return null == list ? new ArrayList<>() : list.stream().map(SysFuncData::new).collect(Collectors.toList());
	}

	@Override
	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional(rollbackFor = Exception.class)
	public int insert(SysFuncModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		Assert.isTrue(sysFuncMapper.checkFuncCode(model) == 0, AssertResponseCode.EXISTS.getMsg(FUNC_CODE));
		if ("MENU".equals(model.getType())) {
			Assert.isTrue(sysFuncMapper.checkDispSeq1(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		} else {
			Assert.isTrue(sysFuncMapper.checkDispSeq2(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		}
		Assert.isTrue(sysFuncMapper.insert(model) == 1, SQL_ERR_INSERT);
		return model.getFuncId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional(rollbackFor = Exception.class)
	public int update(SysFuncModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		if ("MENU".equals(model.getType())) {
			Assert.isTrue(sysFuncMapper.checkDispSeq1(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		} else {
			Assert.isTrue(sysFuncMapper.checkDispSeq2(model) == 0, AssertResponseCode.EXISTS.getMsg(SEQ));
		}
		Assert.isTrue(sysFuncMapper.update(model) == 1, SQL_ERR_UPDATE);
		return model.getFuncId();
	}

	@Override
	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional(rollbackFor = Exception.class)
	public int delete(SysFuncModel model) {
		Assert.isTrue(sysFuncMapper.searchSubRecord(model) == 0, "Sub-Record exists");
		int num = 0;
		num = sysUserFuncPrefMapper.searchByFuncId(model.getFuncId());
		Assert.isTrue(sysUserFuncPrefMapper.deleteByFuncId(model.getFuncId()) == num, SQL_ERR_DELETE);
		num = sysRoleFuncMapper.searchByFuncId(model.getFuncId());
		Assert.isTrue(sysRoleFuncMapper.deleteByFuncId(model.getFuncId()) == num, SQL_ERR_DELETE);
		num = sysRoleFuncMapper.searchByFuncIdChk(model.getFuncId());
		Assert.isTrue(sysRoleFuncMapper.deleteByFuncIdChk(model.getFuncId()) == num, SQL_ERR_DELETE);
		Assert.isTrue(sysFuncMapper.delete(model) == 1, SQL_ERR_DELETE);
		return model.getFuncId();
	}

	/**
	 * @param SysFuncModel
	 * @return
	 */
	private String check(SysFuncModel model) {
		List<String> msg = new ArrayList<>();
		Assert.notBlank(model.getFuncCode(), AssertResponseCode.NOT_EMPTY.getMsg(FUNC_CODE), msg);
		Assert.notBlank(model.getFuncName(), AssertResponseCode.NOT_EMPTY.getMsg("Module Name"), msg);
		Assert.notBlank(model.getType(), AssertResponseCode.NOT_EMPTY.getMsg("Module Type"), msg);
		if (StringUtils.isNotBlank(model.getType())) {
			Assert.notNull(model.getDispSeq(), AssertResponseCode.NOT_EMPTY.getMsg(SEQ), msg);
			Assert.isFalse("FUNC".equals(model.getType()) && model.getParentId() == null, AssertResponseCode.NOT_EMPTY.getMsg("Level 1 Module"), msg);
		}
		return CollectionUtils.isEmpty(msg) ? null : StringUtils.join(msg, ";");
	}
}
