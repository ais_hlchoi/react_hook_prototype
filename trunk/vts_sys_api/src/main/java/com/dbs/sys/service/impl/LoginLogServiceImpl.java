package com.dbs.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.dao.LoginLogMapper;
import com.dbs.sys.model.LoginLogDataModel;
import com.dbs.sys.service.LoginLogService;

@Service
public class LoginLogServiceImpl implements LoginLogService {

	@Autowired
	private LoginLogMapper loginLogMapper;

	@Override
	public List<LoginLogDataModel> search(OperationLogForm model) {
		return loginLogMapper.search(model);
	}
}
