package com.dbs.sys.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.role.data.SysRoleData;
import com.dbs.module.role.data.SysRoleFuncData;
import com.dbs.sys.model.SysRoleFuncModel;
import com.dbs.sys.model.SysRoleModel;
import com.dbs.sys.service.SysRoleService;
import com.dbs.util.PageInfoUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/sysRole")
public class SysRoleController {

	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping("/search")
	public Object search(@RequestBody SysRoleModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<SysRoleModel> list = sysRoleService.search(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SysRoleData.class));
	}

	@RequestMapping("/searchAll")
	public Object searchAll(@RequestBody SysRoleModel model) {
		List<SysRoleData> list = sysRoleService.searchAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchRoleFunc")
	public Object searchRoleFunc(@RequestBody SysRoleFuncModel model) {
		List<SysRoleFuncData> list = sysRoleService.searchRoleFunc(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchRoleFuncChk")
	public Object searchRoleFuncChk(@RequestBody SysRoleFuncModel model) {
		List<SysRoleFuncData> list = sysRoleService.searchRoleFuncChk(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchAllFunc")
	public Object searchAllFunc(@RequestBody SysRoleFuncModel model) {
		List<SysRoleFuncData> list = sysRoleService.searchAllFunc(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/searchFunc")
	public Object searchFunc(@RequestBody SysRoleFuncModel model) {
		List<SysRoleFuncData> list = sysRoleService.searchFunc(model);
		return RestEntity.success(list);
	}

	@RequestMapping("/selectChk")
	public Object selectChk(@RequestBody SysRoleModel model) {
		SysRoleData item = sysRoleService.searchChk(model);
		return RestEntity.success(item);
	}

	@RequestMapping("/insert")
	public Object insert(@RequestBody SysRoleModel model) {
		int id = sysRoleService.insert(model);
		return RestEntity.success(id);
	}

	@RequestMapping("/update")
	public Object update(@RequestBody SysRoleModel model) {
		int id = sysRoleService.update(model);
		return RestEntity.success(id);
	}

	@RequestMapping("/reverse")
	@PreAuthorize("isAuthenticated()")
	public Object reverse(@RequestBody SysRoleModel model) {
		int id = sysRoleService.reverse(model);
		return RestEntity.success(id);
	}

	@RequestMapping("/delete")
	public Object delete(@RequestBody SysRoleModel model) {
		int id = sysRoleService.delete(model);
		return RestEntity.success(id);
	}

	@RequestMapping("/authorize")
	@PreAuthorize("isAuthenticated()")
	public Object authorize(@RequestBody SysRoleModel model) {
		int id = sysRoleService.authorize(model);
		return RestEntity.success(id);
	}
}
