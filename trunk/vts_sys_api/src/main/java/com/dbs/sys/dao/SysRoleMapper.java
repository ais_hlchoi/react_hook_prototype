package com.dbs.sys.dao;

import java.util.List;

import com.dbs.sys.model.SysRoleModel;

public interface SysRoleMapper {

	List<SysRoleModel> search(SysRoleModel sysRoleModel);
	
	SysRoleModel searchChk(SysRoleModel sysRoleModel);

	int insert(SysRoleModel sysRoleModel);

	int update(SysRoleModel sysRoleModel);

	int reverse(SysRoleModel sysRoleModel);

	int delete(SysRoleModel sysRoleModel);

	int onlyDispSeq(SysRoleModel sysRoleModel);

	int onlyRoleCode(SysRoleModel sysRoleModel);

	int deleteMak(SysRoleModel sysRoleModel);
	
	int deleteChk(SysRoleModel sysRoleModel);
	
	int makToChk(SysRoleModel sysRoleModel);
	
	int chkToAud(SysRoleModel sysRoleModel);
	
	int makAuth(SysRoleModel sysRoleModel);
}
