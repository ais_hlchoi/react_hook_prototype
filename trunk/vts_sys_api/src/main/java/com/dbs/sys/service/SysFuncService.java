package com.dbs.sys.service;

import java.util.List;

import com.dbs.module.func.data.SysFuncData;
import com.dbs.sys.model.SysFuncModel;

public interface SysFuncService {

	List<SysFuncModel> search(SysFuncModel model);

	List<SysFuncData> searchAll(SysFuncModel model);

	List<SysFuncData> searchPreferenceMenu(SysFuncModel model);

	List<SysFuncData> searchFunc(SysFuncModel model);

	int insert(SysFuncModel model);

	int update(SysFuncModel model);

	int delete(SysFuncModel model);
}
