package com.dbs.sys.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SysUserSessionAudModel extends SysUserSessionModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String sidAud;

	@JsonInclude
	private String ipAud;

	@JsonInclude
	private Date logoutTime;

	@JsonInclude
	private String logoutMode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSidAud() {
		return sidAud;
	}

	public void setSidAud(String sidAud) {
		this.sidAud = sidAud;
	}

	public String getIpAud() {
		return ipAud;
	}

	public void setIpAud(String ipAud) {
		this.ipAud = ipAud;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getLogoutMode() {
		return logoutMode;
	}

	public void setLogoutMode(String logoutMode) {
		this.logoutMode = logoutMode;
	}
}
