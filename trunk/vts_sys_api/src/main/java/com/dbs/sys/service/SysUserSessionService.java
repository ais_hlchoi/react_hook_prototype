package com.dbs.sys.service;

import com.dbs.module.session.SysUserSession;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.data.UserSessionData;

public interface SysUserSessionService {

	UserSessionData checkAndSaveUserSession(UserSessionForm model);

	UserSessionData checkUserSessionStatus(UserSessionForm model);

	int getUserSessionCount(SysUserSession model);

	int createUserSession(UserSessionForm model);

	int createOrReplaceUserSession(UserSessionForm model);

	int deleteUserSession(UserSessionForm model);

	int updateUserSession(UserSessionForm model);

	int updateAndDeleteUserSession(UserSessionForm model);

	int createUserSessionAudit(UserSessionForm model);

	int createUserSessionAudit(UserSessionForm model, String sid, String ip);
}