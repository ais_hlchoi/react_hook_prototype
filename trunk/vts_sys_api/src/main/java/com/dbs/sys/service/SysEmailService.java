package com.dbs.sys.service;

import com.dbs.module.role.data.UserInfoData;
import com.dbs.sys.model.SysEmailModel;
import org.springframework.security.core.Authentication;

public interface SysEmailService {

	int createSysEmail(SysEmailModel model);
}
