package com.dbs.sys.service.impl;

import com.dbs.sys.dao.SysEmailMapper;
import com.dbs.sys.model.SysEmailDataModel;
import com.dbs.sys.model.SysEmailModel;
import com.dbs.sys.service.SysEmailService;
import com.dbs.util.Assert;
import com.dbs.util.EmailStatusEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SysEmailServiceImpl extends AbstractServiceImpl implements SysEmailService {
	@Autowired
	private SysEmailMapper sysEmailMapper;


	private static final String TYPE = "Type";
	private static final String RECIPIENT = "Recipient";
	private static final String SUBJECT = "Subject";
	private static final String CONTENT = "Content";
	private static final String SENDDATE = "Send Date";
	private static final String REMARK = "Remark";

	public int createSysEmail(SysEmailModel model) {
		String msg = check(model);
		Assert.isNull(msg, msg);
		if(model.getSendDate()==null || model.getSendDate().isEmpty()){
			model.setSendDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		}
		model.setStatus(EmailStatusEnum.PENDING.value());
		Assert.isTrue(sysEmailMapper.insert(model) == 1, SQL_ERR_INSERT);
		SysEmailDataModel sysEmailData = model.getSysEmailDataModel();
		sysEmailData.setEmailId(model.getEmailId());
		sysEmailData.setPartitionInd(new SimpleDateFormat("yyyy-MM").format(new Date()));
		Assert.isTrue(sysEmailMapper.insertContent(sysEmailData) == 1, SQL_ERR_INSERT);


		return model.getEmailId();
	}

	private String check(SysEmailModel model) {
		List<String> msg = new ArrayList<>();
		Assert.notBlank(model.getType(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(TYPE), msg);
		Assert.notBlank(model.getRecipient(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(RECIPIENT), msg);
		Assert.notBlank(model.getSubject(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(SUBJECT), msg);
		Assert.notNull(model.getSysEmailDataModel(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(CONTENT), msg);
		Assert.notBlank(model.getSysEmailDataModel().getContent(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(CONTENT), msg);
		//Assert.notBlank(model.getSendDate(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(SENDDATE), msg);
		//Assert.notBlank(model.getRemark(), AbstractServiceImpl.AssertResponseCode.NOT_EMPTY.getMsg(REMARK), msg);

		return CollectionUtils.isEmpty(msg) ? null : StringUtils.join(msg, ";");
	}
}
