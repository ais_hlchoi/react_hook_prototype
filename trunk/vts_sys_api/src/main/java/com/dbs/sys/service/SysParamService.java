package com.dbs.sys.service;

import java.util.List;

import com.dbs.sys.model.SysParmModel;

public interface SysParamService {

	List<SysParmModel> search(SysParmModel model);

	List<SysParmModel> searchAll(SysParmModel model);

	List<SysParmModel> searchSysParmSegment(SysParmModel model);

	List<SysParmModel> searchSysParmValueBySegment(SysParmModel model);

	Object insert(SysParmModel model);

	Object update(SysParmModel model);

	Object delete(SysParmModel model);
}
