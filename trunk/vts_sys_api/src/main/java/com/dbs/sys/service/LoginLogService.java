package com.dbs.sys.service;

import java.util.List;

import com.dbs.module.log.OperationLogForm;
import com.dbs.sys.model.LoginLogDataModel;

public interface LoginLogService {

	List<LoginLogDataModel> search(OperationLogForm model);
}
