package com.dbs.module.session.util;

import java.sql.Timestamp;

import com.dbs.module.session.SysUserSession;
import com.dbs.module.session.UserSessionForm;
import com.dbs.sys.model.SysUserSessionAudModel;
import com.dbs.sys.model.SysUserSessionModel;

public class SysUserSessionUtils {

	private SysUserSessionUtils() {
		// for SonarQube scanning purpose
	}

	public static SysUserSession parse(final String userId) {
		SysUserSessionModel o = new SysUserSessionModel();
		o.setUserId(userId);
		return o;
	}

	public static SysUserSessionModel parse(final UserSessionForm model) {
		if (null == model)
			return null;

		SysUserSessionModel o = new SysUserSessionModel();
		o.setUserId(model.getUserId());
		o.setSid(model.getSid());
		o.setIp(model.getIp());
		if (null != model.getTime()) {
			o.setLoginTime(new Timestamp(model.getTime()));
		}
		return o;
	}

	public static SysUserSessionAudModel parse(final UserSessionForm model, final String mode) {
		return parse(model, null, null, mode);
	}

	public static SysUserSessionAudModel parse(final UserSessionForm model, final String sid, final String ip, final String mode) {
		if (null == model)
			return null;

		SysUserSessionAudModel o = new SysUserSessionAudModel();
		o.setUserId(model.getUserId());
		o.setSid(sid);
		o.setIp(ip);
		o.setSidAud(model.getSid());
		o.setIpAud(model.getIp());
		o.setLogoutMode(mode);
		return o;
	}
}
