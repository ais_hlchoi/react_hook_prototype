package com.dbs.module.session.constant;

public enum UserSessionConnect {

	OPEN_READY(ClientStatus.CREATE, ServerStatus.EMPTY),
	OPEN_CHECK(ClientStatus.CREATE, ServerStatus.ONE),
	OPEN_ERROR(ClientStatus.CREATE, ServerStatus.MANY),
	CONN_READY(ClientStatus.UPDATE, ServerStatus.ONE),
	CONN_CHECK(ClientStatus.UPDATE, ServerStatus.MANY),
	CONN_ERROR(ClientStatus.UPDATE, ServerStatus.EMPTY),
	QUIT_READY(ClientStatus.DELETE, ServerStatus.ONE),
	QUIT_CHECK(ClientStatus.DELETE, ServerStatus.MANY),
	QUIT_ERROR(ClientStatus.DELETE, ServerStatus.EMPTY),
	;

	private ClientStatus clientStatus;
	private ServerStatus serverStatus;
	private UserSessionConnect(ClientStatus clientStatus, ServerStatus serverStatus) {
		this.clientStatus = clientStatus;
		this.serverStatus = serverStatus;
	}

	public int getClientIndex() {
		return clientStatus.index;
	}

	public int getServerIndex() {
		return serverStatus.index;
	}

	public enum ClientStatus {
		CREATE(1),
		DELETE(2),
		UPDATE(3),
		;
		private final int index;
		private ClientStatus(int index) {
			this.index = index;
		}
	}

	public enum ServerStatus {
		EMPTY(0),
		ONE(1),
		MANY(2),
		;
		private final int index;
		private ServerStatus(int index) {
			this.index = index;
		}
	}
}
