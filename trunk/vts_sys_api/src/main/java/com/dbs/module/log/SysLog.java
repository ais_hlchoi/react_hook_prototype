package com.dbs.module.log;

import java.util.Date;

public interface SysLog {

	Integer getOperationLogId();

	String getOperationContent();

	String getOperationType();

	Date getOperationTime();

	String getStatus();

	String getLoginIpAddress();

	String getServiceAddress();

	String getUserCode();

	String getUserName();

	String getCreatedBy();

	Date getCreatedDate();

	String getLastUpdatedBy();

	Date getLastUpdatedDate();
}
