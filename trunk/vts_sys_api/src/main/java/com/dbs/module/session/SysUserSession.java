package com.dbs.module.session;

import java.sql.Timestamp;

public interface SysUserSession {

	String getUserId();

	String getSid();

	String getIp();

	Timestamp getLoginTime();
}
