package com.dbs.module.session.data;

import java.util.List;

import org.springframework.util.Assert;

import com.dbs.module.session.constant.UserSessionConnect.ClientStatus;
import com.dbs.module.session.constant.UserSessionStatus;
import com.dbs.module.session.model.UserSessionStatusModel;
import com.dbs.module.session.util.UserSessionStatusUtils;

public class UserSessionData {

	private List<UserSessionStatusModel> data;
	protected ClientStatus connect;
	protected UserSessionStatus status;

	public UserSessionData(List<UserSessionStatusModel> data, ClientStatus connect) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
		this.connect = connect;
		this.status = UserSessionStatusUtils.parse(data);
	}

	public UserSessionData(UserSessionStatus status) {
		Assert.notNull(data, "Data cannot be null");
		this.status = status;
	}

	protected List<UserSessionStatusModel> getData() {
		return data;
	}

	protected void setData(List<UserSessionStatusModel> data) {
		this.data = data;
	}

	public String getStatus() {
		return null == status ? UserSessionStatus.I.name() : status.name();
	}

	public String getLabel() {
		return null == status ? UserSessionStatus.I.desc() : status.desc();
	}

	public ClientStatus getConnect() {
		return null == connect ? ClientStatus.UPDATE : connect;
	}
}
