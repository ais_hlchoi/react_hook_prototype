package com.dbs.module.role.data;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

public class UserInfoData {

	private static final String USER_ROLE_ANONYMOUS = "ANONYMOUS";

	private Authentication data;

	public UserInfoData(Authentication data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected Authentication getData() {
		return data;
	}

	protected void setData(Authentication data) {
		this.data = data;
	}

	public UserInfoProps getUserInfo() {
		return new UserInfoProps(getData());
	}

	public class UserInfoProps {
		private Authentication data;

		public UserInfoProps(Authentication data) {
			this.data = data;
		}

		protected Authentication getData() {
			return data;
		}

		public String getUserId() {
			return getData().getName();
		}

		public String getUserCode() {
			return getData().getName();
		}

		public String getUserName() {
			return getData().getName();
		}

		public List<String> getRoleList() {
			if (CollectionUtils.isEmpty(getData().getAuthorities()))
				return Arrays.asList(USER_ROLE_ANONYMOUS);

			List<String> list = getData().getAuthorities().stream().map(GrantedAuthority::getAuthority).filter(StringUtils::isNotBlank).collect(Collectors.toList());
			return CollectionUtils.isEmpty(list) ? Arrays.asList(USER_ROLE_ANONYMOUS) : list;
		}
	}
}
