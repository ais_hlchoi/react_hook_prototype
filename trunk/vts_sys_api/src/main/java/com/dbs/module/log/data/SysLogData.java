package com.dbs.module.log.data;

import java.util.Date;

import org.springframework.util.Assert;

import com.dbs.module.log.SysLog;
import com.dbs.sys.model.LoginLogDataModel;
import com.dbs.sys.model.OperationLogDataModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class SysLogData {

	private SysLog data;

	public SysLogData(LoginLogDataModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	public SysLogData(OperationLogDataModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	@JsonIgnore
	public SysLog getData() {
		return data;
	}

	protected void setData(SysLog data) {
		this.data = data;
	}

	public Integer getOperationLogId() {
		return getData().getOperationLogId();
	}

	public String getOperationContent() {
		return getData().getOperationContent();
	}

	public String getOperationType() {
		return getData().getOperationType();
	}

	public Date getOperationTime() {
		return getData().getOperationTime();
	}

	public String getStatus() {
		return getData().getStatus();
	}

	public String getLoginIpAddress() {
		return getData().getLoginIpAddress();
	}

	public String getServiceAddress() {
		return getData().getServiceAddress();
	}

	public String getUserCode() {
		return getData().getUserCode();
	}

	public String getUserName() {
		return getData().getUserName();
	}

	// for Audit Model

	public String getCreatedBy() {
		return getData().getCreatedBy();
	}

	public Date getCreatedDate() {
		return getData().getCreatedDate();
	}

	public String getLastUpdatedBy() {
		return getData().getLastUpdatedBy();
	}

	public Date getLastUpdatedDate() {
		return getData().getLastUpdatedDate();
	}
}
