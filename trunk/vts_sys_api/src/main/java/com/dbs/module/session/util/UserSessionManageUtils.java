package com.dbs.module.session.util;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.framework.aspect.annotation.UserSession.UserSessionAction;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.constant.UserSessionStatus;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.ApiUtils;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.ResponseCodeUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public class UserSessionManageUtils extends AbstractModuleUtils {

	private UserSessionManageUtils() {
		// for SonarQube scanning purpose
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static UserSessionStatus connect(final UserSessionForm model, final String url, final String auth) {
		if (StringUtils.isBlank(url))
			return UserSessionStatus.I;

		String action = ClassPathUtils.getName().split("\\.")[1];
		String api = String.format("%s/%s", url, action);
		return new UserSessionManageUtils().check(model, api, auth, action);
	}

	public static void reconnect(final UserSessionForm model, final String url, final String auth, final UserSessionAction act) {
		if (StringUtils.isBlank(url) || null == act)
			return;

		switch (act) {
		case RECONNECT:
		case DISCONNECT:
		case RESET:
			String action = act.name().toLowerCase();
			String api = String.format("%s/%s", url, action);
			new UserSessionManageUtils().save(model, api, auth, action);
			break;
		default:
			return;
		}
	}

	protected UserSessionStatus check(UserSessionForm model, String api, String auth, String action) {
		UserSessionStatus o = UserSessionStatus.I;
		try {
			JsonNode node = ApiUtils.post(api, model, Collections.singletonMap("Authorization", auth));
			ResponseCode responseCode = ResponseCodeUtils.getOrElse(node.get("responseCode").asText());
			if (ResponseCodeUtils.isNotSuccess(responseCode)) {
				String responseDesc = (String) node.get("responseDesc").asText();
				logger.error(String.format(MSG_FAIL_FORMAT, FortifyStrategyUtils.toLogString(action, api), FortifyStrategyUtils.toLogString(responseDesc)));
				throw new SysRuntimeException(responseCode, responseDesc);
			}
			o = UserSessionStatus.getOrElse(node.get("object").get("status").asText(), UserSessionStatus.I);
		} catch (JsonProcessingException e) {
			logger.error(String.format(MSG_FAIL_FORMAT, FortifyStrategyUtils.toLogString(action, api), FortifyStrategyUtils.toErrString(e)));
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), String.format("Error found when %s user session", ClassPathUtils.getName().split(".")[1]));
		}
		return o;
	}

	protected void save(UserSessionForm model, String api, String auth, String action) {
		try {
			JsonNode node = ApiUtils.post(api, model, Collections.singletonMap("Authorization", auth));
			ResponseCode responseCode = ResponseCodeUtils.getOrElse(node.get("responseCode").asText());
			if (ResponseCodeUtils.isNotSuccess(responseCode)) {
				String responseDesc = (String) node.get("responseDesc").asText();
				logger.error(String.format(MSG_FAIL_FORMAT, FortifyStrategyUtils.toLogString(action, api), FortifyStrategyUtils.toLogString(responseDesc)));
				throw new SysRuntimeException(responseCode, responseDesc);
			}
		} catch (JsonProcessingException e) {
			logger.error(String.format(MSG_FAIL_FORMAT, FortifyStrategyUtils.toLogString(action, api), FortifyStrategyUtils.toErrString(e)));
			throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), String.format("Error found when %s user session", ClassPathUtils.getName().split(".")[1]));
		}
	}
}
