package com.dbs.module.role.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.role.SysRoleForm;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleFormModel extends AbstractUserModel implements SysRoleForm {

	@JsonInclude
	private List<String> roleList;

	public SysRoleFormModel(List<String> roleList) {
		setRoleList(roleList);
	}

	public SysRoleFormModel() {
	}

	public SysRoleFormModel put(String... args) {
		if (null == roleList)
			roleList = new ArrayList<>();
		Stream.of(args).forEach(o -> roleList.add(o));
		return this;
	}
}
