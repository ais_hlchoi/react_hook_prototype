package com.dbs.module.session.util;

import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.constant.UserSessionMode;
import com.dbs.module.session.model.UserSessionFormModel;

public class UserSessionFormUtils {

	private UserSessionFormUtils() {
		// for SonarQube scanning purpose
	}

	public static UserSessionForm parse(final String userId) {
		UserSessionFormModel o = new UserSessionFormModel();
		o.setUserId(userId);
		return o;
	}

	public static UserSessionForm parse(final String userId, final String sid, final Long time) {
		UserSessionFormModel o = new UserSessionFormModel();
		o.setUserId(userId);
		o.setSid(sid);
		o.setTime(time);
		return o;
	}

	public static UserSessionForm parse(final String userId, final String token, final String ip, final String sid, final Long time) {
		UserSessionFormModel o = new UserSessionFormModel();
		o.setUserId(userId);
		o.setToken(token);
		o.setSid(sid);
		o.setIp(ip);
		o.setTime(time);
		return o;
	}

	public static UserSessionForm parse(final String userId, final String token, final String ip, final String sid, final String mode) {
		UserSessionFormModel o = new UserSessionFormModel();
		o.setUserId(userId);
		o.setToken(token);
		o.setSid(sid);
		o.setIp(ip);
		o.setMode(UserSessionMode.getOrElse(mode, UserSessionMode.N).name());
		return o;
	}

	public static UserSessionForm parse(final UserSessionForm model, final UserSessionMode mode) {
		if (null == model)
			return null;

		return parse(model.getUserId(), model.getToken(), model.getIp(), model.getSid(), null == mode ? null : mode.name());
	}
}
