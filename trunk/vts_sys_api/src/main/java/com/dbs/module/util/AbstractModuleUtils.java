package com.dbs.module.util;

import org.apache.commons.lang3.StringUtils;

public abstract class AbstractModuleUtils {

	protected AbstractModuleUtils() {
		// for SonarQube scanning purpose
	}

	protected static final String MSG_FAIL_FORMAT = "Error found when %s caused by %s";

	protected static <T> T getOrElse(T value, T valueIfNull) {
		if (value instanceof String)
			return StringUtils.isBlank((String) value) ? valueIfNull : value;

		return null == value ? valueIfNull : value;
	}
}
