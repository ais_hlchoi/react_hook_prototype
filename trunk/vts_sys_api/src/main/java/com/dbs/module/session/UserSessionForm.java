package com.dbs.module.session;

import com.dbs.module.model.UserForm;
import com.dbs.module.session.constant.UserSessionMode;
import com.dbs.module.session.constant.UserSessionConnect.ClientStatus;

public interface UserSessionForm extends UserForm {

	String getUserId();

	String getToken();

	String getSid();

	String getIp();

	String getMode();

	Long getTime();

	ClientStatus getConnect();

	UserSessionForm mode(UserSessionMode mode);

	UserSessionForm connect(ClientStatus connect);
}
