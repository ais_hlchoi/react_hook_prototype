package com.dbs.module.role.data;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.sys.model.SysRoleFuncModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class SysRoleFuncData {

	private SysRoleFuncModel data;

	public SysRoleFuncData(SysRoleFuncModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected SysRoleFuncModel getData() {
		return data;
	}

	protected void setData(SysRoleFuncModel data) {
		this.data = data;
	}

	public Integer getRoleFuncId() {
		return getData().getRoleFuncId();
	}

	public Integer getRoleId() {
		return getData().getRoleId();
	}

	public Integer getFuncId() {
		return getData().getFuncId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getFuncCode() {
		return getData().getFuncCode();
	}

	@JsonInclude(Include.NON_NULL)
	public String getFuncName() {
		return getData().getFuncName();
	}

	@JsonInclude(Include.NON_NULL)
	public String getFuncUrl() {
		return getData().getFuncUrl();
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getDispSeq() {
		return getData().getDispSeq();
	}

	@JsonInclude(Include.NON_NULL)
	public String getType() {
		return getData().getType();
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getParentId() {
		return getData().getParentId();
	}

	public String getCanSelect() {
		return getData().getCanSelect();
	}

	public String getCanInsert() {
		return getData().getCanInsert();
	}

	public String getCanUpdate() {
		return getData().getCanUpdate();
	}

	public String getCanDelete() {
		return getData().getCanDelete();
	}

	public String getCanAudit() {
		return getData().getCanAudit();
	}

	public String getCanView() {
		return getData().getCanView();
	}

	public String getCanUpload() {
		return getData().getCanUpload();
	}

	public String getCanDownload() {
		return getData().getCanDownload();
	}

	@JsonInclude(Include.NON_NULL)
	public String getLastInitId() {
		return getData().getLastInitId();
	}

	@JsonInclude(Include.NON_NULL)
	public Date getLastInitDate() {
		return getData().getLastInitDate();
	}

	@JsonInclude(Include.NON_NULL)
	public String getLastPostId() {
		return getData().getLastPostId();
	}

	@JsonInclude(Include.NON_NULL)
	public Date getLastPostDate() {
		return getData().getLastPostDate();
	}

	// for detail page
	@JsonInclude(Include.NON_NULL)
	public List<SysRoleFuncData> getFuncList() {
		return CollectionUtils.isEmpty(getData().getFuncList()) ? null : getData().getFuncList().stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	// for side menu
	@JsonInclude(Include.NON_NULL)
	public List<SysRoleFuncData> getChildren() {
		return CollectionUtils.isEmpty(getData().getChildren()) ? null : getData().getChildren().stream().map(SysRoleFuncData::new).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return getData().getActiveInd();
	}
}
