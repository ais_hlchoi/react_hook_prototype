package com.dbs.module.log.model;

import java.util.Date;

import com.dbs.module.log.OperationLogForm;
import com.dbs.module.model.AbstractSearchModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationLogFormModel extends AbstractSearchModel implements OperationLogForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String userCode;

	@JsonInclude
	private String userName;

	@JsonInclude
	private String funcName;

	@JsonInclude
	private String operationType;

	@JsonInclude
	private Date startDate;

	@JsonInclude
	private Date endDate;
}
