package com.dbs.module.log;

import java.util.Date;

import com.dbs.module.model.SearchForm;

public interface OperationLogForm extends SearchForm {

	String getUserCode();

	String getUserName();

	String getFuncName();

	String getOperationType();

	Date getStartDate();

	Date getEndDate();
}
