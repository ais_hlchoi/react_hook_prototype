package com.dbs.module.session.util;

import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.session.constant.UserSessionStatus;
import com.dbs.module.session.model.UserSessionStatusModel;

public class UserSessionStatusUtils {

	private UserSessionStatusUtils() {
		// for SonarQube scanning purpose
	}

	public static UserSessionStatus parse(final List<UserSessionStatusModel> list) {
		if (CollectionUtils.isEmpty(list))
			return UserSessionStatus.A;

		if (list.size() == 1) {
			UserSessionStatusModel bean = list.get(0);
			if (isEmptySid(bean) || isValidSession(bean))
				return UserSessionStatus.O;

			if (isExpired(bean) && !equalsSession(bean))
				return UserSessionStatus.A;

			return isExpired(bean) || isKicked(bean) ? UserSessionStatus.E : UserSessionStatus.F;
		}

		if (list.stream().anyMatch(equalsSession())) {
			UserSessionStatusModel bean = list.stream().filter(equalsSession()).findFirst().orElse(null);
			if (null != bean && (isExpired(bean) || isKicked(bean)))
				return UserSessionStatus.E;
		}

		if (list.stream().anyMatch(isEmptySid().or(isValidSession())))
			return UserSessionStatus.K;

		if (list.stream().allMatch(isExpired()))
			return UserSessionStatus.R;

		return list.stream().anyMatch(isExpired()) ? UserSessionStatus.T : UserSessionStatus.F;
	}

	protected static boolean isEmptySid(UserSessionStatusModel o) {
		return StringUtils.isBlank(o.getSid());
	}

	protected static boolean isValidSession(UserSessionStatusModel o) {
		return equalsSession(o) && !isExpired(o);
	}

	protected static boolean equalsSession(UserSessionStatusModel o) {
		// TODO - ignore to check equalsIp since specific IP through gatewayApi
		// coreApi: 192.168.5.1
		// sysApi : 192.168.6.1
		return equalsSid(o); // && equalsIp(o);
	}

	protected static boolean notEqualsSession(UserSessionStatusModel o) {
		return !equalsSession(o);
	}

	protected static boolean equalsSid(UserSessionStatusModel o) {
		return StringUtils.isNotBlank(o.getSid()) && o.getSid().equals(o.getSidSrc());
	}

	protected static boolean equalsIp(UserSessionStatusModel o) {
		return StringUtils.isNotBlank(o.getIp()) && o.getIp().equals(o.getIpSrc());
	}

	protected static boolean isExpired(UserSessionStatusModel o) {
		return BooleanUtils.toBoolean(o.getExpiredInd());
	}

	protected static boolean notExpired(UserSessionStatusModel o) {
		return !isExpired(o);
	}

	protected static boolean isKicked(UserSessionStatusModel o) {
		return BooleanUtils.toBoolean(o.getKickedInd());
	}

	public static Predicate<UserSessionStatusModel> isEmptySid() {
		return UserSessionStatusUtils::isEmptySid;
	}

	public static Predicate<UserSessionStatusModel> isValidSession() {
		return equalsSid().and(equalsIp()).and(notExpired());
	}

	public static Predicate<UserSessionStatusModel> equalsSession() {
		return UserSessionStatusUtils::equalsSession;
	}

	public static Predicate<UserSessionStatusModel> notEqualsSession() {
		return UserSessionStatusUtils::notEqualsSession;
	}

	public static Predicate<UserSessionStatusModel> equalsSid() {
		return UserSessionStatusUtils::equalsSid;
	}

	public static Predicate<UserSessionStatusModel> equalsIp() {
		return UserSessionStatusUtils::equalsIp;
	}

	public static Predicate<UserSessionStatusModel> isExpired() {
		return UserSessionStatusUtils::isExpired;
	}

	public static Predicate<UserSessionStatusModel> notExpired() {
		return UserSessionStatusUtils::notExpired;
	}

	public static Predicate<UserSessionStatusModel> isKicked() {
		return UserSessionStatusUtils::isKicked;
	}
}
