package com.dbs.module.session.model;

import java.sql.Timestamp;

import com.dbs.sys.model.SysUserSessionModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSessionStatusModel extends SysUserSessionModel {

	@JsonInclude
	private Timestamp cutOffTime;

	@JsonInclude
	private Timestamp sysDate;

	@JsonInclude
	private String expiredInd;

	@JsonInclude
	private String kickedInd;

	@JsonInclude
	private String sidSrc;

	@JsonInclude
	private String ipSrc;

	@JsonInclude
	private Timestamp loginTimeSrc;
}
