package com.dbs.module.role.data;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.util.AbstractModuleData;
import com.dbs.sys.model.SysRoleFuncModel;
import com.dbs.sys.model.SysRoleModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class SysRoleData extends AbstractModuleData {

	private SysRoleModel data;

	public SysRoleData(SysRoleModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	@JsonIgnore
	public SysRoleModel getData() {
		return data;
	}

	protected void setData(SysRoleModel data) {
		this.data = data;
	}

	public Integer getRoleId() {
		return getData().getRoleId();
	}

	public String getRoleCode() {
		return getData().getRoleCode();
	}

	public String getRoleName() {
		return getData().getRoleName();
	}

	public String getStatInd() {
		return getData().getStatInd();
	}

	public String getAmendInd() {
		return getData().getAmendInd();
	}

	public String getMakChkStatus() {
		return getData().getMakChkStatus();
	}

	@JsonInclude(Include.NON_NULL)
	public String getRemark() {
		return getData().getRemark();
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getDispSeq() {
		return getData().getDispSeq();
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return getData().getActiveInd();
	}

	@JsonInclude(Include.NON_NULL)
	public String getLastInitId() {
		return getData().getLastInitId();
	}

	@JsonInclude(Include.NON_NULL)
	public Date getLastInitDate() {
		return getData().getLastInitDate();
	}

	@JsonInclude(Include.NON_NULL)
	public String getLastPostId() {
		return getData().getLastPostId();
	}

	@JsonInclude(Include.NON_NULL)
	public Date getLastPostDate() {
		return getData().getLastPostDate();
	}

	@JsonInclude(Include.NON_NULL)
	public List<SysRoleFuncModel> getFuncList() {
		return CollectionUtils.isEmpty(getData().getFuncList()) ? null : getData().getFuncList();
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return formatTimeNano(getData().getLastUpdatedDate());
	}

	// for Audit Model

	public String getCreatedBy() {
		return getData().getCreatedBy();
	}

	public Date getCreatedDate() {
		return getData().getCreatedDate();
	}

	public String getLastUpdatedBy() {
		return getData().getLastUpdatedBy();
	}

	public Date getLastUpdatedDate() {
		return getData().getLastUpdatedDate();
	}
}
