package com.dbs.module.func.data;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.util.AbstractModuleData;
import com.dbs.sys.model.SysFuncModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class SysFuncData extends AbstractModuleData {

	private SysFuncModel data;

	public SysFuncData(SysFuncModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected SysFuncModel getData() {
		return data;
	}

	protected void setData(SysFuncModel data) {
		this.data = data;
	}

	public Integer getFuncId() {
		return getData().getFuncId();
	}

	public String getFuncCode() {
		return getData().getFuncCode();
	}

	public String getFuncName() {
		return getData().getFuncName();
	}

	public String getFuncUrl() {
		return getData().getFuncUrl();
	}

	public Integer getDispSeq() {
		return getData().getDispSeq();
	}

	public String getType() {
		return getData().getType();
	}

	public Integer getParentId() {
		return getData().getParentId();
	}

	public Integer getParentIdLevel2() {
		return getData().getParentIdLevel2();
	}

	@JsonInclude(Include.NON_NULL)
	public List<SysFuncData> getChildren() {
		return CollectionUtils.isEmpty(getData().getChildren()) ? null : getData().getChildren().stream().map(SysFuncData::new).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return formatTimeNano(getData().getLastUpdatedDate());
	}

	// for Audit Model

	public String getCreatedBy() {
		return getData().getCreatedBy();
	}

	public Date getCreatedDate() {
		return getData().getCreatedDate();
	}

	public String getLastUpdatedBy() {
		return getData().getLastUpdatedBy();
	}

	public Date getLastUpdatedDate() {
		return getData().getLastUpdatedDate();
	}
}
