package com.dbs.module.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractModuleData {

	protected <T> T getOrElse(T value, T valueIfNull) {
		return AbstractModuleUtils.getOrElse(value, valueIfNull);
	}

	protected String format(Date date) {
		return null == date ? "" : new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	protected String formatDatetime(Date date) {
		return null == date ? "" : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	protected Long[] formatTimeNano(Timestamp time) {
		return null == time ? null : new Long[] { time.getTime(), Long.valueOf(time.getNanos()) };
	}
}
