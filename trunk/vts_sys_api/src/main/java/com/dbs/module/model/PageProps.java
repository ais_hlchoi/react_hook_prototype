package com.dbs.module.model;

import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor(staticName = "of")
public class PageProps<T> {

	@NonNull
	private Integer startRow;

	@NonNull
	private Integer endRow;

	@NonNull
	private Long total;

	@NonNull
	private Integer pages;

	private List<T> list;

	public PageProps<T> put(List<T> list) {
		this.list = list;
		return this;
	}
}
