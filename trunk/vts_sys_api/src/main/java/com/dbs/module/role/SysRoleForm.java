package com.dbs.module.role;

import java.util.List;

public interface SysRoleForm {

	List<String> getRoleList();
}
