package com.dbs.framework.security;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dbs.util.FortifyStrategyUtils;

public class CipherUtil {

	private CipherUtil() {
		// for SonarQube scanning purpose
	}

	private static Logger logger = LoggerFactory.getLogger(CipherUtil.class);

	private static final Charset CHARSET_ENCODING = StandardCharsets.UTF_8;

	public static String encrypt(String text) {
		SecretKey secretKey = KeyStoreUtil.getSecretKey();
		if (secretKey != null) {
			try {
				Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm());
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				byte[] ciperText = cipher.doFinal(text.getBytes());
				byte[] encodedCiperText = Base64.getEncoder().encode(ciperText);
				return new String(encodedCiperText, CHARSET_ENCODING);
			} catch (Exception e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
			}
		}
		return null;
	}

	public static String decrypt(String text) {
		SecretKey secretKey = KeyStoreUtil.getSecretKey();
		if (secretKey != null) {
			try {
				Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm());
				cipher.init(Cipher.DECRYPT_MODE, secretKey);
				byte[] decodedCiperText = Base64.getDecoder().decode(text.getBytes(CHARSET_ENCODING));
				byte[] ciperText = cipher.doFinal(decodedCiperText);
				return new String(ciperText, CHARSET_ENCODING);
			} catch (Exception e) {
				logger.error(FortifyStrategyUtils.toErrString(e));
			}
		}
		return null;
	}
}
