package com.dbs.framework.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface UserSession {

	UserSessionAction value() default UserSessionAction.CONNECT;

	public enum UserSessionAction {
		  IGNORE
		, CONNECT
		, RECONNECT
		, DISCONNECT
		, RESET
	}
}
