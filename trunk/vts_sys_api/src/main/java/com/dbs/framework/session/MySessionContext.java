package com.dbs.framework.session;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

public class MySessionContext {

	private MySessionContext() {
		// for SonarQube scanning purpose
	}

	private static HashMap<String, HttpSession> mymap = new HashMap<>();

	public static synchronized void addSession(HttpSession session) {
		if (session != null) {
			mymap.put(session.getId(), session);
		}
	}

	public static synchronized void delSession(HttpSession session) {
		if (session != null) {
			mymap.remove(session.getId());
		}
	}

	public static synchronized HttpSession getSession(String sessionId) {
		if (sessionId == null)
			return null;

		return (HttpSession) mymap.get(sessionId);
	}
}
