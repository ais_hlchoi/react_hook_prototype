package com.dbs.framework.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.ResponseCodeUtils;

@Component
public class ApplicationConfig {

	private static Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

	public static final String APPLICATION_CONFIG_FILE = "application.properties";

	private static final String KEYSTORE_PATH;
	private static final String KEYSTORE_NAME;
	private static final String KEYSTORE_SECRET;
	private static final String KEYSTORE_TYPE;
	private static final String KEYSTORE_ALIAS;
	private static final String KEYSTORE_ALIAS_SECRET;
	private static final String KEY_GENERATOR_ALGORITHM;

	private ApplicationConfig() {
		// for SonarQube scanning purpose
	}

	static {
		Properties applicationConfigProperties = new Properties();

		// Initialize Application Configure Properties
		InputStream applicationConfigInputStream = null;

		applicationConfigInputStream = ApplicationConfig.class.getClassLoader().getResourceAsStream(APPLICATION_CONFIG_FILE);
		if (applicationConfigInputStream != null) {
			try {
				applicationConfigProperties.load(applicationConfigInputStream);
			} catch (IOException e) {
				throw new SysRuntimeException(ResponseCodeUtils.getFailureCode(), "Failed to load ApplicationConfig Properties.");
			} finally {
				try {
					applicationConfigInputStream.close();
				} catch (IOException e) {
					logger.error(FortifyStrategyUtils.toErrString(e));
				}
			}
		} else {
			logger.error("cannot find property file for ApplicationConfig: " + APPLICATION_CONFIG_FILE);
		}

		KEYSTORE_PATH = applicationConfigProperties.getProperty("keystorePath", "").trim();
		KEYSTORE_NAME = applicationConfigProperties.getProperty("keystoreName", "").trim();
		KEYSTORE_SECRET = applicationConfigProperties.getProperty("keystoreSecret", "").trim();
		KEYSTORE_TYPE = applicationConfigProperties.getProperty("keystoreType", "").trim();
		KEYSTORE_ALIAS = applicationConfigProperties.getProperty("keystoreAlias", "").trim();
		KEYSTORE_ALIAS_SECRET = applicationConfigProperties.getProperty("keystoreAliasSecret", "").trim();
		KEY_GENERATOR_ALGORITHM = applicationConfigProperties.getProperty("keygeneratorAlgorithm", "").trim();
	}

	public static String getKeystorePath() {
		return KEYSTORE_PATH;
	}

	public static String getKeystoreName() {
		return KEYSTORE_NAME;
	}

	public static String getKeystoreSecret() {
		return KEYSTORE_SECRET;
	}

	public static String getKeystoreType() {
		return KEYSTORE_TYPE;
	}

	public static String getKeystoreAlias() {
		return KEYSTORE_ALIAS;
	}

	public static String getKeystoreAliasSecret() {
		return KEYSTORE_ALIAS_SECRET;
	}

	public static String getKeygeneratorAlgorithm() {
		return KEY_GENERATOR_ALGORITHM;
	}
}
