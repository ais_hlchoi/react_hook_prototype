package com.dbs.framework.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum SystemResponseCode implements ResponseCode {

	/**
	 * Success
	 */
	M1000,
	/**
	 * Force to login page
	 */
	M9000,
	/**
	 * Hold on current page
	 */
	M9001,
	/**
	 * 1000001 - Token error
	 */
	E1001("1000001", "Token error", -1),
	/**
	 * 1000002 - Access control not found
	 */
	E1002("1000002", "Access control not found", -1),
	/**
	 * 1000003
	 */
	E1003("1000003", null, -1),
	/**
	 * Error
	 */
	E1999(null, null, -1),
	;

	private String code;
	private String desc;
	private Integer status;

	private SystemResponseCode(String code, String desc, Integer status) {
		this.code = code;
		this.desc = desc;
		this.status = status;
	}

	private SystemResponseCode(String desc) {
		this.code = null;
		this.desc = desc;
		this.status = null;
	}

	private SystemResponseCode() {
		this.code = null;
		this.desc = null;
		this.status = null;
	}

	public String getCode() {
		return null == code ? name() : code;
	}

	public String getDesc() {
		return desc;
	}

	public Integer getStatus() {
		return null == status ? name().startsWith("E") ? -1 : 1 : status;
	}

	public static ResponseCode getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ResponseCode getOrElse(String text, SystemResponseCode o) {
		if (null == o)
			o = E1999;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return SystemResponseCode.valueOf(text);
		} catch (IllegalArgumentException e) {
			return Stream.of(SystemResponseCode.values()).filter(c -> c.getCode().equals(text)).findFirst().orElse(o);
		}
	}
}
