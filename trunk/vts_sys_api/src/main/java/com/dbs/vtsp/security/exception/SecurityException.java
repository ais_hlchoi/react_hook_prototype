package com.dbs.vtsp.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @description:Custom Execption
 */
public class SecurityException extends AuthenticationException {

	private static final long serialVersionUID = 1411474328346767893L;

	public SecurityException(String msg) {
		super(msg);
	}

	public SecurityException(String msg, Throwable t) {
		super(msg, t);
	}
}
