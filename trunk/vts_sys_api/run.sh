java $JAVA_OPTS -jar /vts-core-api.jar --spring.profiles.active=dev &
java $JAVA_OPTS -jar /vts-main-api.jar --spring.profiles.active=dev &
java $JAVA_OPTS -jar /vts-sys-api.jar --spring.profiles.active=dev &
java $JAVA_OPTS -jar /vts-auth-api.jar --spring.profiles.active=dev

