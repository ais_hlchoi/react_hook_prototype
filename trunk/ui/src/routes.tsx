import React, { FunctionComponent, useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import ContainerComponent from "pages/layout/ContainerComponent";
import SignInComponent from "pages/signIn/SignInComponent";
import SelectRoleComponent from "pages/selectRole/SelectRoleComponent";
import RedirectComponent from "pages/selectRole/RedirectComponent";
import PrivateRoute from "shared/PrivateRoute";
import { ServicesContext } from "services/contexts/ServicesContext";

// top level router
const Routes = () => {
  // package.json is located at the same level as /src [ this.page.location: /src/. ]
  const pjson = require("../package.json");
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;

  const WrappedContainerComponent : FunctionComponent = () => <ContainerComponent  pjson={pjson} />;
  const WrappedSignInComponent    : FunctionComponent = () => <SignInComponent     pjson={pjson} />;
  const WrappedSelectRoleComponent: FunctionComponent = () => <SelectRoleComponent pjson={pjson} />;
  const WrappedRedirectComponent  : FunctionComponent = () => <RedirectComponent   pjson={pjson} />;

  return (
    <>
    <Switch>
      {/*
      <Route path="/" exact component={SignIn} />
      <Route path="/register" component={SignUp} />
      <Route path="/dashboard" component={Dashboard} isPrivate />
      redirect user to SignIn page if route does not exist and user is not authenticated */}
      <PrivateRoute path="/app" component={WrappedContainerComponent} isAuthenticated={() => AuthenticationService.isAuthenticated()} />
      <Route path="/signin"     component={WrappedSignInComponent} />
      <Route path="/selectRole" component={WrappedSelectRoleComponent} />
      <Route path="/redirect"   component={WrappedRedirectComponent} />
      <Redirect exact from="/" to="signin" />
    </Switch>
    </>
  );
};

export default Routes;
