export default class CommonUtils {

  static asc    = (a: number, b: number) => a - b;
  static desc   = (a: number, b: number) => b - a;
  static random = (n: number = 10) => { let s = ''; do { s = s.concat(Math.random().toString(10).slice(2)) } while (s.length < n); return s.substring(0, n) };
  static range  = (a: number, b: number, c: number = 1) => Array.from({ length: (b - a) / c + 1 }, (_, i) => a + (i * c));

  static isNullOrUndefined(value: any) {
    return value === undefined || value == null;
  }

  static toBoolean = {
    TRUE: (value: any) => {
      try {
        if (CommonUtils.isNullOrUndefined(value)) {
          return false;
        } else if (typeof value === "number") {
          return value > 0;
        } else if (typeof value === "string") {
          return ["Y", "Yes", "T", "True"].some(o => o.toUpperCase() === value.toUpperCase());
        } else {
          return value;
        }
      } catch (error) {
        return false;
      }
    },
    FALSE: (value: any) => {
      try {
        if (CommonUtils.isNullOrUndefined(value)) {
          return false;
        } else if (typeof value === "number") {
          return value === 0 || value < 0;
        } else if (typeof value === "string") {
          return ["N", "No", "F", "False"].some(o => o.toUpperCase() === value.toUpperCase());
        } else {
          return !value;
        }
      } catch (error) {
        return false;
      }
    }
  };

  static isTrue(...args: any) {
    return args.some((value: any) => this.toBoolean.TRUE(value));
  }

  static isTrueAll(...args: any) {
    return args.every((value: any) => this.toBoolean.TRUE(value));
  }

  static isFalse(...args: any) {
    return args.some((value: any) => this.toBoolean.FALSE(value));
  }

  static isFalseAll(...args: any) {
    return args.every((value: any) => this.toBoolean.FALSE(value));
  }

  static emptySpace = "　";

  static isEmpty(value: any) {
    if (this.isNullOrUndefined(value)) {
      return true;
    } else if (typeof value === "function") {
      return false;
    } else if (typeof value === "number") {
      return false;
    } else if (typeof value === "string") {
      return value === "";
    } else if (Array.isArray(value)) {
      return value.length < 1;
    } else {
      return Object.keys(value).length === 0 || !Object.keys(value).map(key => value.hasOwnProperty(key)).some(item => item);
    }
  }

  static isDirty(value: any) {
    return !this.isEmpty(value);
  }

  static isDiff(arr = []) {
    return arr.length === 0 ? false : !(arr.length > 1 ? arr[0] === arr[1] : false);
  }

  static hasDiff(arr = []) {
    return arr.length === 0 ? false : !(arr.length > 1 ? arr[0] === arr[arr.length - 1] : false);
  }

  static getValueIf(value: any, ...arg: any) {
    return this.isNullOrUndefined(arg) || arg.length === 0 ? [undefined, undefined] : [arg[0], arg.length === 1 ? value : arg[1]];
  }

  static ifNull(value: any, ...arg: any) {
    const [valueIfTrue, valueIfFalse] = this.getValueIf(value, ...arg);
    return this.isNullOrUndefined(value) ? valueIfTrue : valueIfFalse;
  }

  static ifTrue(value: any, ...arg: any) {
    const [arg0, arg1] = arg;
    const [valueIfTrue, valueIfFalse] = this.getValueIf(value, arg0, this.isEmpty(arg1) && typeof arg0 === "string" ? "" : arg1);
    return this.isTrue(value) ? valueIfTrue : valueIfFalse;
  }

  static ifFalse(value: any, ...arg: any) {
    const [arg0, arg1] = arg;
    const [valueIfTrue, valueIfFalse] = this.getValueIf(value, arg0, this.isEmpty(arg1) && typeof arg0 === "string" ? "" : arg1);
    return this.isFalse(value) ? valueIfTrue : valueIfFalse;
  }

  static ifEmpty(value: any, ...arg: any) {
    const [valueIfTrue, valueIfFalse] = this.getValueIf(value, ...arg);
    return this.isEmpty(value) ? valueIfTrue : valueIfFalse;
  }

  static ifDirty(value: any, ...arg: any) {
    const [valueIfTrue, valueIfFalse] = this.getValueIf(value, ...arg);
    return this.isDirty(value) ? valueIfTrue : valueIfFalse;
  }

  static ifNotOwnProperty(map: any, key: string|number, valueIfTrue: any, valueIfFalse: any) {
    return map.hasOwnProperty(key) ? valueIfFalse || map[key] : valueIfTrue;
  }

  static toUpperCase(value: string, uppercase = true) {
    return this.isDirty(value) && uppercase ? value.toUpperCase() : value;
  }
}