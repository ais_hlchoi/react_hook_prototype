import { TabEnum } from "../../enum";
import { useState } from "react";

export default class PopupCommonFunctions {

  static popupFunctions(onConfirm: (key?: any) => void) {
    const [showPopup, setShowPopup] = useState<boolean>(false);
    const [changed] = useState<any>([false]);
    const [backKey] = useState<any>([TabEnum.DEFAULT_TAB, '']);

    const onCloseLeaveWithoutSave = () => setShowPopup(false);
    const onConfirmLeaveWithoutSave = () => {
      confirmBackKey();
      setChanged(false);
      setShowPopup(false);
      onConfirm()
    };

    const isChanged = () => changed[0];
    const setChanged = (value: boolean) => changed[0] = value;
    const confirmBackKey = () => backKey[0] = backKey[1] || TabEnum.DEFAULT_TAB;
    const onChangeCallback = (key: any) => {
      if (!changed[0]) {
        backKey[0] = key;
        onConfirm();
      } else {
        if (backKey[0] === key) {
          if (key !== TabEnum.DEFAULT_TAB) {
            return;
          }
        }
        backKey[1] = key;
        setShowPopup(true);
      }
    };
    return {
      showPopup,
      setShowPopup,
      changed,
      backKey,
      onCloseLeaveWithoutSave,
      setChanged,
      onConfirmLeaveWithoutSave,
      isChanged,
      onChangeCallback
    };
  }
}
