import CommonUtils from "./CommonUtils";

export default class UseEffectUtils {

  private static uid: string = 'useEffectData';
  static getProps = () => Object.assign({}, JSON.parse(localStorage.getItem(UseEffectUtils.uid) || '{}'));
  static setProps = (props: any, arr?: string[]) => { if (arr) UseEffectUtils.removePropsAttr(arr); localStorage.setItem(UseEffectUtils.uid, JSON.stringify({ ...UseEffectUtils.getProps(), ...props })) };
  static resetProps = (type?: string) => { CommonUtils.isEmpty(type) ? localStorage.removeItem(UseEffectUtils.uid) : UseEffectUtils.removePropsAttr(Object.keys(UseEffectUtils.getProps()).filter(key => key === type)) };
  static removePropsAttr = (arr: string[]) => { const props = UseEffectUtils.getProps(); arr.forEach((attr: string) => props.hasOwnProperty(attr) ? delete props[attr] : void 0); localStorage.setItem(UseEffectUtils.uid, JSON.stringify(props)) };

  static hasItem = (type: string) => UseEffectUtils.getProps().hasOwnProperty(type);
  static getItem = (type: string) => UseEffectUtils.hasItem(type) ? UseEffectUtils.getProps()[type] : {};

  private static skip: string = 'skip';
  static isSkip = (type: string, action?: string) => [...UseEffectUtils.getSkip(type)].includes(action || 'all');
  static getSkip = (type: string) => ({ [UseEffectUtils.skip]: [], ...UseEffectUtils.getItem(type) })[UseEffectUtils.skip];
  static setSkip = (type: string, action?: string) => UseEffectUtils.setProps({ [type]: { ...UseEffectUtils.getItem(type), [UseEffectUtils.skip]: UseEffectUtils.getSkip(type).concat(action || 'all') } });
  static resetSkip = (type: string, action?: string) => CommonUtils.isEmpty(action) ? UseEffectUtils.setProps({ [type]: Object.fromEntries(Object.entries(UseEffectUtils.getItem(type)).filter(([k]) => k !== UseEffectUtils.skip)) }) : UseEffectUtils.setProps({ [type]: { ...UseEffectUtils.getItem(type), [UseEffectUtils.skip]: UseEffectUtils.getSkip(type).filter((k: string) => k !== action) } });
}
