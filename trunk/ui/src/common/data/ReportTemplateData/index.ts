import reportTemplate1Data from "./reportTemplate1Data";
import reportTemplate2Data from "./reportTemplate2Data";
import reportTemplate3Data from "./reportTemplate3Data";

export { default as reportTemplate1Data } from "./reportTemplate1Data";
export { default as reportTemplate2Data } from "./reportTemplate2Data";
export { default as reportTemplate3Data } from "./reportTemplate3Data";

const reportTemplateAllData = [
  reportTemplate1Data,
  reportTemplate2Data,
  reportTemplate3Data,
].map((o: any, id: number) => ({ ...o, id }));

const reportTemplateListData = reportTemplateAllData.map(({id, name}) => ({ id, name }));

export { reportTemplateAllData, reportTemplateListData };
