const exportOptionData = [
  { "id": 0, "label": "xlsx", "value": "XLSX" },
  { "id": 1, "label": "csv" , "value": "CSV"  },
  { "id": 2, "label": "pdf" , "value": "PDF"  },
];

export default exportOptionData;
