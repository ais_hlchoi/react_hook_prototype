const tableData = [
  {
    "id": 1, "label": "VTSP",
    "tables": [
      { "id": 26, "file": 1, "label": "ALLOCATIONS" },
      { "id": 27, "file": 1, "label": "ARC_EVENTS_Exchange_Order" },
      { "id": 28, "file": 1, "label": "ARC_EVENTS_Order_and_Trade_Flow" },
      { "id": 29, "file": 1, "label": "ARC_EVENTS_SOR_Audit" },
      { "id": 31, "file": 1, "label": "ARC_EVENTS_Trade_Report" },
      { "id": 32, "file": 1, "label": "ARC_EVENTS_User_Logon" },
      { "id": 33, "file": 1, "label": "ARC_SETTINGS_Service_Strategy" },
      { "id": 34, "file": 1, "label": "BOOKS" },
      { "id": 35, "file": 1, "label": "CHARGE_RULES" },
      { "id": 36, "file": 1, "label": "CLIENTS" },
      { "id": 37, "file": 1, "label": "CLOSING_PRICE" },
      { "id": 38, "file": 1, "label": "EXCHANGE_CONNECTIONS" },
      { "id": 39, "file": 1, "label": "FRONT_OFFICE_TRADES" },
      { "id": 40, "file": 1, "label": "GROUPS" },
      { "id": 41, "file": 1, "label": "INSTRUMENTS" },
      { "id": 42, "file": 1, "label": "LOGON_AUDIT_TRAIL" },
      { "id": 43, "file": 1, "label": "MARKET_SIDE_COUNTERPARTIES" },
      { "id": 44, "file": 1, "label": "MIS_NEW_INSTRUMENTS" },
      { "id": 45, "file": 1, "label": "ORDER_PROGRESS" },
      { "id": 46, "file": 1, "label": "STATIC_DATA_AUDIT" },
      { "id": 47, "file": 1, "label": "SUB_ACCOUNTS" },      
      { "id": 48, "file": 1, "label": "USER_EXCHANGE_CONNECTIONS" },
      { "id": 49, "file": 1, "label": "USER_GROUP_RIGHTS" },
      { "id": 50, "file": 1, "label": "USER_MARKET_ROUTES" },
      { "id": 51, "file": 1, "label": "USERS" },
      { "id": 52, "file": 1, "label": "ORDER_TRANSACTION" },
    ],
  },
  {
    "id": 2, "label": "BOSV",
    "tables": [
      { "id": 26, "file": 2, "label": "ALLOCATIONS" },
      { "id": 27, "file": 2, "label": "ARC_EVENTS_Exchange_Order" },
      { "id": 28, "file": 2, "label": "ARC_EVENTS_Order_and_Trade_Flow" },
    ],
  },
];

export default tableData;
