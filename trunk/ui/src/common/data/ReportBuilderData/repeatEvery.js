const repeatEvery = [
  { "id": 0, "label": "5 minutes", "value": "5" },
  { "id": 1, "label": "10 minutes"  , "value": "10" },
  { "id": 2, "label": "30 minutes"  , "value": "30" },
  { "id": 3, "label": "1 Hour"  , "value": "1h" },
  { "id": 4, "label": "2 Hours"  , "value": "2h" },
];

export default repeatEvery;