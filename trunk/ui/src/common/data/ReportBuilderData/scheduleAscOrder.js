const scheduleAscOrder = [
  { "id": 0, "label": "First", "value": "1-7" },
  { "id": 1, "label": "Second"  , "value": "8-14" },
  { "id": 2, "label": "Third"  , "value": "15-21" },
  { "id": 3, "label": "Forth"  , "value": "21-28" },
  { "id": 4, "label": "Last"  , "value": "L" },
];

export default scheduleAscOrder;