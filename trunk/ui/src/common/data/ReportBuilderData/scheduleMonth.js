const scheduleMonth = [
  { "id": 0, "label": "January", "value": "1" },
  { "id": 1, "label": "February"  , "value": "2" },
  { "id": 2, "label": "March"  , "value": "3" },
  { "id": 3, "label": "April"  , "value": "4" },
  { "id": 4, "label": "May"  , "value": "5" },
  { "id": 5, "label": "June"  , "value": "6" },
  { "id": 6, "label": "July"  , "value": "7" },
  { "id": 6, "label": "August"  , "value": "8" },
  { "id": 6, "label": "September"  , "value": "9" },
  { "id": 6, "label": "October"  , "value": "10" },
  { "id": 6, "label": "November"  , "value": "11" },
  { "id": 6, "label": "December"  , "value": "12" },
];

export default scheduleMonth;