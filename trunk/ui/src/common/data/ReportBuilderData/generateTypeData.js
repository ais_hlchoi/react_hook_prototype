const generateTypeData = [
  { "id": 0, "label": "Both"         , "value": "B" },
  { "id": 1, "label": "Online Only"  , "value": "O" },
  { "id": 2, "label": "Schedule Only", "value": "S" },
];

export default generateTypeData;
