const comparatorData = [
  { "id": 1, "label": "=" , "value": "EQUALS" },
  { "id": 2, "label": "!=", "value": "NOT_EQUALS" },
  { "id": 3, "label": ">=", "value": "GREATER_EQUALS" },
  { "id": 4, "label": ">" , "value": "GREATER_THAN" },
  { "id": 5, "label": "<=", "value": "LESS_EQUALS" },
  { "id": 6, "label": "<" , "value": "LESS_THAN" },
];

export default comparatorData;
