const layoutOptionData = [
  { "id": 0, "label": "Horizontal", "value": "H" },
  { "id": 1, "label": "Vertical"  , "value": "V" },
];

export default layoutOptionData;
