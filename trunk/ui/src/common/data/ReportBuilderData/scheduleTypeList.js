const scheduleTypeList = [
  { scheduleType: '1', shortDesc: 'One Time' },
        { scheduleType: '2', shortDesc: 'Daily' },
        { scheduleType: '3', shortDesc: 'Weekly' },
        { scheduleType: '4', shortDesc: 'Monthly' },
];

export default scheduleTypeList;
