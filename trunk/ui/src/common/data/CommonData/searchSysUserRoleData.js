// http://localhost:9988/sys/api/sys/user/searchSysUserRole
const searchSysUserRoleData = {
  "responseCode": "M1000",
  "responseDesc": null,
  "status": 1,
  "object": [
    {
      "key": 0,
      "createdBy": "SYSTEM",
      "createdDate": "2020-09-02T07:25:57.000+00:00",
      "lastUpdatedBy": "SYSTEM",
      "lastUpdatedDate": "2020-09-02T07:25:57.000+00:00",
      "page": null,
      "pageSize": null,
      "startRow": null,
      "totalPage": null,
      "userId": 1,
      "menuFuncId": null,
      "operationContent": null,
      "defaultSearchText": null,
      "userRoleId": 1,
      "roleId": 1,
      "roleCode": "ROLE_ADMIN",
      "companyCode": "ANY",
      "companyName": "ANY",
      "roleName": "ADMIN",
      "defaultInd": "Y",
      "activeInd": "Y",
      "sysUserRoleList": null,
      "list": null
    }
  ],
  "totalRows": null,
  "failed": false
};

export default searchSysUserRoleData;
