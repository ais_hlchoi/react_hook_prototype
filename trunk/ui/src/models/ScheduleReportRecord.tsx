export default class SavedSearchRecord {
    id!: number;
    savedSearch!: string;
    accessGroup!: string;
    sql!: string;
    exportOpt!: string;
}
