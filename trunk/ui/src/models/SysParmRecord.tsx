export default class SysParmRecord {
  activeInd!: string;
  id!: string;
  segment!: string;
  code!: string;
  shortDesc!: string;
  shortDescTc!: string;
  longDesc!: string;
  longDescTc!: string;
  parmValue!: string;
  dispSeq!: number;
}
