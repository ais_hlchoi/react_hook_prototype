export default class SysRoleRecord {
    roleId!: number;
    roleCode!: string;
    roleName!: string;
    remark!: string;
    dispSeq!: number;
    activeInd!: string;
    makChkStatus!: string;
    lastInitId!: string;
    lastInitDate!: string;
    lastPostId!: string;
    lastPostDate!: string;
    canSave!: boolean;
    canAuthorize!: boolean;
}
