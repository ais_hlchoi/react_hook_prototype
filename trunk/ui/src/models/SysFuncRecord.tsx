export default class SysFuncRecord {
    funcId!: number;
    parentId!: number;
    parentIdLevel2!: number;
    funcCode!: string;
    funcName!: string;
    type!: string;
    dispSeq!: number;
    funcUrl!: string;
}
