export { default as SavedSearchRecord      } from "./SavedSearchRecord";
export { default as SysFuncRecord          } from "./SysFuncRecord";
export { default as SysParmRecord          } from "./SysParmRecord";
export { default as SysRoleRecord          } from "./SysRoleRecord";
export { default as SysUserRecord          } from "./SysUserRecord";
export { default as SysUserRoleRecord      } from "./SysUserRoleRecord";
