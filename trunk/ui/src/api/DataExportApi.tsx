import ApiConfig from "./ApiConfig";
import ReportTemplateModel from "pages/app/ReportTemplateComponent/ReportTemplateModel";
import { ExportOption } from "enum/ExportOption";
import { ReportExportType } from "enum";

export default class DataExportApi {

  public static async generateDynamicRpt(model: ReportTemplateModel) {
    return fetch(getApiUrl('getQuerySql'), getHeader(model))
      .then(res => res.json())
      .then(res => res.object)
      .then(sql => DataExportApi.downloadDynamicRpt(sql, model));
  }

  public static async generateDynamicRptAsync(model: ReportTemplateModel) {
    const { option: { type = ReportExportType.RPT_TMPL } } = model;
    if (type === ReportExportType.RPT_CATG) {
      return DataExportApi.generateDynamicRptSetAsync(model);
    }
    return fetch(getApiUrl('getQuerySql'), getHeader(model))
      .then(res => res.json())
      .then(res => res.object)
      .then(sql => DataExportApi.createReportDownload(sql, model));
  }

  public static async generateDynamicRptSetAsync(model: ReportTemplateModel) {
    return fetch(getApiUrl('createReportDownload'), getHeader(model)).then(res => res.json());
  }

  private static async downloadDynamicRpt(sql: string, model: ReportTemplateModel) {
    if (sql == null || sql === '')
      throw new Error("Fail to generate sql statement.");
    const { id, name, option: { output = ExportOption.DEFAULT_OPTION } } = model;
    const dataExportModel = { id, name, advance: { sql }, option: { output } };
    return fetch(getApiUrl('generateDynamicRpt' + initCap(output)), getHeader(dataExportModel)).then((res) => res.blob());
  }

  private static async createReportDownload(sql: string, model: ReportTemplateModel) {
    if (sql == null || sql === '')
      throw new Error("Fail to generate sql statement.");
    const { id, name, option: { output, layout, type } } = model;
    const dataExportModel = { id, name, advance: { sql }, option: { output, layout, type } };
    return fetch(getApiUrl('createReportDownload'), getHeader(dataExportModel)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/dataExport/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};

const initCap = (text: string) => text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
