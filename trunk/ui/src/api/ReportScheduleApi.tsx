import ApiConfig from "./ApiConfig";

export default class ReportScheduleApi {

  public static async getRecord(model: any): Promise<any> {
    return fetch(getApiUrl('find'), getHeader(model)).then(res => res.json()).then(({object}) => object);
  }

  public static async insertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecords(model: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsDispSeq(model: any): Promise<any> {
    return fetch(getApiUrl('updateDispSeq'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsLastExecution(model: any): Promise<any> {
    return fetch(getApiUrl('updateLastExecution'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportSchedule/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
