import ApiConfig from "./ApiConfig";

export default class LoginApi {

  public static async login(props: any): Promise<any> {
    return fetch(getApiUrl('login'), props).then((res) => res.json());
  }

  public static async signOut(): Promise<any> {
    return fetch(getSysApiUrl('logout'), getHeader({ userId: localStorage.getItem('userId') })).then((res) => res.json());
  }

  public static async breakIn(): Promise<any> {
    return fetch(getSysApiUrl('reconnect'), getHeader({ userId: localStorage.getItem('userId') })).then((res) => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}/${action}`;
};

const getSysApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
