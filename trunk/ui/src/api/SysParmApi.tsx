import ApiConfig from "./ApiConfig";

export default class SysParmApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async searchSysParmSegment(model: any): Promise<any> {
    return fetch(getApiUrl('searchSysParmSegment'), getHeader(model)).then(res => res.json());
  }

  public static async searchSysParmDownListBySegment(model: any): Promise<any> {
    return fetch(getApiUrl('searchSysParmDownListBySegment'), getHeader(model)).then(res => res.json());
  }

  public static async insertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecords(model: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }

  public static async searchSysParmValueBySegment(model: any): Promise<any> {
    return fetch(getApiUrl('searchSysParmValueBySegment'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/sysParm/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
