export default class ApiConfig {
    static apiDomainUrl = `${process.env.REACT_APP_API_DOMAIN_URL}`;
    static mainServices = `${process.env.REACT_APP_MAIN_SERVICES}`;
    static vtspServices = `${process.env.REACT_APP_VTSP_SERVICES}`;
    static sysServices  = `${process.env.REACT_APP_SYS_SERVICES}`;
}