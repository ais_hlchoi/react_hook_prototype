import ApiConfig from "./ApiConfig";

export default class OperationLogApi {

  public static async getLoginActivity(model: any): Promise<any> {
    return fetch(getApiUrl('loginLog/search'), getHeader(model)).then(res => res.json());
  }

  public static async getUserActivity(model: any): Promise<any> {
    return fetch(getApiUrl('operationLog/search'), getHeader(model)).then(res => res.json());
  }

  public static async getUploadActivity(model: any): Promise<any> {
    return fetch(getApiUrl('operationLog/searchUpload'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
