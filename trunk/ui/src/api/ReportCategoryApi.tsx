import ApiConfig from "./ApiConfig";

export default class ReportCategoryApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async getRecord(model: any): Promise<any> {
    return fetch(getApiUrl('getReportCategory'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupRoleList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupRoleList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportScheduleList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportScheduleList'), getHeader(model)).then(res => res.json());
  }

  public static async insertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecords(model: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsReportGroup(model: any): Promise<any> {
    return fetch(getApiUrl('updateReport'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }

  public static async getReportNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportCategoryProps(model: any): Promise<any> {
    return fetch(getApiUrl('getReportCategoryProps'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportCategory/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
