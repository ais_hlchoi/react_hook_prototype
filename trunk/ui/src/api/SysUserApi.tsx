import ApiConfig from "./ApiConfig";
import { searchSysUserRoleData } from "common/data/CommonData";

export default class SysUserApi {

  public static async searchRecords(props: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(props)).then(res => res.json());
  }

  public static async searchRecordsAll(props: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(props)).then(res => res.json());
  }

  public static async searchSysUserRole(props: any): Promise<any> {
    return fetch(getApiUrl('searchSysUserRole'), getHeader(props)).then(res => res.json());
  }

  public static async insertRecords(props: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(props)).then(res => res.json());
  }

  public static async updateRecords(props: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(props)).then(res => res.json());
  }

  public static async deleteRecords(props: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(props)).then(res => res.json());
  }
  
  public static async saveUserRoleRecords(props: any): Promise<any> {
    return fetch(getApiUrl('saveUserRole'), getHeader(props)).then(res => res.json());
  }

  public static async deleteUserRoleRecords(props: any): Promise<any> {
    return fetch(getApiUrl('deleteUserRole'), getHeader(props)).then(res => res.json());
  }

  public static async updatePwdRecords(props: any): Promise<any> {
    return fetch(getApiUrl('updatePwd'), getHeader(props)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/user/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({ ...model }),
  };
};
