import ApiConfig from "./ApiConfig";

export default class MenuApi {

  public static async getSideMenu(props: any): Promise<any> {
    return fetch(getApiUrl('getSideMenu'), getHeader(props)).then(res => res.json());//.then((res) => res.object);
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/menu/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({ ...model }),
  };
};
