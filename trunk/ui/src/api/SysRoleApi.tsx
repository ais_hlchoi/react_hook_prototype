import ApiConfig from "./ApiConfig";

export default class SysRoleApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async searchAllFunc(model: any): Promise<any> {
    return fetch(getApiUrl('searchAllFunc'), getHeader(model)).then(res => res.json());
  }

  public static async searchRoleFuncList(model: any): Promise<any> {
    return fetch(getApiUrl('searchRoleFunc'), getHeader(model)).then(res => res.json());
  }

  public static async insertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecords(model: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(model)).then(res => res.json());
  }

  public static async reverseRecords(model: any): Promise<any> {
    return fetch(getApiUrl('reverse'), getHeader(model)).then(res => res.json());
  }

  public static async authorizeRecords(model: any): Promise<any> {
    return fetch(getApiUrl('authorize'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }

  public static async revertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('revert'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/sysRole/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
