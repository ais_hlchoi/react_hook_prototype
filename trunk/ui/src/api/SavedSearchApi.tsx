import ApiConfig from "./ApiConfig";

export default class SavedSearchApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportBuilder(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportBuilder'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportBuilderAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportBuilderAll'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportAdvance(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportAdvance'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportAdvanceAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportAdvanceAll'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportCategory(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportCategory'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsReportCategoryAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchReportCategoryAll'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupRoleList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupRoleList'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/savedSearch/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
