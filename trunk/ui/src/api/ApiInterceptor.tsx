import ApiConfig from "../api/ApiConfig";
import fetchIntercept from 'fetch-intercept';

export default class ApiInterceptor {

  private static count = 0;

  public static setInterceptor() {
    if (ApiInterceptor.count > 0)
      return;

    ApiInterceptor.count++;

    return fetchIntercept.register({
      request: function (url, config) {
        // Modify the url or config here
        console.log("url: " + url) ;
        if (url !== ApiConfig.apiDomainUrl + "/login")
          config.headers = {
            'Authorization' : 'Bearer ' + localStorage.getItem('token'), 
            'key': localStorage.getItem('key'),
            'roleId': localStorage.getItem('roleId'),
            'Content-type': 'application/json'
          };
        return [url, config];
      },
      requestError: function (error) {
        // Called when an error occured during another 'request' interceptor call
        // console.log('requestError: ' + error);
        return Promise.reject(error);
      },
      response: function (response) {
        // Modify the reponse object
        return response;
      },
      responseError: function (error) {
        // Handle an fetch error
        // console.log('responseError:' + error);
        return Promise.reject(error);
      },
    });
  }
}