import ApiConfig from "./ApiConfig";
import ReportBuilderProps from "pages/app/ReportBuilderComponent/types/ReportBuilderProps";

export default class ReportTemplateApi {

  public static async getReportNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportProps(model: any): Promise<any> {
    return fetch(getApiUrl('getReportProps'), getHeader(model)).then(res => res.json());
  }

  public static async getReportTemplate(model: any): Promise<any> {
    return fetch(getApiUrl('getReportTemplate'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportAccessGroupRoleList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportAccessGroupRoleList'), getHeader(model)).then(res => res.json());
  }

  public static async getReportScheduleList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportScheduleList'), getHeader(model)).then(res => res.json());
  }

  public static async insertRecords(model: ReportBuilderProps): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportTemplate/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
