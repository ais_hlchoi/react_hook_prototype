import ApiConfig from "./ApiConfig";

export default class ReportAccessGroupApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async getRecord(model: any): Promise<any> {
    return fetch(getApiUrl('find'), getHeader(model)).then(res => res.json());
  }

  public static async getReportNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getReportNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getUserNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getUserNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getRoleNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getRoleNameList'), getHeader(model)).then(res => res.json());
  }

  public static async insertRecords(model: any): Promise<any> {
    return fetch(getApiUrl('insert'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecords(model: any): Promise<any> {
    return fetch(getApiUrl('update'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsReportGroup(model: any): Promise<any> {
    return fetch(getApiUrl('updateReport'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsUserGroup(model: any): Promise<any> {
    return fetch(getApiUrl('updateUser'), getHeader(model)).then(res => res.json());
  }

  public static async updateRecordsRoleGroup(model: any): Promise<any> {
    return fetch(getApiUrl('updateRole'), getHeader(model)).then(res => res.json());
  }

  public static async deleteRecords(model: any): Promise<any> {
    return fetch(getApiUrl('delete'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportAccessGroup/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};
