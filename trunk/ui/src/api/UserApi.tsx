import ApiConfig from "./ApiConfig";

export default class UserApi {

  public static async getUserInfo(props: any): Promise<any> {
    return fetch(getApiUrl('getUserInfo'), getHeader(props)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.sysServices}/api/user/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({ ...model }),
  };
};
