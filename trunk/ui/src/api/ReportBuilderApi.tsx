import ApiConfig from "./ApiConfig";

export default class ReportBuilderApi {

  public static async getFileTypeList(model: any): Promise<any> {
    return fetch(getApiUrl('getFileTypeList'), getHeader(model)).then(res => res.json());
  }

  public static async getTableNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getTableNameList'), getHeader(model)).then(res => res.json());
  }

  public static async getColumnNameList(model: any): Promise<any> {
    return fetch(getApiUrl('getColumnNameList'), getHeader(model)).then(res => res.json());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportBuilder/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify(model),
  };
};
