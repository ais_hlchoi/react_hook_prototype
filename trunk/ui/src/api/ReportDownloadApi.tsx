import ApiConfig from "./ApiConfig";

export default class ReportDownloadApi {

  public static async searchRecords(model: any): Promise<any> {
    return fetch(getApiUrl('search'), getHeader(model)).then(res => res.json());
  }

  public static async searchSchedulerDownloadReport(model: any): Promise<any> {
    return fetch(getApiUrl('searchSchedulerDownloadReport'), getHeader(model)).then(res => res.json());
  }

  public static async searchSchedulerDownloadReportHistory(model: any): Promise<any> {
    return fetch(getApiUrl('searchSchedulerDownloadReportHistory'), getHeader(model)).then(res => res.json());
  }

  public static async searchRecordsAll(model: any): Promise<any> {
    return fetch(getApiUrl('searchAll'), getHeader(model)).then(res => res.json());
  }

  public static async getFile(model: any): Promise<any> {
    const { fileExtension, ...restProps } = model;
    return fetch(getApiUrl('export' + initCap(fileExtension)), getHeader(restProps)).then(res => res.blob());
  }
}

const getApiUrl = (action: string) => {
  return `${ApiConfig.apiDomainUrl}${ApiConfig.vtspServices}/api/reportDownload/${action}`;
};

const getHeader = (model: any) => {
  return {
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    body: JSON.stringify({ ...model }),
  };
};

const initCap = (text: string) => text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
