export type TabComponentProps = {
  menuRef?: any;
  shieldRef?: any;
  componentName?: string;
  errFrameShow?: Function;
  onCloseMessage?: Function;
  onChangeCallback?: Function;
  onAuthButtonClick?: Function;
  onSelectChange?: Function;
  onUpdateRecord?: Function;
  onRefreshRecord?: Function;
};
