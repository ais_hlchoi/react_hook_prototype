export type PageComponentProps = {
  menuRef?: any;
  shieldRef?: any;
  componentName?: string;
  title?: string;
};
