export enum DateFormat {
  DATETIME = "YYYY-MM-DD HH:mm:ss",
  DATE     = "YYYY-MM-DD",
  REPORT   = "_YYYYMMDDHHmmssSSS",
  LOGIN_DATE_TIME = 'DD-MM-yyyy HH:mm:ss',
}
