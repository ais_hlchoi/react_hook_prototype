export enum ResponseCode {
  SUCCESS = "M1000",
  SHIELD  = "M9\\d+",
  LOGOUT  = "M9000",
  LOCKED  = "M9001",
  UNAUTHENTICATED = "1000001", // "E1001"
  UNAUTHORIZATED  = "1000002", // "E1002"
}