export enum MessageType {
  SUCCESS = "success",
  WARNING = "warning",
  DANGER  = "danger",
  ERROR   = "error",
  INFO    = "info",
}