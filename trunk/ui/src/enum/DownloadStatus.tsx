export enum DownloadStatus {
  P,
  R,
  C,
}

export enum DownloadStatusLabel {
  P = "Pending",
  R = "Processing",
  C = "Completed",
  F = "Failure",
}