export enum ReportParamType {
  STRING = "string",
  NUMBER = "number",
  DATE = "date",
  TEXT = "text",
  ARRAY = "array",
}