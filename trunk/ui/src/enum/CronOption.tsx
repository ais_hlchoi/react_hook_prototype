export enum CronOption {
  EVERY_YY_01_MM = "* * * 1 *",
  EVERY_MM_01_DD = "* * 1 * *",
  EVERY_DD_00_HH = "0 0 * * *",
  EVERY_HR_00_MI = "0 * * * *",
  SCHEDULE_TYPE_ONE_TIME = '1',
  SCHEDULE_TYPE_DAILY = '2',
  SCHEDULE_TYPE_WEEKLY = '3',
  SCHEDULE_TYPE_MONTHLY = '4',
}