export enum CronFieldType {
  I = 0,
  H = 1,
  D = 2,
  M = 3,
  W = 4,
}