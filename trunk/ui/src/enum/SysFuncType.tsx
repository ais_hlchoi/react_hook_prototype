export enum SysFuncType {
  MENU    = "MENU",
  FUNC    = "FUNC",
  SUBFUNC = "SUBFUNC",
}