export enum ReportBuildType {
  REPORT_UNKNOWN = "N",
  REPORT_BUILDER = "B",
  REPORT_ADVANCE = "A",
  REPORT_PAGE_TYPE_REPORT_CATEGORY = 'ReportCategory',
  REPORT_PAGE_TYPE_SAVE_SEARCH = 'SavedSearch',
}