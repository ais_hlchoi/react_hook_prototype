export enum ReportLayoutType {
  HORIZONTAL = 'H',
  VERTICAL   = 'V',
}