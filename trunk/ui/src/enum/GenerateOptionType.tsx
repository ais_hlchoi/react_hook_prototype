export enum GenerateOptionType {
  BOTH          = "B",
  ONLINE_ONLY   = "O",
  SCHEDULE_ONLY = "S",

  BOTH_TEXT          = "Both",
  ONLINE_ONLY_TEXT   = "Online Only",
  SCHEDULE_ONLY_TEXT = "Schedule Only",
  ONLINE_SCHEDULE_TEXT = "Online / Scheduled",
}
