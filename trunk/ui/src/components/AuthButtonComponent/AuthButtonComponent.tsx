import React, { FunctionComponent } from "react";
import { Form, Row, Col, Button } from "antd";
import { PlusCircleOutlined, EditOutlined, DeleteOutlined, ClockCircleOutlined, VerticalAlignBottomOutlined, RollbackOutlined } from "@ant-design/icons";

import "./AuthButtonComponent.scss";
import { TabEnum } from "enum";

type AuthButtonComponentProps = {
  onAuthButtonClick: Function;
  show?: boolean|number;
  showAuthButton?: boolean;
  showAdd: boolean;
  showEdit: boolean;
  showDelete: boolean;
  showAudit: boolean;
  showExport: boolean;
  showRevert: boolean;
};

const AuthButtonComponent: FunctionComponent<any> = ({ show = true, showAdd= true, showEdit= true, showDelete= true, showAudit= true,showExport= true, showRevert= false, ...props }: AuthButtonComponentProps) => {
  const [form] = Form.useForm();

  if (!show) {
    return null;
  }

  const { onAuthButtonClick } = props;
  const onClick = (value: string) => onAuthButtonClick(value);

  return (
    <div className="AuthButtonWrapper">
      <Form form={form} name="form" {...formItemLayout} className="buttonRelative">
        <Row gutter={24}>
          <Col span={18}>
            {showAdd && <Button type="primary" className="btn-tabAction btn-add" onClick={() => onClick(TabEnum.ADD)}    children="Add"         icon={<PlusCircleOutlined />} />}
            {showEdit && <Button type="primary" className="btn-tabAction btn-edt" onClick={() => onClick(TabEnum.EDIT)}   children="Edit"        icon={<EditOutlined />} />}
            {showDelete && <Button type="primary" className="btn-tabAction btn-del" onClick={() => onClick(TabEnum.DELETE)} children="Delete"      icon={<DeleteOutlined />} />}
            {showRevert && <Button type="primary" className="btn-tabAction btn-revert" onClick={() => onClick(TabEnum.REVERT)} children="Revert"      icon={<RollbackOutlined />} />}
            {showAudit && <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onClick(TabEnum.AUDIT)}  children="Audit"       icon={<ClockCircleOutlined />} />}
            {showExport && <Button type="primary" className="btn-tabAction btn-exp" onClick={() => onClick(TabEnum.EXPORT)} children="Export"      icon={<VerticalAlignBottomOutlined />} />}
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>&nbsp;</Col>
        </Row>
      </Form>
    </div>
  );
};

export default AuthButtonComponent;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
  },
};
