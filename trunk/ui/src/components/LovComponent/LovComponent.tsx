import React, { FunctionComponent, useState } from "react";

import "./LovComponent.scss";
import {
  Table, Input,
} from "antd";

type LovComponentProps = {
  columns: Array<any>;
  data: Array<any>;
  onSearchSelect: Function;
};


const LovComponent: FunctionComponent<LovComponentProps> = ({
  columns,
  data,
  onSearchSelect,
}) => {
  //const [form] = Form.useForm();
  const onSearchSelectClick = (value) => {
    onSearchSelect(value);
    console.log(value);
  } 
  const [selectionType] = useState<any>("checkbox");//const [selectionType, setSelectionType] = useState<any>("checkbox");
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
  const { Search } = Input;
  return (
    <div>
      <Search
        placeholder="input search text"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearchSelectClick}
      />

      <Table rowSelection={{type: selectionType,...rowSelection}}
             columns={columns}
             dataSource={data}
      />
    </div>
  );
};
export default LovComponent;
