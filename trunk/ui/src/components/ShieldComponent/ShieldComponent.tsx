/* eslint-disable */
import React, { FunctionComponent, useContext } from "react";
import { useHistory } from "react-router-dom";
import { Modal, Button } from "antd";
import { CloseCircleTwoTone } from '@ant-design/icons';

import "./ShieldComponent.scss";
import { LoginApi } from "api";
import { ResponseCode } from "enum";
import { common } from "shared/common";
import { ServicesContext } from "services/contexts/ServicesContext";

type ShieldComponentProps = {
  title?: string;
  content: string;
  closeText?: string;
  onClose?: Function;
  onCloseHref?: string;
  timer?: number;
  show?: boolean|number;
};

const ShieldComponent: FunctionComponent<ShieldComponentProps> = (props: any) => {
  const history = useHistory();
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;

  const { show = false } = props;
  if (!show) {
    return null;
  }

  const onChangeHistory = (url: string) => {
    if (url === "/reset") {
      LoginApi.breakIn().then(() => {
        AuthenticationService.setUserModel({});
        history.push("/redirect");
      });
      return;
    }
    if (url === "/signin") {
      LoginApi.signOut().then((response) => {
        const { responseCode, responseDesc } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          AuthenticationService.setUserModel(null);
          localStorage.clear();
        } else {
          console.log(responseDesc);
        }
      });
    }
    history.push(url);
  };

  const { title, content, footer, closeText, onClose, onCloseHref, timer } = props;

  const modalProps = {
    centered: true,
    closable: false,
    footer: null,
    keyboard: false,
    mask: true,
    maskClosable: false,
    visible: true,
    width: "50%",
    wrapClassName: "ShieldWrapper",
  };

  if (common.isNotEmpty(title)) {
    Object.assign(modalProps, { title });
  }

  if (common.isNotEmpty(onClose)) {
    Object.assign(modalProps, {
      maskClosable: true,
      footer: [
        <Button key="ok" onClick={onClose} children={common.isEmpty(closeText) ? 'OK' : closeText} danger={true} />,
      ],
    });
  }
  else if (common.isNotEmpty(onCloseHref)) {
    Object.assign(modalProps, {
      maskClosable: true,
      footer: [
        <Button key="ok" onClick={() => onChangeHistory(onCloseHref)} children={common.isEmpty(closeText) ? 'OK' : closeText} danger={true} />,
      ],
    });
  }
  else if (common.isNotEmpty(footer)) {
    Object.assign(modalProps, {
      footer: footer.map(({text, href, ...restProps}, key: number) => <Button key={key} onClick={() => onChangeHistory(href)} children={text} {...restProps} />),
    });
  }

  return <Modal {...modalProps}>{content}</Modal>;
};

export default ShieldComponent;

class ShieldUtils {
  static convertToShieldProps = (props: any) => {
    const { responseCode, ...restProps } = props;
    switch (responseCode) {
      case ResponseCode.LOGOUT:
        return ShieldUtils.presetPropsLogout({ ...restProps });
      case ResponseCode.LOCKED:
        return ShieldUtils.presetPropsLocked({ ...restProps });
      default:
        return props;
    }
  };
  static presetPropsLogout = (props: any) => {
    const { responseDesc } = props;
    const content = <h3>{responseDesc.split('\n').map((o: string, n: number) => <div key={n}>{o}</div>)}</h3>;
    return { ...EmptyShieldProps(), content, closeText: 'Sign Out', onCloseHref: '/signin' };
  };
  static presetPropsLocked = (props: any) => {
    const { responseDesc } = props;
    const content = (
      <div className="table">
        <div className="tableCellIcon"><CloseCircleTwoTone twoToneColor="red" style={{fontSize: 36}} /></div>
        <div className="tableCellText"><h3>{responseDesc.split('\n').map((o: string, n: number) => <div key={n}>{o}</div>)}</h3></div>
      </div>
    );
    if (responseDesc.toLowerCase().indexOf('online') > 0) {
      const footer = [
        { text: 'Login', href: '/reset', danger: true },
        { text: 'Back' , href: '/signin' },
      ];
      return { ...EmptyShieldProps(), content, footer };
    }
    return { ...EmptyShieldProps(), content, closeText: 'Sign Out', onCloseHref: '/signin' };
  };
  static presetPropsTimer = (props: any) => {
    const { object = {} } = props;
    const { timer = 0 } = object;
    if (timer > 0) {
      // do setTimeInterval
    }
    return props;
  };
  static EmptyShieldProps = (): ShieldProps => ({
    tilte: '',
    content: '',
    show: false,
  });
}

const { EmptyShieldProps } = ShieldUtils;

export interface ShieldProps {
  tilte: string;
  content: string;
  show: boolean|number;
}

export { ShieldUtils, EmptyShieldProps };
