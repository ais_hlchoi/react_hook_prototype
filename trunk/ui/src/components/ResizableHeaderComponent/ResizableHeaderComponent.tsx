import React, { FunctionComponent } from "react";
import { Resizable } from "react-resizable";

import "./ResizableHeaderComponent.scss";

const ResizableHeaderComponent: FunctionComponent<any> = ({
  onResize,
  width,
  onClick,
  ...restProps
}) => {
  let resizing = false;
  if (!width) {
    return <th {...restProps} />;
  }

  return (
    <Resizable
      width={width}
      height={0}
      onResizeStart={() => (resizing = true)}
      onResizeStop={() => {
        setTimeout(() => {
          resizing = false;
        });
      }}
      onResize={onResize}
    >
      <th
        onClick={(...args) => {
          console.log(">>>", resizing);
          if (!resizing && onClick) {
            onClick(...args);
          }
        }}
        {...restProps}
      />
    </Resizable>
  );
};

export default ResizableHeaderComponent;
