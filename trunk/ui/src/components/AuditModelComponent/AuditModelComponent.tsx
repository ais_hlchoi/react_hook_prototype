import React, { FunctionComponent } from "react";
import { Modal, Row, Col, Button } from "antd";
import moment from 'moment';

import "./AuditModelComponent.scss";
import { DateFormat } from "enum";

type AuditModelComponentProps = {
  model: any;
  onClose: Function;
  show?: boolean|number;
};

const AuditModelComponent: FunctionComponent<AuditModelComponentProps> = (props: any) => {
  const { show = false } = props;
  if (!show) {
    return null;
  }

  const { model: { createdBy, createdDate, lastUpdatedBy, lastUpdatedDate }, onClose } = props;

  return (
    <Modal
      className="AuditModelWrapper"
      title="Audit Message"
      visible={true}
      onCancel={onClose}
      maskClosable={false}
      destroyOnClose
      footer={[
        <Button key = "close" onClick={() => onClose()}>Cancel</Button>
      ]}
    >
      <Row gutter={24} className="auditRowMarginBottom">
          <Col span={10}>Created By：</Col>
          <Col span={14}>{createdBy}</Col>
      </Row>
      <Row gutter={24} className="auditRowMarginBottom">
          <Col span={10}>Created Date：</Col>
          <Col span={14}>{moment(createdDate).format(DateFormat.DATETIME)}</Col>
      </Row>
      <Row gutter={24} className="auditRowMarginBottom">
          <Col span={10}>Last Updated By：</Col>
          <Col span={14}>{lastUpdatedBy}</Col>
      </Row>
      <Row gutter={24} className="auditRowMarginBottom">
          <Col span={10}>Last Updated Date：</Col>
          <Col span={14}>{moment(lastUpdatedDate).format(DateFormat.DATETIME)}</Col>
      </Row>
    </Modal>
  );
};

export default AuditModelComponent;
