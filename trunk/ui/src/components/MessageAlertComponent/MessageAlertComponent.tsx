import React, { FunctionComponent } from "react";
import { Row, Col, Alert } from "antd";

import "./MessageAlertComponent.scss";
import { MessageType } from "enum";

type MessageAlertComponentProps = {
  type: MessageType;
  title?: string;
  content: string;
  onClose: Function;
  show?: boolean|number;
};

const MessageAlertComponent: FunctionComponent<MessageAlertComponentProps> = (props: any) => {
  const { show = false } = props;
  if (!show) {
    return null;
  }

  const typeHandler: any = (type: string) => {
    if (type === MessageType.DANGER)
      return MessageType.ERROR;
    if ([ MessageType.SUCCESS, MessageType.INFO, MessageType.WARNING, MessageType.ERROR ].some(o => o === type))
      return type;
    return MessageType.INFO;
  };
  
  const titleHandler: any = (title: string, type: string) => {
    if ((title || '').trim().length === 0) {
      switch (type) {
        case MessageType.ERROR:
          title = "Error";
          break;
        case MessageType.SUCCESS:
          title = "Message";
          break;
        case MessageType.WARNING:
          title = "Warning";
          break;
        default:
          title = "Message";
          break;
      }
    }
    return title;
  };
  
  const contentHandler: any = (content: string) => {
    const arr = (content || '').split(';');
    return arr.length > 1 ? arr.filter((o) => o.trim() !== '').map((o, n) => `${n + 1}.${o}`).join('<br/>') : content;
  };

  const { type, title, content, onClose } = props;

  return (
    <div className="messageAlertWrapper">
      <Row gutter={24}>
        <Col span={24}>
          <div className="alertWrapper">
            <Alert
              type={typeHandler(type)}
              message={titleHandler(title)}
              description={contentHandler(content)}
              afterClose={onClose}
              closable
            />
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default MessageAlertComponent;

export interface MessageAlertProps {
  type: MessageType;
  tilte: string;
  content: string;
  show: boolean|number;
}

export function EmptyMessageAlertProps(): MessageAlertProps {
  return {
    type: MessageType.INFO,
    tilte: '',
    content: '',
    show: false,
  };
}

export { MessageType };
