import React, { FunctionComponent, useEffect, useState } from "react";
import { Form, Row, Col, Input, Button, Select } from "antd";
import { SearchOutlined, RedoOutlined } from "@ant-design/icons";

import "./SearchBarComponent.scss";

type SearchBarComponentProps = {
  onSearch: Function;
  onReset: Function;
  searchFieldList: Array<any>;
};

const SearchBarComponent: FunctionComponent<SearchBarComponentProps> = ({
  onSearch,
  onReset,
  searchFieldList,
}) => {
  const [form] = Form.useForm();
  //const [visible, setVisible] = useState(false);
  const [formItemList, setFormItemList] = useState<Array<any>>([]);

  const onSearchClick = () => {
    onSearch(form.getFieldsValue());
  };

  const onResetClick = () => {
    form.resetFields();
    onReset();
  };

  /*const handleVisibleChange = (visible: boolean) => {
    setVisible(visible);
  };

  const onFilterCheckboxGroupChange = (checkedValues: any) => {
    setFormItemList([...checkedValues]);
    console.log(checkedValues);
  };*/

  /*const filterMenu = () => {
    return (
      <Checkbox.Group onChange={onFilterCheckboxGroupChange}>
        <Menu selectable={false}>
          {searchFieldList.map((searchField, index) => {
            if (searchField.isDefaultDisplayed) {
              return null;
            } else {
              return (
                <Menu.Item key={index}>
                  <Checkbox
                    defaultChecked={searchField.isDefaultDisplayed}
                    value={searchField}
                  >
                    {searchField.label}
                  </Checkbox>
                </Menu.Item>
              );
            }
          })}
        </Menu>
      </Checkbox.Group>
    );
  };*/

  useEffect(() => {
    if (searchFieldList && searchFieldList.length) {
      setFormItemList([...searchFieldList]);
    }
  }, [searchFieldList])

  const splitToRow = (formItemList: Array<any>) => {
    let count = 0;
    let rows: Array<JSX.Element> = [];
    while (count < formItemList.length) {
      const columns: Array<JSX.Element> = [];
      for (let i = count; i < count + 2; i++) {
        if (formItemList[i]) {
          if (formItemList[i].type === "selectOption")
            columns.push(
              <Col span={12} key={i}>
                <Form.Item
                  name={formItemList[i].key}
                  label={formItemList[i].label}
                  labelAlign={"left"}
                >
                  {formItemList[i] && formItemList[i].element ? (
                    formItemList[i].element
                  ) : (
                    <Select 
                      style={{ width: 120 }} 
                      //defaultValue={formItemList[0].list[0].value} //{AuthenticationService.getUserModel().roleId}
                      //onChange={value => props.onChange(value)}
                    >
                      {formItemList[i].list.map(item => (
                        <Select.Option
                          key={item.value}//{item.roleId}
                          value={item.value}//{item.roleId}
                        >
                          {item.label}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            )
          else{
            columns.push(
              <Col span={12} key={i}>
                <Form.Item
                  name={formItemList[i].key}
                  label={formItemList[i].label}
                  labelAlign={"left"}
                >
                  {formItemList[i] && formItemList[i].element ? (
                    formItemList[i].element
                  ) : (
                    <Input />
                  )}
                </Form.Item>
              </Col>
            );
          }
        }
      }

      rows.push(<Row gutter={24} style={{marginBottom: 12}} key={rows.length}>{columns}</Row>);
      count += 2;
    }
    return rows;
  };

  return (
    <Form form={form} name="form" id="searchForm" {...formItemLayout}>
      <Row gutter={24} style={{marginBottom: 12}}>
        <Col span={12}>
          <Form.Item
            name={"defaultSearchText"}
            label={"Search"}
            labelAlign={"left"}
          >
            <Input autoComplete="off" />
          </Form.Item>
        </Col>
        {/*<Col span={4}>
          {
            searchFieldList.length ?
              <Dropdown
                  overlay={filterMenu}
                  trigger={["click"]}
                  onVisibleChange={handleVisibleChange}
                  visible={visible}>
                  <Button type="primary">
                      Filter <DownOutlined />
                  </Button>
              </Dropdown> : <></>
          }
        </Col>*/}
      </Row>
      <>
        {splitToRow(formItemList)}
      </>
      <Row>
        <Col span={20}/>
        <Col span={4}>
          <Button
            style={{ margin: "0 8px" }}
            type="primary"
            onClick={onSearchClick}
            icon={<SearchOutlined />}
          >
            Search
          </Button>
          <Button style={{ margin: "0 8px" }} onClick={onResetClick} icon={<RedoOutlined/>} >
            Reset
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

export default SearchBarComponent;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
  },
};
