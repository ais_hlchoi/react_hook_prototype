import React, {FunctionComponent} from "react";
import {Modal} from "antd";
import {ExclamationCircleOutlined} from '@ant-design/icons';


type ConfirmModelComponentProps = {
  onClose: Function;
  onOk: Function;
  show?: boolean | number;
};

const ConfirmModelComponent: FunctionComponent<ConfirmModelComponentProps> = (props: any) => {
  const {show = false, onClose, onOk} = props;
  if (!show) {
    return null;
  }

  const confirm = () => {
    Modal.confirm({
      title: 'Warning',
      icon: <ExclamationCircleOutlined/>,
      content: 'There are unsaved record. Are you sure to leave?',
      okText: 'Confirm',
      cancelText: 'Cancel',
      onOk() {
        return onOk();
      },
      onCancel() {
        return onClose();
      },
    });
    return <></>;
  }


  return (
    <>
      {confirm()}
    </>
  );
}

export default ConfirmModelComponent;
