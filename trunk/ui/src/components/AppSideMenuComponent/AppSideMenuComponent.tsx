import React, { FunctionComponent, useContext, useEffect, useState, useImperativeHandle } from "react";
import { Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import { MailOutlined, FileOutlined, FieldTimeOutlined, SettingOutlined, CarryOutOutlined, LockOutlined } from "@ant-design/icons";

import "./AppSideMenuComponent.scss";
import SideMenuCollapsedContext from "pages/layout/contexts/SideMenuCollapsedContext";
import { ServicesContext } from "services/contexts/ServicesContext";
import { common } from "shared/common";

const { Sider } = Layout;
const { SubMenu } = Menu;

type AppSideMenuComponentProps = {
  menuList: [];
  menuRef: any;
}

const menuMap = {
  'VTSP100' : <FileOutlined/>,
  'VTSP200' : <FieldTimeOutlined/>,
  'VTSP301' : <MailOutlined/>,
  'VTSP400' : <MailOutlined/>,
  'VTSP501' : <MailOutlined/>,
  'VTSP600' : <SettingOutlined/>,
  'VTSP700' : <CarryOutOutlined/>,
  'SYS100'  : <LockOutlined/>,
}

const AppSideMenuComponent: FunctionComponent<AppSideMenuComponentProps> = (props: any) => {
  const [subMenuList, setSubMenuList] = useState([]);
  const [openKeys, setOpenKeys] = useState<any>([]);
  const [selectedKeys, setSelectedKeys] = useState<any>([]);
  const { collapsed, setCollapsed } = useContext(SideMenuCollapsedContext);
  const { AuthenticationService } = useContext(ServicesContext);
  const { menuList, menuRef } = props;

  useImperativeHandle(menuRef, () => ({
    click: (value: any) => {
      const { funcId } = value;
      setOpenKeys(AuthenticationService.getPathList().filter((o: any) => o.funcId === funcId).map((o: any) => `${o.parentId}`));
      setSelectedKeys([`${funcId}`]);
    }
  }));

  useEffect(() => {
    const createSubMenus = (menuList: any): any => {
      const menus: any = [];
      for (const subMenuItem of menuList) {
        if (subMenuItem.children && subMenuItem.children.length > 0) {
          menus.push(
            <SubMenu key={subMenuItem.funcId} title={
              <span>
                {menuMap[subMenuItem.funcCode] || <MailOutlined/>}
                <span className="fontWt">{subMenuItem.funcName}</span>
              </span>
            }>
              {createSubMenus(subMenuItem.children)}
            </SubMenu>
          );
        } else if (common.isEmpty(subMenuItem.funcUrl)) {
          menus.push(
            <Menu.Item key={subMenuItem.funcId} onClick={() => { onClickMenuItem(subMenuItem) }}>
              <span>
                {menuMap[subMenuItem.funcCode] || <MailOutlined/>}
                <span className="fontWt">{subMenuItem.funcName}</span>
              </span>
            </Menu.Item>
          );
        } else {
          menus.push(
            <Menu.Item key={subMenuItem.funcId} onClick={() => { onClickMenuItem(subMenuItem) }}>
              {subMenuItem.funcName}
              <Link to={"/app" + subMenuItem.funcUrl} replace />
            </Menu.Item>
          );
        }
      }
      return menus;
    };

    const onClickMenuItem = (subMenuItem: any) => {
      if (common.isNotEmpty(subMenuItem.funcId)) {
        AuthenticationService.setFuncId(subMenuItem.funcId);
      }
    };

    setSubMenuList(createSubMenus(menuList));
  }, [menuList, AuthenticationService, selectedKeys]);

  const onCollapseSider = (collapsed: boolean) => {
    setCollapsed(collapsed);
  };

  const onOpenMenu = (value: any) => {
    const keys = value.find((key: any) => openKeys.indexOf(key) === -1);
    setOpenKeys(keys ? [keys] : []);
  };

  const onClickMenu = (value: any) => {
    const keys = value.key;
    setSelectedKeys(keys ? [keys] : []);
  };

  return (
    <div className="AppSideMenuWrapper">
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapseSider} width={280}>
        <div className="logo" />
        <Menu theme="dark" mode="inline" selectedKeys={selectedKeys} openKeys={openKeys} onOpenChange={onOpenMenu} onClick={onClickMenu}>
          {subMenuList}
        </Menu>
      </Sider>
    </div>
  );
};

export default AppSideMenuComponent;
