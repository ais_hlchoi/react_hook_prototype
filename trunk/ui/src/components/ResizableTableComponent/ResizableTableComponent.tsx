import React, { FunctionComponent, useEffect } from "react";
import { Table } from "antd";

import "./ResizableTableComponent.scss";
import ResizableHeaderComponent from "../ResizableHeaderComponent";
import { useRefState } from "shared/useRefState";

const ResizableTableComponent: FunctionComponent<any> = (props) => {
  const [columns, columnsRef, setColumns] = useRefState(
    props.columns.map((col: any, index: number) => ({
      ...col,
      onHeaderCell: (column: any) => ({
        width: column.width,
        onResize: handleResize(index),
      }),
    }))
  );

  useEffect(() => {
    const handlers = document.querySelectorAll(
      ".react-resizable .react-resizable-handle"
    );
    handlers.forEach((handler) =>
      handler.addEventListener("click", (e) => {
        e.stopPropagation();
        return false;
      })
    );
  }, []); // only trigger once

  const handleResize = (index: number) => (
    e: React.SyntheticEvent<Element, Event>,
    { size }: any
  ) => {
    e.stopPropagation();

    const nextColumns = [...(columnsRef.current as any)];
    nextColumns[index] = {
      ...nextColumns[index],
      width: size.width,
    };
    setColumns(nextColumns);
  };

  const components = {
    ...(props.components || {}),
    header: {
      cell: ResizableHeaderComponent,
    },
  };

  return (
    <Table
      {...props}
      tableLayout={"fixed"}
      columns={columns}
      components={components}
      scroll={{ x: true, y: false }}
    ></Table>
  );
};
export default ResizableTableComponent;
