import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Popover, Layout, Button, Select, Input } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

import "./AppHeaderComponent.scss";
import SideMenuCollapsedContext from "pages/layout/contexts/SideMenuCollapsedContext";
import { LoginApi, OperationLogApi } from "api";
import { DateFormat, ResponseCode } from "enum";
import { ServicesContext } from "services/contexts/ServicesContext";
import moment from 'moment';

type AppHeaderComponentProps = {
  shieldRef?: any;
  onChangeRole?: Function;
};

const AppHeaderComponent: FunctionComponent<AppHeaderComponentProps> = (props: any) => {
  const history = useHistory();
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;
  const [userRoleList] = useState<Array<any>>([]);
  const [isShowUserRoleList] = useState<boolean>(false);
  const [isShowUserRole] = useState<boolean>(false);
  const {collapsed, setCollapsed} = useContext(SideMenuCollapsedContext);
  const [loginInformation, setLoginInformation] = useState<any>(null);
  const { shieldRef } = props;
  const toggle = () => setCollapsed(!collapsed);

  const onClickLogout = () => {
    LoginApi.signOut().then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        AuthenticationService.setUserModel(null);
        history.push("/signin");
        localStorage.clear();
        setLoginInformation(null);
      } else {
        console.log(responseDesc);
      }
    });
  };

  const getUserRoleList = () => {
    if (AuthenticationService.getUserModel() && AuthenticationService.getUserModel().roleList) {
      return AuthenticationService.getUserModel().roleList.join(',').trim();
    }
    return '';
  };

  const getUserRoleListShortForm = () => {
    const text = getUserRoleList();
    if (text.length <= 30) {
      return text;
    } else {
      return AuthenticationService.getUserModel().roleList[0].trim() + '...';
    }
  };

  const { onChangeRole = (_value: any) => {} } = props;

  useEffect(() => {
    const isLogin = null != AuthenticationService.getUserModel();
    if (!loginInformation && isLogin) {
      OperationLogApi.getLoginActivity({ page: 1, pageSize: 2,total: 0 }).then((response) => {
        const { responseCode, responseDesc } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const { object: { list } } = response;
          const loginInformation = list.filter((l: any) => 'LOGIN' === l.operationType && 'SUCCESS' === l.status ).sort((a,b) => a.operationTime - b.operationTime)[0];
          setLoginInformation(loginInformation || {});
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          console.log(responseDesc);
        }
      });
    }
  }, [loginInformation, shieldRef, AuthenticationService])

  const userId = ({ userId: '', ...AuthenticationService.getUserModel() }).userId;
  const roleId = ({ roleId: '', ...AuthenticationService.getUserModel() }).roleId;

  return (
    <Layout.Header className="site-layout-background">
      {React.createElement(
        collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
        {
          className: "headMenu trigger",
          onClick: toggle,
        }
      )}
      <span className="headLogo"><img src="./images/top_logo.png" alt="DBS" /></span>
      <span className="headTitle">Vickers Trading Support Platform</span>
      <span className="headUserInfo">
          <div className="userName">
            <span>
              <Popover placement="topLeft" content={ <div>  {getUserRoleList()} </div> } arrowPointAtCenter>Role：{getUserRoleListShortForm()}</Popover>
              &nbsp;&nbsp;
              User Name：{userId}
              &nbsp;&nbsp;
              Last Login Datetime: { loginInformation && moment(loginInformation.operationTime).format(DateFormat.LOGIN_DATE_TIME) }
            </span>
          </div>
          {isShowUserRole && <div className="userRole">
            <span>Role Code：</span>
            {isShowUserRoleList ? <>
            <Select className="select-userRole" defaultValue={roleId} onChange={onChangeRole}>
              {userRoleList.map(item => <Select.Option key={item.roleId} value={item.roleId} children={item.roleName} />)}
            </Select>
            </> : <>
            <Input className="input-userRole" value={userRoleList.map(item => item.roleName)[0]} readOnly />
            </>}
          </div>}
          <div className="signOut">
            <Button danger={true} onClick={onClickLogout} children=" Sign Out " />
          </div>
      </span>
    </Layout.Header>
  );
}

export default AppHeaderComponent;
