import React, { FunctionComponent, useState } from "react";
import { Divider, Tabs } from "antd";

import "./UploadLogComponent.scss";
import { UploadLogSearchTabComponent } from "./tabs";
import { MessageAlertComponent, EmptyMessageAlertProps } from "components";
import { AuditModelComponent } from "components";
import { MessageType, TabEnum } from "enum";
import { PageComponentProps } from "types";
import { common } from "shared/common";

type UploadLogComponentProps = PageComponentProps;

const UploadLogComponent: FunctionComponent<UploadLogComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey] = useState(TabEnum.DEFAULT_TAB);
  const [, setSelectedRow] = useState<any>({});
  const { shieldRef, title = 'User Log' } = props;

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.AUDIT:
        if (common.isNotEmpty(selectedRow)) {
          setAuditModel({ ...auditModel, show: true });
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setSelectedRow(model);
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setSelectedRow(model);
    setAuditModel({ ...auditModel, model });
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const propsTabSummary = { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange };

  return (
    <div className="UploadLogWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
          <UploadLogSearchTabComponent {...propsTabSummary} />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default UploadLogComponent;

const { TabPane } = Tabs;
