import React, { FunctionComponent, useState } from "react";
import { DatePicker, Button, Table, Menu, Dropdown, Checkbox, Spin, Divider, Row, Col, Typography, Tooltip } from "antd";
import { ClockCircleOutlined, DownOutlined } from "@ant-design/icons";

import "./UploadLogSearchTabComponent.scss";
import { OperationLogApi } from "api";
import { SearchBarComponent, ResizableTableComponent } from "components";
import { DateFormat, MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import moment from "moment";

type UploadLogSearchTabComponentProps = TabComponentProps;

const UploadLogSearchTabComponent: FunctionComponent<UploadLogSearchTabComponentProps> = (props: any) =>  {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState({page : 1, pageSize: 10, total : 0});
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const model = { ...EmptyPageProps(), ...value };
    OperationLogApi.getUploadActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
    setSearchModel(EmptyPageProps());
  };

  const onButtonClick = (action: any) => {
    if (action === TabEnum.AUDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
  }

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = { ...searchModel, page, pageSize };
    setLoading(true);
    OperationLogApi.getUploadActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'operationLogId';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  const onClick = (value: string) => onButtonClick(value);

  return (
    <div className="UploadLogSearchTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={searchFieldList}
      />
      <Divider />
      <Row gutter={24} className="buttonRelative">
        <Col span={12}>
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onClick(TabEnum.AUDIT)}    children="Audit"    icon={<ClockCircleOutlined />} />
        </Col>  
      </Row>
      <Row gutter={24}>
        <Col span={12}>&nbsp;</Col>
      </Row>
      <ResizableTableComponent
        className={"sysTable" + (searchList.length ? " tableTop" : "")}
        bordered
        rowKey={rowKey}
        rowSelection={rowSelection}
        dataSource={searchList}
        columns={summaryColumns}
        pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default UploadLogSearchTabComponent;

class UploadLogSearchTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });

  static checkList: any[] = [];

  static onFilterCheckboxGroupChange = (checkedValues: any) => {
    UploadLogSearchTabUtils.checkList = [...checkedValues];
    console.log(checkedValues);
  };

  static filterMenu = () => {
    return (
      <Checkbox.Group onChange={UploadLogSearchTabUtils.onFilterCheckboxGroupChange}>
        <Menu selectable={false}>
        {UploadLogSearchTabUtils.checkList.map((searchField, index) => <Menu.Item key={index}><Checkbox defaultChecked = {true} value={searchField} children={searchField.label} /></Menu.Item>)}
        </Menu>
      </Checkbox.Group>
    );
  };
}

const { EmptyPageProps, filterMenu } = UploadLogSearchTabUtils;
const { Text } = Typography;

const searchFieldList = [
  {
    label: "User Code",
    key: "userCode"
  },
  {
    label: "User Name",
    key: "userName",
  },
  {
    label: "Operation Type",
    key: "operationType",
    element: (
      <Dropdown overlay={filterMenu} visible={true}>
        <Button type="default">
          Operation Type <DownOutlined />
        </Button>
      </Dropdown>
    ),
  },
  {
    label: "Start Datetime",
    key: "opeartionTime",
    element: (
      <DatePicker showTime={true} format={DateFormat.DATETIME}></DatePicker>
    ),
  },
  {
    label: "End Datetime",
    key: "responseTime",
    element: (
      <DatePicker showTime={true} format={DateFormat.DATETIME}></DatePicker>
    ),
  }
];

const summaryColumns = [  
  {
    title: "Operation Content",
    dataIndex: "operationContent",
    key: "operationContent",
    render: (cell: any) => <Tooltip title={cell} overlayClassName="UserLogSearchTabTooltip"><Text className="td-operationContent" ellipsis>{cell}</Text></Tooltip>,
  },
  {
    title: "Operation Time",
    dataIndex: "operationTime",
    key: "operationTime",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
  },
  {
    title: "Response Desc",
    dataIndex: "responseDesc",
    key: "responseDesc",
  },  
];
