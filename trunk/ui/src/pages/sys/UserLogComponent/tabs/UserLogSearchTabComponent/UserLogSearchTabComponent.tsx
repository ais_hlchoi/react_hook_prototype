import React, { FunctionComponent, useState, useImperativeHandle, useEffect } from "react";
import { DatePicker, Button, Table, Menu, Checkbox, Spin, Divider, Row, Col } from "antd";
import { ClockCircleOutlined } from "@ant-design/icons";

import "./UserLogSearchTabComponent.scss";
import { OperationLogApi, SysParmApi } from "api";
import { SearchBarComponent, ResizableTableComponent } from "components";
import { DateFormat, MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import moment from "moment";

type UserLogSearchTabComponentProps = TabComponentProps & {
  searchRef: any;
};

const UserLogSearchTabComponent: FunctionComponent<UserLogSearchTabComponentProps> = (props: any) =>  {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(EmptyPageProps());
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);
  const [operationType, setOperationType] = useState([]);

  useImperativeHandle(props.searchRef, () => ({
    search: () => {
      onSearch(searchModel);
    },
    updateSuccess: (value: any) => {
      errFrameShow(MessageType.SUCCESS, value);
    }
  }));

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const model = { ...EmptyPageProps(), ...value };
    OperationLogApi.getUserActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
    setSearchModel(EmptyPageProps());
  };

  const onButtonClick = (action: any) => {
    if (action === TabEnum.AUDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
  }

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = { ...searchModel, page, pageSize };
    setLoading(true);
    OperationLogApi.getUserActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'operationLogId';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  useEffect(() => {
    if (!operationType.length) {
      const requestModel = {page: 1,pageSize: 20,total : 0,segment: 'OPERATION_TYPE'}
      SysParmApi.searchSysParmValueBySegment(requestModel).then(response => {
        const { responseCode, responseDesc } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const list = response.object;
          setOperationType(list.map((value,index) => {return { id:index, label: value.code, value: value.code } }));
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      })
    }
  },[operationType, errFrameShow, shieldRef])

  const searchFieldList = [
    {
      label: "User Code",
      key: "userCode"
    },
    {
      label: "User Name",
      key: "userName",
    },
    {
      label: "Operation Type",
      key: "operationType",
      type: "selectOption",
      list: operationType,
    },
    {
      label: "Start Datetime",
      key: "startDate",
      element: (
        <DatePicker showTime={true} format={DateFormat.DATETIME}/>
      ),
    },
    {
      label: "End Datetime",
      key: "endDate",
      element: (
        <DatePicker showTime={true} format={DateFormat.DATETIME}/>
      ),
    }
  ];

  return (
    <div className="UserLogSearchTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={searchFieldList}
      />
      <Divider />
      <Row gutter={24} className="buttonRelative">
        <Col span={12}>
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onButtonClick(TabEnum.AUDIT)} children="Audit" icon={<ClockCircleOutlined />} />
        </Col>  
      </Row>
      <Row gutter={24}>
        <Col span={12}>&nbsp;</Col>
      </Row>
      <ResizableTableComponent
        className={"sysTable" + (searchList.length ? " tableTop" : "")}
        bordered
        rowKey={rowKey}
        rowSelection={rowSelection}
        dataSource={searchList}
        columns={summaryColumns}
        expandable={{
          expandedRowRender: (record: any) => <p style={{ margin: 0 }}>{record.operationContent}</p>
        }}
        pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default UserLogSearchTabComponent;

class UserLogSearchTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });

  static checkList: any[] = [];

  static onFilterCheckboxGroupChange = (checkedValues: any) => {
    UserLogSearchTabUtils.checkList = [...checkedValues];
    console.log(checkedValues);
  };

  static filterMenu = () => {
    return (
      <Checkbox.Group onChange={UserLogSearchTabUtils.onFilterCheckboxGroupChange}>
        <Menu selectable={false}>
        {UserLogSearchTabUtils.checkList.map((searchField, index) => <Menu.Item key={index}><Checkbox defaultChecked = {true} value={searchField} children={searchField.label} /></Menu.Item>)}
        </Menu>
      </Checkbox.Group>
    );
  };
}

const { EmptyPageProps } = UserLogSearchTabUtils;

const summaryColumns = [
  {
    title: "User Code",
    dataIndex: "userCode",
    key: "userCode",
    width: 140
  },
  {
    title: "User Name",
    dataIndex: "userName",
    key: "userName",
    width: 140
  },
  /*{
    title: "Operation Content",
    dataIndex: "operationContent",
    key: "operationContent",
    render: (cell: any) => <Tooltip title={cell} overlayClassName="UserLogSearchTabTooltip"><Text className="td-operationContent" ellipsis>{cell}</Text></Tooltip>,
  },*/
  {
    title: "Operation Type",
    dataIndex: "operationType",
    key: "operationType",
    width: 140
  },
  {
    title: "Operation Time",
    dataIndex: "operationTime",
    key: "operationTime",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME),
    width: 140
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
    width: 140
  },
];
