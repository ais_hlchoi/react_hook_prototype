import React, { FunctionComponent, useState, useRef } from "react";
import { Divider, Tabs } from "antd";

import "./UserLogComponent.scss";
import { UserLogSearchTabComponent } from "./tabs";
import { MessageAlertComponent, EmptyMessageAlertProps } from "components";
import { AuditModelComponent } from "components";
import { MessageType, TabEnum } from "enum";
import { PageComponentProps } from "types";
import { common } from "shared/common";

type UserLogComponentProps = PageComponentProps;

const UserLogComponent: FunctionComponent<UserLogComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const { shieldRef, title = 'User Log' } = props;

  const UserLogTabComponentRef: any = useRef();

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.DEFAULT:
        setActiveKey(TabEnum.DEFAULT_TAB);
        break;
      case TabEnum.AUDIT:
        if (common.isNotEmpty(selectedRow)) {
          setAuditModel({ ...auditModel, show: true });
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setAuditModel({ ...auditModel, model });
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const propsTabSummary = { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange, searchRef: UserLogTabComponentRef };

  return (
    <div className="UserLogWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
          <UserLogSearchTabComponent {...propsTabSummary} />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default UserLogComponent;

const { TabPane } = Tabs;
