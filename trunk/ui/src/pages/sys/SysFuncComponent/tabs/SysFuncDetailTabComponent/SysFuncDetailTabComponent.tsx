import React, { FunctionComponent, useState, useEffect } from "react";
import { Form, Divider, Row, Col, Input, InputNumber, Spin, Select, Button } from "antd";
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";

import "./SysFuncDetailTabComponent.scss";
import { SysFuncApi } from "api";
import { SysFuncRecord } from "models";
import { MessageType, ResponseCode, SysFuncType, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { CommonUtils, UseEffectUtils } from "common/utils";

type SysFuncDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  isChanged?: Function;
  setChanged?: Function;
};

const SysFuncDetailTabComponent: FunctionComponent<SysFuncDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const { componentName, title = '' } = props;

  const [form] = Form.useForm();
  const [formDefault, setFormDefault] = useState<any>();
  const [isNotEmptyDefault, setNotEmptyDefault] = useState<boolean>(false);

  const [isEnableFuncId, setEnableFuncId] = useState(false);
  const [isEnabeParentIdLevel2, setEnabeParentIdLevel2] = useState(false);

  const [menuAllList, setMenuAllList] = useState([{funcId: '', funcName: null}]);
  const [funcAllList, setFuncAllList] = useState([{funcId: '', funcName: null}]);
  
  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { funcId = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    const searchMenuAllList = () => {
      const componentAction = 'SysFuncApi.searchAllFuncForMenu';
      if (!isSkip(componentName, componentAction)) {
        setSkip(componentName, componentAction);
        const model = { type : 'MENU' };
        SysFuncApi.searchAllFunc(model).then((response) => {
          const { responseCode, responseDesc } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            setMenuAllList(response.object);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    };
    const searchFuncAllList = (value: any) => {
      const componentAction = 'SysFuncApi.searchAllFuncForItem';
      if (!isSkip(componentName, componentAction)) {
        setSkip(componentName, componentAction);
        const model = { parentId : value };
        SysFuncApi.searchAllFunc(model).then((response) => {
          const { responseCode, responseDesc } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            setFuncAllList(response.object);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    };

    searchMenuAllList();
    if ((funcId || editingKey) && action) {
      setEditingKey(funcId || editingKey);
      const componentAction = 'SysFuncApi.searchRecords';
      if (!isSkip(componentName, componentAction)) {
        setSkip(componentName, componentAction);
        const model = { funcId: funcId || editingKey };
        SysFuncApi.searchRecords(model).then((response) => {
          const { responseCode, responseDesc } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const { object: { list = [] } } = response;
            const formData = convertToSysFuncModel(list.length > 0 ? { ...list[0] } : props.selectedRow);
            form.setFieldsValue(formData);
            setFormDefault(formData);
            setNotEmptyDefault(true);
            const { type = SysFuncType.FUNC, parentIdLevel2 } = formData;
            switch (type) {
              case SysFuncType.FUNC:
                setEnableFuncId(true);
                break;
              case SysFuncType.SUBFUNC:
                setEnableFuncId(false);
                setEnabeParentIdLevel2(true);
                searchFuncAllList(parentIdLevel2);
                break;
              default:
                setEnableFuncId(false);
                setEnabeParentIdLevel2(false);
                break;
            }
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        }).finally(() => {
          setLoading(false);
        });
      }
    } else {
      setEditingKey(0);
      setFormDefault({});
      setNotEmptyDefault(false);
      setEnableFuncId(false);
      setEnabeParentIdLevel2(false);
    }
  }, [props, form, editingKey]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, action, setChanged } = props;

  const onFinish = () => {
    onSave(form.getFieldsValue());
  };

  const onSave = (value: any) => {
    setSkip(componentName);
    setLoading(true);
    const sysFuncModel = { ...value };
    if (!isCreateMode(editingKey) && editingKey !== sysFuncModel.funcId) {
      console.log(`Incorrect SysFuncId [ ${editingKey} != ${sysFuncModel.funcId} ]`);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    (isCreateMode(editingKey) ? SysFuncApi.insertRecords(sysFuncModel) : SysFuncApi.updateRecords(sysFuncModel)).then((response: any) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        setEditingKey(object);
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        // comment resetSkip since prefer to keep form values
//      resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onReset = () => {
    onCloseMessage();
    if (isNotEmptyDefault) {
      form.setFieldsValue(formDefault);
    } else {
      form.resetFields();
    }
    setChanged(false);
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  const onChangeOption = (value: any) => {
    switch (value) {
    case SysFuncType.FUNC:
      setEnableFuncId(true);
      break;
    case SysFuncType.SUBFUNC:
      setEnableFuncId(false);
      setEnabeParentIdLevel2(true);
      break;
    default:
      setEnableFuncId(false);
      setEnabeParentIdLevel2(false);
      break;
    }
  };

  const onChangeParentId2 = (value: any) => {
    form.setFieldsValue({ parentId : '' });
    const model = { parentId : value };
    SysFuncApi.searchAllFunc(model).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        setFuncAllList(response.object);
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    });
  };

  return (
    <div className="SysFuncDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      <Form form={form} name="form" onFinish={onFinish} {...formItemLayout} onValuesChange={() => setChanged(true)}>
        <Form.Item name="funcId" children={<Input hidden={true} />} />
        <Form.Item name="lastUpdatedTime" children={<Input hidden={true} />} />
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"funcCode"}
              label={"Function Code"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Function Code.' }]}
              getValueFromEvent={(event) => codeChange(event.target.value, 31)}
            >
              <Input placeholder="Function Code" autoComplete="off" maxLength={30} disabled={isEditMode(action)} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"funcName"}
              label={"Function Name"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Function Name.' }]}
            >
              <Input placeholder="Function Name" autoComplete="off" maxLength={50} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"type"}
              label={"Module Type"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Module Type.' }]}
            >
              <Select disabled={isEditMode(action)}
                placeholder="Module Type"
                allowClear
                onChange={onChangeOption}
              >
                {typeOption.map(item => (<Option key={item.value} value={item.value}>{item.label}</Option>))}
              </Select>
            </Form.Item>
          </Col>
          {isEnableFuncId &&
          <Col span={8}>
            <Form.Item
              name={"parentId"}
              label={"Level One"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Level One.' }]}
            >
              <Select
                placeholder="Level One"
                allowClear
              >
                {menuAllList.map((item) => <Option key={item.funcId} value={item.funcId}>{item.funcName}</Option>)}
              </Select>
            </Form.Item>
          </Col>
          }
        </Row>
        {isEnabeParentIdLevel2 &&
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"parentIdLevel2"}
              label={"Level One"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Level One.' }]}
            >
              <Select
                placeholder="Level One"
                allowClear
                onChange={onChangeParentId2}
              >
                {menuAllList.map((item) => <Option key={item.funcId} value={item.funcId}>{item.funcName}</Option>)}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"parentId"}
              label={"Level Two"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Level Two.' }]}
            >
              <Select
                placeholder="Level Two"
                allowClear
              >
                {funcAllList.map((item) => <Option key={item.funcId} value={item.funcId}>{item.funcName}</Option>)}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        }
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"dispSeq"}
              label={"Display Sequence"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Display Sequence.' }]}
            >
              <InputNumber placeholder="Display Sequence" precision={0} min={0} maxLength={10} style={{ width: "100%" }} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"funcUrl"}
              label={"Function URL"}
              labelAlign={"right"}
            >
              <Input placeholder="Function URL" autoComplete="off" maxLength={100}/>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24} style={{ marginTop: "14px" }}>
          <Col span={22}>
            <Button type="primary" onClick={onBack}  icon={<CloseCircleOutlined />} children="Back" />
          </Col>
          <Col span={2}>
            <Button type="primary" htmlType="submit" icon={<CheckCircleOutlined />} children="Save" />
            <Button type="primary" onClick={onReset} icon={<CloseCircleOutlined />} children="Reset" style={{ display: "none" }} />
          </Col>
        </Row>
      </Form>
      </Spin>
    </div>
  );
};

export default SysFuncDetailTabComponent;

class SysFuncDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
  static isEditMode = (action: any) => action === TabEnum.EDIT;
  static codeChange = (text: string, maxlength: number) => (CommonUtils.isDirty(text) && text.length < maxlength) ? text.replace(/^ +| +$/g, '').replace(/[^\w_\-*]/ig, '').toLocaleUpperCase() : null;
  static convertToSysFuncModel = (model: any) => {
    const merged = { ...SysFuncDetailTabUtils.EmptySysFuncModel(), ...model };
    const { funcId, parentId, parentIdLevel, parentIdLevel2, funcCode, funcName, type, dispSeq, funcUrl } = merged;
    return {
      funcId,
      parentId,
      parentIdLevel,
      parentIdLevel2,
      funcCode,
      funcName,
      type,
      dispSeq,
      funcUrl,
    };
  };

  static EmptySysFuncModel = (): SysFuncRecord => ({
    funcId: 0,
    parentId: 0,
    parentIdLevel2: 0,
    funcCode: '',
    funcName: '',
    type: '',
    dispSeq: 0,
    funcUrl: '',
  });
}

const { isCreateMode, isEditMode, codeChange, convertToSysFuncModel } = SysFuncDetailTabUtils;
const { isSkip, setSkip, resetSkip } = UseEffectUtils;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const typeOption = [
  {
    value: 'MENU',
    label: 'Level One',
  },
  {
    value: 'FUNC',
    label: 'Level Two',
  },
  {
    value: 'SUBFUNC',
    label: 'Level Three',
  },
];
