import React, { FunctionComponent, useState, useEffect } from "react";
import { Form, Row, Col, Divider, Input, InputNumber, Button, Spin, Select } from "antd";
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";

import "./SysParmDetailTabComponent.scss";
import { SysParmApi } from "api";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { CommonUtils, UseEffectUtils } from "common/utils";
import { SysParmRecord } from "../../../../../models";

type SysParmDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  isChanged?: Function;
  setChanged?: Function;
};

const SysParmDetailTabComponent: FunctionComponent<SysParmDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const { componentName, title = '' } = props;

  const [form] = Form.useForm();

  const [parmSegmentList, setParmSegmentList] = useState([{ segment: '', shortDesc: null }]);

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { id = 0 }, selectedRow, action, isChanged } = props;
    if (isChanged())
      return;
    if (id && action) {
      console.log(selectedRow)
      setEditingKey(id);
      if (!isSkip(componentName)) {
        setSkip(componentName);
        const model = {};
        SysParmApi.searchSysParmSegment(model).then((response) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            setParmSegmentList(object);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
        SysParmApi.searchRecords({page:1, pageSize:1, id}).then(response => {
          const { responseCode, responseDesc } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const {object: {list = []}} = response;
            const formData = SysParmDetailTabUtils.convertToSysParmModel(list.length > 0 ? { ...list[0] } : selectedRow);
            form.setFieldsValue(formData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        }).finally(() => setLoading(false));
      }
    } else {
      setEditingKey(0);
      if (!isSkip(componentName)) {
        setSkip(componentName);
        const model = {};
        SysParmApi.searchSysParmSegment(model).then((response) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            setParmSegmentList(object);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    }
  }, [props, form]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, setChanged } = props;

  const onFinish = () => {
    onSave({ ...form.getFieldsValue() });
  };

  const onSave = (value: any) => {
    onCloseMessage();
    setLoading(true)
    const saveRecords = { ...value };
    if (!isCreateMode(editingKey) && editingKey !== saveRecords.id) {
      console.log(`Incorrect SysParmId [ ${editingKey} != ${saveRecords.id} ]`);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    (isCreateMode(editingKey) ? SysParmApi.insertRecords(saveRecords) : SysParmApi.updateRecords(saveRecords)).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  return (
    <div className="SysParmDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      <Form form={form} name="form" onFinish={onFinish} {...formItemLayout} onValuesChange={() => setChanged(true)}>
        <Form.Item name="id" children={<Input hidden={true} />} />
        <Form.Item name="lastUpdatedTime" children={<Input hidden={true} />} />
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"segment"}
              label={"Segment"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Segment Name.' }]}
            >
              <Select
                placeholder="Segment Name"
                disabled={isEditMode(editingKey)}
                allowClear
              >
                {parmSegmentList.map(({segment}) => <Option key={segment} value={segment}>{segment}</Option>)}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"code"}
              label={"Code"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Code.' }]}
              getValueFromEvent={(event) => codeChange(event.target.value, 31)}
            >
              <Input placeholder="Code" autoComplete="off" maxLength={30} disabled={isEditMode(editingKey)} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"shortDesc"}
              label={"Description"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'DescriptionPlease input .' }]}
            >
              <Input placeholder="Description" maxLength={400} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"shortDescTc"}
              label={"DescriptionTC"}
              labelAlign={"right"}
            >
              <Input placeholder="DescriptionTC" maxLength={400} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"longDesc"}
              label={"Detail Description"}
              labelAlign={"right"}
            >
              <TextArea placeholder="Detail Description" rows={2} maxLength={400}/>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"longDescTc"}
              label={"Detail Description TC"}
              labelAlign={"right"}
            >
              <TextArea placeholder="Detail Description TC" rows={2} maxLength={400}/>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"parmValue"}
              label={"Param Value"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Param Value.' }]}
            >
             <TextArea placeholder="Param Value" rows={5} maxLength={400}/>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"dispSeq"}
              label={"Display Sequence"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Display Sequence.' }]}
            >
              <InputNumber placeholder="Display Sequence" precision={0} min={0} maxLength={10} style={{ width: "100%" }}/>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"activeInd"}
              label={"Active"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please select Active. ' }]}
            >
              <Select
                placeholder="Active"
                allowClear
              >
                {activeList.map((item) => <Option key={item.key} value={item.key}>{item.value}</Option>)}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24} style={{ marginTop: "14px" }}>
          <Col span={22}>
            <Button type="primary"  onClick={onBack}  icon={<CloseCircleOutlined />} children="Back" />
          </Col>
          <Col span={2}>
            <Button type="primary"  htmlType="submit" icon={<CheckCircleOutlined />} children="Save" />
          </Col>
        </Row>
      </Form>
      </Spin>
    </div>
  );
};

export default SysParmDetailTabComponent;

class SysParmDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
  static isEditMode = (action: any) => action === TabEnum.EDIT;
  static codeChange = (text: string, maxlength: number) => ( CommonUtils.isDirty(text) && text.length < maxlength) ? text.replace(/^ +| +$/g, '').replace(/[^\w_\-*]/ig, '').toLocaleUpperCase() : null;
  static convertToSysParmModel = (model: any) => {
    const merged = { ...SysParmDetailTabUtils.EmptySysParmModel(), ...model };
    const { activeInd, code, dispSeq, id, longDesc, longDescTc, parmValue, segment, shortDesc, shortDescTc } = merged;
    return {
      activeInd,
      code,
      dispSeq,
      id,
      longDesc,
      longDescTc,
      parmValue,
      segment,
      shortDesc,
      shortDescTc
    };
  }

  static EmptySysParmModel = (): SysParmRecord => ({
    activeInd: "",
    code: "",
    dispSeq: 0,
    id: "",
    longDesc: "",
    longDescTc: "",
    parmValue: "",
    segment: "",
    shortDesc: "",
    shortDescTc: "",
  });
}


const { isCreateMode, isEditMode, codeChange } = SysParmDetailTabUtils;
const { isSkip, setSkip, resetSkip } = UseEffectUtils;
const { TextArea } = Input;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const activeList = [
  { key: "Y", value: "Yes" },
  { key: "N", value: "No" },
];
