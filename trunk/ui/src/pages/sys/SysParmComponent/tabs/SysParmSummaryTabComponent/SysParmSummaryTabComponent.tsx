import React, { useState, useContext, useImperativeHandle } from "react";
import { Divider, Table, Spin, Modal } from "antd";
import SysParmApi from "../../../../../api/SysParmApi";
import SearchBarComponent from "../../../../../components/SearchBarComponent";
import ResizableTableComponent from "../../../../../components/ResizableTableComponent";
import AuthButtonComponent from "../../../../../components/AuthButtonComponent";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { common } from '../../../../../shared/common';
import { ServicesContext } from "../../../../../services/contexts/ServicesContext";
import "./SysParmSummaryTabComponent.scss";

const confirm = Modal.confirm;

const searchFieldList = [
  {
    label: "Segment",
    key: "segment",
  },
  {
    label: "Code",
    key: "code",
  },
];

const sysParmColumns: any = [
  {
    title: "Segment",
    dataIndex: "segment",
    sorter: (a, b) => { return common.sortMethod(a, b, 'segment') },
    width: 160,
  },
  {
    title: "Code",
    dataIndex: "code",
    sorter: (a, b) => { return common.sortMethod(a, b, 'code') },
    width: 160,
  },
  {
    title: "Short Desc",
    dataIndex: "shortDesc",
    sorter: (a, b) => { return common.sortMethod(a, b, 'shortDesc') },
    width: 160,
  },
  {
    title: "Long Desc",
    dataIndex: "longDesc",
    sorter: (a, b) => { return common.sortMethod(a, b, 'longDesc') },
    width: 320,
  },
  {
    title: "Parm Value",
    dataIndex: "parmValue",
    sorter: (a, b) => { return common.sortMethod(a, b, 'parmValue') },
    width: 160,
  },
  {
    title: "Disp Seq",
    dataIndex: "dispSeq",
    sorter: (a, b) => { return common.sortMethod(a, b, 'dispSeq') },
    width: 160,
  },
];

type SummaryProps = {
  errFrameShow: Function;
  onCloseMessage: Function;
  onAuthButtonClick: Function;
  onSelectChange: Function;
  searchRef;
};

const SysParmSummaryTabComponent = (SummaryProps) => {
  
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;
  const UserInfo = AuthenticationService.getUserModel();
  
  const [loading, setLoading] = useState(false);
  const [sysParmData, setSysParmData] = useState<Array<any>>([]);
  const [searchModel, setSearchModel] = useState({
    page : 1, pageSize: 10, total : 0
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);

  useImperativeHandle(SummaryProps.searchRef, () => ({
    search: () => {
      handleOnSearch(searchModel);
    },
   updateSuccess: (value: any) => {
       SummaryProps.errFrameShow(MessageType.SUCCESS, value);
   }
  }));



  // //initial the variable
  // useEffect(() => {
  //   let searchModel = {
  //     page: 1,
  //     pageSize: 10,
  //   };
  //   handleOnSearch(searchModel);
  // },[]);  

  const handleOnSearch = (searchModel: any) => {
    SummaryProps.onCloseMessage();
    setLoading(true);
    searchModel.page = 1;
    searchModel.pageSize = 10;
    SysParmApi.searchRecords(searchModel).then((result) => {
      if (ResponseCode.SUCCESS === result.responseCode) {
        setSysParmData(result.object.list);
        searchModel.total = result.object.total;
        setSearchModel(searchModel);
      } else {
        SummaryProps.errFrameShow(MessageType.ERROR, result.responseDesc);
      }
    }).finally(()=>{
      setLoading(false)
    });
  };

  const searchAll = (page) => {
    SummaryProps.onCloseMessage();
    setLoading(true);
    var action = TabEnum.EXPORT;
    SysParmApi.searchRecordsAll(searchModel).then((result) => {
      if (ResponseCode.SUCCESS === result.responseCode) {
        SummaryProps.onAuthButtonClick({ action, data: result.object, columns: sysParmColumns });
        //SummaryProps.onAuthButtonClick({page: page, sysParmData: result.object, sysParmColumns: sysParmColumns});

      }  else {
        SummaryProps.errFrameShow(MessageType.ERROR, result.responseDesc);
      }
      setLoading(false);
    });
  };

  const handleOnReset = () => {
    SummaryProps.onCloseMessage();
    setSysParmData([]);
    setSearchModel({ page : 1, pageSize: 10, total : 0 });
  };

  const onButtonClick = (action) => {
    //if EDIT button clicked. Record id also needed to be passed.
    if (action === TabEnum.ADD) {
      SummaryProps.onAuthButtonClick({ action, selectedRow: {} });
    }
    if (action === TabEnum.EDIT) {
      SummaryProps.onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.DELETE) {
      SummaryProps.onAuthButtonClick({ action, selectedRow: selectedRow[0] });
      if(common.isNotEmpty(selectedRow)){
        confirm({
          title: 'Are you sure to delete?',
          content: '',
          okText: 'Confirm',
          okType: 'danger',
          cancelText: 'Cancel',
          onOk() {
            let deleteModel: any = selectedRow[0];
            deleteModel.userId = UserInfo.userId;
            deleteModel.menuFuncId = UserInfo.funcId;
            SysParmApi.deleteRecords(deleteModel)
            .then((result: any) => {
              if (ResponseCode.SUCCESS === result.responseCode) {
                handleOnSearch(searchModel);
                SummaryProps.errFrameShow(MessageType.SUCCESS, "Delete success.");
              } else {
                SummaryProps.errFrameShow(MessageType.ERROR, result.responseDesc);
              }
            });
          },
          onCancel() {
            console.log('onCancel!');
          },
        });
      }else{
        SummaryProps.errFrameShow(MessageType.ERROR, "Please select a record.");
      }

    }
    if (action === TabEnum.AUDIT){
      SummaryProps.onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.EXPORT){
      searchAll(action);
    }
  }

  
  const onSelectChange = (selectedRowKeys:any, selectedRow:any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    SummaryProps.onSelectChange(selectedRowKeys, selectedRow);
  };
  
  /*const clearSelect = () => {
    setSelectedRowKeys([]);
    setSelectedRow([]);
  };*/
  
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  
  const pageChange = (current:any, pageSize:any) => {
    SummaryProps.onCloseMessage();
    searchModel.page = current;
    searchModel.pageSize = pageSize;
    setLoading(true)
    SysParmApi.searchRecords(searchModel).then((result) => {
      if (ResponseCode.SUCCESS === result.responseCode) {
        setSysParmData(result.object.list);
        searchModel.total = result.object.total;
        setSearchModel(searchModel);
      } else {
        SummaryProps.errFrameShow(MessageType.ERROR, result.responseDesc);
      }
      setLoading(false)
    });
  };
  const onUploadRecords = () => {};
  return (
    <div className="SearchTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>

        {}
        <SearchBarComponent 
          onSearch={handleOnSearch} 
          onReset={handleOnReset} 
          searchFieldList={searchFieldList} 
        />
        <Divider />
        {}
        <AuthButtonComponent 
          onAuthButtonClick={value=>onButtonClick(value)}
          isPromotionPage={false}
          promoExportDisabled={false}
          uploadPromo={onUploadRecords}
        />
        <ResizableTableComponent
          className={"sysTable" + (sysParmData.length>0 ? " tableTop" : "")}
          rowKey = 'id' // dataSource must return 'key' value
          rowSelection={rowSelection}
          bordered
          dataSource={sysParmData}
          columns={sysParmColumns}
          pagination={{  
            position: ['topRight'],
            showSizeChanger: true,              
            onChange: pageChange,               
            onShowSizeChange: pageChange,       
            total: searchModel.total,           
            current: searchModel.page,          
            pageSize: searchModel.pageSize,     
            defaultCurrent: 1,                  
          }}
        />
      </Spin>
    </div>
  );
};

export default SysParmSummaryTabComponent;
