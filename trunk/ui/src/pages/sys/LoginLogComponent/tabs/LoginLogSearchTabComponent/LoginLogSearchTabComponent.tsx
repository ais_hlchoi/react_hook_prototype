import React, { FunctionComponent, useState, useImperativeHandle } from "react";
import { DatePicker, Button, Table, Menu, Checkbox, Spin, Divider, Row, Col } from "antd";
import { ClockCircleOutlined } from "@ant-design/icons";

import "./LoginLogSearchTabComponent.scss";
import { OperationLogApi } from "api";
import { SearchBarComponent, ResizableTableComponent } from "components";
import { DateFormat, MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import moment from "moment";

type LoginLogSearchTabComponentProps = TabComponentProps & {
  searchRef: any;
};

const LoginLogSearchTabComponent: FunctionComponent<LoginLogSearchTabComponentProps> = (props: any) =>  {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(EmptyPageProps());
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);

  useImperativeHandle(props.searchRef, () => ({
    search: () => {
      onSearch(searchModel);
    },
    updateSuccess: (value: any) => {
      errFrameShow(MessageType.SUCCESS, value);
    }
  }));

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const model = { ...EmptyPageProps(), ...value };
    OperationLogApi.getLoginActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
    setSearchModel(EmptyPageProps());
  };

  const onButtonClick = (action: any) => {
    if (action === TabEnum.AUDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
  }

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = { ...searchModel, page, pageSize };
    setLoading(true);
    OperationLogApi.getLoginActivity(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'operationLogId';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  return (
    <div className="LoginLogSearchTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={searchFieldList}
      />
      <Divider />
      <Row gutter={24} className="buttonRelative">
        <Col span={12}>
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onButtonClick(TabEnum.AUDIT)}    children="Audit"    icon={<ClockCircleOutlined />} />
        </Col>  
      </Row>
      <Row gutter={24}>
        <Col span={12}>&nbsp;</Col>
      </Row>
      <ResizableTableComponent
        className={"sysTable" + (searchList.length ? " tableTop" : "")}
        bordered
        rowKey={rowKey}
        rowSelection={rowSelection}
        dataSource={searchList}
        columns={summaryColumns}
        pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default LoginLogSearchTabComponent;

class LoginLogSearchTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });

  static checkList: any[] = [];

  static onFilterCheckboxGroupChange = (checkedValues: any) => {
    LoginLogSearchTabUtils.checkList = [...checkedValues];
    console.log(checkedValues);
  };

  static filterMenu = () => {
    return (
      <Checkbox.Group onChange={LoginLogSearchTabUtils.onFilterCheckboxGroupChange}>
        <Menu selectable={false}>
        {LoginLogSearchTabUtils.checkList.map((searchField, index) => <Menu.Item key={index}><Checkbox defaultChecked = {true} value={searchField} children={searchField.label} /></Menu.Item>)}
        </Menu>
      </Checkbox.Group>
    );
  };
}

const { EmptyPageProps } = LoginLogSearchTabUtils;

const searchFieldList = [
  /*{
    label: "User Code",
    key: "userCode"
  },*/
  {
    label: "User Name",
    key: "userName",
  },
  {
    label: "Operation Type",
    key: "operationType",
    type: "selectOption",
    list: [
      { "id": 0, "label": "LOGIN"   , "value": "LOGIN" },
      { "id": 1, "label": "LOGOUT"  , "value": "LOGOUT" },
    ],
  },
  {
    label: "Start Datetime",
    key: "startDate",
    element: (
      <DatePicker showTime={true} format={DateFormat.DATETIME}></DatePicker>
    ),
  },
  {
    label: "End Datetime",
    key: "endDate",
    element: (
      <DatePicker showTime={true} format={DateFormat.DATETIME}></DatePicker>
    ),
  }
];

const summaryColumns = [
  /*{
    title: "User Code",
    dataIndex: "userCode",
    key: "userCode",
  },*/
  {
    title: "User Name",
    dataIndex: "userName",
    key: "userName",
  },
  {
    title: "Operation Type",
    dataIndex: "operationType",
    key: "operationType",
  },
  {
    title: "Login Time",
    dataIndex: "operationTime",
    key: "operationTime",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
  },
];
