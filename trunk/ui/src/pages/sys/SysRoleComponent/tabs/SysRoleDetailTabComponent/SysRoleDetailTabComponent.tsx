import React, {FunctionComponent, useEffect, useState} from "react";
import {Button, Col, Divider, Form, Input, InputNumber, Row, Select, Spin, Switch, Table} from "antd";
import {CheckCircleOutlined, CloseCircleOutlined} from "@ant-design/icons";

import "./SysRoleDetailTabComponent.scss";
import {SysRoleApi} from "api";
import {SysRoleRecord} from "models";
import {MessageType, ResponseCode, TabEnum} from "enum";
import {TabComponentProps} from "types";
import {CommonUtils, UseEffectUtils} from "common/utils";
import {common} from "shared/common";

type SysRoleDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  isChanged?: Function;
  setChanged?: Function;
};

const SysRoleDetailTabComponent: FunctionComponent<SysRoleDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const { componentName, title = '' } = props;

  const [form] = Form.useForm();

  const [funcList, setFuncList] = useState<any>([]);
  const [sysRoleRecord, setSysRoleRecord] = useState<SysRoleRecord>(new SysRoleRecord());

  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { roleId = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    const searchFuncList = () => {
      const componentAction = 'SysRoleApi.searchRoleFuncList';
      if (!isSkip(componentName, componentAction)) {
        setSkip(componentName, componentAction);
        if (roleId && action) {
          const model = { ...props.selectedRow };
          //setMakChkStatus(model.makChkStatus);
          SysRoleApi.searchRoleFuncList(model).then((response) => {
            const { responseCode, responseDesc } = response;
            if (ResponseCode.SUCCESS === responseCode) {
              setFuncList(response.object);
            } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
              shieldRef.current.open({ ...response });
            } else {
              errFrameShow(MessageType.ERROR, responseDesc);
            }
          });
        } else {
          SysRoleApi.searchAllFunc({}).then((response) => {
            const { responseCode, responseDesc } = response;
            if (ResponseCode.SUCCESS === responseCode) {
              setFuncList(response.object);
            } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
              shieldRef.current.open({ ...response });
            } else {
              errFrameShow(MessageType.ERROR, responseDesc);
            }
            setLoading(false)
          });
        }
      }
    }

    if ((roleId || editingKey) && action) {
      setEditingKey(roleId || editingKey);
      const componentAction = 'SysRoleApi.searchRecords';
      if (!isSkip(componentName, componentAction)) {
        setSkip(componentName, componentAction);
        const model = { roleId: roleId || editingKey };
        SysRoleApi.searchRecords(model).then((response) => {
          const { responseCode, responseDesc } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const { object: { list = [] } } = response;
            const formData = convertToSysRoleModel(list.length > 0 ? { ...list[0] } : props.selectedRow);
            form.setFieldsValue(formData);
            setSysRoleRecord(formData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        }).finally(() => {
          setLoading(false);
        });
      }
    } else {
      form.setFieldsValue({ ...EmptySysRoleModel() });
    }
    searchFuncList();
  }, [props, form, editingKey]);

  useEffect(() => {
    refresh && setTimeout(() => setRefresh(false));
  }, [refresh]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, action, setChanged } = props;

  const onFinish = () => {
    onSave(common.parseFormDataToDateFromString({ ...form.getFieldsValue(), funcList: getAllFuncList(funcList) }, getSysRoleModelDateField()));
  };

  const onSave = (value: any) => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const sysRoleModel = { ...value };
    if (!isCreateMode(editingKey) && editingKey !== sysRoleModel.roleId) {
      console.log(`Incorrect SysFuncId [ ${editingKey} != ${sysRoleModel.roleId} ]`);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    (isCreateMode(editingKey) ? SysRoleApi.insertRecords(sysRoleModel) : SysRoleApi.updateRecords(sysRoleModel)).then((response: any) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        setEditingKey(object);
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        // comment resetSkip since prefer to keep form values
//      resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Update fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  /*const onReverse = () => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const model = { roleId: editingKey };
    SysRoleApi.reverseRecords(model).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Reverse fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };*/

  const onAuthorize = () => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const model = common.parseFormDataToDateFromString({ ...form.getFieldsValue(), funcList: getAllFuncList(funcList) }, getSysRoleModelDateField());
    SysRoleApi.authorizeRecords(model).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Authorize fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const checkboxChange = (record: any, attr: string, value: boolean) => {
    Object.assign(record, resetCanAction(record, attr, value));
    if (record.parentId !== 0) { //children
      let newFuncList = Object.assign(funcList, []);
      const parentId = record.parentId;
      const children = funcList.filter(p => p.funcId === parentId)[0].children;
      const newChildren = [...children.map(c => c.funcId === record.funcId ? record : c)]
      const yLength = newChildren.filter(c => 'Y' === c.activeInd).length;
      const nLength = newChildren.filter(c => 'N' === c.activeInd).length;
      if (yLength === children.length) {
        newFuncList.forEach(p => {
          if (p.funcId === parentId) {
            p = Object.assign(p , resetCanAction(p,attr, true));
            p.children = p.children.map(c => c.funcId === record.funcId ? record : resetCanAction(c, attr, true));
          }
        })
      } else if (nLength === children.length) {
        newFuncList.forEach(p => {
          if (p.funcId === parentId) {
            p = Object.assign(p , resetCanAction(p,attr, false));
            p.children = p.children.map(c => c.funcId === record.funcId ? record : resetCanAction(c, attr, false));
          }
        })
      } else { // select one child, the parent should be toggled too
        newFuncList.forEach(p => {
          if (p.funcId === parentId) {
            p =  Object.assign(p , resetCanAction(p,attr, true));
          }
        })
      }
      setFuncList(newFuncList);
    } else { //parent
      record.children = Object.assign(record.children, []).map(c => resetCanAction(c, attr, 'Y' === record.activeInd));
    }
    setRefresh(true);
    setChanged(true);
  };

  const funcColumns = [
    {
      title: "Function Name",
      dataIndex: "funcName",
      key: "funcName",
      width: "20%",
    },
    {
      title: "Module Type",
      dataIndex: "type",
      key: "type",
      width: "10%",
      render: (text: string) => text === "MENU" ? "Level One" : (text === "FUNC" ? "Level Two" : "Level Three")
    },
    {
      title: "Active",
      dataIndex: "activeInd",
      key: "activeInd",
      width: "10%",
      render: (text: string, record: any) => {
        return (
          <Switch checked={text === "Y"} onChange={(e) => checkboxChange(record, "activeInd", e)} />
        );
      }
    },
/*
    {
      title: "Operation",
      dataIndex: "funcId",
      key: "funcId",
      width: "50%",
      render: (_text: string, record: any) => {
        return (
          common.isNotEmpty(record.children) || record.type === "MENU" ? "" :<>
            <Checkbox key="canSelect"   checked={record.canSelect   === "Y"} onChange={(e) => checkboxChange(record, "canSelect"  , e.target.checked)}>Search</Checkbox>
            <Checkbox key="canInsert"   checked={record.canInsert   === "Y"} onChange={(e) => checkboxChange(record, "canInsert"  , e.target.checked)}>Add</Checkbox>
            <Checkbox key="canUpdate"   checked={record.canUpdate   === "Y"} onChange={(e) => checkboxChange(record, "canUpdate"  , e.target.checked)}>Edit</Checkbox>
            <Checkbox key="canDelete"   checked={record.canDelete   === "Y"} onChange={(e) => checkboxChange(record, "canDelete"  , e.target.checked)}>Delete</Checkbox>
            <Checkbox key="canAudit"    checked={record.canAudit    === "Y"} onChange={(e) => checkboxChange(record, "canAudit"   , e.target.checked)}>Audit</Checkbox>
            <Checkbox key="canView"     checked={record.canView     === "Y"} onChange={(e) => checkboxChange(record, "canView"    , e.target.checked)}>View</Checkbox>
            <Checkbox key="canUpload"   checked={record.canUpload   === "Y"} onChange={(e) => checkboxChange(record, "canUpload"  , e.target.checked)}>Upload</Checkbox>
            <Checkbox key="canDownload" checked={record.canDownload === "Y"} onChange={(e) => checkboxChange(record, "canDownload", e.target.checked)}>Download</Checkbox>
          </>
        )
      }
    },
*/
  ];
  const isBtnAuth = sysRoleRecord.canAuthorize && isEditMode(action);
  const isBtnSave = !isEditMode(action) ? true : sysRoleRecord.canSave;
  //const isBtnReverse = checkBtnReverseEnabled(makChkStatus) && isEditMode(action);
  return (
    <div className="SysRoleDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      <Form form={form} name="form" onFinish={onFinish} {...formItemLayout} onValuesChange={(() => setChanged(true))}>
        <Form.Item name="roleId" children={<Input hidden={true} />} />
        <Form.Item name="lastUpdatedTime" children={<Input hidden={true} />} />
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"roleCode"}
              label={"Role Code"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Role CodePlease input .' }]}
              getValueFromEvent={(event) => codeChange(event.target.value, 31)}
            >
              <Input placeholder="Role Code" autoComplete="off" maxLength={30} disabled={isEditMode(editingKey)} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"roleName"}
              label={"Role Name"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Role NamePlease input .' }]}
            >
              <Input placeholder="Role Name" autoComplete="off" maxLength={30} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"activeInd"}
              label={"Active"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please select Active. ' }]}
            >
              <Select
                placeholder="Active"
                allowClear
              >
                {activeList.map((item) => <Option key={item.key} value={item.key}>{item.value}</Option>)}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name={"dispSeq"}
              label={"Display Sequence"}
              labelAlign={"right"}
              rules={[{ required: true, message: 'Please input Display Sequence.' }]}
            >
              <InputNumber placeholder="Display Sequence" precision={0} min={0} maxLength={10} style={{ width: "100%" }} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"remark"}
              label={"Remark"}
              labelAlign={"right"}
            >
              <TextArea placeholder="Remark" rows={2} maxLength={400} />
            </Form.Item>
          </Col>
        </Row>
        {isEditMode(action) && <>
        <br/>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"makChkStatus"}
              label={"Status"}
              labelAlign={"right"}
              children={<span>{sysRoleRecord.makChkStatus}</span>}
            />
              {/*<Input placeholder="Status" autoComplete="off" maxLength={30} readOnly={isEditMode(action)} />
            </Form.Item>*/}
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"lastInitId"}
              label={"Init Id"}
              labelAlign={"right"}
              children={<span>{sysRoleRecord.lastInitId}</span>}
            />
            {/*  <Input placeholder="Init Id" readOnly={true} />*/}
            {/*</Form.Item>*/}
          </Col>
          <Col span={8}>
            <Form.Item
              name={"lastInitDate"}
              label={"Init Date"}
              labelAlign={"right"}
              children={<span>{sysRoleRecord.lastInitDate}</span>}
            />
              {/*<Input placeholder="Init Date" readOnly={true} />
            </Form.Item>*/}
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item
              name={"lastPostId"}
              label={"Post Id"}
              labelAlign={"right"}
              children={<span>{sysRoleRecord.lastPostId}</span>}
            />
              {/*<Input placeholder="Post Id" readOnly={true} />
            </Form.Item>*/}
          </Col>
          <Col span={8}>
            <Form.Item
              name={"lastPostDate"}
              label={"Post Date"}
              labelAlign={"right"}
              children={<span>{sysRoleRecord.lastPostDate}</span>}
            />
              {/*<Input placeholder="Post Date" readOnly={true} />
            </Form.Item>*/}
          </Col>
        </Row>
        </>}
        <br/>
        <Row className="sysRoleBodyRow TabsTwo">
          {common.isEmpty(funcList) ? "" :
            <Table
              rowKey="funcId"
              defaultExpandAllRows={false}
              style={{ width: "90%" }}
              columns={funcColumns} 
              dataSource={funcList} 
              pagination={false}
          />}
        </Row>
        <Row gutter={24} style={{ marginTop: "14px" }}>
          <Col span={ isBtnAuth ? 21 : 22 }>
            <Button type="primary"  onClick={onBack}  icon={<CloseCircleOutlined />} children="Back" />
          </Col>
          <Col span={ isBtnAuth ? 3 : 2 } style={{padding:0}}>
            { isBtnAuth && <Button type="primary"  onClick={onAuthorize} children="Authorize" /> }
            {/*{ isBtnReverse && <Button type="primary"  onClick={onReverse} children="Reverse" /> }*/}
            { isBtnSave && <Button type="primary"  htmlType="submit" icon={<CheckCircleOutlined />} children="Save" />}
          </Col>
        </Row>
        </Form>
      </Spin>
    </div>
  );
};

export default SysRoleDetailTabComponent;

class SysRoleDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
  static isEditMode = (action: any) => action === TabEnum.EDIT;
  static codeChange = (text: string, maxlength: number) => ( CommonUtils.isDirty(text) && text.length < maxlength) ? text.replace(/^ +| +$/g, '').replace(/[^\w_\-*]/ig, '').toLocaleUpperCase() : null;
  static getAllFuncList = (funcList: any) => {
    const list: any = [];
    for (let item of funcList) {
      if (item.children != null) {
        for (let iterator of item.children) {
          list.push(iterator);
          if (common.isNotEmpty(iterator.children)) {
            for (let subFunc of iterator.children) {
              list.push(subFunc);
            }
          }
        }
      }
      list.push(item);
    }
    return list;
  };
  static checkBtnAuthorizeEnabled = (stat: string) => {
    return 'AUTH' !== stat;
  };
  static checkBtnReverseEnabled = (stat: string) => {
    return [ 'AMEND', 'DELETE' ].some(o => o === stat);
  };
  static resetCanAction = (record: any, attr: string, value: boolean) => {
    const arr = [
      'canSelect',
      'canInsert',
      'canUpdate',
      'canDelete',
      'canAudit',
      'canView',
      'canUpload',
      'canDownload',
    ];
    const val = value ? 'Y' : 'N';
    const model = { ...record, [attr]: val };
    if (attr === 'activeInd' && common.isEmpty(record.children)) {
      Object.assign(model, Object.fromEntries(arr.map((o: string) => [o, val])));
    }
    return model;
  };
/*
  static parseFormDataFromDateToString = (model: any) => {
    const { lastInitDate, lastPostDate } = model;
    return {
      ...model,
      lastInitDate: moment(lastInitDate).format(DateFormat.DATETIME),
      lastPostDate: moment(lastPostDate).format(DateFormat.DATETIME),
    };
  };
  static parseFormDataToDateFromString = (model: any) => {
    const { lastInitDate, lastPostDate } = model;
    return {
      ...model,
      lastInitDate: moment(lastInitDate, DateFormat.DATETIME).toDate(),
      lastPostDate: moment(lastPostDate, DateFormat.DATETIME).toDate(),
    };
  };
*/
  static getSysRoleModelDateField = () => ['lastInitDate', 'lastPostDate'];
  static convertToSysRoleModel = (model: any) => {
    const merged = { ...SysRoleDetailTabUtils.EmptySysRoleModel(), ...model };
    const { roleId, roleCode, roleName, remark, dispSeq, activeInd, makChkStatus, lastInitId, lastInitDate, lastPostId, lastPostDate, canSave, canAuthorize } = merged;
    return common.parseFormDataFromDateToString({
      roleId,
      roleCode,
      roleName,
      remark,
      dispSeq,
      activeInd,
      makChkStatus,
      lastInitId,
      lastInitDate,
      lastPostId,
      lastPostDate,
      canSave,
      canAuthorize,
    }, getSysRoleModelDateField());
  };

  static EmptySysRoleModel = (): SysRoleRecord => ({
    roleId: 0,
    roleCode: '',
    roleName: '',
    remark: '',
    dispSeq: 0,
    activeInd: '',
    makChkStatus: '',
    lastInitId: '',
    lastInitDate: '',
    lastPostId: '',
    lastPostDate: '',
    canSave: false,
    canAuthorize: false,
  });
}

const { isCreateMode, isEditMode, codeChange, resetCanAction, getAllFuncList, getSysRoleModelDateField, convertToSysRoleModel, EmptySysRoleModel } = SysRoleDetailTabUtils;
const { isSkip, setSkip, resetSkip } = UseEffectUtils;
const { TextArea } = Input;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const activeList = [
  { key: "Y", value: "Yes" },
  { key: "N", value: "No" },
];

