import React, { FunctionComponent, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Row, Input, Button, Form, Alert } from "antd";
import { UserOutlined, EyeTwoTone, EyeInvisibleOutlined, LockOutlined } from "@ant-design/icons";

import "common/css/common.scss";
import "./SignInComponent.scss";
import { ApiInterceptor, LoginApi } from "api";
import { RouteComponentProps } from "types";

type SignInComponentProps = RouteComponentProps;

const SignInComponent: FunctionComponent<SignInComponentProps> = (props: any) => {
  const [messageAlertProps, setMessageAlertProps] = useState<any>({
    isMessageVisible: false,
    messageAlertContent: "",
    messageAlertType: "error",
  });

  const [loginForm] = Form.useForm();
  const [loading, setLoadings] = useState<boolean>(false);
  const history = useHistory();

  const handleMessageAlertClose = () => {
    setMessageAlertProps({
      isMessageVisible: false,
      messageAlertContent: "",
      messageAlertType: "error",
    });
  };

  const handleInvalidLoginMessageAlert = (errMsg: any) => {
    setMessageAlertProps({
      isMessageVisible: true,
      messageAlertContent: errMsg,
      messageAlertType: "error",
    })
  }

  useEffect(() => {
    ApiInterceptor.setInterceptor();
  }, []);

  const onLogin = (value: any) => {
    setLoadings(true);
    handleMessageAlertClose();
    const details = {
       'username': value.userCode,
       'password': value.pwd
    };
    const formBody: string[] = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    var formBodyString = formBody.join("&");
    const requestOption = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBodyString,
    };
    LoginApi.login(requestOption).then((response) => {
      const { failed = false, errMsg, token, key } = response;
      if (!failed) {
        localStorage.setItem('token', token);
        localStorage.setItem('key', key);
        history.push("/selectRole");
      }
      else{
        handleInvalidLoginMessageAlert(errMsg);        
      }
    }).catch(error => {
      console.log(error);
      handleInvalidLoginMessageAlert("Invalid username or password.");
    });
    setLoadings(false);
  };

  return (
    <div id="container">
      <div className="loginFormRoot">
        <div className="loginBrand"></div>
        <div className="loginWrappedForm">
          <div className="loginWrappedFormLS">
            <div className="loginLogo"><img src="./images/dbsvs_dl1_4c_r.jpg" alt="DBS" /></div>
            <div className="loginWelcome">Welcome to<br />Vickers Trading Support Platform</div>
          </div>
          <div className="loginWrappedFormRS">
              <Row className="loginFormInputWidth">
              {messageAlertProps.isMessageVisible &&
              <Alert
                className="messageAlertComponent"
                message={"Error"}
                description={messageAlertProps.messageAlertContent}
                type={messageAlertProps.messageAlertType}
                closable
                afterClose={handleMessageAlertClose}
              />
              }
              <div className="loginPanel">Login</div>
              <Form onFinish={onLogin} form={loginForm} className="loginForm">
                <Form.Item
                  name="userCode"
                  className="loginLeftAlign"
                  rules={[{ required: true, message: "Please input User Code." }]}
                >
                  <Input
                    maxLength={50}
                    className={"loginInput"}
                    placeholder="User Code"
                    prefix={<UserOutlined />}
                  />
                </Form.Item>
                <Form.Item
                  name="pwd"
                  className="loginLeftAlign"
                  rules={[{ required: true, message: "Please input Password." }]}
                >
                  <Input.Password
                    className="loginInput"
                    placeholder="Password"
                    prefix={<LockOutlined />}
                    iconRender={(visible) => visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />}
                  />
                </Form.Item>
                <Button
                  className="loginFormButton"
                  danger={true}
                  htmlType="submit"
                  loading={loading}
                >
                  Login
                </Button>
              </Form>
            </Row>
          </div>
        </div>
        <div className="loginVersion">{props.pjson.version}</div>
      </div>
    </div>
  );
};

export default SignInComponent;
