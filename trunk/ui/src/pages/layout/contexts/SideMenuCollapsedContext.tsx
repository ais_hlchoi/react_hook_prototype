import React from "react";

const SideMenuCollapsedContext = React.createContext({
    collapsed: false,
    setCollapsed: (collapsed : boolean) => {}
});

export default SideMenuCollapsedContext;