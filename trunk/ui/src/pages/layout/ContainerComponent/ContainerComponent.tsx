import React, { FunctionComponent, useState, useEffect, useContext, useRef, useImperativeHandle } from "react";
import { Layout } from "antd";

import "./ContainerComponent.scss";
import SideMenuCollapsedContext from "pages/layout/contexts/SideMenuCollapsedContext";
import AppRoutes from "pages/layout/routes/AppRoutes";
import { MenuApi } from "api";
import { AppHeaderComponent, AppSideMenuComponent } from "components";
import { ShieldComponent, ShieldProps, ShieldUtils, EmptyShieldProps } from "components";
import { ResponseCode } from "enum";
import { RouteComponentProps } from "types";
import { ServicesContext } from "services/contexts/ServicesContext";

type ContainerComponentProps = RouteComponentProps;

const ContainerComponent: FunctionComponent<ContainerComponentProps> = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [menuList, setMenuList] = useState<any>([]);
  const [selectedRoleId, setSelectedRoleId] = useState();
  const [shieldProps, setShieldProps] = useState<ShieldProps>(EmptyShieldProps());
  const { AuthenticationService, MenuService } = useContext(ServicesContext);

  const AppSideMenuComponentRef: any = useRef();
  const ShieldComponentRef: any = useRef();

  useImperativeHandle(ShieldComponentRef, () => ({
    open: (value: any) => {
      setShieldProps({ ...EmptyShieldProps(), ...ShieldUtils.convertToShieldProps(value), show: true });
    }
  }));
  
  useEffect(() => {
    if (selectedRoleId != null)
      AuthenticationService.setRoleId(selectedRoleId);
    const { userCode, roleId } = AuthenticationService.getUserModel();
    const model = { userCode, roleId };
    MenuApi.getSideMenu(model).then((response: any) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        AuthenticationService.setMenuList(MenuService.groupMenu(object));
        AuthenticationService.setPathList(MenuService.getGroupPath());
        setMenuList(MenuService.getGroupMenu());
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        ShieldComponentRef.current.open({ ...response });
      } else {
        console.log(responseDesc);
      }
    });
  }, [selectedRoleId, AuthenticationService, MenuService]);

  const value = { collapsed, setCollapsed };

  return (
    <SideMenuCollapsedContext.Provider value={value}>
      <Layout className="layout">
        <AppSideMenuComponent menuList={menuList} menuRef={AppSideMenuComponentRef} />
        <Layout className="site-layout">
          <AppHeaderComponent shieldRef={ShieldComponentRef} onChangeRole={(value: any) => setSelectedRoleId(value)} />
          <Content className="site-layout-background">
            <AppRoutes menuRef={AppSideMenuComponentRef} shieldRef={ShieldComponentRef} />
          </Content>
        </Layout>
        <ShieldComponent {...shieldProps} />
      </Layout>
    </SideMenuCollapsedContext.Provider>
  );
};

export default ContainerComponent;

const { Content } = Layout;
