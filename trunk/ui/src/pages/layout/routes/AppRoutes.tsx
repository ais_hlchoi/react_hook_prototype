import React, { FunctionComponent, useContext } from "react";
import { Switch, Route } from "react-router-dom";

import { ServicesContext } from "services/contexts/ServicesContext";

import DataExportComponent from "pages/app/DataExportComponent";
import SavedSearchComponent from "pages/app/SavedSearchComponent";
import ReportAccessGroupComponent from "pages/app/ReportAccessGroupComponent";
import ReportBuilderComponent from "pages/app/ReportBuilderComponent";
import ReportCategoryComponent from "pages/app/ReportCategoryComponent";
import ReportDownloadComponent from "pages/app/ReportDownloadComponent";
import ReportTemplateComponent from "pages/app/ReportTemplateComponent";
import ReportSchedulerDownloadComponent from "pages/app/ReportSchedulerDownloadComponent";

import SysParmComponent from "pages/sys/SysParmComponent";
import SysRoleComponent from "pages/sys/SysRoleComponent";
import SysFuncComponent from "pages/sys/SysFuncComponent";
import LoginLogComponent from "pages/sys/LoginLogComponent";
import UserLogComponent from "pages/sys/UserLogComponent";
import UploadLogComponent from "pages/sys/UploadLogComponent";

const AppRoutes: FunctionComponent<any> = ({ menuRef, shieldRef }) => {
  const { MenuService } = useContext(ServicesContext);
  const paths = [...MenuService.getGroupPath()];
  return (
    <Switch>
      {paths.map(({ funcId, funcName, funcUrl }) => {
        const { render = () => null } = { ...routes.find((o: routeProps) => o.path === funcUrl) };
        return <Route key={funcId} path={`/app${funcUrl}`} children={render({ menuRef, shieldRef, title: funcName })} />
      })}
    </Switch>
  );
};

export default AppRoutes;

type routeProps = {
  path: string;
  render: Function;
};

const routes: routeProps[] = [
  { path: '/app/dataExport'        , render: (props: any) => <DataExportComponent        {...props} /> },
  { path: '/app/savedSearch'       , render: (props: any) => <SavedSearchComponent       {...props} /> },
  { path: '/app/savedSearchAdvance', render: (props: any) => <SavedSearchComponent       {...props} isReportAdvance={true} /> },
  { path: '/app/report'            , render: (props: any) => <ReportDownloadComponent    {...props} /> },
  { path: '/app/report/online'     , render: (props: any) => <ReportDownloadComponent    {...props} /> },
  { path: '/app/report/schedule'   , render: (props: any) => <ReportSchedulerDownloadComponent    {...props} /> },
  { path: '/app/reportAccessGroup' , render: (props: any) => <ReportAccessGroupComponent {...props} /> },
  { path: '/app/reportBuilder'     , render: (props: any) => <ReportBuilderComponent     {...props} /> },
  { path: '/app/reportCategory'    , render: (props: any) => <ReportCategoryComponent    {...props} /> },
  { path: '/app/reportTemplate'    , render: (props: any) => <ReportTemplateComponent    {...props} /> },
  { path: '/sys/sysRole'           , render: (props: any) => <SysRoleComponent           {...props} /> },
  { path: '/sys/sysFunc'           , render: (props: any) => <SysFuncComponent           {...props} /> },
  { path: '/sys/loginLog'          , render: (props: any) => <LoginLogComponent          {...props} /> },
  { path: '/sys/systemActivity'    , render: (props: any) => <UserLogComponent           {...props} /> },
  { path: '/sys/uploadActivity'    , render: (props: any) => <UploadLogComponent         {...props} /> },
  { path: '/sys/sysParm'           , render: (props: any) => <SysParmComponent           {...props} /> },
];
