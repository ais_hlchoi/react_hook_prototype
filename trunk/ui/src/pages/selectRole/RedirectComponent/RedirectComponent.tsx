/* eslint-disable */
import React, { FunctionComponent, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";

import { common } from "shared/common";
import { ServicesContext } from "services/contexts/ServicesContext";

const RedirectComponent: FunctionComponent<any> = () => {
  const history = useHistory();
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (common.isEmpty(token)) {
      localStorage.clear();
      history.push("/signin");
    } else {
      AuthenticationService.setUserModel(null);
      history.push("/selectRole");
    }
  }, [AuthenticationService, history]);

  return <></>;
};

export default RedirectComponent;