import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import "./SelectRoleComponent.scss";
import { ApiInterceptor, UserApi } from "api";
import { ShieldComponent, ShieldProps, ShieldUtils, EmptyShieldProps } from "components";
import { ResponseCode } from "enum";
import { RouteComponentProps } from "types";
import { ServicesContext } from "services/contexts/ServicesContext";

type SelectRoleComponentProps = RouteComponentProps;

const SelectRoleComponent: FunctionComponent<SelectRoleComponentProps> = () => {
  const [shieldProps, setShieldProps] = useState<ShieldProps>(EmptyShieldProps());
  const history = useHistory();
  const services = useContext(ServicesContext);
  const { AuthenticationService } = services;

  useEffect(() => {
    ApiInterceptor.setInterceptor();
    // store token, then get userInfo
    const propsHeader = {};
    UserApi.getUserInfo(propsHeader).then((response) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { userInfo, userInfo: { userId, roleList = [] } } = object;
        AuthenticationService.setUserModel(userInfo);
        localStorage.setItem('userId', userId);
        localStorage.setItem('roleId', roleList.join(', '));
        history.push("/app/app");
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        setShieldProps({ ...EmptyShieldProps(), ...ShieldUtils.convertToShieldProps({...response}), show: true });
      } else {
        console.log(responseDesc);
      }
    });
  }, [AuthenticationService, history]);

  return (
    <div className="SelectRoleWrapper">
      <ShieldComponent {...shieldProps} />
    </div>
  );
};

export default SelectRoleComponent;
