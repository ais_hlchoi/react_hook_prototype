export default interface ReportBuilderFilterProps {
  key: string;
  table: any;
  column: any;
  value: string;
  comparator: any;
  table_1: any;
  column_1: any;
  join: { value: string, children: string, dir: joinDir };
}

type joinDir = 'L'|'R'|'l'|'r'|'';
