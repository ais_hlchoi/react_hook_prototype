export default interface ReportBuilderButtonProps {
  label: string;
  href: string;
  action: any;
  callback: any;
  show: boolean;
  disabled: boolean;
}
