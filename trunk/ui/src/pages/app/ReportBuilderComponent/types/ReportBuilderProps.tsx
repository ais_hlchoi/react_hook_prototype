export default interface ReportBuilderProps {
  file?: { id: any, label: any };
  table: [];
  column: [];
  filter: [];
  sort: [];
  advance: [];
  sql?: string;
  parameter?: [];
  output?: any;
  layout?: any;
  name?: any;
  description?: any;
  generateOpt?: any;
  buildType?: any;
  id?: any;
  lastUpdatedTime?: any;
  // ui only
  done?: boolean;
  current?: number;
}
