import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, List, Select } from "antd";

import "./ReportBuilderColumnListComponent.scss";
import { ReportBuilderDataUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { ResponseCode } from "enum";

const ReportBuilderColumnListComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [columnList, setColumnList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column } = getReportProps();
  const { next } = props;

  useEffect(() => {
    if (isEmptyReportProps()) {
      props.home();
    } else {
      removeDirtyData(getReportProps().table, getReportProps().column);
      props.next(checkBtnNextEnabled(getReportProps().table, getReportProps().column));
      const { shieldRef } = props;
      const columnLoad = 'column';
      if (isLoaded(columnLoad)) {
        const columnData = getLoaded(columnLoad);
        setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
      } else if (isLoading(columnLoad)) {
        console.log(`${columnLoad}Data is loading...`);
      } else {
        setLoading(columnLoad);
        const reportFormModel = {};
        ReportBuilderApi.getColumnNameList(reportFormModel).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const columnData = [...object];
            setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
            console.log(`${columnLoad}Data is loaded!`);
            setLoaded(columnLoad, columnData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            console.log(responseDesc);
          }
        });
      }
    }
  }, [props]);

  const onChangeSelectColumn = (record: any, option: any) => {
    const { id, head } = record;
    setReportProps({ column: [...column.filter(({table}) => table !== id)].concat(option.map(({key, value}) => ({ table: id, head, value: key, children: value }))) });
    next(checkBtnNextEnabled(getReportProps().table, getReportProps().column));
    triggerUseState();
  };

  return (
    <div className="ReportBuilderColumnListWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Select your column list" className="title-columnList editing" children="" />
      </Descriptions>
      <List itemLayout="horizontal" dataSource={columnList} renderItem={(t: any) => (
        <List.Item>
          <List.Item.Meta title={t.label} description={
            <Select
              className="input-columnList"
              defaultValue={column.filter(({table}) => table === t.id).map(({children}) => children)}
              mode="multiple"
              allowClear
              placeholder="Please select"
              filterOption={(value: string, option: any) => option.children.toUpperCase().indexOf(value.toUpperCase()) > -1}
              onChange={(_: any, option: any) => onChangeSelectColumn(t, option)}
              children={t.columns.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />)}
            />
          } />
        </List.Item>
      )} />
    </div>
  );
};

export default ReportBuilderColumnListComponent;

class ReportBuilderColumnListUtils {
  static checkBtnNextEnabled = (table: any, column: any) => table.map((t: any) => t.value).every((o: any) => column.flatMap((c: any) => c.table).includes(o));
  static removeDirtyData = (table: any, column: any) => {
    // remove data which tables not selected
    const columnTemp = column.filter((o: any) => table.map((t: any) => t.value).includes(o.table));
    if (column.length !== columnTemp.length) {
      setReportProps({ column: columnTemp });
    }
  };
}

const { getReportProps, setReportProps, isEmptyReportProps } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded } = ReportBuilderDataUtils;
const { getSelectedDataSource, getSelectedTable } = ReportBuilderPropsUtils;
const { checkBtnNextEnabled, removeDirtyData } = ReportBuilderColumnListUtils;
