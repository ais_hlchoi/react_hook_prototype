import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Table, Input, InputNumber, Switch } from "antd";

import "./ReportBuilderColumnHeaderComponent.scss";
import { ReportBuilderUtils, ReportBuilderDataUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { ResponseCode } from "enum";

const ReportBuilderColumnHeaderComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [columnList, setColumnList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column, filter, sort, advance } = getReportProps();

  useEffect(() => {
    if (isEmptyReportProps()) {
      props.home();
    } else {
      removeDirtyData();
      props.next(checkBtnNextEnabled());
      const { shieldRef } = props;
      const columnLoad = 'column';
      if (isLoaded(columnLoad)) {
        const columnData = getLoaded(columnLoad);
        setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
      } else if (isLoading(columnLoad)) {
        console.log(`${columnLoad}Data is loading...`);
      } else {
        setLoading(columnLoad);
        const reportFormModel = {};
        ReportBuilderApi.getColumnNameList(reportFormModel).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const columnData = [...object];
            setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
            console.log(`${columnLoad}Data is loaded!`);
            setLoaded(columnLoad, columnData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            console.log(responseDesc);
          }
        });
      }
    }
  }, [props]);

  return (
    <div className="ReportBuilderColumnHeaderWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" className="title-columnList" children={getSelectedColumn(table, column)} />
        <Descriptions.Item label="Created Filter" className="title-filtering" children={getCreatedFilter(filter)} />
        <Descriptions.Item label="Order By" className="title-orderBy" children={getCreatedSort(sort)} />
        <Descriptions.Item label="Advanced" className="title-advance" children={getCreatedAdvance(advance)} />
        <Descriptions.Item label="Edit column header" className="title-columnHeader editing" children="" />
      </Descriptions>
      <EditableTable className="table-columnHeader" bordered={true} {...{ columnList, triggerUseState }} />
    </div>
  );
};

export default ReportBuilderColumnHeaderComponent;

class ReportBuilderColumnHeaderUtils {
  static checkBtnNextEnabled = () => true;
  static removeDirtyData = () => {};
}

const { getReportProps, setReportProps, isEmptyReportProps, } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded } = ReportBuilderDataUtils;
const { getSelectedDataSource, getSelectedTable, getSelectedColumn, getCreatedFilter, getCreatedSort, getCreatedAdvance } = ReportBuilderPropsUtils;
const { checkBtnNextEnabled, removeDirtyData } = ReportBuilderColumnHeaderUtils;

const EditableTable = ({
  columnList,
  triggerUseState,
  ...restProps
}) => {
  const [data, setData] = useState<any>([]);

  const [modified, setModified] = useState<Date>(new Date());
  const triggerTableUseState = () => setModified(new Date());

  useEffect(() => {
    const { column } = getReportProps();
    const dataGrouped = ReportBuilderUtils.isDirty(column) ? [...column.map((o: any, n: number) => {
      const [tableProps,] = columnList.filter(({id}) => id === o.table).map(({id, label}) => ({ id, label }));
      const [columnProps,] = columnList.filter(({id}) => id === o.table).flatMap(({columns}) => columns.filter(({value}) => value === o.children).map(({value, label}) => ({ value, label })));
      const tableLabel = ({ label: '', ...tableProps }).label;
      const columnLabel = ({ label: '', ...columnProps }).label;
      const { title, seq, hidden } = o;
      return { ...EmptyItemProps(), table: tableProps, tableLabel, column: columnProps, columnLabel, title, seq, hidden, key: n };
    })] : [];

    let [columnSeqMax,] = dataGrouped.map(({seq}) => seq || 0).sort((a, b) => b - a);
    dataGrouped.forEach((o: any) => ReportBuilderUtils.isEmpty(o.seq) ? ++columnSeqMax : o.seq);

    setData(dataGrouped);
  }, [columnList, modified]);

  const columns = [
    { title: 'Table Name'   , dataIndex: 'tableLabel' , inputType: 'text'    , width: '300px', editable: false },
    { title: 'Column Name'  , dataIndex: 'columnLabel', inputType: 'text'    , width: '300px', editable: false },
    { title: 'Display Title', dataIndex: 'title'      , inputType: 'text'    , width: '300px', editable: true  },
    { title: 'Display Seq'  , dataIndex: 'seq'        , inputType: 'number'  , width: '100px', editable: true  },
    { title: 'Hidden'       , dataIndex: 'hidden'     , inputType: 'checkbox', width: '100px', editable: true  },
  ];
  const mergedColumns = columns.map(col => col.editable
    ? ({ ...col, onCell: (record: ItemProps) => ({
          title: col.title,
          dataIndex: col.dataIndex,
          inputType: col.inputType,
          inputCssStyle: { width: col.width },
          record,
          editing: true,
          dataUtils: { data, triggerTableUseState, setReportProps },
        }),
      })
    : col
  );

  return (
    <>
      <Table {...restProps}
        components={{ body: { cell: EditableCell } }}
        dataSource={data}
        columns={mergedColumns}
        pagination={false}
        rowClassName="editable-row"
        size="small"
      />
    </>
  );
};

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  dataIndex,
  inputType,
  inputCssStyle,
  required,
  record,
  editing,
  dataUtils,
  index,
  children,
  ...restProps
}) => {
  if (ReportBuilderUtils.isEmpty(inputType)) {
    return <td {...restProps} children={children} />;
  }

  if (editing) {
    const editingItem = { ...record };
    const inputNode = inputType === 'text'
      ? <Input style={{ ...inputCssStyle }}
          onChange={(event: any) => onChangeInput(dataIndex, event)}
          value={editingItem[dataIndex]}
        />
      : inputType === 'number'
      ? <span className="input-mandatory" style={{ ...inputCssStyle }}>
        <InputNumber
          onChange={(event: any) => onChangeInputNumber(dataIndex, event)}
          value={editingItem[dataIndex]}
          min={1}
        />
        </span>
      : inputType === 'checkbox'
      ? <span className="input-mandatory" style={{ ...inputCssStyle }}>
        <Switch
          onChange={(event: any) => onChangeSwitch(dataIndex, event)}
          checked={editingItem[dataIndex] === 'Y'}
        />
        </span>
      : <Input style={{ ...inputCssStyle }} readOnly />
    ;

    const onChangeInput = (dataIndex: string, event: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = (event.target.value || '');//.trim();
      const { column = [] } = getReportProps();
      const [itemIndex,] = column.map((o: any, n: number) => o.table === record.table.id && o.children === record.column.value ? n : -1).filter(n => n >= 0);
      if (itemIndex >= 0) {
        const editedColumn = column.map((o: any, n: number) => n === itemIndex ? { ...o, [dataIndex]: text } : o);
        dataUtils.setReportProps({ column: editedColumn });
        dataUtils.triggerTableUseState();
      }
    };

    const onChangeInputNumber = (dataIndex: string, event: number) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = event;
      const { column = [] } = getReportProps();
      const [itemIndex,] = column.map((o: any, n: number) => o.table === record.table.id && o.children === record.column.value ? n : -1).filter(n => n >= 0);
      if (itemIndex >= 0) {
        const editedColumn = column.map((o: any, n: number) => n === itemIndex ? { ...o, [dataIndex]: text } : o);
        dataUtils.setReportProps({ column: editedColumn });
        dataUtils.triggerTableUseState();
      }
    };

    const onChangeSwitch = (dataIndex: string, event: boolean) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = event ? 'Y' : 'N';
      const { column = [] } = getReportProps();
      const [itemIndex,] = column.map((o: any, n: number) => o.table === record.table.id && o.children === record.column.value ? n : -1).filter(n => n >= 0);
      if (itemIndex >= 0) {
        const editedColumn = column.map((o: any, n: number) => n === itemIndex ? { ...o, [dataIndex]: text } : o);
        dataUtils.setReportProps({ column: editedColumn });
        dataUtils.triggerTableUseState();
      }
    };

    return <td {...restProps} children={inputNode} />;
  }

  const { data = [] } = { ...dataUtils };
  const itemIndex = ReportBuilderUtils.isEmpty(data) ? -2 : data.map(({key}, index: number) => ({ key, index })).filter(({key}) => key === record.key).concat({ key: record.key, index: -1 })[0].index;
  const item = itemIndex < 0 ? EmptyItemProps() : { ...EmptyItemProps(), ...data[itemIndex] };

  const labelNode = inputType === 'text' || inputType === 'number'
    ? <Input readOnly bordered={false} value={item[dataIndex]} />
    : <Input readOnly bordered={false} value={getSafe(item[dataIndex], inputType === 'checkbox' ? '' : 'children')} />
  ;

  return <td {...restProps} children={labelNode} />;
};

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  title: any;
  dataIndex: string;
  inputType: string;
  inputCssStyle: any;
  required: any;
  record: ItemProps;
  editing: boolean;
  dataUtils: any;
  index: number;
  children: React.ReactNode;
}

interface ItemProps {
  key: string;
  table: any;
  column: any;
  title?: string;
  seq?: number;
  hidden?: string;
}

const EmptyItemProps = (): ItemProps => ({
  key: '',
  table: {},
  column: {},
  hidden: 'N',
});

const getSafe = (props: any, attr: string): any => {
  if (ReportBuilderUtils.isEmpty(attr))
    return props;
  const map = {};
  map[attr] = '';
  return { ...map, ...props }[attr];
};