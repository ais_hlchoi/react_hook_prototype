import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Table, Input, Checkbox, Select, Button, Typography } from "antd";

import "./ReportBuilderOrderByComponent.scss";
import { ReportBuilderUtils, ReportBuilderDataUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { ResponseCode } from "enum";
import { sortData } from "common/data/ReportBuilderData";

const ReportBuilderOrderByComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [columnList, setColumnList] = useState<any>([]);
  const [sortList, setSortList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column, filter, sort, sort_temp, sort_edit } = getReportProps();

  useEffect(() => {
    if (isEmptyReportProps()) {
      props.home();
    } else {
      removeDirtyData();
      props.next(checkBtnNextEnabled());
      const { shieldRef } = props;
      const columnLoad = 'column';
      if (isLoaded(columnLoad)) {
        const columnData = getLoaded(columnLoad);
        setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
      } else if (isLoading(columnLoad)) {
        console.log(`${columnLoad}Data is loading...`);
      } else {
        setLoading(columnLoad);
        const reportFormModel = {};
        ReportBuilderApi.getColumnNameList(reportFormModel).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const columnData = [...object];
            setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
            console.log(`${columnLoad}Data is loaded!`);
            setLoaded(columnLoad, columnData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            console.log(responseDesc);
          }
        });
      }
      setSortList(sortData);
    }
  }, [props]);

  const br = '\n';
  const onChangeTextArea = (event: any) => {
    const text = (event.target.value || '');
    setReportProps({ sort: ReportBuilderUtils.isEmpty(text.trim()) ? [] : text.split(br).map((value: string, seq: number) => ({ value, seq })) });
    triggerUseState();
  };

  return (
    <div className="ReportBuilderOrderByWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" className="title-columnList" children={getSelectedColumn(table, column)} />
        <Descriptions.Item label="Created Filter" className="title-filtering" children={getCreatedFilter(filter)} />
        <Descriptions.Item label="Prioritize your column" className="title-orderBy editing" children="" />
      </Descriptions>
      <EditableTable className="table-orderBy" bordered={true} {...{ tableList: table, columnList, sortList, sort, sortTemp: sort_temp, sortEdit: sort_edit, triggerUseState }} />
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Sorting" className="title-orderBy editing" children="" />
      </Descriptions>
      <Input.TextArea onChange={onChangeTextArea} value={getCreatedSortText(sort)} rows={Math.max(sort.length, 1) + 1} />
    </div>
  );
};

export default ReportBuilderOrderByComponent;

class ReportBuilderOrderByUtils {
  static checkBtnNextEnabled = () => true;
  static removeDirtyData = () => {};
}

const { getReportProps, setReportProps, isEmptyReportProps, removeReportPropsAttr } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded } = ReportBuilderDataUtils;
const { getSelectedDataSource, getSelectedTable, getSelectedColumn, getCreatedFilter, getCreatedSortText } = ReportBuilderPropsUtils;
const { checkBtnNextEnabled, removeDirtyData } = ReportBuilderOrderByUtils;

const EditableTable = ({
  tableList,
  columnList,
  sortList,
  sort = [],
  sortTemp = [],
  sortEdit,
  triggerUseState,
  allowDataEmpty = false,
  ...restProps
}) => {
  const hasUnsavedEditingItem = ReportBuilderUtils.isDirty(({ table: '', ...sortEdit }).table);
  const dataGrouped = hasUnsavedEditingItem ? [...sortTemp, sortEdit] : [...sortTemp];
  const unsavedEditingItem = hasUnsavedEditingItem ? sortEdit : EmptyItemProps();
  const unsavedEditingKey = hasUnsavedEditingItem ? sortEdit.key : '';

  const [data, setData] = useState<any>(dataGrouped);
  const [editingItem, setEditingItem] = useState<any>(unsavedEditingItem);
  const [editingKey, setEditingKey] = useState<any>(unsavedEditingKey);
  const [isEnableBtnAdd, setEnableBtnAdd] = useState<any>(!hasUnsavedEditingItem);
  const [isAllowDataEmpty] = useState<any>(allowDataEmpty);

  const [, setModified] = useState<Date>(new Date());
  const triggerTableUseState = () => setModified(new Date());

  const isEditing = (record: ItemProps) => record.key === editingKey;
  const edit = (record: Partial<ItemProps> & { key: React.Key }) => {
    const editingItem = data.filter((o: any) => o.key === record.key).concat({ key: ReportBuilderUtils.random(16), unsaved: true })[0];
    setReportProps({ sort_edit: editingItem });
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const cancel = (key: React.Key) => {
    removeReportPropsAttr(['sort_edit']);
    if (data.filter((item: ItemProps) => key === item.key).some(({unsaved}) => unsaved)) {
      setData(data.filter((item: ItemProps) => key !== item.key));
    }
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const save = (key: React.Key) => {
    const dataTemp = data.map((item: ItemProps) => key !== item.key ? item : { ...item, ...editingItem, unsaved: false });
    setReportProps({ sort_temp: dataTemp });
    removeReportPropsAttr(['sort_edit']);
    setData(dataTemp);
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const add = () => {
    const editingItem = { key: ReportBuilderUtils.random(16), unsaved: true };
    setReportProps({ sort_edit: editingItem });
    setData([...data, editingItem]);
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const remove = (record: ItemProps) => {
    const dataTemp = data.filter((item: ItemProps) => record.key !== item.key);
    setReportProps({ sort_temp: dataTemp });
    setData(dataTemp);
  };
  const finish = () => {
    const br = '\n';
    const str = data.map((o: ItemProps) => `${getSafe(o.table, 'children')}.${getSafe(o.column, 'value')} ${getSafe(o.sort, 'children')}`).join(',' + br).trim();
    const old = sort.map(({value}) => value).join(br).trim();
    const and = old.length > 0 ? (',' + br) : '';
    setReportProps({ sort: (`${old}${and}${str}`).split(br).map((value: string, seq: number) => ({ value, seq })) });
    removeReportPropsAttr(['sort_temp']);
    setData([]);
    triggerUseState();
  };
  const combo = (key: React.Key) => {
    save(key);
    finish();
  };

  const columns = [
    { title: 'Table Name' , dataIndex: 'table'     , inputType: 'select'  , width: '300px', editable: true , required: true  },
    { title: 'Column Name', dataIndex: 'column'    , inputType: 'select'  , width: '300px', editable: true , required: true  },
    { title: 'Order'      , dataIndex: 'sort'      , inputType: 'select'  , width: '100px', editable: true , required: true  },
    { title: 'Action'     , dataIndex: 'action'    , inputType: 'self'    , width: '200px', editable: false,
      render: (_value: any, record: ItemProps) => (
        <span>
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => edit(record)}       children="Edit" />}
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => remove(record)}     children="Remove" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => combo(record.key)}  children="Add" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => cancel(record.key)} children="Clear" />}
        </span>
      )
    },
  ];
  const mergedColumns = columns.map(col => col.editable
    ? ({ ...col, onCell: (record: ItemProps) => ({
          title: col.title,
          dataIndex: col.dataIndex,
          inputType: col.inputType,
          inputCssStyle: { width: col.width },
          required: col.required,
          record,
          editing: isEditing(record),
          dataProps: { tableList, columnList, sortList },
          dataUtils: { data, editingItem, setEditingItem, triggerTableUseState, setReportProps },
        }),
      })
    : col
  );

  useEffect(() => {
    removeEditableTableDirtyData();
  });

  const removeEditableTableDirtyData = () => {
    // remove data which key is empty
    const dirtyData = data.map(({key}, n: number) => ({ n, key })).filter(({key}) => ReportBuilderUtils.isEmpty(key)).sort(ReportBuilderUtils.desc);
    if (dirtyData.length) {
      data.forEach(({n}) => data.splice(n, 1));
      setReportProps({ sort_temp: data });
      setData(data);
    }
    // prevent hidden action
    else if (data.length === 1 && ReportBuilderUtils.isEmpty(editingKey)) {
      const editingItem = data[0];
      setReportProps({ sort_edit: editingItem });
      setEditingItem(editingItem);
      setEditingKey(editingItem.key);
      setEnableBtnAdd(false);
    }
    // auto add 1 row if not allowDataEmpty
    else if (!isAllowDataEmpty && data.length < 1)
      add();
  };

  return (
    <>
      {isAllowDataEmpty && (isEnableBtnAdd
        ? <Button type="primary" children="Add" onClick={add} />
        : <Button type="primary" children="Add" disabled />
      )}
      <Table {...restProps}
        components={{ body: { cell: EditableCell } }}
        dataSource={data}
        columns={mergedColumns}
        pagination={false}
        rowClassName="editable-row"
        size="small"
      />
      {isAllowDataEmpty && (isEnableBtnAdd
        ? <Button type="primary" children="Add to order" onClick={finish} />
        : <Button type="primary" children="Add to order" disabled />
      )}
    </>
  );
};

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  dataIndex,
  inputType,
  inputCssStyle,
  required,
  record,
  editing,
  dataProps,
  dataUtils,
  index,
  children,
  ...restProps
}) => {
  if (ReportBuilderUtils.isEmpty(inputType)) {
    return <td {...restProps} children={children} />;
  }

  const { editingItem = EmptyItemProps() } = { ...dataUtils };
  const inputNode = inputType === 'text'
  ? <Input style={{ ...inputCssStyle }}
      onChange={(event: any) => onChangeInput(dataIndex, event)}
      value={editingItem[dataIndex]}
    />
  : inputType === 'checkbox'
  ? <span className="input-mandatory" style={{ ...inputCssStyle }}>
    <Checkbox
      onChange={(event: any) => onChangeCheckbox(dataIndex, event)}
      checked={editingItem[dataIndex] === 'Y'}
    />
    </span>
  : dataIndex === 'table'
  ? <Select style={{ ...inputCssStyle }}
      onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
      value={getSafe(editingItem[dataIndex], 'value')}
      children={dataProps.tableList.map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
    />
  : dataIndex === 'column'
  ? <Select style={{ ...inputCssStyle }}
      onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
      value={getSafe(editingItem[dataIndex], 'value')}
//    children={dataProps.columnList.filter(({table}) => table === getSafe(editingItem.table, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
      children={dataProps.columnList.filter(({id}) => id === getSafe(editingItem.table, 'value')).flatMap(({columns}) => columns.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />))}
      showSearch
    />
  : dataIndex === 'sort'
  ? <Select style={{ ...inputCssStyle }}
      onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
      value={getSafe(editingItem[dataIndex], 'value')}
      children={dataProps.sortList.map(({id, label}) => <Select.Option {...{ key: id, value: id, children: label }} />)}
    />
  : <Input readOnly />
  ;

  const onChangeSelect = (dataIndex: string, option: any) => {
    if (ReportBuilderUtils.isEmpty(dataIndex))
      return;
    editingItem[dataIndex] = option;
    switch (dataIndex) {
      case 'table':
        editingItem['column'] = {};
        break;
      case 'column':
        if (ReportBuilderUtils.isEmpty(getSafe(editingItem['sort'], 'value')))
          editingItem['sort'] = dataProps.sortList.map(({id, label}) => ({ value: id, children: label })).filter(({children}) => children === 'ASC').concat({})[0];
        break;
  }
    dataUtils.setReportProps({ sort_edit: editingItem });
    dataUtils.setEditingItem(editingItem);
    dataUtils.triggerTableUseState();
  };

  const onChangeInput = (dataIndex: string, event: any) => {
    if (ReportBuilderUtils.isEmpty(dataIndex))
      return;
    const text = (event.target.value || '');//.trim();
    editingItem[dataIndex] = text;

    dataUtils.setReportProps({ sort_edit: editingItem });
    dataUtils.setEditingItem(editingItem);
    dataUtils.triggerTableUseState();
  };

  const onChangeCheckbox = (dataIndex: string, event: any) => {
    if (ReportBuilderUtils.isEmpty(dataIndex))
      return;
    const text = (event.target.checked || false) ? 'Y' : 'N';
    editingItem[dataIndex] = text;

    dataUtils.setReportProps({ advance_edit: editingItem });
    dataUtils.setEditingItem(editingItem);
    dataUtils.triggerTableUseState();
  };

  return <td {...restProps} children={inputNode} />;
};

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  title: any;
  dataIndex: string;
  inputType: string;
  inputCssStyle: any;
  required: any;
  record: ItemProps;
  editing: boolean;
  dataProps: any;
  dataUtils: any;
  index: number;
  children: React.ReactNode;
}

interface ItemProps {
  key: string;
  table: any;
  column: any;
  sort: any;
}

const EmptyItemProps = (): ItemProps => ({
  key: '',
  table: {},
  column: {},
  sort: {},
});

const getSafe = (props: any, attr: string): any => {
  const map = {};
  map[attr] = '';
  return { ...map, ...props }[attr];
}
