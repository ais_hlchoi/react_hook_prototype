import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Input, Radio, Table, Transfer } from "antd";
import difference from "lodash/difference";
import CommonUtils from 'common/utils/CommonUtils';

import "./ReportBuilderDataSourceComponent.scss";
import { ReportBuilderDataUtils, ReportBuilderPropsUtils, ReportBuilderUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { GenerateOption, ResponseCode } from "enum";
import { generateTypeData } from "../../../../../common/data/ReportBuilderData";

const ReportBuilderDataSourceComponent: FunctionComponent<any> = (props: any) => {
  if (getReportProps().done) {
    resetReportProps();
    resetDataLoaded();
  }
  setReportProps({ current: props.current });
  const [tableList, setTableList] = useState<any>([]);
  const [generateOptionDefault, setGenerateOptionDefault] = useState<string>(GenerateOption.DEFAULT_OPTION);
  const [generateOptionList, setGenerateOptionList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { table, generateOpt, name, description } = getReportProps();
  const { next, isRefresh, setRefresh } = props;

  useEffect(() => {
    if (isRefresh) {
      setReportProps(name);
      setReportProps(description);
      setReportProps(generateOpt);
      setGenerateOptionDefault(GenerateOption.DEFAULT_OPTION);
      setReportProps(table);
      setRefresh(false)
    }
  }, [table, generateOpt, name, description, isRefresh, setRefresh])

  useEffect(() => {
    props.next(checkBtnNextEnabled(getReportProps().table, generateOpt || GenerateOption.DEFAULT_OPTION, name, description));
  }, [props, table, generateOpt, name, description]);

  useEffect(() => {
    const options: any = [...generateTypeData];
    setGenerateOptionList(options);
    if (!options.some((option: any) => option.value === GenerateOption.DEFAULT_OPTION)) {
      const [option = { value: '' },] = options;
      setGenerateOptionDefault(option.value);
    }
  }, [props]);

  useEffect(() => {
    const { shieldRef } = props;
    const tableLoad = 'table';
    if (isLoaded(tableLoad)) {
      const { table } = getReportProps();
      const tableData = getLoaded(tableLoad);
      const tableDatb = tableData.flatMap(({tables}) => tables).map(({id, label, description, file}, n: number) => ({ key: n, id, label, description, file, chosen: false }));
      const tableDatc = tableDatb.map((o: any) => ({ ...o, datasource: tableData.find(({id}) => id === o.file).label }));
      const tableDatd = table.map((d: any) => ({ ...d, key: ({ key: '', ...tableDatc.find((c: any) => c.id === d.value) }).key }));
      setReportProps({ table: tableDatd });
      setTableList([...tableDatc]);
    } else if (isLoading(tableLoad)) {
      console.log(`${tableLoad}Data is loading...`);
    } else {
      setLoading(tableLoad);
      const reportFormModel = {};
      ReportBuilderApi.getTableNameList(reportFormModel).then((response: any) => {
        const { responseCode, responseDesc, object } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const { table } = getReportProps();
          const tableData = [...object];
          const tableDatb = tableData.flatMap(({tables}) => tables).map(({id, label, description, file}, n: number) => ({ key: n, id, label, description, file, chosen: false }));
          const tableDatc = tableDatb.map((o: any) => ({ ...o, datasource: tableData.find(({id}) => id === o.file).label }));
          const tableDatd = table.map((d: any) => ({ ...d, key: ({ key: '', ...tableDatc.find((c: any) => c.id === d.value) }).key }));
          setReportProps({ table: tableDatd });
          setTableList([...tableDatc]);
          console.log(`${tableLoad}Data is loaded!`);
          setLoaded(tableLoad, tableData);
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          console.log(responseDesc);
        }
      });
    }
  }, [props]);

  const onChangeTransfer = (targetKeys: any, _direction: string, _moveKeys: any) => {
    setReportProps({ table: tableList.filter(({key}) => targetKeys.includes(key)).map(({key, id, label, file}) => ({ key, file, value: id, children: label })) });
    resetDataLoaded('column');
    next(checkBtnNextEnabled(getReportProps().table, generateOpt, name, description));
    triggerUseState();
  };

  const onChangeCompleteInput = (event: any, attr: string) => {
    const text = (event.target.value || '');
    setReportProps({ [attr]: text });
    //const mand = [ 'layout', 'name', 'generateOpt' ];
    //props.done(checkBtnDoneEnabled(Object.entries(getReportProps()).filter(([k]) => mand.includes(k)).map(([, v]) => v)));
    triggerUseState();
  };

  return (
    <div className="ReportBuilderDataSourceWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Report Name" className="title-reportName editing" children="" />
      </Descriptions>
      <Input value={name} className="input-reportName" placeholder="Name" maxLength={100} onChange={(event) => onChangeCompleteInput(event, 'name')} />
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Report Description" className="editing" children="" />
      </Descriptions>
      <Input value={description} className="input-reportDescription" placeholder="Description" maxLength={200} onChange={(event) => onChangeCompleteInput(event, 'description')} />
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Generate Type" className="editing" children="" />
      </Descriptions>
      <Radio.Group value={CommonUtils.ifEmpty(generateOpt, generateOptionDefault)} optionType="button" options={generateOptionList.map(({label, value}) => ({label, value}))} onChange={(event) => onChangeCompleteInput(event, 'generateOpt')} />
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Select your Table" className="editing" children="" />
      </Descriptions>
      <TableTransfer
        dataSource={tableList}
        showSearch
        filterOption={(value: string, option: any) => option.label.toUpperCase().indexOf(value.toUpperCase()) > -1}
        targetKeys={table.map(({key}, n: number) => key || n)}
        onChange={onChangeTransfer}
        leftColumns={leftTableColumns}
        rightColumns={rightTableColumns}
        locale={locale}
      />
    </div>
  );
};

export default ReportBuilderDataSourceComponent;

const isNullOrEmpty = (value: any) => {
  return CommonUtils.isNullOrUndefined(value) || CommonUtils.isEmpty(value);
}

class ReportBuilderDataSourceUtils {
  static checkBtnNextEnabled = (table: any, generateOpt: any, name: any, description: any) =>
    table.length > 0 && !isNullOrEmpty(generateOpt) && !isNullOrEmpty(name) && !isNullOrEmpty(description);
  static checkBtnDoneEnabled = (arr: any[]) => arr.every(o => ReportBuilderUtils.isDirty(o));
  static removeDirtyData = (tableData: any, table: any, fileType: any) => {
    // remove data which tables not listed
    const tableListed = tableData.filter(({id}) => id === fileType).flatMap(({tables}) => (tables||[]).map(({id, file}) => `${file}_${id}`));
    const tableTemp = table.filter(({file, value}) => tableListed.includes(`${file}_${value}`));
    if (table.length !== tableTemp.length) {
      setReportProps({ table: tableTemp });
    }
  };
}

const { getReportProps, setReportProps, resetReportProps } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded, resetDataLoaded } = ReportBuilderDataUtils;
const { checkBtnNextEnabled } = ReportBuilderDataSourceUtils;

const TableTransfer = ({ leftColumns, rightColumns, ...restProps }) => (
  <Transfer {...restProps} showSelectAll={false}>
    {({
      direction,
      filteredItems,
      onItemSelectAll,
      onItemSelect,
      selectedKeys: listSelectedKeys,
      disabled: listDisabled,
    }) => {
      const columns = direction === 'left' ? leftColumns : rightColumns;
      const rowSelection = {
        getCheckboxProps: (o: any) => ({ disabled: listDisabled || o.disabled }),
        onSelectAll(selected: boolean, selectedRows: any) {
          const treeSelectedKeys = selectedRows.filter((o: any) => !o.disabled).map((o: any) => o.key);
          const diffKeys = selected
            ? difference(treeSelectedKeys, listSelectedKeys)
            : difference(listSelectedKeys, treeSelectedKeys);
          onItemSelectAll(diffKeys, selected);
        },
        onSelect: (record: any, selected: boolean) => onItemSelect(record.key, selected),
        selectedRowKeys: listSelectedKeys,
      };
      return (
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={filteredItems}
          size="small"
          style={{ pointerEvents: listDisabled ? 'none' : undefined }}
          onRow={({ key, disabled: itemDisabled }) => ({
            onClick: () => {
              if (itemDisabled || listDisabled)
                return;
              const keyStr = `${key}`;
              onItemSelect(keyStr, !listSelectedKeys.includes(keyStr));
            },
          })}
        />
      );
    }}
  </Transfer>
);

const locale =  { searchPlaceholder: 'Search by table name' };

const leftTableColumns = [
  { dataIndex: 'datasource', title: 'Datasource' },
  { dataIndex: 'label', title: 'Table Name' },
  { dataIndex: 'description', title: 'Description' },
];

const rightTableColumns = [
  { dataIndex: 'label', title: 'Table Name' },
];
