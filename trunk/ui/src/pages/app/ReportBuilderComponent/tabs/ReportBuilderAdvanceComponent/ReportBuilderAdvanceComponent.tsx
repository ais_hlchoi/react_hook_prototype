import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Table, Input, Checkbox, Select, Button, Typography } from "antd";

import "./ReportBuilderAdvanceComponent.scss";
import { ReportBuilderUtils, ReportBuilderDataUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { ResponseCode } from "enum";
import { comparatorData } from "common/data/ReportBuilderData";

const ReportBuilderAdvanceComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [columnList, setColumnList] = useState<any>([]);
  const [comparatorList, setComparatorList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column, filter, sort, advance, advance_edit } = getReportProps();
  const advanceWithKey = advance.map((o: any) => ReportBuilderUtils.isDirty(o.key) ? o : ({ ...o, key: ReportBuilderUtils.random(16) }));

  useEffect(() => {
    if (isEmptyReportProps()) {
      props.home();
    } else {
      removeDirtyData(getReportProps().table, getReportProps().advance);
      props.next(checkBtnNextEnabled());
      const { shieldRef } = props;
      const columnLoad = 'column';
      if (isLoaded(columnLoad)) {
        const columnData = getLoaded(columnLoad);
        setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
      } else if (isLoading(columnLoad)) {
        console.log(`${columnLoad}Data is loading...`);
      } else {
        setLoading(columnLoad);
        const reportFormModel = {};
        ReportBuilderApi.getColumnNameList(reportFormModel).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const columnData = [...object];
            setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
            console.log(`${columnLoad}Data is loaded!`);
            setLoaded(columnLoad, columnData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            console.log(responseDesc);
          }
        });
      }
      setComparatorList(comparatorData);
    }
  }, [props]);

  return (
    <div className="ReportBuilderAdvanceWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" className="title-columnList" children={getSelectedColumn(table, column)} />
        <Descriptions.Item label="Created Filter" className="title-filtering" children={getCreatedFilter(filter)} />
        <Descriptions.Item label="Sorting" className="title-orderBy" children={getCreatedSort(sort)} />
        <Descriptions.Item label="Create online report criteria" className="title-advance editing" children="" />
      </Descriptions>
      <EditableTable className="table-advance" bordered={true} {...{ tableList: table, columnList, comparatorList, advance: advanceWithKey, advanceEdit: advance_edit, triggerUseState }} />
    </div>
  );
};

export default ReportBuilderAdvanceComponent;

class ReportBuilderAdvanceUtils {
  static checkBtnNextEnabled = () => true;
  static removeDirtyData = (table: any, advance: any) => {
    // remove data which tables not selected
    const advanceTemp = advance.filter((o: any) => table.map((t: any) => t.value).includes(o.table.value));
    if (advance.length !== advanceTemp.length) {
      setReportProps({ advance: advanceTemp });
    }
  };
}

const { getReportProps, setReportProps, isEmptyReportProps, removeReportPropsAttr } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded } = ReportBuilderDataUtils;
const { getSelectedDataSource, getSelectedTable, getSelectedColumn, getCreatedFilter, getCreatedSort } = ReportBuilderPropsUtils;
const { checkBtnNextEnabled, removeDirtyData } = ReportBuilderAdvanceUtils;

const EditableTable = ({
  tableList,
  columnList,
  comparatorList,
  advance,
  advanceEdit,
  triggerUseState,
  allowDataEmpty = false,
  ...restProps
}) => {
  const hasUnsavedEditingItem = ReportBuilderUtils.isDirty(({ table: '', ...advanceEdit }).table);
  const dataGrouped = hasUnsavedEditingItem ? [...advance, advanceEdit] : [...advance];
  const unsavedEditingItem = hasUnsavedEditingItem ? advanceEdit : EmptyItemProps();
  const unsavedEditingKey = hasUnsavedEditingItem ? advanceEdit.key : '';

  const [data, setData] = useState<any>(dataGrouped);
  const [editingItem, setEditingItem] = useState<any>(unsavedEditingItem);
  const [editingKey, setEditingKey] = useState<any>(unsavedEditingKey);
  const [isEnableBtnAdd, setEnableBtnAdd] = useState<any>(!hasUnsavedEditingItem);
  const [isAllowDataEmpty] = useState<any>(allowDataEmpty);

  const [, setModified] = useState<Date>(new Date());
  const triggerTableUseState = () => setModified(new Date());

  const isEditing = (record: ItemProps) => record.key === editingKey;
  const edit = (record: Partial<ItemProps> & { key: React.Key }) => {
    const editingItem = data.filter((o: any) => o.key === record.key).concat({ key: ReportBuilderUtils.random(16), unsaved: true })[0];
    setReportProps({ advance_edit: editingItem });
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const cancel = (key: React.Key) => {
    removeReportPropsAttr(['advance_edit']);
    if (data.filter((item: ItemProps) => key === item.key).some(({unsaved}) => unsaved)) {
      setData(data.filter((item: ItemProps) => key !== item.key));
    }
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const save = (key: React.Key) => {
    const dataTemp = data.map((item: ItemProps) => key !== item.key ? item : { ...item, ...editingItem, unsaved: false });
    dataTemp.forEach((item: any) => item.hasOwnProperty('unsaved') && !item.unsaved ? delete item['unsaved'] : void 0);
    setReportProps({ advance: dataTemp });
    removeReportPropsAttr(['advance_edit']);
    setData(dataTemp);
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const add = () => {
    const editingItem = { key: ReportBuilderUtils.random(16), unsaved: true };
    setReportProps({ advance_edit: editingItem });
    setData([...data, editingItem]);
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const remove = (record: ItemProps) => {
    const dataTemp = data.filter((item: ItemProps) => record.key !== item.key);
    setReportProps({ advance: dataTemp });
    setData(dataTemp);
  };
  const finish = () => {
    const dataTemp = data.map((item: ItemProps) => item.column.hasOwnProperty('key') ? ({ ...item, column: { value: item.column.key, children: item.column.value } }) : item);
    setReportProps({ advance: dataTemp });
//  removeReportPropsAttr(['advance_temp']);
  };
  const combo = (key: React.Key) => {
    save(key);
    finish();
  };

  const columns = [
    { title: 'Table Name' , dataIndex: 'table'     , inputType: 'select'  , width: '300px', editable: true , required: true  },
    { title: 'Column Name', dataIndex: 'column'    , inputType: 'select'  , width: '300px', editable: true , required: true  },
    { title: 'Oper'       , dataIndex: 'comparator', inputType: 'select'  , width: '100px', editable: true , required: true  },
    { title: 'Mandatory'  , dataIndex: 'mandatory' , inputType: 'checkbox', width: '100px', editable: true , required: false },
    { title: 'Action'     , dataIndex: 'action'    , inputType: 'self'    , width: '200px', editable: false,
      render: (_value: any, record: ItemProps) => (
        <span>
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => edit(record)}       children="Edit" />}
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => remove(record)}     children="Remove" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => combo(record.key)}  children="Save" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => cancel(record.key)} children="Cancel" />}
        </span>
      )
    },
  ];
  const mergedColumns = columns.map(col => col.editable
    ? ({ ...col, onCell: (record: ItemProps) => ({
          title: col.title,
          dataIndex: col.dataIndex,
          inputType: col.inputType,
          inputCssStyle: { width: col.width },
          required: col.required,
          record,
          editing: isEditing(record),
          dataProps: { tableList, columnList, comparatorList },
          dataUtils: { data, editingItem, setEditingItem, triggerTableUseState, setReportProps },
        }),
      })
    : col
  );

  useEffect(() => {
    removeEditableTableDirtyData();
  });

  const removeEditableTableDirtyData = () => {
    // remove data which key is empty
    const dirtyData = data.map(({key}, n: number) => ({ n, key })).filter(({key}) => ReportBuilderUtils.isEmpty(key)).sort(ReportBuilderUtils.desc);
    if (dirtyData.length) {
      console.log(dirtyData);
//    data.forEach(({n}) => data.splice(n, 1));
//    setReportProps({ advance: data });
//    setData(data);
    }
    // auto add 1 row if not allowDataEmpty
    else if (!isAllowDataEmpty && data.length < 1) {
      add();
    }
  };

  return (
    <>
      {isEnableBtnAdd
        ? <Button type="primary" children="Add" onClick={add} />
        : <Button type="primary" children="Add" disabled />
      }
      <Table {...restProps}
        components={{ body: { cell: EditableCell } }}
        dataSource={data}
        columns={mergedColumns}
        pagination={false}
        rowClassName="editable-row"
        size="small"
      />
    </>
  );
};

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  dataIndex,
  inputType,
  inputCssStyle,
  required,
  record,
  editing,
  dataProps,
  dataUtils,
  index,
  children,
  ...restProps
}) => {
  if (ReportBuilderUtils.isEmpty(inputType)) {
    return <td {...restProps} children={children} />;
  }

  if (editing) {
    const { editingItem = EmptyItemProps() } = { ...dataUtils };
    const inputNode = inputType === 'text'
      ? <Input style={{ ...inputCssStyle }}
          onChange={(event: any) => onChangeInput(dataIndex, event)}
          value={editingItem[dataIndex]}
        />
      : inputType === 'checkbox'
      ? <span className="input-mandatory" style={{ ...inputCssStyle }}>
        <Checkbox
          onChange={(event: any) => onChangeCheckbox(dataIndex, event)}
          checked={editingItem[dataIndex] === 'Y'}
        />
        </span>
      : dataIndex === 'table'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
          children={dataProps.tableList.map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
        />
      : dataIndex === 'column'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'children')}
//        children={dataProps.columnList.filter(({table}) => table === getSafe(editingItem.table, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
          children={dataProps.columnList.filter(({id}) => id === getSafe(editingItem.table, 'value')).flatMap(({columns}) => columns.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />))}
          showSearch
        />
      : dataIndex === 'comparator'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
          children={dataProps.comparatorList.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />)}
        />
      : <Input style={{ ...inputCssStyle }} readOnly />
    ;

    const onChangeSelect = (dataIndex: string, option: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      editingItem[dataIndex] = option;
      switch (dataIndex) {
        case 'table':
          editingItem['column'] = {};
          break;
        case 'column':
          const { key, value, children } = option;
          editingItem[dataIndex] = { value: Number(key), children: value, text: children };
          if (ReportBuilderUtils.isEmpty(getSafe(editingItem['comparator'], 'value')))
            editingItem['comparator'] = dataProps.comparatorList.map(({value, label}) => ({ value, children: label })).filter(({value}) => value === 'EQUALS').concat({})[0];
          break;
      }
      dataUtils.setReportProps({ advance_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    const onChangeInput = (dataIndex: string, event: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = (event.target.value || '');//.trim();
      editingItem[dataIndex] = text;

      dataUtils.setReportProps({ advance_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    const onChangeCheckbox = (dataIndex: string, event: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = (event.target.checked || false) ? 'Y' : 'N';
      editingItem[dataIndex] = text;

      dataUtils.setReportProps({ advance_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    return <td {...restProps} children={inputNode} />;
  }

  const { data = [] } = { ...dataUtils };
  const itemIndex = ReportBuilderUtils.isEmpty(data) ? -2 : data.map(({key}, index: number) => ({ key, index })).filter(({key}) => key === record.key).concat({ key: record.key, index: -1 })[0].index;
  const item = itemIndex < 0 ? EmptyItemProps() : { ...EmptyItemProps(), ...data[itemIndex] };

  let attr = 'children';
  if (dataIndex === 'column') {
    attr = 'text';
    item[dataIndex][attr] = ({ label: '', ...dataProps.columnList.filter(({id}) => id === getSafe(item['table'], 'value')).flatMap(({columns}) => columns).find((o: any) => o.value === getSafe(item['column'], 'children')) }).label;
  }

  const labelNode = inputType === 'text'
    ? <Input readOnly bordered={false} value={item[dataIndex]} />
    : <Input readOnly bordered={false} value={getSafe(item[dataIndex], inputType === 'checkbox' ? '' : attr)} />
  ;

  return <td {...restProps} children={labelNode} />;
};

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  title: any;
  dataIndex: string;
  inputType: string;
  inputCssStyle: any;
  required: any;
  record: ItemProps;
  editing: boolean;
  dataProps: any;
  dataUtils: any;
  index: number;
  children: React.ReactNode;
}

interface ItemProps {
  key: string;
  table: any;
  column: any;
  comparator: any;
  mandatory: any;
  value?: string;
  label?: string;
  placeholder?: string;
}

const EmptyItemProps = (): ItemProps => ({
  key: '',
  table: {},
  column: {},
  comparator: {},
  mandatory: 'N',
});

const getSafe = (props: any, attr: string): any => {
  if (ReportBuilderUtils.isEmpty(attr))
    return props;
  const map = {};
  map[attr] = '';
  return { ...map, ...props }[attr];
};
