import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Radio } from "antd";

import "./ReportBuilderCompleteComponent.scss";
import { ReportBuilderUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { LayoutOption } from "enum";
import { layoutOptionData } from "common/data/ReportBuilderData";

const ReportBuilderCompleteComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [layoutOptionDefault, setLayoutOptionDefault] = useState<string>(LayoutOption.DEFAULT_OPTION);
  const [layoutOptionList, setLayoutOptionList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column, filter, sort, advance, layout } = getReportProps();

  useEffect(() => {
    const options: any = [...layoutOptionData];
    setLayoutOptionList(options);
    if (!options.some((option: any) => option.value === LayoutOption.DEFAULT_OPTION)) {
      const [option = { value: '' },] = options;
      setLayoutOptionDefault(option.value);
    }
  }, [props]);


  useEffect(() => {
    if (isEmptyReportProps()) { // no return home if done. so comment this (isEmptyReportProps() || getReportProps().done)
      props.home();
    } else {
      removeDirtyData();
    }
  }, [props]);

  const onChangeCompleteInput = (event: any, attr: string) => {
    const text = (event.target.value || '');
    setReportProps({ [attr]: text });
    const mand = [ 'layout', 'name', 'generateOpt' ];
    props.done(checkBtnDoneEnabled(Object.entries(getReportProps()).filter(([k]) => mand.includes(k)).map(([, v]) => v)));
    triggerUseState();
  };

  return (
    <div className="ReportBuilderCompleteWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" className="title-columnList" children={getSelectedColumn(table, column)} />
        <Descriptions.Item label="Created Filter" className="title-filtering" children={getCreatedFilter(filter)} />
        <Descriptions.Item label="Sorting" className="title-orderBy" children={getCreatedSort(sort)} />
        <Descriptions.Item label="Advanced" className="title-advance" children={getCreatedAdvance(advance)} />
      </Descriptions>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Horizontal Layout / Vertical Layout" className="editing" children="" />
      </Descriptions>
      <Radio.Group value={ReportBuilderUtils.ifEmpty(layout, layoutOptionDefault)} optionType="button" options={layoutOptionList.map(({label, value}) => ({label, value}))} onChange={(event) => onChangeCompleteInput(event, 'layout')} />
    </div>
  );
};

export default ReportBuilderCompleteComponent;

class ReportBuilderCompleteUtils {
  static checkBtnDoneEnabled = (arr: any[]) => arr.every(o => ReportBuilderUtils.isDirty(o));
  static removeDirtyData = () => {
    // remove data which attr like %_edit
    const removeAttr = ['filter_edit','sort_edit','advance_edit'];
    if (Object.entries(getReportProps()).some(([key]) => removeAttr.includes(key))) {
      removeReportPropsAttr(removeAttr);
    }
  };
}

const { getReportProps, setReportProps, isEmptyReportProps, removeReportPropsAttr } = ReportBuilderPropsUtils;
const { getSelectedDataSource, getSelectedTable, getSelectedColumn, getCreatedFilter, getCreatedSort, getCreatedAdvance } = ReportBuilderPropsUtils;
const { checkBtnDoneEnabled, removeDirtyData } = ReportBuilderCompleteUtils;
