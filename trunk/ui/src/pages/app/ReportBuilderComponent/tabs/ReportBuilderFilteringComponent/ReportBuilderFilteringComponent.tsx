import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Table, Input, Checkbox, Select, Button, Typography } from "antd";

import "./ReportBuilderFilteringComponent.scss";
import { ReportBuilderFilterProps, ReportBuilderUtils, ReportBuilderDataUtils, ReportBuilderPropsUtils, ReportBuilderQueryUtils } from "pages/app/ReportBuilderComponent/tabs";
import { ReportBuilderApi } from "api";
import { ResponseCode } from "enum";
import { comparatorData, joinData } from "common/data/ReportBuilderData";

const ReportBuilderFilteringComponent: FunctionComponent<any> = (props: any) => {
  setReportProps({ current: props.current });
  const [columnList, setColumnList] = useState<any>([]);
  const [comparatorList, setComparatorList] = useState<any>([]);
  const [joinList, setJoinList] = useState<any>([]);

  const [, setModified] = useState<Date>(new Date());
  const triggerUseState = () => setModified(new Date());
  const { file, table, column, filter, filter_temp, filter_edit } = getReportProps();

  useEffect(() => {
    if (isEmptyReportProps()) {
      props.home();
    } else {
      removeDirtyData();
      props.next(checkBtnNextEnabled());
      const { shieldRef } = props;
      const columnLoad = 'column';
      if (isLoaded(columnLoad)) {
        const columnData = getLoaded(columnLoad);
        setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
      } else if (isLoading(columnLoad)) {
        console.log(`${columnLoad}Data is loading...`);
      } else {
        setLoading(columnLoad);
        const reportFormModel = {};
        ReportBuilderApi.getColumnNameList(reportFormModel).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const columnData = [...object];
            setColumnList(columnData.filter((o: any) => getReportProps().table.map((t: any) => t.value).includes(o.id)));
            console.log(`${columnLoad}Data is loaded!`);
            setLoaded(columnLoad, columnData);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            console.log(responseDesc);
          }
        });
      }
      setComparatorList(comparatorData);
      setJoinList(joinData);
    }
  }, [props]);

  const br = '\n';
  const onChangeTextArea = (event: any) => {
    const text = (event.target.value || '');
    setReportProps({ filter: ReportBuilderUtils.isEmpty(text.trim()) ? [] : text.split(br).map((value: string, seq: number) => ({ value, seq })) });
    triggerUseState();
  };

  return (
    <div className="ReportBuilderFilteringWrapper">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Data Source" className="title-fileType" children={getSelectedDataSource(file)} />
        <Descriptions.Item label="Selected Table Name" className="title-tableList" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" className="title-columnList" children={getSelectedColumn(table, column)} />
        <Descriptions.Item label="Create your filter" className="title-filtering editing" children="" />
      </Descriptions>
      <EditableTable className="table-filtering" bordered={true} {...{ tableList: table, columnList, comparatorList, joinList, filter, filterTemp: filter_temp, filterEdit: filter_edit, triggerUseState }} />
      <p>Where 1 = 1</p>
      <Input.TextArea onChange={onChangeTextArea} value={getCreatedFilterText(filter)} rows={Math.max(filter.length, 1) + 1} />
    </div>
  );
};

export default ReportBuilderFilteringComponent;

class ReportBuilderFilteringUtils {
  static checkBtnNextEnabled = () => true;
  static removeDirtyData = () => {};
}

const { getReportProps, setReportProps, isEmptyReportProps, removeReportPropsAttr } = ReportBuilderPropsUtils;
const { isLoaded, isLoading, setLoaded, setLoading, getLoaded } = ReportBuilderDataUtils;
const { getSelectedDataSource, getSelectedTable, getSelectedColumn, getCreatedFilterText } = ReportBuilderPropsUtils;
const { checkBtnNextEnabled, removeDirtyData } = ReportBuilderFilteringUtils;

const EditableTable = ({
  tableList,
  columnList,
  comparatorList,
  joinList,
  filter = [],
  filterTemp = [],
  filterEdit,
  triggerUseState,
  allowDataEmpty = false,
  ...restProps
}) => {
  const hasUnsavedEditingItem = ReportBuilderUtils.isDirty(({ table: '', ...filterEdit }).table);
  const dataGrouped = hasUnsavedEditingItem ? [...filterTemp, filterEdit] : [...filterTemp];
  const unsavedEditingItem = hasUnsavedEditingItem ? filterEdit : EmptyItemProps();
  const unsavedEditingKey = hasUnsavedEditingItem ? filterEdit.key : '';

  const [data, setData] = useState<any>(dataGrouped);
  const [editingItem, setEditingItem] = useState<any>(unsavedEditingItem);
  const [editingKey, setEditingKey] = useState<any>(unsavedEditingKey);
  const [isEnableBtnAdd, setEnableBtnAdd] = useState<any>(!hasUnsavedEditingItem);
  const [isAllowDataEmpty] = useState<any>(allowDataEmpty);

  const [, setModified] = useState<Date>(new Date());
  const triggerTableUseState = () => setModified(new Date());

  const isEditing = (record: ItemProps) => record.key === editingKey;
  const edit = (record: Partial<ItemProps> & { key: React.Key }) => {
    const editingItem = data.filter((o: any) => o.key === record.key).concat({ key: ReportBuilderUtils.random(16), unsaved: true })[0];
    setReportProps({ filter_edit: editingItem });
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const cancel = (key: React.Key) => {
    removeReportPropsAttr(['filter_edit']);
    if (data.filter((item: ItemProps) => key === item.key).some(({unsaved}) => unsaved)) {
      setData(data.filter((item: ItemProps) => key !== item.key));
    }
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const save = (key: React.Key) => {
    const dataTemp = data.map((item: ItemProps) => key !== item.key ? item : { ...item, ...editingItem, unsaved: false });
    dataTemp.forEach((item: any) => item.hasOwnProperty('unsaved') && !item.unsaved ? delete item['unsaved'] : void 0);
    setReportProps({ filter_temp: dataTemp });
    removeReportPropsAttr(['filter_edit']);
    setData(dataTemp);
    setEditingItem(EmptyItemProps());
    setEditingKey('');
    setEnableBtnAdd(true);
  };
  const add = () => {
    const editingItem = { key: ReportBuilderUtils.random(16), unsaved: true };
    setReportProps({ filter_edit: editingItem });
    setData([...data, editingItem]);
    setEditingItem(editingItem);
    setEditingKey(editingItem.key);
    setEnableBtnAdd(false);
  };
  const remove = (record: ItemProps) => {
    const dataTemp = data.filter((item: ItemProps) => record.key !== item.key);
    setReportProps({ filter_temp: dataTemp });
    setData(dataTemp);
  };
  const finish = () => {
    const br = '\n';
    const bgn = data.length > 1 ? ('(' + br) : '';
    const end = data.length > 1 ? (br + ')') : '';
    const str = data.map((o: ItemProps) => ReportBuilderQueryUtils.parse(o)).join(br + 'or' + br).trim();
    const old = filter.map(({value}) => value).join(br).trim();
    const and = old.length > 0 ? (br + 'and ') : 'and ';
    setReportProps({ filter: (`${old}${and}${bgn}${str}${end}`).split(br).map((value: string, seq: number) => ({ value, seq })) });
    removeReportPropsAttr(['filter_temp']);
    setData([]);
    triggerUseState();
  };

  const columns = [
    { title: 'Table Name' , dataIndex: 'table'     , inputType: 'select'  , width: '200px', editable: true , required: true  },
    { title: 'Column Name', dataIndex: 'column'    , inputType: 'select'  , width: '200px', editable: true , required: true  },
//  { title: ''           , dataIndex: 'join_l'    , inputType: 'select'  , width: '70px' , editable: true , required: false },
    { title: 'Oper'       , dataIndex: 'comparator', inputType: 'select'  , width: '80px' , editable: true , required: true  },
    { title: 'Table Name' , dataIndex: 'table_1'   , inputType: 'select'  , width: '200px', editable: true , required: true  },
    { title: 'Column Name', dataIndex: 'column_1'  , inputType: 'select'  , width: '200px', editable: true , required: true  },
    { title: 'Value'      , dataIndex: 'value'     , inputType: 'text'    , width: '150px', editable: true , required: true  },
//  { title: ''           , dataIndex: 'join_r'    , inputType: 'select'  , width: '70px' , editable: true , required: false },
    { title: 'Action'     , dataIndex: 'action'    , inputType: 'self'    , width: '200px', editable: false,
      render: (_value: any, record: ItemProps) => (
        <span>
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => edit(record)}       children="Edit" />}
          {!isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => remove(record)}     children="Remove" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => save(record.key)}   children="Save" />}
          { isEditing(record) && <Typography.Link className="btn-rowAction" onClick={() => cancel(record.key)} children="Cancel" />}
        </span>
      )
    },
  ];
  const mergedColumns = columns.map(col => col.editable
    ? ({ ...col, onCell: (record: ItemProps) => ({
          title: col.title,
          dataIndex: col.dataIndex,
          inputType: col.inputType,
          inputCssStyle: { width: col.width },
          required: col.required,
          record,
          editing: isEditing(record),
          dataProps: { tableList, columnList, comparatorList, joinList },
          dataUtils: { data, editingItem, setEditingItem, triggerTableUseState, setReportProps },
        }),
      })
    : col
  );

  useEffect(() => {
    removeEditableTableDirtyData();
  });

  const removeEditableTableDirtyData = () => {
    // remove data which key is empty
    const dirtyData = data.map(({key}, n: number) => ({ n, key })).filter(({key}) => ReportBuilderUtils.isEmpty(key)).sort(ReportBuilderUtils.desc);
    if (dirtyData.length) {
      data.forEach(({n}) => data.splice(n, 1));
      setReportProps({ filter_temp: data });
      setData(data);
    }
    // auto add 1 row if not allowDataEmpty
    else if (!isAllowDataEmpty && data.length < 1) {
      add();
    }
  };

  return (
    <>
      {isEnableBtnAdd
        ? <Button type="primary" children="Add" onClick={add} />
        : <Button type="primary" children="Add" disabled />
      }
      <Table {...restProps}
        components={{ body: { cell: EditableCell } }}
        dataSource={data}
        columns={mergedColumns}
        pagination={false}
        rowClassName="editable-row"
        size="small"
      />
      {isEnableBtnAdd
        ? <Button type="primary" children="Add to filter" onClick={finish} />
        : <Button type="primary" children="Add to filter" disabled />
      }
    </>
  );
};

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  dataIndex,
  inputType,
  inputCssStyle,
  required,
  record,
  editing,
  dataProps,
  dataUtils,
  index,
  children,
  ...restProps
}) => {
  if (ReportBuilderUtils.isEmpty(inputType)) {
    return <td {...restProps} children={children} />;
  }

  if (editing) {
    const { editingItem = EmptyItemProps() } = { ...dataUtils };
    const inputNode = inputType === 'text'
      ? <Input style={{ ...inputCssStyle }}
          onChange={(event: any) => onChangeInput(dataIndex, event)}
          value={editingItem[dataIndex]}
        />
      : inputType === 'checkbox'
      ? <span className="input-mandatory" style={{ ...inputCssStyle }}>
        <Checkbox
          onChange={(event: any) => onChangeCheckbox(dataIndex, event)}
          checked={editingItem[dataIndex] === 'Y'}
        />
        </span>
      : dataIndex === 'table'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
          children={dataProps.tableList.filter(({value}) => value !== getSafe(editingItem.table_1, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
        />
      : dataIndex === 'column'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
//        children={dataProps.columnList.filter(({table}) => table === getSafe(editingItem.table, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
          children={dataProps.columnList.filter(({id}) => id === getSafe(editingItem.table, 'value')).flatMap(({columns}) => columns.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />))}
          showSearch
        />
      : dataIndex === 'join_l'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getJoin(editingItem['join'], dataIndex.split('_')[1].toUpperCase())}
          children={dataProps.joinList.map(({id, label}) => <Select.Option {...{ key: id, value: id, children: label }} />)}
        />
      : dataIndex === 'table_1'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
          children={dataProps.tableList.filter(({value}) => value !== getSafe(editingItem.table, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
        />
      : dataIndex === 'column_1'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
//        children={dataProps.columnList.filter(({table}) => table === getSafe(editingItem.table_1, 'value')).map(({value, children}) => <Select.Option {...{ key: value, value, children }} />)}
          children={dataProps.columnList.filter(({id}) => id === getSafe(editingItem.table_1, 'value')).flatMap(({columns}) => columns.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />))}
          showSearch
        />
      : dataIndex === 'join_r'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getJoin(editingItem['join'], dataIndex.split('_')[1].toUpperCase())}
          children={dataProps.joinList.map(({id, label}) => <Select.Option {...{ key: id, value: id, children: label }} />)}
        />
      : dataIndex === 'comparator'
      ? <Select style={{ ...inputCssStyle }}
          onChange={(_value: string, option: any) => onChangeSelect(dataIndex, option)}
          value={getSafe(editingItem[dataIndex], 'value')}
          children={dataProps.comparatorList.map(({id, value, label}) => <Select.Option {...{ key: id, value, children: label }} />)}
        />
      : <Input style={{ ...inputCssStyle }} readOnly />
    ;

    const onChangeSelect = (dataIndex: string, option: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      if (dataIndex.indexOf('join_') === 0) {
        const [attr, dir] = dataIndex.split('_');
        editingItem[attr] = { dir, ...option };
      } else {
        editingItem[dataIndex] = option;
      }
      switch (dataIndex) {
        case 'table':
          editingItem['column'] = {};
          editingItem['comparator'] = {};
          break;
        case 'column':
          if (ReportBuilderUtils.isEmpty(getSafe(editingItem['comparator'], 'value')))
            editingItem['comparator'] = dataProps.comparatorList.map(({value, label}) => ({ value, children: label })).filter(({value}) => value === 'EQUALS').concat({})[0];
          break;
        case 'table_1':
          editingItem['column_1'] = {};
          editingItem['value'] = '';
          break;
      }
      dataUtils.setReportProps({ filter_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    const onChangeInput = (dataIndex: string, event: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = (event.target.value || '');//.trim();
      editingItem[dataIndex] = text;
      switch (dataIndex) {
        case 'value':
          if (text.length) {
            editingItem['table_1'] = {};
            editingItem['column_1'] = {};
          }
          break;
      }
      dataUtils.setReportProps({ filter_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    const onChangeCheckbox = (dataIndex: string, event: any) => {
      if (ReportBuilderUtils.isEmpty(dataIndex))
        return;
      const text = (event.target.checked || false) ? 'Y' : 'N';
      editingItem[dataIndex] = text;

      dataUtils.setReportProps({ advance_edit: editingItem });
      dataUtils.setEditingItem(editingItem);
      dataUtils.triggerTableUseState();
    };

    // !!! cannot use Form.Item to handle dependencies dropdowns !!!
    // so, no rule or validation can be implemented if no Form.Item is applied
    // <Form.Item name={dataIndex} children={inputNode} rules={[ { required: false, message: `${title} required!` } ]} />
    return <td {...restProps} children={inputNode} />;
  }

  const { data = [] } = { ...dataUtils };
  const itemIndex = ReportBuilderUtils.isEmpty(data) ? -2 : data.map(({key}, index: number) => ({ key, index })).filter(({key}) => key === record.key).concat({ key: record.key, index: -1 })[0].index;
  const item = itemIndex < 0 ? EmptyItemProps() : { ...EmptyItemProps(), ...data[itemIndex] };

  const labelNode = inputType === 'text'
    ? <Input readOnly bordered={false} value={item[dataIndex]} />
    : <Input readOnly bordered={false} value={getSafe(item[dataIndex], 'children')}/>
  ;

  return <td {...restProps} children={labelNode} />;
};

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  title: any;
  dataIndex: string;
  inputType: string;
  inputCssStyle: any;
  required: any;
  record: ItemProps;
  editing: boolean;
  dataProps: any;
  dataUtils: any;
  index: number;
  children: React.ReactNode;
}

interface ItemProps extends ReportBuilderFilterProps {
  key: string;
}

const EmptyItemProps = (): ItemProps => ({
  key: '',
  table: {},
  table_1: {},
  column: {},
  column_1: {},
  join: { value: '', children: '', dir: '' },
  value: '',
  comparator: {},
});

const getSafe = (props: any, attr: string): any => {
  const map = {};
  map[attr] = '';
  return { ...map, ...props }[attr];
};

const getJoin = (props: any, attr: string): any => {
  const { dir = '', value = '' } = { ...props };
  return attr.toUpperCase() === dir.toUpperCase() ? value : '';
};
