import React, { FunctionComponent, useState } from "react";
import { Button, Divider, Steps, Col, Row } from "antd";

import "./ReportBuilderComponent.scss";
import { ReportBuildType } from "enum";
import { PageComponentProps } from "types";
import {
  ReportBuilderDataSourceComponent,
  ReportBuilderColumnListComponent,
  ReportBuilderFilteringComponent,
  ReportBuilderOrderByComponent,
  ReportBuilderAdvanceComponent,
  ReportBuilderColumnHeaderComponent,
  ReportBuilderCompleteComponent,
  ReportBuilderUtils,
  ReportBuilderDataUtils,
  ReportBuilderPropsUtils,
} from "./tabs";

type ReportBuilderComponentProps = PageComponentProps & {
  propsBtnNext?: ReportBuilderBtnProps;
  propsBtnSave?: ReportBuilderBtnProps;
  propsBtnPrev?: ReportBuilderBtnProps;
  propsBtnBack?: ReportBuilderBtnProps;
  isBlankProps?: boolean;
  current?: number;
};

const ReportBuilderComponent: FunctionComponent<ReportBuilderComponentProps> = (props: any) => {
  const { isBlankProps = true } = props;
  const { id = 0 } = { ...getReportProps() };
  if (id && isBlankProps) {
    resetReportProps();
    resetDataLoaded();
  }

  const [current, setCurrent] = useState<number>(({ current: 1, ...getReportProps() }).current);
  const [isEnableBtnNext, setEnableBtnNext] = useState<boolean>(true);
  const [isEnableBtnSave, setEnableBtnSave] = useState<boolean>(true);
  const [isEnableBtnPrev, setEnableBtnPrev] = useState<boolean>(true);
  const [isRefresh, setRefresh] = useState<boolean>(false);
  const { shieldRef, title = '' } = props;

  const home = () => setCurrent(1);
  const next = (bool: boolean) => setEnableBtnNext(bool);
  const done = (bool: boolean) => setEnableBtnSave(bool && !getReportProps().done);
  const prev = (bool: boolean) => setEnableBtnPrev(bool);

  const resetPage = () => {
    resetReportProps();
    resetDataLoaded();
    home();
    setRefresh(true);
  }

  const stepPanelUtils = { home, next, done, prev, current };

  const { propsBtnNext = {}, propsBtnSave = {}, propsBtnPrev = {}, propsBtnReset = {}, propsBtnBack = {} } = props;
  Object.assign(propsBtnNext, { disabled: !isEnableBtnNext, show: current < steps.length  , action: () => setCurrent(Math.min(current + 1, steps.length)) });
  Object.assign(propsBtnPrev, { disabled: !isEnableBtnPrev, show: current > 1             , action: () => setCurrent(Math.max(current - 1, 1)) });
  Object.assign(propsBtnSave, { disabled: !isEnableBtnSave, show: current === steps.length, callback: () => { setReportProps({ done: true }, ['current_x']); resetPage(); } });
  Object.assign(propsBtnReset,{ show: isBlankProps, action: () => { resetPage(); } });

  const panelProps = { shieldRef, ...stepPanelUtils, isRefresh, setRefresh };

  return (
    <div className="ReportBuilderWrapper">
      {title && <span className="header">{title}</span>}
      <Steps current={Math.max(current - 1, 0)}>
        {steps.map(({ key, title }) => <Steps.Step {...{ key, title }} />)}
      </Steps>
      <Divider />
      <div className="steps-action">
        <Row>
          <Col span={23}>
            <ButtonPrev  {...{ ...props, propsBtnPrev  } } />
          </Col>
          <Col span={1}>
            <ButtonNext  {...{ ...props, propsBtnNext  } } />
          </Col>
        </Row>
      </div>
      <div className="steps-content">
        {current === 1 && <ReportBuilderDataSourceComponent   {...panelProps} />}
        {current === 2 && <ReportBuilderColumnListComponent   {...panelProps} />}
        {current === 3 && <ReportBuilderFilteringComponent    {...panelProps} />}
        {current === 4 && <ReportBuilderOrderByComponent      {...panelProps} />}
        {current === 5 && <ReportBuilderAdvanceComponent      {...panelProps} />}
        {current === 6 && <ReportBuilderColumnHeaderComponent {...panelProps} />}
        {current === 7 && <ReportBuilderCompleteComponent     {...panelProps} />}
      </div>
      <Divider />
      <div className="steps-action">
        <Row>
          <Col span={23}>
            <ButtonReset {...{ ...props, propsBtnReset } } />
            <ButtonBack  {...{ ...props, propsBtnBack  } } />
          </Col>
          <Col span={1}>
            <ButtonSave  {...{ ...props, propsBtnSave  } } />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default ReportBuilderComponent;

const { getReportProps, setReportProps, resetReportProps } = ReportBuilderPropsUtils;
const { resetDataLoaded } = ReportBuilderDataUtils;

type ReportBuilderBtnProps = {
  label?: string;
  href?: string;
  action?: Function;
  callback?: Function;
  show?: boolean;
  disabled?: boolean;
};

const ButtonNext: FunctionComponent<ReportBuilderBtnProps> = (props: any) => {
  const { propsBtnNext } = props;
  const { label = 'Next', href, action, callback, show = true, disabled = false } = propsBtnNext;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction disabled-btn" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (ReportBuilderUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonPrev: FunctionComponent<ReportBuilderBtnProps> = (props: any) => {
  const { propsBtnPrev = {} } = props;
  const { label = 'Previous', href, action, callback, show = true, disabled = false } = propsBtnPrev;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction disabled-btn" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (ReportBuilderUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonReset: FunctionComponent<ReportBuilderBtnProps> = (props: any) => {
  const { propsBtnReset = {} } = props;
  const { label = 'Reset', href, action, callback, show = true, disabled = false } = propsBtnReset;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction disabled-btn" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (ReportBuilderUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonSave: FunctionComponent<ReportBuilderBtnProps> = (props: any) => {
  const { propsBtnSave = {} } = props;
  const { label = 'Save', href, action, callback, show = true, disabled = false } = propsBtnSave;
  const buildType = ReportBuildType.REPORT_ADVANCE;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction disabled-btn" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (ReportBuilderUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback, reportPropsModel: { ...getReportProps(), buildType } }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonBack: FunctionComponent<ReportBuilderBtnProps> = (props: any) => {
  const { propsBtnBack = {} } = props;
  const { label = 'Back', href, action, callback, show = false, disabled = false } = propsBtnBack;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction disabled-btn" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (ReportBuilderUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const steps = [
  { 'key': 1, 'title': 'Data Source' },
  { 'key': 2, 'title': 'Column List' },
  { 'key': 3, 'title': 'Filtering' },
  { 'key': 4, 'title': 'Order By' },
  { 'key': 5, 'title': 'Advanced' },
  { 'key': 6, 'title': 'Column Header' },
  { 'key': 7, 'title': 'Complete' },
];
