import CommonUtils from "common/utils/CommonUtils";

export default class ReportBuilderDataUtils {

  private static uid: string = 'reportData';
  static getDataLoaded = () => Object.assign({}, JSON.parse(localStorage.getItem(ReportBuilderDataUtils.uid) || '{}'));
  static setDataLoaded = (props: any, arr?: string[]) => { if (arr) ReportBuilderDataUtils.removeDataLoadedAttr(arr); localStorage.setItem(ReportBuilderDataUtils.uid, JSON.stringify({ ...ReportBuilderDataUtils.getDataLoaded(), ...props })) };
  static resetDataLoaded = (type?: string) => { CommonUtils.isEmpty(type) ? localStorage.removeItem(ReportBuilderDataUtils.uid) : ReportBuilderDataUtils.removeDataLoadedAttr(Object.keys(ReportBuilderDataUtils.getDataLoaded()).filter(key => key === type || key.match(new RegExp('^' + type + '_')))) };
  static removeDataLoadedAttr = (arr: string[]) => { const props = ReportBuilderDataUtils.getDataLoaded(); arr.forEach((attr: string) => props.hasOwnProperty(attr) ? delete props[attr] : void 0); localStorage.setItem(ReportBuilderDataUtils.uid, JSON.stringify(props)) };

  static isLoaded = (type: string) => ({ [type]: 0, ...ReportBuilderDataUtils.getDataLoaded() })[type] && ReportBuilderDataUtils.getDataLoaded().hasOwnProperty(type + '_data');
  static getLoaded = (type: string) => ({ [type]: {}, ...ReportBuilderDataUtils.getDataLoaded() })[type + '_data'];
  static setLoaded = (type: string, data?: any) => { ReportBuilderDataUtils.setDataLoaded({ [type + '_data']: data }); ReportBuilderDataUtils.setDataLoaded({ [type]: 1 }, [type + '_loading']) };
  static isLoading = (type: string) => ({ [type + '_loading']: 0, ...ReportBuilderDataUtils.getDataLoaded() })[type + '_loading'];
  static setLoading = (type: string) => ReportBuilderDataUtils.setDataLoaded({ [type + '_loading']: 1 });
}
