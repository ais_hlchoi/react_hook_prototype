import React from "react";
import ReportBuilderUtils from "./ReportBuilderUtils";
import ReportBuilderProps from "../types/ReportBuilderProps";
import { ExportOption } from "enum";

export default class ReportBuilderPropsUtils {

  static sp = ', ';
  static br = '\n';

  static getSelectedDataSource(file: any = { label: '' }) {
    return <span className="label-fileType">{file.label || ''}</span>;
  }

  static getSelectedTable(table: any = []) {
    return table.map(({children}, n: number) => <span key={n} className="label-tableList">{children}</span>);
  }

  static getSelectedColumn(table: any = [], column: any = []) {
    return table.map(({value, children}, n: number) => <span key={n} className="label-columnList">{column.filter((c: any) => c.table === value).map((c: any, i: number) => <span key={n + '_' + i} className="label-columnItem">{children}.{c.children}</span>)}</span>);
  }

  static getCreatedFilter(filter: any = []) {
    return filter.map(({value}, n: number) => <span key={n} className="label-filtering">{value}</span>);
  }

  static getCreatedFilterText(filter: any = [], br: string|null = ReportBuilderPropsUtils.br) {
    return filter.map(({value}) => value).join(br);
  }

  static getCreatedSort(sort: any = []) {
    return sort.map(({value}, n: number) => <span key={n} className="label-orderBy">{value}</span>);
  }

  static getCreatedSortText(sort: any = [], br: string|null = ReportBuilderPropsUtils.br) {
    return sort.map(({value}) => value).join(br);
  }

  static getCreatedAdvance(advance: any = []) {
    return advance.map(({table, column, comparator, mandatory}, n: number) => <span key={n} className={ReportBuilderUtils.isTrue(mandatory) ? "label-advance-mandatory" : "label-advance"}>{table.children}.{column.children} {comparator.children}</span>);
  }

  // functions to get or set reportProps in localStorage

  private static uid: string = 'reportProps';
  static getReportProps = (): ReportBuilderProps & ReportBuilderExtProps => Object.assign({}, ReportBuilderPropsUtils.EmptyReportProps(), JSON.parse(localStorage.getItem(ReportBuilderPropsUtils.uid) || '{}'));
  static setReportProps = (props: any, arr?: string[]) => { if (arr) ReportBuilderPropsUtils.removeReportPropsAttr(arr); localStorage.setItem(ReportBuilderPropsUtils.uid, JSON.stringify({ ...ReportBuilderPropsUtils.getReportProps(), ...props })) };
  static isEmptyReportProps = (): boolean => ReportBuilderUtils.isNullOrUndefined(localStorage.getItem(ReportBuilderPropsUtils.uid)) || localStorage.getItem(ReportBuilderPropsUtils.uid) === '';
  static listReportProps = () => console.log(ReportBuilderPropsUtils.getReportProps());
  static resetReportProps = () => localStorage.setItem(ReportBuilderPropsUtils.uid, JSON.stringify(ReportBuilderPropsUtils.EmptyReportProps()));
  static removeReportPropsAttr = (arr: string[]) => { const props = ReportBuilderPropsUtils.getReportProps(); arr.forEach((attr: string) => props.hasOwnProperty(attr) ? delete props[attr] : void 0); localStorage.setItem(ReportBuilderPropsUtils.uid, JSON.stringify(props)) };

  static EmptyReportProps = (): ReportBuilderProps => ({
//  file: { id: 0 }, // TODO - should accept multiple file sources
    table: [],
    column: [],
    filter: [],
    sort: [],
    advance: [],
  });

  static EmptyReportExportProps = (): ReportBuilderProps => ({
    ...ReportBuilderPropsUtils.EmptyReportProps(),
    sql: '',
    parameter: [],
    output: ExportOption.DEFAULT_OPTION,
  });
}

interface ReportBuilderExtProps {
  filter_edit: {},
  filter_temp: [],
  sort_edit: {},
  sort_temp: [],
  advance_edit: {},
  advance_temp: [],
}
