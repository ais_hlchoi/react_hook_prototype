import ReportBuilderUtils from "./ReportBuilderUtils";
import ReportBuilderFilterProps from "../types/ReportBuilderFilterProps";

export default class ReportBuilderQueryUtils {

  static parse(o: ReportBuilderFilterProps): string {
    const { table, column, comparator, value = '', table_1, column_1, join = { dir: '', value: '', children: '' } } = o;

    const str: string[] = [];
    str.push(`${getSafe(table, 'children')}.${getSafe(column, 'children')}`);

    if (join.dir.toUpperCase() === 'L')
      str.push(join.children);

    if (ReportBuilderUtils.isDirty(getSafe(table_1, 'children'))) {
      str.push(`${getSafe(comparator, 'children')}`);
      str.push(`${getSafe(table_1, 'children')}.${getSafe(column_1, 'children')}`);
    } else {
      const useNullValue = ['EQUALS', 'NOT_EQUALS'];
      const comparatorValue = getSafe(comparator, 'value');
      if (useNullValue.includes(comparatorValue) && ReportBuilderUtils.isEmpty(value)) {
        str.push(comparatorValue === 'EQUALS' ? 'is null' : 'is not null');
      } else {
        str.push(`${getSafe(comparator, 'children')} '${value}'`);
      }
    }

    if (join.dir.toUpperCase() === 'R')
      str.push(join.children);

    return str.join(' ').trim();
  }
}

const getSafe = (props: any, attr: string): string => {
  const map = {};
  map[attr] = '';
  return { ...map, ...props }[attr];
};
