/* eslint-disable */
import React, { FunctionComponent, useState, useEffect } from "react";
import { Card, Drawer, Row, Col, Typography, Divider, Tooltip } from "antd";
import { CloudDownloadOutlined, ClockCircleOutlined, EllipsisOutlined, CloseOutlined } from '@ant-design/icons';
import { FilePdfTwoTone, FileExcelTwoTone, FileTextTwoTone, FileTwoTone } from '@ant-design/icons';

import "./ReportDownloadOnlineTabComponent.scss";
import { ReportDownloadApi } from "api";
import { DownloadStatus, MessageType } from "enum";
import { TabComponentProps } from "types";

type ReportDownloadOnlineTabComponentProps = TabComponentProps & {
  dataSource: any[];
};

const ReportDownloadOnlineTabComponent: FunctionComponent<ReportDownloadOnlineTabComponentProps> = (props: any) => {
  const [reportList, setReportList] = useState<any>([]);
  const [selectedRow, setSelectedRow] = useState<any>();
  const [drawerModel, setDrawerModel] = useState<any>(EmptyDrawerProps());

  useEffect(() => {
    const { dataSource = [] } = props;
    setReportList(dataSource);
  }, [props]);

  const onCloseDrawer = () => {
    setDrawerModel({ ...drawerModel, visible: false });
  };

  const onOpenDrawer = (value: any) => {
    const { onCloseMessage } = props;
    onCloseMessage();
    setSelectedRow(value);
    setDrawerModel({ ...drawerModel, visible: true });
  };

  const onExport = (value: any) => {
    const { errFrameShow, onCloseMessage } = props;
    onCloseMessage();
    const { id, rptName, fileExtension } = value;
    const reportDownloadModel = { fileExtension, id };
    // TODO - plan to handle ResponseCode.SHIELD since api returns blob (not json)
    ReportDownloadApi.getFile(reportDownloadModel).then((blob: any) => {
      if (blob.size > 10) {
        var a = document.createElement('a');
        document.body.appendChild(a);
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = `${rptName.replace(/\\s+/g, '_')}.${fileExtension}`;
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
        errFrameShow(MessageType.SUCCESS, "Export success.");
      } else {
        errFrameShow(MessageType.ERROR, "Export fail.");
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Export fail.");
    });
  };

  const propsCard = {
    className: "card-report",
  };

  const propsDrawer = { ...drawerModel, onClose: onCloseDrawer, children: <ReportDownloadDrawerComponent {...convertToReportDownloadCardComponentProps(selectedRow)} /> };

  return (
    <div className="ReportDownloadOnlineTabWrapper">
      <Drawer {...propsDrawer} />
      <Row className="row-reportHighline">
        <Title level={3}>Latest {reportList.length} report{reportList.length > 1 ? 's' : ''}</Title>
      </Row>
      <Row className="row-reportContainer">
        {reportList.map((o: any) => {
          const itemProps = convertToReportDownloadCardComponentProps(o);
          const actions: any = [];
          actions.push(itemProps.ready ? <CloudDownloadOutlined onClick={() => onExport(itemProps)} /> : 'F' === itemProps.status ? <CloseOutlined /> : <ClockCircleOutlined />);
          actions.push(<EllipsisOutlined onClick={() => onOpenDrawer(itemProps)} />);
          return <Card {...{ ...propsCard, actions, key: o.id, children: <ReportDownloadCardComponent {...itemProps} /> }} style={{maxHeight : "50vh"}}/>
        })}
      </Row>
    </div>
  );
};

export default ReportDownloadOnlineTabComponent;

class ReportDownloadOnlineTabUtils {
  static EmptyDrawerProps = () => ({
    title: '',
    width: '60%',
    placement: 'right',
    closable: false,
    onClose: () => void 0,
    visible: false,
  });
  static convertToFileSize = (n: number) => {
    const list: any = (['K', 'M', 'G', 'T']).map((o: string, n: number) => [ n + 1, `${o}b` ]);
    const [a, b] = list.find(([i]) => n < Math.pow(1024, i));
    const [c, d] = list[list.length - 1];
    return a ? `${n / Math.pow(1024, a - 1)} ${b}` : `${n / Math.pow(1024, c - 1)} ${d}`;
  };
  static convertToReportDownloadCardComponentProps = (props: any): ReportDownloadCardComponentProps => {
    return { ...props };
  };
  static convertToReportDownloadTestComponentProps = (props: any): ReportDownloadTestComponentProps => {
    const { id, rptName, rptDate, fileName, filePath, fileExtension, fileLineCount, fileLineProcess, fileSize, printCode, printCount, lastPrintedDate, status, statusLabel, ready, noted } = props;
    return {
      id,
      report: { name: rptName, date: rptDate },
      file  : { name: fileName, path: filePath, ext: fileExtension, total: fileLineCount, process: fileLineProcess, size: fileSize },
      print : { code: printCode, count: printCount, date: lastPrintedDate },
      status: { code: status, label: statusLabel },
      ready,
      noted,
    };
  };
}

const { EmptyDrawerProps, convertToFileSize, convertToReportDownloadCardComponentProps } = ReportDownloadOnlineTabUtils;
const { Title } = Typography;

type ReportDownloadCardComponentProps = {
  id: number;
  rptName: string;
  rptDate: string;
  fileExtension: string;
  ready: boolean;
  noted: boolean;
  status: string;
}

type ReportDownloadTestComponentProps = {
  id: number;
  report: { name: string, date: string };
  file  : { name: string, path: string, ext: string, total: number, process: number, size: number };
  print : { code: string, count: number, date: string };
  status: { code: DownloadStatus, label: string };
  ready: boolean;
  noted: boolean;
}

const ReportDownloadCardComponent: FunctionComponent<ReportDownloadCardComponentProps> = (props: any) => {
  const { id, rptName, rptDate, fileExtension } = props;
  const propsIcon = { className: "icon-state" };
  const shortRptName = rptName && rptName.trim().length > 10 ? rptName.trim().substr(0, 10) + '...' : rptName;
  const icon =
    (fileExtension === 'pdf')  ? <FilePdfTwoTone   {...propsIcon} twoToneColor="#f50" /> :
      (fileExtension === 'csv')  ? <FileTextTwoTone  {...propsIcon} twoToneColor="#1890ff" /> :
        (fileExtension === 'xlsx') ? <FileExcelTwoTone {...propsIcon} twoToneColor="#87d068" /> :
          <FileTwoTone {...propsIcon} />;
  return (
    <Row gutter={24} key={id}>
      {/*<Col>{ready ? (noted ? <CheckCircleTwoTone {...propsIcon} twoToneColor="#52c41a" /> : <ExclamationCircleTwoTone {...propsIcon} twoToneColor="#eb2f96" />) : <HourglassTwoTone {...propsIcon} />}</Col>*/}
      <Col span={24} className="row-reportIcon">{icon}</Col>
      <Col span={24} className="row-reportName"><Tooltip title={rptName}>{shortRptName}</Tooltip></Col>
      <Col span={24} className="row-reportDate">{rptDate}</Col>
    </Row>
  );
};

const ReportDownloadDrawerComponent: FunctionComponent<ReportDownloadCardComponentProps> = (props: any) => {
  const { id, rptName, rptDate, fileName, fileExtension, fileSize, statusLabel, noted } = props;
  return (
    <Row gutter={24} key={id} className={noted ? "rpt-noted" : "rpt-new"}>
      <Col span={24}>Report Name:</Col>
      <Col span={24}> - {rptName}</Col>
      <Divider />
      <Col span={24}>Report Date:</Col>
      <Col span={24}> - {rptDate}</Col>
      <Divider />
      <Col span={24}>Extension:</Col>
      <Col span={24}> - {fileExtension}</Col>
      <Divider />
      <Col span={24}>File Name:</Col>
      <Col span={24}> - {fileName}</Col>
      <Divider />
      <Col span={24}>File Size:</Col>
      <Col span={24}> - {convertToFileSize(fileSize)}</Col>
      <Divider />
      <Col span={24}>Status</Col>
      <Col span={24}> - {statusLabel}</Col>
    </Row>
  );
};
