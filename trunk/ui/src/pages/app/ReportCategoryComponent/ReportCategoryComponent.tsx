import React, { FunctionComponent, useState, useRef } from "react";
import { Divider, Tabs } from "antd";

import "./ReportCategoryComponent.scss";
import { ReportCategorySummaryTabComponent, ReportCategoryDetailTabComponent, ReportCategoryTemplateTabComponent, ReportCategoryAccessTabComponent } from "./tabs";
import { SavedSearchScheduleTabComponent} from "pages/app/ScheduleComponent/tabs";
import { MessageAlertComponent, EmptyMessageAlertProps, ConfirmModelComponent } from "components";
import { AuditModelComponent } from "components";
import { MessageType, ReportBuildType, TabEnum } from "enum";
import { PageComponentProps } from "types";
import { UseEffectUtils, PopupCommonFunctions } from "common/utils";
import { common } from "shared/common";

type ReportCategoryComponentProps = PageComponentProps;

const ReportCategoryComponent: FunctionComponent<ReportCategoryComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const [action, setAction] = useState<string>('');
  const [isEditing, setEditing] = useState<boolean>(false);
  const [selectedRow, setSelectedRow] = useState<any>({});
  const [isReloadSummaryTab] = useState<any>([false]);
  const [isEnableScheduleTab] = useState<any>([true]);
  const { shieldRef, componentName = 'ReportCategoryComponent', title = 'Access Group' } = props;

  const ReportCategorySummaryTabComponentRef: any = useRef();

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.DEFAULT:
        setActiveKey(TabEnum.DEFAULT_TAB);
        setEditing(false);
        if (isReloadSummaryTab[0]) {
          isReloadSummaryTab[0] = false;
          ReportCategorySummaryTabComponentRef.current.search();
        }
        break;
      case TabEnum.ADD:
        setActiveKey(TabEnum.SECOND_TAB);
        setAction(action);
        setSelectedRow({});
        setEditing(true);
        resetSkip(componentName);
        break;
      case TabEnum.EDIT:
        if (common.isNotEmpty(selectedRow)) {
          setActiveKey(TabEnum.SECOND_TAB);
          setAction(action);
          setSelectedRow(selectedRow);
          setEditing(true);
          resetSkip(componentName);
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.DELETE:
        setEditing(false);
        break;
      case TabEnum.AUDIT:
        console.log(selectedRow);
        if (common.isNotEmpty(selectedRow)) {
          setAuditModel({ ...auditModel, show: true });
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.EXPORT:
        common.downloadExcel(props.data, props.columns, 'Saved_Search_');
        setEditing(false);
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setSelectedRow(model);
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setSelectedRow(model);
    setAuditModel({ ...auditModel, model });
  };

  const onConfirm = (key: any = backKey[0]) => {
    onCloseMessage();
    if (key !== TabEnum.DEFAULT_TAB) {
      setActiveKey(key);
    } else {
      onSelectClear();
      setActiveKey(TabEnum.DEFAULT_TAB);
      setAction('');
      setEditing(false);
      if (isReloadSummaryTab[0]) {
        isReloadSummaryTab[0] = false;
        ReportCategorySummaryTabComponentRef.current.search();
        ReportCategorySummaryTabComponentRef.current.updateSuccess("Save success.");
      }
    }
  };

  const onUpdateRecord = (props: any) => {
    const { reload = true } = { ...props };
    isReloadSummaryTab[0] = reload;
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const { showPopup, backKey, onCloseLeaveWithoutSave, setChanged, onConfirmLeaveWithoutSave, isChanged, onChangeCallback } = popupFunctions(onConfirm);

  const groupTitle = () => {
    const list: any = [];
    if (common.isNotEmpty(title))
      list.push(title);
    if (action === TabEnum.ADD)
      list.push('(New)');
    if (action === TabEnum.EDIT)
      list.push('(Amendment)');
    return list.join(' ');
  };

  const propsTabSummary   = { shieldRef, componentName, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange, searchRef: ReportCategorySummaryTabComponentRef };
  const propsTabDetail    = { shieldRef, componentName, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, onSearch: () => ReportCategorySummaryTabComponentRef.current.search(), action, selectedRow, isChanged, setChanged ,pageType:ReportBuildType.REPORT_PAGE_TYPE_REPORT_CATEGORY};
  const propsConfirmModel = { show: showPopup, onClose: onCloseLeaveWithoutSave, onOk: onConfirmLeaveWithoutSave };


  return (
    <div className="ReportCategoryWrapper">
      {title && <><span className="header">{groupTitle()}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <ConfirmModelComponent {...propsConfirmModel} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey} onChange={onChangeCallback}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
         <ReportCategorySummaryTabComponent {...propsTabSummary} />
        </TabPane>
        {isEditing && <>
        {action === TabEnum.ADD && <>
        <TabPane tab="Report Category" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <ReportCategoryDetailTabComponent {...propsTabDetail} />}
        </TabPane>
        </>}
        {action === TabEnum.EDIT && <>
        <TabPane tab="Detail" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <ReportCategoryDetailTabComponent {...propsTabDetail} />}
        </TabPane>
        <TabPane tab="Report Group" key={TabEnum.THIRD_TAB}>
          {activeKey === TabEnum.THIRD_TAB && <ReportCategoryTemplateTabComponent {...propsTabDetail} />}
        </TabPane>
        <TabPane tab="Role Group" key={TabEnum.FORTH_TAB}>
          {activeKey === TabEnum.FORTH_TAB && <ReportCategoryAccessTabComponent {...propsTabDetail} />}
        </TabPane>
        {isEnableScheduleTab && <TabPane tab="Scheduler" key={TabEnum.FIFTH_TAB}>
          {activeKey === TabEnum.FIFTH_TAB && <SavedSearchScheduleTabComponent {...propsTabDetail} />}
        </TabPane>
        }
       </>}
        </>}
      </Tabs>
    </div>
  );
};

export default ReportCategoryComponent;

const { resetSkip } = UseEffectUtils;
const { popupFunctions } = PopupCommonFunctions;
const { TabPane } = Tabs;
