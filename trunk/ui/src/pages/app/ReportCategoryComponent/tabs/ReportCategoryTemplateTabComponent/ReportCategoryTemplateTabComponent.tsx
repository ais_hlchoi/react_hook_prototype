import React, { FunctionComponent, useState, useEffect } from "react";
import {Button, Col, Divider, Row, Spin, Table, Transfer} from "antd";
import difference from "lodash/difference";

import "./ReportCategoryTemplateTabComponent.scss";
import { ReportCategoryApi } from "api";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { UseEffectUtils } from "common/utils";

type ReportCategoryTemplateTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
};

const ReportCategoryTemplateTabComponent: FunctionComponent<ReportCategoryTemplateTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const [editingList, setEditingList] = useState<any>([]);
  const [initialList, setInitialList] = useState<any>([]);
  const [reportCategoryList, setReportCategoryList] = useState<any>([]);
  const [isShowTableList, setShowTableList] = useState<any>(true);
  const [isReadOnlyGroupList, setReadOnlyGroupList] = useState<boolean>(true);
  const { componentName, title = '' } = props;

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { id = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    if (id && action) {
      if (!isSkip(componentName)) {
        const model = {};
        ReportCategoryApi.getReportNameList(model).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const reportCategoryData = [...object];
            const reportCategoryList = reportCategoryData.map(({ id, name }) => ({ key: id, id, name }));
            setReportCategoryList(reportCategoryList);
            const reportCategoryModel = { id };
            ReportCategoryApi.getRecord(reportCategoryModel).then((response: any) => {
              const { responseCode, responseDesc, object } = response;
              if (ResponseCode.SUCCESS === responseCode) {
                const { reportList = [], restrictedFields = { report: true } } = object;
                const formData = [...reportList];
                const formList = formData.map((o: any, n: number) => ({ id: o.id, seq: n }));
                const formDefault = formList.map((o: any) => ({ ...reportCategoryList.find(({id}) => id === o.id), ...o }));
                setEditingKey(id);
                setEditingList(formDefault);
                setInitialList(formDefault);
                setShowTableList(true);
                setReadOnlyGroupList(restrictedFields.role);
              } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
                shieldRef.current.open({ ...response });
              } else {
                errFrameShow(MessageType.ERROR, responseDesc);
              }
            });
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    } else {
      setEditingKey(0);
      setEditingList([]);
      setInitialList([]);
      setShowTableList(false);
      setReadOnlyGroupList(true);
    }
  }, [props]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, setChanged, selectedRow: { name } } = props;

  const onSave = () => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const reportCategoryModel = { id: editingKey, rptTmplList: editingList.map(({id}) => id) };
    ReportCategoryApi.updateRecordsReportGroup(reportCategoryModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setEditingList([]);
    setInitialList([]);
    setShowTableList(false);
    setReadOnlyGroupList(true);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  const onReset = () => {
    onCloseMessage();
    setEditingList(initialList);
    setChanged(false);
  };

  const onChangeTransfer = (targetKeys: any, _direction: string, _moveKeys: any) => {
    const oldKeys = editingList.filter(({key}) => targetKeys.includes(key));
    const newKeys = reportCategoryList.filter(({key}) => targetKeys.includes(key)).filter(({key}) => !oldKeys.map(({key}) => key).includes(key)).map(({key, id, name}) => ({ key, id, name }));
    setEditingList(oldKeys.concat(newKeys).map((o: any, n: number) => ({ ...o, seq: n })));
    setChanged(true);
  };

  return (
    <div className="ReportCategoryTemplateTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      {isShowTableList && isReadOnlyGroupList && <><Row gutter={24} className="row-groupContainer">
      <TableList
        className="table-groupList"
        header={<><span className="hdr-title">Access Group Name</span><span className="hdr-categoryName">{name}</span></>}
        dataSource={reportCategoryList}
        leftColumns={leftTableColumns}
      />
      </Row>
      <Divider />
      <Button type="primary" className="btn-pageAction" children="Back"  onClick={onBack} />
      </>}
      {isShowTableList && !isReadOnlyGroupList && <><Row gutter={24} className="row-groupContainer">
      <TableTransfer
        className="table-groupList"
        header={<><span className="hdr-title">Access Group Name</span><span className="hdr-categoryName">{name}</span></>}
        dataSource={reportCategoryList}
        showSearch
        allowClear
        filterOption={(value: string, option: any) => option.name.toUpperCase().indexOf(value.toUpperCase()) > -1}
        targetKeys={editingList.map(({key}) => key)}
        onChange={onChangeTransfer}
        leftColumns={leftTableColumns}
        rightColumns={rightTableColumns}
      />
      </Row>
      <Divider />
      <Row gutter={24} style={{marginRight: 0}}>
          <Col span={23}>
            <Button type="primary" className="btn-pageAction" children="Reset" onClick={onReset} />
            <Button type="primary" className="btn-pageAction" children="Back"  onClick={onBack} />
          </Col>
          <Col span={1} style={{paddingLeft: 0}}>
            <Button type="primary" className="btn-pageAction" children="Save"  onClick={onSave} />
          </Col>
      </Row>
      </>}
      </Spin>
    </div>
  );
};

export default ReportCategoryTemplateTabComponent;

const { isSkip, setSkip, resetSkip } = UseEffectUtils;

const TableList: FunctionComponent<any> = ({ leftColumns, header, ...restProps }) => (
  <div className="row-groupList ant-list-bordered">
  {header ? <div className="ant-list-header">{header}</div> : null}
  <div className="ant-list-item">
  <Table {...restProps} columns={leftColumns} size="small" />
  </div>
  </div>
);

const TableTransfer: FunctionComponent<any> = ({ leftColumns, rightColumns, header, ...restProps }) => (
  <div className="row-groupList ant-list-bordered">
  {header ? <div className="ant-list-header">{header}</div> : null}
  <div className="ant-list-item">
  <Transfer {...restProps} showSelectAll={false}>
    {({
      direction,
      filteredItems,
      onItemSelectAll,
      onItemSelect,
      selectedKeys: listSelectedKeys,
      disabled: listDisabled,
    }) => {
      const columns = direction === 'left' ? leftColumns : rightColumns;
      const rowSelection = {
        getCheckboxProps: (o: any) => ({ disabled: listDisabled || o.disabled }),
        onSelectAll(selected: boolean, selectedRows: any) {
          const treeSelectedKeys = selectedRows.filter((o: any) => !o.disabled).map((o: any) => o.key);
          const diffKeys = selected
            ? difference(treeSelectedKeys, listSelectedKeys)
            : difference(listSelectedKeys, treeSelectedKeys);
          onItemSelectAll(diffKeys, selected);
        },
        onSelect: (record: any, selected: boolean) => onItemSelect(record.key, selected),
        selectedRowKeys: listSelectedKeys,
      };
      return (
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={filteredItems}
          size="small"
          style={{ pointerEvents: listDisabled ? 'none' : undefined }}
          onRow={({ key, disabled: itemDisabled }) => ({
            onClick: () => {
              if (itemDisabled || listDisabled)
                return;
              const keyStr = `${key}`;
              onItemSelect(keyStr, !listSelectedKeys.includes(keyStr));
            },
          })}
        />
      );
    }}
  </Transfer>
  </div>
  </div>
);

const leftTableColumns = [
  { dataIndex: 'name', title: 'Name' },
];

const rightTableColumns = [
  { dataIndex: 'name', title: 'Name' },
];
