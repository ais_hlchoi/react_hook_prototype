import React, { FunctionComponent, useState, useEffect } from "react";
import { Form, Radio, Descriptions, Divider, Input, Button, Spin, Row, Col } from "antd";

import "./ReportCategoryDetailTabComponent.scss";
import { ReportCategoryApi } from "api";
import { GenerateOption, MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { CommonUtils, UseEffectUtils } from "common/utils";
import { generateTypeData } from "common/data/ReportBuilderData";

type ReportCategoryDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  isChanged?: Function;
  setChanged?: Function;
};

const ReportCategoryDetailTabComponent: FunctionComponent<ReportCategoryDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const { componentName, title = '' } = props;

  const [form] = Form.useForm();
  const [formDefault, setFormDefault] = useState<any>();
  const [generateOptionDefault, setGenerateOptionDefault] = useState<string>(GenerateOption.DEFAULT_OPTION);
  const [generateOptionList, setGenerateOptionList] = useState<any>([]);
  const [isNotEmptyDefault, setNotEmptyDefault] = useState<boolean>(false);
  const [isEnableBtnSave, setEnableBtnSave] = useState<boolean>(false);
  const [isReadOnlyCategoryName, setReadOnlyCategoryName] = useState<boolean>(true);

  useEffect(() => {
    const { isChanged } = props;
    if (isChanged())
      return;
    const options: any = [...generateTypeData];
    setGenerateOptionList(options);
    if (!options.some((option: any) => option.value === GenerateOption.DEFAULT_OPTION)) {
      const [option = { value: '' },] = options;
      setGenerateOptionDefault(option.value);
    }
  }, [props]);

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { id = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    if (id && action) {
      if (!isSkip(componentName)) {
        const model = { id };
        ReportCategoryApi.getRecord(model).then((response) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const { restrictedFields = { name: true } } = object;
            const formData = { ...object };
            setEditingKey(id);
            form.setFieldsValue(formData);
            setFormDefault(formData);
            setNotEmptyDefault(true);
            setReadOnlyCategoryName(restrictedFields.name);
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    } else {
      setEditingKey(0);
      setFormDefault({});
      setNotEmptyDefault(false);
      setReadOnlyCategoryName(false);
    }
  }, [props, form]);

  const onChangeCompleteInput = (_event: any) => {
    const mand = [ 'name' ];
    setEnableBtnSave(checkBtnDoneEnabled(Object.entries(form.getFieldsValue()).filter(([k]) => mand.includes(k)).map(([, v]) => v)));
    setChanged(true);
  };

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, setChanged } = props;

  const onSave = (value: any) => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const { reportCategoryModel, callback } = value;
    if (!isCreateMode(editingKey) && editingKey !== reportCategoryModel.id) {
      console.log(`Incorrect ReportTemplateId [ ${editingKey} != ${reportCategoryModel.id} ]`);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    ReportCategoryApi.insertRecords(reportCategoryModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        if (typeof callback === 'function') {
          callback();
        }
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setFormDefault({});
    setNotEmptyDefault(false);
    setReadOnlyCategoryName(true);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  const onReset = () => {
    onCloseMessage();
    setChanged(false);
    if (isNotEmptyDefault) {
      form.setFieldsValue(formDefault);
      setEnableBtnSave(false);
    } else {
      form.resetFields();
      setEnableBtnSave(false);
    }
  };

  const { propsBtnSave = {}, propsBtnReset = {}, propsBtnBack = {} } = props;
  Object.assign(propsBtnSave,  { callback: onReset, action: onSave, show: !isReadOnlyCategoryName, disabled: !isEnableBtnSave });
  Object.assign(propsBtnReset, { callback: onReset, show: !isReadOnlyCategoryName });
  Object.assign(propsBtnBack,  { callback: onReset, action: onBack });

  return (
    <div className="ReportCategoryDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      <Form form={form} name="form">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Group Report Name" className="editing" children="" />
      </Descriptions>
      <Form.Item name="name">
      <Input className="input-categoryName" placeholder="Name" maxLength={100} onChange={onChangeCompleteInput} autoComplete="off" readOnly={isReadOnlyCategoryName} />
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Group Report Description" className="editing" children="" />
      </Descriptions>
      <Form.Item name="description">
        <Input.TextArea className="input-reportDescription" placeholder="Description" maxLength={200} onChange={onChangeCompleteInput} autoComplete="off" autoSize={{ minRows: 3, maxRows: 50 }} />
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Generate Type" className="editing" children="" />
      </Descriptions>
      <Form.Item name="generateOpt" initialValue={generateOptionDefault}>
        <Radio.Group optionType="button" options={generateOptionList.map(({label, value}) => ({label, value}))} onChange={(event) => onChangeCompleteInput(event)} />
      </Form.Item>
      <Form.Item name="id">
        <Input type="hidden" />
      </Form.Item>
      </Form>
      <Divider />
      <div className="tab-action">
        <Row>
          <Col span={23}>
            <ButtonReset {...{ ...props, propsBtnReset }} />
            <ButtonBack  {...{ ...props, propsBtnBack }} />
          </Col>
          <Col span={1}>
            <ButtonSave  {...{ ...props, propsBtnSave, form }} />
          </Col>
        </Row>
      </div>
      </Spin>
    </div>
  );
};

export default ReportCategoryDetailTabComponent;

class ReportCategoryDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
  static checkBtnDoneEnabled = (arr: any[]) => arr.every(o => CommonUtils.isDirty(o));
}

const { isCreateMode, checkBtnDoneEnabled } = ReportCategoryDetailTabUtils;
const { isSkip, setSkip, resetSkip } = UseEffectUtils;

type ReportCategoryDetailTabBtnProps = {
  label?: string;
  href?: string;
  action?: Function;
  callback?: Function;
  show?: boolean;
  disabled?: boolean;
};

const ButtonReset: FunctionComponent<ReportCategoryDetailTabBtnProps> = (props: any) => {
  const { propsBtnReset = {} } = props;
  const { label = 'Reset', action, callback, show = true, disabled = false } = propsBtnReset;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonSave: FunctionComponent<ReportCategoryDetailTabBtnProps> = (props: any) => {
  const { propsBtnSave = {}, form } = props;
  const { label = 'Save', action, callback, show = true, disabled = false } = propsBtnSave;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback, reportCategoryModel: { ...form.getFieldsValue() } }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonBack: FunctionComponent<ReportCategoryDetailTabBtnProps> = (props: any) => {
  const { propsBtnBack = {}, ...restProps } = props;
  const propsBtnReset = { ...propsBtnBack, label: 'Back' }
  return <ButtonReset {...{ ...restProps, propsBtnReset }} />
};
