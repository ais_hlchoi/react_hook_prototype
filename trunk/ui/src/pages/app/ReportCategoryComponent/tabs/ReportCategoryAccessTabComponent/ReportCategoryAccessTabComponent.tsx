import React, { FunctionComponent, useState, useEffect } from "react";
import {Button, Col, Divider, Row, Select, Spin, Table, Transfer} from "antd";
import difference from "lodash/difference";

import "./ReportCategoryAccessTabComponent.scss";
import { ReportCategoryApi, ReportAccessGroupApi, SavedSearchApi } from "api";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { UseEffectUtils } from "common/utils";

type ReportCategoryAccessTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  multipleRole?: boolean;
};

const ReportCategoryAccessTabComponent: FunctionComponent<ReportCategoryAccessTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const [editingList, setEditingList] = useState<any>([]);
  const [initialList, setInitialList] = useState<any>([]);
  const [reportAccessGroupList, setReportAccessGroupList] = useState<any>([]);
  const [isShowTableList, setShowTableList] = useState<any>(true);
  const { componentName, title = '' } = props;

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { id = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    if (id && action) {
      if (!isSkip(componentName)) {
        const model = {};
        SavedSearchApi.getReportAccessGroupRoleList(model).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const reportAccessGroupData = [...object];
            const reportAccessGroupList = reportAccessGroupData.map(({ id, name }) => ({ key: id, id, name }));
            setReportAccessGroupList(reportAccessGroupList);
            const reportAccessGroupModel = { rptCatgId: id };
            ReportCategoryApi.getReportAccessGroupRoleList(reportAccessGroupModel).then(response => {
              const { responseCode, responseDesc, object } = response;
              if (ResponseCode.SUCCESS === responseCode) {
                const formData = [...object];
                const formList = formData.map(({id}) => id);
                const formDefault = reportAccessGroupList.filter(({id}) => formList.includes(id));
                setEditingKey(id);
                setEditingList(formDefault);
                setInitialList(formDefault);
                setShowTableList(true);
              } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
                shieldRef.current.open({ ...response });
              } else {
                errFrameShow(MessageType.ERROR, responseDesc);
              }
            });
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    } else {
      setEditingKey(0);
      setEditingList([]);
      setInitialList([]);
    }
  }, [props]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, setChanged, selectedRow: { ReportCategory }, multipleRole = true } = props;
  const singleRole = !multipleRole;

  const onSave = () => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const reportAccessGroupModel = { rptCatgId: editingKey, roleList: editingList.map(({id}) => id) };
    ReportAccessGroupApi.updateRecordsRoleGroup(reportAccessGroupModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    onCloseMessage();
    setEditingKey(0);
    setEditingList([]);
    setInitialList([]);
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  const onReset = () => {
    onCloseMessage();
    setEditingList(initialList);
    setChanged(false);
  };

  const onChangeSelect = (value: any, _option: any) => {
    setEditingList(reportAccessGroupList.filter(({key}) => key === value).map(({key, id, name}) => ({ key, id, name })));
    setChanged(true);
  };

  const onChangeTransfer = (targetKeys: any, _direction: string, _moveKeys: any) => {
    setEditingList(reportAccessGroupList.filter(({key}) => targetKeys.includes(key)).map(({key, id, name}) => ({ key, id, name })));
    setChanged(true);
  };

  return (
    <div className="ReportCategoryAccessTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      {isShowTableList && <Row gutter={24} className="row-groupContainer">
      {multipleRole &&
      <TableTransfer
        className="table-groupList"
        header={<><span className="hdr-title">Report Name</span><span className="hdr-reportName">{ReportCategory}</span></>}
        dataSource={reportAccessGroupList}
        showSearch
        allowClear
        filterOption={(value: string, option: any) => option.name.toUpperCase().indexOf(value.toUpperCase()) > -1}
        targetKeys={editingList.map(({key}) => key)}
        onChange={onChangeTransfer}
        leftColumns={leftTableColumns}
        rightColumns={rightTableColumns}
      />
      }
      {singleRole &&
      <SingleSelect
        header={<><span className="hdr-title">Report Name</span><span className="hdr-reportName">{ReportCategory}</span></>}
        dataSource={reportAccessGroupList}
        showSearch
        allowClear
        filterOption={(value: string, option: any) => option.name.toUpperCase().indexOf(value.toUpperCase()) > -1}
        targetKeys={editingList.map(({key}) => key)}
        onChange={onChangeSelect}
      />
      }
      </Row>}
      <Divider />
      <Row gutter={24} style={{marginRight: 0}}>
        <Col span={23}>
          <Button type="primary" className="btn-pageAction" children="Reset" onClick={onReset} />
          <Button type="primary" className="btn-pageAction" children="Back"  onClick={onBack} />
        </Col>
        <Col span={1} style={{paddingLeft: 0}}>
          <Button type="primary" className="btn-pageAction" children="Save"  onClick={onSave} />
        </Col>
      </Row>
      </Spin>
    </div>
  );
};

export default ReportCategoryAccessTabComponent;

const { isSkip, setSkip, resetSkip } = UseEffectUtils;

const SingleSelect: FunctionComponent<any> = ({ dataSource, targetKeys = [], header, ...restProps }) => (
  <div className="row-groupList ant-list-bordered">
  {header ? <div className="ant-list-header">{header}</div> : null}
  <div className="ant-list-item">
  <Select {...restProps} value={targetKeys.length ? targetKeys[0] : undefined}>
    {dataSource.map((o: any) => <Select.Option {...o} value={o.id} children={o.name} />)}
  </Select>
  </div>
  </div>
);

const TableTransfer: FunctionComponent<any> = ({ leftColumns, rightColumns, header, ...restProps }) => (
  <div className="row-groupList ant-list-bordered">
  {header ? <div className="ant-list-header">{header}</div> : null}
  <div className="ant-list-item">
  <Transfer {...restProps} showSelectAll={false}>
    {({
      direction,
      filteredItems,
      onItemSelectAll,
      onItemSelect,
      selectedKeys: listSelectedKeys,
      disabled: listDisabled,
    }) => {
      const columns = direction === 'left' ? leftColumns : rightColumns;
      const rowSelection = {
        getCheckboxProps: (o: any) => ({ disabled: listDisabled || o.disabled }),
        onSelectAll(selected: boolean, selectedRows: any) {
          const treeSelectedKeys = selectedRows.filter((o: any) => !o.disabled).map((o: any) => o.key);
          const diffKeys = selected
            ? difference(treeSelectedKeys, listSelectedKeys)
            : difference(listSelectedKeys, treeSelectedKeys);
          onItemSelectAll(diffKeys, selected);
        },
        onSelect: (record: any, selected: boolean) => onItemSelect(record.key, selected),
        selectedRowKeys: listSelectedKeys,
      };
      return (
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={filteredItems}
          size="small"
          style={{ pointerEvents: listDisabled ? 'none' : undefined }}
          onRow={({ key, disabled: itemDisabled }) => ({
            onClick: () => {
              if (itemDisabled || listDisabled)
                return;
              const keyStr = `${key}`;
              onItemSelect(keyStr, !listSelectedKeys.includes(keyStr));
            },
          })}
        />
      );
    }}
  </Transfer>
  </div>
  </div>
);

const leftTableColumns = [
  { dataIndex: 'name', title: 'Name' },
];

const rightTableColumns = [
  { dataIndex: 'name', title: 'Name' },
];
