export { default as ReportCategoryAccessTabComponent   } from "./ReportCategoryAccessTabComponent";
export { default as ReportCategoryDetailTabComponent   } from "./ReportCategoryDetailTabComponent";
export { default as ReportCategorySummaryTabComponent  } from "./ReportCategorySummaryTabComponent";
export { default as ReportCategoryTemplateTabComponent } from "./ReportCategoryTemplateTabComponent";
