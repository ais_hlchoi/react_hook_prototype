import React, { FunctionComponent, useState, useEffect } from "react";
import { Form, Descriptions, Divider, Input, Button, Spin, Row, Col } from "antd";

import "./ReportAccessGroupDetailTabComponent.scss";
import { ReportAccessGroupApi } from "api";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { CommonUtils } from "common/utils";

type ReportAccessGroupDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
};

const ReportAccessGroupDetailTabComponent: FunctionComponent<ReportAccessGroupDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const { title = '' } = props;

  const [form] = Form.useForm();
  const [formDefault, setFormDefault] = useState<any>();
  const [isNotEmptyDefault, setNotEmptyDefault] = useState<boolean>(false);
  const [isEnableBtnSave, setEnableBtnSave] = useState<boolean>(false);
  const [isReadOnlyAccessGroupName, setReadOnlyAccessGroupName] = useState<boolean>(true);

  useEffect(() => {
    const { shieldRef, errFrameShow, selectedRow: { id = 0 }, action, isChanged } = props;
    if (isChanged())
      return;
    if (id && action) {
      const model = { id };
      ReportAccessGroupApi.getRecord(model).then((response: any) => {
        const { responseCode, responseDesc, object } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const { restrictedFields = { name: true } } = object;
          const formData = { ...object };
          setEditingKey(id);
          form.setFieldsValue(formData);
          setFormDefault(formData);
          setNotEmptyDefault(true);
          setReadOnlyAccessGroupName(restrictedFields.name);
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      });
    } else {
      setEditingKey(0);
      setFormDefault({});
      setNotEmptyDefault(false);
      setReadOnlyAccessGroupName(false);
    }
  }, [props, form]);

  const onChangeCompleteInput = (_event: any) => {
    const mand = [ 'name' ];
    setEnableBtnSave(checkBtnDoneEnabled(Object.entries(form.getFieldsValue()).filter(([k]) => mand.includes(k)).map(([, v]) => v)));
    setChanged(true);
  };

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, setChanged } = props;

  const onSave = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const { reportAccessGroupModel, callback } = value;
    if (!isCreateMode(editingKey) && editingKey !== reportAccessGroupModel.id) {
      console.log(`Incorrect ReportTemplateId [ ${editingKey} != ${reportAccessGroupModel.id} ]`);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    ReportAccessGroupApi.insertRecords(reportAccessGroupModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        onUpdateRecord();
        if (typeof callback === 'function') {
          callback();
        }
        onBack({});
        errFrameShow(MessageType.SUCCESS, "Save success.");
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    });
  };

  const onBack = (value: any) => {
    const { callback } = value;
    setEditingKey(0);
    setFormDefault({});
    setNotEmptyDefault(false);
    setReadOnlyAccessGroupName(true);
    onCloseMessage();
    setChanged(false);
    onChangeCallback(TabEnum.DEFAULT_TAB);
    if (typeof callback === 'function') {
      callback();
    }
  };

  const onReset = () => {
    onCloseMessage();
    setChanged(false);
    if (isNotEmptyDefault) {
      form.setFieldsValue(formDefault);
      setEnableBtnSave(false);
    } else {
      form.resetFields();
      setEnableBtnSave(false);
    }
  };

  const { propsBtnSave = {}, propsBtnReset = {}, propsBtnBack = {} } = props;
  Object.assign(propsBtnSave,  { callback: onReset, action: onSave, show: !isReadOnlyAccessGroupName, disabled: !isEnableBtnSave });
  Object.assign(propsBtnReset, { callback: onReset, show: !isReadOnlyAccessGroupName });
  Object.assign(propsBtnBack,  { callback: onReset, action: onBack });

  return (
    <div className="ReportAccessGroupDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      <Form form={form} name="form">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Access Group Name" className="editing" children="" />
      </Descriptions>
      <Form.Item name="name">
      <Input className="input-accessGroupName" placeholder="Name" onChange={onChangeCompleteInput} autoComplete="off" readOnly={isReadOnlyAccessGroupName} maxLength={100} />
      </Form.Item>
      <Form.Item name="id">
        <Input type="hidden" />
      </Form.Item>
      </Form>
      <Divider />
      <div className="tab-action">
        <Row>
          <Col span={23}>
            <ButtonReset {...{ ...props, propsBtnReset }} />
            <ButtonBack  {...{ ...props, propsBtnBack }} />
          </Col>
          <Col span={1}>
            <ButtonSave  {...{ ...props, propsBtnSave, form }} />
          </Col>
        </Row>
      </div>
      </Spin>
    </div>
  );
};

export default ReportAccessGroupDetailTabComponent;

class ReportAccessGroupDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
  static checkBtnDoneEnabled = (arr: any[]) => arr.every(o => CommonUtils.isDirty(o));
}

const { isCreateMode, checkBtnDoneEnabled } = ReportAccessGroupDetailTabUtils;

type ReportAccessGroupDetailTabBtnProps = {
  label?: string;
  href?: string;
  action?: Function;
  callback?: Function;
  show?: boolean;
  disabled?: boolean;
};

const ButtonReset: FunctionComponent<ReportAccessGroupDetailTabBtnProps> = (props: any) => {
  const { propsBtnReset = {} } = props;
  const { label = 'Reset', action, callback, show = true, disabled = false } = propsBtnReset;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonSave: FunctionComponent<ReportAccessGroupDetailTabBtnProps> = (props: any) => {
  const { propsBtnSave = {}, form } = props;
  const { label = 'Save', action, callback, show = true, disabled = false } = propsBtnSave;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback, reportAccessGroupModel: { ...form.getFieldsValue() } }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonBack: FunctionComponent<ReportAccessGroupDetailTabBtnProps> = (props: any) => {
  const { propsBtnBack = {}, ...restProps } = props;
  const propsBtnReset = { ...propsBtnBack, label: 'Back' }
  return <ButtonReset {...{ ...restProps, propsBtnReset }} />
};
