import React, { FunctionComponent, useState, useImperativeHandle } from "react";
import { Divider, Table, Spin, Modal } from "antd";
import { ModalFuncProps } from "antd/lib/modal";

import "./ReportAccessGroupSummaryTabComponent.scss";
import { ReportAccessGroupApi } from "api";
import { AuthButtonComponent, SearchBarComponent, ResizableTableComponent } from "components";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { common } from "shared/common";

type ReportAccessGroupSummaryTabComponentProps = TabComponentProps & {
  searchRef: any;
};

const ReportAccessGroupSummaryTabComponent: FunctionComponent<ReportAccessGroupSummaryTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(EmptyPageProps());
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);

  useImperativeHandle(props.searchRef, () => ({
    search: () => {
      onSearch(searchModel);
    },
    updateSuccess: (value: any) => {
      errFrameShow(MessageType.SUCCESS, value);
    }
  }));

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const model = { ...EmptyPageProps(), ...value };
    ReportAccessGroupApi.searchRecords(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onExport = (action: any) => {
    onCloseMessage();
    setLoading(true);
    ReportAccessGroupApi.searchRecordsAll(searchModel).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        onAuthButtonClick({ action, data: response.object, columns: summaryColumns });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  }; 

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
    setSearchModel(EmptyPageProps());
  };

  const onButtonClick = (action: any) => {
    // if EDIT button clicked. Record id also needed to be passed.
    if (action === TabEnum.ADD) {
      onAuthButtonClick({ action, selectedRow: {} });
    }
    if (action === TabEnum.EDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.DELETE) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
      if (common.isNotEmpty(selectedRow)) {
        confirm({ ...EmptyConfirmDeleteProps(), ...{
          onOk() {
            const [record] = selectedRow;
            const model = {};
            Object.assign(model, record);
            ReportAccessGroupApi.deleteRecords(model).then((response) => {
              const { responseCode, responseDesc } = response;
              if (ResponseCode.SUCCESS === responseCode) {
                setSelectedRow([]);
                onSearch(searchModel);
                errFrameShow(MessageType.SUCCESS, "Delete success.");
              } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
                shieldRef.current.open({ ...response });
              } else {
                errFrameShow(MessageType.ERROR, responseDesc);
              }
            });
          },
        }});
      } else {
        errFrameShow(MessageType.ERROR, "Please select a record.");
      }
    }
    if (action === TabEnum.AUDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.EXPORT) {
      onExport(action);
    }
  }

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = { ...searchModel, page, pageSize };
    setLoading(true);
    ReportAccessGroupApi.searchRecords(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'id';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  return (
    <div className="ReportAccessGroupSummaryTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={searchFieldList}
      />
      <Divider />
      <AuthButtonComponent
        onAuthButtonClick={onButtonClick}
      />
      <ResizableTableComponent
        className={"sysTable" + (searchList.length ? " tableTop" : "")}
        bordered
        rowKey={rowKey}
        rowSelection={rowSelection}
        dataSource={searchList}
        columns={summaryColumns}
        pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default ReportAccessGroupSummaryTabComponent;

class ReportAccessGroupSummaryTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });
  static EmptyConfirmDeleteProps = (): ModalFuncProps => ({
    title: 'Are you sure to delete?',
    content: '',
    okText: 'Confirm',
    okType: 'danger',
    cancelText: 'Cancel',
    onOk: () => {},
    onCancel: () => console.log('onCancel!'),
  });
}

const { EmptyPageProps, EmptyConfirmDeleteProps } = ReportAccessGroupSummaryTabUtils;
const { confirm } = Modal;

const searchFieldList = [
];

const summaryColumns = [
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Role",
    dataIndex: "roleCount",
  },
  {
    title: "Report",
    dataIndex: "reportCount",
  },
];
