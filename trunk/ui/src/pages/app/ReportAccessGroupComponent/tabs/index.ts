export { default as ReportAccessGroupSummaryTabComponent  } from "./ReportAccessGroupSummaryTabComponent";
export { default as ReportAccessGroupDetailTabComponent   } from "./ReportAccessGroupDetailTabComponent";
export { default as ReportAccessGroupRoleTabComponent     } from "./ReportAccessGroupRoleTabComponent";
export { default as ReportAccessGroupUserTabComponent     } from "./ReportAccessGroupUserTabComponent";
export { default as ReportAccessGroupTemplateTabComponent } from "./ReportAccessGroupTemplateTabComponent";
