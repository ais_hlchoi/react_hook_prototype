import React, { FunctionComponent, useState, useRef } from "react";
import { Divider, Tabs } from "antd";

import "./ReportAccessGroupComponent.scss";
import { ReportAccessGroupSummaryTabComponent, ReportAccessGroupDetailTabComponent, ReportAccessGroupRoleTabComponent, ReportAccessGroupTemplateTabComponent } from "./tabs";
import { MessageAlertComponent, EmptyMessageAlertProps, ConfirmModelComponent } from "components";
import { AuditModelComponent } from "components";
import { MessageType, TabEnum } from "enum";
import { PageComponentProps } from "types";
import { common } from "shared/common";
import { PopupCommonFunctions } from "common/utils";

type ReportAccessGroupComponentProps = PageComponentProps;

const ReportAccessGroupComponent: FunctionComponent<ReportAccessGroupComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const [action, setAction] = useState<string>('');
  const [isEditing, setEditing] = useState<boolean>(false);
  const [selectedRow, setSelectedRow] = useState<any>({});
  const [isReloadSummaryTab] = useState<any>([false]);
  const { shieldRef, title = 'Access Group' } = props;

  const ReportAccessGroupSummaryTabComponentRef: any = useRef();

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.DEFAULT:
        setActiveKey(TabEnum.DEFAULT_TAB);
        setEditing(false);
        if (isReloadSummaryTab[0]) {
          isReloadSummaryTab[0] = false;
          ReportAccessGroupSummaryTabComponentRef.current.search();
        }
        break;
      case TabEnum.ADD:
        setActiveKey(TabEnum.SECOND_TAB);
        setAction(action);
        setSelectedRow({});
        setEditing(true);
        break;
      case TabEnum.EDIT:
        if (common.isNotEmpty(selectedRow)) {
          setActiveKey(TabEnum.SECOND_TAB);
          setAction(action);
          setSelectedRow(selectedRow);
          setEditing(true);
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.DELETE:
        setEditing(false);
        break;
      case TabEnum.AUDIT:
        console.log(selectedRow);
        if (common.isNotEmpty(selectedRow)) {
          setAuditModel({ ...auditModel, show: true });
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.EXPORT:
        common.downloadExcel(props.data, props.columns, 'Saved_Search_');
        setEditing(false);
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setSelectedRow(model);
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setSelectedRow(model);
    setAuditModel({ ...auditModel, model });
  };

  const onConfirm = (key: any = backKey[0]) => {
    onCloseMessage();
    if (key !== TabEnum.DEFAULT_TAB) {
      setActiveKey(key);
    } else {
      onSelectClear();
      setActiveKey(TabEnum.DEFAULT_TAB);
      setAction('');
      setEditing(false);
      if (isReloadSummaryTab[0]) {
        isReloadSummaryTab[0] = false;
        ReportAccessGroupSummaryTabComponentRef.current.search();
      }
    }
  }

  const onUpdateRecord = (props: any) => {
    const { reload = true } = { ...props };
    isReloadSummaryTab[0] = reload;
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const { showPopup, backKey, onCloseLeaveWithoutSave, setChanged, onConfirmLeaveWithoutSave, isChanged, onChangeCallback } = popupFunctions(onConfirm);

  const groupTitle = () => {
    const list: any = [];
    if (common.isNotEmpty(title))
      list.push(title);
    if (action === TabEnum.ADD)
      list.push('(New)');
    if (action === TabEnum.EDIT)
      list.push('(Amendment)');
    return list.join(' ');
  };

  const propsTabSummary = { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange, searchRef: ReportAccessGroupSummaryTabComponentRef };
  const propsTabDetail  = { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, onSearch: () => ReportAccessGroupSummaryTabComponentRef.current.search(), action, selectedRow, isChanged, setChanged };
  const propsConfirmModel = { show: showPopup, onClose: onCloseLeaveWithoutSave, onOk: onConfirmLeaveWithoutSave };

  return (
    <div className="ReportAccessGroupWrapper">
      {title && <><span className="header">{groupTitle()}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <ConfirmModelComponent {...propsConfirmModel} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey} onChange={onChangeCallback}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
         <ReportAccessGroupSummaryTabComponent {...propsTabSummary} />
        </TabPane>
        {isEditing && <>
        {action === TabEnum.ADD && <>
        <TabPane tab="Report Access Group" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <ReportAccessGroupDetailTabComponent {...propsTabDetail} />}
        </TabPane>
        </>}
        {action === TabEnum.EDIT && <>
        <TabPane tab="Detail" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <ReportAccessGroupDetailTabComponent {...propsTabDetail} />}
        </TabPane>
        <TabPane tab="Role Group" key={TabEnum.THIRD_TAB}>
          {activeKey === TabEnum.THIRD_TAB && <ReportAccessGroupRoleTabComponent {...propsTabDetail} />}
        </TabPane>
        <TabPane tab="Report Group" key={TabEnum.FORTH_TAB}>
          {activeKey === TabEnum.FORTH_TAB && <ReportAccessGroupTemplateTabComponent {...propsTabDetail} />}
        </TabPane>
        </>}
        </>}
      </Tabs>
    </div>
  );
};

export default ReportAccessGroupComponent;

const { TabPane } = Tabs;
const { popupFunctions } = PopupCommonFunctions;
