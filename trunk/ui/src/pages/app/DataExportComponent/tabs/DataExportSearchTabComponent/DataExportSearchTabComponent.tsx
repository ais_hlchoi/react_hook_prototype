import React, { FunctionComponent, useState, useEffect } from "react";
import { Row, Col, Select, Divider } from "antd";

import "./DataExportSearchTabComponent.scss";
import ReportTemplateModel from "pages/app/ReportTemplateComponent/ReportTemplateModel";
import ReportTemplateComponent from "pages/app/ReportTemplateComponent";
import { DataExportApi, ReportTemplateApi } from "api";
import { ResponseCode, MessageType } from "enum";
import { TabComponentProps } from "types";

type DataExportSearchTabComponentProps = TabComponentProps;

const DataExportSearchTabComponent: FunctionComponent<DataExportSearchTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [reportNode, setReportNode] = useState<any>(<></>);
  const [reportTemplateList, setReportTemplateList] = useState<any>([]);

  useEffect(() => {
    const { shieldRef, errFrameShow } = props;
    setLoading(true);
    const reportPropsModel = {};
    ReportTemplateApi.getReportNameList(reportPropsModel).then((response: any) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const reportTemplateListData = [...object];
        setReportTemplateList(reportTemplateListData);
        setLoading(false);
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    });
  }, [props]);

  const generateReport = (reportTemplateModel: ReportTemplateModel) => {
    const { errFrameShow, onCloseMessage } = props;
    onCloseMessage();
    setLoading(true);
    // TODO - plan to handle ResponseCode.SHIELD since api returns blob (not json)
    DataExportApi.generateDynamicRpt(reportTemplateModel).then((blob: any) => {
      if (blob.size > 10) {
        var a = document.createElement('a');
        document.body.appendChild(a);
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'dynamic_rpt.' + reportTemplateModel.option.output;
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
        errFrameShow(MessageType.SUCCESS, "Export success.");
      } else {
        errFrameShow(MessageType.ERROR, "Export fail.");
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Export fail.");
    }).finally(() => {
      setLoading(false);
    });
  };

  const onChangeReportKey = (value: any) => {
    const { shieldRef, errFrameShow, onCloseMessage } = props;
    onCloseMessage();
    setReportNode(<></>);
    if (value) {
      setLoading(true);
      const reportPropsModel = { id: value };
      ReportTemplateApi.getReportProps(reportPropsModel).then((response: any) => {
        const { responseCode, responseDesc, object } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const reportProps = { generateReport, isShowBtnExport: true, isShowBtnSave: false, reportPropsModel: object };
          setReportNode(<><Divider /><ReportTemplateComponent {...reportProps} /></>);
          setLoading(false);
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      });
    }
  };

  return (
    <div className="DataExportSearchTabWrapper">
      <Row gutter={17}>
        <Col span={3}>Report Name:</Col>
        <Col span={14}>
          <Select
            className="input-report-key"
            placeholder="Name"
            loading={loading}
            allowClear
            showSearch
            optionFilterProp="children"
            filterOption={(value: string, option: any) => option.children.toUpperCase().indexOf(value.toUpperCase()) > -1}
            onChange={onChangeReportKey}
            children={reportTemplateList.map(({id, name}) => <Select.Option { ...{ key: id, value: id, children: name }} />)}
          />
          {reportNode}
        </Col>
      </Row>
    </div>
  );
};

export default DataExportSearchTabComponent;
