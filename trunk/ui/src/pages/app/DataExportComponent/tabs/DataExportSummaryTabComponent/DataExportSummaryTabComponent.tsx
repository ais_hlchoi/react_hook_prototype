import React, { FunctionComponent, useState, useEffect, useContext } from "react";
import { useHistory } from 'react-router-dom';
import { Typography, Divider, Drawer, Spin } from "antd";


import { SnippetsOutlined, ReconciliationOutlined } from '@ant-design/icons';

import "./DataExportSummaryTabComponent.scss";
import ReportTemplateModel from "pages/app/ReportTemplateComponent/ReportTemplateModel";
import ReportTemplateComponent from "pages/app/ReportTemplateComponent";
import { DataExportApi, ReportCategoryApi, ReportTemplateApi } from "api";
import { MessageType, ReportExportType, ResponseCode } from "enum";
import { TabComponentProps } from "types";
import { ServicesContext } from "services/contexts/ServicesContext";
import { SearchBarComponent, ResizableTableComponent } from "components";

type DataExportSummaryTabComponentProps = TabComponentProps & {
  asyncGenerateReport?: boolean;
};

const { Paragraph } = Typography;

const DataExportSummaryTabComponent: FunctionComponent<DataExportSummaryTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [reportNode, setReportNode] = useState<any>(<></>);
  const [reportTemplateList, setReportTemplateList] = useState<any>([]);
  const [drawerModel, setDrawerModel] = useState<any>(EmptyDrawerProps());


  const history = useHistory();
  const { AuthenticationService } = useContext(ServicesContext);
  const { menuRef } = props;


  useEffect(() => {
    const reportPropsModel = {};
    onSearch(reportPropsModel);  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props]);

  const { shieldRef, errFrameShow, onCloseMessage } = props;

  const onSearch = (value: any) => {
    setReportTemplateList([]);
    setLoading(true);
    ReportTemplateApi.getReportNameList(value).then((response: any) => {
      const { responseCode, responseDesc, object } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const reportTemplateListData = [...object];
        setReportTemplateList(reportTemplateListData);
        setLoading(false);
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    });
  };

  const onReset = () => {
    onCloseMessage();
    setReportTemplateList([]);
  };


  const generateReport = (reportTemplateModel: ReportTemplateModel) => {
    const { shieldRef, errFrameShow, onCloseMessage, asyncGenerateReport = true } = props;
    onCloseMessage();
    setLoading(true);
    if (asyncGenerateReport) {
      DataExportApi.generateDynamicRptAsync(reportTemplateModel).then((response: any) => {
        onCloseDrawer();
        const { responseCode, responseDesc } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const msg = 'Report generate in progress, please download your report in [Download Report] function. Go to Download page after ';
          const sec = 2;
          errFrameShow(MessageType.SUCCESS, `${msg} ${sec} sec ...`);
          for (let i = 1; i < sec; i++) setTimeout(() => errFrameShow(MessageType.SUCCESS, `${msg} ${sec - i} sec ...`), (i * 1000));
          setTimeout(() => redirectToPage('/app/report/online', AuthenticationService, history, menuRef), (sec * 1000));
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      }).catch((error) => {
        console.log(error);
        errFrameShow(MessageType.ERROR, "Export fail.");
      }).finally(() => {
        setLoading(false);
      });
    } else {
      // TODO - plan to handle ResponseCode.SHIELD since api returns blob (not json)
      DataExportApi.generateDynamicRpt(reportTemplateModel).then((blob: any) => {
        onCloseDrawer();
        if (blob.size > 10) {
          var a = document.createElement('a');
          document.body.appendChild(a);
          var url = window.URL.createObjectURL(blob);
          a.href = url;
          a.download = 'dynamic_rpt.' + reportTemplateModel.option.output;
          a.click();
          a.remove();
          window.URL.revokeObjectURL(url);
          errFrameShow(MessageType.SUCCESS, "Export success.");
        } else {
          errFrameShow(MessageType.ERROR, "Export fail.");
        }
      }).catch((error) => {
        console.log(error);
        errFrameShow(MessageType.ERROR, "Export fail.");
      }).finally(() => {
        setLoading(false);
      });
    }
  };

  const onCloseDrawer = () => {
    setDrawerModel({ ...drawerModel, visible: false });
  };

  const onOpenDrawer = (value: any) => {
    const { shieldRef, errFrameShow, onCloseMessage } = props;
    onCloseMessage();
    setReportNode(<></>);
    const { id, type } = value;
    if (id) {
      setLoading(true);
      switch (type) {
        case ReportExportType.RPT_TMPL:
          const reportPropsModel = { id };
          ReportTemplateApi.getReportProps(reportPropsModel).then((response: any) => {
            const { responseCode, responseDesc, object } = response;
            if (ResponseCode.SUCCESS === responseCode) {
              const reportProps = { generateReport, isShowBtnExport: true, isShowBtnSave: false, reportPropsType: type, reportPropsModel: object, title: object.name, description: object.description };
              setReportNode(<ReportTemplateComponent {...reportProps} />);
              setDrawerModel({ ...drawerModel, visible: true });
              setLoading(false);
            } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
              shieldRef.current.open({ ...response });
            } else {
              errFrameShow(MessageType.ERROR, responseDesc);
            }
          });
          break;
        case ReportExportType.RPT_CATG:
          const reportCategoryModel = { id };
          ReportCategoryApi.getReportCategoryProps(reportCategoryModel).then((response: any) => {
            const { responseCode, responseDesc, object } = response;
            if (ResponseCode.SUCCESS === responseCode) {
              const { reportList = [], ...restProps } = object;
              const a = reportList.flatMap(({ advance = [] }) => advance);
              const p = reportList.flatMap(({ parameter = [] }) => parameter);
              const dataCatg = { ...restProps, advance: a, parameter: p };
              const reportProps = { generateReport, isShowBtnExport: true, isShowBtnSave: false, reportPropsType: type, reportPropsModel: dataCatg, title: object.name };
              setReportNode(<ReportTemplateComponent {...reportProps} />);
              setDrawerModel({ ...drawerModel, visible: true });
              setLoading(false);
            } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
              shieldRef.current.open({ ...response });
            } else {
              errFrameShow(MessageType.ERROR, responseDesc);
            }
          });
          break;
      }
    }
  };

  const propsDrawer = { ...drawerModel, onClose: onCloseDrawer, children: reportNode };

  const summaryColumns = [
    {
      title: 'Report Type',
      render: (cell: any) => {
        return cell.type === ReportExportType.RPT_TMPL  ? 'Single Report' : 'Group Report';
      },
      width: 120,
    },
    {
      title: "Name",
      render: (item: any) => {
        if (item.type === ReportExportType.RPT_TMPL)
          return <div style={{ cursor: "pointer" }} onClick={() => onOpenDrawer(item)} ><SnippetsOutlined className="icon-report" /> {item.name}</div>
        if (item.type === ReportExportType.RPT_CATG)
          return <div style={{ cursor: "pointer" }} onClick={() => onOpenDrawer(item)}><ReconciliationOutlined className="icon-report" /> {item.name}</div>
      },
      width: 250,
    },
    {
      title: "Report Description",
      dataIndex: "description",
      render: (cell: any) => <Paragraph
        ellipsis={{ rows: 2, expandable: true, symbol: 'more', }} >{cell} </Paragraph>,

    }
  ];
  return (
    <div className="DataExportSummaryTabWrapper">
      <Spin size="large" tip={"Loading..."} spinning={loading}>
        <Drawer {...propsDrawer} />

        <SearchBarComponent
          onSearch={onSearch}
          onReset={onReset}
          searchFieldList={searchFieldList}
        />
        <Divider />
        <Divider />
        <Divider />
        <Divider />

        <ResizableTableComponent
          className={"sysTable" + (reportTemplateList.length ? " tableTop" : "")}
          bordered
          rowKey={rowKey}
          rowSelection={false}
          dataSource={reportTemplateList}
          columns={summaryColumns}
          pagination={false}
        />


      </Spin>
    </div>
  );
};

export default DataExportSummaryTabComponent;

class DataExportSummaryTabUtils {



  static redirectToPage = (page: string, service: any, history: any, ref: any) => {
    const target = { funcId: service.getUserModel().funcId, ...{ ...service.getPathList().find((o: any) => o.funcUrl === page) } };
    const { funcId, funcUrl } = target;
    service.setFuncId(funcId);
    ref.current.click({ funcId, funcUrl });
    history.push(`/app${page}`);
  };

  static EmptyDrawerProps = () => ({
    title: '',
    width: '60%',
    placement: 'right',
    closable: false,
    onClose: () => void 0,
    visible: false,
  });
}

const { redirectToPage, EmptyDrawerProps } = DataExportSummaryTabUtils;





const rowKey = 'id';
const searchFieldList = [
  {
    label: "Name",
    key: "name",
  }
];







