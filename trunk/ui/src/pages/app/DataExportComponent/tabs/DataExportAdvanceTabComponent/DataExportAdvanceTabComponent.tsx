import React, { FunctionComponent, useState } from "react";
import { Spin } from "antd";

import "./DataExportAdvanceTabComponent.scss";
import ReportAdvanceComponent from "pages/app/ReportAdvanceComponent";
import { ReportTemplateApi } from "api";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";

type DataExportTemplateTabComponentProps = TabComponentProps;

const DataExportAdvanceTabComponent: FunctionComponent<DataExportTemplateTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState<boolean>(false);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback } = props;

  const onSave = (props: any) => {
    onCloseMessage();
    setLoading(true);
    const { reportPropsModel, callback } = props;
    ReportTemplateApi.insertRecords(reportPropsModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        onChangeCallback(TabEnum.DEFAULT_TAB);
        if (typeof callback === 'function') {
          callback();
        }
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    });
  }

  const onBack = (props: any) => {
    const { callback } = props;
    onCloseMessage();
    onChangeCallback(TabEnum.DEFAULT_TAB);
    if (typeof callback === 'function') {
      callback();
    }
  }

  const propsBuild = {
    propsBtnSave: { label: 'Save', action: onSave },
    propsBtnBack: { label: 'Cancel', action: onBack, show: false },
  };

  return (
    <div className="DataExportAdvanceTabWrapper">
      <Spin size="large" tip={"Loading..."} spinning={loading}>
      <ReportAdvanceComponent {...propsBuild} />
      </Spin>
    </div>
  );
};

export default DataExportAdvanceTabComponent;
