/* eslint-disable */
import React, { useState, FunctionComponent } from "react";
import { Divider } from "antd";

import "./DataExportComponent.scss";
import { DataExportSummaryTabComponent } from "./tabs";
import { DataExportSearchTabComponent, DataExportTemplateTabComponent, DataExportAdvanceTabComponent } from "./tabs";
import { TabEnum } from "enum";
import { PageComponentProps } from "types";
import { MessageAlertComponent, MessageAlertProps, MessageType, EmptyMessageAlertProps } from "components";

type DataExportComponentProps = PageComponentProps;

const DataExportComponent: FunctionComponent<DataExportComponentProps> = (props: any) => {
  const [message, setMessage] = useState<MessageAlertProps>(EmptyMessageAlertProps());
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const { menuRef, shieldRef, title = 'Data Export' } = props;

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());

  const propsTabSummary = { menuRef, shieldRef, errFrameShow, onCloseMessage };

  return (
    <div className="DataExportWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <DataExportSummaryTabComponent {...propsTabSummary} />
    </div>
  );
 
  /* show tab panel
  const onChangeCallback = (key: string) => setActiveKey(key);

  const propsTab = { errFrameShow, onCloseMessage, onChangeCallback };

  const panel = [
    { key: TabEnum.FIRST_TAB , tab: 'Export'        , reload: true , children: <DataExportSearchTabComponent   {...propsTab} /> },
    { key: TabEnum.SECOND_TAB, tab: 'Report Builder', reload: true , children: <DataExportTemplateTabComponent {...propsTab} /> },
    { key: TabEnum.THIRD_TAB , tab: 'Advance'       , reload: false, children: <DataExportAdvanceTabComponent  {...propsTab} /> },
  ];

  return (
    <div className="DataExportWrapper">
      {title && <span className="header">{title}</span>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <Divider />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey} onChange={onChangeCallback}>
        {panel.map(({reload, children, ...props}) => <TabPane {...props}>{(!reload || activeKey === props.key) && children}</TabPane>)}
      </Tabs>
    </div>
  );
  */
};

export default DataExportComponent;

