// @ts-nocheck
import React, { FunctionComponent, useState, useEffect } from "react";
import { Button, Divider, Form, Input, InputNumber, List, Radio, Row, Col, Spin, Switch, Select, DatePicker, Checkbox, Collapse, TimePicker } from "antd";

import "./ScheduleTabComponent.scss";
import { CronOption, CronFieldType, ExportOption, IndicatorType, MessageType, ResponseCode, TabEnum, ReportBuildType } from "enum";
import { ReportTemplateApi, ReportScheduleApi } from "api";
import { TabComponentProps } from "types";
import { ReportBuilderUtils } from "pages/app/ReportBuilderComponent/tabs";
import { exportOptionData } from "common/data/ReportBuilderData";
import { scheduleTypeList, scheduleDay, scheduleMonth, scheduleWeek, repeatEvery } from "common/data/ReportBuilderData";
import moment from 'moment';

import LzEditor from 'react-lz-editor'
const { RangePicker } = DatePicker;
const { Panel } = Collapse;
const { TextArea } = Input;


type ScheduleTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  allowDataEmpty?: boolean;
};

const ScheduleTabComponent: FunctionComponent<ScheduleTabComponentProps> = (props: any) => {
  const { allowDataEmpty = true } = props;

  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const [editingItem, setEditingItem] = useState<any>({});
  const [editingList, setEditingList] = useState<any>([]);
  const [reportScheduleList, setReportScheduleList] = useState<any>([]);
  const [isShowTableList, setShowTableList] = useState<any>(false);
  const [isEnableBtnAdd, setEnableBtnAdd] = useState<any>(false);
  const [isAllowDataEmpty] = useState<any>(allowDataEmpty);
  const [collapsedList, setCollapsedList] = useState<any>([]);
  const { title = '' } = props;
  console.log(props.pageType);


  useEffect(() => {
    const { shieldRef, errFrameShow, selectedRow: { id = 0 }, action } = props;
    if (id && action && !isShowTableList) {
      var model;
      if (props.pageType === ReportBuildType.REPORT_PAGE_TYPE_REPORT_CATEGORY) {
        model = { rptCatgId: id };
      }
      else {
        model = { rptTmplId: id };
      }

      ReportTemplateApi.getReportScheduleList(model).then((response) => {
        const { responseCode, responseDesc, object } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const reportScheduleData = [...object];
          const dateFormat = 'yyyy-MM-dd HH:mm:ss';

          const reportScheduleList = reportScheduleData.map((o: any) => ({ ...o, key: ReportBuilderUtils.random(16), between: [moment(o.startDate, dateFormat), moment(o.expiredDate, dateFormat)] }));
          setEditingKey(id);
          setEditingList(reportScheduleList);
          setReportScheduleList(reportScheduleList);
          setShowTableList(true);
          setEnableBtnAdd(true);
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      });
    }
  }, [props, isShowTableList]);

  useEffect(() => {
    if (!isAllowDataEmpty && editingList.length < 1) {
      console.log(reportScheduleList);
      add();
    }
  });

  const edit = (record: Partial<ItemProps> & { key: React.Key }) => {
    const editingItem = { ...EmptyReportScheduleProps(), ...editingList.find((o: any) => o.key === record.key) };
    setEditingItem(editingItem);
    setEnableBtnAdd(false);
  };
  const cancel = (key: React.Key) => {
    if (editingList.filter((item: ItemProps) => key === item.key).some(({ unsaved }) => unsaved)) {
      setEditingList(editingList.filter((item: any) => key !== item.key));
    }
    setEditingItem({});
    setEnableBtnAdd(true);
  };
  const add = () => {
    var editingItem;
    if (props.pageType === ReportBuildType.REPORT_PAGE_TYPE_REPORT_CATEGORY) {
      editingItem = { ...EmptyReportScheduleProps(), rptCatgId: editingKey, key: ReportBuilderUtils.random(16), unsaved: true };
    }
    else {
      editingItem = { ...EmptyReportScheduleProps(), rptTmplId: editingKey, key: ReportBuilderUtils.random(16), unsaved: true };
    }
    setEditingList([...editingList, editingItem]);
    setCollapsedList([...collapsedList, editingItem.key]);

    setEditingItem(editingItem);
    setEnableBtnAdd(false);
  };
  const remove = (record: ItemProps) => {
    setEditingList(editingList.filter((item: ItemProps) => record.key !== item.key));
  };

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, selectedRow: { savedSearch } } = props;

  const onAdd = () => {
    onCloseMessage();
    add();
  };

  const onCancel = () => {
    onCloseMessage();
    cancel(editingItem.key);
  };

  const onEdit = (value: any) => {
    onCloseMessage();
    edit(value);
  };

  const onDelete = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const { id } = value;
    const reportCategoryModel = { id };
    ReportScheduleApi.deleteRecords(reportCategoryModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Delete success.");
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
        return false;
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
        return false;
      }
    }).then((reload) => {
      if (reload) {
        remove(value);
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Delete fail.");
    }).finally(() => {
      setLoading(false);
    });
  };



  const onSave = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const { name, cronExpression, exportOption, dispSeq, activeInd, startDate, expiredDate, recipient, content, subject, emailInd } = value;


    const reportCategoryModel = { ...editingItem, name, cronExpression, exportOption, dispSeq, activeInd, startDate, expiredDate, recipient, content, subject, emailInd };
    ReportScheduleApi.insertRecords(reportCategoryModel).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
        return false;
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
        return false;
      }
    }).then((reload) => {
      if (reload) {
        var model;
        if (props.pageType === ReportBuildType.REPORT_PAGE_TYPE_REPORT_CATEGORY) {
          model = { rptCatgId: editingKey };
        }
        else {
          model = { rptTmplId: editingKey };
        }
        ReportTemplateApi.getReportScheduleList(model).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const reportScheduleData = [...object];
            const reportScheduleList = reportScheduleData.map((o: any) => ({ ...o, key: ReportBuilderUtils.random(16) }));
            setEditingList(reportScheduleList);
            setReportScheduleList(reportScheduleList);
            onUpdateRecord();
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
        setEditingItem({});
        setEnableBtnAdd(true);
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    });
  };

  const onBack = () => {
    setEditingKey(0);
    setEditingItem({});
    setEditingList([]);
    setReportScheduleList([]);
    setShowTableList(false);
    onCloseMessage();
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };


  const callback = (key) => {
    console.log(key)
    var duplicateKey = collapsedList.filter(o1 => key.filter(o2 => o2 === o1).length === 0);
    console.log(duplicateKey)
    console.log(editingItem.key)
    if (duplicateKey && duplicateKey.length > 0 && duplicateKey[0] === editingItem.key) {

    }
    else {
      setCollapsedList(key);
    }

  };

  const genExtra = (item) => (
    <div>
      {item.nextExecutionTime && <text>Next Execution Time: </text>}
      {item.nextExecutionTime && moment(item.nextExecutionTime, 'yyyy-MM-DD HH:mm:ss').format('yyyy-MM-DD HH:mm:ss')}
    </div>
  );




  return (
    <div className="ScheduleTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
        {isShowTableList && <Row gutter={24} className="row-reportContainer">
          <List style={{ width: '100%' }}
            className="row-scheduleList"
            header={<><span style={{fontSize: '20px'}} className="hdr-title">Report Name : </span><span style={{fontSize: '20px'}} className="hdr-reportName">{savedSearch}</span></>}
            bordered
            dataSource={editingList}
            renderItem={(item: any) => (
              <List.Item className="row-scheduleItem" style={{ width: '100%' }}>
                <Collapse style={{ width: '100%' }}
                  onChange={callback}
                  activeKey={collapsedList}
                >

                  <Panel
                    header={(item.name !== null && item.name.length > 0) ? item.name /*+ ' '+item.nextExecutionTime*/ : 'New Schedule'}
                    key={item.key}
                    extra={genExtra(item)}
                    style={{ width: '100%' }}

                  >
                    <ScheduleTabCardComponent {...{ item, onEdit, onDelete, onSave, onCancel, isEditing: item.key === editingItem.key }} />
                  </Panel>
                </Collapse>
              </List.Item>

            )}
          />
        </Row>}
        {isEnableBtnAdd
          ? <Button type="primary" className="btn-pageAction" children="Add" onClick={onAdd} />
          : <Button type="primary" className="btn-pageAction" children="Add" disabled />
        }
        <Button type="primary" className="btn-pageAction" children="Back" onClick={onBack} />
      </Spin>
    </div>
  );
};

export default ScheduleTabComponent;

class ScheduleTabUtils {
  static EmptyReportScheduleProps = (): ReportScheduleProps => ({
    rptTmplId: null,
    rptCatgId: null,
    name: null,
    cronExpression: CronOption.EVERY_DD_00_HH,
    exportOption: ExportOption.DEFAULT_OPTION,
    dispSeq: null,
    activeInd: IndicatorType.Y,
    activateDate: null,
    repeatEveryDay: null,
    repeatEveryWeek: null,
    repeatOnEvery: null,
    months: null,
    days: null,
    between: null,
    repeatCheckbox: null,
    repeatValue: null,
    scheduleType: null,
    onRecurEveryMonthWhichWeek: null
  });
  static getEnumKeys = (type: any) => {
    const keys = Object.keys(type).map(n => n);
    return keys.some(o => !isNaN(Number(o))) ? keys.slice(keys.length / 2, keys.length) : keys;
  };
}

const { EmptyReportScheduleProps, getEnumKeys } = ScheduleTabUtils;

type ScheduleTabCardComponentProps = {
  item: any;
  onEdit?: Function;
  onSave?: Function;
  onCancel?: Function;
  onDelete?: Function;
};

const ScheduleTabCardComponent: FunctionComponent<ScheduleTabCardComponentProps> = (props: any) => {
  const { item: { key, cronExpression, activeInd } } = props;
  const { item, isEditing } = props;
  const [form] = Form.useForm();
  const [exportOptionList] = useState<any>(exportOptionData);
  const [daily, setDaily] = useState<any>(false);
  const [weekly, setWeekly] = useState<any>(false);
  const [monthly, setMonthly] = useState<any>(false);
  /*const [isActivateTypeDay, setActivateTypeDay] = useState<any>(true);*/
  /*const [isActivateTypeWeekDay, setActivateTypeWeekDay] = useState<any>(false);*/
  const [isRepeat, setRepeat] = useState<any>(false);
  const [isSendEmail, setSendEmail] = useState<any>(false);
  const [content, setContent] = useState<any>("");

  const cronExpField = cronExpression.split(' ');
  const dateFormat = 'yyyy-MM-DD HH:mm:ss';
  const displayDateFormat = 'yyyy-MM-DD';
  const dateHourFormat = 'HH:mm';

  var date_ob = new Date();
  var date = ("0" + date_ob.getDate()).slice(-2);
  var month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  var year = date_ob.getFullYear();
  var hours = date_ob.getHours();
  var minutes = date_ob.getMinutes();
  var seconds = date_ob.getSeconds();
  var currentTime = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

  //var currentDayStart  = year + "-" + month + "-" + date + " " + "00" + ":" + "00" + ":" + "00";
  //var currentDayEnd  = year + "-" + month + "-" + date + " " + "23" + ":" + "59" + ":" + "59";

  const tmr = new Date();
  tmr.setDate(tmr.getDate() + 1);

  var tmr_date = ("0" + tmr.getDate()).slice(-2);
  var tmr_month = ("0" + (tmr.getMonth() + 1)).slice(-2);
  var tmr_year = tmr.getFullYear();
  var tmrDayEnd = `${tmr_year}-${tmr_month}-${tmr_date} 23:59:59`;

  const formFieldsValue = { ...item, between: [moment(item.startDate, dateFormat), moment(item.expiredDate, dateFormat)], ...Object.fromEntries(getEnumKeys(CronFieldType).map((o: string, n: number) => ['cronExpField_' + o, cronExpField[n]])), active: activeInd === IndicatorType.Y };
  if (formFieldsValue.startDate && formFieldsValue.expiredDate) {
    var startMoment = moment(formFieldsValue.startDate, dateFormat)
    var endMoment = moment(formFieldsValue.expiredDate, dateFormat)
    form.setFieldsValue({ between: [startMoment, endMoment] });
  }


  useEffect(() => {
    var fieldI = form.getFieldsValue().cronExpField_I + '';
    var fieldH = form.getFieldsValue().cronExpField_H + '';
    var fieldD = form.getFieldsValue().cronExpField_D + '';
    var fieldW = form.getFieldsValue().cronExpField_W + '';
    var fieldM = form.getFieldsValue().cronExpField_M + '';
    setContent(formFieldsValue.content);
    console.log(formFieldsValue.between);
    if (formFieldsValue.between && formFieldsValue.between[0].isValid()) {
      console.log(formFieldsValue.between[0].format("yyyy-MM-DD HH:mm:ss"));
      console.log(moment(formFieldsValue.between[0].format("yyyy-MM-DD HH:mm:ss"), dateFormat));
      form.setFieldsValue({ startTime: moment(formFieldsValue.between[0].format("yyyy-MM-DD HH:mm:ss"), dateFormat) });
    }
    else {

      form.setFieldsValue({ startTime: moment(currentTime, dateFormat) });
    }

    if (formFieldsValue.emailInd === 'Y') {
      setSendEmail(true);
      form.setFieldsValue({ sendByEmail: true });
    }
    else {
      setSendEmail(false);
      form.setFieldsValue({ sendByEmail: false });
    }
    form.setFieldsValue({ cotnent: CronOption.SCHEDULE_TYPE_ONE_TIME });
    var splitValues;
    if (fieldI !== '*' || fieldH !== '*') {
      if (fieldI && fieldI !== '*' && fieldI && fieldI.includes(',')) {
        form.setFieldsValue({ repeatCheckbox: true });

        splitValues = fieldI.includes(',') ? fieldI.split(',') : fieldI;
        setRepeat(true);
        if (splitValues.length > 1) {
          form.setFieldsValue({ repeatValue: splitValues[1] - splitValues[0] + '' });
        }
        else {
          form.setFieldsValue({ repeatValue: splitValues + '' });
        }

      }
      else if (fieldH && fieldH !== '*' && fieldH.includes(',')) {
        form.setFieldsValue({ repeatCheckbox: true });
        setRepeat(true);
        splitValues = fieldH.includes(',') ? fieldH.split(',') : fieldH;
        if (splitValues.length > 1) {
          form.setFieldsValue({ repeatValue: splitValues[1] - splitValues[0] + 'h' });
        }
        else {
          form.setFieldsValue({ repeatValue: splitValues + 'h' });
        }
      }
      else {
        form.setFieldsValue({ repeatCheckbox: false });
        setRepeat(false);
      }

    }
    var settedSelectType = false;
    if (fieldD !== '*' && fieldD !== '?' && fieldD && fieldD.split('/').length > 1) {
      selectType(CronOption.SCHEDULE_TYPE_DAILY);
      form.setFieldsValue({ scheduleType: CronOption.SCHEDULE_TYPE_DAILY });
      settedSelectType = true;
      var tempHourValue = fieldD.split('/')[1];
      form.setFieldsValue({ repeatEveryDay: tempHourValue });
    }

    if (fieldW !== '*' && fieldW !== '?' && fieldW) {
      if ((fieldD === "*" || fieldD === "?") && fieldM === "*") {
        selectType(CronOption.SCHEDULE_TYPE_WEEKLY);
        form.setFieldsValue({ scheduleType: CronOption.SCHEDULE_TYPE_WEEKLY });
        settedSelectType = true;

        form.setFieldsValue({ repeatOnEvery: fieldW.split(',') });

      }

    }
    if (fieldM !== '*' && fieldM) {
      selectType(CronOption.SCHEDULE_TYPE_MONTHLY);
      form.setFieldsValue({ scheduleType: CronOption.SCHEDULE_TYPE_MONTHLY });
      settedSelectType = true;
      form.setFieldsValue({ months: fieldM.split(',') });

      if (fieldD !== '*' && fieldD && fieldD.split(',').length > 0) {
        var tempDayValue = fieldD.split(',');
        form.setFieldsValue({ days: tempDayValue });
      }

    }
    if (!settedSelectType) {
      form.setFieldsValue({ scheduleType: CronOption.SCHEDULE_TYPE_ONE_TIME });
      selectType(CronOption.SCHEDULE_TYPE_ONE_TIME);
    }
    // eslint-disable-next-line
  }, [props, form]);

  const onCancel = () => {
    form.resetFields();
    props.onCancel();
  }
  const onEdit = () => props.onEdit(item);
  const onDelete = () => props.onDelete(item);
  const onReset = () => form.resetFields();
  const onFinish = () => onSave({ ...form.getFieldsValue() });
  const onSave = (formValue: any) => {
    const activeInd = form.getFieldValue('active') ? IndicatorType.Y : IndicatorType.N;
    const emailInd = form.getFieldValue('sendByEmail') ? IndicatorType.Y : IndicatorType.N;
    const value = { ...formValue, key, activeInd, emailInd };
    var startTime = form.getFieldValue('startTime');
    var hours = '';
    var minutes = '';
    if (startTime) {
      hours = startTime.format("HH");
      minutes = startTime.format("mm");
    }
    else {
      var date_obj = new Date();
      hours = date_obj.getHours();
      minutes = date_obj.getMinutes();
    }
    var remainder;
    var tmpValue;
    if (value.repeatCheckbox === true && value.repeatValue) {
      if (value.repeatValue.endsWith('h')) {
        value.cronExpField_I = minutes;
        form.setFieldsValue({ cronExpField_I: minutes });

        var hourRepeatValue = value.repeatValue.replace('h', '');
        remainder = hours % hourRepeatValue;
        tmpValue = remainder;
        var H_values = [];
        while (tmpValue < 24) {
          H_values.push(tmpValue);
          tmpValue += parseInt(hourRepeatValue);
        }

        value.cronExpField_H = H_values.join(',');
        form.setFieldsValue({ cronExpField_H: H_values.join(',') });
      }
      else {
        value.cronExpField_H = '*';
        form.setFieldsValue({ cronExpField_H: '*' });

        remainder = minutes % value.repeatValue;
        tmpValue = remainder;
        var I_values = [];
        while (tmpValue < 60) {
          I_values.push(tmpValue);
          tmpValue += parseInt(value.repeatValue);
        }
        value.cronExpField_I = I_values.join(',');
        form.setFieldsValue({ cronExpField_I: I_values.join(',') });
      }

    }
    else {
      value.cronExpField_I = minutes;
      form.setFieldsValue({ cronExpField_I: minutes });
      value.cronExpField_H = hours;
      form.setFieldsValue({ cronExpField_H: hours });

    }

    if (value.scheduleType === CronOption.SCHEDULE_TYPE_ONE_TIME) {
      value.cronExpField_D = '*';
      value.cronExpField_M = '*';
      value.cronExpField_W = '*';
      form.setFieldsValue({ cronExpField_D: '*' });
      form.setFieldsValue({ cronExpField_M: '*' });
      form.setFieldsValue({ cronExpField_W: '*' });
    }
    else if (value.scheduleType === CronOption.SCHEDULE_TYPE_DAILY) {
      value.cronExpField_D = '*/' + value.repeatEveryDay;
      value.cronExpField_M = '*';
      value.cronExpField_W = '?';
      form.setFieldsValue({ cronExpField_D: '*/' + value.repeatEveryDay });
      form.setFieldsValue({ cronExpField_M: '*' });
      form.setFieldsValue({ cronExpField_W: '?' });
    }

    else if (value.scheduleType === CronOption.SCHEDULE_TYPE_WEEKLY) {
      value.cronExpField_D = '?';
      value.cronExpField_M = '*';
      value.cronExpField_W = value.repeatOnEvery.join(',');
      form.setFieldsValue({ cronExpField_D: '?' });
      form.setFieldsValue({ cronExpField_M: '*' });
      form.setFieldsValue({ cronExpField_W: value.repeatOnEvery.join(',') });
    }

    else if (value.scheduleType === CronOption.SCHEDULE_TYPE_MONTHLY) {
      value.cronExpField_D = value.days.join(',');
      value.cronExpField_M = value.months.join(',');
      value.cronExpField_W = '?';

      form.setFieldsValue({ cronExpField_D: value.days.join(',') });
      form.setFieldsValue({ cronExpField_M: value.months.join(',') });
      form.setFieldsValue({ cronExpField_W: '?' });
    }

    if (value.cronExpField_W === '*' && value.cronExpField_D === '*') {
      value.cronExpField_W = '?';
      form.setFieldsValue({ cronExpField_W: '?' });
    }


    var cronExpression = value.cronExpField_I + ' ' + value.cronExpField_H + ' ' + value.cronExpField_D + ' ' + value.cronExpField_M + ' ' + value.cronExpField_W;
    var startDate = currentTime;
    var expiredDate = tmrDayEnd;
    if (value.between) {
      startDate = value.between[0].format("yyyy-MM-DD") + " " + hours + ":" + minutes + ":00";
      expiredDate = value.between[1].format("yyyy-MM-DD") + ' 23:59:59';
    }



    props.onSave({ ...value, cronExpression, startDate, expiredDate });
  };
  const propsFormItem = {
    ...ScheduleTabCardComponentProps,
    ...Object.fromEntries(getEnumKeys(CronFieldType).map((o: string, n: number) => ['cronExpField_' + o, { className: "item-field-item", label: n ? "" : "Cron Expression" }])),
  };
  const buttonList = [
    { editMode: true, children: "Reset", onClick: onReset },
    { editMode: true, children: "Cancel", onClick: onCancel },
    { editMode: false, children: "Edit", onClick: onEdit },
    { editMode: false, children: "Delete", onClick: onDelete },
  ];


  const { Option } = Select;
  /*const scheduleAscOrderList = scheduleAscOrder.map(function (row) { return <Option key={row.value} value={row.value}>{row.label}</Option>; });*/
  const scheduleDayList = scheduleDay.map(function (row) { return <Option key={row.value} value={row.value}>{row.label}</Option>; });
  const scheduleMonthList = scheduleMonth.map(function (row) { return <Option key={row.value} value={row.value}>{row.label}</Option>; });
  const scheduleWeekList = scheduleWeek.map(function (row) { return <Option key={row.value} value={row.value}>{row.label}</Option>; });
  const repeatEveryList = repeatEvery.map(function (row) { return <Option key={row.value} value={row.value}>{row.label}</Option>; });



  const selectType = e => {
    setDaily(false);
    setWeekly(false);
    setMonthly(false);
    if (e === CronOption.SCHEDULE_TYPE_DAILY) {
      setDaily(true);
    }
    else if (e === CronOption.SCHEDULE_TYPE_WEEKLY) {
      setWeekly(true);
    }
    else if (e === CronOption.SCHEDULE_TYPE_MONTHLY) {
      setMonthly(true);
    }
  };

  const onCheckBoxChanged = e => {
    setRepeat(e.target.checked);


  };

  const onSendByEmailChanged = e => {
    setSendEmail(e.target.checked);

  };

  const onHandleContentChange = (content: any) => {

    var lastIndex = content.lastIndexOf('\n');

    if (lastIndex === -1) {
      form.setFieldsValue({ content: content });
    }

    var beginString = content.substring(0, lastIndex);
    var endString = content.substring(lastIndex + '\n'.length);
    form.setFieldsValue({ content: beginString + '' + endString });

    // content = content.replaceLast('\n');
    // form.setFieldsValue({ content: content });
    console.log(content);
  }

  const emailValidation = (rule, value, callback) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var arr = value.split(";")
    var error = '';
    arr.forEach(tmp => {
      var valid = re.test(String(tmp).toLowerCase());
      if (!valid) {
        error += (error === '') ? tmp : ',' + tmp
      }

    });
    if (error !== '') {
      callback('Input is not valid email \n' + error);
    } else {
      callback();
    }

  }

  const intValidation = (rule, value, callback) => {
    if (Number.isInteger(Number(value)) && Number(value) > 0) {
      callback();
    } else {
      callback('Input must be integer');
    }
  }



  /* const activateDate = e => {
     setActivateTypeDay(false);
     setActivateTypeWeekDay(false);
     form.setFieldsValue({ days: [] });
     form.setFieldsValue({ on: [] });
     form.setFieldsValue({ onRecurEveryMonthWhichWeek: [] });
     if (e.target.value === 'day') {
       setActivateTypeDay(true);
     }
     else if (e.target.value === 'weekdayend') {
       setActivateTypeWeekDay(true);
     }
 
     return e;
   };
 */


  const getPropsFormItem = (name: string) => ({ ...propsFormItem[name], initialValue: formFieldsValue[name], name });
  return (
    <Form key={key} form={form} onFinish={onFinish} name="form" id={key + "_0"} className="item-form" style={{ marginTop: 5, marginBottom: 5 }} {...formItemLayout}>
      <Row gutter={24}>
        <Col span={10} >
          <Form.Item labelAlign={"left"} {...getPropsFormItem("name")} rules={[{ required: true }]}>
            {!isEditing ? <Input {...{ id: key + "_1", readOnly: true }} /> : <Input autoComplete="off" />}
          </Form.Item>
        </Col>
        <Col span={8} >
          <Form.Item labelAlign={"left"} {...getPropsFormItem("active")} valuePropName="checked">
            {!isEditing ? <Switch {...{ id: key + "_5", disabled: true }} /> : <Switch />}
          </Form.Item>
        </Col>


        <Col span={6} >
          {isEditing && <Button type="primary" className="btn-tabAction" htmlType="submit" children="Save" />}
          {buttonList.map(({ editMode, ...propsBtn }, n: number) => editMode === isEditing && <Button key={n} type="primary" className="btn-rowAction" {...propsBtn} />)}
        </Col>
      </Row>
      <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
        <Col span={0} order={1} >
          <Form.Item {...getPropsFormItem("cronExpField_I")}>
            {!isEditing ? <Input {...{ id: key + "_2_0", readOnly: true }} /> : <Input autoComplete="off" {...{ readOnly: true }} />}
          </Form.Item>
          <Form.Item {...getPropsFormItem("cronExpField_H")}>
            {!isEditing ? <Input {...{ id: key + "_2_1", readOnly: true }} /> : <Input autoComplete="off" {...{ readOnly: true }} />}
          </Form.Item>
          <Form.Item {...getPropsFormItem("cronExpField_D")}>
            {!isEditing ? <Input {...{ id: key + "_2_2", readOnly: true }} /> : <Input autoComplete="off" {...{ readOnly: true }} />}
          </Form.Item>
          <Form.Item {...getPropsFormItem("cronExpField_M")}>
            {!isEditing ? <Input {...{ id: key + "_2_3", readOnly: true }} /> : <Input autoComplete="off" {...{ readOnly: true }} />}
          </Form.Item>
          <Form.Item {...getPropsFormItem("cronExpField_W")}>
            {!isEditing ? <Input {...{ id: key + "_2_4", readOnly: true }} /> : <Input autoComplete="off" {...{ readOnly: true }} />}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
        <Col span={10}>
          <Form.Item labelAlign={"left"} {...getPropsFormItem("exportOption")} >
            {!isEditing ? <Input {...{ id: key + "_3", readOnly: true }} /> : <Radio.Group optionType="button" options={exportOptionList.map(({ label }) => label)} />}
          </Form.Item>
        </Col>

        <Col span={8} >
          <Form.Item labelAlign={"left"} {...getPropsFormItem("dispSeq")} rules={[{ required: true, message: 'Please select display sequence.' }]}>
            {!isEditing ? <Input {...{ id: key + "_4", readOnly: true }} /> : <InputNumber autoComplete="off" />}
          </Form.Item>
        </Col>
      </Row>


      <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
        <Col span={10}>
          <Form.Item name={"between"} label={"Between"} labelAlign={"left"} /*rules={[{ required: true, message: 'Please select activate date.' }]}*/ children={

            <RangePicker showTime disabled={!isEditing} defaultValue={[moment(currentTime, dateFormat), moment(tmrDayEnd, dateFormat)]} format={displayDateFormat} />

          } ></Form.Item>
        </Col>
      </Row>

      <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
        <Col span={10}>
          <Form.Item {...getPropsFormItem("startTime")} name={"startTime"} labelAlign={"left"} children={
            <TimePicker defaultValue={moment(currentTime, dateFormat)} format={dateHourFormat} disabled={!isEditing} />
          } ></Form.Item>
        </Col>
      </Row>

      {
        <div>
          <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
            <Col span={10} >
              <Form.Item name={"repeatCheckbox"} label={"Repeat"} labelAlign={"left"} valuePropName='checked' children={
                <Checkbox
                  onChange={onCheckBoxChanged}
                  disabled={!isEditing}
                >

                </Checkbox>
              }></Form.Item>
            </Col>
          </Row>
          {isRepeat &&
            <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
              <Col span={10}>


                <Form.Item name={"repeatValue"} label={"Repeat every"} labelAlign={"left"} rules={[{ required: true }]} children={
                  <Select
                    style={{ width: 240 }}
                    placeholder="Please select"
                    disabled={!isEditing || !isRepeat}
                  >
                    {repeatEveryList}
                  </Select>

                }
                ></Form.Item>

              </Col>
            </Row>
          }



          <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
            <Col span={10}>
              <Form.Item
                name={"scheduleType"}
                label={"Schedule Type"}
                labelAlign={"left"}
                rules={[{ required: true, message: 'Please select Schedule Type.' }]}

              >
                <Select
                  placeholder="Parameter Name"
                  allowClear
                  onChange={selectType}
                  disabled={!isEditing}
                >
                  {scheduleTypeList.map((item) => <option key={item.scheduleType} value={item.scheduleType}>{item.shortDesc}</option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>




          <Row gutter={24} style={{ marginLeft: 15 }}>
            <Col span={10}>


              {daily &&
                <div>
                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                  <Form.Item labelAlign={"left"} {...getPropsFormItem("repeatEveryDay")} name={"repeatEveryDay"} initialValue={"1"} type={"number"} rules={[{ required: true, message: 'Day please input' }, { validator: intValidation }]} >
                    <Input autoComplete="off" suffix="days" disabled={!isEditing} />
                  </Form.Item>
                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                </div>

              }
              {weekly &&
                <div>
                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                  {/* <Form.Item {...getPropsFormItem("repeatEveryWeek" style={{ marginTop: 5, marginBottom: 5 }})}><InputNumber autoComplete="off" /> weeks on:
             </Form.Item> */}
                  <Form.Item {...getPropsFormItem("repeatOnEvery")} rules={[{ required: true }]}>
                    <Select
                      mode="multiple"
                      allowClear
                      style={{ width: '100%' }}
                      placeholder="Please select"
                      disabled={!isEditing}
                    >
                      {scheduleWeekList}
                    </Select>
                  </Form.Item>
                  <br />
                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                </div>

              }
              {monthly &&

                <div>
                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />



                  <Form.Item {...getPropsFormItem("months")} labelAlign={"left"} style={{ marginTop: 5, marginBottom: 5 }} rules={[{ required: true }]}>
                    <Select
                      mode="multiple"
                      allowClear
                      placeholder="Please select"
                      disabled={!isEditing}
                    >
                      {scheduleMonthList}
                    </Select>
                  </Form.Item>

                  {/*<Form.Item name="activateDate" label="Activate Date" labelAlign={"left"} initialValue={exportOptionDefault} style={{ marginTop: 5, marginBottom: 5 }} >
                    <Radio.Group defaultValue={exportOptionDefault} onChange={(event) => activateDate(event)} disabled={!isEditing} >
                      <Radio.Button value="day">Day</Radio.Button>
                      <Radio.Button value="weekdayend">Specific Weekday / Weekend</Radio.Button>
                    </Radio.Group>

                  </Form.Item>
                  */
                  }
                  { /*isActivateTypeDay &&*/
                    <Form.Item {...getPropsFormItem("days")} labelAlign={"left"} style={{ marginTop: 5, marginBottom: 5 }} rules={[{ required: true }]}>
                      <Select
                        mode="multiple"
                        allowClear
                        placeholder="Please select"
                        disabled={!isEditing}
                      >
                        {scheduleDayList}
                      </Select>
                    </Form.Item>
                  }
                  { /*isActivateTypeWeekDay &&
                    <div>
                      <Form.Item {...getPropsFormItem("on")} labelAlign={"left"} style={{ marginTop: 5, marginBottom: 5 }}>
                        <Select
                          mode="multiple"
                          allowClear
                          placeholder="Please select"
                          disabled={!isEditing}
                        >
                          {scheduleAscOrderList}
                        </Select>
                      </Form.Item>
                      <Form.Item name={"onRecurEveryMonthWhichWeek"} labelAlign={"left"} style={{ marginTop: 5, marginBottom: 5 }}>
                        <Select
                          mode="multiple"
                          allowClear
                          placeholder="Please select"
                          disabled={!isEditing}
                        >
                          {scheduleWeekList}
                        </Select>
                      </Form.Item>
                    </div>
                    */
                  }

                  <Divider style={{ marginTop: 15, marginBottom: 15 }} />
                </div>


              }
            </Col>
          </Row>

          <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
            <Col span={10} >
              <Form.Item {...getPropsFormItem("sendByEmail")} name={"sendByEmail"} labelAlign={"left"} valuePropName='checked' children={
                <Checkbox
                  onChange={onSendByEmailChanged}
                  disabled={!isEditing}
                >

                </Checkbox>
              }></Form.Item>
            </Col>
          </Row>
          {isSendEmail &&
            <div >
              <Divider style={{ marginTop: 15, marginBottom: 15 }} />
              <div style={{ marginLeft: 15 }}>
                <div>




                  <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
                    <Col span={10}>
                      <Form.Item
                        {...getPropsFormItem("subject")}
                        name={"subject"}
                        label={"Subject"}
                        labelAlign={"left"}
                        rules={[{ required: true, message: 'Please input email subject.' }]}
                      >
                       <TextArea disabled={!isEditing} placeholder="Remark" rows={2} maxLength={400} />
                      </Form.Item>
                    </Col>
                  </Row>


                  <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
                    <Col span={10}>
                      <Form.Item
                        {...getPropsFormItem("recipient")}
                        name={"recipient"}
                        label={"Recipient"}
                        labelAlign={"left"}
                        rules={[{ required: true, message: 'Please input email recipient.' }, { validator: emailValidation }]}
                      >
                        <Input disabled={!isEditing} autoComplete="off"  />
                      </Form.Item>
                    </Col>


                  </Row>


                  <Row gutter={24} style={{ marginTop: 5, marginBottom: 5 }}>
                    <Col span={10}>
                      <Form.Item
                        {...getPropsFormItem("content")}
                        name={"content"}
                        label={"Content"}
                        labelAlign={"left"}
                        rules={[{ required: true, message: 'Please input email content.' }]}

                      >
                        <div >
                          <LzEditor
                            disabled={!isEditing}
                            active={true}
                            image={false}
                            video={false}
                            audio={false}
                            urls={false}
                            autoSave={false}
                            fullScreen={false}
                            color={false}
                            pasteNoStyle={false}
                            convertFormat="markdown"
                            cbReceiver={onHandleContentChange}
                            importContent={content}
                          />
                        </div>
                      </Form.Item>
                    </Col>
                  </Row>
                </div>

              </div>
              <Divider style={{ marginTop: 15, marginBottom: 15 }} />
            </div>
          }

        </div>
      }

    </Form>
  );
};





const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
  },
};

const ScheduleTabCardComponentProps = {
  name: { className: "item-field", label: "Schedule Name" },
  cronExpression: { className: "item-field", label: "Cron Expression" },
  exportOption: { className: "item-field", label: "Export Option" },
  dispSeq: { className: "item-field", label: "Display Seq" },
  active: { className: "item-field", label: "Active" },
  repeatEveryDay: { className: "item-field", label: "Recur every" },
  repeatEveryWeek: { className: "item-field", label: "Recur every" },
  repeatOnEvery: { className: "item-field", label: "Repeat on every" },
  months: { className: "item-field", label: "Months" },
  days: { className: "item-field", label: "Days" },
  on: { className: "item-field", label: "On:" },
  every: { className: "item-field", label: "Every:" },
  subject: { className: "item-field", label: "Subject" },
  content: { className: "item-field", label: "Content" },
  recipient: { className: "item-field", label: "Recipient" },
  sendByEmail: { className: "item-field", label: "Send by email" },
  startTime: { className: "item-field", label: "Start Time" },
};

interface ReportScheduleProps {
  id?: number;
  rptTmplId: number | null;
  rptCatgId: number | null;
  name: string | null;
  cronExpression: string | null;
  exportOption: string;
  dispSeq: number | null;
  activeInd: IndicatorType;
  activateDate: any | null;
  repeatEveryDay: any | null;
  repeatEveryWeek: any | null;
  repeatOnEvery: any | null;
  months: any | null;
  days: any | null;
  between: any | null;
  repeatCheckbox: any | null;
  repeatValue: any | null;
  scheduleType: any | null;
  onRecurEveryMonthWhichWeek: any | null;
}

interface ItemProps extends ReportScheduleProps {
  key: string;
}
