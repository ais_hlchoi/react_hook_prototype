export default interface ReportTemplateModel {
  file?: { id: any, label: string };
  basic: {
    select: [];
    from: [];
    where: [];
    order: [];
  };
  advance: {
    sql: string;
  },
  option: {
    parameter: [];
    output?: string;
    layout?: string;
    type?: string;
  },
  name?: string;
  id?: any;
}
