import React, { FunctionComponent, useState, useEffect } from "react";
import { Descriptions, Divider, Form, Input, InputNumber, Select, Radio, DatePicker, List, Button } from "antd";
import moment from "moment";

import "./ReportTemplateComponent.scss";
import ReportTemplateModel from "./ReportTemplateModel";
import { DateFormat, ExportOption, ReportExportType, ReportParamType } from "enum";
import { PageComponentProps } from "types";
import { ReportBuilderProps, ReportBuilderPropsUtils, ReportBuilderUtils, ReportBuilderQueryUtils } from "pages/app/ReportBuilderComponent/tabs";
import { exportOptionData } from "common/data/ReportBuilderData";

type ReportTemplateComponentProps = PageComponentProps & {
  isShowBtnExport?: boolean;
  reportPropsModel?: ReportBuilderProps & { sql: string };
  reportPropsType?: ReportExportType;
  generateReport?: Function;
};

const ReportTemplateComponent: FunctionComponent<ReportTemplateComponentProps> = (props: any) => {
  const [form] = Form.useForm();
  const [reportPropsModel, setReportPropsModel] = useState<ReportBuilderProps>({ ...EmptyReportProps(), ...props.reportPropsModel });
  const [reportPropsType, setReportPropsType] = useState<ReportExportType>(ReportExportType.RPT_TMPL);
  const [exportOption, setExportOption] = useState<string>(ExportOption.DEFAULT_OPTION);
  const [exportOptionDefault, setExportOptionDefault] = useState<string>(ExportOption.DEFAULT_OPTION);
  const [exportOptionList, setExportOptionList] = useState<any>([]);
  const [isShowBtnExport, setShowBtnExport] = useState<any>(true);
  const [isShowBtnReset, setShowBtnReset] = useState<any>(false);
  const { title = '' } = props;
  const { description = '' } = props;

  const { table, column, filter, sort, advance, sql, parameter } = { ...EmptyReportExportProps(), ...reportPropsModel };
  const isShowDivTable     = false && ReportBuilderUtils.isDirty(table);
  const isShowDivFilter    = false && ReportBuilderUtils.isDirty(filter);
  const isShowDivSort      = false && ReportBuilderUtils.isDirty(sort);
  const isShowDivAdvance   = ReportBuilderUtils.isDirty(advance);
  const isShowDivParameter = ReportBuilderUtils.isDirty(parameter);
  const isShowDivAdvSql    = false && ReportBuilderUtils.isDirty(sql);

  useEffect(() => {
    const { isShowBtnExport = false, reportPropsModel = {}, reportPropsType = ReportExportType.RPT_TMPL } = props;
    setShowBtnExport(isShowBtnExport);
    setReportPropsType(reportPropsType);

    const options: any = [...exportOptionData];
    setExportOptionList(options);
    if (!options.some((option: any) => option.label === ExportOption.DEFAULT_OPTION)) {
      const [option = { label: '' },] = options;
      setExportOptionDefault(option.label);
    }

    const reportPropsModelEdited = { ...EmptyReportProps(), ...reportPropsModel };
    setShowBtnReset(reportPropsModelEdited.advance.filter(({mandatory}) => mandatory || false).length > 0);
  }, [props]);

  useEffect(() => {
    const a = Object.fromEntries((advance   || []).map((_o: any, n: number) => [ `a_${n}`, form.getFieldValue(`a_${n}`) ]));
    const p = Object.fromEntries((parameter || []).map((_o: any, n: number) => [ `p_${n}`, form.getFieldValue(`p_${n}`) ]));
    form.setFieldsValue({ ...a, ...p, exportOption });
  }, [form, advance, parameter, exportOption]);

  const onClickExport = (_event: any) => {
    const { exportOption, ...restProps } = form.getFieldsValue();
    const { advance, parameter = [] } = reportPropsModel;
    const a = mergeFieldValues(advance  , Object.entries(restProps).filter(([k]) => k.substr(0, 1) === 'a').map(([,v]) => v));
    const p = mergeFieldValues(parameter, Object.entries(restProps).filter(([k]) => k.substr(0, 1) === 'p').map(([,v]) => v));
    const { generateReport = () => {} } = props;
    const reportPropsModelEdited = { ...reportPropsModel, advance: a, parameter: p };
    const isEnableBtnExport = assertNotNull(reportPropsModelEdited);
    setReportPropsModel(reportPropsModelEdited);
    setExportOption(exportOption);
    if (!isEnableBtnExport) {
      form.validateFields();
      return false;
    }
    switch (reportPropsType) {
      case ReportExportType.RPT_TMPL:
        generateReport(convertToReportTemplateModel({ ...reportPropsModelEdited, output: exportOption, type: reportPropsType }));
        break;
      case ReportExportType.RPT_CATG:
        generateReport(convertToReportCategoryModel({ ...reportPropsModelEdited, output: exportOption, type: reportPropsType }));
        break;
    }
  };

  const onClickReset = (_event: any) => {
    const { advance, parameter = [] } = reportPropsModel;
    advance.forEach((o: any) => o.hasOwnProperty('value') ? delete o['value'] : void 0);
    parameter.forEach((o: any) => o.hasOwnProperty('value') ? delete o['value'] : void 0);
    const reportPropsModelEdited = { ...reportPropsModel, advance, parameter };
    setReportPropsModel(reportPropsModelEdited);
    form.resetFields();
  };

  return (
    <div className="ReportTemplateWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      {description && <><span className="label-value">{description}</span><Divider /></>}
      {isShowDivTable && <>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Selected Table Name" children={getSelectedTable(table)} />
        <Descriptions.Item label="Selected Column List" children={getSelectedColumn(table, column)} />
      </Descriptions>
      </>}
      {isShowDivFilter && <>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Created Filter" children={getCreatedFilter(filter)} />
      </Descriptions>
      </>}
      {isShowDivSort && <>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Sorting" children={getCreatedSort(sort)} />
      </Descriptions>
      </>}
      <Form form={form} name="form">
      {(isShowDivAdvance || isShowDivParameter) && <>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Report Criteria" className="editing" children="" />
      </Descriptions>
      {isShowDivAdvance &&
      <List className="list-advance" itemLayout="horizontal" dataSource={advance} renderItem={(a: any, n: number) => (
        <List.Item>
          <List.Item.Meta description={
            <div key={n} className="row-advance">
              <span className="label-mandatory">{ReportBuilderUtils.isTrue(a.mandatory) ? '*' : ''}</span>
              <span className="label-table-column">{a.table.children}.{a.column.children}</span>
              <span className="label-comparator">{a.comparator.children}</span>
              <span className="label-value">
                <Form.Item name={`a_${n}`} rules={[{ required: ReportBuilderUtils.isTrue(a.mandatory), message: `Please input ${a.table.children}.${a.column.children}.` }]} children={<Input autoComplete="off" />} />
              </span>
            </div>
          } />
        </List.Item>
      )} />
      }
      {isShowDivParameter &&
      <List className="list-parameter" itemLayout="horizontal" dataSource={parameter} renderItem={(a: any, n: number) => (
        <List.Item>
          <List.Item.Meta description={
            <div key={n} className="row-advance">
              <span className="label-mandatory">{ReportBuilderUtils.isTrue(a.mandatory) ? '*' : ''}</span>
              <span className="label-table-column">{a.label}</span>
              <span className="label-comparator">{a.comparator.children}</span>
              <span className="label-value">
                {/*case date:*/
                a.datatype === ReportParamType.DATE ?
                <Form.Item name={`p_${n}`} rules={[{ required: ReportBuilderUtils.isTrue(a.mandatory), message: `Please input ${a.label}.` }]} children={<DatePicker />} />
                :/*case number:*/
                a.datatype === ReportParamType.NUMBER ?
                <Form.Item name={`p_${n}`} rules={[{ required: ReportBuilderUtils.isTrue(a.mandatory), message: `Please input ${a.label}.` }]} children={<InputNumber />} />
                :/*case array:*/
                a.datatype === ReportParamType.ARRAY ?
                <Form.Item name={`p_${n}`} rules={[{ required: ReportBuilderUtils.isTrue(a.mandatory), message: `Please input ${a.label}.` }]} children={<Select children={(a.option || []).map(({ value, children }, i: number) => <Select.Option {...{ value, children, key: `${n}_${i}` }} />)} />} />
                :/*default:*/
                <Form.Item name={`p_${n}`} rules={[{ required: ReportBuilderUtils.isTrue(a.mandatory), message: `Please input ${a.label}.` }]} children={<Input autoComplete="off" />} />
                /*}*/}
              </span>
            </div>
          } />
        </List.Item>
      )} />
      }
      </>}
      {isShowDivAdvSql && <>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Advance Sql" children={sql} />
      </Descriptions>
      </>}
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Export Option" className="editing" children="" />
      </Descriptions>
      <Form.Item name="exportOption" initialValue={exportOptionDefault}>
      <Radio.Group optionType="button" options={exportOptionList.map(({label}) => label)} />
      </Form.Item>
      </Form>
      <Divider />
      {isShowBtnExport && <Button type="primary" className="btn-pageAction" children="Export" onClick={onClickExport} />}
      {isShowBtnReset  && <Button type="primary" className="btn-pageAction" children="Reset"  onClick={onClickReset} />}
    </div>
  );
};

export default ReportTemplateComponent;

class ReportTemplateUtils {
  static convertToReportTemplateModel = (reportProps: ReportBuilderProps & { type: ReportExportType }): ReportTemplateModel => {
    const { table, column, filter, sort, advance, sql, parameter, output, layout, name, type, id } = { ...ReportBuilderPropsUtils.EmptyReportExportProps(), ...reportProps };
    const isFilter = ({mandatory, value}) => (mandatory === 'Y') || (mandatory !== 'Y' && ReportBuilderUtils.isDirty(value));
    const filterAdvance = advance.filter((o: any) => isFilter(o)).map((o: any, n: number) => ({ value: `and ${ReportBuilderQueryUtils.parse(o)}`, seq: n + filter.length }));
    const parameterValue = parameter?.map(({ text, datatype, value }) => ({ text, datatype, value }));
    const model = ReportTemplateUtils.EmptyReportTemplateModel();
    Object.assign(model, {
      basic: {
        select: column,
        from: table,
        where: [...filter, ...filterAdvance],
        order: sort,
      },
      advance: {
        sql,
      },
      option: {
        parameter: parameterValue,
        output,
        layout,
        type,
      },
      name,
      id,
    });
    return model;
  };

  static EmptyReportTemplateModel = (): ReportTemplateModel => ({
    basic: {
      select: [],
      from: [],
      where: [],
      order: [],
    },
    advance: {
      sql: '',
    },
    option: {
      parameter: [],
    },
  });

  static convertToReportCategoryModel = (reportProps: any) => {
    const { advance = [], parameter = [], output, name, type, id } = reportProps;
    const a = advance.map(({ table, column, datatype = ReportParamType.STRING, value }) => ({ text: `&{${table.children}.${column.children}}`, datatype, value }));
    const p = parameter.map(({ text, datatype, value }) => ({ text, datatype, value }));
    const model = ReportTemplateUtils.EmptyReportTemplateModel();
    Object.assign(model, {
      option: {
        parameter: a.concat(p),
        output,
        type,
      },
      name,
      id,
    });
    return model;
  };

  static mergeFieldValues = (list: any, arr: any) => {
    const getValue = (datatype: string, value: any) => datatype === ReportParamType.DATE ? moment(value).format(DateFormat.DATE) : value;
    return list.map((o: any, n: number) => ({ ...o, value: getValue(o.datatype, arr[n]) }));
  };

  static assertNotNull = (model: any): boolean => {
    const { advance, parameter } = model;
    const hasEmptyFields = (list: any) => list.filter(({mandatory}) => ReportBuilderUtils.isTrue(mandatory)).some(({value}) => ReportBuilderUtils.isEmpty(value));
    return !hasEmptyFields(advance) && !hasEmptyFields(parameter);
  };
}

const { EmptyReportProps, EmptyReportExportProps } = ReportBuilderPropsUtils;
const { getSelectedTable, getSelectedColumn, getCreatedFilter, getCreatedSort } = ReportBuilderPropsUtils;
const { convertToReportTemplateModel, convertToReportCategoryModel, mergeFieldValues, assertNotNull } = ReportTemplateUtils;
