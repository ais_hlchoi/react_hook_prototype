import React, { FunctionComponent, useState, useEffect } from "react";
import { Form, Descriptions, Divider, Input, Radio, Button, Row, Col } from "antd";

import "./ReportAdvanceComponent.scss";
import { LayoutOption, GenerateOption, ReportBuildType } from "enum";
import { PageComponentProps } from "types";
import { ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { CommonUtils } from "common/utils";
import { layoutOptionData, generateTypeData } from "common/data/ReportBuilderData";

type ReportAdvanceComponentProps = PageComponentProps & {
  propsBtnSave?: ReportAdvanceBtnProps;
  propsBtnReset?: ReportAdvanceBtnProps;
  propsBtnBack?: ReportAdvanceBtnProps;
  editingItem?: any;
};

const ReportAdvanceComponent: FunctionComponent<ReportAdvanceComponentProps> = (props: any) => {
  const [form] = Form.useForm();
  const [formDefault, setFormDefault] = useState<any>();
  const [layoutOptionDefault, setLayoutOptionDefault] = useState<string>(LayoutOption.DEFAULT_OPTION);
  const [layoutOptionList, setLayoutOptionList] = useState<any>([]);
  const [generateOptionDefault, setGenerateOptionDefault] = useState<string>(GenerateOption.DEFAULT_OPTION);
  const [generateOptionList, setGenerateOptionList] = useState<any>([]);
  const [isNotEmptyDefault, setNotEmptyDefault] = useState<boolean>(false);
  const [isEnableBtnSave, setEnableBtnSave] = useState<boolean>(false);
  const { title = '' } = props;

  useEffect(() => {
    const options: any = [...layoutOptionData];
    setLayoutOptionList(options);
    if (!options.some((option: any) => option.value === LayoutOption.DEFAULT_OPTION)) {
      const [option = { value: '' },] = options;
      setLayoutOptionDefault(option.value);
    }
  }, [props]);

  useEffect(() => {
    const options: any = [...generateTypeData];
    setGenerateOptionList(options);
    if (!options.some((option: any) => option.value === GenerateOption.DEFAULT_OPTION)) {
      const [option = { value: '' },] = options;
      setGenerateOptionDefault(option.value);
    }
  }, [props]);

  useEffect(() => {
    const { editingItem } = props;
    const { id = '' } = { ...editingItem };
    if (id) {
      form.setFieldsValue(editingItem);
      setFormDefault(editingItem);
      setNotEmptyDefault(true);
    }
  }, [props, form]);

  const onChangeCompleteInput = (_event: any) => {
    const mand = [ 'sql', 'layout', 'name', 'generateOpt' ];
    setEnableBtnSave(checkBtnDoneEnabled(Object.entries(form.getFieldsValue()).filter(([k]) => mand.includes(k)).map(([, v]) => v)));
  };

  const onReset = () => {
    if (isNotEmptyDefault) {
      form.setFieldsValue(formDefault);
      setEnableBtnSave(false);
    } else {
      form.resetFields();
      setEnableBtnSave(false);
    }
  };

  const { propsBtnSave = {}, propsBtnReset = {}, propsBtnBack = {} } = props;
  Object.assign(propsBtnSave,  { callback: onReset, disabled: !isEnableBtnSave });
  Object.assign(propsBtnReset, { callback: onReset });
  Object.assign(propsBtnBack,  { callback: onReset });

  return (
    <div className="ReportAdvanceWrapper">
      {title && <span className="header">{title}</span>}
      <Form form={form} name="form">
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Report Name" className="editing" children="" />
      </Descriptions>
      <Form.Item name="name">
        <Input className="input-reportName" placeholder="Name" maxLength={100} onChange={onChangeCompleteInput} autoComplete="off" />
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Report Description" className="editing" children="" />
      </Descriptions>
      <Form.Item name="description">
        <Input.TextArea className="input-reportDescription" placeholder="Description" maxLength={200} onChange={onChangeCompleteInput} autoComplete="off" autoSize={{ minRows: 3, maxRows: 50 }} />
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Query" className="editing" children="" />
      </Descriptions>
      <Form.Item name="sql">
        <Input.TextArea className="input-sql" placeholder="Query" onChange={onChangeCompleteInput} autoSize={{ minRows: 3, maxRows: 50 }} maxLength={4000}/>
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Horizontal Layout / Vertical Layout" className="editing" children="" />
      </Descriptions>
      <Form.Item name="layout" initialValue={layoutOptionDefault}>
        <Radio.Group optionType="button" options={layoutOptionList.map(({label, value}) => ({label, value}))} onChange={(event) => onChangeCompleteInput(event)} />
      </Form.Item>
      <Descriptions bordered={false} column={1} size="small">
        <Descriptions.Item label="Generate Type" className="editing" children="" />
      </Descriptions>
      <Form.Item name="generateOpt" initialValue={generateOptionDefault}>
        <Radio.Group optionType="button" options={generateOptionList.map(({label, value}) => ({label, value}))} onChange={(event) => onChangeCompleteInput(event)} />
      </Form.Item>
      <Form.Item name="id">
        <Input type="hidden" />
      </Form.Item>
      <Form.Item name="lastUpdatedTime">
        <Input type="hidden" />
      </Form.Item>
      <span>Note*:<br/></span>
      <span className="note">            
            <u>Customized Variable Definition:</u><br/>
            e.g SELECT * FROM TABLE_NAME WHERE FILED_NAME <i>${"{"}code:label:datatype{"}"}</i><br/>
            <b>${"{"}code:label:datatype{"}"}</b><br/>
            code: Unique identifier for the variable and troubleshoot purpose<br/>
            label: Variable label shown in Report Template<br/>
            datatype: Various datatype<br/>
            &nbsp;&nbsp;<i>string / number / date / array "[]" / ${"{"}[:(segment):]{"}"}</i><br/>
            For the array, it is used to configure an option list for user to select<br/>
            &nbsp;&nbsp;&nbsp;[]:  [Y|N]   e.g SELECT * FROM TABLE_NAME WHERE ACTIVE_IND = <i>${"{"}activeInd:Active Indicator:[Y|N]{"}"}</i><br/>
       </span>
      </Form>
      <Divider />
      <div className="tab-action">
        <Row>
          <Col span={23}>
            <ButtonReset {...{ ...props, propsBtnReset }} />
            <ButtonBack  {...{ ...props, propsBtnBack  }} />
          </Col>
          <Col span={1}>
            <ButtonSave  {...{ ...props, propsBtnSave, form }} />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default ReportAdvanceComponent;

class ReportAdvanceUtils {
  static checkBtnDoneEnabled = (arr: any[]) => arr.every(o => CommonUtils.isDirty(o));
}

const { EmptyReportProps } = ReportBuilderPropsUtils;
const { checkBtnDoneEnabled } = ReportAdvanceUtils;

type ReportAdvanceBtnProps = {
  label?: string;
  href?: string;
  action?: Function;
  callback?: Function;
  show?: boolean;
  disabled?: boolean;
};

const ButtonReset: FunctionComponent<ReportAdvanceBtnProps> = (props: any) => {
  const { propsBtnReset = {} } = props;
  const { label = 'Reset', href, action, callback, show = true, disabled = false } = propsBtnReset;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (CommonUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonSave: FunctionComponent<ReportAdvanceBtnProps> = (props: any) => {
  const { propsBtnSave = {}, form } = props;
  const { label = 'Save', href, action, callback, show = true, disabled = false } = propsBtnSave;
  const buildType = ReportBuildType.REPORT_ADVANCE;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (CommonUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback, reportPropsModel: { ...EmptyReportProps(), ...form.getFieldsValue(), buildType } }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};

const ButtonBack: FunctionComponent<ReportAdvanceBtnProps> = (props: any) => {
  const { propsBtnBack = {} } = props;
  const { label = 'Back', href, action, callback, show = false, disabled = false } = propsBtnBack;
  if (show) {
    if (disabled)
      return <Button type="primary" className="btn-tabAction" children={label} disabled />;
    const propsBtn = { onClick: callback };
    if (CommonUtils.isDirty(href)) {
      Object.assign(propsBtn, {
        href: '#/app' + href,
      });
    } else if (action) {
      Object.assign(propsBtn, {
        onClick: () => action({ callback }),
      });
    }
    return <Button type="primary" className="btn-tabAction" children={label} {...propsBtn} />;
  }
  return null;
};
