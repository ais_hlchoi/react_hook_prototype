import React, { FunctionComponent, useState, useRef } from "react";
import { Divider, Tabs } from "antd";

import "./SavedSearchComponent.scss";
import { SavedSearchAccessTabComponent, SavedSearchDetailTabComponent, SavedSearchSummaryTabComponent } from "./tabs";
import { SavedSearchScheduleTabComponent} from "pages/app/ScheduleComponent/tabs";
import { MessageAlertComponent, EmptyMessageAlertProps } from "components";
import { AuditModelComponent } from "components";
import { MessageType, ReportBuildType, TabEnum , } from "enum";
import { PageComponentProps } from "types";
import { UseEffectUtils } from "common/utils";
import { common } from "shared/common";

type SavedSearchComponentProps = PageComponentProps;

const SavedSearchComponent: FunctionComponent<SavedSearchComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const [action, setAction] = useState<string>('');
  const [isEditing, setEditing] = useState<boolean>(false);
  const [selectedRow, setSelectedRow] = useState<any>({});
  const [isReloadSummaryTab] = useState<any>([false]);
  const [isEnableScheduleTab] =  useState<any>([true]);
  const { shieldRef, componentName = 'SavedSearchComponent', title = 'Saved Search' } = props;

  const SavedSearchSummaryTabComponentRef: any = useRef();

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.DEFAULT:
        setActiveKey(TabEnum.DEFAULT_TAB);
        setEditing(false);
        if (isReloadSummaryTab[0]) {
          isReloadSummaryTab[0] = false;
          SavedSearchSummaryTabComponentRef.current.search();
        }
        break;
      case TabEnum.ADD:
        setActiveKey(TabEnum.SECOND_TAB);
        setAction(action);
        setSelectedRow({});
        setEditing(true);
        resetSkip(componentName);
        break;
      case TabEnum.EDIT:
        if (common.isNotEmpty(selectedRow)) {
          setActiveKey(TabEnum.SECOND_TAB);
          setAction(action);
          setSelectedRow(selectedRow);
          setEditing(true);
          resetSkip(componentName);
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.DELETE:
        setEditing(false);
        break;
      case TabEnum.AUDIT:
        if (common.isNotEmpty(selectedRow)) {
          setAuditModel({ ...auditModel, show: true });
        } else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.EXPORT:
        common.downloadExcel(props.data, props.columns, 'Saved_Search_');
        setEditing(false);
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setSelectedRow(model);
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setSelectedRow(model);
    setAuditModel({ ...auditModel, model });
  };

  const onChangeCallback = (key: any) => {
    onCloseMessage();
    if (common.isNotEmpty(key)) {
      if (key !== TabEnum.DEFAULT_TAB) {
        setActiveKey(key);
      } else {
        onSelectClear();
        setActiveKey(TabEnum.DEFAULT_TAB);
        setAction('');
        setEditing(false);
        if (isReloadSummaryTab[0]) {
          isReloadSummaryTab[0] = false;
          SavedSearchSummaryTabComponentRef.current.search();
          SavedSearchSummaryTabComponentRef.current.updateSuccess("Save success.");
        }
      }
    }
  };

  const scheduleEnableCallback = (flag: boolean) => {
    isEnableScheduleTab[0] = flag;
  };

  const onUpdateRecord = (props: any) => {
    const { reload = true } = { ...props };
    isReloadSummaryTab[0] = reload;
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const groupTitle = () => {
    const list: any = [];
    if (common.isNotEmpty(title))
      list.push(title);
    if (action === TabEnum.ADD)
      list.push('(New)');
    if (action === TabEnum.EDIT)
      list.push('(Amendment)');
    return list.join(' ');
  };

  const { isReportAdvance = false } = props;
  const propsTabSummary = { shieldRef, componentName, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange, searchRef: SavedSearchSummaryTabComponentRef, isReportAdvance };
  const propsTabDetail  = { shieldRef, componentName, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, onSearch: () => SavedSearchSummaryTabComponentRef.current.search(), action, selectedRow, isReportAdvance, scheduleEnableCallback,pageType:ReportBuildType.REPORT_PAGE_TYPE_SAVE_SEARCH };

  return (
    <div className="SavedSearchWrapper">
      {title && <><span className="header">{groupTitle()}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey} onChange={onChangeCallback}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
          <SavedSearchSummaryTabComponent {...propsTabSummary} />
        </TabPane>
        {isEditing && <>
        {action === TabEnum.ADD && <>
        {<TabPane tab="Report Builder" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <SavedSearchDetailTabComponent {...propsTabDetail} reportBuildType={isReportAdvance ? ReportBuildType.REPORT_ADVANCE : ReportBuildType.REPORT_BUILDER} />}
        </TabPane>
        }
        </>}
        {action === TabEnum.EDIT && <>
        <TabPane tab="Detail" key={TabEnum.SECOND_TAB}>
          {activeKey === TabEnum.SECOND_TAB && <SavedSearchDetailTabComponent {...propsTabDetail} />}
        </TabPane>
        <TabPane tab="Role Group" key={TabEnum.THIRD_TAB}>
          {activeKey === TabEnum.THIRD_TAB && <SavedSearchAccessTabComponent {...propsTabDetail} />}
        </TabPane>
        {isEnableScheduleTab[0] && <TabPane tab="Scheduler" key={TabEnum.FORTH_TAB}>
          {activeKey === TabEnum.FORTH_TAB && <SavedSearchScheduleTabComponent {...propsTabDetail} />}
        </TabPane>
        }
        </>}
        </>}
      </Tabs>
    </div>
  );
};

export default SavedSearchComponent;

const { resetSkip } = UseEffectUtils;
const { TabPane } = Tabs;
