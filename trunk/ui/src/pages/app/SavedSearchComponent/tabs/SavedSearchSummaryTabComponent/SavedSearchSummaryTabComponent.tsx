import React, { FunctionComponent, useState, useImperativeHandle } from "react";
import { Divider, Table, Spin, Modal, Tag } from "antd";
import { ModalFuncProps } from "antd/lib/modal";
import moment from "moment";

import "./SavedSearchSummaryTabComponent.scss";
import { ReportTemplateApi, SavedSearchApi } from "api";
import { AuthButtonComponent, SearchBarComponent, ResizableTableComponent } from "components";
import { DateFormat, GenerateOptionType, MessageType, ReportBuildType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { common } from "shared/common";
import { generateTypeData } from "common/data/ReportBuilderData";

type SavedSearchSummaryTabComponentProps = TabComponentProps & {
  searchRef: any;
};

const SavedSearchSummaryTabComponent: FunctionComponent<SavedSearchSummaryTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(EmptyPageProps());
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);

  useImperativeHandle(props.searchRef, () => ({
    search: () => {
      onSearch(searchModel);
    },
    updateSuccess: (value: any) => {
      errFrameShow(MessageType.SUCCESS, value);
    }
  }));

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick, isReportAdvance } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const buildType = isReportAdvance ? ReportBuildType.REPORT_ADVANCE : ReportBuildType.REPORT_BUILDER;
    const model = { ...EmptyPageProps(), ...value, buildType };
    SavedSearchApi.searchRecords(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onExport = (action: any) => {
    onCloseMessage();
    setLoading(true);
    const buildType = isReportAdvance ? ReportBuildType.REPORT_ADVANCE : ReportBuildType.REPORT_BUILDER;
    const model = { ...searchModel, buildType };
    SavedSearchApi.searchRecordsAll(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        onAuthButtonClick({ action, data: response.object, columns: isReportAdvance ? summaryReportAdvanceColumnsExport : summaryReportBuilderColumnsExport });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  }; 

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
    setSearchModel(EmptyPageProps());
  };

  const onButtonClick = (action: any) => {
    // if EDIT button clicked. Record id also needed to be passed.
    if (action === TabEnum.ADD) {
      onAuthButtonClick({ action, selectedRow: {} });
    }
    if (action === TabEnum.EDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.DELETE) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
      if (common.isNotEmpty(selectedRow)) {
        confirm({ ...EmptyConfirmDeleteProps(), ...{
          onOk() {
            const [{ id, savedSearch }] = selectedRow;
            const model = { id, name: savedSearch };
            ReportTemplateApi.deleteRecords(model).then((response) => {
              const { responseCode, responseDesc } = response;
              if (ResponseCode.SUCCESS === responseCode) {
                setSelectedRow([]);
                onSearch(searchModel);
                errFrameShow(MessageType.SUCCESS, "Delete success.");
              } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
                shieldRef.current.open({ ...response });
              } else {
                errFrameShow(MessageType.ERROR, responseDesc);
              }
            });
          },
        }});
      } else {
        errFrameShow(MessageType.ERROR, "Please select a record.");
      }
    }
    if (action === TabEnum.AUDIT) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.EXPORT) {
      onExport(action);
    }
  }

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const buildType = isReportAdvance ? ReportBuildType.REPORT_ADVANCE : ReportBuildType.REPORT_BUILDER;
    const model = { ...searchModel, page, pageSize, buildType };
    setLoading(true);
    SavedSearchApi.searchRecords(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'id';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  return (
    <div className="SavedSearchSummaryTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={searchFieldList}
      />
      <Divider />
      <AuthButtonComponent
        onAuthButtonClick={onButtonClick}
      />
      <ResizableTableComponent
        className={"sysTable" + (searchList.length ? " tableTop" : "")}
        bordered
        rowKey={rowKey}
        rowSelection={rowSelection}
        dataSource={searchList}
        columns={isReportAdvance ? summaryReportAdvanceColumnsDisplay : summaryReportBuilderColumnsDisplay}
        expandable={isReportAdvance && {
          expandedRowRender: (record: any) => <p style={{ margin: 0 }}>{record.sql}</p>
        }}
        pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default SavedSearchSummaryTabComponent;

class SavedSearchSummaryTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });
  static EmptyConfirmDeleteProps = (): ModalFuncProps => ({
    title: 'Are you sure to delete?',
    content: '',
    okText: 'Confirm',
    okType: 'danger',
    cancelText: 'Cancel',
    onOk: () => {},
    onCancel: () => console.log('onCancel!'),
  });
  static parseGenerateType = (text: any) => {
    if (text === GenerateOptionType.ONLINE_ONLY) {
      return GenerateOptionType.ONLINE_ONLY_TEXT;
    }
    else if (text === GenerateOptionType.SCHEDULE_ONLY) {
      return GenerateOptionType.SCHEDULE_ONLY_TEXT;
    }
    else {
      return GenerateOptionType.ONLINE_SCHEDULE_TEXT;
    }
  };
}

const { EmptyPageProps, EmptyConfirmDeleteProps, parseGenerateType } = SavedSearchSummaryTabUtils;
const { confirm } = Modal;

const searchFieldList = [
  {
    label: "Generate Type",
    type: "selectOption",
    key: "generateOpt",
    list: generateTypeData,
  },
];

const summaryReportBuilderColumnsDisplay = [
  {
    title: "Report Name",
    dataIndex: "savedSearch",
  },
  {
    title: "Report Description",
    dataIndex: "description",
  },
  {
    title: "Report Table",
    dataIndex: "table",
    render: (cell: any) => <div>{cell.split(',').map((table: any, key: number) => <Tag key={key} color='pink'>{table}</Tag>)}</div>,
    key: "table",
    width: 400,
  },
  {
    title: "Access Role",
    dataIndex: "roleList",
  },
  {
    title: "Generate Type",
    dataIndex: "generateOpt",
    render: (cell: any) => parseGenerateType(cell),
  },
  {
    title: "Create Date",
    dataIndex: "createDate",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
];

const summaryReportBuilderColumnsExport = [
  {
    title: "Report Name",
    dataIndex: "savedSearch",
  },
  {
    title: "Report Description",
    dataIndex: "description",
  },
  {
    title: "Report Table",
    dataIndex: "table",
    key: "table",
    width: 400,
  },
  {
    title: "Access Role",
    dataIndex: "roleList",
  },
  {
    title: "Type",
    dataIndex: "generateOpt",
    render: (cell: any) => parseGenerateType(cell),
  },
  {
    title: "Create Date",
    dataIndex: "createDate",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
];

const summaryReportAdvanceColumnsDisplay = [
  {
    title: "Report Name",
    dataIndex: "savedSearch",
  },
  {
    title: "Report Description",
    dataIndex: "description",
  },
  {
    title: "Access Role",
    dataIndex: "roleList",
  },
  {
    title: "Type",
    dataIndex: "generateOpt",
    render: (cell: any) => parseGenerateType(cell),
  },
  {
    title: "Create Date",
    dataIndex: "createDate",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
];

const summaryReportAdvanceColumnsExport = [
  {
    title: "Report Name",
    dataIndex: "savedSearch",
  },
  {
    title: "Report Description",
    dataIndex: "description",
  },
  {
    title: "Report Query",
    dataIndex: "sql",
    /*render: (cell: any) => <Tooltip title={cell} overlayClassName="SavedSearchSummaryTabTooltip"><Text className="td-sql" ellipsis>{cell}</Text></Tooltip>,*/
    key: "sql",
    width: 400,
  },
  {
    title: "Access Role",
    dataIndex: "roleList",
  },
  {
    title: "Type",
    dataIndex: "generateOpt",
    render: (cell: any) => parseGenerateType(cell),
  },
  {
    title: "Create Date",
    dataIndex: "createDate",
    render: (cell: any) => moment(cell).format(DateFormat.DATETIME)
  },
];
