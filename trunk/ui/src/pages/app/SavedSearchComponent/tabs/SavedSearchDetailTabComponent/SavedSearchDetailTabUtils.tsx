import React, { FunctionComponent } from "react";
import { Form,  Row,  Col, Input, Button } from "antd";
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { MessageType, ResponseCode, TabEnum } from "enum";
import { common } from "shared/common";

export class SavedSearchDetailTabUtils {

  static onChangeCode = (event: any) => {
    const text = (event.target.value || '');
    if (common.isNotEmpty(text) && text.length < 31) {
      return text.replace(/^ +| +$/g, '').replace(/[^\w_\-*]/ig, '').toLocaleUpperCase();
    }
    return '';
  };

  static SavedSearchDetail: FunctionComponent<any> = (props: any) => {
    const propsComponent = { ...props, onSave: () => onSave(props), onReset: () => onReset(props) };
    return <SavedSearchEditTabComponent {...propsComponent} />;
  };
}
const onSave = (props: any) => {
  const { SavedSearchApi, UserInfo } = props;
  const { form, formAction, setLoading } = props;
  const { messageShowHandler, errFrameShow, onChangeCallback, search } = props;
  messageShowHandler();
  setLoading(true);

  const callbackAction = (response: any) => {
    const { responseCode, responseDesc } = response;
    if (ResponseCode.SUCCESS === responseCode) {
      onChangeCallback(TabEnum.DEFAULT_TAB);
      search();
      errFrameShow(MessageType.SUCCESS, "Save success.");
    } else {
      errFrameShow(MessageType.ERROR, responseDesc);
    }
  };
  const finallyAction = () => setLoading(false);

  const model = { ...form.getFieldsValue(), menuFuncId: UserInfo.funcId };
  if (formAction === TabEnum.ADD) {
    SavedSearchApi.insertRecords(model).then(callbackAction).finally(finallyAction);
  } else {
    SavedSearchApi.updateRecords(model).then(callbackAction).finally(finallyAction);
  }
}

const onReset = (props: any) => {
  const { form } = props;
  const { messageShowHandler, onChangeCallback } = props;
  messageShowHandler();
  form.resetFields();
  onChangeCallback(TabEnum.DEFAULT_TAB);
};

const SavedSearchEditTabComponent: FunctionComponent<any> = (props: any) => {
  const { form, formLabel, formAction, onSave, onReset } = props;
  return (
    <Form form={form} name="form" onFinish={onSave} {...formItemLayout}>
    <Row gutter={24}>
      <Col span={12}>
        <span className="id">{formLabel}</span>
      </Col>
      <Col span={12}>
        <Form.Item name="id">
          <Input hidden={true} />
        </Form.Item>
      </Col>        
    </Row>
    <Row gutter={24}>
      <Col span={8}>
        <Form.Item name="savedSearch" label="Saved Search" labelAlign="right" rules={[{ required: true, message: 'Please input name.' }]}>
          <Input placeholder="Saved Search" maxLength={30} disabled={formAction === TabEnum.EDIT}/>
        </Form.Item>
      </Col>
      <Col span={8}>
        <Form.Item name="accessGroup" label="Access Group" labelAlign="right" rules={[{ required: true, message: 'Please input access group.' }]}>
          <Input placeholder="Access Group" maxLength={50}/>
        </Form.Item>
      </Col>
    </Row>
    <Row gutter={24}>
      <Col span={8}>
        <Form.Item name="sql" label="Query" labelAlign="right">
          <Input placeholder="Query" maxLength={30} disabled/>
        </Form.Item>
      </Col>
      <Col span={8}>
        <Form.Item name="exportOpt" label="Export Option" labelAlign="right" rules={[{ required: true, message: 'Please input export option.' }]}>
          <Input placeholder="Export Option" maxLength={30} disabled/>
        </Form.Item>
      </Col>
    </Row>
    <Row gutter={24}>
      <Col span={24} style={{ textAlign: "right" }}>
        <Button type="primary" className="btn-tabAction" icon={<CheckCircleOutlined />} children="Save" htmlType="submit" />
        <Button type="primary" className="btn-tabAction" icon={<CloseCircleOutlined />} children="Cancel" onClick={onReset} />
      </Col>
    </Row>
    </Form>
  );
};

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
  },
};
