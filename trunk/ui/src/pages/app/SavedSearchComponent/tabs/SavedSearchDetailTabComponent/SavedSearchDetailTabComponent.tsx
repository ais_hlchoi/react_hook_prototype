import React, { FunctionComponent, useState, useEffect } from "react";
import { Divider, Spin } from "antd";

import "./SavedSearchDetailTabComponent.scss";
import ReportAdvanceComponent from "pages/app/ReportAdvanceComponent";
import ReportBuilderComponent from "pages/app/ReportBuilderComponent";
import { ReportTemplateApi } from "api";
import { MessageType, ReportBuildType, ResponseCode, TabEnum, GenerateOptionType } from "enum";
import { TabComponentProps } from "types";
import { ReportBuilderDataUtils, ReportBuilderPropsUtils } from "pages/app/ReportBuilderComponent/tabs";
import { CommonUtils, UseEffectUtils } from "common/utils";

type SavedSearchDetailTabComponentProps = TabComponentProps & {
  title?: string;
  onSearch: Function;
  action: string;
  selectedRow: any;
  reportBuildType?: ReportBuildType;
};

const SavedSearchDetailTabComponent: FunctionComponent<SavedSearchDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [editingKey, setEditingKey] = useState<number>(0);
  const [editingItem, setEditingItem] = useState<any>({});
  const [reportBuildType, setReportBuildType] = useState<any>(ReportBuildType.REPORT_UNKNOWN);
  const { componentName, title = '' } = props;

  useEffect(() => {
    const { shieldRef, componentName, errFrameShow, selectedRow: { id = 0 }, action, reportBuildType = ReportBuildType.REPORT_ADVANCE } = props;
    if (id && action) {
      if (!isSkip(componentName)) {
        const model = { id };
        ReportTemplateApi.getReportProps(model).then((response: any) => {
          const { responseCode, responseDesc, object } = response;
          if (ResponseCode.SUCCESS === responseCode) {
            const formData = { ...object };
            const formDataReportBuildType = (formData.table && formData.table.length) ? ReportBuildType.REPORT_BUILDER : ReportBuildType.REPORT_ADVANCE;
            if (formDataReportBuildType === ReportBuildType.REPORT_BUILDER) {
              Object.assign(formData, { current: 1 });
              resetReportProps();
              resetDataLoaded();
              setReportProps(formData);
            }
            setEditingKey(id);
            setEditingItem(formData);
            setReportBuildType(formDataReportBuildType);
            props.scheduleEnableCallback(formData.generateOpt === GenerateOptionType.BOTH || formData.generateOpt === GenerateOptionType.SCHEDULE_ONLY  );
          } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
            shieldRef.current.open({ ...response });
          } else {
            errFrameShow(MessageType.ERROR, responseDesc);
          }
        });
      }
    } else {
      setEditingKey(0);
      setEditingItem({});
      setReportBuildType(action ? reportBuildType : ReportBuildType.REPORT_UNKNOWN);
    }
  }, [props]);

  const { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onUpdateRecord, isReportAdvance } = props;

  const onSave = (value: any) => {
    setSkip(componentName);
    onCloseMessage();
    setLoading(true);
    const buildType = isReportAdvance ? ReportBuildType.REPORT_ADVANCE : ReportBuildType.REPORT_BUILDER;
    const { reportPropsModel, callback } = value;
    if (!isCreateMode(editingKey) && editingKey !== reportPropsModel.id) {
      console.log(`Incorrect ReportTemplateId [ ${editingKey} != ${reportPropsModel.id} ]`);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
      setLoading(false);
      return false;
    }
    const model = { ...reportPropsModel, buildType };
    ReportTemplateApi.insertRecords(model).then((response: any) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        errFrameShow(MessageType.SUCCESS, "Save success.");
        onUpdateRecord();
        if (typeof callback === 'function') {
          callback();
        }
        return true;
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        resetSkip(componentName);
        shieldRef.current.open({ ...response });
      } else {
        resetSkip(componentName);
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).catch((error) => {
      console.log(error);
      resetSkip(componentName);
      errFrameShow(MessageType.ERROR, "Create fail.");
    }).finally(() => {
      setLoading(false);
    }).then((boo: any = false) => {
      if (boo) {
        onBack();
      }
    });
  };

  const onBack = () => {
    setEditingKey(0);
    setEditingItem({});
    setReportBuildType(ReportBuildType.REPORT_UNKNOWN);
    onCloseMessage();
    onChangeCallback(TabEnum.DEFAULT_TAB);
  };

  const propsBuild = {
    editingItem,
    propsBtnSave: { action: onSave },
    propsBtnBack: { action: onBack, show: true },
    isBlankProps: isCreateMode(editingKey),
  };

  return (
    <div className="SavedSearchDetailTabWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      {reportBuildType !== ReportBuildType.REPORT_UNKNOWN && <>
      <Spin size="large" tip={"Data saving in progress..."} spinning={loading}>
      {reportBuildType === ReportBuildType.REPORT_BUILDER && <ReportBuilderComponent {...propsBuild} />}
      {reportBuildType === ReportBuildType.REPORT_ADVANCE && <ReportAdvanceComponent {...propsBuild} />}
      </Spin>
      </>}
    </div>
  );
};

export default SavedSearchDetailTabComponent;

class SavedSearchDetailTabUtils {
  static isCreateMode = (editingKey: any) => CommonUtils.isEmpty(editingKey) || editingKey < 1;
}

const { isCreateMode }  = SavedSearchDetailTabUtils;
const { resetDataLoaded } = ReportBuilderDataUtils;
const { setReportProps, resetReportProps } = ReportBuilderPropsUtils;
const { isSkip, setSkip, resetSkip } = UseEffectUtils;
