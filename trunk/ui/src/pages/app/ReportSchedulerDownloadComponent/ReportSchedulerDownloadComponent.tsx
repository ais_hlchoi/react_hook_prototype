/* eslint-disable */
import React, {FunctionComponent, useRef, useState} from "react";
import { Divider, Tabs } from "antd";

import "./ReportSchedulerDownloadComponent.scss";
import { ReportSchedulerDownloadSearchTabComponent, ReportSchedulerDownloadDetailTabComponent } from "./tabs";
import { MessageAlertComponent, EmptyMessageAlertProps } from "components";
import { AuditModelComponent } from "components";
import { MessageType, TabEnum } from "enum";
import { PageComponentProps } from "types";
import { common } from "shared/common";

const { TabPane } = Tabs;
type ReportDownloadComponentProps = PageComponentProps;

const ReportSchedulerDownloadComponent: FunctionComponent<ReportDownloadComponentProps> = (props: any) => {
  const [message, setMessage] = useState<any>(EmptyMessageAlertProps());
  const [auditModel, setAuditModel] = useState<any>({});
  const [activeKey, setActiveKey] = useState<any>(TabEnum.DEFAULT_TAB);
  const [action, setAction] = useState<string>('');
  const [selectedRow, setSelectedRow] = useState<any>({});
  const { shieldRef, title = 'Report Download' } = props;

  const ReportDownloadComponent: any = useRef();

  const onAuthButtonClick = (props: any) => {
    onCloseMessage();
    const { action, selectedRow } = props;
    switch (action) {
      case TabEnum.RERUN:
        console.log("rerun download report")
        break;
      case TabEnum.REVIEW:
        if (common.isNotEmpty(selectedRow)) {
          setActiveKey(TabEnum.SECOND_TAB);
          setAction(action);
          setSelectedRow(selectedRow);
        }  else {
          errFrameShow(MessageType.ERROR, "Please select a record.");
        }
        break;
      case TabEnum.EXPORT:
        common.downloadExcel(props.data, props.columns, 'Export');
        break;
    }
  };

  const onSelectChange = (_selectedRowKeys: any, selectedRow: any) => {
    if (common.isNotEmpty(selectedRow)) {
      const [model] = selectedRow;
      setSelectedRow(model);
      setAuditModel({ ...auditModel, model });
    } else {
      onSelectClear();
    }
  };

  const onSelectClear = () => {
    const model = {};
    setSelectedRow(model);
    setAuditModel({ ...auditModel, model });
  };

  const errFrameShow = (type: MessageType, content: string) => {
    setMessage({ ...EmptyMessageAlertProps(), type, content, show: true });
    document.documentElement.scrollTop = 0;
  };

  const onCloseMessage = () => setMessage(EmptyMessageAlertProps());
  const onCloseAudit = () => setAuditModel({ ...auditModel, show: false });

  const onChangeCallback = (key: any) => {
    onCloseMessage();
    if (common.isNotEmpty(key)) {
      if (key !== TabEnum.DEFAULT_TAB) {
        setActiveKey(key);
      } else {
        onSelectClear();
        setActiveKey(TabEnum.DEFAULT_TAB);
        setAction('');
      }
    }
  };

  const onChangeCallbackRefresh = (key: any, refresh:boolean) => {
    onCloseMessage();
    if (common.isNotEmpty(key)) {
      if (key !== TabEnum.DEFAULT_TAB) {
        setActiveKey(key);
      } else {
        onSelectClear();
        setActiveKey(TabEnum.DEFAULT_TAB);
        setAction('');
      }
    }
  };

  const propsTabSummary = { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick, onSelectChange };
  const propsTabDetail  = { shieldRef, errFrameShow, onCloseMessage, onChangeCallback, onChangeCallbackRefresh , onSearch: () => ReportDownloadComponent.current.search(), action, selectedRow };
  return (
    <div className="ReportDownloadWrapper">
      {title && <><span className="header">{title}</span><Divider /></>}
      <MessageAlertComponent {...{ ...message, onClose: onCloseMessage }} />
      <AuditModelComponent   {...{ ...auditModel, onClose: onCloseAudit }} />
      <Tabs defaultActiveKey={TabEnum.DEFAULT_TAB} activeKey={activeKey} onChange={onChangeCallback}>
        <TabPane tab="Summary" key={TabEnum.FIRST_TAB}>
          <ReportSchedulerDownloadSearchTabComponent {...propsTabSummary} />
        </TabPane>
        {
          action === TabEnum.REVIEW && activeKey === TabEnum.SECOND_TAB &&
          <TabPane tab="Detail" key={TabEnum.SECOND_TAB}>
            <ReportSchedulerDownloadDetailTabComponent {...propsTabDetail}/>
          </TabPane>
        }
      </Tabs>
    </div>
  );
};

export default ReportSchedulerDownloadComponent;
