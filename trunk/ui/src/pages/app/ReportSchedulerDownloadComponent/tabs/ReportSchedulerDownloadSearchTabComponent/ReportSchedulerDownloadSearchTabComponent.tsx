import React, { FunctionComponent, useState, useEffect } from "react";
import { Spin, Divider, Row, Col, Button, Table } from "antd";

import "./ReportSchedulerDownloadSearchTabComponent.scss";
import { ReportDownloadApi } from "api";
import { ResizableTableComponent, SearchBarComponent } from "components";
import { DateFormat, MessageType, ResponseCode, TabEnum } from "enum";
import { TabComponentProps } from "types";
import { ClockCircleOutlined, VerticalAlignBottomOutlined, RedoOutlined } from "@ant-design/icons";
import { common } from "../../../../../shared/common";
import moment from 'moment'

type ReportDownloadSearchTabComponentProps = TabComponentProps;

const ReportSchedulerDownloadSearchTabComponent: FunctionComponent<ReportDownloadSearchTabComponentProps> = (props: any) =>  {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(EmptyPageProps());
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRow, setSelectedRow] = useState([]);
  const [initSearch, setInitSearch] = useState(false);

  useEffect(() => {
    if (!initSearch) {
      setInitSearch(true);
      const { shieldRef, errFrameShow } = props;
      setLoading(true);
      const model = { ...EmptyPageProps() };
      ReportDownloadApi.searchSchedulerDownloadReport(model).then((response) => {
        const { responseCode, responseDesc } = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const { object: { list, total } } = response;
          setSearchList(list);
          setSearchModel({ ...model, total });
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({ ...response });
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      }).finally(() => {
        setLoading(false);
      });
    }
  }, [props, initSearch]);

  const { shieldRef, errFrameShow, onCloseMessage, onAuthButtonClick } = props;

  const onSearch = (value: any) => {
    onCloseMessage();
    setLoading(true);
    const model = { ...EmptyPageProps(), ...value };
    ReportDownloadApi.searchSchedulerDownloadReport(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onReset = () => {
    onCloseMessage();
    setSearchList([]);
  };

  const onSelectChange = (selectedRowKeys: any, selectedRow: any) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRow(selectedRow);
    props.onSelectChange(selectedRowKeys, selectedRow);
  };

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = { ...searchModel, page, pageSize };
    setLoading(true);
    ReportDownloadApi.searchSchedulerDownloadReport(model).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const { object: { list, total } } = response;
        setSearchList(list);
        setSearchModel({ ...model, total });
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const { page, pageSize, total } = searchModel;

  const rowKey = 'id';
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };
  const rowSelection = {
    type: "radio",
    selectedRowKeys,
    selectedRow,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT
    ]
  };

  const onExport = (action: any) => {
    if (selectedRow.length === 0) {
      errFrameShow(MessageType.ERROR, "Please select a record.");
      return;
    }
    onCloseMessage();
    setLoading(true);
    const refId : string = selectedRow[0]['refId'] || '';
    const refType : string = selectedRow[0]['refType'] || '';
    ReportDownloadApi.searchSchedulerDownloadReportHistory({page: 1, pageSize: 1, total : 0, refId, refType}).then((response) => {
      const { responseCode, responseDesc } = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const {object: {list}} = response;
        if (list && list.length > 0){
          const fileSearchModel = list[0];
          const extension = fileSearchModel.fileExtension;
          const exportModel = {id: fileSearchModel.fileId, fileExtension: extension};
          ReportDownloadApi.getFile(exportModel).then((blob: any) => {
            if (blob.size > 10) {
              let a = document.createElement('a');
              document.body.appendChild(a);
              let url = window.URL.createObjectURL(blob);
              a.href = url;
              const rptName : string  = selectedRow[0]['rptName'] || '';
              a.download = `${rptName.replace(/\\s+/g, '_')}.${extension}`;
              a.click();
              a.remove();
              window.URL.revokeObjectURL(url);
              errFrameShow(MessageType.SUCCESS, "Export success.");
            } else {
              errFrameShow(MessageType.ERROR, "Export fail.");
            }
          }).catch((error) => {
            console.log(error);
            errFrameShow(MessageType.ERROR, "Export fail.");
          });
        }
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({ ...response });
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const onButtonClick = (action: any) => {
    if (action === TabEnum.RERUN) {
      console.log("rerun download report, the requirement need to be clarified")
    }
    if (action === TabEnum.REVIEW) {
      onAuthButtonClick({ action, selectedRow: selectedRow[0] });
    }
    if (action === TabEnum.EXPORT) {
      onExport(action);
    }
  }

  return (
    <div className="ReportDownloadSearchTabWrapper">
      <Spin size="large" tip={"Searching..."} spinning={loading}>
      <SearchBarComponent
        onSearch={onSearch}
        onReset={onReset}
        searchFieldList={[]}
      />
      <Divider />
      <Row gutter={24} className="buttonRelative">
        <Col span={18}>
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onButtonClick(TabEnum.RERUN)}     children="Rerun"     icon={<RedoOutlined />} />
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onButtonClick(TabEnum.REVIEW)}    children="Review"    icon={<ClockCircleOutlined />} />
          <Button type="primary" className="btn-tabAction btn-aud" onClick={() => onButtonClick(TabEnum.EXPORT)}    children="Export"    icon={<VerticalAlignBottomOutlined />} />
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={12}>&nbsp;</Col>
      </Row>
      <ResizableTableComponent
          className={"sysTable" + (searchList.length ? " tableTop" : "")}
          bordered
          rowKey={rowKey}
          rowSelection={rowSelection}
          dataSource={searchList}
          columns={summaryColumns}
          pagination={pagination}
      />
      </Spin>
    </div>
  );
};

export default ReportSchedulerDownloadSearchTabComponent;

class ReportDownloadSearchTabUtils {
  static EmptyPageProps = () => ({
    page: 1,
    pageSize: 10,
    total : 0,
  });
}

const { EmptyPageProps } = ReportDownloadSearchTabUtils;

const summaryColumns: any = [
  {
    title: "Scheduler Name",
    dataIndex: "schedulerName",
    sorter: (a, b) => { return common.sortMethod(a, b, 'schedulerName') },
    width: 160,
  },
  {
    title: "Report Name",
    dataIndex: "rptName",
    sorter: (a, b) => { return common.sortMethod(a, b, 'rptName') },
    width: 160,
  },
  {
    title: "Report Description",
    dataIndex: "description",
    sorter: (a, b) => { return common.sortMethod(a, b, 'description') },
    width: 160,
  },
  {
    title: "Export Option",
    dataIndex: "exportOption",
    sorter: (a, b) => { return common.sortMethod(a, b, 'exportOption') },
    width: 160,
  },
  {
    title: "Last Execution Date",
    dataIndex: "lastExecutionTime",
    sorter: (a, b) => { return common.sortMethod(a, b, 'lastExecutionTime') },
    width: 160,
    render: (cell: any) => cell && moment(cell).format(DateFormat.DATETIME),
  },
  {
    title: "Next Execution Date",
    dataIndex: "nextExecutionTime",
    sorter: (a, b) => { return common.sortMethod(a, b, 'nextExecutionTime') },
    width: 160,
    render: (cell: any) => cell && moment(cell).format(DateFormat.DATETIME),
  }
];
