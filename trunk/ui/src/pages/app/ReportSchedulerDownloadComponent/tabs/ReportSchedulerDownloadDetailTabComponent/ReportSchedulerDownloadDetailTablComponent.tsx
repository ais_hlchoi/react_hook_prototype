import React, {FunctionComponent, useState, useEffect} from "react";
import './ReportSchedulerDownloadDetailTabComponent.scss'
import {TabComponentProps} from "types";
import {Spin, Divider, Row, Col, Button} from "antd";
import {DateFormat, MessageType, ResponseCode} from "../../../../../enum";
import {RedoOutlined, DownloadOutlined} from "@ant-design/icons";
import {ResizableTableComponent} from "../../../../../components";
import {common} from "../../../../../shared/common";
import ReportDownloadApi from "../../../../../api/ReportDownloadApi";
import moment from 'moment';

type ReportDownloadDetailTabComponentProps = TabComponentProps;
const defaultSearchModel = {
  page: 1,
  pageSize: 10,
  total: 0,
  schedId: null,
  refType: null,
};

const ReportSchedulerDownloadDetailTablComponent: FunctionComponent<ReportDownloadDetailTabComponentProps> = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [searchModel, setSearchModel] = useState(defaultSearchModel);
  const {selectedRow, shieldRef, errFrameShow, onCloseMessage} = props;
  const [initSearch, setInitSearch] = useState(false);

  useEffect(() => {
    if (!initSearch) {
      setInitSearch(true);
      const model = {...defaultSearchModel, schedId: selectedRow.id, refType: selectedRow.refType};
      ReportDownloadApi.searchSchedulerDownloadReportHistory(model).then((response) => {
        const {responseCode, responseDesc} = response;
        if (ResponseCode.SUCCESS === responseCode) {
          const {object: {list, total}} = response;
          list.forEach((value, index) => {
              value.exportBtn = {fileId: value.fileId, fileExtension: value.fileExtension}
              value.id = index;
          });
          setSearchList(list);
          setSearchModel({...model, total});
        } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
          shieldRef.current.open({...response});
        } else {
          errFrameShow(MessageType.ERROR, responseDesc);
        }
      }).finally(() => {
        setLoading(false);
      });
    }
  },  [initSearch, selectedRow, shieldRef, errFrameShow] )


  const handleOnExport = (fileId: string, fileExtension: string) => {
    ReportDownloadApi.getFile({id: fileId, fileExtension}).then((blob: any) => {
      if (blob.size > 10) {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        const rptName: string = selectedRow['rptName'] || '';
        a.download = `${rptName.replace(/\\s+/g, '_')}.${fileExtension}`;
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
        errFrameShow(MessageType.SUCCESS, "Export success.");
      } else {
        errFrameShow(MessageType.ERROR, "Export fail.");
      }
    }).catch((error) => {
      console.log(error);
      errFrameShow(MessageType.ERROR, "Export fail.");
    });
  }

  const pageChange = (page: any, pageSize: any) => {
    onCloseMessage();
    const model = {...searchModel, page, pageSize};
    setLoading(true);
    ReportDownloadApi.searchSchedulerDownloadReportHistory(model).then((response) => {
      const {responseCode, responseDesc} = response;
      if (ResponseCode.SUCCESS === responseCode) {
        const {object: {list, total}} = response;
        list.forEach((value, index) => {
          value.exportBtn = {fileId: value.fileId, fileExtension: value.fileExtension}
          value.id = index;
        });
        setSearchList(list);
        setSearchModel({...model, total});
      } else if (new RegExp(ResponseCode.SHIELD).test(responseCode)) {
        shieldRef.current.open({...response});
      } else {
        errFrameShow(MessageType.ERROR, responseDesc);
      }
    }).finally(() => {
      setLoading(false);
    });
  };

  const rowKey = 'id';
  const {page, pageSize, total} = searchModel;
  const pagination = {
    position: ['topRight'],
    showSizeChanger: true,
    onChange: pageChange,
    onShowSizeChange: pageChange,
    total,
    current: page,
    pageSize,
    defaultCurrent: 1,
  };


  const detailColumns: any = [
    {
      title: "File Name",
      dataIndex: "fileName",
      sorter: (a, b) => {
        return common.sortMethod(a, b, 'fileName')
      },
      width: 160,
    },
    {
      title: "Execution Date",
      dataIndex: "executionDate",
      sorter: (a, b) => {
        return common.sortMethod(a, b, 'executionDate')
      },
      width: 160,
      render: (cell: any) => cell && moment(cell).format(DateFormat.DATETIME),
    },
    {
      title: "Export",
      dataIndex: "exportBtn",
      width: 160,
      render: (cell : any) => <Button icon={<DownloadOutlined/>} onClick={() => {
        handleOnExport(cell.fileId, cell.fileExtension)
      }}>Export</Button>
    }
  ];

  return (
    <>
      <Spin size="large" tip={"Searching..."} spinning={loading}>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Report Name:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.rptName}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Schedule Name:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.schedulerName}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Report Description:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.description}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Export Option:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.exportOption}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Last Execution Date:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.lastExecutionTime && moment(selectedRow.lastExecutionTime).format(DateFormat.DATETIME)}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={4}>
            <span className={"report-desc"}>Next Execution Date:</span>
          </Col>
          <Col span={20}>
            <span>{selectedRow.nextExecutionTime && moment(selectedRow.nextExecutionTime).format(DateFormat.DATETIME)}</span>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24}/>
        </Row>
        <Row gutter={24}>
          <Col span={24}>
            <Button type="primary" className="btn-tabAction btn-aud" onClick={() => {
              console.log("rerun download report, the requirement need to be clarified")
            }} children="Rerun" icon={<RedoOutlined/>}/>
          </Col>
        </Row>
        <Divider/>
        <div>
          <ResizableTableComponent
            className={"sysTable" + (searchList.length ? " tableTop" : "")}
            bordered
            rowKey={rowKey}
            //rowSelection={rowSelection}
            dataSource={searchList}
            columns={detailColumns}
            pagination={pagination}
          />
        </div>
      </Spin>
    </>
  )
}

export default ReportSchedulerDownloadDetailTablComponent;
