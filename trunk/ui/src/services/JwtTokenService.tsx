const JwtTokenService = () => {
  let jwtToken = "";

  const getJwtToken = () => {
    return jwtToken;
  };

  const setJwtToken = (token: any) => {
    jwtToken = token;
  };

  return {
    getJwtToken,
    setJwtToken
  };
};

export default JwtTokenService;
