const UserInfoService = () => {
    let userInfo:any = null
  
    const getUserInfo = () => {
        return userInfo;
    };
  
    const setUserInfo = (props: any) => {
        userInfo = props;
    };
  
    return {
        getUserInfo,
        setUserInfo
    };
  };
  
  export default UserInfoService;
  