import { utils } from "shared/utils";

const MenuService = () => {

  let menuServiceList: any[] = [];
  let menuPathList: any[] = [];

  const groupMenu = (menuList: any[]) => {
    menuServiceList = menuList.filter((item: any) => item.type === 'MENU');
    menuServiceList.forEach((item: any) => {
      item.children = menuList.filter((o: any) => o.parentId === item.funcId);
      item.children.sort((a: any, b: any) => a.dispSeq - b.dispSeq);
    });

    menuPathList = menuList.flatMap((lv1: any) => (
      [
        { parentId: lv1.parentId, funcId: lv1.funcId, funcCode: lv1.funcCode, funcName: lv1.funcName, funcUrl: lv1.funcUrl },
        ...(lv1.children || [])
          .filter((lv2: any) => utils.isNotEmpty(lv2.children))
          .map((lv2: any) => ({ parentId: lv2.parentId, funcId: lv2.funcId, funcCode: lv2.funcCode, funcName: lv2.funcName, funcUrl: lv2.funcUrl }))
      ]
    )).filter(({funcUrl}) => utils.isNotEmpty(funcUrl));

    return menuServiceList;
  };

  const getGroupMenu = () => {
    return menuServiceList;
  };

  const getGroupPath = () => {
    return menuPathList;
  };

  return {
    groupMenu,
    getGroupMenu,
    getGroupPath,
  };
};

export default MenuService;
