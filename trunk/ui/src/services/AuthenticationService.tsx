const AuthenticationService = () => {

  let authenticatedUser: any = null;
  let menuList: any = [];
  let pathList: any = [];

  const isAuthenticated = () => {
    return authenticatedUser != null;
  };

  const getUserModel = () => {
    return authenticatedUser;
  };

  const setUserModel = (userModel: any) => {
    authenticatedUser = userModel;
  };

  const setRoleId = (roleId: any) => {
    if (isAuthenticated())
      authenticatedUser.roleId = roleId;
  };

  const setFuncId = (funcId: any) => {
    if (isAuthenticated())
      authenticatedUser.funcId = funcId;
  };

  const getMenuList = () => {
    return menuList;
  };

  const setMenuList = (list: any) => {
    menuList = list;
  };

  const getPathList = () => {
    return pathList;
  };

  const setPathList = (list: any) => {
    pathList = list;
  };

  return {
    isAuthenticated,
    getUserModel,
    setUserModel,
    setRoleId,
    setFuncId,
    getMenuList,
    setMenuList,
    getPathList,
    setPathList,
  };
};

export default AuthenticationService;
