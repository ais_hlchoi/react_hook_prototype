import React from "react";
import MenuService from "../MenuService";
import AuthenticationService from "../AuthenticationService";
import JwtTokenService from "../JwtTokenService";
import UserInfoService from "../UserInfoService";

export const servicesState = {
  MenuService: MenuService(),
  AuthenticationService: AuthenticationService(),
  JwtTokenService: JwtTokenService(),
  UserInfoService: UserInfoService(),
};

export const ServicesContext = React.createContext(servicesState);
