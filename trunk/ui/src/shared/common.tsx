import ExportJsonExcel from 'js-export-excel';
import moment from 'moment';

import { DateFormat } from 'enum';
/**
 * Common method
 */
export class common {

    /**
     * Empty validation
     * String type validation，value equals（null,undefined,'','  '）will return true
     * Array type validation，if value equals（null,undefined,Array.length === 0）will return true
     * @param {*} param  
     */
    static isEmpty(param) {
        if (param == null) {
            return true;
        }
        let flag = false;
        if (typeof param === "string") {
            if (param.trim() === '') {
                flag = true;
            }
        }
        else if (param instanceof Array) {
            if (param.length === 0) {
                flag = true;
            }
        }
        else if (typeof param === "object") {
            if (JSON.stringify(param) === "{}") {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * @param {*} param 
     */
    static isNotEmpty(param) {
        return !common.isEmpty(param);
    }

    /**
     */
    static getUrlRelativePath() {
        var url = document.location.toString();
        var arrUrl = url.split("//");

        var start = arrUrl[1].indexOf("/");
        var relUrl = arrUrl[1].substring(start);

        if (relUrl.indexOf("?") !== -1) {
            relUrl = relUrl.split("?")[0];
        }
        return relUrl;
    }

    /**
     * (iPad/phone/pc)
     */
    static getEquipmentType = function () {
        let type;
        if (navigator.userAgent.indexOf('iPad') !== -1) {
            type = 'iPad';
        } else if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
            type = 'phone';
        } else {
            type = 'pc';
        }
        return type;
    };
    
    static cgIsEnd = function () {
        if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
            return true;
        } else {
            return false;
        }
    };
    
    static cgIsPhone = function () {
        if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Windows Phone)/i))) {
            return "iPhone";
        } else if ((navigator.userAgent.match(/(Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian)/i))) {
            return "android";
        } else {
            return "0"
        }
    };
    
    static cgIsWeixn = function () {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) + '' === "micromessenger") {
            return true;
        } else {
            return false;
        }
    };
    
    static cgIsView = function () {
        var userAgent = navigator.userAgent;
        return userAgent.split(" ");

    };

    static stopClickFirstTime = 0;//last button click time
    /**Prevent duplicate click */
    static stopClick(time = 3000) {
        let secondTime = new Date().getTime();
        if (secondTime - common.stopClickFirstTime > time) {
            common.stopClickFirstTime = secondTime;
            return true;
        }
        return false;
    };

    /**
     * @param {number} number 
     */
    static numberFormat(number, len = 0) {
        if (common.isEmpty(number)) {
            return "";
        }
        if (Object.is(parseFloat(number), NaN)) {
            return number;
        }
        if (Object.is(parseFloat(len + ''), NaN)) {
            return number;
        }
        if (common.isNotEmpty(len)) {
            if (len > 3) {
                let str = number.toString()
                let decimalStr = str.lastIndexOf(".") > -1 ? 0 + str.slice(str.lastIndexOf("."), str.length) : "0";
                let rounding = parseFloat(decimalStr).toFixed(len)
                let decimal = rounding.slice(rounding.indexOf("."), rounding.length)
                let integer = str.lastIndexOf(".") > -1 ? str.slice(0, str.lastIndexOf(".")) : str
                let formatNumber = `${parseFloat(integer)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + decimal
                return formatNumber;
            }
            return `${parseFloat(number).toFixed(len)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } else {
            return `${parseFloat(number)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }
    }

    /**
    * @param date 
    * @param fmt 
    */
    static dateFormat(date, fmt = "dd/MM/yyyy") { //author: meizz
        if (date == null) {
            return;
        }
        date = new Date(date);
        var o = {
            "M+": date.getMonth() + 1,                
            "d+": date.getDate(),                    
            "h+": date.getHours(),                   
            "m+": date.getMinutes(),                 
            "s+": date.getSeconds(),                 
            "q+": Math.floor((date.getMonth() + 3) / 3), 
            "S": date.getMilliseconds()             
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }

    /**
     * @param {*} value
     * @param {*} callback
     */
    static checkNbrZero = (rule, value, callback) => {
        if (common.isNotEmpty(value) && value <= 0) {
            callback('Value error, range larger more than 0.');
        } else {
            callback();
        }
    };

    /**
     * @param {*} value
     * @param {*} callback
     */
    static checkNbrZeroOrNull = (rule, value, callback) => {
        if (common.isEmpty(value)) {
            callback('Please input Value.');
        } else if (value <= 0) {
            callback('Value error, range should more than 0.');
        } else {
            callback();
        }
    };

    /**
     * @param {*} value
     * @param {*} callback
     * @param {*} min
     * @param {*} max
     */
    static checkNbr = (value, callback, min, max) => {
        if (common.isNotEmpty(value)) {
            if (max != null && value > max) {
                callback('Value error, range should more than' + max);
            } else if (min != null && value < min) {
                callback('Value error, range should less than' + min);
            } else if (min == null && value <= 0) {
                callback('Value error, range should equal to 0.');
            } else {
                callback();
            }
        } else {
            callback();
        }
    };

    /**
     * @param {*} value
     * @param {*} callback
     * @param {*} min
     * @param {*} max
     */
    static checkNbrOrNull = (value, callback, min, max) => {
        if (common.isEmpty(value)) {
            callback('Please input Value.');
        } else {
            if (max != null && value > max) {
                callback('Value error, range should more than' + max);
            } else if (min != null && value < min) {
                callback('Value error, range should less than' + min);
            } else if (min == null && value <= 0) {
                callback('Value error, range should equal to 0.');
            } else {
                callback();
            }
        }
    };

    /**
     * @param {*} value
     * @param {*} callback
     * @param {*} min
     * @param {*} max
     */
    static checkNbrNullAndZero = (value, callback, min, max) => {
        if (common.isEmpty(value)) {
            callback('Please input Value.');
        } else {
            if (max != null && value > max) {
                callback('Value error, range should more than' + max);
            } else if (min != null && value < min) {
                callback('Value error, range should less than' + min);
            } else if (min == null && value < 0) {
                callback('Value error, range should equal to 0.');
            } else {
                callback();
            }
        }
    };

    /**
     * @param {*} a 
     * @param {*} b 
     * @param {*} name
     */
    static sortMethod(a, b, name) {
        if (a[name] == null && b[name] != null) {
            return -1;
        } else if (a[name] != null && b[name] == null) {
            return 1;
        } else if (a[name] != null && b[name] != null) {
            var stringA = a[name];
            var stringB = b[name];
            if (a[name].constructor === String || b[name].constructor === String) {
                stringA = stringA.toUpperCase(); // ignore upper and lowercase
                stringB = stringB.toUpperCase();
            }
            if (stringA < stringB) {
                return -1;
            }
            if (stringA > stringB) {
                return 1;
            }
        }
        return 0;
    }

    static timeDifference(startTime, endTime) { 
        let start = typeof (startTime) == "number" ? startTime : new Date(startTime).getTime(),
            end = typeof (endTime) == "number" ? endTime : new Date(endTime).getTime(),
            difference = end - start, 
            days = Math.floor(difference / (24 * 3600 * 1000)); 
            //leave1 = difference % (24 * 3600 * 1000), 
            //hours = Math.floor(leave1 / (3600 * 1000)), 
            //leave2 = leave1 % (3600 * 1000), 
            //minutes = Math.floor(leave2 / (60 * 1000)), 
            //leave3 = leave2 % (60 * 1000), 
            //seconds = Math.round(leave3 / 1000);
        return days
    }

    static loginOvertime() {
        window.location.href = window.location.href.substr(0, window.location.href.lastIndexOf("#/")) + "#/login";
    }

    static downloadExcel = (data, columns, name) => {
      let dataTable: any[] = [], columnWidthList: any[] = [], model, index = -1, headers: any[] = [];
      for (let item of data) {
        model = {};
        index++;
        for (let column of columns) {
          if(common.isNotEmpty(column.dataIndex) && common.isNotEmpty(column.title)) {
            if(column.render != null) {
              if(column.render(item[column.dataIndex], item, index).props != null) {
                model[column.title] = column.render(item[column.dataIndex], item, index).props.children;
              } else {
                model[column.title] = column.render(item[column.dataIndex], item, index);
              }
            } else {
              model[column.title] = item[column.dataIndex];
            }
          }
        }
        dataTable.push(model);
      }
      for (let column of columns) {
        if(common.isNotEmpty(column.dataIndex) && common.isNotEmpty(column.title)) {
          headers.push(column.title);
          columnWidthList.push(column.columnWidth);
        }
      }
      let option = {
          fileName: name + moment(new Date()).format(DateFormat.REPORT),
          datas: [{
              sheetData: dataTable,    
              sheetName: 'sheet',      
              sheetHeader: headers,    
              columnWidths: columnWidthList, 
          }]
      }
      var toExcel = new ExportJsonExcel(option);
      toExcel.saveExcel();
    }

    static parseFormDataFromDateToString = (model: any, arr: string[]) => {
      arr.forEach(o => {
        Object.assign(model, model[o] ? Object.fromEntries([[o, moment(model[o]).format(DateFormat.DATETIME)]]) : "");
      });
      return model;
    };

    static parseFormDataToDateFromString = (model: any, arr: string[]) => {
      arr.forEach(o => {
        Object.assign(model, Object.fromEntries([[o, moment(model[o], DateFormat.DATETIME).toDate()]]));
      });
      return model;
    };
}

