export class utils {
  public static checkNumber(value: any, defaultValue = 0) {
    if (!value || isNaN(value)) {
      return defaultValue;
    } else {
      return value;
    }
  }

  public static isNotEmpty(param) {
    return !this.isEmpty(param);
  }

  public static isEmpty(param) {
    if (param == null) {
      return true;
    }
    let flag = false;
    // 基本类型判断（还可以判断 number,boolean 类型）
    if (typeof param === "string") {
      if (param.trim() === "") {
        flag = true;
      }
    } // 对象类型判断（还可以判断 Object，Function 类型）
    else if (param instanceof Array) {
      if (param.length === 0) {
        flag = true;
      }
    }
    return flag;
  }
}
