import React from "react";
//import logo from "./logo.svg";
import "./App.scss";
import { HashRouter } from "react-router-dom";
import Routes from "./routes";
import Doc from "./Doc";

import { ServicesContext, servicesState} from "./services/contexts/ServicesContext";
const App = () => {
  return (
    <ServicesContext.Provider value={servicesState}>
     <Doc />
      <HashRouter>
        <Routes />
      </HashRouter>
    </ServicesContext.Provider>
  );
}

export default App;
