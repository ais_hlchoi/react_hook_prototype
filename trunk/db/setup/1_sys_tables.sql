CREATE TABLE `SYS_FUNC` (
  `FUNC_ID`             int(10)      NOT NULL AUTO_INCREMENT COMMENT 'Function ID',
  `PARENT_ID`           int(10)      DEFAULT NULL COMMENT 'Parent ID',
  `FUNC_CODE`           varchar(80)  COLLATE utf8_bin DEFAULT NULL COMMENT 'Function Code',
  `FUNC_NAME`           varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT 'Function Name',
  `TYPE`                varchar(10)  COLLATE utf8_bin NOT NULL COMMENT 'Level One: MENU, Level 2 FUNC, Level 3 SUBFUNC',
  `DISP_SEQ`            int(10)      DEFAULT NULL COMMENT 'Display Sequence',
  `FUNC_URL`            varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT 'Function URL',
  `IMAGE`               varchar(50)  COLLATE utf8_bin DEFAULT NULL COMMENT 'Image',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`),
  KEY `SYS_FUNC_TYPE_I` (`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Function'
;

CREATE TABLE `SYS_PARM` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT,
  `SEGMENT`             varchar(100) NOT NULL,
  `CODE`                varchar(150) NOT NULL,
  `PARM_VALUE`          varchar(400) DEFAULT NULL,
  `SHORT_DESC_TC`       varchar(400) DEFAULT NULL,
  `SHORT_DESC`          varchar(400) DEFAULT NULL,
  `LONG_DESC_TC`        varchar(400) DEFAULT NULL,
  `LONG_DESC`           varchar(400) DEFAULT NULL,
  `DISP_SEQ`            int(10)      NOT NULL,
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_PARM_CODE_I` (`CODE`),
  KEY `SYS_PARM_SEGMENT_CODE_I` (`SEGMENT`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

ALTER TABLE `SYS_PARM` ADD CONSTRAINT `SYS_PARM_SEGMENT_CODE_UI` UNIQUE (`SEGMENT`,`CODE`);

CREATE TABLE `SYS_PARM_SEG` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT,
  `SEGMENT`             varchar(100) NOT NULL,
  `PARM_VALUE`          varchar(400) DEFAULT NULL,
  `SHORT_DESC_TC`       varchar(400) DEFAULT NULL,
  `SHORT_DESC`          varchar(400) DEFAULT NULL,
  `LONG_DESC_TC`        varchar(400) DEFAULT NULL,
  `LONG_DESC`           varchar(400) DEFAULT NULL,
  `DISP_SEQ`            int(10)      NOT NULL,
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_PARM_SEG_SEGMENT_I` (`SEGMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE `SYS_ROLE` (
  `ROLE_ID`             int(10)      NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `ROLE_CODE`           varchar(80)  COLLATE utf8_bin NOT NULL COMMENT 'Role Code',
  `ROLE_NAME`           varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Role Name',
  `REMARK`              varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'Remark',
  `DISP_SEQ`            int(10)      NOT NULL COMMENT 'Display Sequence',
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Active Indicator',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Role'
;

CREATE TABLE `SYS_ROLE_MAK` (
  `ROLE_ID`             int(10)      NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `ROLE_CODE`           varchar(80)  COLLATE utf8_bin NOT NULL COMMENT 'Role Code',
  `ROLE_NAME`           varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Role Name',
  `REMARK`              varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'Remark',
  `DISP_SEQ`            int(10)      NOT NULL COMMENT 'Display Sequence',
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Active Indicator',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Role - Maker'
;

CREATE TABLE `SYS_ROLE_FUNC` (
  `ROLE_FUNC_ID`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Role Function ID',
  `ROLE_ID`             int(10)      NOT NULL COMMENT 'Role ID',
  `FUNC_ID`             int(10)      NOT NULL COMMENT 'Function ID',
  `CAN_SELECT`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Search Privileges',
  `CAN_INSERT`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'New Privileges',
  `CAN_UPDATE`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Update Privileges',
  `CAN_DELETE`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Delete Privileges',
  `CAN_AUDIT`           varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Audit Privileges',
  `CAN_VIEW`            varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'View Privileges',
  `CAN_UPLOAD`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Upload Privileges',
  `CAN_DOWNLOAD`        varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Download Privileges',
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Active Indicator',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ROLE_FUNC_ID`),
  KEY `SYS_ROLE_FUNC_ROLE_ID_I` (`ROLE_ID`),
  KEY `SYS_ROLE_FUNC_FUNC_ID_I` (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Role Function Relation'
;

CREATE TABLE `SYS_ROLE_FUNC_MAK` (
  `ROLE_FUNC_ID`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Role Function ID',
  `ROLE_ID`             int(10)      NOT NULL COMMENT 'Role ID',
  `FUNC_ID`             int(10)      NOT NULL COMMENT 'Function ID',
  `CAN_SELECT`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Search Privileges',
  `CAN_INSERT`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'New Privileges',
  `CAN_UPDATE`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Update Privileges',
  `CAN_DELETE`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Delete Privileges',
  `CAN_AUDIT`           varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Audit Privileges',
  `CAN_VIEW`            varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'View Privileges',
  `CAN_UPLOAD`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Upload Privileges',
  `CAN_DOWNLOAD`        varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Download Privileges',
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y' COMMENT 'Active Indicator',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ROLE_FUNC_ID`),
  KEY `SYS_ROLE_FUNC_ROLE_ID_I` (`ROLE_ID`),
  KEY `SYS_ROLE_FUNC_FUNC_ID_I` (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Role Function Relation - Maker'
;

ALTER TABLE `SYS_ROLE_FUNC`     ADD UNIQUE INDEX `SYS_ROLE_FUNC_ROLE_ID_FUNC_ID_UI`     (`ROLE_ID`,`FUNC_ID`);
ALTER TABLE `SYS_ROLE_FUNC_MAK` ADD UNIQUE INDEX `SYS_ROLE_FUNC_MAK_ROLE_ID_FUNC_ID_UI` (`ROLE_ID`,`FUNC_ID`);

CREATE TABLE `SYS_ROLE_AUD` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `OP_ID`               varchar(30)  DEFAULT NULL,
  `OP_TYPE_IND`         varchar(10)  DEFAULT NULL,
  `SYS_DATE`            datetime(6)  DEFAULT NULL,
  `ROLE_ID`             int(10)      NOT NULL,
  `ROLE_CODE`           varchar(80)  COLLATE utf8_bin NOT NULL,
  `ROLE_NAME`           varchar(100) COLLATE utf8_bin NOT NULL,
  `REMARK`              varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `DISP_SEQ`            int(10)      NOT NULL,
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

CREATE TABLE `SYS_ROLE_FUNC_AUD` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `OP_ID`               varchar(30)  DEFAULT NULL,
  `OP_TYPE_IND`         varchar(10)  DEFAULT NULL,
  `SYS_DATE`            datetime(6)  DEFAULT NULL,
  `ROLE_FUNC_ID`        int(10) UNSIGNED NOT NULL,
  `ROLE_ID`             int(10)      NOT NULL,
  `FUNC_ID`             int(10)      NOT NULL,
  `CAN_SELECT`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_INSERT`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_UPDATE`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_DELETE`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_AUDIT`           varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_VIEW`            varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_UPLOAD`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CAN_DOWNLOAD`        varchar(1)   NOT NULL DEFAULT 'Y',
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y',
  `STAT_IND`            varchar(1)   DEFAULT NULL,
  `AMEND_IND`           varchar(1)   DEFAULT NULL,
  `LAST_INIT_ID`        varchar(100) DEFAULT NULL,
  `LAST_INIT_DATE`      datetime(6)  DEFAULT NULL,
  `LAST_POST_ID`        varchar(100) DEFAULT NULL,
  `LAST_POST_DATE`      datetime(6)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

CREATE TABLE `SYS_OPERATION_LOG` (
  `OPERATION_LOG_ID`    int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Operation Log ID',
  `USER_ID`             varchar(100) NOT NULL COMMENT 'User ID',
  `FUNC_ID`             int(10)      DEFAULT NULL COMMENT 'Functinon ID',
  `OPERATION_TYPE`      varchar(50)  COLLATE utf8_bin NOT NULL COMMENT 'Operation Type',
  `SERVICE_ADDRESS`     varchar(20)  COLLATE utf8_bin DEFAULT NULL COMMENT 'Service Address',
  `STATUS`              varchar(20)  COLLATE utf8_bin NOT NULL COMMENT 'Result (success/fail',
  `LOGIN_IP_ADDRESS`    varchar(20)  COLLATE utf8_bin DEFAULT NULL COMMENT 'Login IP Address',
  `OPERATION_CONTENT`   longtext     COLLATE utf8_bin COMMENT 'Operation Content',
  `OPERATION_TIME`      datetime     NOT NULL COMMENT 'Operation Time',
  `RESPONSE_CODE`       varchar(10)  COLLATE utf8_bin DEFAULT NULL,
  `RESPONSE_DESC`       longtext     COLLATE utf8_bin,
  `RESPONSE_TIME`       datetime     DEFAULT NULL,
  `REQUEST_URL`         varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT 'request Url',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`OPERATION_LOG_ID`),
  KEY `OPERATION_LOG_FUNC_ID_I` (`FUNC_ID`),
  KEY `OPERATION_LOG_USER_ID_I` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Operation Log'
;

ALTER TABLE `SYS_ROLE_FUNC`       ADD CONSTRAINT `SYS_ROLE_FUNC_FUNC_ID_FK`           FOREIGN KEY (`FUNC_ID`)         REFERENCES `SYS_FUNC`    (`FUNC_ID`);
ALTER TABLE `SYS_ROLE_FUNC`       ADD CONSTRAINT `SYS_ROLE_FUNC_ROLE_ID_FK`           FOREIGN KEY (`ROLE_ID`)         REFERENCES `SYS_ROLE`    (`ROLE_ID`);
ALTER TABLE `SYS_ROLE_FUNC_MAK`   ADD CONSTRAINT `SYS_ROLE_FUNC_MAK_FUNC_ID_FK`       FOREIGN KEY (`FUNC_ID`)         REFERENCES `SYS_FUNC`    (`FUNC_ID`);
ALTER TABLE `SYS_ROLE_FUNC_MAK`   ADD CONSTRAINT `SYS_ROLE_FUNC_MAK_ROLE_ID_FK`       FOREIGN KEY (`ROLE_ID`)         REFERENCES `SYS_ROLE_MAK`(`ROLE_ID`);
ALTER TABLE `SYS_OPERATION_LOG`   ADD CONSTRAINT `SYS_OPERATION_LOG_FUNC_ID_FK`       FOREIGN KEY (`FUNC_ID`)         REFERENCES `SYS_FUNC`    (`FUNC_ID`);

-- [ AUD audit table should not have any constraint to master table ]
-- ALTER TABLE `SYS_ROLE_FUNC_AUD`   ADD CONSTRAINT `SYS_ROLE_FUNC_AUD_FUNC_ID_FK`       FOREIGN KEY (`FUNC_ID`)         REFERENCES `SYS_FUNC`    (`FUNC_ID`);
-- ALTER TABLE `SYS_ROLE_FUNC_AUD`   ADD CONSTRAINT `SYS_ROLE_FUNC_AUD_ROLE_ID_FK`       FOREIGN KEY (`ROLE_ID`)         REFERENCES `SYS_ROLE`    (`ROLE_ID`);

----------------------------------------------------------------------------------------------------

CREATE TABLE `SYS_USER_SESS` (
  `USER_ID`             varchar(100) NOT NULL,
  `SID`                 varchar(500) DEFAULT NULL,
  `IP`                  varchar(50)  DEFAULT NULL,
  `LOGIN_TIME`          datetime(6)  DEFAULT NULL,
  `LAST_ACTIVE_TIME`    datetime(6)  DEFAULT NULL,
  KEY `SYS_USER_SESS_USER_ID_I` (`USER_ID`),
  KEY `SYS_USER_SESS_SID_I` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE `SYS_USER_SESS_AUD` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `USER_ID`             varchar(100) NOT NULL,
  `SID`                 varchar(500) NOT NULL,
  `IP`                  varchar(50)  NOT NULL,
  `LOGIN_TIME`          datetime(6)  DEFAULT NULL,
  `SID_AUD`             varchar(500) NOT NULL,
  `IP_AUD`              varchar(50)  NOT NULL,
  `LOGOUT_TIME`         datetime(6)  DEFAULT NULL,
  `LOGOUT_MODE`         varchar(1)   NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_USER_SESS_AUD_USER_ID_I` (`USER_ID`),
  KEY `SYS_USER_SESS_AUD_SID_I` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

----------------------------------------------------------------------------------------------------

CREATE TABLE `SYS_USER_FUNC_PREF` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT COMMENT 'Preference ID',
  `USER_ID`             varchar(100) NOT NULL COMMENT 'User ID',
  `FUNC_ID`             int(10)      NOT NULL COMMENT 'Functino ID',
  `DISP_SEQ`            int(10)      NOT NULL COMMENT 'Display Sequence',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_USER_FUNC_PREF_FUNC_ID_I` (`FUNC_ID`),
  KEY `SYS_USER_FUNC_PREF_USER_ID_I` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User Preference'
;

ALTER TABLE `SYS_USER_FUNC_PREF`  ADD CONSTRAINT `SYS_USER_FUNC_PREF_FUNC_ID_FK`      FOREIGN KEY (`FUNC_ID`)         REFERENCES `SYS_FUNC`    (`FUNC_ID`);


CREATE TABLE `SYS_FUNC_API` (
  `FUNC_API_ID`         int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Function API ID',
  `FUNC_ID`             int(10) NOT NULL COMMENT 'Function ID',
  `END_POINT`           varchar(100) NOT NULL COMMENT 'API Endpoint',	
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`FUNC_API_ID`),
  KEY `SYS_FUNC_API_FUNC_ID_I` (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Function API Endpoint Relation'
;

ALTER TABLE `SYS_FUNC_API` ADD CONSTRAINT `SYS_FUNC_API_FUNC_ID_FK` FOREIGN KEY (`FUNC_ID`) REFERENCES `SYS_FUNC` (`FUNC_ID`);


ALTER TABLE SYS_PARM ADD
(  "STAT_IND" varchar(1) COLLATE utf8_bin DEFAULT NULL,
  "AMEND_IND" varchar(1) COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_ID" varchar(100) COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_DATE" datetime(6) DEFAULT NULL,
  "LAST_POST_ID" varchar(100) COLLATE utf8_bin DEFAULT NULL,
  "LAST_POST_DATE" datetime(6) DEFAULT NULL);
  
CREATE TABLE SYS_PARM_MAK (
  "ID" int(10) AUTO_INCREMENT NOT NULL,
  "SEGMENT" varchar(100) NOT NULL,
  "CODE" varchar(150) NOT NULL,
  "PARM_VALUE" varchar(400) DEFAULT NULL,
  "SHORT_DESC_TC" varchar(400) DEFAULT NULL,
  "SHORT_DESC" varchar(400) DEFAULT NULL,
  "LONG_DESC_TC" varchar(400) DEFAULT NULL,
  "LONG_DESC" varchar(400) DEFAULT NULL,
  "DISP_SEQ" int(10) NOT NULL,
  "ACTIVE_IND" varchar(1) NOT NULL DEFAULT 'Y',
  "CREATED_BY" varchar(100) DEFAULT NULL,
  "CREATED_DATE" datetime(6) DEFAULT NULL,
  "LAST_UPDATED_BY" varchar(100) DEFAULT NULL,
  "LAST_UPDATED_DATE" datetime(6) DEFAULT NULL,
  "STAT_IND" varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "AMEND_IND" varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_ID" varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_DATE" datetime(6) DEFAULT NULL,
  "LAST_POST_ID" varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_POST_DATE" datetime(6) DEFAULT NULL,
  PRIMARY KEY ("ID"),
  UNIQUE KEY "SYS_PARM_MAK_SEGMENT_CODE_UI" ("SEGMENT","CODE"),
  KEY "SYS_PARM_MAK_CODE_I" ("CODE"),
  KEY "SYS_PARM_MAK_SEGMENT_CODE_I" ("SEGMENT","CODE")
)  ;


CREATE TABLE SYS_PARM_AUD (  
  "ID" int(10) AUTO_INCREMENT NOT NULL,
  "OP_ID" varchar(30) COLLATE utf8_bin DEFAULT NULL,
  "OP_TYPE_IND" varchar(10) COLLATE utf8_bin DEFAULT NULL,  
  "SYS_PARM_ID" int(10) NOT NULL,
  "SEGMENT" varchar(100) NOT NULL,
  "CODE" varchar(150) NOT NULL,
  "PARM_VALUE" varchar(400) DEFAULT NULL,
  "SHORT_DESC_TC" varchar(400) DEFAULT NULL,
  "SHORT_DESC" varchar(400) DEFAULT NULL,
  "LONG_DESC_TC" varchar(400) DEFAULT NULL,
  "LONG_DESC" varchar(400) DEFAULT NULL,
  "DISP_SEQ" int(10) NOT NULL,
  "ACTIVE_IND" varchar(1) NOT NULL DEFAULT 'Y',
  "CREATED_BY" varchar(100) DEFAULT NULL,
  "CREATED_DATE" datetime(6) DEFAULT NULL,
  "LAST_UPDATED_BY" varchar(100) DEFAULT NULL,
  "LAST_UPDATED_DATE" datetime(6) DEFAULT NULL,
  "STAT_IND" varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "AMEND_IND" varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_ID" varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_INIT_DATE" datetime(6) DEFAULT NULL,
  "LAST_POST_ID" varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  "LAST_POST_DATE" datetime(6) DEFAULT NULL,
  PRIMARY KEY ("ID")
)  ;  

