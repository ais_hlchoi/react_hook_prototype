DROP TABLE IF EXISTS `DATA_ALLOCATIONS_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_EXCHANGE_ORDER_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_SOR_AUDIT_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_TRADE_REPORT_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_USER_LOGON_EXT`;
DROP TABLE IF EXISTS `DATA_ARC_SETTINGS_SERVICE_STRATEGY_EXT`;
DROP TABLE IF EXISTS `DATA_BOOKS_EXT`;
DROP TABLE IF EXISTS `DATA_CHARGE_RULES_EXT`;
DROP TABLE IF EXISTS `DATA_CLIENTS_EXT`;
DROP TABLE IF EXISTS `DATA_CLOSING_PRICE_EXT`;
DROP TABLE IF EXISTS `DATA_EXCHANGE_CONNECTIONS_EXT`;
DROP TABLE IF EXISTS `DATA_FRONT_OFFICE_TRADES_EXT`;
DROP TABLE IF EXISTS `DATA_GROUPS_EXT`;
DROP TABLE IF EXISTS `DATA_INSTRUMENTS_EXT`;
DROP TABLE IF EXISTS `DATA_LOGON_AUDIT_TRAIL_EXT`;
DROP TABLE IF EXISTS `DATA_MARKET_SIDE_COUNTERPARTIES_EXT`;
DROP TABLE IF EXISTS `DATA_MIS_NEW_INSTRUMENTS_EXT`;
DROP TABLE IF EXISTS `DATA_ORDER_PROGRESS_EXT`;
DROP TABLE IF EXISTS `DATA_STATIC_DATA_AUDIT_EXT`;
DROP TABLE IF EXISTS `DATA_SUB_ACCOUNTS_EXT`;
DROP TABLE IF EXISTS `DATA_USERS_EXT`;
DROP TABLE IF EXISTS `DATA_USER_EXCHANGE_CONNECTIONS_EXT`;
DROP TABLE IF EXISTS `DATA_USER_GROUP_RIGHTS_EXT`;
DROP TABLE IF EXISTS `DATA_USER_MARKET_ROUTES_EXT`;

DROP TABLE IF EXISTS `DATA_ALLOCATIONS`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_EXCHANGE_ORDER`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_SOR_AUDIT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_TRADE_REPORT`;
DROP TABLE IF EXISTS `DATA_ARC_EVENTS_USER_LOGON`;
DROP TABLE IF EXISTS `DATA_ARC_SETTINGS_SERVICE_STRATEGY`;
DROP TABLE IF EXISTS `DATA_BOOKS`;
DROP TABLE IF EXISTS `DATA_CHARGE_RULES`;
DROP TABLE IF EXISTS `DATA_CLIENTS`;
DROP TABLE IF EXISTS `DATA_CLOSING_PRICE`;
DROP TABLE IF EXISTS `DATA_EXCHANGE_CONNECTIONS`;
DROP TABLE IF EXISTS `DATA_FRONT_OFFICE_TRADES`;
DROP TABLE IF EXISTS `DATA_GROUPS`;
DROP TABLE IF EXISTS `DATA_INSTRUMENTS`;
DROP TABLE IF EXISTS `DATA_LOGON_AUDIT_TRAIL`;
DROP TABLE IF EXISTS `DATA_MARKET_SIDE_COUNTERPARTIES`;
DROP TABLE IF EXISTS `DATA_MIS_NEW_INSTRUMENTS`;
DROP TABLE IF EXISTS `DATA_ORDER_PROGRESS`;
DROP TABLE IF EXISTS `DATA_STATIC_DATA_AUDIT`;
DROP TABLE IF EXISTS `DATA_SUB_ACCOUNTS`;
DROP TABLE IF EXISTS `DATA_USERS`;
DROP TABLE IF EXISTS `DATA_USER_EXCHANGE_CONNECTIONS`;
DROP TABLE IF EXISTS `DATA_USER_GROUP_RIGHTS`;
DROP TABLE IF EXISTS `DATA_USER_MARKET_ROUTES`;

DROP TABLE IF EXISTS `RPT_FILE_UPLD`;

DROP TABLE IF EXISTS `RPT_SRC_FILE_HDR_COL`;
DROP TABLE IF EXISTS `RPT_SRC_FILE_HDR`;
DROP TABLE IF EXISTS `RPT_SRC_FILE`;

CREATE TABLE `RPT_SRC_FILE` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT,
  `DATASOURCE`          varchar(10)  DEFAULT NULL,
  `FILE_NAME_START`     varchar(50)  DEFAULT NULL,
  `FILE_DESC`           varchar(100) DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RPT_SRC_FILE_FILE_NAME_START_I` (`FILE_NAME_START`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;

CREATE TABLE `RPT_SRC_FILE_HDR` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT,
  `SRC_FILE_ID`         int(10)      NOT NULL,
  `EFFECTIVE_DATE`      datetime     NOT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RPT_SRC_FILE_HDR_SRC_FILE_EFFECTIVE_DATE_I` (`SRC_FILE_ID`,`EFFECTIVE_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;

CREATE TABLE `RPT_SRC_FILE_HDR_COL` (
  `ID`                  int(10)      NOT NULL AUTO_INCREMENT,
  `SRC_FILE_HDR_ID`     int(10)      NOT NULL,
  `COLUMN_IDX`          int(10)      NOT NULL,
  `COLUMN_NAME`         varchar(100) NOT NULL,
  `COLUMN_DESC`         varchar(100) NOT NULL,
  `STATIC_IND`          varchar(1)   NOT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RPT_SRC_FILE_HDR_COL_FILE_HDR_COLUMN_NAME_I` (`SRC_FILE_HDR_ID`,`COLUMN_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;

CREATE TABLE `RPT_FILE_UPLD` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SRC_FILE_ID`         int(10)      NOT NULL,
  `SRC_FILE_HDR_ID`     int(10)      DEFAULT NULL,
  `FILE_NAME`           varchar(250) DEFAULT NULL,
  `UPLD_DATE`           datetime     NOT NULL,
  `STATUS`              varchar(1)   NOT NULL DEFAULT 'I',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RPT_FILE_UPLD_STATUS_I` (`STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;

ALTER TABLE `RPT_FILE_UPLD`          ADD CONSTRAINT `RPT_FILE_UPLD_SRC_FILE_ID_FK`           FOREIGN KEY (`SRC_FILE_ID`)     REFERENCES `RPT_SRC_FILE`     (`ID`);
ALTER TABLE `RPT_FILE_UPLD`          ADD CONSTRAINT `RPT_FILE_UPLD_SRC_FILE_HDR_ID_FK`       FOREIGN KEY (`SRC_FILE_HDR_ID`) REFERENCES `RPT_SRC_FILE_HDR` (`ID`);

CREATE TABLE `DATA_ALLOCATIONS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `RECORD_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATE`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `QUANTITY`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_CODE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ISIN_CODE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SEDOL_CODE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_PRICE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NET_NET_PRICE`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_CONSIDERATION`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NET_NET_CONSIDERATION`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SETTLEMENT_CONSIDERATION`       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_DATE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_TIME`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION_NUMBER`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEALT_CCY`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SETTLEMENT_CCY`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_BY`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADER`                         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SUB_ACCOUNT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PTM_LEVY`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OASYS_REF`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NOTES`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_COUNTERPARTY`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_COUNTERPARTY_CRF2`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_SUB_ACCOUNT_CRF2`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORTING_EXCHANGE_ID`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BOOKING_TRADE_ID`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_DATETIME_GMT`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ALLOCATIONS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ALLOCATIONS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ALLOCATIONS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_EXCHANGE_ORDER` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OBJECT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_REFERENCES`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_OBJECTS`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_SETTINGS`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_MEMBER`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_SESSION`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_TRADER`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_CLIENT_ORDER_ID`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_REMAINING_VOLUME`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_MESSAGE_TYPE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_CUMULATIVE_QUANTITY`       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_ON_BEHALF_COMP_ID`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_ON_BEHALF_SUB_ID`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_ON_BEHALF_LOC_ID`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_TRANSACTION_TIME`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_MARKET_PARTICIPANT_ID`     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_ORDER_STATE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_SELF_MATCH_CODE`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_CANCELLED_VOLUME`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_REGULATORY_ID`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SENDER_LOCATION_ID`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_DATETIME`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MANUAL_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MEMBER_IDENTIFIER`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLEARING_ORDER_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SECONDARY_CLEARING_CODE`        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COMPLETION_REASON_QUALIFIER`    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REASON_TEXT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REGULATORY_ALGO_ID`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DESTINATION_EXCHANGE_ID`        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_EXCHANGE_ORDER_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_EXCHANGE_ORDER_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_EVENTS-Exchange_Order'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OBJECT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_REFERENCES`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_OBJECTS`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_SETTINGS`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_FIM_CODE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_EXPIRY_DATE`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_TRADED_CCY`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_DESCRIPTION`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CFI_CODE`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OPTION_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STRIKE_PRICE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRACT_SIZE`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MARKET_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL_QUALIFIER`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_BY`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_DATETIME`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_PRICE_TYPE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_PRICE_TYPE_QUALIFIER`     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADING_PERIOD`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LIMIT_PRICE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STOP_PRICE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADING_QUANTITY`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DISPLAY_QUANTITY`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `QUANTITY_FILLED`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AVERAGE_FILL_PRICE`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEALT_CURRENCY_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_DATETIME`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CPTY_ACCOUNT_CODE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_EVENTS-Order_and_Trade_Flow'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_SOR_AUDIT` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OBJECT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_REFERENCES`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_OBJECTS`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_SETTINGS`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_SOR_AUDIT_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_SOR_AUDIT_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_EVENTS-SOR_Audit'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_TRADE_REPORT` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OBJECT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_REFERENCES`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_OBJECTS`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_SETTINGS`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_DESTINATION_ID`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_STREAM`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORTED_TIMESTAMP`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_DATETIME`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VALUE_DATE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LAST_MARKET`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_VENUE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_FIM_CODE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CURRENCY_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_PRICE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_PRICE_TYPE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `QUANTITY`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_QUANTITY_TYPE`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_TRADE_FLAGS`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_FIRM_VIEW_CODE`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_COUNTERPARTY_VIEW_CODE`  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_CAPACITY`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_ORDER_CLASS`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NUMBER_OF_LEGS`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `WAIVER_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_STATUS`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_STATUS_QUALIFIER`        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_TEXT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ERROR_CODE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ERROR_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_TRADE_REPORT_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_TRADE_REPORT_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_EVENTS-Trade_Report'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_USER_LOGON` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OBJECT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_REFERENCES`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_OBJECTS`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RELATED_SETTINGS`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `USER_NAME`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_PEER_ADDR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_IP_ADDR`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ACTING_AS`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_USER_LOGON_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_USER_LOGON_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_EVENTS-User_Logon'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ARC_SETTINGS_SERVICE_STRATEGY` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INTERNAL_SEQUENCE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TIMESTAMP`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRIBUTOR_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SETTING_TYPE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SETTING_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TYPE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_AUDITOR`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EVENT_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_STRATEGY_NAME`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_STRATEGY_TYPE`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTOR_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SCOPE`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS`                         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_0`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_1`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_2`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_3`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_4`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_5`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_6`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_7`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_8`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_9`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_10`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_11`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_12`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_13`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_14`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_15`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_16`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_17`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_18`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_19`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_20`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_21`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `XP_DATA_22`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ARC_SETTINGS_SERVICE_STRATEGY_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_SETTINGS_SERVICE_STRATEGY_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ARC_SETTINGS-Service_Strategy'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_BOOKS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `BOOK_MNEMONIC`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BOOK_DESCRIPTION`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BOOK_TYPE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_BOOK`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OWNING_ENTITY`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CURRENCY`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LONG_VALUE_LIMIT`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SHORT_VALUE_LIMIT`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_1`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_2`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BACK_OFFICE_CODE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_BOOKS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_BOOKS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='BOOKS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_CHARGE_RULES` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `CHARGE_RULE_ID`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_NAME`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRANSACTION_TYPE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY_TYPE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_MNEMONIC`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_TYPE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FIRM_BUY_SELL`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_VALUE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_METHOD`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_CURRENCY`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_BASIS`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ROUNDING`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ROUNDING_PRECISION`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MINIMUM_CHARGE`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MAXIMUM_CHARGE`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PAID_RECEIVED`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INCLUSIVE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_CHARGE_RULES_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CHARGE_RULES_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='CHARGE_RULES'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_CLIENTS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `CLIENT_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_MNEMONIC`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_DESCRIPTION`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_GROUP`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_SALES_TRADER`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPRESENTATIVE`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `HAS_ACCOUNT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTRY_OF_DOMICILE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_PRICE_BASIS`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AGREED_PRICE_BASIS`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONFIRMATION_METHOD`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONFIRMATION_LEVEL`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DECIMAL_PLACES`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_SYMBOLOGY`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CHARGE_PRESENTATION`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALLOW_WAREHOUSING`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `WAREHOUSE_DURATION_BUY`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `WAREHOUSE_DURATION_SELL`        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `WAREHOUSE_LIMIT_BUY`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `WAREHOUSE_LIMIT_SELL`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `OASYS`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALERT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FUND`                           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PERSHING`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_1`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_2`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BACK_OFFICE_CODE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PERMISSIONING_GROUP`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPRESENTATIVE_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AGGREGATION_MODEL`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTRY_OF_ORIGIN`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_CLIENTS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CLIENTS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='CLIENTS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_CLOSING_PRICE` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `STOCK_CODE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STOCK_CLOSING_PRICE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STOCK_NAME`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MARKET_CODE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ISIN_CODE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LOT_SIZE`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CURRENCY_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALLOW_SHORTSELL`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BEST_BID_PRICE`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BEST_ASK_PRICE`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_TYPE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_CLOSING_PRICE_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CLOSING_PRICE_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='CLOSING_PRICE'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_EXCHANGE_CONNECTIONS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `EXCH_CONNECTION_ID`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_CODE`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ROUTE_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADING_ENTITY_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_AGENCY_BOOK_ID`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_MARKET_MAKER_BOOK_ID`   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_PRINCIPAL_BOOK_ID`      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `IS_SINGLE_USER`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FDM_ACTIVE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_EXCHANGE_CONNECTIONS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_EXCHANGE_CONNECTIONS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='EXCHANGE_CONNECTIONS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_FRONT_OFFICE_TRADES` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `RECORD_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATE`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `QUANTITY`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_CODE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ISIN_CODE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_ID`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_PRICE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_CONSIDERATION`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_DATE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_TIME`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION_NUMBER`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEALT_CCY`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEALT_TO_SETTLEMENT_RATE`       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_BY`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_TRADE_CODE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BOOK_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADER`                         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `SUB_ACCOUNT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NOTES`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_COUNTERPARTY`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_SUB_ACCOUNT`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADING_ENTITY_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BO_ORDER_COUNTERPARTY`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_COUNTERPARTY`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADER_REF`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_PRICE_TYPE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPORTING_EXCHANGE_ID`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADE_DATETIME_GMT`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_FRONT_OFFICE_TRADES_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_FRONT_OFFICE_TRADES_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='FRONT_OFFICE_TRADES'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_GROUPS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `USER_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `USER_NAME`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RISK_GROUP_ID`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `IS_HANDOVER_GROUP`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS`                         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_GROUPS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_GROUPS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='GROUPS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_INSTRUMENTS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `INSTRUMENT_ID`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FIM`                            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ISIN`                           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_DESCRIPTION`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_TYPE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_TRADER`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MARKET_MAKER`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_BOOK`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_MNEMONIC`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_INSTRUMENTS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_INSTRUMENTS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='INSTRUMENTS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_LOGON_AUDIT_TRAIL` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `LOGON_AUDIT_DATETIME`           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_PEER_ADDR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_NAME`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LOGON_EVENT`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LOGON_TEXT`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `USER_NAME`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `APPLICATION`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LOCATION`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_LOGON_AUDIT_TRAIL_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_LOGON_AUDIT_TRAIL_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='LOGON_AUDIT_TRAIL'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_MARKET_SIDE_COUNTERPARTIES` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `COUNTERPARTY_ID`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY_MNEMONIC`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY_DESCRIPTION`       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_QUALIFIER`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTRY_OF_DOMICILE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_MNEMONIC`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_IDENTIFIER`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_1`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_2`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BACK_OFFICE_CODE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AGGREGATION_MODEL`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BIC_CODE`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_MARKET_SIDE_COUNTERPARTIES_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_MARKET_SIDE_COUNTERPARTIES_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='MARKET_SIDE_COUNTERPARTIES'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_MIS_NEW_INSTRUMENTS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `FIM`                            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_MIS_NEW_INSTRUMENTS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_MIS_NEW_INSTRUMENTS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='MIS_NEW_INSTRUMENTS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ORDER_PROGRESS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `ORDERSTATE`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONTRACTING_ENTITY`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `INSTRUMENT_CODE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ISIN_CODE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MARKET_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY_CODE`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTERPARTY_DESCRIPTION`       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ORDER_ID`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VERSION`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BUY_SELL`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TOTAL_QUANTITY`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `QUANTITY_FILLED`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LIMIT_PRICE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROSS_FILL_PRICE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEALT_CCY`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AMENDED_DATE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AMENDED_TIME`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_DATE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_TIME`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_BY`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REPRESENTATIVE_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NUM_FILLS`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_DATE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ENTERED_TIME`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `GROUP_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_PRICE_TYPE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_ENTRY_TYPE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `AMENDED_BY`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_ORDER_PROGRESS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ORDER_PROGRESS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ORDER_PROGRESS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_STATIC_DATA_AUDIT` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `RTD_KEY_1`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_KEY_2`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_KEY_3`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_TABLE_NAME`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_FIELD_NAME`                 varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_OPERATION`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_FIELD_VALUE_BEFORE`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RTD_FIELD_VALUE_AFTER`          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_STATIC_DATA_AUDIT_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_STATIC_DATA_AUDIT_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='STATIC_DATA_AUDIT'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_SUB_ACCOUNTS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `CLIENT_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CLIENT_MNEMONIC`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ACCOUNT_ID`                     varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ACCOUNT_MNEMONIC`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ACCOUNT_DESCRIPTION`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ACCOUNT_TYPE`                   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `REGISTERED_CHARITY`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `COUNTRY_OF_DOMICILE`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CONFIRMATION_METHOD`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ALERT`                          varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FUND`                           varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PERSHING`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_1`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `CUSTOMER_CODE_2`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_INDICATOR`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `BACK_OFFICE_CODE`               varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_SUB_ACCOUNTS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_SUB_ACCOUNTS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SUB_ACCOUNTS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_USERS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `USER_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PRIMARY_GROUP`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `HANDOVER_GROUP_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `USER_ROLE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `LOGON_LOCATION_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `STATUS`                         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `USER_TYPE`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_ACCESS_RIGHTS`            varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_USERS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USERS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='USERS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_USER_EXCHANGE_CONNECTIONS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `USER_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCH_CONNECTION_ID`             varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `EXCHANGE_CODE`                  varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ROUTE_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PREFERRED_ROUTE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_USER_EXCHANGE_CONNECTIONS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_EXCHANGE_CONNECTIONS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='USER_EXCHANGE_CONNECTIONS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_USER_GROUP_RIGHTS` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `USER_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RIGHTS_TYPE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `RIGHTS_CODE`                    varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_USER_GROUP_RIGHTS_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_GROUP_RIGHTS_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='USER_GROUP_RIGHTS'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_USER_MARKET_ROUTES` (
  `FILE_UPLD_ID`                   int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`                       int(10) NOT NULL,
  `PARTITION_IND`                  varchar(20) DEFAULT NULL,
  `CREATED_DATE`                   datetime DEFAULT CURRENT_TIMESTAMP,
  `LATEST_VERSION_IND`             varchar(1)  DEFAULT NULL,
  `USER_ID`                        varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `MARKET_ID`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ROUTE_ID`                       varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `PREFERRED_ROUTE`                varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TRADING_ENTITY_ID`              varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_AGENCY_BOOK_ID`         varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_MARKET_MAKER_BOOK_ID`   varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `DEFAULT_PRINCIPAL_BOOK_ID`      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `TIMESTAMP`                      varchar(50) COLLATE utf8_bin DEFAULT NULL,
  KEY `DATA_USER_MARKET_ROUTES_FILE_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_MARKET_ROUTES_LINE_SEQ_I`(`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='USER_MARKET_ROUTES'
PARTITION BY KEY(`PARTITION_IND`);

CREATE TABLE `DATA_ALLOCATIONS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ALLOCATIONS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ALLOCATIONS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_EXCHANGE_ORDER_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_EXCHANGE_ORDER_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_EXCHANGE_ORDER_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_ORDER_AND_TRADE_FLOW_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_SOR_AUDIT_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_SOR_AUDIT_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_SOR_AUDIT_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_TRADE_REPORT_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_TRADE_REPORT_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_TRADE_REPORT_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_EVENTS_USER_LOGON_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_EVENTS_USER_LOGON_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_EVENTS_USER_LOGON_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ARC_SETTINGS_SERVICE_STRATEGY_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ARC_SETTINGS_SERVICE_STRATEGY_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ARC_SETTINGS_SERVICE_STRATEGY_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_BOOKS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_BOOKS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_BOOKS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_CHARGE_RULES_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_CHARGE_RULES_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CHARGE_RULES_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_CLIENTS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_CLIENTS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CLIENTS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_CLOSING_PRICE_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_CLOSING_PRICE_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_CLOSING_PRICE_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_EXCHANGE_CONNECTIONS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_EXCHANGE_CONNECTIONS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_EXCHANGE_CONNECTIONS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_FRONT_OFFICE_TRADES_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_FRONT_OFFICE_TRADES_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_FRONT_OFFICE_TRADES_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_GROUPS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_GROUPS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_GROUPS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_INSTRUMENTS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_INSTRUMENTS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_INSTRUMENTS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_LOGON_AUDIT_TRAIL_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_LOGON_AUDIT_TRAIL_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_LOGON_AUDIT_TRAIL_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_MARKET_SIDE_COUNTERPARTIES_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_MARKET_SIDE_COUNTERPARTIES_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_MARKET_SIDE_COUNTERPARTIES_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_MIS_NEW_INSTRUMENTS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_MIS_NEW_INSTRUMENTS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_MIS_NEW_INSTRUMENTS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_ORDER_PROGRESS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_ORDER_PROGRESS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_ORDER_PROGRESS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_STATIC_DATA_AUDIT_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_STATIC_DATA_AUDIT_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_STATIC_DATA_AUDIT_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_SUB_ACCOUNTS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_SUB_ACCOUNTS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_SUB_ACCOUNTS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_USERS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_USERS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USERS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_USER_EXCHANGE_CONNECTIONS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_USER_EXCHANGE_CONNECTIONS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_EXCHANGE_CONNECTIONS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_USER_GROUP_RIGHTS_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_USER_GROUP_RIGHTS_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_GROUP_RIGHTS_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `DATA_USER_MARKET_ROUTES_EXT` (
  `FILE_UPLD_ID`        int(10) UNSIGNED NOT NULL,
  `LINE_SEQ`            int(10)      NOT NULL,
  `DYNAMIC_COLS`        blob         DEFAULT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  KEY `DATA_USER_MARKET_ROUTES_EXT_FILD_UPLD_ID_I` (`FILE_UPLD_ID`),
  KEY `DATA_USER_MARKET_ROUTES_EXT_LINE_SEQ_I` (`LINE_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (`PARTITION_IND`);


INSERT INTO `RPT_SRC_FILE`
(CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, DATASOURCE, FILE_NAME_START, FILE_DESC) VALUES
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ALLOCATIONS'                    , 'Allocation Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_EVENTS_EXCHANGE_ORDER'      , 'Archived Exchange Order Data'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_EVENTS_ORDER_AND_TRADE_FLOW', 'Archived Order and Trade Flow'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_EVENTS_SOR_AUDIT'           , 'Archived SOR Audit'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_EVENTS_TRADE_REPORT'        , 'Archived Trade Report Data'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_EVENTS_USER_LOGON'          , 'Archived User Logon Data'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ARC_SETTINGS_SERVICE_STRATEGY'  , 'Archived setting service strategy'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'BOOKS'                          , 'Book Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'CHARGE_RULES'                   , 'Charge Rules Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'CLIENTS'                        , 'Clients Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'CLOSING_PRICE'                  , 'Closing Price Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'EXCHANGE_CONNECTIONS'           , 'Exchange Connections Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'FRONT_OFFICE_TRADES'            , 'Front Office Trades Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'GROUPS'                         , 'Groups Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'INSTRUMENTS'                    , 'Instruments Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'LOGON_AUDIT_TRAIL'              , 'Logon Audit Trail Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'MARKET_SIDE_COUNTERPARTIES'     , 'Market Side Counterparties Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'MIS_NEW_INSTRUMENTS'            , 'Mis New Instruments Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'ORDER_PROGRESS'                 , 'Order Progress Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'STATIC_DATA_AUDIT'              , 'Static Data Audit Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'SUB_ACCOUNTS'                   , 'Sub Accounts Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'USER_EXCHANGE_CONNECTIONS'      , 'User Exchange Connections Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'USER_GROUP_RIGHTS'              , 'User Group Rights Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'USER_MARKET_ROUTES'             , 'User Market Routes Records'),
('SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, 'FIDESSA', 'USERS'                          , 'User Records')
;
