CREATE TABLE `SYS_EMAIL` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `TYPE`                varchar(50)  NOT NULL,
  `SUBJECT`             varchar(200) COLLATE utf8_bin NOT NULL,
  `RECIPIENT`           varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `RECIPIENT_CC`        varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `RECIPIENT_BCC`       varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `SEND_DATE`           datetime     DEFAULT NULL,
  `REMARK`              varchar(400) DEFAULT NULL,
  `STATUS`              varchar(1)   NOT NULL DEFAULT 'I',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_EMAIL_TYPE_I` (`TYPE`),
  KEY `SYS_EMAIL_SUBJECT_I` (`SUBJECT`),
  KEY `SYS_EMAIL_RECIPIENT_I` (`RECIPIENT`),
  KEY `SYS_EMAIL_STATUS_I` (`STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

CREATE TABLE `SYS_EMAIL_DATA` (
  `EMAIL_ID`            int(10) UNSIGNED NOT NULL,
  `PARTITION_IND`       varchar(20)  DEFAULT NULL,
  `CONTENT`             blob         DEFAULT NULL,
  KEY (`EMAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
PARTITION BY KEY (`PARTITION_IND`);

CREATE TABLE `SYS_EMAIL_ATTACH` (
  `ID`                  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `EMAIL_ID`            int(10) UNSIGNED NOT NULL,
  `FILE_PATH`           varchar(500) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY (`EMAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

ALTER TABLE `SYS_EMAIL_ATTACH` ADD CONSTRAINT `SYS_EMAIL_ATTACH_EMAIL_ID_FK` FOREIGN KEY (`EMAIL_ID`) REFERENCES `SYS_EMAIL` (`ID`);

----------------------------------------------------------------------------------------------------

CREATE TABLE `SYS_EMAIL_TMPL` (
  `ID`                  int(10) NOT NULL AUTO_INCREMENT,
  `REF_TABLE`           varchar(100) NOT NULL,
  `REF_ID`              int(10)      NOT NULL,
  `NAME`                varchar(200) COLLATE utf8_bin NOT NULL,
  `SUBJECT`             varchar(200) COLLATE utf8_bin NOT NULL,
  `RECIPIENT`           varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `RECIPIENT_CC`        varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `RECIPIENT_BCC`       varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT`             varchar(2000) DEFAULT NULL,
  `DISP_SEQ`            int(10)      NOT NULL,
  `ACTIVE_IND`          varchar(1)   NOT NULL DEFAULT 'Y',
  `CREATED_BY`          varchar(100) DEFAULT NULL,
  `CREATED_DATE`        datetime(6)  DEFAULT NULL,
  `LAST_UPDATED_BY`     varchar(100) DEFAULT NULL,
  `LAST_UPDATED_DATE`   datetime(6)  DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SYS_EMAIL_TMPL_REF_TABLE_ID_I` (`REF_TABLE`,`REF_ID`),
  KEY `SYS_EMAIL_TMPL_ACTIVE_IND_I` (`ACTIVE_IND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;
