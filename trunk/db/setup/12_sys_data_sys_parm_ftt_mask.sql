INSERT INTO `SYS_PARM_SEG`
(CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, SEGMENT, DISP_SEQ, ACTIVE_IND, PARM_VALUE)
select
 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, segment, disp_seq , 'Y', parm_value
from (
select 'DATA_COL_OUTPUT_FTT_CNTPT'                  segment, 'FTT_CNTPT'                  parm_value, 201 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_CNTPT_AUD'              segment, 'FTT_CNTPT_AUD'              parm_value, 202 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT'              segment, 'FTT_MIFIR_RPT'              parm_value, 203 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT_AUD'          segment, 'FTT_MIFIR_RPT_AUD'          parm_value, 204 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN'              segment, 'FTT_MIFIR_TXN'              parm_value, 205 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN_AUD'          segment, 'FTT_MIFIR_TXN_AUD'          parm_value, 206 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER'                  segment, 'FTT_ORDER'                  parm_value, 207 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD'              segment, 'FTT_ORDER_AUD'              parm_value, 208 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD_RL'           segment, 'FTT_ORDER_AUD_RL'           parm_value, 209 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_SYS_MSG_LOG'      segment, 'FTT_ORDER_SYS_MSG_LOG'      parm_value, 210 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DAILY_TXN'         segment, 'FTT_TEMP_DAILY_TXN'         parm_value, 211 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DEALR_DAILY_TXN'   segment, 'FTT_TEMP_DEALR_DAILY_TXN'   parm_value, 212 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       segment, 'FTT_TEMP_ORDER_AMEND'       parm_value, 213 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_FX_RTE_DETL' segment, 'FTT_TEMP_ORDER_FX_RTE_DETL' parm_value, 214 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        segment, 'FTT_TNOVR_DETL_HIST'        parm_value, 215 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        segment, 'FTT_TNOVR_SUMM_HIST'        parm_value, 216 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    segment, 'FTT_TNOVR_SUMM_HIST_AUD'    parm_value, 217 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN'                    segment, 'FTT_TXN'                    parm_value, 218 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT'               segment, 'FTT_TXN_ACCT'               parm_value, 219 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT_AUD'           segment, 'FTT_TXN_ACCT_AUD'           parm_value, 220 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_AUD'                segment, 'FTT_TXN_AUD'                parm_value, 221 disp_seq from dual
) g;



INSERT INTO `SYS_PARM`
(CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, SEGMENT, CODE, DISP_SEQ, PARM_VALUE)
select
 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, segment, code, disp_seq, parm_value
from (
select 'DATA_COL_OUTPUT_FTT_CNTPT'                  code, 'FTT_CNTPT'                  parm_value, 'DATA_COL_OUTPUT' segment, 201 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_CNTPT_AUD'              code, 'FTT_CNTPT_AUD'              parm_value, 'DATA_COL_OUTPUT' segment, 202 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT'              code, 'FTT_MIFIR_RPT'              parm_value, 'DATA_COL_OUTPUT' segment, 203 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT_AUD'          code, 'FTT_MIFIR_RPT_AUD'          parm_value, 'DATA_COL_OUTPUT' segment, 204 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN'              code, 'FTT_MIFIR_TXN'              parm_value, 'DATA_COL_OUTPUT' segment, 205 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN_AUD'          code, 'FTT_MIFIR_TXN_AUD'          parm_value, 'DATA_COL_OUTPUT' segment, 206 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER'                  code, 'FTT_ORDER'                  parm_value, 'DATA_COL_OUTPUT' segment, 207 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD'              code, 'FTT_ORDER_AUD'              parm_value, 'DATA_COL_OUTPUT' segment, 208 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD_RL'           code, 'FTT_ORDER_AUD_RL'           parm_value, 'DATA_COL_OUTPUT' segment, 209 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_SYS_MSG_LOG'      code, 'FTT_ORDER_SYS_MSG_LOG'      parm_value, 'DATA_COL_OUTPUT' segment, 210 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DAILY_TXN'         code, 'FTT_TEMP_DAILY_TXN'         parm_value, 'DATA_COL_OUTPUT' segment, 211 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DEALR_DAILY_TXN'   code, 'FTT_TEMP_DEALR_DAILY_TXN'   parm_value, 'DATA_COL_OUTPUT' segment, 212 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       code, 'FTT_TEMP_ORDER_AMEND'       parm_value, 'DATA_COL_OUTPUT' segment, 213 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_FX_RTE_DETL' code, 'FTT_TEMP_ORDER_FX_RTE_DETL' parm_value, 'DATA_COL_OUTPUT' segment, 214 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        code, 'FTT_TNOVR_DETL_HIST'        parm_value, 'DATA_COL_OUTPUT' segment, 215 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        code, 'FTT_TNOVR_SUMM_HIST'        parm_value, 'DATA_COL_OUTPUT' segment, 216 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    code, 'FTT_TNOVR_SUMM_HIST_AUD'    parm_value, 'DATA_COL_OUTPUT' segment, 217 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN'                    code, 'FTT_TXN'                    parm_value, 'DATA_COL_OUTPUT' segment, 218 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT'               code, 'FTT_TXN_ACCT'               parm_value, 'DATA_COL_OUTPUT' segment, 219 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT_AUD'           code, 'FTT_TXN_ACCT_AUD'           parm_value, 'DATA_COL_OUTPUT' segment, 220 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_AUD'                code, 'FTT_TXN_AUD'                parm_value, 'DATA_COL_OUTPUT' segment, 221 disp_seq from dual
) g;


INSERT INTO `SYS_PARM`
(CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, SEGMENT, CODE, DISP_SEQ, PARM_VALUE)
select
 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM', CURRENT_TIMESTAMP, segment, code, disp_seq, parm_value
from (
select 'DATA_COL_OUTPUT_FTT_CNTPT'                  segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_CNTPT_AUD'              segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT'              segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT_AUD'          segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN'              segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN_AUD'          segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER'                  segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD'              segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD_RL'           segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_SYS_MSG_LOG'      segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DAILY_TXN'         segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DEALR_DAILY_TXN'   segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_FX_RTE_DETL' segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN'                    segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT'               segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT_AUD'           segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_AUD'                segment, 'INDEX' code, 'C0:R0' parm_value, 1 disp_seq from dual union all

select 'DATA_COL_OUTPUT_FTT_CNTPT'                  segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_CNTPT_AUD'              segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT'              segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT_AUD'          segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN'              segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN_AUD'          segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER'                  segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD'              segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD_RL'           segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_SYS_MSG_LOG'      segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DAILY_TXN'         segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DEALR_DAILY_TXN'   segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_FX_RTE_DETL' segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN'                    segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT'               segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT_AUD'           segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_AUD'                segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all

select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       segment, 'INDEX' code, 'C0:R0,R1' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        segment, 'INDEX' code, 'C0:R0,R1' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        segment, 'INDEX' code, 'C0:R0,R1' parm_value, 1 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    segment, 'INDEX' code, 'C0:R0,R1' parm_value, 1 disp_seq from dual union all

select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    segment, 'C0' code, 'TRUE'  parm_value, 2 disp_seq from dual union all

select 'DATA_COL_OUTPUT_FTT_CNTPT'                  segment, 'R0' code, 'CNTPT_NAM:xxxxx'        parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_CNTPT_AUD'              segment, 'R0' code, 'CNTPT_NAM:xxxxx'        parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT'              segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_RPT_AUD'          segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN'              segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_MIFIR_TXN_AUD'          segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER'                  segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD'              segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_AUD_RL'           segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_ORDER_SYS_MSG_LOG'      segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DAILY_TXN'         segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_DEALR_DAILY_TXN'   segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_FX_RTE_DETL' segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN'                    segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT'               segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_ACCT_AUD'           segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TXN_AUD'                segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       segment, 'R0' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        segment, 'R0' code, 'CNTPT_NAM:xxxxx'        parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        segment, 'R0' code, 'CNTPT_NAM:xxxxx'        parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    segment, 'R0' code, 'CNTPT_NAM:xxxxx'        parm_value, 3 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TEMP_ORDER_AMEND'       segment, 'R1' code, 'OLD_TXN_ACCT_NAM:xxxxx' parm_value, 4 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_DETL_HIST'        segment, 'R1' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 4 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST'        segment, 'R1' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 4 disp_seq from dual union all
select 'DATA_COL_OUTPUT_FTT_TNOVR_SUMM_HIST_AUD'    segment, 'R1' code, 'TXN_ACCT_NAM:xxxxx'     parm_value, 4 disp_seq from dual
) g;


