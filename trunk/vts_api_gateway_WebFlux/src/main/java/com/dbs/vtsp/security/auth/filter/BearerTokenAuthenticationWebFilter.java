package com.dbs.vtsp.security.auth.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;

public class BearerTokenAuthenticationWebFilter implements WebFilter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String name;

	public BearerTokenAuthenticationWebFilter(String name) {
		this.name = name;
	}

	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		logger.info("Process in Bearer Authenication web filter " + name + " ...");
		if ("OPTIONS".equals(exchange.getRequest().getMethod().toString())) {
			exchange.getResponse().setStatusCode(HttpStatus.OK);
			return Mono.empty();
		}
		return chain.filter(exchange);
	}
}
