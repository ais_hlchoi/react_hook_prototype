package com.dbs.vtsp.security.auth.handler;

import java.nio.charset.StandardCharsets;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class CustomAuthenticationFailureHandler implements ServerAuthenticationFailureHandler {

	private static final String PATTERN = "{\"failed\":true,\"errMsg\":\"%s\"}";

	/*
	 * private final URI location = URI.create("/login");
	 * 
	 * private ServerRedirectStrategy redirectStrategy = new
	 * DefaultServerRedirectStrategy();
	 */

	@Override
	public Mono<Void> onAuthenticationFailure(WebFilterExchange webFilterExchange, AuthenticationException exception) {
		ServerWebExchange exchange = webFilterExchange.getExchange();
		byte[] bytes = String.format(PATTERN, exception.getMessage()).getBytes(StandardCharsets.UTF_8);
		DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
		exchange.getResponse().getHeaders().add(HttpHeaders.AUTHORIZATION, String.join(" ", "Error ", StringUtils.replace(exception.getMessage(), " ", "."), "_end"));
		return exchange.getResponse().writeWith(Flux.just(buffer));
		//return redirectStrategy.sendRedirect(exchange, location);
	}
}
