package com.dbs.vtsp.user.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.dbs.util.AesEncryptUtil;
import com.dbs.util.ApiUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.user.services.LdapService;
import com.fasterxml.jackson.databind.JsonNode;

@Component
public class LdapRepositoryServiceImpl implements LdapService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${user.auth.url}")
	private String userAuthApi;

	public LdapRepositoryServiceImpl(String authApi) {
		Assert.notNull(authApi, "authApi is required");
		this.userAuthApi = authApi;
	}

	public LdapRepositoryServiceImpl() {
		
	}

	@Override
	public JsonNode login(String username, String password) {
		JsonNode result = null;
		try {
			Assert.notNull(username, "username is required");
			Assert.notNull(password, "password is required");
			logger.info(String.format("Pending to login =%s", FortifyStrategyUtils.toLogString(username)));
			Map<String, String> params = new HashMap<>();
			params.put("username", username);
			params.put("password", AesEncryptUtil.encode(password.getBytes()));
			Map<String, String> customHeaders = new HashMap<>();
			result = ApiUtils.post(userAuthApi, params, customHeaders);
			Assert.isTrue("M1000".equals(result.get("responseCode").asText()), String.format("%s", result.get("responseDesc").asText()));
			logger.info(String.format("Success to login =%s", FortifyStrategyUtils.toLogString(username)));
		} catch (Exception e) {			
			logger.error(FortifyStrategyUtils.toLogString(e.getMessage()));
			throw new UsernameNotFoundException(e.getMessage(), e.getCause());
		}
		return result;
	}
}
