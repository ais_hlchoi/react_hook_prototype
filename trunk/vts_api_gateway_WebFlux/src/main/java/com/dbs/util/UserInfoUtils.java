package com.dbs.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class UserInfoUtils {

	private UserInfoUtils() {
		// for SonarQube scanning purpose
	}

	public static String[] getUserCode(final String path, final String username) throws UsernameNotFoundException {
		String[] userCode = new String[3];
		try {
			Map<String, String> params = new HashMap<>();
			params.put("username", encodeValue(username));
			String url = path + getParamValue(path, params);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
			Assert.isTrue(HttpStatus.OK == response.getStatusCode(), String.format("No response from %s", path));
			userCode = parseUserCodeFromJsonNode(path, username, response.getBody());
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage(), e.getCause());
		}
		return userCode;
	}

	protected static String getParamValue(final String path, final Map<String, String> params) {
		if (MapUtils.isEmpty(params))
			return "";

		final String q = "?";
		final String a = "&";
		return (StringUtils.isBlank(path) || path.indexOf(q) < 0 ? q : a) + params.entrySet().stream().map(o -> String.format("%s=%s", o.getKey(), o.getValue())).collect(Collectors.joining("&"));
	}

	protected static String encodeValue(final String value) throws UnsupportedEncodingException {
		return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
	}

	protected static String[] parseUserCodeFromJsonNode(final String path, final String username, final String responseBody) throws JsonMappingException, JsonProcessingException {
		String[] userCode = new String[3];
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(responseBody);
		JsonNode object = root.path("object");
		ArrayNode userInfoList = (ArrayNode) object.get("userInfo");
		Assert.notNull(userInfoList, String.format("No userInfo from %s%n%s", path, responseBody));

		Iterator<JsonNode> itu = userInfoList.elements();
		Assert.isTrue(itu.hasNext(), String.format("Empty userInfo from %s%n%s", path, responseBody));

		JsonNode userInfo = itu.next();
		userCode[0] = userInfo.path("userCode").asText();
		Assert.isTrue(StringUtils.isNotBlank(userCode[0]), String.format("Empty userCode from %s%n%s", path, responseBody));

		userCode[1] = userInfo.path("pwd").asText();
		ArrayNode userRoleList = (ArrayNode) userInfo.get("roles");
		if (null == userRoleList)
			return userCode;

		Iterator<JsonNode> itr = userRoleList.elements();
		Assert.isTrue(itr.hasNext(), String.format("Empty userRole where username=%s%n%s", username, responseBody));

		userCode[2] = StreamSupport.stream(Spliterators.spliteratorUnknownSize(itr, Spliterator.ORDERED), false).map(UserInfoUtils::asText).collect(Collectors.joining(","));
		return userCode;
	}

	private static String asText(JsonNode node) {
		// for SonarQube scanning purpose
		return node.asText();
	}
}
