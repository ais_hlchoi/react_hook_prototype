package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.model.ReportAccessGroupFormModel;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportAccessGroupController;
import com.dbs.vtsp.service.ReportService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportAccessGroupUnitTest {

	@Autowired 
	private ReportAccessGroupController reportAccessGroupController;
	
	@Autowired
	private ReportService reportService;
	
	private ReportPropsModel reportPropsModel = TestUtil.getTestObject(ReportPropsModel.class);
	
	private ReportAccessGroupModel reportAccessGroupModel = TestUtil.getTestObject(ReportAccessGroupModel.class);
	
	private ReportAccessGroupFormModel reportAccessGroupFormModel = TestUtil.getTestObject(ReportAccessGroupFormModel.class);
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}
	
	private void prepearReportTemplate() {
		int rptTmplId = reportService.createReportTemplate(reportPropsModel);
		reportAccessGroupModel.setRptTmplId(rptTmplId);
	}

	@Test
	public void search() {
		reportAccessGroupController.search(reportAccessGroupFormModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchAll() {
		reportAccessGroupController.searchAll(reportAccessGroupFormModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	@Test
	public void insertUpdateDelete() {
		prepearReportTemplate();
		insert();
		insertReport();
		insertUser();
		insertRole();
		find();
		getReportNameList();
		getUserNameList();
		getRoleNameList();
		update();
		updateReport();
		updateUser();
		updateRole();
		deleteRole();
		deleteUser();
		deleteReport();
		delete();
		deleteReportTemplate();
	}

	private void find() {
		reportAccessGroupController.find(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	private void insert() {
		int rptAcgId = (int)((RestEntity)reportAccessGroupController.insert(reportAccessGroupModel)).getObject();
		reportAccessGroupModel.setId(rptAcgId);
		System.out.println(ClassPathUtils.getName());
	}

	private void insertReport() {
		reportAccessGroupController.insertReport(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void insertUser() {
		reportAccessGroupController.insertUser(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void insertRole() {
		reportAccessGroupController.insertRole(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() {
		reportAccessGroupController.update(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateReport() {
		reportAccessGroupController.updateReport(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateUser() {
		reportAccessGroupController.updateUser(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateRole() {
		reportAccessGroupController.updateRole(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() {
		reportAccessGroupController.delete(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void deleteReport() {
		reportAccessGroupController.deleteReport(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void deleteUser() {
		reportAccessGroupController.deleteUser(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void deleteRole() {
		reportAccessGroupController.deleteRole(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	private void deleteReportTemplate() {
		ReportPropsModel template = new ReportPropsModel();
		template.setId(reportAccessGroupModel.getRptTmplId());
		reportService.deleteReportTemplate(template);
	}

	private void getReportNameList() {
		reportAccessGroupController.getReportNameList(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void getUserNameList() {
		reportAccessGroupController.getUserNameList(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void getRoleNameList() {
		reportAccessGroupController.getRoleNameList(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}
}