package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.report.model.ReportFormModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportBuilderController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportBuilderUnitTest {
	@Autowired
	private ReportBuilderController reportBuilderController;

	private ReportFormModel reportFormModel = TestUtil.getTestObject(ReportFormModel.class);
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}	

	@Test
	public void getTableNameList() {
		reportBuilderController.getTableNameList(reportFormModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	@Test
	public void getColumnNameList() {
		reportBuilderController.getColumnNameList(reportFormModel);
		System.out.println(ClassPathUtils.getName());
	}
}