package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.report.model.ReportCategoryFormModel;
import com.dbs.module.report.model.ReportCategoryModel;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportCategoryController;
import com.dbs.vtsp.service.ReportService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportCategoryUnitTest {

	@Autowired
	private ReportService reportService;

	@Autowired
	private ReportCategoryController reportCategoryController;

	private ReportCategoryFormModel rptCatgFormModel = TestUtil.getTestObject(ReportCategoryFormModel.class);

	private ReportCategoryModel rptCatgModel = TestUtil.getTestObject(ReportCategoryModel.class);

	private ReportPropsModel reportPropsModel = TestUtil.getTestObject(ReportPropsModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	private void prepearReportCategoryModel() {
		int rptTmplId = reportService.createReportTemplate(reportPropsModel);
		rptCatgModel.setRptTmplId(rptTmplId);
	}

	private void deleteReportTemplate() {
		ReportPropsModel template = new ReportPropsModel();
		template.setId(rptCatgModel.getRptTmplId());
		reportService.deleteReportTemplate(template);
	}

	@Test
	public void insertUpdateDelete() {
		prepearReportCategoryModel();
		insert();
		insertReport();
		update();
		updateReport();
		deleteReport();
		delete();
		deleteReportTemplate();
	}

	@Test
	public void getReportCategory() {
		reportCategoryController.getReportCategory(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void insert() {
		int rptCatgId = (int) reportCategoryController.insert(rptCatgModel).getObject();
		rptCatgModel.setId(rptCatgId);
		System.out.println(ClassPathUtils.getName());
	}

	private void insertReport() {
		reportCategoryController.insertReport(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() {
		rptCatgModel.setName("JUnit_test2");
		reportCategoryController.update(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateReport() {
		reportCategoryController.updateReport(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() {
		reportCategoryController.delete(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void deleteReport() {
		reportCategoryController.deleteReport(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportNameList() {
		reportCategoryController.getReportNameList(rptCatgFormModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportCategoryProps() {
		reportCategoryController.getReportCategoryProps(rptCatgModel);
		System.out.println(ClassPathUtils.getName());
	}
}