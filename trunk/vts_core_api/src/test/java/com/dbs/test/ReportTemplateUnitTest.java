package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.app.Application;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.module.report.model.ReportTemplateFormModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportTemplateController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportTemplateUnitTest {

	@Autowired
	private ReportTemplateController reportTemplateController;

	private ReportTemplateFormModel reportTemplateFormModel = TestUtil.getTestObject(ReportTemplateFormModel.class);

	private ReportPropsModel reportPropsModel = TestUtil.getTestObject(ReportPropsModel.class);

	private ReportScheduleModel reportScheduleModel = TestUtil.getTestObject(ReportScheduleModel.class);

	private ReportAccessGroupModel reportAccessGroupModel = TestUtil.getTestObject(ReportAccessGroupModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void getReportNameList() {
		reportTemplateController.getReportNameList(reportTemplateFormModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportProps() {
		reportTemplateController.getReportProps(reportPropsModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportTemplate() {
		reportTemplateController.getReportTemplate(reportPropsModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportAccessGroupList() {
		reportTemplateController.getReportAccessGroupList(reportAccessGroupModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getReportScheduleList() {
		reportTemplateController.getReportScheduleList(reportScheduleModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void insertUpdateDelete() {
		insert();
		update();
		delete();
	}

	private void insert() {
		int id = (int) ((RestEntity) reportTemplateController.insert(reportPropsModel)).getObject();
		reportPropsModel.setId(id);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() {
		reportTemplateController.update(reportPropsModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void delete() {
		reportTemplateController.delete(reportPropsModel);
		System.out.println(ClassPathUtils.getName());
	}
}