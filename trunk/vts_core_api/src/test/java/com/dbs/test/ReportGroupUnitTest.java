package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.util.ClassPathUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportGroupUnitTest {

	@Value("${test.demo.report-group}")
	private String fileName;

	@Test
	public void test() {
		System.out.println(String.format("%s: %d <- [ %s ]", ClassPathUtils.getName(), 1, fileName));
	}
}