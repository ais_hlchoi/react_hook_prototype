package com.dbs.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.download.data.ReportDownloadData;
import com.dbs.module.download.model.ReportDownloadFormModel;
import com.dbs.module.download.model.ReportDownloadModel;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.util.Assert;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportDownloadController;
import com.dbs.vtsp.model.RptFileDnldHistoryModel;
import com.dbs.vtsp.model.RptFileDnldSummaryModel;
import com.dbs.vtsp.service.ReportDownloadService;

import com.github.pagehelper.PageInfo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportDownloadUnitTest {

	@Value("${test.demo.data-export}")
	private String fileName;

	@Autowired
	private ReportDownloadController reportDownloadController;

	@Autowired
	private ReportDownloadService reportDownloadService;

	private ReportDownloadFormModel reportDownloadFormModel = TestUtil.getTestObject(ReportDownloadFormModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void search() {
		reportDownloadFormModel.setDefaultSearchText(ExportOption.CSV.ext);
		reportDownloadController.search(reportDownloadFormModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchAll() {
		reportDownloadController.search(new ReportDownloadFormModel());
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void exportXlsx() {
		ReportDownloadModel model = getTempModel(ExportOption.XLSX.ext);
		if(null != model) {
			reportDownloadController.exportXlsx(model);
		}
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void exportCsv() {
		ReportDownloadModel model = getTempModel(ExportOption.CSV.ext);
		if(null != model) {
			reportDownloadController.exportCsv(model);
		}
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void exportPdf() {
		ReportDownloadModel model = getTempModel(ExportOption.PDF.ext);
		if(null != model) {
			reportDownloadController.exportPdf(model);
		}
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchReportDownload() {
		reportDownloadFormModel.setDefaultSearchText(ExportOption.CSV.ext);
		List<RptFileDnldSummaryModel> model = reportDownloadService.searchDownloadReport(reportDownloadFormModel);
		Assert.notNull(model, "Cannot be null");
	}

	@Test
	public void searchReportDownloadHistory() {
		reportDownloadFormModel.setRefId("1");
		reportDownloadFormModel.setRefType("R");
		List<RptFileDnldHistoryModel> model = reportDownloadService.searchDownloadReportHistory(reportDownloadFormModel);
		Assert.notNull(model, "Cannot be null");
	}

	private ReportDownloadModel getTempModel(String searchText) {
		reportDownloadFormModel.setDefaultSearchText(searchText);
		RestEntity restEntity = reportDownloadController.search(reportDownloadFormModel);
		PageInfo pageInfo = (PageInfo)restEntity.getObject();
		List<ReportDownloadData> reportDownloadDataList= pageInfo.getList();
		ReportDownloadModel model = null;
		for(ReportDownloadData row : reportDownloadDataList) {
			if("C".equals(row.getStatus())) {
				model = new ReportDownloadModel();
				model.setId(row.getId());
				break;
			}
		}
		return model;
	}
}
