package com.dbs.test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


import com.dbs.app.Application;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.data.ReportScheduleData;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.ReportScheduleController;
import com.dbs.vtsp.service.ReportService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ReportScheduleUnitTest {

	@Autowired
	private ReportScheduleController reportScheduleController;
	
	@Autowired
	private ReportService reportService;

	private ReportScheduleModel scheduleModel = TestUtil.getTestObject(ReportScheduleModel.class);
	
	private int scheduleId;
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void insertUpdateDelete() {
		prepearScheduleRecord();
		insert();
		find();
		update();
		updateDispSeq();
		updateLastExecution();
		delete();
	}
	
	private void find() {
		reportScheduleController.find(scheduleModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	private void prepearScheduleRecord() {
		ReportPropsModel model = TestUtil.getTestObject(ReportPropsModel.class);
		int id = reportService.createReportTemplate(model);
		scheduleModel.setRptTmplId(id);
	}
	
	private void insert() {
		scheduleId = (int)reportScheduleController.insert(scheduleModel).getObject();
		scheduleModel.setId(scheduleId);
		System.out.println(ClassPathUtils.getName());
	}

	private void update() {
		reportScheduleController.update(scheduleModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateDispSeq() {
		reportScheduleController.updateDispSeq(scheduleModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void updateLastExecution() {
		reportScheduleController.updateLastExecution(scheduleModel);
		System.out.println(ClassPathUtils.getName());
	}

	private void  delete() {
		reportScheduleController.delete(scheduleModel);

		ReportPropsModel template = new ReportPropsModel();
		template.setId(scheduleModel.getRptTmplId());
		reportService.deleteReportTemplate(template);
		System.out.println(ClassPathUtils.getName());
	}
}