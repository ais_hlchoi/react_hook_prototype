package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.report.model.ReportAccessGroupFormModel;
import com.dbs.module.report.model.SavedSearchFormModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.SavedSearchController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SavedSearchUnitTest {

	@Autowired
	private SavedSearchController savedSearchController;

	private SavedSearchFormModel savedSearchFormModel = TestUtil.getTestObject(SavedSearchFormModel.class);

	private ReportAccessGroupFormModel reportAccessGroupFormModel = TestUtil.getTestObject(ReportAccessGroupFormModel.class);

	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void searchReportBuilder() {
		savedSearchController.searchReportBuilder(savedSearchFormModel);
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void searchReportBuilderAll() {
		savedSearchController.searchReportBuilderAll(new SavedSearchFormModel());
		System.out.println(ClassPathUtils.getName());
	}

	@Test
	public void getAccessGroupNameList() {
		savedSearchController.getAccessGroupNameList(reportAccessGroupFormModel);
		System.out.println(ClassPathUtils.getName());
	}
}