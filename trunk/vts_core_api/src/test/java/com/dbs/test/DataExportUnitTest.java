package com.dbs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dbs.app.Application;
import com.dbs.module.report.model.ReportTemplateModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.controller.external.DataExportController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataExportUnitTest {

	@Value("${test.demo.data-export}")
	private String fileName;

	@Autowired
	private DataExportController dataExportController;
	
	private ReportTemplateModel reportTemplateModel = TestUtil.getTestObject(ReportTemplateModel.class);
	
	static {
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("JUnit", null));
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void getQuerySql() {
		dataExportController.getQuerySql(reportTemplateModel);
		System.out.println(ClassPathUtils.getName());
	}
	
	@Test
	public void createReportDownload() throws Exception {
		dataExportController.createReportDownload(reportTemplateModel);
		System.out.println(ClassPathUtils.getName());
	}

}