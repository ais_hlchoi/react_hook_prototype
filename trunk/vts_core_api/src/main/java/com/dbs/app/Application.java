package com.dbs.app;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableAutoConfiguration 
@MapperScan({"com.dbs.vtsp.dao","com.dbs.sys.dao"})//packages mybatis will scan for xml-java mapper
@ComponentScan(basePackages = {"com.dbs"})//packages spring will map scan for DI
@SpringBootApplication
@Configuration
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static final String UID = new SimpleDateFormat("yyMMdd").format(new Date());

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		logger.info("ヾ(◍°∇°◍)ﾉﾞ    bootdo startup     ヾ(◍°∇°◍)ﾉﾞ\n" +
                " ______                    _   ______            \n" +
                "|_   _ \\                  / |_|_   _ `.          \n" +
                "  | |_) |   .--.    .--. `| |-' | | `. \\  .--.   \n" +
                "  |  __'. / .'`\\ \\/ .'`\\ \\| |   | |  | |/ .'`\\ \\ \n" +
                " _| |__) || \\__. || \\__. || |, _| |_.' /| \\__. | \n" +
                "|_______/  '.__.'  '.__.' \\__/|______.'  '.__.'  ");
	}

}
