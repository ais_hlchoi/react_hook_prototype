package com.dbs.vtsp.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.data.ReportAccessGroupCategoryNameData;
import com.dbs.module.report.data.ReportAccessGroupListData;
import com.dbs.module.report.data.ReportAccessGroupObjectData;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportAccessGroupTemplateNameData;
import com.dbs.module.report.data.ReportAccessGroupUserNameData;
import com.dbs.module.report.model.ReportAccessGroupFormModel;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/reportAccessGroup")
@RoleAccess
public class ReportAccessGroupController {

	@Autowired
	private ReportAccessGroupService reportAccessGroupService;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity search(@RequestBody ReportAccessGroupFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportAccessGroupListData> list = reportAccessGroupService.searchReportAccessGroup(model);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping(value = "/searchAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchAll(@RequestBody ReportAccessGroupFormModel model) {
		List<ReportAccessGroupListData> list = reportAccessGroupService.searchReportAccessGroup(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity find(@RequestBody ReportAccessGroupModel model) {
		ReportAccessGroupObjectData item = reportAccessGroupService.getReportAccessGroup(model);
		return RestEntity.success(item);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insert(@RequestBody ReportAccessGroupModel model) {
		if (null != model.getId())
			return update(model);

		int id = reportAccessGroupService.createReportAccessGroup(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/insertReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertReport(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.createReportAccessGroupTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/insertCategory", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertCategory(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.createReportAccessGroupCategory(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/insertUser", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertUser(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.createReportAccessGroupUser(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/insertRole", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertRole(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.createReportAccessGroupRole(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity update(@RequestBody ReportAccessGroupModel model) {
		if (null == model.getId())
			return insert(model);

		int id = reportAccessGroupService.updateReportAccessGroup(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/updateReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateReport(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.updateReportAccessGroupTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/updateCategory", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateCategory(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.updateReportAccessGroupCategory(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateUser(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.updateReportAccessGroupUser(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/updateRole", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateRole(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.updateReportAccessGroupRole(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity delete(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.deleteReportAccessGroup(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/deleteReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity deleteReport(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.deleteReportAccessGroupTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/deleteCategory", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity deleteCategory(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.deleteReportAccessGroupCategory(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity deleteUser(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.deleteReportAccessGroupUser(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/deleteRole", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity deleteRole(@RequestBody ReportAccessGroupModel model) {
		reportAccessGroupService.deleteReportAccessGroupRole(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/getReportNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportNameList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupTemplateNameData> list = reportAccessGroupService.getReportNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getCatgNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getCatgNameList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupCategoryNameData> list = reportAccessGroupService.getCatgNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getUserNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getUserNameList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupUserNameData> list = reportAccessGroupService.getUserNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getRoleNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getRoleNameList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupRoleNameData> list = reportAccessGroupService.getRoleNameList(model);
		return RestEntity.success(list);
	}
}
