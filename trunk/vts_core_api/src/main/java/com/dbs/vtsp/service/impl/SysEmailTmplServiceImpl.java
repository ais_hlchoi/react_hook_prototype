package com.dbs.vtsp.service.impl;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.report.*;
import com.dbs.module.report.config.ReportConfig;
import com.dbs.module.report.constant.Datatype;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportAccessGroupType;
import com.dbs.module.report.constant.ReportScheduleRefTable;
import com.dbs.module.report.data.*;
import com.dbs.module.report.model.*;
import com.dbs.module.report.util.*;
import com.dbs.util.Assert;
import com.dbs.util.ConstantUtil;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.*;
import com.dbs.vtsp.model.*;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.dbs.vtsp.service.ReportService;
import com.dbs.vtsp.service.SysEmailTmplService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SysEmailTmplServiceImpl extends AbstractServiceImpl implements SysEmailTmplService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());



	@Autowired
	private SysEmailTmplMapper sysEmailTmplMapper;




	@Override
	public SysEmailTmplModel getSysEmailTmpl(SysEmailTmplModel model) {
		return getSysEmailTmpl(model.getRefId(),model.getRefTable());
	}


	@Override
	public SysEmailTmplModel getSysEmailTmpl(Integer refId, String refTable) {
		SysEmailTmplModel existEmailTmplModel =  sysEmailTmplMapper.getSysEmailTmpl(refId,refTable);
		return existEmailTmplModel;
	}


	@Override
	public int insert(SysEmailTmplModel model) {
		int result = sysEmailTmplMapper.insert(model);
		Assert.isTrue(result == 1, String.format("Fail to insert  email template (%s)", getOrElse(model.getName(), "")));
		return result;
	}

	@Override
	public int delete(Integer refId, String refTable) {
		return sysEmailTmplMapper.delete(refId,refTable);
	}

	@Override
	public int delete(SysEmailTmplModel model) {
		return delete(model.getRefId(),model.getRefTable());
	}

	@Override
	public int update(SysEmailTmplModel model) {
		SysEmailTmplModel existEmailTmplModel = getSysEmailTmpl(model.getRefId(), model.getRefTable());
		if (existEmailTmplModel == null) {
			return insert(model);
		} else {
			existEmailTmplModel.setName(model.getName());
			existEmailTmplModel.setSubject(model.getSubject());
			existEmailTmplModel.setContent(model.getContent());
			existEmailTmplModel.setRecipient(model.getRecipient());
			existEmailTmplModel.setRecipientCc(model.getRecipientCc());
			existEmailTmplModel.setRecipientBcc(model.getRecipientBcc());
			existEmailTmplModel.setDispSeq(model.getDispSeq());
			existEmailTmplModel.setActiveInd(model.getActiveInd());
			existEmailTmplModel.setLastUpdatedBy(model.getLastUpdatedBy());
			int result = sysEmailTmplMapper.update(existEmailTmplModel);
			Assert.isTrue(result == 1, String.format("Fail to update email template %s", getOrElse(model.getName(), "")));
			return result;
		}
	}

		@Override
		public int updateActiveInd(SysEmailTmplModel model,boolean active) {
			SysEmailTmplModel existEmailTmplModel = getSysEmailTmpl(model.getRefId(), model.getRefTable());
			if (existEmailTmplModel == null) {
				return 0;
			} else {
				existEmailTmplModel.setActiveInd(active ? ConstantUtil.Y : ConstantUtil.N);
				existEmailTmplModel.setLastUpdatedBy(model.getLastUpdatedBy());
				int result = sysEmailTmplMapper.update(existEmailTmplModel);
				Assert.isTrue(result == 1, String.format("Fail to update email template %s", getOrElse(model.getName(), "")));
				return result;
			}
		}
}
