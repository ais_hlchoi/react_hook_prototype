package com.dbs.vtsp.dao;

import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldLogModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.RptFileDnldUserGrpModel;

public interface FileDnldMapper {

	RptFileDnldModel getFileDownload(Integer id);

	RptFileDnldDataModel getFileDownloadData(Integer id);

	int createFileDownload(RptFileDnldModel model);

	int createFileDownloadData(RptFileDnldDataModel model);

	int createFileDownloadUserGroup(RptFileDnldUserGrpModel model);

	int createFileDownloadLog(RptFileDnldLogModel model);

	int updateFileDownloadStatus(RptFileDnldModel model);

	int updateFileDownloadStart(RptFileDnldModel model);

	int updateFileDownloadProcess(RptFileDnldModel model);

	int updateFileDownloadFailure(RptFileDnldModel model);

	int updateFileDownloadComplete(RptFileDnldModel model);

	int updateFileDownloadStatusPending(Integer id);

	int updateFileDownloadLogAged(Integer id);
}
