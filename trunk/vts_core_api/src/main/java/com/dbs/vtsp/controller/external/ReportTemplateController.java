package com.dbs.vtsp.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.constant.ReportBuildType;
import com.dbs.module.report.data.ReportAccessGroupData;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportPropsData;
import com.dbs.module.report.data.ReportScheduleData;
import com.dbs.module.report.data.ReportTemplateData;
import com.dbs.module.report.data.ReportTemplateNameData;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.module.report.model.ReportTemplateFormModel;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.dbs.vtsp.service.ReportService;

@RestController
@CrossOrigin
@RequestMapping("/api/reportTemplate")
@RoleAccess
public class ReportTemplateController {

	@Autowired
	private ReportService reportService;

	@Autowired
	private ReportAccessGroupService reportAccessGroupService;

	@RequestMapping(value = "/getReportNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportNameList(@RequestBody ReportTemplateFormModel model) {
		List<ReportTemplateNameData> list = reportService.getReportNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportProps", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportProps(@RequestBody ReportPropsModel model) {
		ReportPropsData item = reportService.getReportProps(model);
		return RestEntity.success(item);
	}

	@RequestMapping(value = "/getReportTemplate", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportTemplate(@RequestBody ReportPropsModel model) {
		ReportTemplateData item = reportService.getReportTemplate(model);
		return RestEntity.success(item);
	}

	@RequestMapping(value = "/getReportAccessGroupList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportAccessGroupList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupData> list = reportAccessGroupService.getReportAccessGroupListByTemplate(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportAccessGroupRoleList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportAccessGroupRoleList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupRoleNameData> list = reportService.getReportAccessGroupRoleList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportScheduleList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportScheduleList(@RequestBody ReportScheduleModel model) {
		List<ReportScheduleData> list = reportService.getReportScheduleList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insert(@RequestBody ReportPropsModel model) {
		if (null != model.getId())
			return update(model);

		int id = reportService.createReportTemplate(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/insertReportAdvance", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertReportAdvance(@RequestBody ReportPropsModel model) {
		model.setBuildType(ReportBuildType.REPORT_ADVANCE.name());
		if (null != model.getId())
			return update(model);

		int id = reportService.createReportTemplate(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity update(@RequestBody ReportPropsModel model) {
		if (null == model.getId())
			return insert(model);

		int id = reportService.updateReportTemplate(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity delete(@RequestBody ReportPropsModel model) {
		reportService.deleteReportTemplate(model);
		return RestEntity.success();
	}
}
