package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldSummaryModel {

	@JsonInclude
	private String rptName;

	@JsonInclude
	private String schedulerName;

	@JsonInclude
	private String id;

	@JsonInclude
	private String refId;

	@JsonInclude
	private String refType;

	@JsonInclude
	private String exportOption;

	@JsonInclude
	private String lastExecutionTime;

	@JsonInclude
	private String nextExecutionTime;

	@JsonInclude
	private String description;

}
