package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldLogModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer fileDnldId;

	@JsonInclude
	private String logMessage;

	@JsonInclude
	private String status;

	@JsonInclude
	private String currentInd;
}
