package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupCatgModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String userId;

	@JsonInclude
	private String name;

	@JsonInclude
	private String description;

	@JsonInclude
	private String generateOpt;

	@JsonInclude
	private String activeInd;
}
