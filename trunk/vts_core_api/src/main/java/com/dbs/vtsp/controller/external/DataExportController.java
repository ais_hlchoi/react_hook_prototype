package com.dbs.vtsp.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.download.util.ReportDownloadUtils;
import com.dbs.module.report.model.ReportTemplateModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.vtsp.service.FileDownloadService;
import com.dbs.vtsp.service.ReportBuilderService;

@RestController
@CrossOrigin
@RequestMapping("/api/dataExport")
@RoleAccess
public class DataExportController {

	@Autowired
	private ReportBuilderService reportBuilderService;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private DownloadConfig downloadConfig;

	@RequestMapping(value = "/getQuerySql", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getQuerySql(@RequestBody ReportTemplateModel model) {
		String result = reportBuilderService.getQuerySql(model);
		return RestEntity.success(result);
	}

	@RequestMapping(value = "/createReportDownload", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity createReportDownload(@RequestBody ReportTemplateModel model) {
		ReportDownload bean = ReportDownloadUtils.parsePending(model, ClassPathUtils.getOrElse(model.getName(), downloadConfig.getFileDownloadReportName()));
		int id = fileDownloadService.createFileDownloadCheckCount(bean);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/resetReportDownload", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity resetReportDownload(@RequestBody ReportTemplateModel model) {
		fileDownloadService.updateFileDownloadStatusPending(ReportDownloadUtils.parse(model.getId()));
		return RestEntity.success();
	}
}
