package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.ReportDownloadForm;
import com.dbs.module.download.model.ReportDownloadFormModel;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.vtsp.model.RptFileDnldHistoryModel;
import com.dbs.vtsp.model.RptFileDnldSummaryModel;

public interface RptDnldMapper {

	List<ReportDownloadObjectModel> searchReportDownload(ReportDownloadForm model);

	Integer getReportDownloadCountInHistory(ReportDownload model);

	List<RptFileDnldSummaryModel> searchDownloadReport(ReportDownloadForm model);

	List<RptFileDnldHistoryModel> searchDownloadReportHistory(ReportDownloadFormModel model);
}
