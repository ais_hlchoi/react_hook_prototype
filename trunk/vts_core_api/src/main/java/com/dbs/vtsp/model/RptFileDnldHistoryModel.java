package com.dbs.vtsp.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldHistoryModel {
	private String fileName;
	private String fileId;
	private String fileExtension;
	private String executionDate;
	private String filePath;
}
