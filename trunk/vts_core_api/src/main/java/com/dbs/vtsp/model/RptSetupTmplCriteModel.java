package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupTmplCriteModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer criteSeq;

	@JsonInclude
	private Integer tableId;

	@JsonInclude
	private String tableLabel;

	@JsonInclude
	private Integer columnId;

	@JsonInclude
	private String columnLabel;

	@JsonInclude
	private String comparatorCde;

	@JsonInclude
	private String comparatorLabel;

	@JsonInclude
	private String fieldValue;

	@JsonInclude
	private String mandatoryInd;

	@JsonInclude
	private String ignoreCasesInd;
}
