package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportAccessGroupForm;
import com.dbs.module.report.data.ReportAccessGroupCategoryNameData;
import com.dbs.module.report.data.ReportAccessGroupData;
import com.dbs.module.report.data.ReportAccessGroupListData;
import com.dbs.module.report.data.ReportAccessGroupObjectData;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportAccessGroupTemplateNameData;
import com.dbs.module.report.data.ReportAccessGroupUserNameData;

public interface ReportAccessGroupService {

	List<ReportAccessGroupListData> searchReportAccessGroup(ReportAccessGroupForm model);

	List<ReportAccessGroupListData> getReportAccessGroupListByActive(ReportAccessGroupForm model);

	List<ReportAccessGroupData> getReportAccessGroupListByTemplate(ReportAccessGroup model);

	List<ReportAccessGroupData> getReportAccessGroupListByCategory(ReportAccessGroup model);

	List<ReportAccessGroupData> getReportAccessGroupListByUser(ReportAccessGroup model);

	List<ReportAccessGroupData> getReportAccessGroupListByRole(ReportAccessGroup model);

	List<ReportAccessGroupTemplateNameData> getReportNameList(ReportAccessGroup model);

	List<ReportAccessGroupCategoryNameData> getCatgNameList(ReportAccessGroup model);

	List<ReportAccessGroupUserNameData> getUserNameList(ReportAccessGroup model);

	List<ReportAccessGroupRoleNameData> getRoleNameList(ReportAccessGroup model);

	ReportAccessGroupObjectData getReportAccessGroup(ReportAccessGroup model);

	int createReportAccessGroup(ReportAccessGroup model);

	int createReportAccessGroupTemplate(ReportAccessGroup model);

	int createReportAccessGroupCategory(ReportAccessGroup model);

	int createReportAccessGroupUser(ReportAccessGroup model);

	int createReportAccessGroupRole(ReportAccessGroup model);

	int updateReportAccessGroup(ReportAccessGroup model);

	int updateReportAccessGroupTemplate(ReportAccessGroup model);

	int updateReportAccessGroupCategory(ReportAccessGroup model);

	int updateReportAccessGroupUser(ReportAccessGroup model);

	int updateReportAccessGroupRole(ReportAccessGroup model);

	int deleteReportAccessGroup(ReportAccessGroup model);

	int deleteReportAccessGroupTemplate(ReportAccessGroup model);

	int deleteReportAccessGroupCategory(ReportAccessGroup model);

	int deleteReportAccessGroupUser(ReportAccessGroup model);

	int deleteReportAccessGroupRole(ReportAccessGroup model);
}
