package com.dbs.vtsp.batch;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.batch.model.ArcEventOrderTradeModel;
import com.dbs.vtsp.batch.processor.ArcEventOrderTradeProcessor;
import com.dbs.vtsp.batch.writer.ArcEventOrderTradeWriter;

//@Configuration
//@EnableBatchProcessing
public class ArcEventOrderTradeReportJob {

	private static final Logger logger = LoggerFactory.getLogger(ArcEventOrderTradeReportJob.class);

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	private String requestId;

	@Bean
	public Job readCSVFilesJob() {
		return jobBuilderFactory.get("readCSVFilesJob").incrementer(new RunIdIncrementer()).start(step1()).build();
	}

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<ArcEventOrderTradeModel, ArcEventOrderTradeModel>chunk(5).reader(multiResourceItemReader()).processor(processor()).writer(writer()).build();
	}

	@Bean
	@StepScope
	public MultiResourceItemReader<ArcEventOrderTradeModel> multiResourceItemReader() {
		requestId = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		logger.info("[multiResourceItemReader] report generate request id: " + requestId);
		String fileSet = "input/ARC_EVENTS-Order_and_Trade_Flow-20200924.csv";
		Resource[] inputResources = null;
		FileSystemXmlApplicationContext patternResolver = null;
		MultiResourceItemReader<ArcEventOrderTradeModel> resourceItemReader = null;
		try {
			patternResolver = new FileSystemXmlApplicationContext();
			inputResources = patternResolver.getResources(fileSet);
			resourceItemReader = new MultiResourceItemReader<>();
			resourceItemReader.setResources(inputResources);
			resourceItemReader.setDelegate(reader());
		} catch (IOException e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		} finally {
			if (null != patternResolver)
				patternResolver.close();
		}
		return resourceItemReader;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public FlatFileItemReader<ArcEventOrderTradeModel> reader() {
		FlatFileItemReader<ArcEventOrderTradeModel> reader = new FlatFileItemReader<>();
		reader.setLinesToSkip(1);

		DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
		delimitedLineTokenizer.setNames(new String[] { "id", "code", "value" });

		BeanWrapperFieldSetMapper beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
		beanWrapperFieldSetMapper.setTargetType(ArcEventOrderTradeModel.class);

		DefaultLineMapper defaultLineMapper = new DefaultLineMapper();
		defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
		defaultLineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);

		reader.setLineMapper(defaultLineMapper);
		return reader;
	}

	@Bean
	@StepScope
	public ArcEventOrderTradeProcessor processor() {
		logger.info("[processor] report generate request id: " + requestId);
		String filterRule = "00000357241ARSG1";
		return new ArcEventOrderTradeProcessor(filterRule);
	}

	@Bean
	@StepScope
	public ArcEventOrderTradeWriter<ArcEventOrderTradeModel> writer() {
		logger.info("[writer] report generate request id: " + requestId);
		return new ArcEventOrderTradeWriter<>("./", requestId, ".csv");
	}
}
