package com.dbs.vtsp.model;

import java.sql.Timestamp;
import java.util.List;

import com.dbs.module.model.AbstractFortifyStrategyModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractAuditModel extends AbstractFortifyStrategyModel {

	@JsonIgnore
	private String oneBankId;

	@JsonIgnore
	private List<String> oneBankRoleList;

	@JsonIgnore
	private String createdBy;

	@JsonIgnore
	private Timestamp createdDate;

	@JsonIgnore
	private String lastUpdatedBy;

	@JsonIgnore
	private Timestamp lastUpdatedDate;

	@JsonIgnore
	private String defaultSearchText;

	@JsonIgnore
	public String getLastUpdatedDatetimeStr() {
		return null == lastUpdatedDate ? null : lastUpdatedDate.toString();
	}
}
