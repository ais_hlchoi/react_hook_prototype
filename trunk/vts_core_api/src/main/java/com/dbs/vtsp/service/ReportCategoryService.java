package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.data.*;
import com.dbs.module.report.model.ReportCategoryFormModel;

public interface ReportCategoryService {

	List<ReportCategoryTemplateNameData> getReportNameList(ReportCategoryForm model);

	ReportCategoryPropsData getReportCategoryProps(ReportCategory model);

	ReportCategoryObjectData getReportCategory(ReportCategory model);

	List<ReportCategoryListData> searchAll(ReportCategoryFormModel model);

	int createReportCategory(ReportCategory model);

	int createReportCategoryTemplate(ReportCategory model);

	int updateReportCategory(ReportCategory model);

	int updateReportCategoryTemplate(ReportCategory model);

	int deleteReportCategory(ReportCategory model);

	int deleteReportCategoryTemplate(ReportCategory model);

	List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model);

	List<ReportScheduleData> getReportScheduleList(ReportSchedule model);
}
