package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupAcsgUserModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptAcsgId;

	@JsonInclude
	private String userId;
}
