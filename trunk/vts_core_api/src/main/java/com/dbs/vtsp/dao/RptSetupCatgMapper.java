package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptSetupCatgTmplModel;
import com.dbs.vtsp.model.RptSetupCatgModel;

public interface RptSetupCatgMapper {

	List<RptSetupCatgModel> getUserReportCategoryListByTemplate(Integer id);

	RptSetupCatgModel getUserReportCategory(Integer id);

	List<RptSetupCatgTmplModel> getUserReportCategoryTemplateList(Integer id);

	int getUserReportCategoryTemplateDispSeqNextVal(Integer id);

	int getUserReportCategoryTemplateCount(RptSetupCatgTmplModel model);

	int insert(RptSetupCatgModel model);

	int insertGroupTemplate(RptSetupCatgTmplModel model);

	int delete(Integer id);

	int deleteGroupTemplate(RptSetupCatgTmplModel model);

	int deleteGroupTemplateByTemplate(Integer id);

	int deleteGroupTemplateByCategory(Integer id);

	int update(RptSetupCatgModel model);

	int updateGroupTemplateDispSeq(RptSetupCatgTmplModel model);
}
