package com.dbs.vtsp.security.auth.jwt;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 8225599274069226978L;

	public CustomAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}

	public CustomAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
	}

	public CustomAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, Object details) {
		super(principal, credentials, authorities);
		setDetails(details);
	}

	public CustomAuthenticationToken(UserDetails user, Object details) {
		this(user.getUsername(), user.getPassword(), user.getAuthorities(), details);
	}
}
