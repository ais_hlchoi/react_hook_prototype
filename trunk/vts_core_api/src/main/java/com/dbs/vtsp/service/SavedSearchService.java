package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportAccessGroupForm;
import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.SavedSearchForm;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportCategoryListData;
import com.dbs.module.report.data.SavedSearchAccessGroupNameData;
import com.dbs.module.report.data.SavedSearchListData;
import com.dbs.module.report.model.ReportCategoryCountModel;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;

public interface SavedSearchService {

	List<ReportTemplatePrivilegeModel> searchReportTemplate(SavedSearchForm model);

	List<SavedSearchListData> searchReportTemplateAll(SavedSearchForm model);

	List<ReportCategoryCountModel> searchReportCategory(SavedSearchForm model);

	List<ReportCategoryListData> searchReportCategoryAll(SavedSearchForm model);

	List<SavedSearchAccessGroupNameData> getReportAccessGroupNameList(ReportAccessGroupForm model);

	List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model);
}
