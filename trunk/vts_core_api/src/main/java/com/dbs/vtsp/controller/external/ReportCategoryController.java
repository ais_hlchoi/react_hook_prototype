package com.dbs.vtsp.controller.external;

import java.util.List;

import com.dbs.module.report.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.module.report.model.ReportCategoryFormModel;
import com.dbs.module.report.model.ReportCategoryModel;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.dbs.vtsp.service.ReportCategoryService;

@RestController
@CrossOrigin
@RequestMapping("/api/reportCategory")
@RoleAccess
public class ReportCategoryController {

	@Autowired
	private ReportCategoryService reportCategoryService;

	@Autowired
	private ReportAccessGroupService reportAccessGroupService;

	@RequestMapping(value = "/getReportCategory", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportCategory(@RequestBody ReportCategoryModel model) {
		ReportCategoryObjectData item = reportCategoryService.getReportCategory(model);
		return RestEntity.success(item);
	}

	@RequestMapping(value = "/searchAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchAll(@RequestBody ReportCategoryFormModel model) {
		List<ReportCategoryListData> list = reportCategoryService.searchAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportAccessGroupList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportAccessGroupList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupData> list = reportAccessGroupService.getReportAccessGroupListByCategory(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportAccessGroupRoleList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportAccessGroupRoleList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupRoleNameData> list = reportCategoryService.getReportAccessGroupRoleList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportScheduleList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportScheduleList(@RequestBody ReportScheduleModel model) {
		List<ReportScheduleData> list = reportCategoryService.getReportScheduleList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insert(@RequestBody ReportCategoryModel model) {
		if (null != model.getId())
			return update(model);

		int id = reportCategoryService.createReportCategory(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/insertReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insertReport(@RequestBody ReportCategoryModel model) {
		reportCategoryService.createReportCategoryTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity update(@RequestBody ReportCategoryModel model) {
		if (null == model.getId())
			return insert(model);

		int id = reportCategoryService.updateReportCategory(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/updateReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateReport(@RequestBody ReportCategoryModel model) {
		reportCategoryService.updateReportCategoryTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity delete(@RequestBody ReportCategoryModel model) {
		reportCategoryService.deleteReportCategory(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/deleteReport", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity deleteReport(@RequestBody ReportCategoryModel model) {
		reportCategoryService.deleteReportCategoryTemplate(model);
		return RestEntity.success();
	}

	@RequestMapping(value = "/getReportNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportNameList(@RequestBody ReportCategoryFormModel model) {
		List<ReportCategoryTemplateNameData> list = reportCategoryService.getReportNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportCategoryProps", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getReportCategoryProps(@RequestBody ReportCategoryModel model) {
		ReportCategoryPropsData item = reportCategoryService.getReportCategoryProps(model);
		return RestEntity.success(item);
	}
}
