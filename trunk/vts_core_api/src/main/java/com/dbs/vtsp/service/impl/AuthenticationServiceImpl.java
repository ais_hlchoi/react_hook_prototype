package com.dbs.vtsp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.vtsp.dao.AuthenticationMapper;
import com.dbs.vtsp.model.RoleAccessModel;
import com.dbs.vtsp.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl extends AbstractServiceImpl implements AuthenticationService {

	@Autowired
	private AuthenticationMapper authenticationMapper;

	@Override
	public Integer hasRoleAccess(RoleAccessModel roleAccessModel) {
		return authenticationMapper.hasRoleAccess(roleAccessModel);
	}
}
