package com.dbs.vtsp.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.dbs.vtsp.batch.model.ArcEventOrderTradeModel;

public class ArcEventOrderTradeProcessor implements ItemProcessor<ArcEventOrderTradeModel, ArcEventOrderTradeModel> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private String filterRule;

	public ArcEventOrderTradeProcessor(String filterRule) {
		this.filterRule = filterRule;
	}

	@Override
	public ArcEventOrderTradeModel process(final ArcEventOrderTradeModel order) throws Exception {
		if (null != order)
			logger.info("add row (" + order.toString() + ") based on [ " + filterRule + "]");

		return order;
	}
}
