package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldDataModel extends AbstractAuditModel {

	@JsonInclude
	private Integer fileDnldId;

	@JsonInclude
	private String rptSql;

	@JsonInclude
	private String rptLayout;

	@JsonInclude
	private String refType;

	@JsonInclude
	private Integer refId;

	@JsonInclude
	private Integer refSched;
}
