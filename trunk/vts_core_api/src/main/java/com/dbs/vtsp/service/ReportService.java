package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportProps;
import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.ReportTemplateForm;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportPropsData;
import com.dbs.module.report.data.ReportScheduleData;
import com.dbs.module.report.data.ReportTemplateData;
import com.dbs.module.report.data.ReportTemplateListData;
import com.dbs.module.report.data.ReportTemplateNameData;

public interface ReportService {

	List<ReportTemplateListData> getReportPrivilegeList(ReportTemplateForm model);

	List<ReportTemplateNameData> getReportNameList(ReportTemplateForm model);

	ReportPropsData getReportProps(ReportProps model);

	ReportTemplateData getReportTemplate(ReportProps model);

	int createReportTemplate(ReportProps model);

	int updateReportTemplate(ReportProps model);

	int deleteReportTemplate(ReportProps model);

	int createReportTemplate(ReportTemplate model);

	int updateReportTemplate(ReportTemplate model);

	int deleteReportTemplate(ReportTemplate model);

	List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model);

	List<ReportScheduleData> getReportScheduleList(ReportSchedule model);

	ReportScheduleData getReportSchedule(ReportSchedule model);

	int createReportSchedule(ReportSchedule model);

	int updateReportSchedule(ReportSchedule model);

	int updateReportScheduleDispSeq(ReportSchedule model);

	int updateReportScheduleLastExecution(ReportSchedule model);

	int deleteReportSchedule(ReportSchedule model);
}
