package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupTmplFileModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer fileId;

	@JsonInclude
	private String fileLabel;
}
