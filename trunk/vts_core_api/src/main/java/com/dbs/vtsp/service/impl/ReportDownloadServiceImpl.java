package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dbs.module.aws.util.S3ClientUtils;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.ReportDownloadForm;
import com.dbs.module.download.config.DownloadConfig;
import com.dbs.module.download.data.ReportDownloadData;
import com.dbs.module.download.model.ReportDownloadFormModel;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.export.util.ReportFilePathUtils;
import com.dbs.module.file.FileImport;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.util.Assert;
import com.dbs.util.FileUtil;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.FileDnldMapper;
import com.dbs.vtsp.dao.RptDnldMapper;
import com.dbs.vtsp.model.RptFileDnldHistoryModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.RptFileDnldSummaryModel;
import com.dbs.vtsp.service.ReportDownloadService;

@Service
public class ReportDownloadServiceImpl extends AbstractServiceImpl implements ReportDownloadService {

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private RptDnldMapper rptDnldMapper;

	@Autowired
	private FileDnldMapper fileDnldMapper;

	@Autowired
	private DownloadConfig downloadConfig;

	@Autowired
	private FileImport fileImport;

	public List<ReportDownloadData> search(ReportDownloadForm model) {
		List<ReportDownloadObjectModel> list = rptDnldMapper.searchReportDownload(model);
		return null == list ? new ArrayList<>() : list.stream().map(this::parseData).collect(Collectors.toList());
	}

	public List<RptFileDnldSummaryModel> searchDownloadReport(ReportDownloadForm model) {
		List<RptFileDnldSummaryModel> list = rptDnldMapper.searchDownloadReport(model);
		return CollectionUtils.isEmpty(list) ? Collections.emptyList() : list;
	}

	public List<RptFileDnldHistoryModel> searchDownloadReportHistory(ReportDownloadFormModel model) {
		List<RptFileDnldHistoryModel> list = rptDnldMapper.searchDownloadReportHistory(model);
		return CollectionUtils.isEmpty(list) ? Collections.emptyList() : list;
	}

	public void outputFile(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = fileDnldMapper.getFileDownload(model.getId());
		Assert.notNull(bean, "Download task cannot be null");
		Assert.notBlank(bean.getFilePath(), "File path cannot be null");
		String filePath = FortifyStrategyUtils.toPathString(ReportFilePathUtils.restoreFileOutputPath(downloadConfig.getFolderOutput(), bean.getFilePath()));
		Assert.isTrue(fileImport.exist(filePath), String.format("No file is located at [ %s ]", filePath));
		if (S3ClientUtils.isService()) {
			FileUtil.downloadFileUTF8Byte(response, fileImport.getFileByte(filePath), bean.getFileName(), ExportOption.getOrElse(bean.getFileExtension()).contentType);
		} else {
			FileUtil.downloadFileUTF8(response, fileImport.getFile(filePath), bean.getFileName(), ExportOption.getOrElse(bean.getFileExtension()).contentType);
		}
	}

	private ReportDownloadData parseData(ReportDownloadObjectModel model) {
		model.setFilePathPrefix(downloadConfig.getFileDownloadPathPrefix());
		return new ReportDownloadData(model);
	}
}
