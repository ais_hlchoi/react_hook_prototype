package com.dbs.vtsp.service.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.util.FileDownloadUtils;
import com.dbs.module.download.util.ReportDownloadUtils;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.constant.ReportExportType;
import com.dbs.module.report.model.ReportCategoryTemplateModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.ReportPropsUtils;
import com.dbs.module.report.util.ReportTemplateUtils;
import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.FileDnldMapper;
import com.dbs.vtsp.dao.RptCatgMapper;
import com.dbs.vtsp.dao.RptDnldMapper;
import com.dbs.vtsp.dao.RptTmplMapper;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldLogModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.service.FileDownloadService;

@Service
public class FileDownloadServiceImpl extends AbstractServiceImpl implements FileDownloadService {

	private final Logger logger = LoggerFactory.getLogger(FileDownloadServiceImpl.class);

	@Autowired
	private RptCatgMapper rptCatgMapper;

	@Autowired
	private RptDnldMapper rptDnldMapper;

	@Autowired
	private RptTmplMapper rptTmplMapper;

	@Autowired
	private FileDnldMapper fileDnldMapper;

	@Autowired
	private DynQuerySqlUtils DynQuerySqlUtils;

	public RptFileDnldModel getFileDownload(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		return fileDnldMapper.getFileDownload(model.getId());
	}

	public RptFileDnldDataModel getFileDownloadData(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		return fileDnldMapper.getFileDownloadData(model.getId());
	}

	public int createFileDownloadCheckCount(ReportDownload model) {
		Assert.isFalse(isDownloadFoundInHistory(model), "Same user within same report cannot execute more than 1 time");
		return createFileDownload(model);
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int createFileDownload(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownload(model);
		Assert.isTrue(fileDnldMapper.createFileDownload(bean) == 1, String.format("Fail to insert file download %s", getOrElse(model.getReportName(), "")));
		Assert.isTrue(fileDnldMapper.createFileDownloadUserGroup(FileDownloadUtils.parseReportDownloadUserGroup(model, bean.getId())) == 1, String.format("Fail to insert file download user group %s", getOrElse(model.getReportName(), "")));
		switch (ReportExportType.getOrElse(model.getReportType())) {
		case R:
		case C:
			if (StringUtils.isNotBlank(model.getSql()))
				Assert.isTrue(fileDnldMapper.createFileDownloadData(FileDownloadUtils.parseReportDownloadData(model, bean.getId())) == 1, String.format("Fail to insert file download data %s", getOrElse(model.getReportName(), "")));
			break;
		case S:
			List<ReportCategoryTemplateModel> list = rptCatgMapper.getUserReportCategoryTemplateNameList(model.getRptCatgId());
			Assert.isTrue(CollectionUtils.isNotEmpty(list), "Reports cannot be empty");
			list.stream().forEach(o -> createFileDownloadChild(model, o.getId(), bean.getId()));
			break;
		default:
			break;
		}
		return bean.getId();
	}

	private static final String LABEL_ID = "id: ";
	private static final String MSG_FAIL_ROW_COUNT_FORMAT   = "Fail to update file download [ id: %s, rowCount: %s ]";
	private static final String MSG_FAIL_ROW_PROCESS_FORMAT = "Fail to update file download [ id: %s, rowProcess: %s ]";
	private static final String MSG_FAIL_FILE_SIZE_FORMAT   = "Fail to update file download [ id: %s, fileSize: %s ]";

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadStatus(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownloadStatus(model);
		Assert.isTrue(fileDnldMapper.updateFileDownloadStatus(bean) == 1, String.format(MSG_FAIL_ROW_PROCESS_FORMAT, model.getId(), model.getRowProcess()));
		logger.info(String.format("Update file download status %s", FortifyStrategyUtils.toLogString(LABEL_ID + model.getId(), model.getStatus())));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadStart(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownloadStart(model);
		Assert.isTrue(fileDnldMapper.updateFileDownloadStart(bean) == 1, String.format(MSG_FAIL_ROW_COUNT_FORMAT, model.getId(), model.getRowCount()));
		logger.info(String.format("Update file download when start %s", FortifyStrategyUtils.toLogString(LABEL_ID + model.getId(), model.getRowCount() + " rows total")));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadProcess(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownloadProcess(model);
		Assert.isTrue(fileDnldMapper.updateFileDownloadProcess(bean) == 1, String.format(MSG_FAIL_ROW_PROCESS_FORMAT, model.getId(), model.getRowProcess()));
		logger.info(String.format("Update file download when processing %s", FortifyStrategyUtils.toLogString(LABEL_ID + model.getId(), model.getRowProcess() + " rows procesed")));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadFailure(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownloadFailure(model);
		Assert.isTrue(fileDnldMapper.updateFileDownloadFailure(bean) == 1, String.format(MSG_FAIL_ROW_PROCESS_FORMAT, model.getId(), model.getRowProcess()));
		logger.info(String.format("Update file download when failure %s", FortifyStrategyUtils.toLogString(LABEL_ID + model.getId(), model.getRowProcess() + " rows processed")));
		fileDnldMapper.updateFileDownloadLogAged(model.getId());
		RptFileDnldLogModel log = FileDownloadUtils.parseReportDownloadLog(model);
		Assert.isTrue(fileDnldMapper.createFileDownloadLog(log) == 1, String.format(MSG_FAIL_ROW_PROCESS_FORMAT, model.getId(), model.getRowProcess()));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadComplete(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptFileDnldModel bean = FileDownloadUtils.parseReportDownloadComplete(model);
		Assert.isTrue(fileDnldMapper.updateFileDownloadComplete(bean) == 1, String.format(MSG_FAIL_FILE_SIZE_FORMAT, model.getId(), model.getFileSize()));
		logger.info(String.format("Update file download when completed %s", FortifyStrategyUtils.toLogString(LABEL_ID + model.getId(), FileDownloadUtils.getFileSizeKb(model.getFileSize()) + "Kb")));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DOWNLOAD)
	public int updateFileDownloadStatusPending(ReportDownload model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		fileDnldMapper.updateFileDownloadStatusPending(model.getId());
		return model.getId();
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected void createFileDownloadChild(ReportDownload model, Integer rptTmplId, Integer dnldId) {
		ReportTemplateObjectModel item = rptTmplMapper.getUserReportTemplateObject(ReportPropsUtils.parse(rptTmplId));
		ReportTemplate tmpl = ReportTemplateUtils.parse(item, null == model.getModel().getOption() ? null : model.getModel().getOption().getParameter(), DynQuerySqlUtils);
		ReportDownload bean = ReportDownloadUtils.parsePending(tmpl, model, dnldId);
		createFileDownload(bean);
	}

	protected boolean isDownloadFoundInHistory(ReportDownload model) {
		return rptDnldMapper.getReportDownloadCountInHistory(model) > 0;
	}
}
