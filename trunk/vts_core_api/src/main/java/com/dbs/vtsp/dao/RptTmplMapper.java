package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.ReportProps;
import com.dbs.module.report.ReportTemplateForm;
import com.dbs.module.report.model.ReportTemplateNameModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;

public interface RptTmplMapper {

	List<ReportTemplatePrivilegeModel> searchUserReportTemplate(ReportTemplateForm model);

	List<ReportTemplateNameModel> getUserReportNameList(ReportTemplateForm model);

	ReportTemplateObjectModel getUserReportTemplateObject(ReportProps model);
}
