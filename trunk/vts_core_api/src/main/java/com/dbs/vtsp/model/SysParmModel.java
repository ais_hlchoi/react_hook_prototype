package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysParmModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String segment;

	@JsonInclude
	private String segmentValue;

	@JsonInclude
	private String segmentName;

	@JsonInclude
	private String code;

	@JsonInclude
	private String shortDesc;

	@JsonInclude
	private String longDesc;

	@JsonInclude
	private String shortDescTc;

	@JsonInclude
	private String longDescTc;

	@JsonInclude
	private String parmValue;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;
}
