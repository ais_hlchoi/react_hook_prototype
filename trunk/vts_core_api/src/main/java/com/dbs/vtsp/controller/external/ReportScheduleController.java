package com.dbs.vtsp.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.data.ReportScheduleData;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.vtsp.service.ReportService;

@RestController
@CrossOrigin
@RequestMapping("/api/reportSchedule")
@RoleAccess
public class ReportScheduleController {

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity find(@RequestBody ReportScheduleModel model) {
		ReportScheduleData item = reportService.getReportSchedule(model);
		return RestEntity.success(item);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity insert(@RequestBody ReportScheduleModel model) {
		if (null != model.getId())
			return update(model);

		int id = reportService.createReportSchedule(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity update(@RequestBody ReportScheduleModel model) {
		if (null == model.getId())
			return insert(model);

		int id = reportService.updateReportSchedule(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/updateDispSeq", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateDispSeq(@RequestBody ReportScheduleModel model) {
		int id = reportService.updateReportScheduleDispSeq(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/updateLastExecution", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity updateLastExecution(@RequestBody ReportScheduleModel model) {
		int id = reportService.updateReportScheduleLastExecution(model);
		return RestEntity.success(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity delete(@RequestBody ReportScheduleModel model) {
		reportService.deleteReportSchedule(model);
		return RestEntity.success();
	}
}
