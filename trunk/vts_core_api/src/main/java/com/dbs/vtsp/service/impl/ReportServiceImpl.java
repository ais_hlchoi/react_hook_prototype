package com.dbs.vtsp.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.dbs.module.report.constant.*;
import com.dbs.module.report.util.*;
import com.dbs.util.ConstantUtil;
import com.dbs.util.CronExpressionUtils;
import com.dbs.vtsp.dao.*;
import com.dbs.vtsp.model.*;
import com.dbs.vtsp.service.SysEmailTmplService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportProps;
import com.dbs.module.report.ReportPropsParameter;
import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.ReportTemplateForm;
import com.dbs.module.report.config.ReportConfig;
import com.dbs.module.report.data.ReportAccessGroupObjectData;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportPropsData;
import com.dbs.module.report.data.ReportScheduleData;
import com.dbs.module.report.data.ReportTemplateData;
import com.dbs.module.report.data.ReportTemplateListData;
import com.dbs.module.report.data.ReportTemplateNameData;
import com.dbs.module.report.model.ReportAccessGroupObjectModel;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.report.model.ReportTemplateNameModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.dbs.vtsp.service.ReportService;

@Service
public class ReportServiceImpl extends AbstractServiceImpl implements ReportService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RptTmplMapper rptTmplMapper;

	@Autowired
	private RptParmMapper rptParmMapper;

	@Autowired
	private RptAcsgMapper rptAcsgMapper;

	@Autowired
	private RptSetupTmplMapper rptSetupTmplMapper;

	@Autowired
	private RptSetupCatgMapper rptSetupCatgMapper;

	@Autowired
	private RptSetupAcsgMapper rptSetupAcsgMapper;

	@Autowired
	private RptSetupSchedMapper rptSetupSchedMapper;

	@Autowired
	private SysEmailTmplMapper sysEmailTmplMapper;

	@Autowired
	private ReportAccessGroupService reportAccessGroupService;


	@Autowired
	private SysEmailTmplService sysEmailTmplService;


	@Autowired
	private ReportConfig reportConfig;

	// ----------------------------------------------------------------------------------------------------
	// report template
	// ----------------------------------------------------------------------------------------------------

	public List<ReportTemplateListData> getReportPrivilegeList(ReportTemplateForm model) {
		List<ReportTemplatePrivilegeModel> list = rptTmplMapper.searchUserReportTemplate(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportTemplateListData::new).collect(Collectors.toList());
	}

	public List<ReportTemplateNameData> getReportNameList(ReportTemplateForm model) {
		List<ReportTemplateNameModel> list = rptTmplMapper.getUserReportNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportTemplateNameData::new).collect(Collectors.toList());
	}

	public ReportPropsData getReportProps(ReportProps model) {
		ReportTemplateObjectModel item = rptTmplMapper.getUserReportTemplateObject(model);
		ReportPropsData data = null == item ? null : new ReportPropsData(item);
		if (null == data || CollectionUtils.isEmpty(data.getParameter())) {
			return data;
		}
		Map<String, List<ReportParamModel>> map = data.getParameter().stream()
				.filter(o -> Datatype.ARRAY.value().equals(o.getDatatype()) && MapUtils.isNotEmpty(o.getAttributes()) && StringUtils.isNotBlank(o.getSegment()))
				.map(ReportPropsParameter::getSegment)
				.collect(Collectors.toMap(o -> o, o -> rptParmMapper.getParmValueList(o)));
		return MapUtils.isNotEmpty(map) ? data.put(map) : data;
	}

	public ReportTemplateData getReportTemplate(ReportProps model) {
		ReportTemplateObjectModel item = rptTmplMapper.getUserReportTemplateObject(model);
		ReportTemplateData data = null == item ? null : new ReportTemplateData(item);
		if (null == data || null == data.getOption() || CollectionUtils.isEmpty(data.getOption().getParameter())) {
			return data;
		}
		Map<String, List<ReportParamModel>> map = data.getOption().getParameter().stream()
				.filter(o -> Datatype.ARRAY.value().equals(o.getDatatype()) && MapUtils.isNotEmpty(o.getAttributes()) && StringUtils.isNotBlank(o.getSegment()))
				.map(ReportPropsParameter::getSegment)
				.collect(Collectors.toMap(o -> o, o -> rptParmMapper.getParmValueList(o)));
		return MapUtils.isNotEmpty(map) ? data.put(map) : data;
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportTemplate(ReportProps model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupTmplModel bean = VtspUserReportTemplateUtils.parseReport(model);
		Assert.isTrue(rptSetupTmplMapper.insert(bean) == 1, String.format("Fail to insert report %s", getOrElse(model.getName(), "")));
		return createReportTemplateObject(VtspUserReportTemplateUtils.parse(model, bean));
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportTemplate(ReportProps model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupTmplModel bean = VtspUserReportTemplateUtils.parseReport(model, rptSetupTmplMapper.getUserReportTemplate(model.getId()));
		Assert.isTrue(rptSetupTmplMapper.update(bean) == 1, String.format("Fail to update report %s", getOrElse(model.getName(), "")));
		deleteReportTemplateObject(bean.getId());
		return insertReportTemplateObject(VtspUserReportTemplateUtils.parse(model, bean));
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportTemplate(ReportProps model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.isTrue(rptSetupCatgMapper.getUserReportCategoryTemplateCount(VtspUserReportCategoryUtils.parseGroupTemplate(model.getId(), null, null)) < 1, String.format("Report %s is members of Report Category", getOrElse(model.getName(), "")));
		Assert.isTrue(deleteReportTemplateObjectFull(model.getId()) > 0, String.format("Fail to delete report %s", getOrElse(model.getName(), "")));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportTemplate(ReportTemplate model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupTmplModel bean = VtspUserReportTemplateUtils.parseReport(model);
		Assert.isTrue(rptSetupTmplMapper.insert(bean) == 1, String.format("Fail to insert report %s", getOrElse(model.getName(), "")));
		return createReportTemplateObject(VtspUserReportTemplateUtils.parse(model, bean));
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportTemplate(ReportTemplate model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupTmplModel bean = VtspUserReportTemplateUtils.parseReport(model, rptSetupTmplMapper.getUserReportTemplate(model.getId()));
		Assert.isTrue(rptSetupTmplMapper.update(bean) == 1, String.format("Fail to update report %s", getOrElse(model.getName(), "")));
		deleteReportTemplateObject(bean.getId());
		return insertReportTemplateObject(VtspUserReportTemplateUtils.parse(model, bean));
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportTemplate(ReportTemplate model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.isTrue(rptSetupCatgMapper.getUserReportCategoryTemplateCount(VtspUserReportCategoryUtils.parseGroupTemplate(model.getId(), null, null)) < 1, String.format("Report %s is members of Report Category", getOrElse(model.getName(), "")));
		Assert.isTrue(deleteReportTemplateObjectFull(model.getId()) > 0, String.format("Fail to delete report %s", getOrElse(model.getName(), "")));
		return model.getId();
	}

	// ----------------------------------------------------------------------------------------------------
	// report access group
	// ----------------------------------------------------------------------------------------------------

	public List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByTemplate(model.getRptTmplId());
		Integer id = CollectionUtils.isEmpty(list) ? null : list.get(0).getId();
		if (null == id)
			return new ArrayList<>();
		ReportAccessGroupObjectModel item = rptAcsgMapper.getUserReportAccessGroupObject(ReportAccessGroupUtils.parse(id));
		return null == item ? new ArrayList<>() : new ReportAccessGroupObjectData(item).getRoleList();
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	private int createReportAccessGroup(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupAcsgModel bean = VtspUserReportAccessGroupUtils.parse(model, ReportAccessGroupType.R);
		Assert.isTrue(rptSetupAcsgMapper.insert(bean) == 1, String.format("Fail to insert report category %s", getOrElse(model.getName(), "")));
		return bean.getId();
	}

	// ----------------------------------------------------------------------------------------------------
	// report schedule
	// ----------------------------------------------------------------------------------------------------

	public List<ReportScheduleData> getReportScheduleList(ReportSchedule model) {
		List<RptSetupSchedModel> list = rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(model));
		return null == list ? new ArrayList<>() : list.stream().map(ReportScheduleData::new).collect(Collectors.toList());
	}

	public ReportScheduleData getReportSchedule(ReportSchedule model) {
		RptSetupSchedModel item = rptSetupSchedMapper.getUserReportSchedule(model.getId());
		return null == item ? null : new ReportScheduleData(item);
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportSchedule(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parse(model);
		SysEmailTmplModel emailBean = VtspSysEmailTmplUtils.parse(model);
		if (StringUtils.isBlank(bean.getName()) && null != bean.getRefId()) {
			String refTable = bean.getRefTable();
			switch (ReportScheduleRefTable.getOrElse(refTable)) {
			case RPT_SETUP_CATG:
				RptSetupCatgModel catg = rptSetupCatgMapper.getUserReportCategory(bean.getRefId());
				bean.setName(null == catg ? String.format("Schedule Group Report %d", bean.getRefId()) : catg.getName());
				emailBean.setName(null == catg ? String.format("Schedule Group Report %d", bean.getRefId()) : catg.getName());
				break;
			case RPT_SETUP_TMPL:
				RptSetupTmplModel tmpl = rptSetupTmplMapper.getUserReportTemplate(bean.getRefId());
				bean.setName(null == tmpl ? String.format("Schedule Report %d", bean.getRefId()) : tmpl.getName());
				emailBean.setName(null == tmpl ? String.format("Schedule Report %d", bean.getRefId()) : tmpl.getName());
				break;
			default:
				bean.setName(String.format("Schedule %d", bean.getRefId()));
				emailBean.setName(String.format("Schedule %d", bean.getRefId()));
			}
		}

		String nextExecTime = CronExpressionUtils.getNextValidTimeBetweenStartDateEndDate(model.getCronExpression(),bean.getStartDate(),bean.getExpiredDate()) ;
		bean.setNextExecutionTime(nextExecTime);

		if (null == bean.getDispSeq()) {
			Integer seq = rptSetupSchedMapper.getReportScheduleDispSeqNextVal(ReportScheduleUtils.parse(bean));
			bean.setDispSeq(seq);
			Assert.isTrue(rptSetupSchedMapper.insert(bean) == 1, String.format("Fail to insert report schedule (%s)", getOrElse(model.getName(), "")));
			emailBean.setDispSeq(seq);
		} else {
			Assert.isTrue(rptSetupSchedMapper.insert(bean) == 1, String.format("Fail to insert report schedule (%s)", getOrElse(model.getName(), "")));
			if (rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(bean)).stream().filter(o -> !o.getId().equals(bean.getId())).anyMatch(o -> o.getDispSeq().equals(model.getDispSeq()))) {
				Assert.isTrue(rptSetupSchedMapper.updateDispSeqRecursive(bean) >= 1, String.format("Fail to insert report schedule %s (#%s +)", getOrElse(model.getName(), ""), model.getDispSeq()));
			}
			emailBean.setDispSeq(bean.getDispSeq());
		}

		emailBean.setRefId(bean.getId());
		emailBean.setRefTable(EmailRefTable.RPT_SETUP_SCHED.name());
		if(ConstantUtil.Y.equals(bean.getEmailInd())) {
			emailBean.setActiveInd(ConstantUtil.Y);
			sysEmailTmplService.update(emailBean);
		}
		else{
			sysEmailTmplService.updateActiveInd(emailBean,false);
		}
		return bean.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportSchedule(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupSchedModel existSetupModel =  rptSetupSchedMapper.getUserReportSchedule(model.getId());
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parse(model, existSetupModel);
		//need to reset next execution
		String nextExecTime = CronExpressionUtils.getNextValidTimeBetweenStartDateEndDate(model.getCronExpression(),bean.getStartDate(),bean.getExpiredDate()) ;
		bean.setNextExecutionTime(nextExecTime);


		Assert.isTrue(existSetupModel.getLastUpdatedDate().equals(bean.getLastUpdatedDate()) , String.format("Somewhere updated the record , Please refresh %s", getOrElse(model.getName(), "")));
		Assert.isTrue(rptSetupSchedMapper.update(bean) == 1, String.format("Fail to update report schedule %s", getOrElse(model.getName(), "")));
		if (null != model.getDispSeq() && !model.getDispSeq().equals(bean.getDispSeq())) {
			updateReportScheduleDispSeq(model, bean);
		}
		SysEmailTmplModel emailBean = VtspSysEmailTmplUtils.parse(model);
		emailBean.setRefId(bean.getId());
		emailBean.setRefTable(EmailRefTable.RPT_SETUP_SCHED.name());
		if(ConstantUtil.Y.equals(bean.getEmailInd())) {
			emailBean.setActiveInd(ConstantUtil.Y);
			sysEmailTmplService.update(emailBean);
		}
		else{
			sysEmailTmplService.updateActiveInd(emailBean,false);
		}
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportScheduleDispSeq(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parse(model, rptSetupSchedMapper.getUserReportSchedule(model.getId()));
		Assert.notNull(bean.getRefId(), String.format("Fail to update report schedule %s since report is invalid", getOrElse(model.getName(), "")));
		if (null != model.getDispSeq() && !model.getDispSeq().equals(bean.getDispSeq())) {
			updateReportScheduleDispSeq(model, bean);
		}
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportScheduleLastExecution(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parseLastExecution(model);
		Assert.isTrue(rptSetupSchedMapper.updateLastExecution(bean) == 1, String.format("Fail to update report schedule %s (%s)", getOrElse(model.getName(), ""), "LastExecutionTime"));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportScheduleNextExecution(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parseNextExecution(model);
		Assert.isTrue(rptSetupSchedMapper.updateNextExecution(bean) == 1, String.format("Fail to update report schedule %s (%s)", getOrElse(model.getName(), ""), "NextExecutionTime"));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportSchedule(ReportSchedule model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		sysEmailTmplService.delete(model.getId(),EmailRefTable.RPT_SETUP_SCHED.name());
		rptSetupSchedMapper.delete(model.getId());
		return model.getId();
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	private static final String ERR_INSERT_FORMAT = "Fail to insert report (%s)";

	protected int createReportTemplateObject(ReportTemplateObjectModel bean) {
		insertReportTemplateObject(bean);
		RptSetupTmplPrivsModel privs = VtspUserReportTemplateUtils.parsePrivilege(bean.getUserId(), bean.getId());
		Assert.isTrue(rptSetupTmplMapper.insertPrivilege(privs) == 1, String.format(ERR_INSERT_FORMAT, "Access Control"));
		String[] defaultAccessGroupNames = VtspUserReportAccessGroupUtils.getUserReportAccessGroupName(reportConfig.getDefaultUserReportAccessGroupName(), bean.getId()).split("\\s*,\\s*");
		Assert.isTrue(insertReportTemplateAccessGroup(defaultAccessGroupNames[0], bean.getId(), true) > 0, String.format(ERR_INSERT_FORMAT, "Access Group"));
		if (defaultAccessGroupNames.length > 1)
			Stream.of(Arrays.copyOfRange(defaultAccessGroupNames, 1, defaultAccessGroupNames.length)).forEach(name -> insertReportTemplateAccessGroup(name, bean.getId(), false));
		return bean.getId();
	}

	protected int insertReportTemplateObject(ReportTemplateObjectModel bean) {
		Assert.isTrue(!(bean.hasFile() && rptSetupTmplMapper.insertFile(bean.getFile()) != 1), String.format(ERR_INSERT_FORMAT, "Data Source"));
		Assert.isTrue(!(bean.hasTable() && bean.getTable().stream().map(o -> rptSetupTmplMapper.insertTable(o)).anyMatch(o -> o != 1)), String.format(ERR_INSERT_FORMAT, "Table"));
		Assert.isTrue(!(bean.hasColumn() && bean.getColumn().stream().map(o -> rptSetupTmplMapper.insertColumn(o)).anyMatch(o -> o != 1)), String.format(ERR_INSERT_FORMAT, "Column"));
		Assert.isTrue(!(bean.hasFilter() && bean.getFilter().stream().map(o -> rptSetupTmplMapper.insertFilter(o)).anyMatch(o -> o != 1)), String.format(ERR_INSERT_FORMAT, "Filter"));
		Assert.isTrue(!(bean.hasSort() && bean.getSort().stream().map(o -> rptSetupTmplMapper.insertSort(o)).anyMatch(o -> o != 1)), String.format(ERR_INSERT_FORMAT, "Sort"));
		Assert.isTrue(!(bean.hasCriteria() && bean.getCriteria().stream().map(o -> rptSetupTmplMapper.insertCriteria(o)).anyMatch(o -> o != 1)), String.format(ERR_INSERT_FORMAT, "Advance"));
		return bean.getId();
	}

	protected int deleteReportTemplateObject(Integer id) {
		if (null == id)
			return 0;
		rptSetupTmplMapper.deleteFile(id);
		rptSetupTmplMapper.deleteTable(id);
		rptSetupTmplMapper.deleteColumn(id);
		rptSetupTmplMapper.deleteFilter(id);
		rptSetupTmplMapper.deleteSort(id);
		rptSetupTmplMapper.deleteCriteria(id);
		return id;
	}

	protected int deleteReportTemplateObjectFull(Integer id) {
		if (deleteReportTemplateObject(id) < 1)
			return 0;
		List<RptSetupTmplPrivsModel> privs = rptSetupTmplMapper.getUserReportPrivilegeList(id);
		if (CollectionUtils.isNotEmpty(privs)) {
			String owner = privs.stream().filter(o -> Indicator.checkIfTrue(o.getOwnerInd())).map(RptSetupTmplPrivsModel::getUserId).map(String::valueOf).collect(Collectors.joining(", "));
			String users = privs.stream().filter(o -> Indicator.checkIfNotTrue(o.getOwnerInd())).map(RptSetupTmplPrivsModel::getUserId).map(String::valueOf).collect(Collectors.joining(", "));
			logger.info(String.format("There are %d rows to be deleted which affect owner %s and users %s", privs.size(), FortifyStrategyUtils.toLogString(owner), FortifyStrategyUtils.toLogString(users)));
		}
		rptSetupTmplMapper.deletePrivilegeByRptTmplId(id);
		List<ReportAccessGroupObjectModel> acsgs = rptAcsgMapper.getUserReportAccessGroupIdByType(ReportAccessGroupUtils.parse(id, ReportAccessGroupType.R));
		if (CollectionUtils.isNotEmpty(acsgs)) {
			acsgs.stream().forEach(item -> reportAccessGroupService.deleteReportAccessGroup(ReportAccessGroupUtils.parse(item.getId())));
		}
		rptSetupAcsgMapper.deleteGroupTemplateByTemplate(id);
		List<Integer> scheds = rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(ReportScheduleRefTable.RPT_SETUP_TMPL.name(), id)).stream().map(RptSetupSchedModel::getId).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(scheds)) {
			logger.info(String.format("There are %d rows to be deleted which affect schedule jobs %s", scheds.size(), FortifyStrategyUtils.toLogString(StringUtils.join(scheds, ", "))));
		}
		rptSetupSchedMapper.deleteByTemplate(id);
		rptSetupTmplMapper.delete(id);
		return id;
	}

	protected int insertReportTemplateAccessGroup(String name, Integer rptTmplId, boolean mandatory) {
		String categoryName = VtspUserReportAccessGroupUtils.getDefaultUserReportAccessGroupName(name);
		boolean isAccessGroupPreset = VtspUserReportAccessGroupUtils.isPresetUserReportAccessGroupName(categoryName);
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByName(categoryName);
		boolean isAccessGroupFound = !(CollectionUtils.isEmpty(list) || null == list.get(0));
		Integer id = isAccessGroupFound ? list.get(0).getId() : (isAccessGroupPreset ? createReportAccessGroup(ReportAccessGroupUtils.getPublicInstance(categoryName)) : null);
		if (null == id && rptSetupAcsgMapper.getUserReportAccessGroupRoleCodeCount(name) > 0)
			id = createReportAccessGroup(ReportAccessGroupUtils.getPublicInstance(name));
		Assert.isTrue(!(mandatory && null == id), "AccessGroup cannot be null");
		if (null == id || id < 1)
			return 0;
		rptSetupAcsgMapper.insertGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(rptTmplId, id));
		return id;
	}

	protected int updateReportScheduleDispSeq(ReportSchedule model, RptSetupSchedModel source) {
		RptSetupSchedModel bean = VtspUserReportScheduleUtils.parseDispSeq(model, source);
		if (null == bean) {
			return model.getId();
		}
		if (null != model.getDispSeq() && rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(bean)).stream().filter(o -> !o.getId().equals(model.getId())).anyMatch(o -> o.getDispSeq().equals(model.getDispSeq()))) {
			Assert.isTrue(rptSetupSchedMapper.updateDispSeqRecursive(bean) >= 1, String.format("Fail to update report schedule %s (#%s +)", getOrElse(model.getName(), ""), model.getDispSeq()));
		} else {
			Assert.isTrue(rptSetupSchedMapper.updateDispSeq(bean) == 1, String.format("Fail to update report schedule %s (#%s)", getOrElse(model.getName(), ""), null == model.getDispSeq() ? "N + 1" : String.valueOf(model.getDispSeq())));
		}
		return model.getId();
	}


}
