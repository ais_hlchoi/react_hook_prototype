package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportAccessGroupForm;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportAccessGroupType;
import com.dbs.module.report.data.ReportAccessGroupCategoryNameData;
import com.dbs.module.report.data.ReportAccessGroupData;
import com.dbs.module.report.data.ReportAccessGroupListData;
import com.dbs.module.report.data.ReportAccessGroupObjectData;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportAccessGroupTemplateNameData;
import com.dbs.module.report.data.ReportAccessGroupUserNameData;
import com.dbs.module.report.model.ReportAccessGroupCategoryModel;
import com.dbs.module.report.model.ReportAccessGroupCountModel;
import com.dbs.module.report.model.ReportAccessGroupObjectModel;
import com.dbs.module.report.model.ReportAccessGroupRoleModel;
import com.dbs.module.report.model.ReportAccessGroupTemplateModel;
import com.dbs.module.report.model.ReportAccessGroupUserModel;
import com.dbs.module.report.util.ReportAccessGroupUtils;
import com.dbs.module.report.util.VtspUserReportAccessGroupUtils;
import com.dbs.vtsp.dao.RptAcsgMapper;
import com.dbs.vtsp.dao.RptSetupAcsgMapper;
import com.dbs.vtsp.model.RptSetupAcsgCatgModel;
import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.dbs.vtsp.model.RptSetupAcsgRoleModel;
import com.dbs.vtsp.model.RptSetupAcsgTmplModel;
import com.dbs.vtsp.model.RptSetupAcsgUserModel;
import com.dbs.vtsp.service.ReportAccessGroupService;

@Service
public class ReportAccessGroupServiceImpl extends AbstractServiceImpl implements ReportAccessGroupService {

	@Autowired
	private RptAcsgMapper rptAcsgMapper;

	@Autowired
	private RptSetupAcsgMapper rptSetupAcsgMapper;

	private static final String ACCESS_GROUP = "AccessGroup";
	private static final String REPORT = "Report";

	private static final String ERR_INSERT_FORMAT = "Fail to insert report category (%s)";

	public List<ReportAccessGroupListData> searchReportAccessGroup(ReportAccessGroupForm model) {
		List<ReportAccessGroupCountModel> list = rptAcsgMapper.searchUserReportAccessGroup(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupListData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupListData> getReportAccessGroupListByActive(ReportAccessGroupForm model) {
		List<ReportAccessGroupCountModel> list = rptAcsgMapper.getUserReportAccessGroupList(model);
		return null == list ? new ArrayList<>() : list.stream().filter(o -> Indicator.checkIfTrue(o.getUserReportAccessGroup().getActiveInd())).map(ReportAccessGroupListData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupData> getReportAccessGroupListByTemplate(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByTemplate(model.getRptTmplId());
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupData> getReportAccessGroupListByCategory(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByCategory(model.getRptCatgId());
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupData> getReportAccessGroupListByUser(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByUser(model.getUserId());
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupData> getReportAccessGroupListByRole(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByRole(model.getRoleId());
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupTemplateNameData> getReportNameList(ReportAccessGroup model) {
		List<ReportAccessGroupTemplateModel> list = rptAcsgMapper.getReportNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupTemplateNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupCategoryNameData> getCatgNameList(ReportAccessGroup model) {
		List<ReportAccessGroupCategoryModel> list = rptAcsgMapper.getCatgNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupCategoryNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupUserNameData> getUserNameList(ReportAccessGroup model) {
		List<ReportAccessGroupUserModel> list = rptAcsgMapper.getUserNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupUserNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupRoleNameData> getRoleNameList(ReportAccessGroup model) {
		List<ReportAccessGroupRoleModel> list = rptAcsgMapper.getRoleNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupRoleNameData::new).collect(Collectors.toList());
	}

	public ReportAccessGroupObjectData getReportAccessGroup(ReportAccessGroup model) {
		ReportAccessGroupObjectModel item = rptAcsgMapper.getUserReportAccessGroupObject(model);
		return null == item ? null : new ReportAccessGroupObjectData(item);
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportAccessGroup(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupAcsgModel bean = VtspUserReportAccessGroupUtils.parse(model);
		Assert.isTrue(rptSetupAcsgMapper.insert(bean) == 1, String.format("Fail to insert report category %s", getOrElse(model.getName(), "")));
		return bean.getId();
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportAccessGroupTemplate(ReportAccessGroup model) {
		if (isTemplateFoundInGroup(model.getRptTmplId(), model.getId()))
			return 0;
		RptSetupAcsgTmplModel bean = VtspUserReportAccessGroupUtils.parseGroupTemplate(model);
		Assert.isTrue(rptSetupAcsgMapper.insertGroupTemplate(bean) == 1, String.format(ERR_INSERT_FORMAT, "Template Group"));
		return 1;
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportAccessGroupCategory(ReportAccessGroup model) {
		if (isCategoryFoundInGroup(model.getRptCatgId(), model.getId()))
			return 0;
		RptSetupAcsgCatgModel bean = VtspUserReportAccessGroupUtils.parseGroupCategory(model);
		Assert.isTrue(rptSetupAcsgMapper.insertGroupCategory(bean) == 1, String.format(ERR_INSERT_FORMAT, "Category Group"));
		return 1;
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportAccessGroupUser(ReportAccessGroup model) {
		if (isUserFoundInGroup(model.getUserId(), model.getId()))
			return 0;
		RptSetupAcsgUserModel bean = VtspUserReportAccessGroupUtils.parseGroupUser(model);
		Assert.isTrue(rptSetupAcsgMapper.insertGroupUser(bean) == 1, String.format(ERR_INSERT_FORMAT, "User Group"));
		return 1;
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportAccessGroupRole(ReportAccessGroup model) {
		if (isRoleFoundInGroup(model.getRoleId(), model.getId()))
			return 0;
		RptSetupAcsgRoleModel bean = VtspUserReportAccessGroupUtils.parseGroupRole(model);
		Assert.isTrue(rptSetupAcsgMapper.insertGroupRole(bean) == 1, String.format(ERR_INSERT_FORMAT, "Role Group"));
		return 1;
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportAccessGroup(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupAcsgModel bean = VtspUserReportAccessGroupUtils.parse(model, rptSetupAcsgMapper.getUserReportAccessGroup(model.getId()));
		Assert.isTrue(rptSetupAcsgMapper.update(bean) == 1, String.format("Fail to update report category %s", getOrElse(model.getName(), "")));
		updateReportAccessGroupTemplate(model.getId(), model.getRptTmplList());
		updateReportAccessGroupUser(model.getId(), model.getUserList());
		updateReportAccessGroupRole(model.getId(), model.getRoleList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportAccessGroupTemplate(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (null != model.getRptTmplId()) {
			updateReportAccessGroupTemplate(model.getRptAcsgList(), model.getRptTmplId());
			return model.getRptTmplId();
		}
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		updateReportAccessGroupTemplate(model.getId(), model.getRptTmplList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportAccessGroupCategory(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (null != model.getRptCatgId()) {
			updateReportAccessGroupCategory(model.getRptAcsgList(), model.getRptCatgId());
			return model.getRptCatgId();
		}
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		updateReportAccessGroupCategory(model.getId(), model.getRptCatgList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportAccessGroupUser(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (null != model.getUserId()) {
			updateReportAccessGroupUser(model.getRptAcsgList(), model.getUserId());
			return 1;
		}
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		updateReportAccessGroupUser(model.getId(), model.getUserList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportAccessGroupRole(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (null != model.getRoleId()) {
			updateReportAccessGroupRole(model.getRptAcsgList(), model.getRoleId());
			return model.getRoleId();
		}
		if (null != model.getRptTmplId() && null == model.getId()) {
			List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByTemplate(model.getRptTmplId());
			Integer id = CollectionUtils.isEmpty(list) ? null : list.get(0).getId();
			Assert.notNull(id, AssertResponseCode.NOT_NULL_ID.getMsg());
			updateReportAccessGroupRole(id, model.getRoleList());
			updateReportAccessGroupType(id, model);
			return id;
		}
		if (null != model.getRptCatgId() && null == model.getId()) {
			List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByCategory(model.getRptCatgId());
			Integer id = CollectionUtils.isEmpty(list) ? null : list.get(0).getId();
			Assert.notNull(id, AssertResponseCode.NOT_NULL_ID.getMsg());
			updateReportAccessGroupRole(id, model.getRoleList());
			updateReportAccessGroupType(id, model);
			return id;
		}
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		updateReportAccessGroupRole(model.getId(), model.getRoleList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportAccessGroup(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		rptSetupAcsgMapper.deleteGroupTemplateByAccessGroup(model.getId());
		rptSetupAcsgMapper.deleteGroupCategoryByAccessGroup(model.getId());
		rptSetupAcsgMapper.deleteGroupUserByAccessGroup(model.getId());
		rptSetupAcsgMapper.deleteGroupRoleByAccessGroup(model.getId());
		rptSetupAcsgMapper.delete(model.getId());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportAccessGroupTemplate(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL.getMsg(ACCESS_GROUP));
		Assert.notNull(model.getRptTmplId(), AssertResponseCode.NOT_NULL.getMsg(REPORT));
		return rptSetupAcsgMapper.deleteGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(model));
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportAccessGroupCategory(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL.getMsg(ACCESS_GROUP));
		Assert.notNull(model.getRptCatgId(), AssertResponseCode.NOT_NULL.getMsg(REPORT));
		return rptSetupAcsgMapper.deleteGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(model));
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportAccessGroupUser(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL.getMsg(ACCESS_GROUP));
		Assert.notNull(model.getUserId(), "User cannot be null");
		return rptSetupAcsgMapper.deleteGroupUser(VtspUserReportAccessGroupUtils.parseGroupUser(model));
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportAccessGroupRole(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL.getMsg(ACCESS_GROUP));
		Assert.notNull(model.getRoleId(), "Role cannot be null");
		return rptSetupAcsgMapper.deleteGroupRole(VtspUserReportAccessGroupUtils.parseGroupRole(model));
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected int insertReportTemplateAccessGroup(String name, Integer rptTmplId, boolean mandatory) {
		String categoryName = VtspUserReportAccessGroupUtils.getDefaultUserReportAccessGroupName(name);
		boolean isAccessGroupPublic = VtspUserReportAccessGroupUtils.CATEGORY_PUBLIC.equals(categoryName);
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByName(categoryName);
		boolean isAccessGroupFound = !(CollectionUtils.isEmpty(list) || null == list.get(0));
		Integer id = isAccessGroupFound ? list.get(0).getId() : (isAccessGroupPublic ? createReportAccessGroup(ReportAccessGroupUtils.getPublicInstance(categoryName)) : -1);
		Assert.isTrue(!(mandatory && null == id), AssertResponseCode.NOT_NULL.getMsg(ACCESS_GROUP));
		if (null == id || id < 1)
			return 0;
		rptSetupAcsgMapper.insertGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(rptTmplId, id));
		return id;
	}

	protected void updateReportAccessGroupTemplate(Integer id, List<Integer> groupList) {
		// if reportTemplateGroupList is null, then update reportAccessGroup only and no update to child reportTemplateGroup
		if (null == groupList)
			return;

		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupTemplateList(id).stream().map(RptSetupAcsgTmplModel::getRptTmplId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(o, id)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(o, id)));
		} else if (isTemplateFoundInGroup(null, id)) {
			rptSetupAcsgMapper.deleteGroupTemplateByAccessGroup(id);
		}
	}

	protected void updateReportAccessGroupTemplate(List<Integer> groupList, Integer id) {
		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupListByTemplate(id).stream().map(RptSetupAcsgModel::getId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(id, o)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupTemplate(VtspUserReportAccessGroupUtils.parseGroupTemplate(id, o)));
		} else if (isTemplateFoundInGroup(id, null)) {
			rptSetupAcsgMapper.deleteGroupTemplateByTemplate(id);
		}
	}

	protected void updateReportAccessGroupCategory(Integer id, List<Integer> groupList) {
		// if reportCategoryGroupList is null, then update reportAccessGroup only and no update to child reportCategoryGroup
		if (null == groupList)
			return;

		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupCategoryList(id).stream().map(RptSetupAcsgCatgModel::getRptCatgId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(o, id)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(o, id)));
		} else if (isCategoryFoundInGroup(null, id)) {
			rptSetupAcsgMapper.deleteGroupCategoryByAccessGroup(id);
		}
	}

	protected void updateReportAccessGroupCategory(List<Integer> groupList, Integer id) {
		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupListByCategory(id).stream().map(RptSetupAcsgModel::getId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(id, o)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(id, o)));
		} else if (isCategoryFoundInGroup(id, null)) {
			rptSetupAcsgMapper.deleteGroupCategoryByCategory(id);
		}
	}

	protected void updateReportAccessGroupUser(Integer id, List<String> groupList) {
		// if userGroupList is null, then update reportAccessGroup only and no update to child userGroup
		if (null == groupList)
			return;

		if (CollectionUtils.isNotEmpty(groupList)) {
			List<String> list = rptSetupAcsgMapper.getUserReportAccessGroupUserList(id).stream().map(RptSetupAcsgUserModel::getUserId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupUser(VtspUserReportAccessGroupUtils.parseGroupUser(o, id)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupUser(VtspUserReportAccessGroupUtils.parseGroupUser(o, id)));
		} else if (isUserFoundInGroup(null, id)) {
			rptSetupAcsgMapper.deleteGroupUserByAccessGroup(id);
		}
	}

	protected void updateReportAccessGroupUser(List<Integer> groupList, String id) {
		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupListByUser(id).stream().map(RptSetupAcsgModel::getId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupUser(VtspUserReportAccessGroupUtils.parseGroupUser(id, o)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupUser(VtspUserReportAccessGroupUtils.parseGroupUser(id, o)));
		} else if (isUserFoundInGroup(id, null)) {
			rptSetupAcsgMapper.deleteGroupUserByUser(id);
		}
	}

	protected void updateReportAccessGroupRole(Integer id, List<Integer> groupList) {
		// if roleGroupList is null, then update reportAccessGroup only and no update to child roleGroup
		if (null == groupList)
			return;

		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupRoleList(id).stream().map(RptSetupAcsgRoleModel::getRoleId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupRole(VtspUserReportAccessGroupUtils.parseGroupRole(o, id)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupRole(VtspUserReportAccessGroupUtils.parseGroupRole(o, id)));
		} else if (isRoleFoundInGroup(null, id)) {
			rptSetupAcsgMapper.deleteGroupRoleByAccessGroup(id);
		}
	}

	protected void updateReportAccessGroupRole(List<Integer> groupList, Integer id) {
		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupAcsgMapper.getUserReportAccessGroupListByRole(id).stream().map(RptSetupAcsgModel::getId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupAcsgMapper.deleteGroupRole(VtspUserReportAccessGroupUtils.parseGroupRole(id, o)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> rptSetupAcsgMapper.insertGroupRole(VtspUserReportAccessGroupUtils.parseGroupRole(id, o)));
		} else if (isRoleFoundInGroup(id, null)) {
			rptSetupAcsgMapper.deleteGroupRoleByRole(id);
		}
	}

	protected boolean isTemplateFoundInGroup(Integer rptTmplId, Integer id) {
		return rptSetupAcsgMapper.getUserReportAccessGroupTemplateCount(VtspUserReportAccessGroupUtils.parseGroupTemplate(rptTmplId, id)) > 0;
	}

	protected boolean isCategoryFoundInGroup(Integer rptCatgId, Integer id) {
		return rptSetupAcsgMapper.getUserReportAccessGroupCategoryCount(VtspUserReportAccessGroupUtils.parseGroupCategory(rptCatgId, id)) > 0;
	}

	protected boolean isUserFoundInGroup(String userId, Integer id) {
		return rptSetupAcsgMapper.getUserReportAccessGroupUserCount(VtspUserReportAccessGroupUtils.parseGroupUser(userId, id)) > 0;
	}

	protected boolean isRoleFoundInGroup(Integer roleId, Integer id) {
		return rptSetupAcsgMapper.getUserReportAccessGroupRoleCount(VtspUserReportAccessGroupUtils.parseGroupRole(roleId, id)) > 0;
	}

	protected void updateReportAccessGroupType(Integer id, ReportAccessGroup model) {
		if (null == id || null == model)
			return;

		RptSetupAcsgModel bean = rptSetupAcsgMapper.getUserReportAccessGroup(id);
		if (null == bean)
			return;

		switch (ReportAccessGroupType.getOrElse(bean.getType())) {
		case I:
			if (null != model.getRptTmplId()) {
				bean.setType(ReportAccessGroupType.R.name());
				rptSetupAcsgMapper.update(bean);
				return;
			}
			if (null != model.getRptCatgId()) {
				bean.setType(ReportAccessGroupType.S.name());
				rptSetupAcsgMapper.update(bean);
				return;
			}
			break;
		default:
			// nothing to do
		}
	}
}
