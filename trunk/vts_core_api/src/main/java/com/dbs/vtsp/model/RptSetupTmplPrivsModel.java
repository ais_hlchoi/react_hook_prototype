package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupTmplPrivsModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String userId;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private String ownerInd;

	@JsonInclude
	private String adminInd;

	@JsonInclude
	private String canSelect;

	@JsonInclude
	private String canUpdate;

	@JsonInclude
	private String canDelete;

	@JsonInclude
	private String canExport;

	@JsonInclude
	private String activeInd;
}
