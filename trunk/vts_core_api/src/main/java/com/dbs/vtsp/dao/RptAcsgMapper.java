package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportAccessGroupForm;
import com.dbs.module.report.model.ReportAccessGroupCategoryModel;
import com.dbs.module.report.model.ReportAccessGroupCountModel;
import com.dbs.module.report.model.ReportAccessGroupObjectModel;
import com.dbs.module.report.model.ReportAccessGroupRoleModel;
import com.dbs.module.report.model.ReportAccessGroupTemplateModel;
import com.dbs.module.report.model.ReportAccessGroupUserModel;

public interface RptAcsgMapper {

	List<ReportAccessGroupCountModel> searchUserReportAccessGroup(ReportAccessGroupForm model);

	List<ReportAccessGroupCountModel> getUserReportAccessGroupList(ReportAccessGroupForm model);

	List<ReportAccessGroupObjectModel> getUserReportAccessGroupIdByType(ReportAccessGroup model);

	ReportAccessGroupObjectModel getUserReportAccessGroupObject(ReportAccessGroup model);

	List<ReportAccessGroupTemplateModel> getReportNameList(ReportAccessGroup model);

	List<ReportAccessGroupCategoryModel> getCatgNameList(ReportAccessGroup model);

	List<ReportAccessGroupUserModel> getUserNameList(ReportAccessGroup model);

	List<ReportAccessGroupRoleModel> getRoleNameList(ReportAccessGroup model);

	List<ReportAccessGroupRoleModel> getUserReportAccessGroupRoleNameAllList();
}
