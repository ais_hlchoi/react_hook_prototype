package com.dbs.vtsp.service;

import com.dbs.vtsp.model.RoleAccessModel;

public interface AuthenticationService {
	Integer hasRoleAccess(RoleAccessModel roleAccessModel);
}
