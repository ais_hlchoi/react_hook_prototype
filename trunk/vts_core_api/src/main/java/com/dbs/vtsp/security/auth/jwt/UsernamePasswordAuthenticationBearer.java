/*
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dbs.vtsp.security.auth.jwt;

import java.text.ParseException;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.nimbusds.jwt.SignedJWT;

import reactor.core.publisher.Mono;

/**
 * This converter takes a SignedJWT and extracts all information
 * contained to build an Authentication Object
 * The signed JWT has already been verified.
 *
 */
public class UsernamePasswordAuthenticationBearer {

	public static Mono<Authentication> create(SignedJWT signedJWTMono) {
		return Mono.justOrEmpty(new UsernamePasswordAuthenticationBearer().issue(signedJWTMono));
	}

	private Authentication issue(SignedJWT signedJWTMono) {
		SignedJWT signedJWT = signedJWTMono;
		String subject;
		String roles;
		Object details;

		try {
			subject = signedJWT.getJWTClaimsSet().getSubject();
			roles = (String) signedJWT.getJWTClaimsSet().getClaim("roles");
			details = signedJWT.getJWTClaimsSet().getClaim("details");
		} catch (ParseException e) {
			return null;
		}

		if (StringUtils.isBlank(subject) || StringUtils.isBlank(roles))
			return null;

		if (null == details)
			details = new HashMap<>();

		return new CustomAuthenticationToken(subject, null, Stream.of(roles.split(",")).map(SimpleGrantedAuthority::new).collect(Collectors.toList()), details);
	}
}
