package com.dbs.vtsp.service;

import com.dbs.module.download.ReportDownload;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldModel;

public interface FileDownloadService {

	RptFileDnldModel getFileDownload(ReportDownload model);

	RptFileDnldDataModel getFileDownloadData(ReportDownload model);

	int createFileDownloadCheckCount(ReportDownload model);

	int createFileDownload(ReportDownload model);

	int updateFileDownloadStatus(ReportDownload model);

	int updateFileDownloadStart(ReportDownload model);

	int updateFileDownloadProcess(ReportDownload model);

	int updateFileDownloadFailure(ReportDownload model);

	int updateFileDownloadComplete(ReportDownload model);

	int updateFileDownloadStatusPending(ReportDownload model);
}
