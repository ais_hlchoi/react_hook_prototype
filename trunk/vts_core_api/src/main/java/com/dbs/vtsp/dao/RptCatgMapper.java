package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.model.ReportCategoryCountModel;
import com.dbs.module.report.model.ReportCategoryObjectModel;
import com.dbs.module.report.model.ReportCategoryTemplateModel;

public interface RptCatgMapper {

	List<ReportCategoryCountModel> searchUserReportCategory(ReportCategoryForm model);

	ReportCategoryObjectModel getUserReportCategoryObject(ReportCategory model);

	List<ReportCategoryTemplateModel> getUserReportCategoryTemplateNameList(Integer id);

	List<ReportCategoryTemplateModel> getUserReportNameList(ReportCategoryForm model);

	ReportCategoryObjectModel getUserReportCategoryTemplateObject(ReportCategory model);
}
