package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.UserTabColsModel;

public interface InformationSchemaMapper {

	List<UserTabColsModel> getUserTabColsList(UserTabColsModel model);

	List<String> getUserTabColNameList(UserTabColsModel model);
}
