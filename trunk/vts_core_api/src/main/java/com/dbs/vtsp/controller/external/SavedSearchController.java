package com.dbs.vtsp.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.constant.ReportBuildType;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportCategoryListData;
import com.dbs.module.report.data.SavedSearchAccessGroupNameData;
import com.dbs.module.report.data.SavedSearchListData;
import com.dbs.module.report.model.ReportAccessGroupFormModel;
import com.dbs.module.report.model.ReportAccessGroupModel;
import com.dbs.module.report.model.ReportCategoryCountModel;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.module.report.model.SavedSearchFormModel;
import com.dbs.util.PageInfoUtils;
import com.dbs.vtsp.service.SavedSearchService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/savedSearch")
@RoleAccess
public class SavedSearchController {

	@Autowired
	private SavedSearchService savedSearchService;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity search(@RequestBody SavedSearchFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportTemplatePrivilegeModel> list = savedSearchService.searchReportTemplate(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SavedSearchListData.class));
	}

	@RequestMapping(value = "/searchAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchAll(@RequestBody SavedSearchFormModel model) {
		List<SavedSearchListData> list = savedSearchService.searchReportTemplateAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/searchReportBuilder", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportBuilder(@RequestBody SavedSearchFormModel model) {
		model.setBuildType(ReportBuildType.REPORT_BUILDER.name());
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportTemplatePrivilegeModel> list = savedSearchService.searchReportTemplate(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SavedSearchListData.class));
	}

	@RequestMapping(value = "/searchReportBuilderAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportBuilderAll(@RequestBody SavedSearchFormModel model) {
		model.setBuildType(ReportBuildType.REPORT_BUILDER.name());
		List<SavedSearchListData> list = savedSearchService.searchReportTemplateAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/searchReportAdvance", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportAdvance(@RequestBody SavedSearchFormModel model) {
		model.setBuildType(ReportBuildType.REPORT_ADVANCE.name());
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportTemplatePrivilegeModel> list = savedSearchService.searchReportTemplate(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), SavedSearchListData.class));
	}

	@RequestMapping(value = "/searchReportAdvanceAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportAdvanceAll(@RequestBody SavedSearchFormModel model) {
		model.setBuildType(ReportBuildType.REPORT_ADVANCE.name());
		List<SavedSearchListData> list = savedSearchService.searchReportTemplateAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/searchReportCategory", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportCategory(@RequestBody SavedSearchFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportCategoryCountModel> list = savedSearchService.searchReportCategory(model);
		return RestEntity.success(PageInfoUtils.parse(new PageInfo<>(list), ReportCategoryListData.class));
	}

	@RequestMapping(value = "/searchReportCategoryAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchReportCategoryAll(@RequestBody SavedSearchFormModel model) {
		List<ReportCategoryListData> list = savedSearchService.searchReportCategoryAll(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportAccessGroupNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getAccessGroupNameList(@RequestBody ReportAccessGroupFormModel model) {
		List<SavedSearchAccessGroupNameData> list = savedSearchService.getReportAccessGroupNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getReportAccessGroupRoleList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getAccessGroupRoleList(@RequestBody ReportAccessGroupModel model) {
		List<ReportAccessGroupRoleNameData> list = savedSearchService.getReportAccessGroupRoleList(model);
		return RestEntity.success(list);
	}
}
