package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptSetupTmplColumnModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplFileModel;
import com.dbs.vtsp.model.RptSetupTmplFilterModel;
import com.dbs.vtsp.model.RptSetupTmplModel;
import com.dbs.vtsp.model.RptSetupTmplPrivsModel;
import com.dbs.vtsp.model.RptSetupTmplSortModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;

public interface RptSetupTmplMapper {

	RptSetupTmplModel getUserReportTemplate(Integer id);

	RptSetupTmplPrivsModel getUserReportPrivilege(Integer id);

	List<RptSetupTmplPrivsModel> getUserReportPrivilegeList(Integer id);

	List<RptSetupTmplTableModel> getUserReportSetupTable(List<Integer> ids);

	int insert(RptSetupTmplModel model);

	int insertFile(RptSetupTmplFileModel model);

	int insertTable(RptSetupTmplTableModel model);

	int insertColumn(RptSetupTmplColumnModel model);

	int insertFilter(RptSetupTmplFilterModel model);

	int insertSort(RptSetupTmplSortModel model);

	int insertCriteria(RptSetupTmplCriteModel model);

	int insertPrivilege(RptSetupTmplPrivsModel model);

	int delete(Integer id);

	int deleteFile(Integer id);

	int deleteTable(Integer id);

	int deleteColumn(Integer id);

	int deleteFilter(Integer id);

	int deleteSort(Integer id);

	int deleteCriteria(Integer id);

	int deletePrivilege(Integer id);

	int deletePrivilegeByRptTmplId(Integer id);

	int update(RptSetupTmplModel model);

	int updatePrivilege(RptSetupTmplPrivsModel model);

	int updateOwner(RptSetupTmplPrivsModel model);
}
