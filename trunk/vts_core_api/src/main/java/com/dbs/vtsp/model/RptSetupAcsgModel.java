package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupAcsgModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String name;

	@JsonInclude
	private String type;

	@JsonInclude
	private String activeInd;
}
