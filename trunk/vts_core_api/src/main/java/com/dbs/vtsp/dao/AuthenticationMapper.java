package com.dbs.vtsp.dao;

import com.dbs.vtsp.model.RoleAccessModel;

public interface AuthenticationMapper {
	Integer hasRoleAccess(RoleAccessModel roleAccess);
}
