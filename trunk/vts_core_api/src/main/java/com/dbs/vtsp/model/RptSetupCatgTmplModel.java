package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupCatgTmplModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptCatgId;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer dispSeq;
}
