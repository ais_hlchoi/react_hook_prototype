package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.dbs.module.report.data.*;
import com.dbs.module.report.model.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.OperationLogAction;
import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.config.ReportConfig;
import com.dbs.module.report.constant.Datatype;
import com.dbs.module.report.constant.ReportAccessGroupType;
import com.dbs.module.report.constant.ReportScheduleRefTable;
import com.dbs.module.report.util.ReportAccessGroupUtils;
import com.dbs.module.report.util.ReportScheduleUtils;
import com.dbs.module.report.util.VtspUserReportAccessGroupUtils;
import com.dbs.module.report.util.VtspUserReportCategoryUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.dao.RptAcsgMapper;
import com.dbs.vtsp.dao.RptCatgMapper;
import com.dbs.vtsp.dao.RptParmMapper;
import com.dbs.vtsp.dao.RptSetupAcsgMapper;
import com.dbs.vtsp.dao.RptSetupCatgMapper;
import com.dbs.vtsp.dao.RptSetupSchedMapper;
import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.dbs.vtsp.model.RptSetupCatgModel;
import com.dbs.vtsp.model.RptSetupCatgTmplModel;
import com.dbs.vtsp.model.RptSetupSchedModel;
import com.dbs.vtsp.service.ReportAccessGroupService;
import com.dbs.vtsp.service.ReportCategoryService;

@Service
public class ReportCategoryServiceImpl extends AbstractServiceImpl implements ReportCategoryService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RptAcsgMapper rptAcsgMapper;

	@Autowired
	private RptCatgMapper rptCatgMapper;

	@Autowired
	private RptParmMapper rptParmMapper;

	@Autowired
	private RptSetupCatgMapper rptSetupCatgMapper;

	@Autowired
	private RptSetupAcsgMapper rptSetupAcsgMapper;

	@Autowired
	private RptSetupSchedMapper rptSetupSchedMapper;

	@Autowired
	private ReportAccessGroupService reportAccessGroupService;

	@Autowired
	private ReportConfig reportConfig;

	// ----------------------------------------------------------------------------------------------------
	// report category
	// ----------------------------------------------------------------------------------------------------

	public List<ReportCategoryTemplateNameData> getReportNameList(ReportCategoryForm model) {
		List<ReportCategoryTemplateModel> list = rptCatgMapper.getUserReportNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportCategoryTemplateNameData::new).collect(Collectors.toList());
	}

	public ReportCategoryPropsData getReportCategoryProps(ReportCategory model) {
		ReportCategoryObjectModel item = rptCatgMapper.getUserReportCategoryTemplateObject(model);
		ReportCategoryPropsData data = null == item ? null : new ReportCategoryPropsData(item);
		if (null == data || CollectionUtils.isEmpty(data.getReportList())) {
			return data;
		}
		Map<String, List<ReportParamModel>> map = data.getReportList().stream()
				.flatMap(r -> r.getParameter().stream().filter(o -> Datatype.ARRAY.value().equals(o.getDatatype()) && MapUtils.isNotEmpty(o.getAttributes()) && StringUtils.isNotBlank(o.getAttributes().get("segment"))))
				.map(o -> o.getAttributes().get("segment"))
				.collect(Collectors.toMap(o -> o, o -> rptParmMapper.getParmValueList(o)));
		return MapUtils.isNotEmpty(map) ? data.put(map) : data;
	}

	public ReportCategoryObjectData getReportCategory(ReportCategory model) {
		ReportCategoryObjectModel item = rptCatgMapper.getUserReportCategoryObject(model);
		return null == item ? null : new ReportCategoryObjectData(item);
	}


	@Override
	public List<ReportCategoryListData> searchAll(ReportCategoryFormModel model) {
		List<ReportCategoryCountModel> list = rptCatgMapper.searchUserReportCategory(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportCategoryListData::new).collect(Collectors.toList());
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportCategory(ReportCategory model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupCatgModel bean = VtspUserReportCategoryUtils.parseCategory(model);
		Assert.isTrue(rptSetupCatgMapper.insert(bean) == 1, String.format("Fail to insert report category %s", getOrElse(model.getName(), "")));
		return createReportCategoryObject(VtspUserReportCategoryUtils.parse(model, bean));
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	public int createReportCategoryTemplate(ReportCategory model) {
		if (isTemplateFoundInGroup(model.getRptTmplId(), model.getId()))
			return 0;
		RptSetupCatgTmplModel bean = VtspUserReportCategoryUtils.parseGroupTemplate(model);
		Assert.isTrue(rptSetupCatgMapper.insertGroupTemplate(bean) == 1, String.format("Fail to insert report category (%s)", "Template Group"));
		return 1;
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportCategory(ReportCategory model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		RptSetupCatgModel bean = VtspUserReportCategoryUtils.parseCategory(model, rptSetupCatgMapper.getUserReportCategory(model.getId()));
		Assert.isTrue(rptSetupCatgMapper.update(bean) == 1, String.format("Fail to update report category %s", getOrElse(model.getName(), "")));
		updateReportCategoryTemplate(model.getId(), model.getRptTmplList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.MODIFY)
	@Transactional
	public int updateReportCategoryTemplate(ReportCategory model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		if (null != model.getRptTmplId()) {
			updateReportCategoryTemplate(model.getRptCatgList(), model.getRptTmplId());
			return model.getRptTmplId();
		}
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		updateReportCategoryTemplate(model.getId(), model.getRptTmplList());
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportCategory(ReportCategory model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), AssertResponseCode.NOT_NULL_ID.getMsg());
		Assert.isTrue(deleteReportCategoryObjectFull(model.getId()) > 0, String.format("Fail to delete report category %s", getOrElse(model.getName(), "")));
		return model.getId();
	}

	@OperationLog(operation = OperationLogAction.DELETE)
	@Transactional
	public int deleteReportCategoryTemplate(ReportCategory model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.notNull(model.getId(), "Set cannot be null");
		Assert.notNull(model.getRptTmplId(), "Report cannot be null");
		return rptSetupCatgMapper.deleteGroupTemplate(VtspUserReportCategoryUtils.parseGroupTemplate(model));
	}

	// ----------------------------------------------------------------------------------------------------
	// report access group
	// ----------------------------------------------------------------------------------------------------

	public List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model) {
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByCategory(model.getRptCatgId());
		Integer id = CollectionUtils.isEmpty(list) ? null : list.get(0).getId();
		if (null == id)
			return new ArrayList<>();
		ReportAccessGroupObjectModel item = rptAcsgMapper.getUserReportAccessGroupObject(ReportAccessGroupUtils.parse(id));
		return null == item ? new ArrayList<>() : new ReportAccessGroupObjectData(item).getRoleList();
	}

	@OperationLog(operation = OperationLogAction.ADD)
	@Transactional
	private int createReportAccessGroup(ReportAccessGroup model) {
		Assert.notNull(model, AssertResponseCode.NOT_NULL_DATA.getMsg());
		Assert.isNull(model.getId(), AssertResponseCode.EMPTY_ID.getMsg());
		RptSetupAcsgModel bean = VtspUserReportAccessGroupUtils.parse(model, ReportAccessGroupType.S);
		Assert.isTrue(rptSetupAcsgMapper.insert(bean) == 1, String.format("Fail to insert report category %s", getOrElse(model.getName(), "")));
		return bean.getId();
	}

	// ----------------------------------------------------------------------------------------------------
	// report schedule
	// ----------------------------------------------------------------------------------------------------

	public List<ReportScheduleData> getReportScheduleList(ReportSchedule model) {
		List<RptSetupSchedModel> list = rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(model));
		return null == list ? new ArrayList<>() : list.stream().map(ReportScheduleData::new).collect(Collectors.toList());
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	private static final String ERR_INSERT_FORMAT = "Fail to insert report category (%s)";

	protected int createReportCategoryObject(ReportCategoryObjectModel bean) {
		String[] defaultAccessGroupNames = VtspUserReportAccessGroupUtils.getUserReportAccessGroupName(reportConfig.getDefaultUserReportAccessGroupName(), bean.getId()).split("\\s*,\\s*");
		Assert.isTrue(insertReportCategoryAccessGroup(defaultAccessGroupNames[0], bean.getId(), true) > 0, String.format(ERR_INSERT_FORMAT, "Access Group"));
		if (defaultAccessGroupNames.length > 1)
			Stream.of(Arrays.copyOfRange(defaultAccessGroupNames, 1, defaultAccessGroupNames.length)).forEach(name -> insertReportCategoryAccessGroup(name, bean.getId(), false));
		return bean.getId();
	}

	protected int deleteReportCategoryObjectFull(Integer id) {
		List<ReportAccessGroupObjectModel> acsgs = rptAcsgMapper.getUserReportAccessGroupIdByType(ReportAccessGroupUtils.parse(id, ReportAccessGroupType.S));
		if (CollectionUtils.isNotEmpty(acsgs)) {
			acsgs.stream().forEach(item -> reportAccessGroupService.deleteReportAccessGroup(ReportAccessGroupUtils.parse(item.getId())));
		}
		rptSetupAcsgMapper.deleteGroupCategoryByCategory(id);
		List<Integer> scheds = rptSetupSchedMapper.getUserReportScheduleList(ReportScheduleUtils.parse(ReportScheduleRefTable.RPT_SETUP_CATG.name(), id)).stream().map(RptSetupSchedModel::getId).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(scheds)) {
			logger.info(String.format("There are %d rows to be deleted which affect schedule jobs %s", scheds.size(), FortifyStrategyUtils.toLogString(StringUtils.join(scheds, ", "))));
		}
		rptSetupSchedMapper.deleteByCategory(id);
		rptSetupCatgMapper.deleteGroupTemplateByCategory(id);
		rptSetupCatgMapper.delete(id);
		return id;
	}

	protected boolean isTemplateFoundInGroup(Integer rptTmplId, Integer id) {
		return rptSetupCatgMapper.getUserReportCategoryTemplateCount(VtspUserReportCategoryUtils.parseGroupTemplate(rptTmplId, null, id)) > 0;
	}

	protected void updateReportCategoryTemplate(Integer id, List<Integer> groupList) {
		// if reportTemplateGroupList is null, then update reportCategory only and no update to child reportTemplateGroup
		if (null == groupList)
			return;

		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupCatgMapper.getUserReportCategoryTemplateList(id).stream().map(RptSetupCatgTmplModel::getRptTmplId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupCatgMapper.deleteGroupTemplate(VtspUserReportCategoryUtils.parseGroupTemplate(o, null, id)));
			Map<Integer, Integer> groupMap = IntStream.range(0, groupList.size()).boxed().collect(Collectors.toMap(groupList::get, o -> o + 1));
			groupMap.entrySet().stream().filter(o -> list.contains(o.getKey())).forEach(o -> rptSetupCatgMapper.updateGroupTemplateDispSeq(VtspUserReportCategoryUtils.parseGroupTemplate(o.getKey(), o.getValue(), id)));
			groupMap.entrySet().stream().filter(o -> !list.contains(o.getKey())).forEach(o -> rptSetupCatgMapper.insertGroupTemplate(VtspUserReportCategoryUtils.parseGroupTemplate(o.getKey(), o.getValue(), id)));
		} else if (isTemplateFoundInGroup(null, id)) {
			rptSetupCatgMapper.deleteGroupTemplateByCategory(id);
		}
	}

	protected int insertReportCategoryAccessGroup(String name, Integer rptCatgId, boolean mandatory) {
		String categoryName = VtspUserReportAccessGroupUtils.getDefaultUserReportAccessGroupName(name);
		boolean isAccessGroupPreset = VtspUserReportAccessGroupUtils.isPresetUserReportAccessGroupName(categoryName);
		List<RptSetupAcsgModel> list = rptSetupAcsgMapper.getUserReportAccessGroupListByName(categoryName);
		boolean isAccessGroupFound = !(CollectionUtils.isEmpty(list) || null == list.get(0));
		Integer id = isAccessGroupFound ? list.get(0).getId() : (isAccessGroupPreset ? createReportAccessGroup(ReportAccessGroupUtils.getPublicInstance(categoryName)) : null);
		if (null == id && rptSetupAcsgMapper.getUserReportAccessGroupRoleCodeCount(name) > 0)
			id = createReportAccessGroup(ReportAccessGroupUtils.getPublicInstance(name));
		Assert.isTrue(!(mandatory && null == id), "AccessGroup cannot be null");
		if (null == id || id < 1)
			return 0;
		rptSetupAcsgMapper.insertGroupCategory(VtspUserReportAccessGroupUtils.parseGroupCategory(rptCatgId, id));
		return id;
	}

	protected void updateReportCategoryTemplate(List<Integer> groupList, Integer id) {
		if (CollectionUtils.isNotEmpty(groupList)) {
			List<Integer> list = rptSetupCatgMapper.getUserReportCategoryListByTemplate(id).stream().map(RptSetupCatgModel::getId).collect(Collectors.toList());
			list.stream().filter(o -> !groupList.contains(o)).forEach(o -> rptSetupCatgMapper.deleteGroupTemplate(VtspUserReportCategoryUtils.parseGroupTemplate(id, null, o)));
			groupList.stream().filter(o -> !list.contains(o)).forEach(o -> {
				Integer dispSeq = rptSetupCatgMapper.getUserReportCategoryTemplateDispSeqNextVal(o);
				rptSetupCatgMapper.insertGroupTemplate(VtspUserReportCategoryUtils.parseGroupTemplate(id, dispSeq, o));
			});
		} else if (isTemplateFoundInGroup(id, null)) {
			rptSetupCatgMapper.deleteGroupTemplateByTemplate(id);
		}
	}
}
