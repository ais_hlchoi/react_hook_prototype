package com.dbs.vtsp.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoleAccessModel extends AbstractAuditModel{
	private String uri;
}
