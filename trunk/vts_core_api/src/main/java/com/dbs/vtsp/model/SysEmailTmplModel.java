package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SysEmailTmplModel extends AbstractAuditModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String refTable;

	@JsonInclude
	private Integer refId;

	@JsonInclude
	private String name;

	@JsonInclude
	private String subject;

	@JsonInclude
	private String recipient;

	@JsonInclude
	private String recipientCc;

	@JsonInclude
	private String recipientBcc;

	@JsonInclude
	private String content;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;


}
