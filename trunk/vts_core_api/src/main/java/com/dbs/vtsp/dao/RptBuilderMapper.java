package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.ReportForm;
import com.dbs.module.report.model.ReportBuilderFileModel;
import com.dbs.module.report.model.ReportBuilderTableModel;

public interface RptBuilderMapper {

	List<ReportBuilderFileModel> getFileTypeList(ReportForm model);

	List<ReportBuilderFileModel> getTableNameList(ReportForm model);

	List<ReportBuilderTableModel> getColumnNameList(ReportForm model);
}
