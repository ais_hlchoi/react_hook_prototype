package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSrcFileModel extends AbstractAuditModel {


	@JsonInclude
	private Integer id;

	@JsonInclude
	private String datasource;

	@JsonInclude
	private String fileNameStart;

	@JsonInclude
	private String fileDesc;

	@JsonInclude
	private String filePattRegex;

	@JsonInclude
	private String tableName;
}
