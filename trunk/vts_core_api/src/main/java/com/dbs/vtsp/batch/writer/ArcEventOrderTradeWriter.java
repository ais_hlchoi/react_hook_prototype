package com.dbs.vtsp.batch.writer;

import java.io.FileWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.dbs.util.Assert;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.vtsp.batch.model.ArcEventOrderTradeModel;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

public class ArcEventOrderTradeWriter<T> implements ItemWriter<T> {

	private static final Logger logger = LoggerFactory.getLogger(ArcEventOrderTradeWriter.class);

	private String filePath;
	private String requestId;
	private String extension;
	private String header;
	private String column;

	public ArcEventOrderTradeWriter(String filePath, String requestId, String extension) {
		Assert.notBlank(filePath, "FilePath cannot be null");
		this.filePath = filePath;
		this.requestId = requestId;
		this.extension = StringUtils.isNotBlank(extension) ? ".txt" : extension;
	}

	public ArcEventOrderTradeWriter<T> put(String header, String column) {
		this.header = header;
		this.column = column;
		return this;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void write(List<? extends T> items) throws Exception {
		if (CollectionUtils.isEmpty(items))
			return;

		Assert.notBlank(header, "Header cannot be null");
		Assert.notBlank(column, "Column cannot be null");
		try (
				Writer writer = new FileWriter(filePath + requestId + extension);
			) {
			ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
			mappingStrategy.setType(ArcEventOrderTradeModel.class);
			mappingStrategy.setColumnMapping(column.split("\\s*,\\s*"));
			writer.write(header);
			writer.write("\n");
			StatefulBeanToCsv<ArcEventOrderTradeModel> sbc = new StatefulBeanToCsvBuilder(writer).withMappingStrategy(mappingStrategy).withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withSeparator(CSVWriter.DEFAULT_SEPARATOR).build();
			sbc.write((List<ArcEventOrderTradeModel>) items);
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}
	}
}
