package com.dbs.vtsp.service.impl;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public abstract class AbstractServiceImpl {

	protected <T> T getOrElse(T value, T valueIfNull) {
		if (value instanceof String)
			return StringUtils.isBlank((String) value) ? valueIfNull : value;

		return null == value ? valueIfNull : value;
	}

	protected enum AssertResponseCode {

		/**
		 * check if not null
		 */
		  NOT_NULL("%s cannot be null", true)
		, NOT_NULL_DATA("Data")
		, NOT_NULL_ID("Id")
		/**
		 * check if not empty
		 */
		, NOT_EMPTY("%s cannot be empty", true)
		/**
		 * check if empty
		 */
		, EMPTY("%s must be empty", true)
		, EMPTY_ID("Id")
		/**
		 * check if exists
		 */
		, EXISTS("%s cannot be repeated", true)
		;

		private String value;
		private boolean group = false; 

		private AssertResponseCode(String value) {
			this.value = value;
			this.group = false;
		}

		private AssertResponseCode(String value, boolean group) {
			this.value = value;
			this.group = group;
		}

		public String value(String valueIfNull) {
			return StringUtils.isNotBlank(valueIfNull) ? valueIfNull : value();
		}

		public String value() {
			return value;
		}

		public boolean group() {
			return group;
		}

		public String getMsg(String msg) {
			String text = null == msg ? value() : msg;
			AssertResponseCode code = Stream.of(AssertResponseCode.values()).filter(AssertResponseCode::group).filter(o -> name().startsWith(o.name())).findFirst().orElse(null);
			return null == code ? text : String.format(code.value, value(text));
		}

		public String getMsg() {
			return getMsg(null);
		}
	}
}
