package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dbs.module.report.ReportForm;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.data.ReportBuilderColumnData;
import com.dbs.module.report.data.ReportBuilderFileData;
import com.dbs.module.report.data.ReportBuilderTableData;
import com.dbs.module.report.model.ReportBuilderColumnModel;
import com.dbs.module.report.model.ReportBuilderFileModel;
import com.dbs.module.report.model.ReportBuilderTableModel;
import com.dbs.module.report.util.DynQuerySqlUtils;
import com.dbs.module.report.util.ReportFormUtils;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.vtsp.dao.InformationSchemaMapper;
import com.dbs.vtsp.dao.RptBuilderMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.service.ReportBuilderService;

@Service
public class ReportBuilderServiceImpl implements ReportBuilderService {

	@Autowired
	private RptBuilderMapper rptBuilderMapper;

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private InformationSchemaMapper informationSchemaMapper;

	@Value("${datasource.tableSchema}")
	private String tableSchema;

	public List<ReportBuilderFileData> getFileTypeList(ReportForm model) {
		ReportForm filter = null;
		List<ReportBuilderFileModel> list = rptBuilderMapper.getFileTypeList(filter);
		return null == list ? new ArrayList<>() : list.stream().map(ReportBuilderFileData::new).collect(Collectors.toList());
	}

	public List<ReportBuilderTableData<ReportBuilderTableModel>> getTableNameList(ReportForm model) {
		ReportForm filter = ReportFormUtils.parse(model.getSrcName(), model.getTableName(), model.getPage(), model.getPageSize());
		List<ReportBuilderFileModel> list = rptBuilderMapper.getTableNameList(filter);
		return null == list ? new ArrayList<>() : list.stream().map(ReportBuilderTableData::new).collect(Collectors.toList());
	}

	public List<ReportBuilderColumnData<ReportBuilderColumnModel>> getColumnNameList(ReportForm model) {
		ReportForm filter = ReportFormUtils.parse(model.getSrcName(), model.getTableName(), model.getColumnName(), model.getPage(), model.getPageSize());
		List<ReportBuilderTableModel> list = rptBuilderMapper.getColumnNameList(filter);
		return null == list ? new ArrayList<>() : list.stream().map(ReportBuilderColumnData::new).collect(Collectors.toList());
	}

	public String getQuerySql(ReportTemplate model) {
		String sql = DynQuerySqlUtils.render(model, srcFileMapper, informationSchemaMapper, tableSchema);
		return StringUtils.isBlank(sql) || null == model.getOption() ? sql : ReportParamUtils.parse(sql, model.getOption().getParameter());
	}
}
