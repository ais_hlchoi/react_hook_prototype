package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.model.ReportParamModel;

public interface RptParmMapper {

	List<ReportParamModel> getParmValueList(String segment);
}
