package com.dbs.vtsp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.ReportAccessGroupForm;
import com.dbs.module.report.SavedSearchForm;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.data.ReportAccessGroupRoleNameData;
import com.dbs.module.report.data.ReportCategoryListData;
import com.dbs.module.report.data.SavedSearchAccessGroupNameData;
import com.dbs.module.report.data.SavedSearchListData;
import com.dbs.module.report.model.ReportAccessGroupCountModel;
import com.dbs.module.report.model.ReportAccessGroupRoleModel;
import com.dbs.module.report.model.ReportCategoryCountModel;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.module.report.util.ReportCategoryFormUtils;
import com.dbs.module.report.util.ReportTemplateFormUtils;
import com.dbs.vtsp.dao.RptAcsgMapper;
import com.dbs.vtsp.dao.RptCatgMapper;
import com.dbs.vtsp.dao.RptTmplMapper;
import com.dbs.vtsp.service.SavedSearchService;

@Service
public class SavedSearchServiceImpl extends AbstractServiceImpl implements SavedSearchService {

	@Autowired
	private RptAcsgMapper rptAcsgMapper;

	@Autowired
	private RptCatgMapper rptCatgMapper;

	@Autowired
	private RptTmplMapper rptTmplMapper;

	public List<ReportTemplatePrivilegeModel> searchReportTemplate(SavedSearchForm model) {
		List<ReportTemplatePrivilegeModel> list = rptTmplMapper.searchUserReportTemplate(ReportTemplateFormUtils.parse(model));
		return null == list ? new ArrayList<>() : list;
	}

	public List<SavedSearchListData> searchReportTemplateAll(SavedSearchForm model) {
		List<ReportTemplatePrivilegeModel> list = rptTmplMapper.searchUserReportTemplate(ReportTemplateFormUtils.parse(model));
		return null == list ? new ArrayList<>() : list.stream().map(SavedSearchListData::new).collect(Collectors.toList());
	}

	public List<ReportCategoryCountModel> searchReportCategory(SavedSearchForm model) {
		List<ReportCategoryCountModel> list = rptCatgMapper.searchUserReportCategory(ReportCategoryFormUtils.parse(model));
		return null == list ? new ArrayList<>() : list;
	};

	public List<ReportCategoryListData> searchReportCategoryAll(SavedSearchForm model) {
		List<ReportCategoryCountModel> list = rptCatgMapper.searchUserReportCategory(ReportCategoryFormUtils.parse(model));
		return null == list ? new ArrayList<>() : list.stream().map(ReportCategoryListData::new).collect(Collectors.toList());
	}

	public List<SavedSearchAccessGroupNameData> getReportAccessGroupNameList(ReportAccessGroupForm model) {
		List<ReportAccessGroupCountModel> list = rptAcsgMapper.getUserReportAccessGroupList(model);
		return null == list ? new ArrayList<>() : list.stream().filter(o -> Indicator.checkIfTrue(o.getUserReportAccessGroup().getActiveInd())).map(SavedSearchAccessGroupNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupRoleNameData> getReportAccessGroupRoleList(ReportAccessGroup model) {
		List<ReportAccessGroupRoleModel> list = rptAcsgMapper.getRoleNameList(model);
		return null == list ? new ArrayList<>() : list.stream().map(ReportAccessGroupRoleNameData::new).collect(Collectors.toList());
	}
}
