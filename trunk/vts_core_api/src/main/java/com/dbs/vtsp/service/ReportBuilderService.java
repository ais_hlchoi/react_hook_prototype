package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.report.ReportForm;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.data.ReportBuilderColumnData;
import com.dbs.module.report.data.ReportBuilderFileData;
import com.dbs.module.report.data.ReportBuilderTableData;
import com.dbs.module.report.model.ReportBuilderColumnModel;
import com.dbs.module.report.model.ReportBuilderTableModel;

public interface ReportBuilderService {

	List<ReportBuilderFileData> getFileTypeList(ReportForm model);

	List<ReportBuilderTableData<ReportBuilderTableModel>> getTableNameList(ReportForm model);

	List<ReportBuilderColumnData<ReportBuilderColumnModel>> getColumnNameList(ReportForm model);

	String getQuerySql(ReportTemplate model);
}
