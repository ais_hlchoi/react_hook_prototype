package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.ReportScheduleForm;
import com.dbs.vtsp.model.RptSetupSchedModel;

public interface RptSetupSchedMapper {

	List<RptSetupSchedModel> searchUserReportSchedule(ReportScheduleForm model);

	List<RptSetupSchedModel> getUserReportScheduleList(ReportSchedule model);

	RptSetupSchedModel getUserReportSchedule(Integer id);

	int getReportScheduleDispSeqNextVal(ReportSchedule model);

	int insert(RptSetupSchedModel model);

	int delete(Integer id);

	int deleteByTemplate(Integer id);

	int deleteByCategory(Integer id);

	int update(RptSetupSchedModel model);

	int updateDispSeq(RptSetupSchedModel model);

	int updateDispSeqRecursive(RptSetupSchedModel model);

	int updateLastExecution(RptSetupSchedModel model);

	int updateNextExecution(RptSetupSchedModel model);
}
