package com.dbs.vtsp.service;

import com.dbs.module.report.*;
import com.dbs.module.report.data.*;
import com.dbs.vtsp.model.SysEmailTmplModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysEmailTmplService {

	SysEmailTmplModel getSysEmailTmpl(SysEmailTmplModel sysEmailTmplModel);

	SysEmailTmplModel getSysEmailTmpl(Integer refId ,String refTable);

	int insert(SysEmailTmplModel model);

	int delete(Integer refId , String refTable);
	int delete(SysEmailTmplModel sysEmailTmplModel);

	int update(SysEmailTmplModel model);
	int updateActiveInd(SysEmailTmplModel model,boolean active);
}
