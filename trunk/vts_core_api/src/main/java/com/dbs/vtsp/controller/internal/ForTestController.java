package com.dbs.vtsp.controller.internal;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.module.report.model.SavedSearchFormModel;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.LocalHostUtils;
import com.dbs.vtsp.service.SavedSearchService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/internal/t")
public class ForTestController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String MSG_START = "%s -> start";
	
	@Autowired
	private SavedSearchService savedSearchService;

	@RequestMapping("/get")
	public String update() {
		return "hello world";
	}

	@RequestMapping("/host")
	public RestEntity host() {
		logger.info(String.format(MSG_START, ClassPathUtils.getName()));
		return RestEntity.success(LocalHostUtils.getHostConfig());
	}

	@RequestMapping(value = "/search")
	public RestEntity search() {
		SavedSearchFormModel model = new SavedSearchFormModel();
		model.setPage(1);
		model.setPageSize(10);
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportTemplatePrivilegeModel> list = savedSearchService.searchReportTemplate(model);
		return RestEntity.success(new PageInfo<>(list));
	}
}
