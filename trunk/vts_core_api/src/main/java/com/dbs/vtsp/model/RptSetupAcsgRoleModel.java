package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupAcsgRoleModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptAcsgId;

	@JsonInclude
	private Integer roleId;
}
