package com.dbs.vtsp.service;

import java.util.List;

import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.ReportDownloadForm;
import com.dbs.module.download.data.ReportDownloadData;
import com.dbs.module.download.model.ReportDownloadFormModel;
import com.dbs.vtsp.model.RptFileDnldHistoryModel;
import com.dbs.vtsp.model.RptFileDnldSummaryModel;

public interface ReportDownloadService {

	List<ReportDownloadData> search(ReportDownloadForm model);

	List<RptFileDnldSummaryModel> searchDownloadReport(ReportDownloadForm model);

	void outputFile(ReportDownload model);

	List<RptFileDnldHistoryModel> searchDownloadReportHistory(ReportDownloadFormModel model);
}
