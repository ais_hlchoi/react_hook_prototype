package com.dbs.vtsp.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.report.data.ReportBuilderColumnData;
import com.dbs.module.report.data.ReportBuilderFileData;
import com.dbs.module.report.data.ReportBuilderTableData;
import com.dbs.module.report.model.ReportBuilderColumnModel;
import com.dbs.module.report.model.ReportBuilderTableModel;
import com.dbs.module.report.model.ReportFormModel;
import com.dbs.vtsp.service.ReportBuilderService;

@RestController
@CrossOrigin
@RequestMapping("/api/reportBuilder")
@RoleAccess
public class ReportBuilderController {

	@Autowired
	private ReportBuilderService reportBuilderService;

	@RequestMapping(value = "/getFileTypeList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getFileTypeList(@RequestBody ReportFormModel model) {
		List<ReportBuilderFileData> list = reportBuilderService.getFileTypeList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getTableNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getTableNameList(@RequestBody ReportFormModel model) {
		List<ReportBuilderTableData<ReportBuilderTableModel>> list = reportBuilderService.getTableNameList(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/getColumnNameList", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public RestEntity getColumnNameList(@RequestBody ReportFormModel model) {
		List<ReportBuilderColumnData<ReportBuilderColumnModel>> list = reportBuilderService.getColumnNameList(model);
		return RestEntity.success(list);
	}
}
