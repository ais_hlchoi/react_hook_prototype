package com.dbs.vtsp.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.framework.aspect.annotation.RoleAccess;
import com.dbs.framework.shiro.entity.RestEntity;
import com.dbs.module.download.data.ReportDownloadData;
import com.dbs.module.download.model.ReportDownloadFormModel;
import com.dbs.module.download.model.ReportDownloadModel;
import com.dbs.vtsp.model.RptFileDnldHistoryModel;
import com.dbs.vtsp.model.RptFileDnldSummaryModel;
import com.dbs.vtsp.service.ReportDownloadService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@CrossOrigin
@RequestMapping("/api/reportDownload")
@RoleAccess
public class ReportDownloadController {

	@Autowired
	private ReportDownloadService reportDownloadService;

	private static final String JSON = MediaType.APPLICATION_JSON_VALUE;
	private static final String XML = MediaType.APPLICATION_XML_VALUE;
	private static final String PDF = MediaType.APPLICATION_PDF_VALUE;
	private static final String CSV = "text/csv";

	@RequestMapping("/search")
	@PreAuthorize("isAuthenticated()")
	public RestEntity search(@RequestBody ReportDownloadFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<ReportDownloadData> list = reportDownloadService.search(model);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchSchedulerDownloadReport")
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchDownloadReport(@RequestBody ReportDownloadFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<RptFileDnldSummaryModel> list = reportDownloadService.searchDownloadReport(model);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchSchedulerDownloadReportHistory")
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchDownloadReportHistory(@RequestBody ReportDownloadFormModel model) {
		PageHelper.startPage(model.getPage(), model.getPageSize());
		List<RptFileDnldHistoryModel> list = reportDownloadService.searchDownloadReportHistory(model);
		return RestEntity.success(new PageInfo<>(list));
	}

	@RequestMapping("/searchAll")
	@PreAuthorize("isAuthenticated()")
	public RestEntity searchAll(@RequestBody ReportDownloadFormModel model) {
		List<ReportDownloadData> list = reportDownloadService.search(model);
		return RestEntity.success(list);
	}

	@RequestMapping(value = "/exportXlsx", method = RequestMethod.POST, produces = { XML, JSON })
	@PreAuthorize("isAuthenticated()")
	public void exportXlsx(@RequestBody ReportDownloadModel model) {
		reportDownloadService.outputFile(model);
	}

	@RequestMapping(value = "/exportCsv", method = RequestMethod.POST, produces = { CSV })
	@PreAuthorize("isAuthenticated()")
	public void exportCsv(@RequestBody ReportDownloadModel model) {
		reportDownloadService.outputFile(model);
	}

	@RequestMapping(value = "/exportPdf", method = RequestMethod.POST, produces = { PDF })
	@PreAuthorize("isAuthenticated()")
	public void exportPdf(@RequestBody ReportDownloadModel model) {
		reportDownloadService.outputFile(model);
	}
}
