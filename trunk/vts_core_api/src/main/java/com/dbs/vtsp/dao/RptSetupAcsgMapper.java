package com.dbs.vtsp.dao;

import java.util.List;

import com.dbs.vtsp.model.RptSetupAcsgCatgModel;
import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.dbs.vtsp.model.RptSetupAcsgRoleModel;
import com.dbs.vtsp.model.RptSetupAcsgTmplModel;
import com.dbs.vtsp.model.RptSetupAcsgUserModel;

public interface RptSetupAcsgMapper {

	List<RptSetupAcsgModel> getUserReportAccessGroupListByName(String name);

	List<RptSetupAcsgModel> getUserReportAccessGroupListByTemplate(Integer id);

	List<RptSetupAcsgModel> getUserReportAccessGroupListByCategory(Integer id);

	List<RptSetupAcsgModel> getUserReportAccessGroupListByUser(String id);

	List<RptSetupAcsgModel> getUserReportAccessGroupListByRole(Integer id);

	RptSetupAcsgModel getUserReportAccessGroup(Integer id);

	List<RptSetupAcsgTmplModel> getUserReportAccessGroupTemplateList(Integer id);

	List<RptSetupAcsgCatgModel> getUserReportAccessGroupCategoryList(Integer id);

	List<RptSetupAcsgUserModel> getUserReportAccessGroupUserList(Integer id);

	List<RptSetupAcsgRoleModel> getUserReportAccessGroupRoleList(Integer id);

	int getUserReportAccessGroupTemplateCount(RptSetupAcsgTmplModel model);

	int getUserReportAccessGroupCategoryCount(RptSetupAcsgCatgModel model);

	int getUserReportAccessGroupUserCount(RptSetupAcsgUserModel model);

	int getUserReportAccessGroupRoleCount(RptSetupAcsgRoleModel model);

	int getUserReportAccessGroupRoleCodeCount(String name);

	int insert(RptSetupAcsgModel model);

	int insertGroupTemplate(RptSetupAcsgTmplModel model);

	int insertGroupCategory(RptSetupAcsgCatgModel model);

	int insertGroupUser(RptSetupAcsgUserModel model);

	int insertGroupRole(RptSetupAcsgRoleModel model);

	int delete(Integer id);

	int deleteGroupTemplate(RptSetupAcsgTmplModel model);

	int deleteGroupTemplateByTemplate(Integer id);

	int deleteGroupTemplateByAccessGroup(Integer id);

	int deleteGroupCategory(RptSetupAcsgCatgModel model);

	int deleteGroupCategoryByCategory(Integer id);

	int deleteGroupCategoryByAccessGroup(Integer id);

	int deleteGroupUser(RptSetupAcsgUserModel model);

	int deleteGroupUserByUser(String id);

	int deleteGroupUserByAccessGroup(Integer id);

	int deleteGroupRole(RptSetupAcsgRoleModel model);

	int deleteGroupRoleByRole(Integer id);

	int deleteGroupRoleByAccessGroup(Integer id);

	int update(RptSetupAcsgModel model);
}
