package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptSetupTmplTableModel extends AbstractAuditModel {

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer tableId;

	@JsonInclude
	private String tableLabel;
}
