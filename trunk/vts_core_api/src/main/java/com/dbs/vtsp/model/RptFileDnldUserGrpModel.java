package com.dbs.vtsp.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RptFileDnldUserGrpModel extends AbstractAuditModel {

	@JsonInclude
	private Integer fileDnldId;

	@JsonInclude
	private String userId;
}
