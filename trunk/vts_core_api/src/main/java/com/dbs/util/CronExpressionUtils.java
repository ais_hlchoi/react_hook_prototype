package com.dbs.util;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CronExpressionUtils {

	private CronExpressionUtils() {
		// for SonarQube scanning purpose
	}

	private static final String ONE = "?";
	private static final String ANY = "*";

	public static String getValidCronExpression(final String text, final boolean isSpringCronExpression) {
//		if (StringUtils.isBlank(text))
//			return null;
//
		String[] arr = text.trim().split("\\s+");
		int n = arr.length;
//
//		if (n < 5 || n > 7)
//			return null;
//
//		if (isSpringCronExpression)
//			return StringUtils.join(arr, " ") + (n == 5 ? (" " + ANY) : "");
//
//		switch (n) {
//		case 5:
//			return String.format("%s %s %s", StringUtils.join(arr, " "), text.indexOf(ONE) < 0 ? ONE : ANY, ANY);
//		case 6:
//			return String.format("%s %s", StringUtils.join(arr, " "), ANY);
//		case 7:
//			return StringUtils.join(arr, " ");
//		default:
//			return null;
//		}
		return String.format("%s %s","0",StringUtils.join(arr, " "));
	}

	public static boolean isValid(final String cron) {
		return CronExpression.isValidExpression(cron);
	}

	public static Date getNextValidTimeAfter(final String text) {
		return getNextValidTimeAfter(text, null);
	}

	public static Date getNextValidTimeAfter(String text, Date date) {
		String cron = getValidCronExpression(text, false);
		if (null == cron || !isValid(cron))
			return null;

		try {
			return new CronExpression(cron).getNextValidTimeAfter(null == date ? new Date() : date);
		} catch (ParseException e) {
			LoggerFactory.getLogger(CronExpressionUtils.class).info(FortifyStrategyUtils.toErrString(e));
		}
		return null;
	}

	public static String getNextValidTimeBetweenStartDateEndDate(String text, String startDateStr,String expiredDateStr) {


		Date checkDate = new Date();
		Date startDate =  new Date();
		Date expiredDate = new Date();
		try {
			startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDateStr);
			expiredDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(expiredDateStr);
		}
		catch (Exception e){
			return null;
		}
		return getNextValidTimeBetweenStartDateEndDate(text,startDate,expiredDate);
	}

	public static String getNextValidTimeBetweenStartDateEndDate(String text, Date startDate,Date expiredDate) {
		String cron = getValidCronExpression(text, false);
		if (null == cron || !isValid(cron))
			return null;


		Date checkDate = new Date();
		if(startDate.before(new Date())){
			if(expiredDate.before(new Date())){
				return null;
			}
			else{
				checkDate  = new Date();
			}
		}
		else{
			checkDate =  new Date();

		}
		try {
			Date nextExecuteDate =  new CronExpression(cron).getNextValidTimeAfter(checkDate);
			if(expiredDate.before(nextExecuteDate)){
				return null;
			}
			else{
				return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nextExecuteDate);
			}
		} catch (ParseException e) {
			LoggerFactory.getLogger(CronExpressionUtils.class).info(FortifyStrategyUtils.toErrString(e));
		}
		return null;
	}
}
