package com.dbs.util;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class TableNameUtils {

	private TableNameUtils() {
		// for SonarQube scanning purpose
	}

	public static String parse(String name) {
		return parse(name, Char.SPACE, Char.HYPHEN);
	}

	public static String parse(String name, Char...chars) {
		if (null == name)
			return "";

		if (ArrayUtils.isEmpty(chars))
			return name;

		return name.trim().replaceAll(Stream.of(chars).flatMap(c -> Stream.of(c.getRegex())).collect(Collectors.toSet()).stream().collect(Collectors.joining("|")), "_").toUpperCase();
	}

	public enum Char {
		SPACE("\\s+"),
		HYPHEN("-"),
		;
		private String[] regex;
		private Char(String... regex) {
			this.regex = regex;
		}
		public String[] getRegex() {
			return regex;
		}
		public String getRegexString() {
			return ArrayUtils.isEmpty(regex) ? "" : StringUtils.join(regex, "|");
		}
	}
}
