package com.dbs.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class FileUtil {

	private FileUtil() {
		// for SonarQube scanning purpose
	}

	private FileUtil(HttpServletResponse response) {
		this.response = response;
	}

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private HttpServletResponse response;

	private static final String ATTACH_FILENAME = "attachment; filename=";
	private static final String UTF_8 = "UTF-8";
	private static final String FILE_DATE_FORMAT = "_yyyyMMdd_HHmmss_SSS";

	private static final String ERR_DNLOAD_FORMAT = "Fail to download %s caused by %s";

	/**
	 * Excel Export
	 *
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadFileWithDate(final HttpServletResponse response, final XSSFWorkbook workbook, final String fileName) {
		new FileUtil(response).download(workbook, fileName, MediaType.MULTIPART_FORM_DATA_VALUE, FILE_DATE_FORMAT, false);
	}

	/**
	 * Excel Export
	 *
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadFileWithDate(final HttpServletResponse response, final SXSSFWorkbook workbook, final String fileName) {
		new FileUtil(response).download(workbook, fileName, MediaType.MULTIPART_FORM_DATA_VALUE, FILE_DATE_FORMAT, false);
	}

	/**
	 * Excel Export
	 *
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadFileUTF8(final HttpServletResponse response, final XSSFWorkbook workbook, final String fileName) {
		new FileUtil(response).download(workbook, fileName, MediaType.MULTIPART_FORM_DATA_VALUE, null, true);
	}

	/**
	 * File Export
	 *
	 * @param file
	 * @param fileName
	 * @param contentType
	 */
	public static void downloadFileUTF8(final HttpServletResponse response, File file, String fileName, String contentType) {
		new FileUtil(response).download(file, fileName, contentType);
	}

	public static void downloadFileUTF8Byte(final HttpServletResponse response, byte[] bytes, String fileName, String contentType) {
		new FileUtil(response).download(bytes, fileName, contentType);
	}
	/**
	 * PDF Export
	 *
	 * @param bytes
	 * @param fileName
	 */
	public static void downloadPdfFile(final HttpServletResponse response, byte[] bytes, String fileName) {
		new FileUtil(response).download(bytes, fileName, MediaType.APPLICATION_PDF_VALUE + ";charset=" + UTF_8);
	}

	/**
	 * Excel Export
	 *
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadXlsxFile(final HttpServletResponse response, XSSFWorkbook workbook, String fileName) {
		new FileUtil(response).download(workbook, fileName, MediaType.APPLICATION_OCTET_STREAM_VALUE, null, true);
	}

	/**
	 * Excel Export
	 *
	 * @param workbook
	 * @param fileName
	 */
	public static void downloadXlsFile(final HttpServletResponse response, HSSFWorkbook workbook, String fileName) {
		new FileUtil(response).download(workbook, fileName, MediaType.APPLICATION_OCTET_STREAM_VALUE, null, true);
	}

	public static void downloadZipFile(final HttpServletResponse response, File file, String fileName) {
		new FileUtil(response).downloadZip(file, fileName);
	}

	/**
	 * InputStream Export
	 *
	 * @param binary
	 * @param fileName
	 */
	public static void downloadFile(final HttpServletResponse response, BufferedInputStream bin, String fileName) {
		new FileUtil(response).download(bin, fileName);
	}

	/**
	 * Byte Export (write to file path)
	 *
	 * @param bytes
	 * @param filePath
	 */
	public static void writeFile(byte[] bytes, String filePath) {
		new FileUtil(null).write(bytes, filePath);
	}


	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected String getAttachmentFileName(String fileName, String dateFormat, boolean isFileNameUTF8) {
		try {
			String date = StringUtils.isNotBlank(dateFormat) ? new SimpleDateFormat(dateFormat).format(new Date()) : "";
			String name = FilenameUtils.getBaseName(fileName);
			String ext = FilenameUtils.getExtension(fileName);
			String charset = isFileNameUTF8 ? "*=" + UTF_8 + "''" : "";
			return String.format("%s%s%s.%s", charset, URLEncoder.encode(name, UTF_8), date, ext);
		} catch (Exception e) {
			logger.error(String.format("Fail to get file name %s date format %s caused by %s", fileName, dateFormat, FortifyStrategyUtils.toErrString(e)));
		}
		return fileName;
	}

	protected void download(Workbook workbook, String fileName, String contentType, String dateFormat, boolean isFileNameUTF8) {
		if (null == workbook || StringUtils.isBlank(fileName) || StringUtils.isBlank(contentType))
			return;

		response.setContentType(contentType);
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ATTACH_FILENAME + getAttachmentFileName(FortifyStrategyUtils.toPathString(fileName), dateFormat, isFileNameUTF8));
		response.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
		response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

		ServletOutputStream out = null;
		try {
			out = response.getOutputStream();
			workbook.write(out);
		} catch (IOException e) {
			logger.error(String.format(ERR_DNLOAD_FORMAT, FortifyStrategyUtils.toLogString("workbook", fileName, contentType), FortifyStrategyUtils.toErrString(e)));
		} finally {
			close(out);
		}
	}

	protected void download(File file, String fileName, String contentType) {
		if (null == file || StringUtils.isBlank(fileName) || StringUtils.isBlank(contentType))
			return;

		response.setContentType(contentType);
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ATTACH_FILENAME + getAttachmentFileName(FortifyStrategyUtils.toPathString(fileName), null, true));
		response.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
		response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

		ServletOutputStream out = null;
		try {
			out = response.getOutputStream();
			out.write(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			logger.error(String.format(ERR_DNLOAD_FORMAT, FortifyStrategyUtils.toLogString("file", fileName, contentType), FortifyStrategyUtils.toErrString(e)));
		} finally {
			close(out);
		}
	}

	protected void download(byte[] bytes, String fileName, String contentType) {
		if (null == bytes || bytes.length < 1 || StringUtils.isBlank(fileName) || StringUtils.isBlank(contentType))
			return;

		response.setContentType(contentType);
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ATTACH_FILENAME + getAttachmentFileName(FortifyStrategyUtils.toPathString(fileName), null, true));
		response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

		ServletOutputStream out = null;
		try {
			out = response.getOutputStream();
			out.write(bytes);
		} catch (IOException e) {
			logger.error(String.format(ERR_DNLOAD_FORMAT, FortifyStrategyUtils.toLogString("binary", fileName, contentType), FortifyStrategyUtils.toErrString(e)));
		} finally {
			close(out);
		}
	}

	protected void downloadZip(File file, String fileName) {
		if (null == file || StringUtils.isBlank(fileName))
			return;

		response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ATTACH_FILENAME + getAttachmentFileName(FortifyStrategyUtils.toPathString(fileName), null, true));
		response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

		try (
				BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
				FileInputStream in = new FileInputStream(file);
			) {
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				out.write(b, 0, i);
			}
			out.flush();
		} catch (IOException e) {
			logger.error(String.format(ERR_DNLOAD_FORMAT, FortifyStrategyUtils.toLogString("zip", fileName), FortifyStrategyUtils.toErrString(e)));
		}
	}

	protected void download(BufferedInputStream bin, String fileName) {
		if (null == bin || StringUtils.isBlank(fileName))
			return;

		response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ATTACH_FILENAME + getAttachmentFileName(FortifyStrategyUtils.toPathString(fileName), null, false));
		response.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
		response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

		ServletOutputStream out = null;
		try {
			out = response.getOutputStream();
			int size = 0;
			byte[] buf = new byte[1024];
			while ((size = bin.read(buf)) != -1) {
				out.write(buf, 0, size);
			}
		} catch (IOException e) {
			logger.error(String.format(ERR_DNLOAD_FORMAT, FortifyStrategyUtils.toLogString("stream", fileName), FortifyStrategyUtils.toErrString(e)));
		} finally {
			close(out);
			close(bin);
		}
	}

	protected void write(byte[] bytes, String filePath) {
		if (null == bytes || bytes.length < 1 || StringUtils.isBlank(filePath))
			return;

		File file = load(filePath);
		if (null == file) {
			logger.error(String.format("Fail to load file %s", FilenameUtils.getName(filePath)));
			return;
		}
		try (
				FileOutputStream out = new FileOutputStream(file);
			) {
			out.write(bytes);
		} catch (IOException e) {
			logger.error(String.format("Fail to write file %s caused by %s", FilenameUtils.getName(filePath), FortifyStrategyUtils.toErrString(e)));
		}
	}

	private File load(String filePath) {
		File file = new File(FilenameUtils.getFullPath(FortifyStrategyUtils.toPathString(filePath)));
		return file.exists() ? file : createFile(file);
	}

	private File createFile(File file) {
		try {
			if (!file.createNewFile())
				file = null;
		} catch (IOException e) {
			file = null;
		}
		return file;
	}

	private void close(OutputStream out) {
		try {
			if (out != null) {
				out.flush();
			}
		} catch (IOException e) {
			logger.error(String.format("Fail to flush output stream caused by %s", FortifyStrategyUtils.toErrString(e)));
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			logger.error(String.format("Fail to close output stream caused by %s", FortifyStrategyUtils.toErrString(e)));
		}
	}

	private void close(InputStream in) {
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			logger.error(String.format("Fail to close input stream caused by %s", FortifyStrategyUtils.toErrString(e)));
		}
	}
}
