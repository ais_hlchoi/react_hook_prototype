package com.dbs.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;

public class Assert extends org.springframework.util.Assert {

	// message

	public static void notBlank(@Nullable String text, String message) {
		if (StringUtils.isBlank(text)) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isFalse(boolean expression, String message) {
		if (expression) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void equals(@Nullable Object a, Object b, String message) {
		if ((a == null && b != null) || (a != null && !a.equals(b))) {
			throw new IllegalArgumentException(message);
		}
	}

	// message + list

	public static void isTrue(boolean expression, String message, List<String> list) {
		if (!expression && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void isFalse(boolean expression, String message, List<String> list) {
		if (expression && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void isNull(@Nullable Object object, String message, List<String> list) {
		if (object != null && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void notNull(@Nullable Object object, String message, List<String> list) {
		if (object == null && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	public static void notBlank(@Nullable String text, String message, List<String> list) {
		if (StringUtils.isBlank(text) && null != list && StringUtils.isNotBlank(message)) {
			list.add(message);
		}
	}

	// message + ResponseCode

	public static void isTrue(boolean expression, String message, ResponseCode code) {
		if (!expression && null != code && StringUtils.isNotBlank(getOrElse(message, code.getDesc()))) {
			throw new SysRuntimeException(code, message);
		}
	}

	public static void isFalse(boolean expression, String message, ResponseCode code) {
		if (expression && null != code && StringUtils.isNotBlank(getOrElse(message, code.getDesc()))) {
			throw new SysRuntimeException(code, message);
		}
	}

	public static void isNull(@Nullable Object object, String message, ResponseCode code) {
		if (object != null && null != code && StringUtils.isNotBlank(getOrElse(message, code.getDesc()))) {
			throw new SysRuntimeException(code, message);
		}
	}

	public static void notNull(@Nullable Object object, String message, ResponseCode code) {
		if (object == null && null != code && StringUtils.isNotBlank(getOrElse(message, code.getDesc()))) {
			throw new SysRuntimeException(code, message);
		}
	}

	public static void notBlank(@Nullable String text, String message, ResponseCode code) {
		if (StringUtils.isBlank(text) && null != code && StringUtils.isNotBlank(getOrElse(message, code.getDesc()))) {
			throw new SysRuntimeException(code, message);
		}
	}

	// ResponseCode

	public static void isTrue(boolean expression, ResponseCode code) {
		isTrue(expression, code.getDesc(), code);
	}

	public static void isFalse(boolean expression, ResponseCode code) {
		isFalse(expression, code.getDesc(), code);
	}

	public static void isNull(@Nullable Object object, ResponseCode code) {
		isNull(object, code.getDesc(), code);
	}

	public static void notNull(@Nullable Object object, ResponseCode code) {
		notNull(object, code.getDesc(), code);
	}

	public static void notBlank(@Nullable String text, ResponseCode code) {
		notBlank(text, code.getDesc(), code);
	}

	// utilities

	private static String getOrElse(String value, String valueIfNull) {
		return StringUtils.isBlank(value) ? valueIfNull : value;
	}
}
