package com.dbs.util;

import com.dbs.module.model.PageProps;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections4.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class PageInfoUtils {

	private PageInfoUtils() {
		// for SonarQube scanning purpose
	}

	public static <T, R> PageProps<R> parse(PageInfo<T> pageInfo, Class<R> cls) {
		PageProps<R> props = PageProps.of(pageInfo.getStartRow(), pageInfo.getEndRow(), pageInfo.getTotal(), pageInfo.getPages());
		if (CollectionUtils.isEmpty(pageInfo.getList()))
			return props.put(new ArrayList<R>());

		return props.put(pageInfo.getList().stream().map(o -> PageInfoUtils.castData(o, cls)).collect(Collectors.toList()));
	}

	private static <T, R> R castData(T model, Class<R> cls) {
		try {
			return cls.getDeclaredConstructor(model.getClass()).newInstance(model);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// write log if necessary
		}
		return null;
	}
}
