package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.UserForm;

public interface ReportCategory extends UserForm {

	String getName();

	String getDescription();

	String getGenerateOpt();

	String getActiveInd();

	Integer getId();

	Integer getRptTmplId();

	Integer getDispSeq();

	List<Integer> getRptCatgList();

	List<Integer> getRptTmplList();

	List<String> getRptNameList();
}
