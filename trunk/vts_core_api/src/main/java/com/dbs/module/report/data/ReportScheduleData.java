package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportScheduleRefTable;
import com.dbs.module.util.AbstractModuleData;
import com.dbs.vtsp.model.RptSetupSchedModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportScheduleData extends AbstractModuleData {

	private RptSetupSchedModel data;

	public ReportScheduleData(RptSetupSchedModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected RptSetupSchedModel getData() {
		return data;
	}

	protected void setData(RptSetupSchedModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getRefTable() {
		switch (ReportScheduleRefTable.getOrElse(getData().getRefTable())) {
		case RPT_SETUP_CATG:
		case RPT_SETUP_TMPL:
			return null;
		default:
			return getData().getRefTable();
		}
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getRefId() {
		switch (ReportScheduleRefTable.getOrElse(getData().getRefTable())) {
		case RPT_SETUP_CATG:
		case RPT_SETUP_TMPL:
			return null;
		default:
			return getData().getRefId();
		}
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getRptTmplId() {
		switch (ReportScheduleRefTable.getOrElse(getData().getRefTable())) {
		case RPT_SETUP_TMPL:
			return getData().getRefId();
		default:
			return null;
		}
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getRptCatgId() {
		switch (ReportScheduleRefTable.getOrElse(getData().getRefTable())) {
		case RPT_SETUP_CATG:
			return getData().getRefId();
		default:
			return null;
		}
	}

	public String getName() {
		return getData().getName();
	}

	public String getCronExpression() {
		return getData().getCronExpression();
	}

	public String getExportOption() {
		return getData().getExportOption();
	}

	public String getLastExecutionTime() {
		return formatDatetime(getData().getLastExecutionTime());
	}

	public String getNextExecutionTime() {
		return getData().getNextExecutionTime();
	}

	public Integer getDispSeq() {
		return getData().getDispSeq();
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return Indicator.toString(Indicator.checkIfTrue(getData().getActiveInd()));
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return formatTimeNano(getData().getLastUpdatedDate());
	}

	@JsonInclude(Include.NON_NULL)
	public String getExpiredDate() {
		return getData().getExpiredDate();
	}

	@JsonInclude(Include.NON_NULL)
	public String getStartDate() {
		return getData().getStartDate();
	}


	@JsonInclude(Include.NON_NULL)
	public String getEmailInd() {
		return getData().getEmailInd();
	}

	public String getSubject() {
		return  getData().getSysEmailTmplModel()!=null ? getData().getSysEmailTmplModel().getSubject() : null;
	}


	public String getRecipient() {
		return getData().getSysEmailTmplModel()!=null ?  getData().getSysEmailTmplModel().getRecipient() : null;
	}


	public String getRecipientCc() {
		return  getData().getSysEmailTmplModel()!=null ?  getData().getSysEmailTmplModel().getRecipientCc() : null;
	}


	public String getRecipientBcc() {
		return  getData().getSysEmailTmplModel()!=null ? getData().getSysEmailTmplModel().getRecipientBcc() : null;
	}


	public String getContent() {
		return getData().getSysEmailTmplModel()!=null ? getData().getSysEmailTmplModel().getContent() : null;
	}




}
