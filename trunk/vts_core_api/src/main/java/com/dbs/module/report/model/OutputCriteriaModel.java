package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsCriteria;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputCriteriaModel implements ReportPropsCriteria {

	@JsonInclude
	private Integer seq;

	@JsonInclude
	private OutputTableModel table;

	@JsonInclude
	private OutputColumnModel column;

	@JsonInclude
	private OutputComparatorModel comparator;

	@JsonInclude(Include.NON_NULL)
	private String value;

	@JsonInclude(Include.NON_NULL)
	private String mandatory;
}
