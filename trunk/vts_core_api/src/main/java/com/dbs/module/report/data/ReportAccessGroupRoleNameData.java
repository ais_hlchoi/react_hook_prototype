package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportAccessGroupRoleModel;

public class ReportAccessGroupRoleNameData {

	private ReportAccessGroupRoleModel data;

	public ReportAccessGroupRoleNameData(ReportAccessGroupRoleModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupRoleModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupRoleModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}
}
