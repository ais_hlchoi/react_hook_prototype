package com.dbs.module.report.model;

import java.util.List;

import com.dbs.vtsp.model.RptSetupCatgModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryCountModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer rptTmplCnt;

	@JsonInclude
	private RptSetupCatgModel userReportCategory;

	@JsonInclude
	private List<ReportAccessGroupRoleModel> roleList;
}
