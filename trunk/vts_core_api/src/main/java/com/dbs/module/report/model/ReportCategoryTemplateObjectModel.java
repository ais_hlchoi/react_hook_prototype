package com.dbs.module.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryTemplateObjectModel {

	@JsonInclude
	private Integer id;
}
