package com.dbs.module.export.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.dbs.module.download.util.FileDownloadUtils;
import com.dbs.util.FilenameUtils;

@Component
public class ReportFilePathUtils {

	private ReportFilePathUtils() {
		// for SonarQube scanning purpose
	}

	public static String replaceFileOutputPath(final String folderOutput, final String path) {
		String a = FilenameUtils.toPathString(path);
		String b = FilenameUtils.toPathString(folderOutput);
		return FilenameUtils.toPathString(StringUtils.replace(a, b, FileDownloadUtils.FOLDER_OUTPUT));
	}

	public static String restoreFileOutputPath(final String folderOutput, final String path) {
		String a = FilenameUtils.toPathString(path);
		String b = FilenameUtils.toPathString(folderOutput);
		return FilenameUtils.toPathString(StringUtils.replace(a, FileDownloadUtils.FOLDER_OUTPUT, b));
	}
}
