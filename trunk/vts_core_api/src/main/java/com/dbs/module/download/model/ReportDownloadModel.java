package com.dbs.module.download.model;

import java.util.Date;

import com.dbs.module.download.ReportDownload;
import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportDownloadModel extends AbstractUserModel implements ReportDownload {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	// crate
	@JsonInclude
	private String reportName;

	@JsonInclude
	private Date reportDate;

	@JsonInclude
	private String reportType;

	@JsonInclude
	private Integer parentId;

	@JsonInclude
	private String userName;

	@JsonInclude
	private String fileExtension;

	@JsonInclude
	private String status;

	@JsonInclude
	private String sql;

	@JsonInclude
	private String layout;

	@JsonInclude
	private String refType;

	@JsonInclude
	private Integer refId;

	@JsonInclude
	private Integer refSched;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer rptCatgId;

	// update
	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer rowCount;

	@JsonInclude
	private Integer rowProcess;

	@JsonInclude
	private String message;

	@JsonInclude
	private String fileName;

	@JsonInclude
	private String filePath;

	@JsonInclude
	private Long fileSize;

	// for report category use [ suggest to slim this object ]
	@JsonInclude
	private ReportTemplate model;
}
