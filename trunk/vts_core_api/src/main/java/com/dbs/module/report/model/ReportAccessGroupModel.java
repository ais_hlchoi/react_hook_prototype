package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.util.ReportAccessGroupUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupModel extends AbstractUserModel implements ReportAccessGroup {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private String type;

	@JsonInclude
	private String activeInd;

	// detail

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer rptCatgId;

	@JsonInclude
	private String userId;

	@JsonInclude
	private Integer roleId;

	// list

	@JsonInclude
	private List<Integer> rptAcsgList;

	@JsonInclude
	private List<Integer> rptTmplList;

	@JsonInclude
	private List<Integer> rptCatgList;

	@JsonInclude
	private List<String> userList;

	@JsonInclude
	private List<Integer> roleList;

	@JsonInclude
	private List<String> rptNameList;

	@JsonInclude
	private List<String> catNameList;

	@JsonInclude
	private List<String> userNameList;

	@JsonInclude
	private List<String> roleNameList;

	// change parameterType from List to Object
	public void setRptNameList(Object names) {
		this.rptNameList = ReportAccessGroupUtils.parseNameList(names);
	}

	// change parameterType from List to Object
	public void setCatNameList(Object names) {
		this.catNameList = ReportAccessGroupUtils.parseNameList(names);
	}

	// change parameterType from List to Object
	public void setUserNameList(Object names) {
		this.userNameList = ReportAccessGroupUtils.parseNameList(names);
	}

	// change parameterType from List to Object
	public void setRoleNameList(Object names) {
		this.roleNameList = ReportAccessGroupUtils.parseNameList(names);
	}
}
