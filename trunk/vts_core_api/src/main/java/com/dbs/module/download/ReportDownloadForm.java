package com.dbs.module.download;

import java.util.Date;

import com.dbs.module.model.SearchForm;

public interface ReportDownloadForm extends SearchForm {

	String getRptName();

	Date getRptDateFrom();

	Date getRptDateTo();

	String getStatus();

	String getCreatedBy();
}
