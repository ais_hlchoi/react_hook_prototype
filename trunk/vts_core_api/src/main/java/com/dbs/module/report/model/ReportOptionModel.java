package com.dbs.module.report.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.module.report.ReportPropsCriteria;
import com.dbs.module.report.ReportPropsOption;
import com.dbs.module.report.ReportPropsParameter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportOptionModel implements ReportPropsOption {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private List<OutputCriteriaModel> criteria;

	@JsonInclude
	private List<OutputParameterModel> parameter;

	@JsonInclude
	private List<String> output;

	@JsonInclude
	private String layout;

	// for data export use only
	@JsonInclude
	private String type;

	@JsonInclude
	private String async;

	@JsonInclude(Include.NON_NULL)
	public List<ReportPropsCriteria> getCriteria() {
		return CollectionUtils.isEmpty(criteria) ? new ArrayList<>() : criteria.stream().map(o -> (ReportPropsCriteria) o).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public List<ReportPropsParameter> getParameter() {
		return CollectionUtils.isEmpty(parameter) ? new ArrayList<>() : parameter.stream().map(o -> (ReportPropsParameter) o).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public List<String> getOutput() {
		return output;
	}

	// change parameterType from List to Object
	public void setOutput(Object output) {
		if (null == output)
			return;

		// "output": "csv, pdf"
		if (output instanceof String) {
			this.output = Arrays.asList(((String) output).split("\\s*,\\s*"));
		}
		// "export": [ "csv", "pdf" ]
		else if (output instanceof Collection) {
			this.output = ((Collection<?>) output).stream().map(String::valueOf).collect(Collectors.toList());
		}
	}
}
