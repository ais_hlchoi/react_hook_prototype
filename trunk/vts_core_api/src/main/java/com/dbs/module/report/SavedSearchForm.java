package com.dbs.module.report;

import com.dbs.module.model.SearchForm;

public interface SavedSearchForm extends SearchForm {

	String getName();

	String getSql();

	String getGenerateOpt();

	String getBuildType();
}
