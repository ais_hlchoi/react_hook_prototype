package com.dbs.module.download.util;

import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.model.ReportDownloadModel;
import com.dbs.module.export.util.DataExportUtils;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.constant.ExportOption;
import com.dbs.module.report.constant.ReportExportType;
import com.dbs.module.report.constant.ReportLayoutType;
import com.dbs.util.CurrentUserUtils;

public class ReportDownloadUtils {

	private ReportDownloadUtils() {
		// for SonarQube scanning purpose
	}

	private static final String REPORT_NAME = "Dynamic Report";

	public static String getDownloadReportName(final String name) {
		return StringUtils.isBlank(name) ? REPORT_NAME : name;
	}

	public static ReportDownloadModel parse(final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		o.setId(id);
		return o;
	}

	public static ReportDownloadModel parse(final String status) {
		ReportDownloadModel o = new ReportDownloadModel();
		o.setStatus(status);
		return o;
	}

	public static ReportDownloadModel parsePending(final ReportTemplate model, final String reportName) {
		return parsePending(model, reportName, null);
	}

	public static ReportDownloadModel parsePending(final ReportTemplate model, final String reportName, final Integer schedId) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == model || StringUtils.isBlank(reportName))
			return o;

		o.setReportName(reportName);
		o.setReportDate(new Date());
		o.setReportType(ReportExportType.getOrElse(model.getType(), ReportExportType.R).name());
		o.setUserName(CurrentUserUtils.getOneBankId());
		o.setFileExtension(ExportOption.getOrElse(model.getOutput(), ExportOption.CSV).ext);
		o.setStatus(DownloadStatus.PENDING.index());
		o.setSql(DataExportUtils.parse(model).getSql());
		o.setLayout(ReportLayoutType.getOrElse(model.getLayout(), ReportLayoutType.H).name());
		o.setRefType(ReportExportType.getOrElse(model.getType(), ReportExportType.R).name());
		o.setRefId(model.getId());
		o.setRefSched(schedId);
		switch (ReportExportType.getOrElse(o.getRefType())) {
		case R:
			o.setRptTmplId(model.getId());
			break;
		case S:
			o.setRptCatgId(model.getId());
			break;
		default:
			break;
		}
		o.setModel(model);
		return o;
	}

	public static ReportDownloadModel parsePending(final ReportTemplate model, final ReportDownload parent, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == model || null == parent)
			return o;

		o.setReportName(model.getName());
		o.setReportDate(new Date());
		o.setReportType(ReportExportType.getOrElse(model.getType(), ReportExportType.C).name());
		o.setParentId(id);
		o.setUserName(parent.getUserName());
		o.setFileExtension(ExportOption.CSV.ext); // force to use CSV which read file easily when merging data
		o.setStatus(parent.getStatus());
		o.setSql(DataExportUtils.parse(model, parent.getModel().getOption().getParameter()).getSql());
		o.setLayout(ReportLayoutType.getOrElse(model.getLayout(), ReportLayoutType.H).name());
		o.setRefType(ReportExportType.R.name());
		o.setRefId(model.getId());
		o.setRefSched(parent.getRefSched());
		o.setRptTmplId(model.getId());
		return o;
	}

	public static ReportDownloadModel parse(final DownloadStatus status, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == status || null == id)
			return o;

		o.setId(id);
		o.setStatus(status.index());
		return o;
	}

	public static ReportDownloadModel parseStart(final Integer rowCount, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == id)
			return o;

		o.setId(id);
		o.setRowCount(rowCount);
		o.setStatus(DownloadStatus.PENDING.index());
		return o;
	}

	public static ReportDownloadModel parseProcess(final Integer rowProcess, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == rowProcess || null == id)
			return o;

		o.setId(id);
		o.setRowProcess(rowProcess);
		o.setStatus(DownloadStatus.PROCESS.index());
		return o;
	}

	public static ReportDownloadModel parseFailure(final Integer rowProcess, final String message, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == rowProcess || null == id)
			return o;

		o.setId(id);
		o.setRowProcess(rowProcess);
		o.setStatus(DownloadStatus.FAILURE.index());
		o.setMessage(message);
		return o;
	}

	public static ReportDownloadModel parseComplete(final String filePath, final Long fileSize, final Date reportDate, final Integer id) {
		ReportDownloadModel o = new ReportDownloadModel();
		if (null == filePath || null == fileSize || null == id)
			return o;

		o.setId(id);
		o.setReportDate(reportDate);
		o.setFileName(FilenameUtils.getBaseName(filePath));
		o.setFilePath(filePath);
		o.setFileSize(fileSize);
		o.setStatus(DownloadStatus.COMPLETE.index());
		return o;
	}
}
