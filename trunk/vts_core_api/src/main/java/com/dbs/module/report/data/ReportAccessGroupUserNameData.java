package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportAccessGroupUserModel;

public class ReportAccessGroupUserNameData {

	private ReportAccessGroupUserModel data;

	public ReportAccessGroupUserNameData(ReportAccessGroupUserModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupUserModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupUserModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}
}
