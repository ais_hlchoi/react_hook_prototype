package com.dbs.module.report;

import java.util.Map;

public interface ReportParam {

	String getName();

	String getDatatype();

	String getLabel();

	String getText();

	Integer getSeq();

	Map<String, String> getAttributes();
}
