package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryTemplateModel extends AbstractUserModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String name;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private RptSetupTmplModel userReportTemplate;

	@JsonInclude
	private List<RptSetupTmplCriteModel> criteria;
}
