package com.dbs.module.report.model;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.vtsp.model.RptSetupTmplColumnModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplFileModel;
import com.dbs.vtsp.model.RptSetupTmplFilterModel;
import com.dbs.vtsp.model.RptSetupTmplModel;
import com.dbs.vtsp.model.RptSetupTmplPrivsModel;
import com.dbs.vtsp.model.RptSetupTmplSortModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportTemplateObjectModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private RptSetupTmplModel userReportTemplate;

	@JsonInclude
	private RptSetupTmplFileModel file;

	@JsonInclude
	private List<RptSetupTmplTableModel> table;

	@JsonInclude
	private List<RptSetupTmplColumnModel> column;

	@JsonInclude
	private List<RptSetupTmplFilterModel> filter;

	@JsonInclude
	private List<RptSetupTmplSortModel> sort;

	@JsonInclude
	private List<RptSetupTmplCriteModel> criteria;

	@JsonInclude
	private RptSetupTmplPrivsModel privilege;

	public String getUserId() {
		return null == userReportTemplate ? null : userReportTemplate.getUserId();
	}

	public boolean hasFile() {
		return !(null == file || null == file.getFileId());
	}

	public boolean hasTable() {
		return CollectionUtils.isNotEmpty(table);
	}

	public boolean hasColumn() {
		return CollectionUtils.isNotEmpty(column);
	}

	public boolean hasFilter() {
		return CollectionUtils.isNotEmpty(filter);
	}

	public boolean hasSort() {
		return CollectionUtils.isNotEmpty(sort);
	}

	public boolean hasCriteria() {
		return CollectionUtils.isNotEmpty(criteria);
	}
}
