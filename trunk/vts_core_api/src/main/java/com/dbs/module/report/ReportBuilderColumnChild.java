package com.dbs.module.report;

public interface ReportBuilderColumnChild extends ReportBuilderOption {

	Integer getIndex();

	Integer getTable();

	Integer getHead();

	String getStaticInd();
}
