package com.dbs.module.report;

public interface ReportBuilderTableChild extends ReportBuilderOption {

	String getDescription();

	Integer getFile();
}
