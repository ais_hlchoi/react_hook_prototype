package com.dbs.module.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportParamModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String value;

	@JsonInclude
	private String label;
}
