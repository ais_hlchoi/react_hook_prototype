package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsTable;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputTableModel implements ReportPropsTable {

	@JsonInclude
	private Integer value;

	@JsonInclude
	private String children;
}
