package com.dbs.module.report.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import com.dbs.module.report.util.VtspUserReportAccessGroupUtils;

public enum ReportAccessGroupType {

	  N()
	, P(VtspUserReportAccessGroupUtils.CATEGORY_PUBLIC)
	, I(VtspUserReportAccessGroupUtils.CATEGORY_INHERIT)
	, R(VtspUserReportAccessGroupUtils.CATEGORY_INHERIT + "_RPT_SETUP_TMPL")
	, S(VtspUserReportAccessGroupUtils.CATEGORY_INHERIT + "_RPT_SETUP_CATG")
	;

	private String desc;

	private ReportAccessGroupType(String desc) {
		this.desc = desc;
	}

	private ReportAccessGroupType() {
		this.desc = name();
	}

	public String desc() {
		return desc;
	}

	public static ReportAccessGroupType getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ReportAccessGroupType getOrElse(String text, ReportAccessGroupType o) {
		if (null == o)
			o = N;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ReportAccessGroupType.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return Stream.of(ReportAccessGroupType.values()).filter(c -> text.equalsIgnoreCase(c.desc())).findFirst().orElse(o);
		}
	}
}
