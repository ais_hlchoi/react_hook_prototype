package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.dbs.module.report.ReportPropsComparator;
import com.dbs.module.report.ReportPropsParameter;
import com.dbs.module.report.ReportParam;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportParamData extends AbstractModuleData implements ReportPropsParameter {

	private ReportParam data;

	public ReportParamData(ReportParam data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportParam getData() {
		return data;
	}

	protected void setData(ReportParam data) {
		this.data = data;
	}

	public String getName() {
		return getData().getName();
	}

	public String getDatatype() {
		return getData().getDatatype();
	}

	public String getLabel() {
		return getData().getLabel();
	}

	public String getPlaceholder() {
		return getOrElse(getData().getAttributes().get(ParamAttrKey.PLACEHOLDER.value), getLabel());
	}

	@JsonInclude(Include.NON_NULL)
	public String getFormat() {
		return getData().getAttributes().get(ParamAttrKey.FORMAT.value);
	}

	@JsonInclude(Include.NON_EMPTY)
	public List<Map<String, String>> getOption() {
		if (StringUtils.isBlank(getData().getAttributes().get(ParamAttrKey.OPTION.value)))
			return new ArrayList<>();

		String[] arr = new String[] { "value", "children" };
		return Stream.of(getData().getAttributes().get(ParamAttrKey.OPTION.value).split("\\|")).map(o -> getOptionMap(o, arr)).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getValue() {
		return null;
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getRptTmplId() {
		return null;
	}

	@JsonInclude(Include.NON_NULL)
	public ReportPropsComparator getComparator() {
		return ReportParamUtils.getComparatorEquals();
	}

	@JsonInclude(Include.NON_NULL)
	public String getMandatory() {
		return ReportParamUtils.getMandatory();
	}

	@JsonInclude(Include.NON_NULL)
	public Map<String, String> getAttributes() {
		if (MapUtils.isEmpty(getData().getAttributes()))
			return null;

		List<String> excludes = Stream.of(ParamAttrKey.values()).map(Enum::name).collect(Collectors.toList());
		Map<String, String> map = getData().getAttributes().entrySet().stream().filter(o -> !excludes.contains(o.getKey())).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		return MapUtils.isEmpty(map) ? null : map;
	}

	@JsonIgnore
	public String getSegment() {
		return null == getAttributes() ? null : getAttributes().get("segment");
	}

	@JsonInclude(Include.NON_NULL)
	public String getText() {
		return getData().getText();
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getSeq() {
		return getData().getSeq();
	}

	private Map<String, String> getOptionMap(String text, String[] arr) {
		Map<String, String> map = new LinkedHashMap<>();
		if (StringUtils.isBlank(text)) {
			for (String key : arr)
				map.put(key, "");
			return map;
		}

		String[] val = text.split(":", arr.length);
		if (val.length == arr.length) {
			for (int i = 0; i < arr.length; i++)
				map.put(arr[i], val[i]);
		} else {
			for (int i = 0; i < arr.length; i++)
				map.put(arr[i], i < val.length ? val[i] : val[0]);
		}
		return map;
	}

	private enum ParamAttrKey {

		  PLACEHOLDER("placeholder")
		, FORMAT("format")
		, OPTION("option")
		;

		public final String value;

		private ParamAttrKey(String value) {
			this.value = value;
		}
	}
}
