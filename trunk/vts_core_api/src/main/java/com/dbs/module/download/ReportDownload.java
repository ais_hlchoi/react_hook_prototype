package com.dbs.module.download;

import java.util.Date;

import com.dbs.module.model.UserForm;
import com.dbs.module.report.ReportTemplate;

public interface ReportDownload extends UserForm {

	String getReportName();

	Date getReportDate();

	String getReportType();

	Integer getParentId();

	String getUserName();

	String getFileExtension();

	String getStatus();

	String getSql();

	String getLayout();

	String getRefType();

	Integer getRefId();

	Integer getRefSched();

	Integer getRptTmplId();

	Integer getRptCatgId();

	Integer getId();

	Integer getRowCount();

	Integer getRowProcess();

	String getMessage();

	String getFileName();

	String getFilePath();

	Long getFileSize();

	ReportTemplate getModel();
}
