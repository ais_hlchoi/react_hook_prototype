package com.dbs.module.report.model;

import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupCountModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer rptTmplCnt;

	@JsonInclude
	private Integer userCnt;

	@JsonInclude
	private Integer roleCnt;

	@JsonInclude
	private RptSetupAcsgModel userReportAccessGroup;

	@JsonInclude
	private String rptTmplName;
}
