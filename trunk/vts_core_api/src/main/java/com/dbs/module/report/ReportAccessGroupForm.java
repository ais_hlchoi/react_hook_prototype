package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.SearchForm;

public interface ReportAccessGroupForm extends SearchForm {

	String getName();

	List<Integer> getRptTmplList();

	List<Integer> getRptCatgList();

	List<Integer> getUserList();

	List<Integer> getRoleList();
}
