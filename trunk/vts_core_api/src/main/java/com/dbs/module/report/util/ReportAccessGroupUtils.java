package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportAccessGroupType;
import com.dbs.module.report.model.ReportAccessGroupModel;

public class ReportAccessGroupUtils {

	private ReportAccessGroupUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportAccessGroupModel getPublicInstance(final String name) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setName(VtspUserReportAccessGroupUtils.getDefaultUserReportAccessGroupName(name));
		o.setActiveInd(Indicator.Y.name());
		return o;
	}

	public static ReportAccessGroupModel parse(final String name) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setName(name);
		return o;
	}

	public static ReportAccessGroupModel parse(final Integer id) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setId(id);
		return o;
	}

	public static ReportAccessGroupModel parse(final Integer refId, final ReportAccessGroupType type) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		if (null == refId || null == type)
			return o;

		o.setName(refId + VtspUserReportAccessGroupUtils.CATEGORY_INHERIT_SUBFIX);
		o.setType(type.name());
		return o;
	}

	public static ReportAccessGroupModel parseGroupTemplate(final Integer rptTmplId, final Integer id) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setId(id);
		o.setRptTmplId(rptTmplId);
		return o;
	}

	public static ReportAccessGroupModel parseGroupCategory(final Integer rptCatgId, final Integer id) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setId(id);
		o.setRptCatgId(rptCatgId);
		return o;
	}

	public static ReportAccessGroupModel parseGroupUser(final String userId, final Integer id) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setId(id);
		o.setUserId(userId);
		return o;
	}

	public static ReportAccessGroupModel parseGroupRole(final Integer roleId, final Integer id) {
		ReportAccessGroupModel o = new ReportAccessGroupModel();
		o.setId(id);
		o.setRoleId(roleId);
		return o;
	}

	public static List<String> parseNameList(final Object names) {
		if (null == names)
			return new ArrayList<>();

		if (names instanceof String)
			return Arrays.asList((String) names);

		if (names instanceof Collection)
			return ((Collection<?>) names).stream().map(String::valueOf).collect(Collectors.toList());

		return new ArrayList<>();
	}
}
