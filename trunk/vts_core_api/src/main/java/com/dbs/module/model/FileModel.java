package com.dbs.module.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class FileModel {

	private String filePath;

	private String fileName;

	private String fileExtension;

	private String fullPathNameExtension;

	private byte[] bytes;

	@JsonIgnore
	private File file;

	public static FileModel of(String filePath, String fileName, String fileExtension, String fullPathNameExtension) {
		FileModel o = new FileModel();
		o.setFilePath(filePath);
		o.setFileName(fileName);
		o.setFileExtension(fileExtension);
		o.setFullPathNameExtension(fullPathNameExtension);
		return o;
	}

	public static FileModel of(String fileName, String fileExtension, File file) {
		FileModel o = new FileModel();
		o.setFileName(fileName);
		o.setFileExtension(fileExtension);
		o.setFile(file);
		return o;
	}

	public FileModel file(File file) {
		setFile(file);
		return this;
	}

	public String getFileFullName() {
		String a = StringUtils.isBlank(getFileName()) ? "" : getFileName();
		String b = StringUtils.isBlank(getFileExtension()) ? "" : String.format(".%s", getFileExtension());
		return a + b;
	}
}
