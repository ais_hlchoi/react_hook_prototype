package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportCategoryObjectModel;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportCategoryPropsData extends AbstractModuleData {

	private ReportCategoryObjectModel data;
	private Map<String, List<ReportParamModel>> paramOptionMap;

	public ReportCategoryPropsData(ReportCategoryObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	public ReportCategoryPropsData put(Map<String, List<ReportParamModel>> paramOptionMap) {
		this.paramOptionMap = paramOptionMap;
		return this;
	}

	protected ReportCategoryObjectModel getData() {
		return data;
	}

	protected void setData(ReportCategoryObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getUserReportCategory().getName();
	}

	public List<ReportCategoryTemplateCriteriaData> getReportList() {
		return null == getData().getReportTemplate() ? new ArrayList<>() : getData().getReportTemplate().stream().map(o -> new ReportCategoryTemplateCriteriaData(o).put(paramOptionMap)).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return null == getData().getUserReportCategory() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getUserReportCategory().getActiveInd()));
	}

	@JsonInclude(Include.NON_NULL)
	public Map<String, List<ReportParamModel>> getParamOption() {
		return paramOptionMap;
	}
}
