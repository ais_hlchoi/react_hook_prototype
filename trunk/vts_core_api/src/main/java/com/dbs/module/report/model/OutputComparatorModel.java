package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsComparator;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputComparatorModel implements ReportPropsComparator {

	@JsonInclude
	private String value;

	@JsonInclude
	private String children;
}
