package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.SearchForm;

public interface ReportCategoryForm extends SearchForm {

	String getName();

	String getGenerateOpt();

	List<Integer> getRptTmplList();

	List<String> getRptNameList();
}
