package com.dbs.module.report;

import java.util.Date;

import com.dbs.module.model.UserForm;

public interface ReportSchedule extends UserForm {

	Integer getId();

	String getRefTable();

	Integer getRefId();

	Integer getRptTmplId();

	Integer getRptCatgId();

	String getName();

	String getCronExpression();

	String getExportOption();

	Date getLastExecutionTime();

	String getNextExecutionTime();

	String getStatus();

	Integer getDispSeq();

	String getActiveInd();

	String getStartDate();

	String getExpiredDate();

	String getEmailInd();



	String getSubject();

	String getRecipient();

	String getRecipientCc();

	String getRecipientBcc();

	String getContent();
}
