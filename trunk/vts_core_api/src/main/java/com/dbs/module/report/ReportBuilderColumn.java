package com.dbs.module.report;

import java.util.List;

public interface ReportBuilderColumn<T extends ReportBuilderColumnChild> extends ReportBuilderOption {

	Integer getHead();

	List<T> getColumns();
}
