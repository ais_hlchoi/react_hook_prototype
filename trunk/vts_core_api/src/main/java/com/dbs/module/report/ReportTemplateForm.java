package com.dbs.module.report;

import com.dbs.module.model.SearchForm;

public interface ReportTemplateForm extends SearchForm {

	String getName();

	String getOwnerName();

	String getSql();

	String getGenerateOpt();

	String getReportBuildType();
}
