package com.dbs.module.report.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportScheduleRefTable;
import com.dbs.module.schedule.constant.ReportScheduleStatus;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.Assert;
import com.dbs.vtsp.model.RptSetupSchedModel;

import lombok.Getter;
import lombok.Setter;

public class VtspUserReportScheduleUtils extends AbstractModuleUtils {

	private VtspUserReportScheduleUtils() {
		// for SonarQube scanning purpose
	}

	public static RptSetupSchedModel parse(final ReportSchedule model) {
		RptSetupSchedModel o = new RptSetupSchedModel();
		RptSchedRefProps refProps = parseRefProps(model);
		o.setRefTable(refProps.getRefTable());
		o.setRefId(refProps.getRefId());
		o.setName(model.getName());
		o.setCronExpression(model.getCronExpression());
		o.setExportOption(model.getExportOption());
		o.setStatus(ReportScheduleStatus.INITIAL.index());
		o.setDispSeq(model.getDispSeq());
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		o.setStartDate(model.getStartDate());
		o.setExpiredDate(model.getExpiredDate());
		o.setEmailInd(model.getEmailInd());
		return o;
	}

	public static RptSetupSchedModel parse(final ReportSchedule model, final RptSetupSchedModel source) {
		if (null == source)
			return source;
		RptSetupSchedModel o = new RptSetupSchedModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setRefTable(source.getRefTable());
		o.setRefId(source.getRefId());
		o.setName(model.getName());
		o.setCronExpression(model.getCronExpression());
		o.setExportOption(model.getExportOption());
		o.setStatus(ReportScheduleStatus.AMEND.index());
		o.setDispSeq(source.getDispSeq()); // update it in parseDispSeq
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		o.setStartDate(model.getStartDate());
		o.setExpiredDate(model.getExpiredDate());
		o.setEmailInd(model.getEmailInd());
		return o;
	}

	public static RptSetupSchedModel parseDispSeq(final ReportSchedule model, final RptSetupSchedModel source) {
		if (null == source)
			return source;
		RptSetupSchedModel o = new RptSetupSchedModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setRefTable(source.getRefTable());
		o.setRefId(source.getRefId());
		o.setDispSeq(model.getDispSeq());
		o.setStartDate(model.getStartDate());
		o.setExpiredDate(model.getExpiredDate());
		return o;
	}

	public static RptSetupSchedModel parseLastExecution(final ReportSchedule model) {
		RptSetupSchedModel o = new RptSetupSchedModel();
		o.setId(model.getId());
		o.setLastExecutionTime(getOrElse(model.getLastExecutionTime(), new Date()));
		return o;
	}

	public static RptSetupSchedModel parseNextExecution(final ReportSchedule model) {
		RptSetupSchedModel o = new RptSetupSchedModel();
		o.setId(model.getId());
		o.setNextExecutionTime(model.getNextExecutionTime() == null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()): model.getNextExecutionTime());
		return o;
	}

	protected static RptSchedRefProps parseRefProps(final ReportSchedule model) {
		if (null == model)
			return new RptSchedRefProps();

		if (null != model.getRptTmplId())
			return RptSchedRefProps.of(ReportScheduleRefTable.RPT_SETUP_TMPL.name(), model.getRptTmplId());

		if (null != model.getRptCatgId())
			return RptSchedRefProps.of(ReportScheduleRefTable.RPT_SETUP_CATG.name(), model.getRptCatgId());

		ReportScheduleRefTable refTable = ReportScheduleRefTable.getOrElse(model.getRefTable());
		return null == refTable || null == model.getRefId() ? new RptSchedRefProps() : RptSchedRefProps.of(refTable.name(), model.getRefId());
	}
}

@Getter
@Setter
class RptSchedRefProps {

	private String refTable;
	private Integer refId;

	public static RptSchedRefProps of(String refTable, Integer refId) {
		RptSchedRefProps o = new RptSchedRefProps();
		o.setRefTable(refTable);
		o.setRefId(refId);
		return o;
	}
}
