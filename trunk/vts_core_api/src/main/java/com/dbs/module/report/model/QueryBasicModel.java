package com.dbs.module.report.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.module.report.ReportPropsBasic;
import com.dbs.module.report.ReportPropsColumn;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryBasicModel implements ReportPropsBasic {

	@JsonInclude
	private OutputFileModel file;

	@JsonInclude
	private List<OutputColumnModel> select;

	@JsonInclude
	private List<OutputTableModel> from;

	@JsonInclude
	private List<OutputFilterModel> where;

	@JsonInclude
	private List<OutputSortModel> order;

	public List<ReportPropsColumn> getSelect() {
		return CollectionUtils.isEmpty(select) ? new ArrayList<>() : select.stream().map(o -> (ReportPropsColumn) o).collect(Collectors.toList());
	}

	public List<ReportPropsTable> getFrom() {
		return CollectionUtils.isEmpty(from) ? new ArrayList<>() : from.stream().map(o -> (ReportPropsTable) o).collect(Collectors.toList());
	}

	public List<ReportPropsFilter> getWhere() {
		return CollectionUtils.isEmpty(where) ? new ArrayList<>() : where.stream().map(o -> (ReportPropsFilter) o).collect(Collectors.toList());
	}

	public List<ReportPropsSort> getOrder() {
		return CollectionUtils.isEmpty(order) ? new ArrayList<>() : order.stream().map(o -> (ReportPropsSort) o).collect(Collectors.toList());
	}
}
