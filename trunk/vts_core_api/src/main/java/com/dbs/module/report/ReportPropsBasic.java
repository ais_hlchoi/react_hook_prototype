package com.dbs.module.report;

import java.util.List;

public interface ReportPropsBasic {

	ReportPropsFile getFile();

	List<ReportPropsColumn> getSelect();

	List<ReportPropsTable> getFrom();

	List<ReportPropsFilter> getWhere();

	List<ReportPropsSort> getOrder();
}
