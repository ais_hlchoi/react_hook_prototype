package com.dbs.module.report.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class ReportConfig {

	@Value("${user.report.build.access-group-name}")
	private String defaultUserReportAccessGroupName;
}
