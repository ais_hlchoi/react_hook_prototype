package com.dbs.module.download.data;

import org.springframework.util.Assert;

import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportDownloadData extends AbstractModuleData {

	private ReportDownloadObjectModel data;

	public ReportDownloadData(ReportDownloadObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportDownloadObjectModel getData() {
		return data;
	}

	protected void setData(ReportDownloadObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getRptName() {
		return getData().getRptName();
	}

	public String getRptDate() {
		return formatDatetime(getData().getRptDate());
	}

	public String getRptSql() {
		return null == getData().getData() ? null : getData().getData().getRptSql();
	}

	public String getRptLayout() {
		return null == getData().getData() ? null : getData().getData().getRptLayout();
	}

	public String getFileName() {
		return getData().getFileName();
	}

	public String getFilePath() {
		return getOrElse(getData().getFilePathPrefix(), "") + getData().getFilePath();
	}

	public String getFileExtension() {
		return getData().getFileExtension();
	}

	public Integer getFileLineCount() {
		return getData().getFileLineCount();
	}

	public Integer getFileLineProcess() {
		return getData().getFileLineProcess();
	}

	public Long getFileSize() {
		return getData().getFileSize();
	}

	public String getPrintCode() {
		return getData().getPrintCode();
	}

	public Integer getPrintCount() {
		return getData().getPrintCount();
	}

	public String getLastPrintedDate() {
		return formatDatetime(getData().getLastPrintedDate());
	}

	public String getStatus() {
		return getData().getStatus();
	}

	public String getStatusLabel() {
		return getOrElse(DownloadStatus.getOrElse(getData().getStatus()).label(), getData().getStatus());
	}

	public boolean isReady() {
		return DownloadStatus.COMPLETE.equals(DownloadStatus.getOrElse(getData().getStatus()));
	}

	public boolean isNoted() {
		return getOrElse(getData().getPrintCount(), 0) > 0;
	}
}
