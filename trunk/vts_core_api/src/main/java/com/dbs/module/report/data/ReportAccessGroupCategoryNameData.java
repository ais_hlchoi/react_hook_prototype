package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportAccessGroupCategoryModel;

public class ReportAccessGroupCategoryNameData {

	private ReportAccessGroupCategoryModel data;

	public ReportAccessGroupCategoryNameData(ReportAccessGroupCategoryModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupCategoryModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupCategoryModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}
}
