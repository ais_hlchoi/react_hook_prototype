package com.dbs.module.session.constant;

import org.apache.commons.lang3.StringUtils;

public enum UserSessionStatus {

	/**
	 * create new session
	 */
	  A("Available")
	/**
	 * update existing session
	 */
	, O("Online")
	/**
	 * delete existing session and create new session
	 */
	, K("Kicked")
	/**
	 * delete expired session and create new session
	 */
	, R("Released")
	/**
	 * delete expired session and reject new session
	 */
	, T("Truncated")
	/**
	 * reject new session
	 */
	, F("Found")
	/**
	 * reject existing session
	 */
	, E("Expired")
	/**
	 * block any session
	 */
	, I("Inactive")
	;

	private String desc;

	private UserSessionStatus(String desc) {
		this.desc = desc;
	}

	private UserSessionStatus() {
		this.desc = UserSessionStatus.getOrElse(name().substring(0, 1)).desc();
	}

	public String desc() {
		return desc;
	}

	public static UserSessionStatus getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static UserSessionStatus getOrElse(String text, UserSessionStatus o) {
		if (null == o)
			o = E;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return UserSessionStatus.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
