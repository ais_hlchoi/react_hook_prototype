package com.dbs.module.report.util;

import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.model.ReportCategoryModel;

public class ReportCategoryUtils {

	private ReportCategoryUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportCategory parse(final Integer id) {
		ReportCategoryModel o = new ReportCategoryModel();
		o.setId(id);
		return o;
	}
}
