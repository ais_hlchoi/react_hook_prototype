package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportAccessGroupTemplateModel;

public class ReportAccessGroupTemplateNameData {

	private ReportAccessGroupTemplateModel data;

	public ReportAccessGroupTemplateNameData(ReportAccessGroupTemplateModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupTemplateModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupTemplateModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}
}
