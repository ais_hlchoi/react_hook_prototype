package com.dbs.module.report.data;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.ReportBuilderTable;
import com.dbs.module.report.ReportBuilderTableChild;

public class ReportBuilderTableData<T extends ReportBuilderTableChild> {

	private ReportBuilderTable<T> data;

	public ReportBuilderTableData(ReportBuilderTable<T> data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportBuilderTable<T> getData() {
		return data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getLabel() {
		return getData().getLabel();
	}

	public List<ReportBuilderTableChildData> getTables() {
		return null == getData().getTables() ? null : getData().getTables().stream().map(ReportBuilderTableChildData::new).collect(Collectors.toList());
	}
}
