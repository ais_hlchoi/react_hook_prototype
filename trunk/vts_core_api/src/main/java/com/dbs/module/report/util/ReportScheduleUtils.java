package com.dbs.module.report.util;

import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.model.ReportScheduleModel;
import com.dbs.vtsp.model.RptSetupSchedModel;

public class ReportScheduleUtils {

	private ReportScheduleUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportScheduleModel parse(final RptSetupSchedModel model) {
		if (null == model)
			return new ReportScheduleModel();

		ReportScheduleModel o = new ReportScheduleModel();
		o.setId(model.getId());
		o.setRefTable(model.getRefTable());
		o.setRefId(model.getRefId());
		o.setName(model.getName());
		o.setCronExpression(model.getCronExpression());
		o.setExportOption(model.getExportOption());
		o.setLastExecutionTime(model.getLastExecutionTime());
		o.setNextExecutionTime(model.getNextExecutionTime());
		o.setDispSeq(model.getDispSeq());
		o.setActiveInd(model.getActiveInd());
		return o;
	}

	public static ReportScheduleModel parse(final ReportSchedule model) {
		if (null == model)
			return new ReportScheduleModel();

		return parse(VtspUserReportScheduleUtils.parse(model));
	} 

	public static ReportScheduleModel parse(final String table, final Integer id) {
		if (StringUtils.isBlank(table) || null == id)
			return new ReportScheduleModel();

		ReportScheduleModel o = new ReportScheduleModel();
		o.setRefTable(table);
		o.setRefId(id);
		return o;
	} 
}
