package com.dbs.module.report.constant;

import org.apache.commons.lang3.StringUtils;

public enum ReportLayoutType {

	  H("Horizontal")
	, V("Vertical")
	;

	private String desc;

	private ReportLayoutType(String desc) {
		this.desc = desc;
	}

	public String desc() {
		return desc;
	}

	public static ReportLayoutType getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ReportLayoutType getOrElse(String text, ReportLayoutType o) {
		if (null == o)
			o = H;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ReportLayoutType.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
