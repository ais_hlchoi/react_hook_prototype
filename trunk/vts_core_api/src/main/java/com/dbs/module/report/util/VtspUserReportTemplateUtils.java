package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.ReportPropsColumn;
import com.dbs.module.report.ReportPropsCriteria;
import com.dbs.module.report.ReportPropsFile;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportProps;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.dbs.module.report.constant.GenerateOption;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportBuildType;
import com.dbs.module.report.constant.ReportLayoutType;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.Assert;
import com.dbs.util.CurrentUserUtils;
import com.dbs.vtsp.model.RptSetupTmplColumnModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplFileModel;
import com.dbs.vtsp.model.RptSetupTmplFilterModel;
import com.dbs.vtsp.model.RptSetupTmplModel;
import com.dbs.vtsp.model.RptSetupTmplPrivsModel;
import com.dbs.vtsp.model.RptSetupTmplSortModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;

public class VtspUserReportTemplateUtils extends AbstractModuleUtils {

	private VtspUserReportTemplateUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportTemplateObjectModel parse(final ReportProps model, final RptSetupTmplModel source) {
		ReportTemplateObjectModel o = new ReportTemplateObjectModel();
		o.setId(source.getId());
		o.setUserReportTemplate(source);
		o.setFile(parseFile(model.getFile(), source.getId()));
		o.setTable(parseFrom(model.getTable(), source.getId()));
		o.setColumn(parseSelect(model.getColumn(), source.getId()));
		o.setFilter(parseWhere(model.getFilter(), source.getId()));
		o.setSort(parseOrder(model.getSort(), source.getId()));
		o.setCriteria(parseAdvance(model.getAdvance(), source.getId()));
		return o;
	}

	public static ReportTemplateObjectModel parse(final ReportTemplate model, final RptSetupTmplModel source) {
		ReportTemplateObjectModel o = new ReportTemplateObjectModel();
		o.setId(source.getId());
		o.setUserReportTemplate(source);
		if (null != model.getBasic()) {
			o.setFile(parseFile(model.getBasic().getFile(), source.getId()));
			o.setTable(parseFrom(model.getBasic().getFrom(), source.getId()));
			o.setColumn(parseSelect(model.getBasic().getSelect(), source.getId()));
			o.setFilter(parseWhere(model.getBasic().getWhere(), source.getId()));
			o.setSort(parseOrder(model.getBasic().getOrder(), source.getId()));
			o.setCriteria(parseAdvance(model.getOption().getCriteria(), source.getId()));
		}
		o.setPrivilege(parsePrivilege(CurrentUserUtils.getOneBankId(), source.getId()));
		return o;
	}

	public static RptSetupTmplModel parseReport(final ReportProps model) {
		RptSetupTmplModel o = new RptSetupTmplModel();
		o.setId(model.getId());
		o.setUserId(CurrentUserUtils.getOneBankId());
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		o.setAdvanceSql(model.getSql());
		o.setLayoutInd(getOrElse(model.getLayout(), ReportLayoutType.H.name()));
		o.setActiveInd(Indicator.Y.name());
		o.setLastUpdatedDate(model.getLastUpdatedDate());

		ReportBuildType buildType = ReportBuildType.getOrElse(model.getBuildType(), ReportBuildType.REPORT_BUILDER);
		if (ReportBuildType.REPORT_ADVANCE != buildType && StringUtils.isNotBlank(o.getAdvanceSql()))
			buildType = ReportBuildType.REPORT_ADVANCE;
		o.setReportBuildType(buildType.name());

		return o;
	}

	public static RptSetupTmplModel parseReport(final ReportTemplate model) {
		RptSetupTmplModel o = new RptSetupTmplModel();
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		if (null != model.getAdvance()) {
			o.setAdvanceSql(model.getAdvance().getSql());
		}
		o.setLayoutInd(getOrElse(model.getLayout(), ReportLayoutType.H.name()));
		o.setActiveInd(Indicator.Y.name());
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupTmplModel parseReport(final ReportProps model, final RptSetupTmplModel source) {
		if (null == source)
			return source;
		RptSetupTmplModel o = new RptSetupTmplModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setUserId(CurrentUserUtils.getOneBankId());
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		o.setAdvanceSql(model.getSql());
		o.setLayoutInd(getOrElse(model.getLayout(), ReportLayoutType.H.name()));
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupTmplModel parseReport(final ReportTemplate model, final RptSetupTmplModel source) {
		if (null == source)
			return source;
		RptSetupTmplModel o = new RptSetupTmplModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setUserId(CurrentUserUtils.getOneBankId());
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		if (null != model.getAdvance()) {
			o.setAdvanceSql(model.getAdvance().getSql());
		}
		o.setLayoutInd(getOrElse(model.getLayout(), ReportLayoutType.H.name()));
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupTmplFileModel parseFile(final ReportPropsFile item, final Integer id) {
		RptSetupTmplFileModel o = new RptSetupTmplFileModel();
		if (null == item)
			return o;

		o.setRptTmplId(id);
		o.setFileId(item.getId());
		o.setFileLabel(item.getLabel());
		return o;
	}

	public static List<RptSetupTmplColumnModel> parseSelect(List<? extends ReportPropsColumn> list, final Integer id) {
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		return list.stream().map(item -> {
			if (null == item)
				return null;

			RptSetupTmplColumnModel o = new RptSetupTmplColumnModel();
			o.setRptTmplId(id);
			o.setTableId(item.getTable());
			o.setHeadId(item.getHead());
			o.setColumnId(item.getValue());
			o.setColumnLabel(item.getChildren());
			o.setDispTitle(item.getTitle());
			o.setDispSeq(item.getSeq());
			o.setDispInd(BooleanUtils.toBoolean(getOrElse(item.getHidden(), "N")) ? "N" : "Y");
			return o;
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static List<RptSetupTmplTableModel> parseFrom(List<? extends ReportPropsTable> list, final Integer id) {
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		return list.stream().map(item -> {
			if (null == item)
				return null;

			RptSetupTmplTableModel o = new RptSetupTmplTableModel();
			o.setRptTmplId(id);
			o.setTableId(item.getValue());
			o.setTableLabel(item.getChildren());
			return o;
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static List<RptSetupTmplFilterModel> parseWhere(List<? extends ReportPropsFilter> list, final Integer id) {
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		return IntStream.rangeClosed(0, list.size() - 1).boxed().map(n -> {
			ReportPropsFilter item = list.get(n);
			if (null == item)
				return null;

			RptSetupTmplFilterModel o = new RptSetupTmplFilterModel();
			o.setRptTmplId(id);
			o.setFilterSeq(n);
			o.setFilterText(item.getValue());
			return o;
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static List<RptSetupTmplSortModel> parseOrder(List<? extends ReportPropsSort> list, final Integer id) {
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		return IntStream.rangeClosed(0, list.size() - 1).boxed().map(n -> {
			ReportPropsSort item = list.get(n);
			if (null == item)
				return null;

			RptSetupTmplSortModel o = new RptSetupTmplSortModel();
			o.setRptTmplId(id);
			o.setSortSeq(n);
			o.setSortText(item.getValue());
			return o;
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static List<RptSetupTmplCriteModel> parseAdvance(List<? extends ReportPropsCriteria> list, final Integer id) {
		if (CollectionUtils.isEmpty(list))
			return new ArrayList<>();

		return IntStream.rangeClosed(0, list.size() - 1).boxed().map(n -> {
			ReportPropsCriteria item = list.get(n);
			if (null == item)
				return null;

			RptSetupTmplCriteModel o = new RptSetupTmplCriteModel();
			o.setRptTmplId(id);
			o.setCriteSeq(n);
			o.setTableId(item.getTable().getValue());
			o.setTableLabel(item.getTable().getChildren());
			o.setColumnId(item.getColumn().getValue());
			o.setColumnLabel(item.getColumn().getChildren());
			o.setComparatorCde(item.getComparator().getValue());
			o.setComparatorLabel(item.getComparator().getChildren());
			o.setFieldValue(item.getValue());
			o.setMandatoryInd(Indicator.toString(Indicator.checkIfTrue(item.getMandatory())));
			o.setIgnoreCasesInd(Indicator.Y.name());
			return o;
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static RptSetupTmplPrivsModel parsePrivilege(final String userId, final Integer id) {
		RptSetupTmplPrivsModel o = VtspUserReportPrivilegeUtils.getOwnerInstance(id);
		o.setUserId(userId);
		o.setActiveInd(Indicator.Y.name());
		return o;
	}
}
