package com.dbs.module.report.model;

import com.dbs.module.report.ReportBuilderColumnChild;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportBuilderColumnModel extends ReportBuilderOptionModel implements ReportBuilderColumnChild {

	@JsonInclude
	private Integer index;

	@JsonInclude
	private Integer table;

	@JsonInclude
	private Integer head;

	@JsonInclude
	private String staticInd;
}
