package com.dbs.module.report.model;

import java.util.Date;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportSchedule;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportScheduleModel extends AbstractUserModel implements ReportSchedule {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String refTable;

	@JsonInclude
	private Integer refId;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer rptCatgId;

	@JsonInclude
	private String name;

	@JsonInclude
	private String cronExpression;

	@JsonInclude
	private String exportOption;

	@JsonInclude
	private Date lastExecutionTime;

	@JsonInclude
	private String nextExecutionTime;

	@JsonInclude
	private String status;

	@JsonInclude
	private Integer dispSeq;

	@JsonInclude
	private String activeInd;

	@JsonInclude
	private String startDate;

	@JsonInclude
	private String expiredDate;

	private String emailInd;



	@JsonInclude
	private String subject;

	@JsonInclude
	private String recipient;

	@JsonInclude
	private String recipientCc;

	@JsonInclude
	private String recipientBcc;

	@JsonInclude
	private String content;
}
