package com.dbs.module.report.data;

import com.dbs.module.report.ReportBuilderTableChild;

public class ReportBuilderTableChildData {

	private ReportBuilderTableChild data;

	public ReportBuilderTableChildData(ReportBuilderTableChild data) {
		this.data = data;
	}

	protected ReportBuilderTableChild getData() {
		return data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getLabel() {
		return getData().getLabel();
	}

	public String getDescription() {
		return getData().getDescription();
	}

	public Integer getFile() {
		return getData().getFile();
	}
}
