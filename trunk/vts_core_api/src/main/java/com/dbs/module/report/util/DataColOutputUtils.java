package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.DataColOutput;
import com.dbs.vtsp.model.SysParmModel;

public class DataColOutputUtils extends MatcherUtils {

	private DataColOutputUtils() {
		// for SonarQube scanning purpose
	}

	public static final String SEGMENT = "DATA_COL_OUTPUT";

	private static final String INDEX = "INDEX";
	private static final String SP = ",";
	private static final String SP_BY_CHAR = "[" + SP + "]";
	private static final String SP_BY_PATT = SP + "?\\s*(\\w+)";
	private static final String BR = ":";

	public static DataColOutput parse(final List<SysParmModel> list) {
		DataColOutputProps o = new DataColOutputProps();
		if (CollectionUtils.isEmpty(list))
			return o;
		SysParmModel index = list.stream().filter(item -> INDEX.equalsIgnoreCase(item.getCode())).findFirst().orElse(null);
		if (null == index || StringUtils.isBlank(index.getParmValue()))
			return o;
		String[] args = index.getParmValue().split(BR);
		parseFilter(args[0], SP_BY_CHAR).stream().forEach(filter -> {
			o.addFilter(Stream.of(filter.split(SP))
					.map(code -> list.stream().filter(item -> code.equalsIgnoreCase(item.getCode())).findFirst().orElse(null))
					.filter(Objects::nonNull)
					.map(SysParmModel::getParmValue)
					.filter(StringUtils::isNotBlank)
					.map(MatcherUtils::parseFilterProps)
					.collect(Collectors.toList()));
		});
		parseOutput(args.length < 1 ? EMPTY : args[1], SP_BY_PATT).stream().forEach(code -> {
			SysParmModel model = list.stream().filter(item -> code.equalsIgnoreCase(item.getCode())).findFirst().orElse(null);
			if (null != model && StringUtils.isNotBlank(model.getParmValue()))
				o.addOutput(parseOutputProps(model.getParmValue()));
		});
		return o;
	}

	private static List<String> parseFilter(final String text, final String format) {
		return new MatcherUtils().parseByChar(text, format);
	}

	private static List<String> parseOutput(final String text, final String format) {
		return new MatcherUtils().parseByPattern(text, format);
	}

	protected static String parseColumnIndex(String[] args) {
		return args[ParmValueIndex.NAME.value];
	}

	protected static Object[] parseFilterIndex(final String[] columns, final String[] args) {
		return new Object[] { index(columns, args[FilterIndex.INDEX.value]), pattern(args[FilterIndex.PATTERN.value]), match(args[FilterIndex.MATCH.value]) };
	}

	protected static void parseOutputIndex(final String[] columns, final String[] args, Map<Integer, String> map) {
		Integer n = index(columns, args[OutputIndex.INDEX.value]);
		if (n < 0)
			return;
		map.put(n, args[1]);
	}
}

class DataColOutputProps implements DataColOutput {

	private List<List<String[]>> filters = new ArrayList<>();
	private List<String[]> outputs = new ArrayList<>();
	private Map<String, Object[]> index = new LinkedHashMap<>();
	private Map<Integer, String> delta = new LinkedHashMap<>();
	private List<String> error = new ArrayList<>();

	public void addFilter(List<String[]> filter) {
		filters.add(filter);
	}

	public void addOutput(String[] output) {
		outputs.add(output);
	}

	public List<String> getErrors() {
		return error.stream().collect(Collectors.toList());
	}

	public String getError() {
		return hasError() ? error.get(0) : "";
	}

	public int getErrorCount() {
		return error.size();
	}

	public boolean hasError() {
		return CollectionUtils.isNotEmpty(error);
	}

	public void build(String[] columns) {
		index.clear();
		delta.clear();
		error.clear();
		error.addAll(DataColOutputUtils.validate(filters));
		if (hasError())
			return;
		filters.stream().flatMap(f -> f.stream()).forEach(c -> index.put(DataColOutputUtils.parseColumnIndex(c), DataColOutputUtils.parseFilterIndex(columns, c) ));
		outputs.stream().forEach(c -> DataColOutputUtils.parseOutputIndex(columns, c, delta));
	}

	public String[] apply(String[] line) {
		return DataColOutputUtils.apply(line, filters, index, delta);
	}

	public String toFilterString() {
		return filters.stream().flatMap(f -> f.stream().map(c -> StringUtils.join(c, ", "))).collect(Collectors.joining("\n"));
	}

	public String toOutputString() {
		return outputs.stream().map(c -> StringUtils.join(c, ", ")).collect(Collectors.joining("\n"));
	}
}
