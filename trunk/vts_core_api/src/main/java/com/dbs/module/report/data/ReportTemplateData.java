package com.dbs.module.report.data;

import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.dbs.module.report.ReportPropsAdvance;
import com.dbs.module.report.ReportPropsBasic;
import com.dbs.module.report.ReportPropsOption;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.util.ReportTemplateUtils;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportTemplateData extends AbstractModuleData {

	private ReportTemplateObjectModel data;
	private Map<String, List<ReportParamModel>> paramOptionMap;

	public ReportTemplateData(ReportTemplateObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	public ReportTemplateData put(Map<String, List<ReportParamModel>> paramOptionMap) {
		this.paramOptionMap = paramOptionMap;
		return this;
	}

	protected ReportTemplateObjectModel getData() {
		return data;
	}

	protected void setData(ReportTemplateObjectModel data) {
		this.data = data;
	}

	public ReportPropsBasic getBasic() {
		return ReportTemplateUtils.parseBasic(getData());
	}

	public ReportPropsAdvance getAdvance() {
		return ReportTemplateUtils.parseAdvance(getData().getUserReportTemplate());
	}

	public ReportPropsOption getOption() {
		return ReportTemplateUtils.parseOption(getData().getUserReportTemplate(), getData().getCriteria(), paramOptionMap);
	}

	public String getName() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getName();
	}

	public Integer getId() {
		return getData().getId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getUserId() {
		return null == getData().getPrivilege() ? null : getData().getUserReportTemplate().getUserId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getActiveInd();
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return null == getData().getUserReportTemplate() ? null : formatTimeNano(getData().getUserReportTemplate().getLastUpdatedDate());
	}
}
