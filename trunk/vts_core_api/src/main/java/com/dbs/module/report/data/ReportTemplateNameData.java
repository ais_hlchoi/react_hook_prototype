package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportTemplateNameModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportTemplateNameData extends AbstractModuleData {

	private ReportTemplateNameModel data;

	public ReportTemplateNameData(ReportTemplateNameModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportTemplateNameModel getData() {
		return data;
	}

	protected void setData(ReportTemplateNameModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}

	public String getType() {
		return getData().getType();
	}

	public String getDescription() {
		return getData().getDescription();
	}
}
