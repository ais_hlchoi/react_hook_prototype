package com.dbs.module.download.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.dbs.module.download.ReportDownloadForm;
import com.dbs.module.model.AbstractSearchModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportDownloadFormModel extends AbstractSearchModel implements ReportDownloadForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String rptName;

	@JsonInclude
	private List<Date> rptDate;

	@JsonInclude
	private String status;

	@JsonInclude
	private String createdBy;

	@JsonInclude
	private String refId;

	@JsonInclude
	private String refType;

	@JsonInclude
	private String schedId;

	public Date getRptDateFrom() {
		return CollectionUtils.isNotEmpty(getRptDate()) && getRptDate().size() > 0 ? getRptDate().get(0) : null;
	}

	public Date getRptDateTo() {
		return CollectionUtils.isNotEmpty(getRptDate()) && getRptDate().size() > 1 ? getRptDate().get(1) : null;
	}
}
