package com.dbs.module.export.model;

import com.dbs.module.export.DataExport;
import com.dbs.module.model.AbstractUserModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataExportModel extends AbstractUserModel implements DataExport {

	@JsonInclude
	private String select;

	@JsonInclude
	private String from;

	@JsonInclude
	private String where;

	@JsonInclude
	private String orderBy;

	@JsonInclude
	private String groupBy;

	@JsonInclude
	private String groupAs;

	@JsonInclude
	private String sql;

	@JsonInclude
	private String layout;
}
