package com.dbs.module.report.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum ReportBuildType {

	  REPORT_UNKNOWN("N", "Unknown")
	, REPORT_BUILDER("B", "ReportBuilder")
	, REPORT_ADVANCE("A", "ReportAdvance")
	;

	private String index;
	private String desc;

	private ReportBuildType(String index, String desc) {
		this.index = index;
		this.desc = desc;
	}

	public String index() {
		return index;
	}

	public String desc() {
		return desc;
	}

	public static ReportBuildType getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ReportBuildType getOrElse(String text, ReportBuildType o) {
		if (null == o)
			o = REPORT_UNKNOWN;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ReportBuildType.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return Stream.of(ReportBuildType.values()).filter(c -> text.equalsIgnoreCase(c.index())).findFirst().orElse(o);
		}
	}
}
