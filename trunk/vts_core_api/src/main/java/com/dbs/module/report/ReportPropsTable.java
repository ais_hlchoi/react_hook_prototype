package com.dbs.module.report;

public interface ReportPropsTable {

	Integer getValue();

	String getChildren();
}
