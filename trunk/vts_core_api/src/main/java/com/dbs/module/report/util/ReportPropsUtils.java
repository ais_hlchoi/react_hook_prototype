package com.dbs.module.report.util;

import com.dbs.module.report.ReportPropsParameter;

import org.apache.commons.lang3.BooleanUtils;

import com.dbs.module.report.ReportProps;
import com.dbs.module.report.model.OutputColumnModel;
import com.dbs.module.report.model.OutputComparatorModel;
import com.dbs.module.report.model.OutputCriteriaModel;
import com.dbs.module.report.model.OutputFileModel;
import com.dbs.module.report.model.OutputFilterModel;
import com.dbs.module.report.model.OutputParameterModel;
import com.dbs.module.report.model.OutputSortModel;
import com.dbs.module.report.model.OutputTableModel;
import com.dbs.module.report.model.ReportPropsModel;
import com.dbs.vtsp.model.RptSetupTmplColumnModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplFileModel;
import com.dbs.vtsp.model.RptSetupTmplFilterModel;
import com.dbs.vtsp.model.RptSetupTmplSortModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;

public class ReportPropsUtils {

	private ReportPropsUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportProps parse(final Integer id) {
		ReportPropsModel o = new ReportPropsModel();
		o.setId(id);
		return o;
	}

	public static OutputFileModel parse(final RptSetupTmplFileModel model) {
		OutputFileModel o = new OutputFileModel();
		if (null == model)
			return o;

		o.setId(model.getFileId());
		o.setLabel(model.getFileLabel());
		return o;
	}

	public static OutputTableModel parse(final RptSetupTmplTableModel model) {
		OutputTableModel o = new OutputTableModel();
		if (null == model)
			return o;

		o.setValue(model.getTableId());
		o.setChildren(model.getTableLabel());
		return o;
	}

	public static OutputColumnModel parse(final RptSetupTmplColumnModel model) {
		OutputColumnModel o = new OutputColumnModel();
		if (null == model)
			return o;

		o.setTable(model.getTableId());
		o.setHead(model.getHeadId());
		o.setValue(model.getColumnId());
		o.setChildren(model.getColumnLabel());
		o.setTitle(model.getDispTitle());
		o.setSeq(model.getDispSeq());
		o.setHidden(BooleanUtils.toBoolean(model.getDispInd()) ? "N" : "Y");
		return o;
	}

	public static OutputFilterModel parse(final RptSetupTmplFilterModel model) {
		OutputFilterModel o = new OutputFilterModel();
		if (null == model)
			return o;

		o.setSeq(model.getFilterSeq());
		o.setValue(model.getFilterText());
		return o;
	}

	public static OutputSortModel parse(final RptSetupTmplSortModel model) {
		OutputSortModel o = new OutputSortModel();
		if (null == model)
			return o;

		o.setSeq(model.getSortSeq());
		o.setValue(model.getSortText());
		return o;
	}

	public static OutputCriteriaModel parse(final RptSetupTmplCriteModel model) {
		OutputCriteriaModel o = new OutputCriteriaModel();
		if (null == model)
			return o;

		OutputTableModel table = new OutputTableModel();
		table.setValue(model.getTableId());
		table.setChildren(model.getTableLabel());
		o.setTable(table);

		OutputColumnModel column = new OutputColumnModel();
		column.setValue(model.getColumnId());
		column.setChildren(model.getColumnLabel());
		o.setColumn(column);

		OutputComparatorModel comparator = new OutputComparatorModel();
		comparator.setValue(model.getComparatorCde());
		comparator.setChildren(model.getComparatorLabel());
		o.setComparator(comparator);

		o.setSeq(model.getCriteSeq());
		o.setValue(model.getFieldValue());
		o.setMandatory(model.getMandatoryInd());
		return o;
	}

	public static OutputParameterModel parse(final ReportPropsParameter model) {
		OutputParameterModel o = new OutputParameterModel();
		if (null == model)
			return o;

		o.setName(model.getName());
		o.setDatatype(model.getDatatype());
		o.setLabel(model.getLabel());
		o.setFormat(model.getFormat());
		o.setValue(model.getValue());

		OutputComparatorModel comparator = new OutputComparatorModel();
		comparator.setValue(model.getComparator().getValue());
		comparator.setChildren(model.getComparator().getChildren());
		o.setComparator(comparator);

		o.setMandatory(model.getMandatory());
		return o;
	}
}
