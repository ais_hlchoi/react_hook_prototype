package com.dbs.module.model;

public interface SearchForm extends UserForm {

	int getPage();

	int getPageSize();

	String getDefaultSearchText();
}
