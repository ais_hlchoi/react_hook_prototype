package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.report.ReportPropsCriteria;
import com.dbs.module.report.model.ReportCategoryTemplateModel;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.module.report.util.ReportPropsUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportCategoryTemplateCriteriaData {

	private ReportCategoryTemplateModel data;
	private Map<String, List<ReportParamModel>> paramOptionMap;

	public ReportCategoryTemplateCriteriaData(ReportCategoryTemplateModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	public ReportCategoryTemplateCriteriaData put(Map<String, List<ReportParamModel>> paramOptionMap) {
		this.paramOptionMap = paramOptionMap;
		return this;
	}

	protected ReportCategoryTemplateModel getData() {
		return data;
	}

	protected void setData(ReportCategoryTemplateModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getName();
	}

	@JsonInclude(Include.NON_NULL)
	public List<ReportPropsCriteria> getAdvance() {
		return CollectionUtils.isEmpty(getData().getCriteria()) ? new ArrayList<>() : getData().getCriteria().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getSql() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getAdvanceSql();
	}

	@JsonInclude(Include.NON_NULL)
	public List<ReportParamData> getParameter() {
		return ReportParamUtils.getParamMap(getSql(), paramOptionMap);
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getDispSeq() {
		return getData().getDispSeq();
	}
}
