package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.ReportForm;
import com.dbs.module.report.model.ReportFormModel;

public class ReportFormUtils {

	private ReportFormUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportForm parse(final String srcName, final String tableName, final int page, final int size) {
		ReportFormModel o = new ReportFormModel();
		o.setSrcName(srcName);
		o.setTableName(tableName);
		o.setPage(page);
		o.setPageSize(size);
		List<String> srcNameList = parse(o.getSrcName());
		if (CollectionUtils.isNotEmpty(srcNameList)) {
			o.setSrcNameList(srcNameList);
			o.setSrcName(null);
		}
		List<String> tableNameList = parse(o.getTableName());
		if (CollectionUtils.isNotEmpty(tableNameList)) {
			o.setTableNameList(tableNameList);
			o.setTableName(null);
		}
		return o;
	}

	public static ReportForm parse(final String srcName, final String tableName, final String columnName, final int page, final int size) {
		ReportFormModel o = new ReportFormModel();
		o.setSrcName(srcName);
		o.setTableName(tableName);
		o.setColumnName(columnName);
		o.setPage(page);
		o.setPageSize(size);
		List<String> srcNameList = parse(o.getSrcName());
		if (CollectionUtils.isNotEmpty(srcNameList)) {
			o.setSrcNameList(srcNameList);
			o.setSrcName(null);
		}
		List<String> tableNameList = parse(o.getTableName());
		if (CollectionUtils.isNotEmpty(tableNameList)) {
			o.setTableNameList(tableNameList);
			o.setTableName(null);
		}
		List<String> columnNameList = parse(o.getColumnName());
		if (CollectionUtils.isNotEmpty(columnNameList)) {
			o.setColumnNameList(columnNameList);
			o.setColumnName(null);
		}
		return o;
	}

	private static List<String> parse(final String text) {
		if (StringUtils.isBlank(text) || text.indexOf(',') < 0)
			return new ArrayList<>();

		return Stream.of(text.split("\\s*,\\s*")).filter(StringUtils::isNotBlank).collect(Collectors.toList());
	}
}
