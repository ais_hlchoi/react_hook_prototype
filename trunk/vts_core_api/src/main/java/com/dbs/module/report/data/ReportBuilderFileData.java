package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.ReportBuilderFile;

public class ReportBuilderFileData {

	private ReportBuilderFile data;

	public ReportBuilderFileData(ReportBuilderFile data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportBuilderFile getData() {
		return data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getLabel() {
		return getData().getLabel();
	}
}
