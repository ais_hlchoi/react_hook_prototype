package com.dbs.module.session;

import com.dbs.module.model.UserForm;

public interface UserSessionForm extends UserForm {

	String getUserId();

	String getToken();

	String getSid();

	String getIp();

	String getMode();

	Long getTime();
}
