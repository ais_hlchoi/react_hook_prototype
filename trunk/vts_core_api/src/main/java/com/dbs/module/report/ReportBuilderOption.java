package com.dbs.module.report;

public interface ReportBuilderOption {

	Integer getId();

	String getLabel();

	String getValue();
}
