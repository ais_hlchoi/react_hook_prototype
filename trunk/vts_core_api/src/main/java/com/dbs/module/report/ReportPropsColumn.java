package com.dbs.module.report;

public interface ReportPropsColumn {

	Integer getTable();

	Integer getHead();

	Integer getValue();

	String getChildren();

	String getTitle();

	Integer getSeq();

	String getHidden();
}
