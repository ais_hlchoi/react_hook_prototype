package com.dbs.module.report;

public interface ReportPropsComparator {

	String getValue();

	String getChildren();
}
