package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractSearchModel;
import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.util.ReportAccessGroupUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryFormModel extends AbstractSearchModel implements ReportCategoryForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private String generateOpt;

	@JsonInclude
	private List<Integer> rptTmplList;

	@JsonInclude
	private List<String> rptNameList;

	// change parameterType from List to Object
	public void setRptNameList(Object names) {
		this.rptNameList = ReportAccessGroupUtils.parseNameList(names);
	}
}
