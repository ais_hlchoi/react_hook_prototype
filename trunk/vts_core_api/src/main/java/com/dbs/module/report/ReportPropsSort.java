package com.dbs.module.report;

public interface ReportPropsSort {

	Integer getSeq();

	String getValue();
}
