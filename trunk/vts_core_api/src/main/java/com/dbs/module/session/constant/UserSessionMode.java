package com.dbs.module.session.constant;

import org.apache.commons.lang3.StringUtils;

public enum UserSessionMode {

	/**
	 * quit existing session
	 */
	  N("Nomral")
	/**
	 * kick out existing session
	 */
	, K("Kicked")
	/**
	 * release expired session
	 */
	, E("Expired")
	/**
	 * close existing session conditionally
	 */
	, U("Unknown")
	;

	private String desc;

	private UserSessionMode(String desc) {
		this.desc = desc;
	}

	public String desc() {
		return desc;
	}

	public static UserSessionMode getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static UserSessionMode getOrElse(String text, UserSessionMode o) {
		if (null == o)
			o = E;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return UserSessionMode.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
