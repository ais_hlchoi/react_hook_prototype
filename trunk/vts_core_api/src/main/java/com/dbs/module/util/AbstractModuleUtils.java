package com.dbs.module.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public abstract class AbstractModuleUtils {

	protected AbstractModuleUtils() {
		// for SonarQube scanning purpose
	}

	protected static final String MSG_FAIL_FORMAT = "Error found when %s caused by %s";

	protected static <T> T getOrElse(T value, T valueIfNull) {
		if (value instanceof String)
			return StringUtils.isBlank((String) value) ? valueIfNull : value;

		return null == value ? valueIfNull : value;
	}

	/**
	 * This is a simply tool to bind data to model through getter/setter.
	 * Be simple, only both name and parameter count are checked in order to match suitable method from class.
	 * 
	 * @param key   : getter/setter
	 * @param model : object of target
	 * @param value : value set to target
	 */
	protected static void bindData(String key, Object model, Object value) {
		bindData(key, model, value, false);
	}

	@SuppressWarnings("unchecked")
	protected static void bindData(String key, Object model, Object value, boolean check) {
		if (StringUtils.isBlank(key) || null == model)
			return;

		if (model instanceof Map) {
			Map<String, Object> map = (Map<String, Object>) model;
			map.put(key, check ? getOrElse(map.get(key), value) : value);
			return;
		}

		String get = String.format("get%s%s", key.substring(0, 1).toUpperCase(), key.substring(1));
		String set = String.format("set%s%s", key.substring(0, 1).toUpperCase(), key.substring(1));
		boolean isNotEmptyOldValue = true;

		for (Method methodSet : model.getClass().getMethods()) {
			if (methodSet.getName().equals(set) && methodSet.getParameterCount() == 1) {
				if (null != value && !methodSet.getParameterTypes()[0].isAssignableFrom(value.getClass()))
					continue;
				if (check && isNotEmptyOldValue) {
					for (Method methodGet : model.getClass().getMethods()) {
						if (methodGet.getName().equals(get) && methodGet.getParameterCount() == 0) {
							try {
								isNotEmptyOldValue = null != methodGet.invoke(model);
								if (isNotEmptyOldValue)
									return;
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
								isNotEmptyOldValue = false;
							}
							break;
						}
					}
				}
				try {
					methodSet.invoke(model, value);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					continue;
				}
				break;
			}
		}
	}
}
