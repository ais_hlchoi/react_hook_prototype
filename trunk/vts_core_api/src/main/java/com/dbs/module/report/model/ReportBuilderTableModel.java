package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.report.ReportBuilderColumn;
import com.dbs.module.report.ReportBuilderTableChild;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportBuilderTableModel extends ReportBuilderOptionModel implements ReportBuilderTableChild, ReportBuilderColumn<ReportBuilderColumnModel> {

	@JsonInclude
	private Integer file;

	@JsonInclude
	private Integer head;

	@JsonInclude
	private List<ReportBuilderColumnModel> columns;
}
