package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.dbs.module.report.ReportPropsColumn;
import com.dbs.module.report.ReportPropsCriteria;
import com.dbs.module.report.ReportPropsFile;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.module.report.util.ReportParamUtils;
import com.dbs.module.report.util.ReportPropsUtils;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportPropsData extends AbstractModuleData {

	private ReportTemplateObjectModel data;
	private Map<String, List<ReportParamModel>> paramOptionMap;

	public ReportPropsData(ReportTemplateObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	public ReportPropsData put(Map<String, List<ReportParamModel>> paramOptionMap) {
		this.paramOptionMap = paramOptionMap;
		return this;
	}

	protected ReportTemplateObjectModel getData() {
		return data;
	}

	protected void setData(ReportTemplateObjectModel data) {
		this.data = data;
	}

	public ReportPropsFile getFile() {
		return ReportPropsUtils.parse(getData().getFile());
	}

	public List<ReportPropsTable> getTable() {
		return CollectionUtils.isEmpty(getData().getTable()) ? new ArrayList<>() : getData().getTable().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	public List<ReportPropsColumn> getColumn() {
		return CollectionUtils.isEmpty(getData().getColumn()) ? new ArrayList<>() : getData().getColumn().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	public List<ReportPropsFilter> getFilter() {
		return CollectionUtils.isEmpty(getData().getFilter()) ? new ArrayList<>() : getData().getFilter().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	public List<ReportPropsSort> getSort() {
		return CollectionUtils.isEmpty(getData().getSort()) ? new ArrayList<>() : getData().getSort().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	public List<ReportPropsCriteria> getAdvance() {
		return CollectionUtils.isEmpty(getData().getCriteria()) ? new ArrayList<>() : getData().getCriteria().stream().map(ReportPropsUtils::parse).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getSql() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getAdvanceSql();
	}

	@JsonInclude(Include.NON_NULL)
	public List<ReportParamData> getParameter() {
		return ReportParamUtils.getParamMap(getSql(), paramOptionMap);
	}

	@JsonInclude(Include.NON_NULL)
	public String getLayout() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getLayoutInd();
	}

	public String getName() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getName();
	}

	public String getDescription() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getDescription();
	}

	public String getGenerateOpt() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getGenerateOpt();
	}

	public Integer getId() {
		return getData().getId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getUserId() {
		return null == getData().getPrivilege() ? null : getData().getUserReportTemplate().getUserId();
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return null == getData().getUserReportTemplate() ? null : getData().getUserReportTemplate().getActiveInd();
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return null == getData().getUserReportTemplate() ? null : formatTimeNano(getData().getUserReportTemplate().getLastUpdatedDate());
	}
}
