package com.dbs.module.report;

import com.dbs.module.model.UserForm;

public interface ReportTemplate extends UserForm {

	ReportPropsBasic getBasic();

	ReportPropsAdvance getAdvance();

	ReportPropsOption getOption();

	String getOutput();

	String getLayout();

	String getType();

	String getName();

	String getDescription();

	String getGenerateOpt();

	Integer getId();

	String getActiveInd();
}
