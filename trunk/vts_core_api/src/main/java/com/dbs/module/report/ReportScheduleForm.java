package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.SearchForm;

public interface ReportScheduleForm extends SearchForm {

	String getName();

	String getRptTmplName();

	String getRptCatgName();

	String getExportOption();

	String getLastExecution();

	String getNextExecution();

	Integer getRptTmplId();

	Integer getRptCatgId();

	List<Integer> getRptTmplList();

	List<Integer> getRptCatgList();

	String getExpiredDate();

	String getStartDate();
}
