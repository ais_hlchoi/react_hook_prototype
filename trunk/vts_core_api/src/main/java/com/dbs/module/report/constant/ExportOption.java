package com.dbs.module.report.constant;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;

public enum ExportOption {

	  CSV(new MediaType("text", "csv", StandardCharsets.UTF_8))
//	, XLS(MediaType.MULTIPART_FORM_DATA)
	, XLSX(MediaType.APPLICATION_OCTET_STREAM)
	, PDF(MediaType.APPLICATION_PDF)
	;

	public final MediaType mediaType;
	public final String contentType;
	public final String ext;

	private ExportOption(MediaType mediaType) {
		this.mediaType = mediaType;
		this.contentType = getMediaTypeValue(mediaType);
		this.ext = this.name().toLowerCase();
	}

	private static String getMediaTypeValue(MediaType mediaType) {
		if (null == mediaType.getCharset())
			return String.format("%s/%s", mediaType.getType(), mediaType.getSubtype());

		return String.format("%s/%s;charset=%s", mediaType.getType(), mediaType.getSubtype(), mediaType.getCharset().name());
	}

	public static ExportOption getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ExportOption getOrElse(String text, ExportOption o) {
		if (null == o)
			o = CSV;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ExportOption.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
