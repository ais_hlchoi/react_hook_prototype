package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportCategoryTemplateModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportCategoryTemplateNameData {

	private ReportCategoryTemplateModel data;

	public ReportCategoryTemplateNameData(ReportCategoryTemplateModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportCategoryTemplateModel getData() {
		return data;
	}

	protected void setData(ReportCategoryTemplateModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getDispSeq() {
		return getData().getDispSeq();
	}
}
