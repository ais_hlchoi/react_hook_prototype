package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.report.ReportBuilderFile;
import com.dbs.module.report.ReportBuilderTable;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportBuilderFileModel extends ReportBuilderOptionModel implements ReportBuilderFile, ReportBuilderTable<ReportBuilderTableModel> {

	@JsonInclude
	private List<ReportBuilderTableModel> tables;
}
