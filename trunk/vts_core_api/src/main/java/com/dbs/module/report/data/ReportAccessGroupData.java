package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.util.AbstractModuleData;
import com.dbs.vtsp.model.RptSetupAcsgModel;

public class ReportAccessGroupData extends AbstractModuleData {

	private RptSetupAcsgModel data;

	public ReportAccessGroupData(RptSetupAcsgModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected RptSetupAcsgModel getData() {
		return data;
	}

	protected void setData(RptSetupAcsgModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}

	public String getType() {
		return getData().getType();
	}

	public String getActiveInd() {
		return Indicator.toString(Indicator.checkIfTrue(getData().getActiveInd()));
	}
}
