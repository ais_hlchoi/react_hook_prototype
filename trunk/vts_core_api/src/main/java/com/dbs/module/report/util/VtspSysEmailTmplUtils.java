package com.dbs.module.report.util;

import com.dbs.module.report.ReportSchedule;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportScheduleRefTable;
import com.dbs.module.schedule.constant.ReportScheduleStatus;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.Assert;
import com.dbs.vtsp.model.SysEmailTmplModel;
import com.dbs.vtsp.model.SysEmailTmplModel;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VtspSysEmailTmplUtils extends AbstractModuleUtils {

	private VtspSysEmailTmplUtils() {
		// for SonarQube scanning purpose
	}

	public static SysEmailTmplModel parse(final ReportSchedule model) {
		SysEmailTmplModel o = new SysEmailTmplModel();
		//o.setRefTable(refTable);
		//o.setRefId(refId);
		o.setName(model.getName());
		o.setSubject(model.getSubject());
		o.setRecipient(model.getRecipient());
		o.setRecipientCc(model.getRecipientCc());
		o.setRecipientBcc(model.getRecipientBcc()); // update it in parseDispSeq
		o.setContent(model.getContent());
		o.setDispSeq(model.getDispSeq());
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static SysEmailTmplModel parse(final ReportSchedule model, final SysEmailTmplModel source) {
		if (null == source)
			return source;
		SysEmailTmplModel o = new SysEmailTmplModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setRefTable(source.getRefTable());
		o.setRefId(source.getRefId());
		o.setName(model.getName());
		o.setSubject(model.getSubject());
		o.setRecipient(model.getRecipient());
		o.setRecipientCc(model.getRecipientCc());
		o.setRecipientBcc(model.getRecipientBcc()); // update it in parseDispSeq
		o.setContent(model.getContent());
		o.setDispSeq(model.getDispSeq());
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}






}

@Getter
@Setter
class SysEmailTmpProps {

	private String refTable;
	private Integer refId;

	public static SysEmailTmpProps of(String refTable, Integer refId) {
		SysEmailTmpProps o = new SysEmailTmpProps();
		o.setRefTable(refTable);
		o.setRefId(refId);
		return o;
	}
}
