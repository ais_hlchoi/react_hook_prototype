package com.dbs.module.report.model;

import com.dbs.module.report.ReportBuilderOption;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportBuilderOptionModel implements ReportBuilderOption {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String label;

	@JsonInclude
	private String value;

	@JsonInclude
	private String description;
}
