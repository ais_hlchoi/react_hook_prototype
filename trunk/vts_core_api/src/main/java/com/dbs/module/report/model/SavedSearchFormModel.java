package com.dbs.module.report.model;

import com.dbs.module.model.AbstractSearchModel;
import com.dbs.module.report.SavedSearchForm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SavedSearchFormModel extends AbstractSearchModel implements SavedSearchForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private String sql;

	@JsonInclude
	private String generateOpt;

	@JsonInclude
	private String buildType;
}
