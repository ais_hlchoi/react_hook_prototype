package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractSearchModel;
import com.dbs.module.report.ReportScheduleForm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportScheduleFormModel extends AbstractSearchModel implements ReportScheduleForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private String rptTmplName;

	@JsonInclude
	private String rptCatgName;

	@JsonInclude
	private String exportOption;

	@JsonInclude
	private String lastExecution;

	@JsonInclude
	private String nextExecution;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer rptCatgId;

	@JsonInclude
	private List<Integer> rptTmplList;

	@JsonInclude
	private List<Integer> rptCatgList;

	@JsonInclude
	private String expiredDate;

	@JsonInclude
	private String startDate;
}
