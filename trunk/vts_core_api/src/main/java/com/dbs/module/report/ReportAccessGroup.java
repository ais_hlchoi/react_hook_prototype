package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.UserForm;

public interface ReportAccessGroup extends UserForm {

	String getName();

	String getType();

	String getActiveInd();

	Integer getId();

	Integer getRptTmplId();

	Integer getRptCatgId();

	String getUserId();

	Integer getRoleId();

	List<Integer> getRptAcsgList();

	List<Integer> getRptTmplList();

	List<Integer> getRptCatgList();

	List<String> getUserList();

	List<Integer> getRoleList();

	List<String> getRptNameList();

	List<String> getCatNameList();

	List<String> getUserNameList();

	List<String> getRoleNameList();
}
