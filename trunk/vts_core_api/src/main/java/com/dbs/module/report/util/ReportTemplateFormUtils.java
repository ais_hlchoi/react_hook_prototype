package com.dbs.module.report.util;

import com.dbs.module.report.SavedSearchForm;
import com.dbs.module.report.constant.ReportBuildType;

import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.ReportTemplateForm;
import com.dbs.module.report.model.ReportTemplateFormModel;

public class ReportTemplateFormUtils {

	private ReportTemplateFormUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportTemplateForm parse(final SavedSearchForm model) {
		ReportTemplateFormModel o = new ReportTemplateFormModel();
		o.setName(model.getName());
		o.setSql(model.getSql());
		o.setDefaultSearchText(model.getDefaultSearchText());
		o.setGenerateOpt(model.getGenerateOpt());
		// allows search all reportBuildTypes if buildType is empty
		if (StringUtils.isNotBlank(model.getBuildType())) {
			o.setReportBuildType(ReportBuildType.getOrElse(model.getBuildType(), ReportBuildType.REPORT_BUILDER).name());
		}
		return o;
	}
}
