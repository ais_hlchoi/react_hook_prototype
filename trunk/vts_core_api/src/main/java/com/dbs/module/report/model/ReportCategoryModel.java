package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.util.ReportAccessGroupUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryModel extends AbstractUserModel implements ReportCategory {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private String description;

	@JsonInclude
	private String generateOpt;

	@JsonInclude
	private String activeInd;

	// detail

	@JsonInclude
	private Integer id;

	@JsonInclude
	private Integer rptTmplId;

	@JsonInclude
	private Integer dispSeq;

	// list

	@JsonInclude
	private List<Integer> rptCatgList;

	@JsonInclude
	private List<Integer> rptTmplList;

	@JsonInclude
	private List<String> rptNameList;

	// change parameterType from List to Object
	public void setRptNameList(Object names) {
		this.rptNameList = ReportAccessGroupUtils.parseNameList(names);
	}
}
