package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportAccessGroupCountModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportAccessGroupListData extends AbstractModuleData {

	private ReportAccessGroupCountModel data;

	public ReportAccessGroupListData(ReportAccessGroupCountModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupCountModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupCountModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return String.format(getData().getUserReportAccessGroup().getName(), getData().getRptTmplName());
	}

	public String getType() {
		return getData().getUserReportAccessGroup().getType();
	}

	public Integer getReportCount() {
		return getOrElse(getData().getRptTmplCnt(), 0);
	}

	public Integer getUserCount() {
		return getOrElse(getData().getUserCnt(), 0);
	}

	public Integer getRoleCount() {
		return getOrElse(getData().getRoleCnt(), 0);
	}

	public String getActiveInd() {
		return null == getData().getUserReportAccessGroup() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getUserReportAccessGroup().getActiveInd()));
	}
}
