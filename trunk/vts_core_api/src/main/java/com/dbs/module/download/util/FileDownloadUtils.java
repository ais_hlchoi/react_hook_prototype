package com.dbs.module.download.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.dbs.module.download.ReportDownload;
import com.dbs.module.download.constant.DownloadStatus;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.CurrentUserUtils;
import com.dbs.vtsp.model.RptFileDnldDataModel;
import com.dbs.vtsp.model.RptFileDnldLogModel;
import com.dbs.vtsp.model.RptFileDnldModel;
import com.dbs.vtsp.model.RptFileDnldUserGrpModel;

public class FileDownloadUtils extends AbstractModuleUtils {

	private FileDownloadUtils() {
		// for SonarQube scanning purpose
	}

	public static final String FOLDER_OUTPUT = "${folder.output}/";

	public static RptFileDnldModel parseReportDownload(final ReportDownload model) {
		RptFileDnldModel o = new RptFileDnldModel();
		if (StringUtils.isBlank(model.getReportName()) || StringUtils.isBlank(model.getUserName()) || StringUtils.isBlank(model.getFileExtension()) || null == model.getReportDate())
			return o;

		String reportPath = String.format("/rpt/%s_%s_%s.%s", model.getReportName().replaceAll("\\s+", "_"), model.getUserName(), new SimpleDateFormat("yyyyMMddHHmmss").format(model.getReportDate()), model.getFileExtension().toLowerCase());

		o.setRptName(model.getReportName());
		o.setRptDate(model.getReportDate());
		o.setRptType(model.getReportType());
		o.setParentId(model.getParentId());
		o.setFileName(null);
		o.setFilePath(reportPath);
		o.setFileExtension(model.getFileExtension());
		o.setFileLineCount(null);
		o.setFileLineProcess(null);
		o.setFileSize(null);
		o.setPrintCode(null);
		o.setPrintCount(null);
		o.setLastPrintedDate(null);
		o.setStatus(model.getStatus());
		return o;
	}

	public static RptFileDnldModel parseReportDownloadStatus(final ReportDownload model) {
		RptFileDnldModel o = new RptFileDnldModel();
		o.setId(model.getId());
		o.setStatus(DownloadStatus.getOrElse(model.getStatus(), DownloadStatus.PENDING).index());
		return o;
	}

	public static RptFileDnldModel parseReportDownloadStart(final ReportDownload model) {
		RptFileDnldModel o = new RptFileDnldModel();
		o.setId(model.getId());
		o.setFileLineCount(model.getRowCount());
		o.setStatus(DownloadStatus.PROCESS.index());
		return o;
	}

	public static RptFileDnldModel parseReportDownloadProcess(final ReportDownload model) {
		RptFileDnldModel o = new RptFileDnldModel();
		o.setId(model.getId());
		o.setFileLineProcess(model.getRowProcess());
		o.setStatus(DownloadStatus.PROCESS.index());
		return o;
	}

	public static RptFileDnldModel parseReportDownloadFailure(final ReportDownload model) {
		RptFileDnldModel o = new RptFileDnldModel();
		o.setId(model.getId());
		o.setFileLineProcess(model.getRowProcess());
		o.setStatus(DownloadStatus.FAILURE.index());
		return o;
	}

	public static RptFileDnldModel parseReportDownloadComplete(final ReportDownload model) {
		String uuid = UUID.randomUUID().toString();
		String printCode = String.format("%s_%s", new SimpleDateFormat("yyyyMMdd").format(getOrElse(model.getReportDate(), new Date())), uuid.replaceAll("-", "").substring(0, 16));

		RptFileDnldModel o = new RptFileDnldModel();
		o.setId(model.getId());
		o.setFileName(model.getFileName());
		o.setFilePath(model.getFilePath());
		o.setFileSize(getFileSizeKb(model.getFileSize()));
		o.setPrintCode(printCode);
		o.setPrintCount(0);
		o.setStatus(DownloadStatus.COMPLETE.index());
		return o;
	}

	public static RptFileDnldDataModel parseReportDownloadData(final ReportDownload model, final Integer id) {
		RptFileDnldDataModel o = new RptFileDnldDataModel();
		o.setFileDnldId(id);
		o.setRptSql(model.getSql());
		o.setRptLayout(model.getLayout());
		o.setRefType(model.getRefType());
		o.setRefId(model.getRefId());
		o.setRefSched(model.getRefSched());
		return o;
	}

	public static RptFileDnldUserGrpModel parseReportDownloadUserGroup(final ReportDownload model, final Integer id) {
		RptFileDnldUserGrpModel o = new RptFileDnldUserGrpModel();
		o.setFileDnldId(id);
		o.setUserId(getOrElse(model.getOneBankId(), CurrentUserUtils.getOneBankId()));
		return o;
	}

	public static RptFileDnldLogModel parseReportDownloadLog(final ReportDownload model) {
		RptFileDnldLogModel o = new RptFileDnldLogModel();
		o.setFileDnldId(model.getId());
		o.setLogMessage(model.getMessage());
		o.setStatus(model.getStatus());
		o.setCurrentInd(Indicator.Y.name());

		if (o.getLogMessage().length() > 2000)
			o.setLogMessage(o.getLogMessage().substring(0, 1996) + " ...");
		return o;
	}

	public static Long getFileSizeKb(final Long fileSize) {
		return (long) Math.ceil((double) fileSize / 1024);
	}
}
