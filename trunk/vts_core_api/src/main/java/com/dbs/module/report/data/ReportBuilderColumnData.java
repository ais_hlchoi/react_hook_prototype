package com.dbs.module.report.data;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.ReportBuilderColumn;
import com.dbs.module.report.ReportBuilderColumnChild;

public class ReportBuilderColumnData<T extends ReportBuilderColumnChild> {

	private ReportBuilderColumn<T> data;

	public ReportBuilderColumnData(ReportBuilderColumn<T> data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportBuilderColumn<T> getData() {
		return data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getLabel() {
		return getData().getLabel();
	}

	public Integer getHead() {
		return getData().getHead();
	}

	public List<ReportBuilderColumnChildData> getColumns() {
		return null == getData().getColumns() ? null : getData().getColumns().stream().map(ReportBuilderColumnChildData::new).collect(Collectors.toList());
	}
}
