package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsFile;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputFileModel implements ReportPropsFile {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String label;
}
