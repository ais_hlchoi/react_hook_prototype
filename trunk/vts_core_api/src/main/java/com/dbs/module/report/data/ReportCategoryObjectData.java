package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportCategoryObjectModel;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportCategoryObjectData extends AbstractModuleData {

	private ReportCategoryObjectModel data;

	public ReportCategoryObjectData(ReportCategoryObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportCategoryObjectModel getData() {
		return data;
	}

	protected void setData(ReportCategoryObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return null == getData().getUserReportCategory() ? null : getData().getUserReportCategory().getName();
	}

	public String getDescription() {
		return null == getData().getUserReportCategory() ? null : getData().getUserReportCategory().getDescription();
	}

	public String getGenerateOpt() {
		return null == getData().getUserReportCategory() ? null : getData().getUserReportCategory().getGenerateOpt();
	}

	public List<ReportCategoryTemplateNameData> getReportList() {
		return null == getData().getReportTemplate() ? new ArrayList<>() : getData().getReportTemplate().stream().map(ReportCategoryTemplateNameData::new).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return null == getData().getUserReportCategory() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getUserReportCategory().getActiveInd()));
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return null == getData().getUserReportCategory() ? null : formatTimeNano(getData().getUserReportCategory().getLastUpdatedDate());
	}

	public Map<String, Boolean> getRestrictedFields() {
		Map<String, Boolean> map = new HashMap<>();
		map.put("name", false);
		map.put("report", false);
		return map;
	}
}
