package com.dbs.module.report.constant;

import org.apache.commons.lang3.StringUtils;

public enum ReportScheduleRefTable {

	  RPT_SETUP_TMPL
	, RPT_SETUP_CATG
	;

	public static ReportScheduleRefTable getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static ReportScheduleRefTable getOrElse(String text, ReportScheduleRefTable o) {
		if (null == o)
			o = RPT_SETUP_TMPL;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return ReportScheduleRefTable.valueOf(text);
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
