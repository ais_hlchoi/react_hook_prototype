package com.dbs.module.report.model;

import com.dbs.module.model.AbstractUserModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupRoleModel extends AbstractUserModel {

	@JsonInclude
	private Integer id;

	@JsonInclude(Include.NON_NULL)
	private Integer rptAcsgId;

	@JsonInclude
	private String name;
}
