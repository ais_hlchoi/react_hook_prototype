package com.dbs.module.report.constant;

import org.apache.commons.lang3.StringUtils;

public enum Indicator {

	  Y(0, true)
	, N(1, false)
	;

	public final int index;
	public final boolean value;

	private Indicator(int index, boolean value) {
		this.index = index;
		this.value = value;
	}

	public static String toString(boolean bool) {
		return bool ? Y.name() : N.name();
	}

	public static boolean checkIfTrue(String name) {
		return check(name, Y);
	}

	public static boolean checkIfNotTrue(String name) {
		return !check(name, Y);
	}

	public static boolean checkIfFalse(String name) {
		return check(name, N);
	}

	public static boolean checkIfNotFalse(String name) {
		return !check(name, N);
	}

	private static boolean check(String text, Indicator o) {
		if (StringUtils.isBlank(text) || null == o)
			return false;

		try {
			return o.equals(Indicator.valueOf(text.toUpperCase()));
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
}
