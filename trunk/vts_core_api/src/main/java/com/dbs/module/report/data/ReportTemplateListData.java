package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportTemplateListData extends AbstractModuleData {

	private ReportTemplatePrivilegeModel data;

	public ReportTemplateListData(ReportTemplatePrivilegeModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportTemplatePrivilegeModel getData() {
		return data;
	}

	protected void setData(ReportTemplatePrivilegeModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getName();
	}

	public String getOwnerName() {
		return getData().getOwnerName();
	}

	public boolean isOwner() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getOwnerInd());
	}

	public boolean isAdmin() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getAdminInd());
	}

	public boolean isCanSelect() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanSelect());
	}

	public boolean isCanUpdate() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanUpdate());
	}

	public boolean isCanDelete() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanDelete());
	}

	public boolean isCanExport() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanExport());
	}

	public String getActiveInd() {
		return null == getData().getPrivilege() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getPrivilege().getActiveInd()));
	}
}
