package com.dbs.module.file.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.dbs.module.file.FileImport;
import com.dbs.module.file.FileImportProps;
import com.dbs.module.model.FileModel;
import com.dbs.module.model.FileProps;
import com.dbs.util.Assert;
import com.dbs.util.FilenameUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.opencsv.CSVReader;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Service
public class LocalStorageUtils implements FileImport {

	@Getter
	@Setter(AccessLevel.PROTECTED)
	private FileImportProps props;

	protected LocalStorageUtils() {
		// for SonarQube scanning purpose
	}

	public LocalStorageUtils(FileImportProps props) {
		setProps(props);
	}

	public FileProps[] list() {
		return list(getProps().getLoadPath(), null);
	}

	public FileProps[] list(String path) {
		return list(path, null);
	}

	public FileProps[] list(String path, String prefix) {
		if (StringUtils.isBlank(path))
			return new FileProps[] {};

		File folder = getFile(path);
		String[] files = folder.list();
		if (ArrayUtils.isEmpty(files))
			return new FileProps[] {};

		return (StringUtils.isBlank(prefix) ? Stream.of(files) : Stream.of(files).filter(o -> o.startsWith(prefix))).map(baseName -> {
			String filePath = FilenameUtils.concat(folder.getAbsolutePath(), baseName);
			return FileProps.of(filePath, getFile(filePath).length());
		}).toArray(FileProps[]::new);
	}

	public FileModel[] getFileModel() {
		return getFileModel(getProps().getLoadPath(), null);
	}

	public FileModel[] getFileModel(String path, String prefix) {
		if (StringUtils.isBlank(path))
			return new FileModel[] {};

		File folder = getFile(path);
		String[] files = folder.list();
		if (ArrayUtils.isEmpty(files))
			return new FileModel[] {};

		return (StringUtils.isBlank(prefix) ? Stream.of(files) : Stream.of(files).filter(o -> o.startsWith(prefix))).map(file -> FileModel.of(path, FilenameUtils.getBaseName(file), FilenameUtils.getExtension(file), path + file).file(getFile(path + file))).toArray(FileModel[]::new);
	}

	public void copy(String source, String destination) {
		LocalStorageUtils.moveFile(source, destination, StandardCopyOption.COPY_ATTRIBUTES);
	}

	public void move(String source, String destination) {
		LocalStorageUtils.moveFile(source, destination, StandardCopyOption.REPLACE_EXISTING);
	}

	public File getFile(String name) {
		return LocalStorageUtils.openFile(name);
	}

	public byte[] getFileByte(String name) {
		byte[] bytes = null;
		try {
			bytes = Files.readAllBytes(Paths.get(FortifyStrategyUtils.toPathString(name)));
		} catch (IOException e) {
			// no action
		}
		Assert.isTrue(bytes != null, String.format("File not existed"));
		return bytes;
	}

	public InputStream getStream(String name) {
		try {
			return new FileInputStream(getFile(name));
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	public CSVReader getCsvReader(String name) {
		try {
			return new CSVReader(new FileReader(getFile(name)));
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	public boolean isFile(String name) {
		return getFile(name).isFile();
	}

	public boolean isMatch(String name, String pattern, String ext, String parent) {
		return isFile(name) && FilenameUtils.match(name, pattern, ext) && FilenameUtils.matchParent(name, parent);
	}

	public List<String> prepare(String... paths) {
		List<String> logger = new ArrayList<>();
		Stream.of(paths).map(o -> FilenameUtils.getFullPath(FortifyStrategyUtils.toPathString(o))).map(Paths::get).forEach(path -> {
			try {
				Files.createDirectories(path);
				logger.add(String.format("Path is created [ %s ]", path.toString()));
			} catch (FileAlreadyExistsException e) {
				// nothing to do
			} catch (IOException e) {
				logger.add(FortifyStrategyUtils.toErrString(e));
			}
		});
		return logger;
	}

	// ----------------------------------------------------------------------------------------------------
	// common logic
	// ----------------------------------------------------------------------------------------------------

	protected static File openFile(String name) {
		return new File(FortifyStrategyUtils.toPathString(name));
	}

	protected static void moveFile(String source, String destination, StandardCopyOption option) {
		Assert.isTrue(new File(FortifyStrategyUtils.toPathString(source)).exists(), "File not found.");
		String msg = null;
		try {
			Path from = Paths.get(FortifyStrategyUtils.toPathString(source));
			Path to = Paths.get(destination);
			Files.move(from, to, option);
		} catch (IOException e) {
			msg = FortifyStrategyUtils.toErrString(e);
		}
		Assert.isNull(msg, msg);
	}

	public Object saveFile(FileModel fileModel) {
		// TODO
		return new NotImplementedException();
	}

	public Object saveFile(FileModel file, String storageKey) {
		// TODO
		return new NotImplementedException();
	}

	public Object saveFile(String fileName, String fileExtension, String content) {
		// TODO
		return new NotImplementedException();
	}

	public boolean exist(String name) {
		return Files.exists(Paths.get(FortifyStrategyUtils.toPathString(name)));
	}

	public boolean delete(String name) {
		return getFile(name).delete();
	}
}
