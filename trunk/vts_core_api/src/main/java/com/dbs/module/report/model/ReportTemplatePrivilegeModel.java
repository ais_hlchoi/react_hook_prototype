package com.dbs.module.report.model;

import java.util.List;

import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.dbs.vtsp.model.RptSetupTmplModel;
import com.dbs.vtsp.model.RptSetupTmplPrivsModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportTemplatePrivilegeModel extends RptSetupTmplModel {

	@JsonInclude
	private String ownerName;

	@JsonInclude
	private RptSetupTmplPrivsModel privilege;

	@JsonInclude
	private List<RptSetupAcsgModel> accessGroup;

	@JsonInclude
	private List<ReportAccessGroupRoleModel> roleList;

	@JsonInclude
	private List<RptSetupTmplTableModel> table;
}
