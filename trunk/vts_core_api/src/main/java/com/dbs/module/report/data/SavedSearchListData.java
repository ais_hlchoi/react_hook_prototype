package com.dbs.module.report.data;

import java.sql.Timestamp;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.dbs.module.report.model.ReportAccessGroupRoleModel;
import com.dbs.vtsp.model.RptSetupTmplTableModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.dbs.module.report.constant.ExportOption;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportLayoutType;
import com.dbs.module.report.model.ReportTemplatePrivilegeModel;
import com.dbs.module.util.AbstractModuleData;
import com.dbs.vtsp.model.RptSetupAcsgModel;

public class SavedSearchListData extends AbstractModuleData {

	private ReportTemplatePrivilegeModel data;

	public SavedSearchListData(ReportTemplatePrivilegeModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportTemplatePrivilegeModel getData() {
		return data;
	}

	protected void setData(ReportTemplatePrivilegeModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getSavedSearch() {
		return getData().getName();
	}

	public String getDescription() {
		return getData().getDescription();
	}

	public String getGenerateOpt() {
		return getData().getGenerateOpt();
	}

	public String getAccessGroup() {
		return null == getData().getAccessGroup() ? "" : getData().getAccessGroup().stream().map(RptSetupAcsgModel::getName).collect(Collectors.joining(", "));
	}

	public String getRoleList() {
		return null == getData().getRoleList() ? "" : getData().getRoleList().stream().map(ReportAccessGroupRoleModel::getName).collect(Collectors.joining(", "));
	}

	public String getTable() {
		return null == getData().getTable() ? "" : getData().getTable().stream().map(RptSetupTmplTableModel::getTableLabel).collect(Collectors.joining(", "));
	}

	public String getSql() {
		return getData().getAdvanceSql();
	}

	public String getLayout() {
		return ReportLayoutType.getOrElse(getData().getLayoutInd()).desc();
	}

	public String getExportOption() {
		return Stream.of(ExportOption.values()).map(Enum::name).map(StringUtils::lowerCase).collect(Collectors.joining(", "));
	}

	public String getCreatedBy() {
		return getData().getCreatedBy();
	}

	public Timestamp getCreatedDate() {
		return getData().getCreatedDate();
	}

	public String getLastUpdatedBy() {
		return getData().getLastUpdatedBy();
	}

	public Timestamp getLastUpdatedDate() {
		return getData().getLastUpdatedDate();
	}

	public boolean isCanSelect() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanSelect());
	}

	public boolean isCanUpdate() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanUpdate());
	}

	public boolean isCanDelete() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanDelete());
	}

	public boolean isCanExport() {
		return null != getData().getPrivilege() && Indicator.checkIfTrue(getData().getPrivilege().getCanExport());
	}
}
