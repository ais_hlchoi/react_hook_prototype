package com.dbs.module.report.model;

import java.util.Map;

import com.dbs.module.report.ReportPropsParameter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputParameterModel implements ReportPropsParameter {

	@JsonInclude
	private String name;

	@JsonInclude
	private String datatype;

	@JsonInclude
	private String label;

	@JsonInclude(Include.NON_NULL)
	private String format;

	@JsonInclude(Include.NON_NULL)
	private String value;

	@JsonInclude(Include.NON_NULL)
	private Integer rptTmplId;

	@JsonInclude(Include.NON_NULL)
	private OutputComparatorModel comparator;

	@JsonInclude(Include.NON_NULL)
	private String mandatory;

	@JsonInclude(Include.NON_NULL)
	private Map<String, String> attributes;

	@JsonInclude(Include.NON_NULL)
	private String text;

	@JsonIgnore
	public String getSegment() {
		return null == getAttributes() ? null : getAttributes().get("segment");
	}
}
