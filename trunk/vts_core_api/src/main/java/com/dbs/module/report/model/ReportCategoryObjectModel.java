package com.dbs.module.report.model;

import java.util.List;

import com.dbs.vtsp.model.RptSetupCatgModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCategoryObjectModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private RptSetupCatgModel userReportCategory;

	@JsonInclude
	private List<ReportCategoryTemplateModel> reportTemplate;
}
