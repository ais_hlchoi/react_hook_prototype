package com.dbs.module.export;

import com.dbs.module.model.UserForm;

public interface DataExport extends UserForm {

	String getSelect();

	String getFrom();

	String getWhere();

	String getOrderBy();

	String getGroupBy();

	String getGroupAs();

	String getSql();

	String getLayout();
}
