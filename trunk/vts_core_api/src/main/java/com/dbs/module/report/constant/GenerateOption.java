package com.dbs.module.report.constant;

import org.apache.commons.lang3.StringUtils;

public enum GenerateOption {

	  B("Both")
	, O("Online")
	, S("Schedule")
	;

	private String desc;

	private GenerateOption(String desc) {
		this.desc = desc;
	}

	public String desc() {
		return desc;
	}

	public static GenerateOption getOrElse(String text) {
		return getOrElse(text, null);
	}

	public static GenerateOption getOrElse(String text, GenerateOption o) {
		if (null == o)
			o = B;

		if (StringUtils.isBlank(text))
			return o;

		try {
			return GenerateOption.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
