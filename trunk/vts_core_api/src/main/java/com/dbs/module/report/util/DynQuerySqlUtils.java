package com.dbs.module.report.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.dbs.module.report.ReportPropsAdvance;
import com.dbs.module.report.ReportPropsBasic;
import com.dbs.module.report.ReportPropsColumn;
import com.dbs.module.report.ReportPropsFilter;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.ReportPropsSort;
import com.dbs.module.report.ReportPropsTable;
import com.dbs.vtsp.dao.InformationSchemaMapper;
import com.dbs.vtsp.dao.SrcFileMapper;
import com.dbs.vtsp.model.RptSrcFileHdrColModel;
import com.dbs.vtsp.model.RptSrcFileHdrModel;
import com.dbs.vtsp.model.RptSrcFileModel;
import com.dbs.vtsp.model.UserTabColsModel;

@Service
public class DynQuerySqlUtils {

	@Autowired
	private SrcFileMapper srcFileMapper;

	@Autowired
	private InformationSchemaMapper informationSchemaMapper;

	@Value("${datasource.tableSchema}")
	private String tableSchema;

	public static final String RPT_FILE_UPLD = "rpt_file_upld";

	public static final String TABLE_ALIAS_FILE_SUBFIX = "_file_set";
	public static final String TABLE_ALIAS_DATA_SUBFIX = "_data_set";

	public static final String DATA_SUBFIX  = "_EXT";

	public static final String PARTITION_BY_FORMAT = "%Y%m";

	private static final String COLUMN_STAT = "t.%s";
	private static final String COLUMN_DATA = "COLUMN_GET(d.dynamic_cols, upper('%s') as char)";
	private static final String COLUMN_NULL = "null";

	private static final String COLUMN_HAS_ALIAS = "%2$s %1$s";
	private static final String COLUMN_NOT_ALIAS = "%1$s";

	public static final String COLUMN_DATA_FUNC_PARM_NAME = "value";
	public static final String COLUMN_DATA_FUNC_PARM_VALUE = "${" + COLUMN_DATA_FUNC_PARM_NAME + "}";
	public static final String COLUMN_DATA_FUNC_PARM_REGEX = "\\$\\{" + COLUMN_DATA_FUNC_PARM_NAME + "\\}";

	private static final String COLUMN_DATA_FUNC_FORMAT_DATE = "%Y-%m-%d";
	private static final String COLUMN_DATA_FUNC_FORMAT_TIME = "%H:%i:%s";
	private static final String COLUMN_DATA_FUNC_FORMAT_DATETIME = COLUMN_DATA_FUNC_FORMAT_DATE + " " + COLUMN_DATA_FUNC_FORMAT_TIME;

	private static final String DB_OBJ_NAME_PATTERN  = "([a-zA-Z0-9_]+)";
	private static final String TABLE_ALIAS_PATTERN  = "([\\s+|,|\\(])" + DB_OBJ_NAME_PATTERN + "\\.";
	private static final String FIELD_COLUMN_PATTERN = TABLE_ALIAS_PATTERN + DB_OBJ_NAME_PATTERN;

	private static final String WITH   = " with ";
	private static final String SELECT = " select ";
	private static final String FROM   = " from ";
	private static final String WHERE  = " where ";
	private static final String ORDER  = " order by ";

	public static String render(final ReportTemplate model, final SrcFileMapper srcFileMapper, final InformationSchemaMapper informationSchemaMapper, final String tableSchema) {
		return new DynQuerySqlUtils(srcFileMapper, informationSchemaMapper, tableSchema).getSql(model);
	}

	public static List<String> parse(final List<ReportPropsColumn> select, final List<ReportPropsTable> from) {
		return new DynQuerySqlUtils().getFieldColumnList(select, from, null).stream().map(FieldColumn::toString).collect(Collectors.toList());
	}

	protected DynQuerySqlUtils() {
	}

	protected DynQuerySqlUtils(SrcFileMapper srcFileMapper, InformationSchemaMapper informationSchemaMapper, String tableSchema) {
		this.srcFileMapper = srcFileMapper;
		this.informationSchemaMapper = informationSchemaMapper;
		this.tableSchema = tableSchema;
	}

	public String getSql(ReportTemplate model) {
		if (null == model)
			return null;

		String sql = parseSql(model.getAdvance());
		return StringUtils.isNotBlank(sql) ? sql : parseSql(model.getBasic());
	}

	protected String parseSql(ReportPropsAdvance bean) {
		if (null == bean || StringUtils.isBlank(bean.getSql()))
			return null;
		
		return bean.getSql();
	}

	protected String parseSql(ReportPropsBasic bean) {
		if (null == bean)
			return null;

		validate(bean);

		StringBuilder withClause = new StringBuilder();
		StringBuilder fromClause = new StringBuilder();
		StringBuilder selectClause = new StringBuilder();
		StringBuilder whereClause = new StringBuilder();
		StringBuilder orderClause = new StringBuilder();

		bean.getWhere().stream().forEach(o -> whereClause.append(getWhereClause(o)));
		bean.getOrder().stream().forEach(o -> orderClause.append(getOrderClause(o)));

		List<TableColumn> tableColumnList = getTableColumnList(bean.getSelect(), bean.getFrom(), whereClause.toString(), orderClause.toString());
		Map<String, Map<String, RptSrcFileHdrColModel>> tableColumnMap = getDataColumnMap(tableColumnList.stream().map(TableColumn::getTable).collect(Collectors.toList()));
		tableColumnList.stream().forEach(o -> withClause.append(getWithClause(o, tableColumnMap)));
		tableColumnList.stream().forEach(o -> fromClause.append(getFromClause(o)));

		List<FieldColumn> fieldColumnList = getFieldColumnList(bean.getSelect(), bean.getFrom(), tableColumnMap);
		fieldColumnList.stream().forEach(o -> selectClause.append(getSelectClause(o)));

		StringBuilder sql = new StringBuilder();
		sql.append(WITH + withClause.toString().substring(1));
		sql.append(SELECT + selectClause.toString().substring(1));
		sql.append(FROM + fromClause.toString().substring(1));

		if (StringUtils.isNotBlank(whereClause.toString()))
			sql.append(WHERE + whereClause.toString().replaceAll("^\\s*[Aa][Nn][Dd]\\s+", ""));

		if (StringUtils.isNotBlank(orderClause.toString()))
			sql.append(ORDER + orderClause.toString());

		return sql.toString().trim();
	}

	protected void validate(ReportPropsBasic bean) {
		Assert.notEmpty(bean.getSelect(), "Column cannot be null");
		Assert.notEmpty(bean.getFrom(), "Table cannot be null");
		Assert.isTrue(!(CollectionUtils.isEmpty(bean.getWhere()) && bean.getFrom().size() > 1), "Filter cannot be null");
	}

	protected List<TableColumn> getTableColumnList(List<ReportPropsColumn> select, List<ReportPropsTable> from, String... text) {
		List<String> tables = from.stream().map(ReportPropsTable::getChildren).collect(Collectors.toList());
		Map<String, RptSrcFileModel> map = srcFileMapper.getSrcFileByName(tables).stream().collect(Collectors.toMap(RptSrcFileModel::getFileNameStart, Function.identity()));
		List<TableColumn> list = from.stream().map(t -> new TableColumn(t.getChildren().toUpperCase(), map.get(t.getChildren().toUpperCase()).getTableName(), map.get(t.getChildren().toUpperCase()).getId(), select.stream().filter(c -> c.getTable().equals(t.getValue())).map(ReportPropsColumn::getChildren).map(StringUtils::upperCase).collect(Collectors.toList()))).collect(Collectors.toList());
		addExtraColumns(list, text);
		return list;
	}

	protected List<FieldColumn> getFieldColumnList(List<ReportPropsColumn> select, List<ReportPropsTable> from, Map<String, Map<String, RptSrcFileHdrColModel>> tableColumnMap) {
		Map<Integer, String> tables = from.stream().collect(Collectors.toMap(ReportPropsTable::getValue, ReportPropsTable::getChildren));
		List<FieldColumn> list = select.stream().map(o -> new FieldColumn(tables.get(o.getTable()), o.getChildren(), o.getTitle(), o.getSeq(), o.getHidden())).filter(FieldColumn::isVisible).collect(Collectors.toList());
		if (MapUtils.isNotEmpty(tableColumnMap)) {
			list.stream().forEach(o -> o.index(getFieldColumnIndex(o.getField(), tableColumnMap.get(o.getTable()))));
		}
		Collections.sort(list);
		return list;
	}

	protected void addExtraColumns(List<TableColumn> list, String... text) {
		if (null == text || text.length < 1)
			return;

		List<FieldColumn> extraColumnList = findFieldColumns(" " + StringUtils.join(text, " ")).values().stream().collect(Collectors.toList());
		if (CollectionUtils.isEmpty(extraColumnList))
			return;
		for (TableColumn tc : list) {
			List<String> columnList = extraColumnList.stream().filter(c -> c.getTable().equalsIgnoreCase(tc.getTable())).map(FieldColumn::getField).map(StringUtils::upperCase).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(columnList))
				continue;
			for (String column : columnList) {
				if (tc.getColumns().contains(column))
					continue;
				tc.getColumns().add(column);
			}
		}
	}

	protected String getWithClause(TableColumn o, Map<String, Map<String, RptSrcFileHdrColModel>> tableColumnMap) {
		final String s1 = "%1$s";
		final String s2 = "%2$s";
		final String s3 = "%3$s";
		final String s4 = "%4$s";
		StringBuilder sb = new StringBuilder("," + s1 + " as (select");

		List<String> statColumnList = informationSchemaMapper.getUserTabColNameList(parseUserTabCols(tableSchema, o.getDbname())).stream().map(StringUtils::upperCase).collect(Collectors.toList());

		final String wherePrimaryClause = "f.src_file_id = " + s2 + " and t.file_upld_id = f.id and t.latest_version_ind = 'Y'";

		Map<String, RptSrcFileHdrColModel> dataColumnMap = new LinkedHashMap<>();
		if (MapUtils.isEmpty(dataColumnMap))
			dataColumnMap.putAll(getDataColumnMap(Collections.singletonList(o.getTable())).get(o.getTable()));
		else
			dataColumnMap.putAll(tableColumnMap.get(o.getTable()));

		// columns only from file_static table
		if (o.getColumns().stream().allMatch(statColumnList::contains)) {
			sb.append(" " + s3);
			sb.append(FROM + RPT_FILE_UPLD + " f, " + o.getDbname() + " t");
			sb.append(WHERE + wherePrimaryClause);
			sb.append(")");
			return String.format(sb.toString(), o.getTable(), o.getFile(), o.getColumns().stream().map(c -> getStaticColumn(c, dataColumnMap)).collect(Collectors.joining(", ")));
		}

		// columns only from file_upload_data table
		if (o.getColumns().stream().noneMatch(statColumnList::contains)) {
			sb.append(" " + s3);
			sb.append(FROM + RPT_FILE_UPLD + " f, " + o.getDbname() + " t, " + getDynamicTable(o) + " d");
			sb.append(WHERE + wherePrimaryClause);
			sb.append(" and t.file_upld_id = d.file_upld_id");
			sb.append(" and t.line_seq = d.line_seq");
			sb.append(")");
			return String.format(sb.toString(), o.getTable(), o.getFile(), o.getColumns().stream().map(c -> getDynamicColumn(c, dataColumnMap)).collect(Collectors.joining(", ")));
		}

		// columns from both file_static and file_upload_data tables
		sb.append(" " + s3 + ", " + s4 + "");
		sb.append(FROM + RPT_FILE_UPLD + " f, " + o.getDbname() + " t, " + getDynamicTable(o) + " d");
		sb.append(WHERE + wherePrimaryClause);
		sb.append(" and t.file_upld_id = d.file_upld_id");
		sb.append(" and t.line_seq = d.line_seq");
		sb.append(")");
		return String.format(sb.toString(), o.getTable(), o.getFile()
			, o.getColumns().stream().filter(c ->  statColumnList.contains(c)).map(c -> getStaticColumn(c, dataColumnMap)).collect(Collectors.joining(", "))
			, o.getColumns().stream().filter(c -> !statColumnList.contains(c)).map(c -> getDynamicColumn(c, dataColumnMap)).collect(Collectors.joining(", "))
		);
	}

	public UserTabColsModel parseUserTabCols(String tableSchema, String tableName) {
		UserTabColsModel o = new UserTabColsModel();
		o.setTableSchema(tableSchema);
		o.setTableName(tableName);
		return o;
	}

	protected String getDynamicTable(TableColumn o) {
		return o.getDbname() + DATA_SUBFIX;
	}

	protected String getDynamicColumn(String columnName, Map<String, RptSrcFileHdrColModel> dataColumnMap) {
		String columnValue = COLUMN_NULL;
		boolean hasAlias = true;
		RptSrcFileHdrColModel col = dataColumnMap.get(columnName);
		if (null != col) {
			FieldDataType type = FieldDataType.getOrElse(col.getDataType());
			columnValue = getOrElse(col.getDataFunc(), type.getDataFunc()).replaceAll(COLUMN_DATA_FUNC_PARM_REGEX, String.format(COLUMN_DATA, columnName));
		}
		return String.format(hasAlias ? COLUMN_HAS_ALIAS : COLUMN_NOT_ALIAS, columnName, columnValue);
	}

	protected String getStaticColumn(String columnName, Map<String, RptSrcFileHdrColModel> dataColumnMap) {
		String columnValue = COLUMN_STAT;
		boolean hasAlias = false;
		RptSrcFileHdrColModel col = dataColumnMap.get(columnName);
		if (null != col) {
			FieldDataType type = FieldDataType.getOrElse(col.getDataType());
			if (StringUtils.isNotBlank(col.getDataFunc()) || !type.isEmptyDataFunc()) {
				columnValue = getOrElse(col.getDataFunc(), type.getDataFunc()).replaceAll(COLUMN_DATA_FUNC_PARM_REGEX, String.format(COLUMN_STAT, columnName));
				hasAlias = true;
			}
		}
		return String.format(hasAlias ? COLUMN_HAS_ALIAS : COLUMN_NOT_ALIAS, columnName, columnValue);
	}

	private Integer getFieldColumnIndex(String columnName, Map<String, RptSrcFileHdrColModel> dataColumnMap) {
		return MapUtils.isEmpty(dataColumnMap) || !dataColumnMap.containsKey(columnName) ? null : dataColumnMap.get(columnName).getColumnIdx();
	}

	protected String getFromClause(TableColumn o) {
		return String.format(", %s", o.getTable());
	}

	protected String getSelectClause(FieldColumn o) {
		return String.format(", %s", o.toString());
	}

	protected String getWhereClause(ReportPropsFilter o) {
		return String.format(" %s", o.getValue());
	}

	protected String getOrderClause(ReportPropsSort o) {
		return String.format(" %s", o.getValue());
	}

	protected String convertTableAlias(String text, String prefix) {
		if (StringUtils.isBlank(text))
			return "";

		StringBuilder sb = new StringBuilder();
		String s = "'";
		int n = text.indexOf(s);
		int c = 0;
		boolean o = false;
		Pattern p = Pattern.compile(TABLE_ALIAS_PATTERN);
		while (n > -1) {
			if (n == 0 || text.substring(n - 1, n).equals("\\")) {
				n = text.indexOf(s, n + s.length());
				continue;
			}
			if (o) {
				n += s.length();
				sb.append(text.substring(c, n));
				o = false;
			} else {
				sb.append(appendAliasSubfix(text.substring(c, n), p));
				o = true;
			}
			c = n;
			n = text.indexOf(s, n + s.length());
		}
		sb.append(appendAliasSubfix(text.substring(c), p));

		return String.format(" %s %s", prefix, sb.toString());
	}

	private String appendAliasSubfix(String text, Pattern pattern) {
		if (StringUtils.isBlank(text) || null == pattern)
			return "";

		StringBuilder sb = new StringBuilder();
		int n = 0;
		Matcher m = pattern.matcher(text);
		while (m.find()) {
			sb.append(text.substring(n, m.start()));
			sb.append(String.format("%s%s%s.", m.group(1), m.group(2), TABLE_ALIAS_DATA_SUBFIX));
			n = m.end();
		}
		sb.append(text.substring(n));
		return sb.toString();
	}

	protected Map<String, FieldColumn> findFieldColumns(String text) {
		if (StringUtils.isBlank(text))
			return new HashMap<>();

		Map<String, FieldColumn> map = new HashMap<>();
		String s = "'";
		int n = text.indexOf(s);
		int c = 0;
		boolean o = false;
		Pattern p = Pattern.compile(FIELD_COLUMN_PATTERN);
		while (n > -1) {
			if (n == 0 || text.substring(n - 1, n).equals("\\")) {
				n = text.indexOf(s, n + s.length());
				continue;
			} else {
				if (o) {
					n += s.length();
					o = false;
				} else {
					map.putAll(getFieldColumns(text.substring(c, n), p));
					o = true;
				}
				c = n;
				n = text.indexOf(s, n + s.length());
			}
		}
		map.putAll(getFieldColumns(text.substring(c), p));

		return map;
	}

	private Map<String, FieldColumn> getFieldColumns(String text, Pattern pattern) {
		if (StringUtils.isBlank(text) || null == pattern)
			return new HashMap<>();

		Map<String, FieldColumn> map = new HashMap<>();
		Matcher m = pattern.matcher(text);
		while (m.find()) {
			map.put(String.format("%s.%s", m.group(2), m.group(3)), new FieldColumn(m.group(2), m.group(3)));
		}

		return map;
	}

	private Map<String, Map<String, RptSrcFileHdrColModel>> getDataColumnMap(List<String> tables) {
		if (CollectionUtils.isEmpty(tables))
			return new LinkedHashMap<>();

		Map<String, Map<String, RptSrcFileHdrColModel>> map = new LinkedHashMap<>();
		for (String table : tables) {
			List<RptSrcFileHdrModel> dataHeaderList = srcFileMapper.getSrcFileHeaderList(table);
			if (CollectionUtils.isEmpty(dataHeaderList)) {
				map.put(table, new HashMap<>());
				continue;
			}
			RptSrcFileHdrColModel filter = new RptSrcFileHdrColModel();
			filter.setSrcFileHdrId(dataHeaderList.get(0).getId());
			map.put(table, srcFileMapper.getSrcFileColumnList(filter).stream().collect(Collectors.toMap(c -> c.getColumnName().toUpperCase(), Function.identity())));
		}
		return map;
	}

	private static String getOrElse(String value, String valueIfNull) {
		return StringUtils.isBlank(value) ? valueIfNull : value;
	}

	private class TableColumn {
		private String table;
		private String dbname;
		private Integer file;
		private List<String> columns;
		public TableColumn(String table, String dbname, Integer file, List<String> columns) {
			this.table = table;
			this.dbname = dbname;
			this.file = file;
			this.columns = columns;
		}
		public String getTable() {
			return table;
		}
		public String getDbname() {
			return dbname;
		}
		public Integer getFile() {
			return file;
		}
		public List<String> getColumns() {
			return columns;
		}
	}

	private class FieldColumn implements Comparable<FieldColumn> {
		private String table;
		private String field;
		private String title;
		private Integer seq;
		private Integer index;
		private boolean hidden = false;
		public FieldColumn(String table, String field) {
			this(table, field, null, null, "Y");
		}
		public FieldColumn(String table, String field, String title, Integer seq, String hidden) {
			this.table = getOrElse(table, "").toUpperCase();
			this.field = getOrElse(field, "").toUpperCase();
			this.title = title;
			this.seq = seq;
			this.hidden = BooleanUtils.toBoolean(hidden);
		}
		public FieldColumn index(Integer index) {
			this.index = index;
			return this;
		}
		public String getTable() {
			return table;
		}
		public String getField() {
			return field;
		}
		public String getTitle() {
			return title;
		}
		public Integer getSeq() {
			return seq;
		}
		public Integer getIndex() {
			return index;
		}
		public boolean isVisible() {
			return !hidden;
		}
		public int compareTo(FieldColumn o) {
			int c = 0;
			// check display sequence
			if (null == getSeq() && null == o.getSeq())
				c = 0;
			else if (null != getSeq() && null != o.getSeq())
				c = getSeq().compareTo(o.getSeq());
			else
				c = null == getSeq() ? 1 : -1;
			if (c != 0)
				return c;
			// check table name
			if (StringUtils.isBlank(getTable()) && StringUtils.isBlank(o.getTable()))
				c = 0;
			else if (StringUtils.isNotBlank(getTable()) && StringUtils.isNotBlank(o.getTable()))
				c = getTable().compareTo(o.getTable());
			else
				c = StringUtils.isNotBlank(getTable()) ? 1 : -1;
			if (c != 0)
				return c;
			// check column index
			if (null == getIndex() && null == o.getIndex())
				c = 0;
			else if (null != getIndex() && null != o.getIndex())
				c = getIndex().compareTo(o.getIndex());
			else
				c = null == getIndex() ? 1 : -1;
			// return result
			return c;
		}
		public String toString() {
			return String.format("%1$s.%2$s" + (StringUtils.isBlank(title) ? "" : " `%3$s`"), getTable(), getField(), getTitle());
		}
	}

	public enum FieldDataType {
		VARCHAR,
		INT,
		DATE(getDataFuncDateFormat(COLUMN_DATA_FUNC_FORMAT_DATE)),
		TIME(getDataFuncDateFormat(COLUMN_DATA_FUNC_FORMAT_TIME)),
		DATETIME(getDataFuncDateFormat(COLUMN_DATA_FUNC_FORMAT_DATETIME)),
		;
		private String dataFunc;
		private FieldDataType() {
			this.dataFunc = getDataFuncDefault();
		}
		private FieldDataType(String dataFunc) {
			this.dataFunc = dataFunc;
		}
		public String getDataFunc() {
			return null == dataFunc ? getDataFuncDefault() : dataFunc;
		}
		public boolean isEmptyDataFunc() {
			return null == dataFunc || getDataFuncDefault().equals(dataFunc);
		}
		public static FieldDataType getOrElse(String text) {
			if (StringUtils.isBlank(text))
				return VARCHAR;

			try {
				return FieldDataType.valueOf(text.toUpperCase());
			} catch (IllegalArgumentException e) {
				return VARCHAR;
			}
		}
		public static String getDataFuncDefault() {
			return COLUMN_DATA_FUNC_PARM_VALUE;
		}
		public static String getDataFuncDateFormat(String format) {
			return String.format("date_format(" + COLUMN_DATA_FUNC_PARM_VALUE + ", '%s')", format);
		}
	}
}
