package com.dbs.module.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractSearchModel extends AbstractUserModel implements SearchForm {

	@JsonInclude
	private int page = 1;

	@JsonInclude
	private int pageSize = 10;

	@JsonInclude(Include.NON_NULL)
	private String defaultSearchText;
}
