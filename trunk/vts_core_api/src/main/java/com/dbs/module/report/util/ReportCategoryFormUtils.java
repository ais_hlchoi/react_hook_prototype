package com.dbs.module.report.util;

import com.dbs.module.report.ReportCategoryForm;
import com.dbs.module.report.SavedSearchForm;
import com.dbs.module.report.model.ReportCategoryFormModel;

public class ReportCategoryFormUtils {

	private ReportCategoryFormUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportCategoryForm parse(final SavedSearchForm model) {
		ReportCategoryFormModel o = new ReportCategoryFormModel();
		o.setName(model.getName());
		o.setDefaultSearchText(model.getDefaultSearchText());
		o.setGenerateOpt(model.getGenerateOpt());
		return o;
	}
}
