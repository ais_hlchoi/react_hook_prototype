package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsColumn;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputColumnModel implements ReportPropsColumn {

	@JsonInclude
	private Integer value;

	@JsonInclude
	private String children;

	@JsonInclude(Include.NON_NULL)
	private String title;

	@JsonInclude(Include.NON_NULL)
	private Integer seq;

	@JsonInclude(Include.NON_NULL)
	private String hidden;

	@JsonInclude(Include.NON_NULL)
	private Integer table;

	@JsonInclude(Include.NON_NULL)
	private Integer head;
}
