package com.dbs.module.report.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportAccessGroupObjectModel;
import com.dbs.module.report.util.VtspUserReportAccessGroupUtils;
import com.dbs.module.util.AbstractModuleData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ReportAccessGroupObjectData extends AbstractModuleData {

	private ReportAccessGroupObjectModel data;

	public ReportAccessGroupObjectData(ReportAccessGroupObjectModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupObjectModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupObjectModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return null == getData().getUserReportAccessGroup() ? null: String.format(getData().getUserReportAccessGroup().getName(), getData().getRptTmplName());
	}

	public String getType() {
		return null == getData().getUserReportAccessGroup() ? null: getData().getUserReportAccessGroup().getType();
	}

	public List<ReportAccessGroupTemplateNameData> getReportList() {
		return null == getData().getReportTemplate() ? new ArrayList<>() : getData().getReportTemplate().stream().map(ReportAccessGroupTemplateNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupUserNameData> getUserList() {
		return null == getData().getUser() ? new ArrayList<>() : getData().getUser().stream().map(ReportAccessGroupUserNameData::new).collect(Collectors.toList());
	}

	public List<ReportAccessGroupRoleNameData> getRoleList() {
		return null == getData().getRole() ? new ArrayList<>() : getData().getRole().stream().map(ReportAccessGroupRoleNameData::new).collect(Collectors.toList());
	}

	@JsonInclude(Include.NON_NULL)
	public String getActiveInd() {
		return null == getData().getUserReportAccessGroup() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getUserReportAccessGroup().getActiveInd()));
	}

	@JsonInclude(Include.NON_NULL)
	public Long[] getLastUpdatedTime() {
		return null == getData().getUserReportAccessGroup() ? null : formatTimeNano(getData().getUserReportAccessGroup().getLastUpdatedDate());
	}

	public Map<String, Boolean> getRestrictedFields() {
		boolean isRestricted = VtspUserReportAccessGroupUtils.isPresetUserReportAccessGroupName(null == getData().getUserReportAccessGroup() ? null : getData().getUserReportAccessGroup().getName());
		Map<String, Boolean> map = new HashMap<>();
		map.put("name", isRestricted);
		map.put("report", false);
		map.put("user", isRestricted);
		map.put("role", false);
		return map;
	}
}
