package com.dbs.module.report.model;

import com.dbs.module.model.AbstractUserModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupTemplateModel extends AbstractUserModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private String name;
}
