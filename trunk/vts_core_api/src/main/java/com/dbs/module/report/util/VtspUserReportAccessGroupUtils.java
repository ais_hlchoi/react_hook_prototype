package com.dbs.module.report.util;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.dbs.module.report.ReportAccessGroup;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.constant.ReportAccessGroupType;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.Assert;
import com.dbs.vtsp.model.RptSetupAcsgCatgModel;
import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.dbs.vtsp.model.RptSetupAcsgRoleModel;
import com.dbs.vtsp.model.RptSetupAcsgTmplModel;
import com.dbs.vtsp.model.RptSetupAcsgUserModel;

public class VtspUserReportAccessGroupUtils extends AbstractModuleUtils {

	private VtspUserReportAccessGroupUtils() {
		// for SonarQube scanning purpose
	}

	private enum CategoryType {
		  PUBLIC
		, INHERIT
	}

	public static final String CATEGORY_PUBLIC = CategoryType.PUBLIC.name();
	public static final String CATEGORY_INHERIT = CategoryType.INHERIT.name();

	public static final String CATEGORY_INHERIT_SUBFIX = "_%s";
	public static final String CATEGORY_INHERIT_PATTERN = "\\d+" + CATEGORY_INHERIT_SUBFIX;

	public static String getDefaultUserReportAccessGroupName(final String name) {
		return StringUtils.isBlank(name) ? CATEGORY_PUBLIC : name;
	}

	public static String getUserReportAccessGroupName(final String name, final int id) {
		return CATEGORY_INHERIT.equals(name) ? id + CATEGORY_INHERIT_SUBFIX : getDefaultUserReportAccessGroupName(name);
	}

	public static boolean isPresetUserReportAccessGroupName(final String name) {
		return StringUtils.isNotBlank(name) && (name.matches(CATEGORY_INHERIT_PATTERN) || Stream.of(CategoryType.values()).anyMatch(o -> o.name().equals(name)));
	}

	public static RptSetupAcsgModel parse(final ReportAccessGroup model) {
		return parse(model, (ReportAccessGroupType) null);
	}

	public static RptSetupAcsgModel parse(final ReportAccessGroup model, final ReportAccessGroupType type) {
		RptSetupAcsgModel o = new RptSetupAcsgModel();
		o.setName(model.getName());
		o.setType((null == type ? ReportAccessGroupType.getOrElse(o.getName()) : type).name());
		o.setActiveInd(Indicator.Y.name());
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupAcsgModel parse(final ReportAccessGroup model, final RptSetupAcsgModel source) {
		if (null == source)
			return source;
		RptSetupAcsgModel o = new RptSetupAcsgModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setName(model.getName());
		o.setType(source.getType()); // not suggest to change this value
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupAcsgTmplModel parseGroupTemplate(final ReportAccessGroup model) {
		RptSetupAcsgTmplModel o = new RptSetupAcsgTmplModel();
		o.setRptAcsgId(model.getId());
		o.setRptTmplId(model.getRptTmplId());
		return o;
	}

	public static RptSetupAcsgCatgModel parseGroupCategory(final ReportAccessGroup model) {
		RptSetupAcsgCatgModel o = new RptSetupAcsgCatgModel();
		o.setRptAcsgId(model.getId());
		o.setRptCatgId(model.getRptCatgId());
		return o;
	}

	public static RptSetupAcsgUserModel parseGroupUser(final ReportAccessGroup model) {
		RptSetupAcsgUserModel o = new RptSetupAcsgUserModel();
		o.setRptAcsgId(model.getId());
		o.setUserId(model.getUserId());
		return o;
	}

	public static RptSetupAcsgRoleModel parseGroupRole(final ReportAccessGroup model) {
		RptSetupAcsgRoleModel o = new RptSetupAcsgRoleModel();
		o.setRptAcsgId(model.getId());
		o.setRoleId(model.getRoleId());
		return o;
	}

	public static RptSetupAcsgModel parse(final String name) {
		return parse(ReportAccessGroupUtils.parse(name));
	}

	public static RptSetupAcsgTmplModel parseGroupTemplate(final Integer rptTmplId, final Integer id) {
		return parseGroupTemplate(ReportAccessGroupUtils.parseGroupTemplate(rptTmplId, id));
	}

	public static RptSetupAcsgCatgModel parseGroupCategory(final Integer rptCatgId, final Integer id) {
		return parseGroupCategory(ReportAccessGroupUtils.parseGroupCategory(rptCatgId, id));
	}

	public static RptSetupAcsgUserModel parseGroupUser(final String userId, final Integer id) {
		return parseGroupUser(ReportAccessGroupUtils.parseGroupUser(userId, id));
	}

	public static RptSetupAcsgRoleModel parseGroupRole(final Integer roleId, final Integer id) {
		return parseGroupRole(ReportAccessGroupUtils.parseGroupRole(roleId, id));
	}
}
