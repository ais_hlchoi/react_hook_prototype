package com.dbs.module.report.data;

import org.springframework.util.Assert;

import com.dbs.module.report.model.ReportAccessGroupCountModel;
import com.dbs.module.util.AbstractModuleData;

public class SavedSearchAccessGroupNameData extends AbstractModuleData {

	private ReportAccessGroupCountModel data;

	public SavedSearchAccessGroupNameData(ReportAccessGroupCountModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportAccessGroupCountModel getData() {
		return data;
	}

	protected void setData(ReportAccessGroupCountModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return String.format(getData().getUserReportAccessGroup().getName(), getData().getRptTmplName());
	}
}
