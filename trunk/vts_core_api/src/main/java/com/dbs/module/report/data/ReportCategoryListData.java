package com.dbs.module.report.data;

import java.sql.Timestamp;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportAccessGroupRoleModel;
import com.dbs.module.report.model.ReportCategoryCountModel;
import com.dbs.module.util.AbstractModuleData;

public class ReportCategoryListData extends AbstractModuleData {

	private ReportCategoryCountModel data;

	public ReportCategoryListData(ReportCategoryCountModel data) {
		Assert.notNull(data, "Data cannot be null");
		this.data = data;
	}

	protected ReportCategoryCountModel getData() {
		return data;
	}

	protected void setData(ReportCategoryCountModel data) {
		this.data = data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getName() {
		return getData().getUserReportCategory().getName();
	}

	public String getDescription() {
		return getData().getUserReportCategory().getDescription();
	}

	public String getGenerateOpt() {
		return getData().getUserReportCategory().getGenerateOpt();
	}

	public String getCreatedBy() {
		return getData().getUserReportCategory().getCreatedBy();
	}

	public Timestamp getCreatedDate() {
		return getData().getUserReportCategory().getCreatedDate();
	}

	public String getLastUpdatedBy() {
		return getData().getUserReportCategory().getLastUpdatedBy();
	}

	public Timestamp getLastUpdatedDate() {
		return getData().getUserReportCategory().getLastUpdatedDate();
	}

	public String getRoleList() {
		return null == getData().getRoleList() ? "" : getData().getRoleList().stream().map(ReportAccessGroupRoleModel::getName).collect(Collectors.joining(", "));
	}
	
	public Integer getReportCount() {
		return getOrElse(getData().getRptTmplCnt(), 0);
	}

	public String getActiveInd() {
		return null == getData().getUserReportCategory() ? Indicator.N.name() : Indicator.toString(Indicator.checkIfTrue(getData().getUserReportCategory().getActiveInd()));
	}
}
