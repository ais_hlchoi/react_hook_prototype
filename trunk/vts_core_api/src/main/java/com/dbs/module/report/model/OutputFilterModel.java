package com.dbs.module.report.model;

import com.dbs.module.report.ReportPropsFilter;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputFilterModel implements ReportPropsFilter {

	@JsonInclude
	private Integer seq;

	@JsonInclude
	private String value;
}
