package com.dbs.module.report;

import java.util.List;

public interface DataColOutput {

	List<String> getErrors();

	String getError();

	int getErrorCount();

	boolean hasError();

	void build(String[] column);

	String[] apply(String[] line);
}
