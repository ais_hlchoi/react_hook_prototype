package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractSearchModel;
import com.dbs.module.report.ReportForm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportFormModel extends AbstractSearchModel implements ReportForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String srcName;

	@JsonInclude
	private String tableName;

	@JsonInclude
	private String columnName;

	@JsonInclude
	private List<String> srcNameList;

	@JsonInclude
	private List<String> tableNameList;

	@JsonInclude
	private List<String> columnNameList;
}
