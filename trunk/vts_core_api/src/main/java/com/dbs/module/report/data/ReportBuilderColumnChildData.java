package com.dbs.module.report.data;

import org.apache.commons.lang3.BooleanUtils;

import com.dbs.module.report.ReportBuilderColumnChild;

public class ReportBuilderColumnChildData {

	private ReportBuilderColumnChild data;

	public ReportBuilderColumnChildData(ReportBuilderColumnChild data) {
		this.data = data;
	}

	protected ReportBuilderColumnChild getData() {
		return data;
	}

	public Integer getId() {
		return getData().getId();
	}

	public String getLabel() {
		return getData().getLabel();
	}

	public String getValue() {
		return getData().getValue();
	}

	public Integer getIndex() {
		return getData().getIndex();
	}

	public Integer getTable() {
		return getData().getTable();
	}

	public Integer getHead() {
		return getData().getHead();
	}

	public boolean isStatic() {
		return BooleanUtils.toBoolean(getData().getStaticInd());
	}
}
