package com.dbs.module.report;

import java.util.List;

import com.dbs.module.model.SearchForm;

public interface ReportForm extends SearchForm {

	String getSrcName();

	String getTableName();

	String getColumnName();

	List<String> getSrcNameList();

	List<String> getTableNameList();

	List<String> getColumnNameList();
}
