package com.dbs.module.report;

import java.util.List;

public interface ReportPropsOption {

	List<ReportPropsCriteria> getCriteria();

	List<ReportPropsParameter> getParameter();

	List<String> getOutput();
}
