package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dbs.module.report.ReportPropsComparator;
import com.dbs.module.report.ReportPropsParameter;
import com.dbs.module.report.ReportParam;
import com.dbs.module.report.constant.CompareOperator;
import com.dbs.module.report.constant.Datatype;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.data.ReportParamData;
import com.dbs.module.report.model.OutputComparatorModel;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.util.AbstractModuleData;
import com.dbs.module.util.AbstractModuleUtils;

@Component
public class ReportParamUtils {

	public static final String PARAM_NAME_PATTERN_DEFAULT = "(\\w+)(:([\\w\\s]+))?->1:name;3:label";

	protected static String PARAM_NAME_PATTERN_REPORT_TEMPLATE = "(\\w+)->1:name";
	protected static String PARAM_NAME_PATTERN_USER_DEFINED = PARAM_NAME_PATTERN_DEFAULT;

	@Value("${user.report.template.user-defined-param-name}")
	private void setUserDefinedParamName(String userDefinedParamName) {
		ReportParamUtils.PARAM_NAME_PATTERN_USER_DEFINED = userDefinedParamName;
	}

	public static List<ReportParamData> getParamMap(final String sql) {
		return getParamMap(sql, null, null);
	}

	public static List<ReportParamData> getParamMap(final String sql, final Map<String, List<ReportParamModel>> paramOptionMap) {
		return getParamMap(sql, null, paramOptionMap);
	}

	public static List<ReportParamData> getParamMap(final String sql, final String paramNamePattern, final Map<String, List<ReportParamModel>> paramOptionMap) {
		if (StringUtils.isBlank(sql))
			return new ArrayList<>();

		List<ReportParamData> list = new ArrayList<>();
		// direct query statement  -> [ #{name:label} ]
		list.addAll(ParamUtils.parseParamName(sql, PARAM_NAME_PATTERN_DEFAULT, "\\#", ParamUtils.toMap("datatype:text"), paramOptionMap));
		// user defined parameters -> [ ${name:label:datatype[format]} ]
		list.addAll(ParamUtils.parseParamName(sql, ParamUtils.getOrElse(paramNamePattern, PARAM_NAME_PATTERN_USER_DEFINED), "\\$", paramOptionMap));
		ParamUtils.sort(list);
		return list;
	}

	public static ReportPropsComparator getComparatorEquals() {
		OutputComparatorModel o = new OutputComparatorModel();
		o.setValue(CompareOperator.EQUALS.name());
		o.setChildren(CompareOperator.EQUALS.label());
		return o;
	}

	public static String getMandatory() {
		return Indicator.Y.name();
	}

	public static String parse(String sql, List<? extends ReportPropsParameter> list) {
		if (StringUtils.isBlank(sql) || CollectionUtils.isEmpty(list))
			return sql;

		Map<String, String> map = new LinkedHashMap<>();
		map.put("#", "\\#");
		map.put("$", "\\$");
		map.put("&", "\\&");

		for (ReportPropsParameter o : list) {
			sql = ParamUtils.replace(sql, o, map);
		}
		return sql;
	}

	public static List<String> parseFilterAdvance(List<? extends ReportPropsParameter> list) {
		return CollectionUtils.isEmpty(list) ? new ArrayList<>() : list.stream().map(ParamUtils::parseFilterAdvanceParam).filter(StringUtils::isNotBlank).collect(Collectors.toList());
	}
}

class UserDefinedParamNameProps {
	private String prefix;
	private String pattern;
	private Map<Integer, String> group = new LinkedHashMap<>();

	public UserDefinedParamNameProps(String prefix, String pattern, Map<Integer, String> group) {
		this.prefix = prefix;
		this.pattern = pattern;
		this.group = group;
	}
	public String getPattern() {
		return StringUtils.isBlank(pattern) ? "" : String.format("%s\\{%s\\}", prefix, pattern);
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public Map<Integer, String> getGroup() {
		return group;
	}
	public void setGroup(Map<Integer, String> group) {
		this.group = group;
	}
}

class ParamProps extends AbstractModuleData implements ReportParam {
	private String text;
	private Integer seq;
	private final Map<String, String> map;
	private final Map<String, String> attrMap = new LinkedHashMap<>();
	private final String[] excludes = new String[] { "name", "datatype", "label" };

	public ParamProps(String text, Integer seq, Map<String, String> map) {
		this.text = text;
		this.seq = seq;
		this.map = null == map ? new LinkedHashMap<>() : map;
	}
	public ParamProps put(String key, String value) {
		attrMap.put(key, value);
		return this;
	}
	public String getName() {
		String key = "name";
		return attrMap.get(key);
	}
	public String getDatatype() {
		String key = "datatype";
		return getOrElse(attrMap.get(key), map.get(key));
	}
	public String getLabel() {
		String key = "label";
		return getOrElse(attrMap.get(key), getName());
	}
	public String getText() {
		return text;
	}
	public Integer getSeq() {
		return seq;
	}
	public Map<String, String> getAttributes() {
		return attrMap.entrySet().stream().filter(o -> !Arrays.asList(excludes).contains(o.getKey())).collect(Collectors.toMap(Entry::getKey, o -> getOrElse(o.getValue(), map.get(o.getKey()))));
	}
}

class ParamUtils extends AbstractModuleUtils {

	private static final Logger logger = LoggerFactory.getLogger(ReportParamUtils.class);

	private static final String PARAM_NAME_PATTERN_SEPARATOR = "->";

	protected static UserDefinedParamNameProps parseUserDefinedParamName(final String text, final String prefix) {
		if (StringUtils.isBlank(text))
			return null;

		int index = text.lastIndexOf(PARAM_NAME_PATTERN_SEPARATOR);
		if (index < 1)
			return null;

		String pattern = text.substring(0, index);
		String attribute = text.substring(index + PARAM_NAME_PATTERN_SEPARATOR.length());
		return new UserDefinedParamNameProps(prefix, pattern, Stream.of(attribute.split(";")).map(o -> o.split(":")).filter(o -> NumberUtils.isCreatable(o[0])).collect(Collectors.toMap(o -> Integer.parseInt(o[0]), o -> o[1])));
	}

	public static List<ReportParamData> parseParamName(final String sql, final String paramNamePattern, final String prefix, final Map<String, List<ReportParamModel>> paramOptionMap) {
		return parseParamName(sql, paramNamePattern, prefix, null, paramOptionMap);
	}

	protected static List<ReportParamData> parseParamName(final String sql, final String paramNamePattern, final String prefix, final Map<String, String> map, final Map<String, List<ReportParamModel>> paramOptionMap) {
		List<ReportParamData> list = new ArrayList<>();
		UserDefinedParamNameProps props = parseUserDefinedParamName(paramNamePattern, prefix);
		if (null == props || MapUtils.isEmpty(props.getGroup()))
			return list;
		List<String> keys = new ArrayList<>();
		Pattern pattern = Pattern.compile(props.getPattern());
		Matcher matcher = pattern.matcher(sql);
		while (matcher.find()) {
			String key = matcher.group(0);
			if (keys.contains(key))
				continue;
			list.add(new ReportParamData(parseParam(key, matcher.start(), props.getGroup().entrySet().stream().map(o -> new String[] { o.getValue(), matcher.group(o.getKey()) }).filter(o -> o.length > 1).collect(Collectors.toList()), map, paramOptionMap)));
			keys.add(key);
		}
		return list;
	}

	protected static ReportParam parseParam(final String text, final int seq, final List<String[]> list, final Map<String, String> map, final Map<String, List<ReportParamModel>> paramOptionMap) {
		ParamProps o = new ParamProps(text, seq, map);
		list.stream().forEach(f -> o.put(f[0], f[1]));
		// special handling when Datatype.ARRAY
		if (Datatype.ARRAY == Datatype.getOrElse(o.getDatatype())) {
			Matcher matcher = Pattern.compile(Datatype.ARRAY.pattern()).matcher(o.getDatatype());
			String option = matcher.find() ? matcher.group(1) : "Y|N";
			o.put("datatype", Datatype.ARRAY.value());
			if (option.matches(":\\w+:")) {
				String segment = option.substring(1, option.length() - 1);
				o.put("segment", segment);
				if (MapUtils.isNotEmpty(paramOptionMap) && paramOptionMap.containsKey(segment) && null != paramOptionMap.get(segment)) {
					option = paramOptionMap.get(segment).stream().map(p -> String.format("%s:%s", p.getValue(), p.getLabel())).collect(Collectors.joining("|"));
				} else {
					option = null;
				}
			}
			if (StringUtils.isNotBlank(option)) {
				o.put("option", option);
			}
		}
		return o;
	}

	protected static void sort(final List<ReportParamData> list) {
		if (CollectionUtils.isEmpty(list))
			return;

		Collections.sort(list, new java.util.Comparator<ReportParamData>() {
			public int compare(ReportParamData a, ReportParamData b) {
                return a.getSeq().compareTo(b.getSeq());
            }
		});
	}

	protected static Map<String, String> toMap(final String text) {
		if (StringUtils.isBlank(text))
			return new LinkedHashMap<>();

		List<String> list = Arrays.asList(text.split(";"));
		return list.stream().map(o -> o.split(":")).filter(o -> o.length == 2).collect(Collectors.toMap(o -> o[0], o -> o[1]));
	}

	public static String replace(final String sql, final ReportPropsParameter param, final Map<String, String> map) {
		if (StringUtils.isBlank(sql) || null == param)
			return sql;

		String str = null;
		try {
			str = sql.replaceAll(getSearch(param.getText(), map), getReplacement(param.getValue(), param.getDatatype()));
			if (!sql.equals(str)) {
				logger.info(String.format("%n-+ %s%n-> %s", sql.replaceAll("\\n", "\\s"), str.replaceAll("\\n", "\\s")));
				logger.info(StringUtils.repeat("-", 20));
			}
		} catch (Exception e) {
			logger.error(String.format("Error found when replace param [ %s ] <- [ %s ]", param.getText(), sql));
			throw e;
		}
		return str;
	}

	protected static String getSearch(final String text, final Map<String, String> map) {
		if (StringUtils.isBlank(text) || MapUtils.isEmpty(map))
			return text;

		String key = text.substring(0, 1);
		String str = null;
		if (map.containsKey(key) && text.length() > 3 && text.matches("^.\\{.*\\}$")) {
			str = text.substring(2, text.length() - 1);
			// check if Datatype.ARRAY (since its pattern likes [...|...])
			String open = "[";
			String close = "]";
			int a = str.indexOf(open);
			int b = str.lastIndexOf(close);
			if (a > 0) {
				str = a < b ? str.substring(0, a) + "\\[[^\\[\\]]+\\]" + str.substring(b + close.length(), str.length()) : null;
			}
		}
		return StringUtils.isBlank(str) ? text : String.format("%s\\{%s\\}", getOrElse(map.get(key), key), str);
	}

	protected static String getReplacement(final String value, final String datatype) {
		if (StringUtils.isBlank(value))
			return "null";

		switch (Datatype.getOrElse(datatype)) {
		case STRING:
			return String.format("'%s'", value);
		case DATE:
			return String.format("'%s'", value);
		case ARRAY:
			return String.format("'%s'", value);
		default:
			return value;
		}
	}

	public static String parseFilterAdvanceParam(final ReportPropsParameter param) {
		if (null == param || StringUtils.isBlank(param.getText()))
			return null;

		if (!param.getText().matches("^\\&\\{.*\\}$"))
			return null;

		String[] arr = param.getText().substring(2, param.getText().length() - 1).split("\\.");
		if (arr.length < 2) {
			logger.info(String.format("Invalid paramName pattern [ %s ]", param.getText()));
			return null;
		}

		return String.format("and %s.%s %s", arr[arr.length - 2], arr[arr.length - 1], StringUtils.isBlank(param.getValue()) ? "is null" : String.format("= '%s'", param.getValue().replaceAll("'", "''")));
	}

	public static boolean isFoundInGetMethod(final String name, final Class<?> cls) {
		return isFoundInClassMethod("get", name, cls);
	}

	public static boolean isFoundInSetMethod(final String name, final Class<?> cls) {
		return isFoundInClassMethod("set", name, cls);
	}

	protected static boolean isFoundInClassMethod(final String prefix, final String name, final Class<?> cls) {
		return Stream.of(cls.getMethods()).anyMatch(o -> o.getName().equals(String.format("%s%s", prefix, getInitCap(name))));
	}

	public static String getInitCap(final String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	protected static <T> T getOrElse(T value, T valueIfNull) {
		return AbstractModuleUtils.getOrElse(value, valueIfNull);
	}
}
