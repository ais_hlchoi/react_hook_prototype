package com.dbs.module.report;

import java.util.List;

public interface ReportBuilderTable<T extends ReportBuilderTableChild> extends ReportBuilderOption {

	List<T> getTables();
}
