package com.dbs.module.report.constant;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum CompareOperator {

	  EQUALS("eq", "=")
	, NOT_EQUALS("ne", "!=")
	, EMPTY()
	, NOT_EMPTY()
	, BLANK()
	, NOT_BLANK()
	, CONTAINS()
	, NOT_CONTAINS()
	, GREATER_EQUALS("ge", ">=")
	, GREATER_THAN("gt", ">")
	, LESS_EQUALS("le", "<=")
	, LESS_THAN("lt", "<")
	;

	private String abbr;
	private String label;

	private CompareOperator(String abbr, String label) {
		this.abbr = abbr;
		this.label = label;
	}

	private CompareOperator() {
		this(null, null);
	}

	public String abbr() {
		return abbr;
	}

	public String label() {
		return label;
	}

	public static CompareOperator getOrElse(String text) {
		return getOrElse(text, EQUALS);
	}

	public static CompareOperator getOrElse(String text, CompareOperator o) {
		if (null == o)
			o = EQUALS;

		if (StringUtils.isBlank(text))
			return o;

		try {
			if (text.length() == 2)
				return Stream.of(CompareOperator.values()).filter(c -> text.equalsIgnoreCase(c.abbr())).findFirst().orElse(o);
			else
				return CompareOperator.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			return o;
		}
	}
}
