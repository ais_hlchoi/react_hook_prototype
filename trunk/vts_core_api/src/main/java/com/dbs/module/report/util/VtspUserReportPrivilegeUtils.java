package com.dbs.module.report.util;

import com.dbs.module.report.constant.Indicator;
import com.dbs.vtsp.model.RptSetupTmplPrivsModel;

public class VtspUserReportPrivilegeUtils {

	private VtspUserReportPrivilegeUtils() {
		// for SonarQube scanning purpose
	}

	public static RptSetupTmplPrivsModel getOwnerInstance(Integer rptTmplId) {
		RptSetupTmplPrivsModel o = new RptSetupTmplPrivsModel();
		o.setRptTmplId(rptTmplId);
		o.setOwnerInd(Indicator.Y.name());
		o.setAdminInd(Indicator.N.name());
		o.setCanSelect(Indicator.Y.name());
		o.setCanUpdate(Indicator.Y.name());
		o.setCanDelete(Indicator.Y.name());
		o.setCanExport(Indicator.Y.name());
		return o;
	}

	public static RptSetupTmplPrivsModel getAdminInstance(Integer rptTmplId) {
		RptSetupTmplPrivsModel o = new RptSetupTmplPrivsModel();
		o.setRptTmplId(rptTmplId);
		o.setOwnerInd(Indicator.N.name());
		o.setAdminInd(Indicator.Y.name());
		o.setCanSelect(Indicator.Y.name());
		o.setCanUpdate(Indicator.Y.name());
		o.setCanDelete(Indicator.Y.name());
		o.setCanExport(Indicator.Y.name());
		return o;
	}

	public static RptSetupTmplPrivsModel getUserInstance(Integer rptTmplId) {
		RptSetupTmplPrivsModel o = new RptSetupTmplPrivsModel();
		o.setRptTmplId(rptTmplId);
		o.setOwnerInd(Indicator.N.name());
		o.setAdminInd(Indicator.N.name());
		o.setCanSelect(Indicator.Y.name());
		o.setCanUpdate(Indicator.N.name());
		o.setCanDelete(Indicator.N.name());
		o.setCanExport(Indicator.Y.name());
		return o;
	}
}
