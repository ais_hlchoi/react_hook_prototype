package com.dbs.module.report.model;

import java.util.List;

import com.dbs.vtsp.model.RptSetupAcsgModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupObjectModel {

	@JsonInclude
	private Integer id;

	@JsonInclude
	private RptSetupAcsgModel userReportAccessGroup;

	@JsonInclude
	private List<ReportAccessGroupTemplateModel> reportTemplate;

	@JsonInclude
	private List<ReportAccessGroupUserModel> user;

	@JsonInclude
	private List<ReportAccessGroupRoleModel> role;

	@JsonInclude
	private String rptTmplName;
}
