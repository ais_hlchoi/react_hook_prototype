package com.dbs.module.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dbs.module.download.model.ReportDownloadObjectModel;
import com.dbs.module.report.ReportPropsParameter;
import com.dbs.module.report.ReportTemplate;
import com.dbs.module.report.model.OutputFilterModel;
import com.dbs.module.report.model.QueryAdvanceModel;
import com.dbs.module.report.model.QueryBasicModel;
import com.dbs.module.report.model.ReportOptionModel;
import com.dbs.module.report.model.ReportParamModel;
import com.dbs.module.report.model.ReportTemplateModel;
import com.dbs.module.report.model.ReportTemplateObjectModel;
import com.dbs.vtsp.model.RptSetupTmplCriteModel;
import com.dbs.vtsp.model.RptSetupTmplModel;

public class ReportTemplateUtils {

	private ReportTemplateUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportTemplate parse(final ReportTemplateObjectModel model) {
		return parse(model, null, null);
	}

	public static ReportTemplate parse(final ReportTemplateObjectModel model, final List<? extends ReportPropsParameter> list, DynQuerySqlUtils DynQuerySqlUtils) {
		ReportTemplateModel o = new ReportTemplateModel();
		o.setId(model.getId());
		o.setBasic(parseBasic(model));
		o.setAdvance(parseAdvance(model));
		o.setOption(parseOption(model));

		if (null == model.getUserReportTemplate())
			return o;

		o.setName(model.getUserReportTemplate().getName());
		o.setActiveInd(model.getUserReportTemplate().getActiveInd());

		ReportParamUtils.parseFilterAdvance(list).stream().forEach(c -> o.getBasic().getWhere().add(ReportTemplateUtils.parseFilter(c)));

		if (StringUtils.isBlank(o.getAdvance().getSql()) && null != DynQuerySqlUtils) {
			o.getAdvance().setSql(DynQuerySqlUtils.getSql(o));
		}
		return o;
	}

	protected static OutputFilterModel parseFilter(final String value) {
		OutputFilterModel o = new OutputFilterModel();
		o.setValue(value);
		return o;
	}

	public static QueryBasicModel parseBasic(final ReportTemplateObjectModel model) {
		QueryBasicModel o = new QueryBasicModel();
		if (null == model)
			return o;

		o.setFile(ReportPropsUtils.parse(model.getFile()));
		o.setSelect(CollectionUtils.isEmpty(model.getColumn()) ? new ArrayList<>() : model.getColumn().stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));
		o.setFrom(CollectionUtils.isEmpty(model.getTable()) ? new ArrayList<>() : model.getTable().stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));
		o.setWhere(CollectionUtils.isEmpty(model.getFilter()) ? new ArrayList<>() : model.getFilter().stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));
		o.setOrder(CollectionUtils.isEmpty(model.getSort()) ? new ArrayList<>() : model.getSort().stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));
		return o;
	}

	protected static QueryAdvanceModel parseAdvance(final ReportTemplateObjectModel model) {
		QueryAdvanceModel o = new QueryAdvanceModel();
		if (null == model)
			return o;

		o.setSql(null == model.getUserReportTemplate() ? null : model.getUserReportTemplate().getAdvanceSql());
		return o;
	}

	public static QueryAdvanceModel parseAdvance(final RptSetupTmplModel model) {
		QueryAdvanceModel o = new QueryAdvanceModel();
		if (null == model)
			return o;

		o.setSql(model.getAdvanceSql());
		return o;
	}

	protected static ReportOptionModel parseOption(final ReportTemplateObjectModel model) {
		ReportOptionModel o = new ReportOptionModel();
		if (null == model)
			return o;

		o.setLayout(null == model.getUserReportTemplate() ? null : model.getUserReportTemplate().getLayoutInd());
		return o;
	}

	public static ReportOptionModel parseOption(final RptSetupTmplModel model, final List<RptSetupTmplCriteModel> list, final Map<String, List<ReportParamModel>> paramOptionMap) {
		ReportOptionModel o = new ReportOptionModel();
		if (CollectionUtils.isNotEmpty(list)) {
			o.setCriteria(list.stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));
		}
		if (null != model) {
			if (StringUtils.isNotBlank(model.getAdvanceSql()))
				o.setParameter(ReportParamUtils.getParamMap(model.getAdvanceSql(), paramOptionMap).stream().map(ReportPropsUtils::parse).collect(Collectors.toList()));

			o.setLayout(model.getLayoutInd());
		}
		return o;
	}

	public static ReportTemplate parse(final String fileExtension, final String sql, final String layout, final String type, Integer id) {
		ReportTemplateModel o = new ReportTemplateModel();
		o.setId(id);
		o.setAdvance(new QueryAdvanceModel());
		o.getAdvance().setSql(sql);
		o.setOption(new ReportOptionModel());
		o.getOption().setOutput(fileExtension);
		o.getOption().setLayout(layout);
		o.getOption().setType(type);
		return o;
	}

	public static ReportTemplate parse(final ReportDownloadObjectModel model) {
		ReportTemplateModel o = new ReportTemplateModel();
		o.setId(model.getData().getRefId());
		o.setAdvance(new QueryAdvanceModel());
		o.getAdvance().setSql(model.getData().getRptSql());
		o.setOption(new ReportOptionModel());
		o.getOption().setOutput(model.getFileExtension());
		o.getOption().setLayout(model.getData().getRptLayout());
		o.getOption().setType(model.getRptType());
		return o;
	}
}
