package com.dbs.module.report.util;

import com.dbs.module.report.ReportCategory;
import com.dbs.module.report.constant.GenerateOption;
import com.dbs.module.report.constant.Indicator;
import com.dbs.module.report.model.ReportCategoryObjectModel;
import com.dbs.module.util.AbstractModuleUtils;
import com.dbs.util.Assert;
import com.dbs.vtsp.model.RptSetupCatgModel;
import com.dbs.vtsp.model.RptSetupCatgTmplModel;

public class VtspUserReportCategoryUtils extends AbstractModuleUtils {

	private VtspUserReportCategoryUtils() {
		// for SonarQube scanning purpose
	}

	public static ReportCategoryObjectModel parse(final ReportCategory model, final RptSetupCatgModel source) {
		ReportCategoryObjectModel o = new ReportCategoryObjectModel();
		o.setId(source.getId());
		o.setUserReportCategory(source);
		return o;
	}

	public static RptSetupCatgModel parseCategory(final ReportCategory model) {
		RptSetupCatgModel o = new RptSetupCatgModel();
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		o.setActiveInd(Indicator.Y.name());
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupCatgModel parseCategory(final ReportCategory model, final RptSetupCatgModel source) {
		if (null == source)
			return source;
		RptSetupCatgModel o = new RptSetupCatgModel();
		Assert.equals(source.getId(), model.getId(), "Id must be equals");
		o.setId(model.getId());
		o.setName(model.getName());
		o.setDescription(model.getDescription());
		o.setGenerateOpt(getOrElse(model.getGenerateOpt(), GenerateOption.B.name()));
		o.setActiveInd(getOrElse(model.getActiveInd(), Indicator.Y.name()));
		o.setLastUpdatedDate(model.getLastUpdatedDate());
		return o;
	}

	public static RptSetupCatgTmplModel parseGroupTemplate(final ReportCategory model) {
		RptSetupCatgTmplModel o = new RptSetupCatgTmplModel();
		o.setRptCatgId(model.getId());
		o.setRptTmplId(model.getRptTmplId());
		o.setDispSeq(model.getDispSeq());
		return o;
	}

	public static RptSetupCatgTmplModel parseGroupTemplate(final Integer rptTmplId, final Integer dispSeq, final Integer id) {
		RptSetupCatgTmplModel o = new RptSetupCatgTmplModel();
		o.setRptCatgId(id);
		o.setRptTmplId(rptTmplId);
		o.setDispSeq(dispSeq);
		return o;
	}
}
