package com.dbs.module.session.model;

import com.dbs.module.model.AbstractUserModel;
import com.dbs.module.session.UserSessionForm;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSessionFormModel extends AbstractUserModel implements UserSessionForm {

	@JsonInclude
	private String userId;

	@JsonInclude
	private String token;

	@JsonInclude
	private String sid;

	@JsonInclude
	private String ip;

	@JsonInclude
	private String mode;

	@JsonInclude
	private Long time;
}
