package com.dbs.module.report.model;

import java.util.List;

import com.dbs.module.model.AbstractSearchModel;
import com.dbs.module.report.ReportAccessGroupForm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportAccessGroupFormModel extends AbstractSearchModel implements ReportAccessGroupForm {

	// for Fortify scanning purpose
	@JsonIgnore
	private String dummy;

	@JsonInclude
	private String name;

	@JsonInclude
	private List<Integer> rptTmplList;

	@JsonInclude
	private List<Integer> rptCatgList;

	@JsonInclude
	private List<Integer> userList;

	@JsonInclude
	private List<Integer> roleList;
}
