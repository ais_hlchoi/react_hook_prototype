package com.dbs.framework.shiro.entity;

public interface EntityBase {

	String getResponseDesc();

	String getResponseCode();

	Integer getStatus();

	Object getObject();
}
