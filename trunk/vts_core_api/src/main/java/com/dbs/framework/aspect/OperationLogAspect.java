package com.dbs.framework.aspect;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.dbs.framework.aspect.annotation.OperationLog;
import com.dbs.framework.constant.ResponseCode;
import com.dbs.framework.exception.SysRuntimeException;
import com.dbs.framework.model.CommonExceptionHandling;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.JointPointUtils;
import com.dbs.util.OperationLogUtils;
import com.dbs.util.OperationLogUtils.OperationLogStatus;
import com.dbs.util.RequestHeaderUtils;
import com.dbs.util.ResponseCodeUtils;
import com.dbs.vtsp.model.OperationLogModel;

/**
 * Centralize user request, and log down all activity 
 * 
 * @Aspect： define interface
 * @Before： before controller method
 * @After： after controller method
 * @AfterReturning： after controller method success
 * @AfterThrowing： after controller throw exception
 * @Around： before and after
 * @Pointcut： define point cut expression
 */
@Aspect
@Component
@Order(2)
public class OperationLogAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private HttpServletRequest request;

	@Value("${sys.log.url}")
	private String sysLogApi;

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.OperationLog)")
	public void operationLog() {
		// do nothing because of point cut
	}

	@Before("operationLog()")
	public void doBefore(JoinPoint joinPoint) {
		if (StringUtils.isBlank(sysLogApi))
			logger.info("Before: Application properties variable [ sys.log.url ] is empty.");
	}

	@AfterReturning(value = "operationLog()", returning = "object")
	public void doAfterReturning(Object object) {
		if (null != object)
			logger.info("AfterReturning: object is returned.");
	}

	@Around("operationLog()")
	public Object doAround(ProceedingJoinPoint joinPoint) {
		OperationLogModel log = new OperationLogModel(new Date(), RequestHeaderUtils.getClientIPAddress(request));
		log.setUserId(CurrentUserUtils.getOneBankId());

		if (ArrayUtils.isNotEmpty(joinPoint.getArgs())) {
			log.setFuncId(OperationLogUtils.getValue(joinPoint.getArgs()[0], "menuFuncId", Integer.class));
		}

		boolean isLogDescReturn = false;
		boolean isLogErrorOnly = false;

		Signature signature = joinPoint.getSignature();
		Method method = ((MethodSignature) signature).getMethod();

		if (method.isAnnotationPresent(OperationLog.class)) {
			log.setOperationType(method.getAnnotation(OperationLog.class).operation().name());
			isLogDescReturn = method.getAnnotation(OperationLog.class).logDescReturn();
			isLogErrorOnly = method.getAnnotation(OperationLog.class).logErrorOnly();
		}

		Map<String, Object> content = new LinkedHashMap<>();
		content.put("service", String.format("%s.%s", ClassPathUtils.getShortName(signature.getDeclaringTypeName()), method.getName()));
		content.put("parameter", joinPoint.getArgs());

		Object result = null; // result of aspect is always executed at ServiceImpl, not RestEntity is returned
		SysRuntimeException error = null;
		try {
			result = JointPointUtils.proceedJoinPoint(joinPoint);
			log.setResponseCode(ResponseCodeUtils.getSuccess());

			// only support string temporarily
			if (isLogDescReturn && result instanceof String) {
				log.setResponseDesc((String) result);
			}

		} catch (SysRuntimeException e) {
			error = e;
			log.setResponseDesc(CommonExceptionHandling.formatError(e.getMessage()));
		}

		if (StringUtils.isNotBlank(log.getResponseDesc()))
			content.put("result", log.getResponseDesc());

		log.setOperationContentObject(content);
		log.setStatus(OperationLogStatus.getOrElse(log.getResponseCode()).name());
		log.setResponseTime(new Date());

		boolean isError = null != error;
		try {
			if (isError) {
				ResponseCode errorCode = ResponseCodeUtils.getOrElse(error.getResponseCode());
				if (errorCode.getStatus() > 0) {
					logger.warn(String.format("%s -> %s", errorCode.name(), ClassPathUtils.toJSONString(content)));
					isError = false;
				} else {
					content.put("error", errorCode);
					log.setOperationContentObject(content);
				}
			}

			if ((isError && isLogErrorOnly) || !isLogErrorOnly) {
				logger.info(String.format("%3$s %1$s %3$s [ %2$s ]", "AOP-operation", ClassPathUtils.toJSONString(log), StringUtils.repeat("-", 10)));
				OperationLogUtils.insert(log, sysLogApi);
			}
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}
		if (isError)
			throw new SysRuntimeException(ResponseCodeUtils.getOrElse(error.getResponseCode()), error.getResponseDesc(), error.getCause()).putAll(error.getParam());

		return result;
	}
}
