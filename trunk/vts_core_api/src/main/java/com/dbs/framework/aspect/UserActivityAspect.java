package com.dbs.framework.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.dbs.framework.aspect.annotation.UserSession;
import com.dbs.framework.aspect.annotation.UserSession.UserSessionAction;
import com.dbs.module.session.UserSessionForm;
import com.dbs.module.session.util.UserSessionFormUtils;
import com.dbs.module.session.util.UserSessionManageUtils;
import com.dbs.util.ClassPathUtils;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.FortifyStrategyUtils;
import com.dbs.util.JointPointUtils;

@Aspect
@ConfigurationProperties("interceptor")
@Component
public class UserActivityAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private HttpServletRequest request;

	@Value("${sys.sid.url}")
	private String sysSidUrl;

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.UserSession)")
	public void userSessionPoint() {
		// do nothing because of point cut
	}

	@Pointcut("execution(* com.dbs.*..controller.external.*..*(..))")
	public void controllerPoint() {
		// do nothing because of point cut
	}

	@Around("controllerPoint() || userSessionPoint()")
	public Object intercept(ProceedingJoinPoint joinPoint) {
		UserSessionAction action = UserSessionAction.CONNECT;

		Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
		if (method.isAnnotationPresent(UserSession.class))
			action = method.getAnnotation(UserSession.class).value();

		if (UserSessionAction.IGNORE == action)
			return JointPointUtils.proceedJoinPoint(joinPoint);
			
		UserSessionForm model = null;
		try {
			model = UserSessionFormUtils.parse(CurrentUserUtils.getOneBankId(), CurrentUserUtils.getAuthToken(request), CurrentUserUtils.getClientIPAddress(request), CurrentUserUtils.getSessionCode(), CurrentUserUtils.getLoginTimeInMillis());
		} catch (Exception e) {
			logger.error(FortifyStrategyUtils.toErrString(e));
		}

		if (null == model)
			return JointPointUtils.proceedJoinPoint(joinPoint);

		String authorization = request.getHeader("Authorization");

		switch (action) {
		case CONNECT:
			UserSessionManageUtils.connect(model, sysSidUrl, authorization);
			break;
		case RECONNECT:
		case DISCONNECT:
		case RESET:
			UserSessionManageUtils.reconnect(model, sysSidUrl, authorization, action);
			break;
		default:
			logger.info(String.format("Skip to check user session when %s %s", request.getRequestURI(), FortifyStrategyUtils.toLogString(model.getUserId(), model.getIp())));
		}

		return JointPointUtils.proceedJoinPoint(joinPoint);
	}

	@EventListener
	public void startup(ContextRefreshedEvent event) {
		logger.info(String.format("%s", ClassPathUtils.getName()));
	}
}
