package com.dbs.framework.aspect;

import java.util.Objects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dbs.vtsp.model.RoleAccessModel;
import com.dbs.vtsp.service.AuthenticationService;
import com.dbs.util.JointPointUtils;

@Aspect
@Component
public class RoleAccessAspect {

	@Autowired
	private AuthenticationService authenticationService;

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.RoleAccess)")
	public void methodRoleAccess() {
		// do nothing because of point cut
	}

	@Pointcut("execution(* (@com.dbs.framework.aspect.annotation.RoleAccess *).*(..))")
	public void classRoleAccess() {
		// do nothing because of point cut
	}

	@Around("methodRoleAccess() || classRoleAccess()")
	public Object doAround(ProceedingJoinPoint joinPoint) {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		String requestURI = Objects.requireNonNull(requestAttributes).getRequest().getRequestURI();
		Integer roleCount = authenticationService.hasRoleAccess(RoleAccessModel.builder().uri(requestURI).build());
		if (roleCount == null || roleCount < 1) {
			throw new AuthenticationServiceException("This role have no permission to call this API " + requestURI);
		}
		return JointPointUtils.proceedJoinPoint(joinPoint);
	}
}
