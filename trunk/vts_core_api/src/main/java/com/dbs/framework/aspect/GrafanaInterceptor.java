package com.dbs.framework.aspect;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.dbs.util.ClassPathUtils;
import com.dbs.util.CurrentUserUtils;
import com.dbs.util.JointPointUtils;
import com.dbs.util.RequestHeaderUtils;

@Aspect
@ConfigurationProperties("interceptor")
@Component
public class GrafanaInterceptor {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String APPLICATION_NAME = "VTSP";
	private static final String APPLICATION_COUNTRY = "HKG";
	private static final String DEFAULT_GRAFANA_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String GRAFANA_MONITOR = "GRAFANA_MONITOR";
	private static final String SEPARATOR = "|";

	@Around("execution(* *.*(..)) && within(com.dbs..*.controller..*)")
	public Object intercept(ProceedingJoinPoint joinPoint) {
		String uuid = UUID.randomUUID().toString();
		StopWatch timer = new StopWatch();
		timer.start();

		final String uri = getRequestURI();
		final String sid = getSessCode();
		final String uid = getUserCode();
		logger.debug(ClassPathUtils.toCharString(getMonitorLog(uri, sid, uid, null)));
		logger.info(ClassPathUtils.toCharString(getGrafanaMonitorLog(uri, uuid, sid, uid, null, timer.getTotalTimeMillis() + "ms")));
		Object result = JointPointUtils.proceedJoinPoint(joinPoint);
		timer.stop();
		logger.debug(ClassPathUtils.toCharString(getMonitorLog(uri, sid, uid, timer.getTotalTimeMillis() + "ms")));
		logger.info(ClassPathUtils.toCharString(getGrafanaMonitorLog(uri, uuid, sid, uid, getStatus(), timer.getTotalTimeMillis() + "ms")));
		return result;
	}

	@EventListener
	public void startup(ContextRefreshedEvent event) {
		logger.info(String.format("%s", ClassPathUtils.getName()));
	}

	private String getRequestURI() {
		HttpServletRequest request = RequestHeaderUtils.getCurrentRequest();
		return null == request ? "" : request.getRequestURI();
	}

	private int getStatus() {
		HttpServletResponse response = RequestHeaderUtils.getCurrentResponse();
		return null == response ? 200 : response.getStatus();
	}

	private String getSessCode() {
		String code = CurrentUserUtils.getSessionCode();
		return StringUtils.isBlank(code) ? new SimpleDateFormat("yyyyMMddHHmmssuF").format(new Date()) : code;
	}

	private String getUserCode() {
		return CurrentUserUtils.getOneBankId();
	}

	private String getMonitorLog(String requestUri, String sessionId, String userId, String stopTime) {
		StringBuilder sb = new StringBuilder();
		sb.append("[Session Id:: " + sessionId + "]");
		if (StringUtils.isNotBlank(userId)) {
			sb.append("[User id:: " + userId + "]");
		}
		sb.append(" - request " + requestUri);
		if (StringUtils.isNotBlank(stopTime)) {
			sb.append(" - total Time for processing " + stopTime);
		}
		return sb.toString();
	}

	private String getGrafanaMonitorLog(String requestUri, String requestId, String sessionId, String userId, Integer httpStatus, String stopTime) {
		List<String> logs = new ArrayList<>();
		logs.add(new SimpleDateFormat(DEFAULT_GRAFANA_DATETIME_PATTERN).format(new Date()));
		logs.add(APPLICATION_NAME);
		logs.add(APPLICATION_COUNTRY);
		logs.add(requestUri);
		logs.add(stopTime);
		logs.add(requestId);
		logs.add(sessionId);
		logs.add(userId);
		if (null != httpStatus) {
			logs.add(httpStatus.toString());
			logs.add(HttpStatus.valueOf(httpStatus).getReasonPhrase());
		}
		return GRAFANA_MONITOR + " - " + StringUtils.join(logs, SEPARATOR);
	}
}
