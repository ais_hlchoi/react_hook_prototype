package com.dbs.framework.aspect;

import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.dbs.util.ClassPathUtils;
import com.dbs.util.LocalHostUtils;

@Aspect
@Component
public class LocalHostAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Pointcut("execution(* com.dbs..*..*ServiceImpl.*(..))")
	public void servicePoint() {
		// do nothing because of point cut
	}

	@Pointcut("@annotation(com.dbs.framework.aspect.annotation.LocalHostConfig)")
	public void configPoint() {
		// do nothing because of point cut
	}

	@Before("servicePoint() || configPoint()")
	public void prepare(JoinPoint joinPoint) {
		if (ArrayUtils.isNotEmpty(joinPoint.getArgs()))
			Stream.of(joinPoint.getArgs()).forEach(LocalHostUtils::bind);
	}

	@EventListener
	public void startup(ContextRefreshedEvent event) {
		logger.info(String.format("%s", ClassPathUtils.getName()));
	}
}
