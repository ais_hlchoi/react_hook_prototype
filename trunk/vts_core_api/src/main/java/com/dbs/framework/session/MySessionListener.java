package com.dbs.framework.session;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

@WebListener
public class MySessionListener {

	public void sessionCreated(HttpSessionEvent httpSessionEvent) {
		MySessionContext.addSession(httpSessionEvent.getSession());
	}

	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
		HttpSession session = httpSessionEvent.getSession();
		MySessionContext.delSession(session);
	}
}
