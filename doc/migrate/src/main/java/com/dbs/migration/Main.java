package com.dbs.migration;

import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;

public class Main {
	private final static Logger logger = LoggerFactory.getLogger(Main.class);

	//Build: mvn install
	//Run: java -Dlog4j.configurationFile="C:\migrate\src\main\resources\log4j2.xml" -Dorg.owasp.esapi.resources="C:\migrate\src\main\resources" -jar target/migration-0.0.1-SNAPSHOT-jar-with-dependencies.jar
	
	public static void main(String[] args) throws Exception {
		logger.info("start");
        Connection conn = null;
    	Console cnsl = System.console();
        
        try{
        	//String dburl = "//10.52.8.192:6603/vtsp";
        	//String username = "vtsp";
        	//char[] pwd = new char[]{'H','e','l','l','o','@','1','2','3','4'};
        	String dburl = cnsl.readLine("Enter DB URL: ");/*  //10.52.8.192:6603/vtsp   */
        	String username = cnsl.readLine("Enter Username: ");/*  vtsp   */
        	char[] pwd = cnsl.readPassword("Enter password : ");/*  Hello@1234   */
        	
        	logger.info("connecting to db...");
    		Class.forName("org.mariadb.jdbc.Driver");
        	conn = DriverManager.getConnection("jdbc:mariadb:"+dburl, username, String.valueOf(pwd));
        	
        	//String csvDirectory = "C:\\Users\\hoilokchoi\\Desktop\\migrate\\data\\";
        	String csvDirectory = cnsl.readLine("Enter CSV Directory: ");  /*  C:\\Users\\hoilokchoi\\Desktop\\migrate\\data\\ */
	        
        	File importFolder = new File(csvDirectory);
	        if(!importFolder.exists()) {
	        	logger.error("Folder not exists");
	        	return;
	        }
	        
	        File bakFolder = new File(importFolder.getPath() + File.separator + "bak");
	        if(!bakFolder.exists())
	        	bakFolder.mkdir();
	        
	        File processFolder = new File(importFolder.getPath() + File.separator + "process");
	        if(!processFolder.exists())
	        	processFolder.mkdir();
	        
	        File errFolder = new File(importFolder.getPath() + File.separator + "err");
	        if(!errFolder.exists())
	        	errFolder.mkdir();
	        
	        String[] fileList = importFolder.list();
	        CSV2DB m = new CSV2DB();
	        for(String file: fileList) {
	        	if(!new File(importFolder.getPath() + File.separator + file).isFile())
	        		continue;
	        	FileUtils.copyFile(new File(importFolder.getPath() + File.separator + file), new File(processFolder + File.separator + file));
	        	new File(importFolder.getPath() + File.separator + file).delete();
	        	CSVReader reader = null;        	
	        	String tableName = file.substring(0, file.length() - 11);
	        	try {
	        		logger.info("Migrate " + file + " to " + tableName );
	        		reader = new CSVReader(new FileReader(processFolder + File.separator + file));
	        		m.migrateFile2Table(conn, reader, tableName);
		        	FileUtils.copyFile(new File(processFolder.getPath() + File.separator + file), new File(bakFolder + File.separator + file));	      
		        	new File(processFolder.getPath() + File.separator + file).delete();
	        	}
	        	catch(Exception e) {
		        	FileUtils.copyFile(new File(processFolder.getPath() + File.separator + file), new File(errFolder + File.separator + file));	      
		        	new File(processFolder.getPath() + File.separator + file).delete();
	        		logger.error(e.getMessage());
	        	}
	        	finally {
	        		reader.close();
	        	}
	        }
        }
        catch(Exception e) {
        	logger.error(e.getMessage());
        }
        finally {
            conn.close();
        }        
        logger.info("end");
	}

}
