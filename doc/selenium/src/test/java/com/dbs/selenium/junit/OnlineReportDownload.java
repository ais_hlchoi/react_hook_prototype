package com.dbs.selenium.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class OnlineReportDownload {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "Online Report";
    private final String MENU_2 = "Download Reports";
    private String searchText = "a";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/		
        Common.clickButton(driver, Common.BTN_RESET);      
        Common.setSearchInput(driver, searchText);		 
        Common.clickButton(driver, Common.BTN_SEARCH);                      
        selectReport(driver, 1);
        selectReportDetail(driver, 1);        
        Common.logout(driver);        
        /**INSIDE FUNCTION**/
    }
    
    private void selectReport(WebDriver driver, int i) throws InterruptedException {
    	driver.findElement(By.tagName("main")).findElements(By.className("ant-spin-container")).get(0).findElements(By.className("anticon-cloud-download")).get(0).click();
        Thread.sleep(2000);    	
	}
    
    private void selectReportDetail(WebDriver driver, int i) throws InterruptedException {
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.tagName("main")).findElements(By.className("ant-spin-container")).get(0).findElements(By.className("anticon-ellipsis")).get(0)).click().build().perform();
		Thread.sleep(1000);
		WebElement ele = driver.findElement(By.className("ant-drawer-mask"));
		//because the element is not clickable due to disabled, so need to use javascript to click
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele);
	}
    

	@After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
