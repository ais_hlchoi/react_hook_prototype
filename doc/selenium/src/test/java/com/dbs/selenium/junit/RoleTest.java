package com.dbs.selenium.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class RoleTest {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "System";
    private final String MENU_2 = "Role";
    private String searchText = "xxxxx";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/		
        int ddCnt = 0;
		Common.clickButton(driver, Common.BTN_ADD); 
        Common.clickButton(driver, Common.BTN_CANCEL);
		Common.clickButton(driver, Common.BTN_ADD);
		Common.setInput(driver, "form_roleCode", searchText);
		Common.setInput(driver, "form_roleName", searchText);
		Common.selectItem(driver, "form_activeInd", ddCnt++, 0);
		Common.setInput(driver, "form_dispSeq", "999");
		Common.setInput(driver, "form_remark", searchText);
		Common.clickSwitch(driver, 0);
		Common.clickButton(driver, Common.BTN_SAVE);
		
        Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);        
        Common.clickButton(driver, Common.BTN_AUDIT);     
        Common.clickButton(driver, Common.BTN_CANCEL);

		Common.clickButton(driver, Common.BTN_EDIT); 
        Common.clickButton(driver, Common.BTN_CANCEL);
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.clickButton(driver, Common.BTN_SAVE);
 
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);     
        Common.clickButton(driver, Common.BTN_EXP);     
        Common.logout(driver);

        Common.login(driver, Common.CHK_USERNAME, Common.CHK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);       
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.clickButton(driver, Common.BTN_AUTH);   
        Common.logout(driver);
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);     
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);          
        Common.clickButton(driver, Common.BTN_DELETE);     
        Common.clickButton(driver, Common.BTN_CONFIRM);   
        Common.logout(driver);  
        
        Common.login(driver, Common.CHK_USERNAME, Common.CHK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);       
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.clickButton(driver, Common.BTN_REV);   
        Common.logout(driver);        
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);     
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);          
        Common.clickButton(driver, Common.BTN_DELETE);     
        Common.clickButton(driver, Common.BTN_CONFIRM);   
        Common.logout(driver);  
        
        Common.login(driver, Common.CHK_USERNAME, Common.CHK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);       
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.clickButton(driver, Common.BTN_AUTH);   
        Common.logout(driver);
        
        Common.clickButton(driver, Common.BTN_RESET);       
        
        /**INSIDE FUNCTION**/
        Common.logout(driver);
    }
    
    @After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
