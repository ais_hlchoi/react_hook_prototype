package com.dbs.selenium.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Common {
	public final static String MAK_USERNAME = "vtsp_support";
	public final static String MAK_PWD = "R%R8sfYmImLs";
	public final static String CHK_USERNAME = "vtsp_sales";
	public final static String CHK_PWD = "R%R8sfYmImLs";
	
	public final static String SIGN_IN_URL = "http://vtsp-core-ui.api.apps.ocpsit.uat.dbs.com/#/signin";
	public final static String BTN_LOGIN = "Login";
	public final static String BTN_LOGOUT = "Sign Out";
	public final static String BTN_SEARCH = "Search";
	public final static String BTN_AUDIT = "Audit";
	public final static String BTN_CANCEL = "Cancel";
	public final static String BTN_BACK = "Back";
	public final static String BTN_ADD = "Add";
	public final static String BTN_SAVE = "Save";
	public final static String BTN_EDIT = "Edit";
	public final static String BTN_DELETE = "Delete";
	public final static String BTN_EXP = "Export";
	public final static String BTN_CONFIRM = "Confirm";
	public final static String BTN_RESET = "Reset";
	public final static String BTN_AUTH = "Authorize";
	public final static String BTN_REV = "Reverse";
	public final static String BTN_REVIEW = "Review";
	public final static String BTN_NEXT = "Next";
	
	
	public static void login(WebDriver driver, String username, String pwd) throws Exception {
		WebElement functionContent = driver.findElement(By.id("container"));		
		setInput(functionContent, "userCode", username);		
		setInput(functionContent, "pwd", pwd);		
		List<WebElement> buttonEle = getButton(driver);
		clickButton(buttonEle, BTN_LOGIN);
        Thread.sleep(1000);
	}
	
	public static void logout(WebDriver driver) throws Exception {
		List<WebElement> buttonEle = getButton(driver);
        if(buttonEle.isEmpty())
        	throw new Exception("button not found.");
        clickButton(buttonEle, BTN_LOGOUT);
        Thread.sleep(500);
	}
	
	public static List<WebElement> getButton(WebDriver driver){
		return driver.findElements(By.tagName("button"));
	}

	
	public static List<WebElement> getButton(WebElement functionContent){
		return functionContent.findElements(By.tagName("button"));
	}
	
	
	public static void selectMenu(WebDriver driver, String menu1, String menu2) throws Exception {
		List<WebElement> menuEle = driver.findElements(By.tagName("li"));
        if(menuEle.isEmpty())
        	throw new Exception("menu not found.");
        StringBuilder str  = new StringBuilder();
        boolean mainMenuFound = false;
        boolean subMenuFound = false;
        for(WebElement headMenu : menuEle) {
    		str.append("[").append(headMenu.getText()).append("]");
        	if(menu1.equals(headMenu.getText())) {
        		mainMenuFound = true;
        		str  = new StringBuilder();
        		headMenu.click();        		 
                Thread.sleep(1000);
        		headMenu.findElements(By.tagName("li"));
        		List<WebElement> subMenuEle = headMenu.findElements(By.tagName("li"));
                if(menuEle.isEmpty())
                	throw new Exception("sub menu not found.");
                for(WebElement subMenu : subMenuEle) {
            		str.append("[").append(subMenu.getText()).append("]");
                	if(menu2.equalsIgnoreCase(subMenu.getText().trim())) {
                		subMenu.click();
                		subMenuFound = true;
                		break;
                	}
                }   
                break;
        	}
        }    
        if(!mainMenuFound)
        	throw new Exception("main menu not found. lookup["+menu1+"][" + str.toString() + "]");      
        if(!subMenuFound)
        	throw new Exception("sub menu not found. lookup["+menu2+"][" + str.toString() + "]");    
        Thread.sleep(2000);
	}
	
	public static void clickButton(WebDriver driver, String buttonTxt) throws Exception {
        clickButton(getButton(driver),buttonTxt);
	}
	
	
	public static void clickButton(List<WebElement> buttonEle, String buttonTxt) throws Exception {
        if(buttonEle.isEmpty())
        	throw new Exception("button not found.");        
		for(WebElement button : buttonEle) {
        	if(buttonTxt.equals(button.getText())) {
        		button.click();
        		break;
        	}
        } 
        Thread.sleep(2000);
	}
	
	public static void setInput(WebDriver driver, String key, String value) throws Exception {
		driver.findElement(By.id(key)).sendKeys(value);  
        Thread.sleep(500);
	}
	
	public static void setSearchInput(WebDriver driver, String value) throws Exception {
		setInput(driver, "form_defaultSearchText", value);
	}
		
		
	public static void setInput(WebElement functionContent, String key, String value) throws Exception {
		functionContent.findElement(By.id(key)).sendKeys(value);  
        Thread.sleep(500);
	}

	public static void clickRadioButton(WebDriver driver, int i) {
		driver.findElements(By.className("ant-radio-input")).get(i).click();
	}

	public static void clickRadioButton(WebElement functionContent, int i) {
		functionContent.findElements(By.className("ant-radio-input")).get(i).click();
	}

	public static WebElement getHtmlContent(WebDriver driver, String tagName) {
		return driver.findElement(By.tagName(tagName));
	}

	public static WebElement getHtmlContent(WebElement functionContent, String tagName) {
		return functionContent.findElement(By.tagName(tagName));
	}

	public static void selectItem(WebDriver driver, String key, int ddCnt, int i) {
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.id(key))).click().build().perform();
		driver.findElements(By.className("rc-virtual-list-holder-inner")).get(ddCnt).findElements(By.className("ant-select-item")).get(i).click();
	}

	public static void selectMultipleItem(WebDriver driver, int ddCnt, Integer[] select) throws InterruptedException {
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.className("ant-select-selector"))).click().build().perform();
        Thread.sleep(1000);		
		for(Integer i : select) 
			driver.findElements(By.className("rc-virtual-list-holder-inner")).get(ddCnt).findElements(By.className("ant-select-item")).get(i.intValue()).click();
	}

	public static void clickSwitch(WebDriver driver, int i) {
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElements(By.className("ant-switch-handle")).get(i)).click().build().perform();		
	}

	public static void selectBoolean(WebDriver driver, String key, int i) {
		driver.findElement(By.id(key)).findElements(By.className("ant-radio-button-wrapper")).get(i).click();
		
	}

	public static void selectTab(WebDriver driver, String tabName) throws InterruptedException {
    	List<WebElement> tabList = driver.findElement(By.tagName("main")).findElements(By.className("ant-tabs-tab-btn"));
    	for(WebElement tab : tabList) {
    		if(tabName.equalsIgnoreCase(tab.getText())) {
    			tab.click();
    			break;
    		}
    	}
        Thread.sleep(2000);
	}

	public static void multipleSelect(WebDriver driver, Integer[] select) throws InterruptedException {
		List<WebElement> checkboxList = driver.findElement(By.tagName("main")).findElements(By.className("ant-table-content")).get(0).findElements(By.className("ant-checkbox-input"));
    	for(Integer i : select) {
    		WebElement checkbox = checkboxList.get(i.intValue());
    		checkbox.click();
    	}
    	driver.findElement(By.tagName("main")).findElements(By.className("ant-transfer")).get(0).findElement(By.className("ant-transfer-operation")).findElements(By.tagName("button")).get(0).click();
        Thread.sleep(2000);
	}
	
}
