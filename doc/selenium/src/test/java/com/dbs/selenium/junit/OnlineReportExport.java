package com.dbs.selenium.junit;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class OnlineReportExport {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "Online Report";
    private final String MENU_2 = "Report Template";
    private String searchText = "xx";
    private String reportName1 = "c";
    private String reportName2 = "d";
    private String reportName3 = "f";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/		
        Common.setSearchInput(driver, searchText);		
        Common.clickButton(driver, Common.BTN_RESET);       
        Common.clickButton(driver, Common.BTN_SEARCH);                      
        selectReport(driver, reportName1);
        //Select XLS
        Common.selectBoolean(driver, "form_exportOption", 0);
        Common.clickButton(driver, Common.BTN_EXP);   
        Common.logout(driver);

        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);        
        Common.selectMenu(driver, MENU_1, MENU_2);
        selectReport(driver, reportName2);
        //Select CSV
        Common.selectBoolean(driver, "form_exportOption", 1);
        Common.clickButton(driver, Common.BTN_EXP);     
        Common.logout(driver);
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);        
        Common.selectMenu(driver, MENU_1, MENU_2);
        selectReport(driver, reportName3);
        //Select PDF
        Common.selectBoolean(driver, "form_exportOption", 2);
        Common.clickButton(driver, Common.BTN_EXP);     
        Common.logout(driver);        
        /**INSIDE FUNCTION**/
    }
    
    private void selectReport(WebDriver driver, int i) throws InterruptedException {
    	driver.findElement(By.tagName("main")).findElements(By.className("ant-table-body")).get(0).findElements(By.className("ant-table-cell")).get(0).click();
        Thread.sleep(2000);
    	
	}

	private void selectReport(WebDriver driver, String reportName) throws InterruptedException {
    	List<WebElement> reportList = driver.findElement(By.tagName("main")).findElements(By.className("ant-table-body")).get(0).findElements(By.className("ant-table-cell"));
    	for(WebElement report : reportList) {
    		if(reportName.equalsIgnoreCase(report.getText())) {
    			report.click();
    			break;
    		}
    	}
        Thread.sleep(2000);
	}

	@After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
