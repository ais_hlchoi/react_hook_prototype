package com.dbs.selenium.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class ReportSetupTest {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "Configuration";
    private final String MENU_2 = "Report Setup";
    private String searchText = "xxxxx";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/	
        int ddCnt = 0;
		Common.clickButton(driver, Common.BTN_ADD);
        Common.multipleSelect(driver, new Integer[] {1});   
		Common.clickButton(driver, Common.BTN_NEXT);
        Common.selectMultipleItem(driver, ddCnt++, new Integer[] {0,1});
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        
        driver.findElement(By.className("input-reportName")).sendKeys(searchText); 
        driver.findElement(By.className("input-reportDescription")).sendKeys(searchText); 
		Common.clickButton(driver, Common.BTN_SAVE);
		
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);        
        Common.clickButton(driver, Common.BTN_AUDIT);     
        Common.clickButton(driver, Common.BTN_CANCEL);
        
        Common.clickButton(driver, Common.BTN_EDIT);   
		Common.clickButton(driver, Common.BTN_BACK);
        Common.clickButton(driver, Common.BTN_EDIT);
        
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        Common.clickButton(driver, Common.BTN_NEXT);
        
        Common.selectTab(driver, "Role Group");   
        Common.multipleSelect(driver, new Integer[] {2,3});   
        Common.clickButton(driver, Common.BTN_SAVE);
        Common.selectTab(driver, "Summary");    
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);     
        Common.clickButton(driver, Common.BTN_EXP);     

        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);        
        Common.clickButton(driver, Common.BTN_DELETE);     
        Common.clickButton(driver, Common.BTN_CONFIRM);         
        Common.clickButton(driver, Common.BTN_RESET);       
        
        /**INSIDE FUNCTION**/
        Common.logout(driver);
    }
    
    @After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
