package com.dbs.selenium.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class ReportSetupAdvanceTest {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "Configuration";
    private final String MENU_2 = "Report Setup Advance";
    private String searchText = "xxxxx";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/	
		Common.clickButton(driver, Common.BTN_ADD);
		Common.clickButton(driver, Common.BTN_CANCEL);
		Common.clickButton(driver, Common.BTN_ADD);
		Common.setInput(driver, "form_sql", "select * from DATA_ALLOCATIONS");
		Common.selectBoolean(driver, "form_layout", 1);
		Common.setInput(driver, "form_name", searchText);
		Common.setInput(driver, "form_description", searchText);
		Common.selectBoolean(driver, "form_generateOpt", 2);
		Common.clickButton(driver, Common.BTN_SAVE);
		
		Common.setSearchInput(driver, searchText);	
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);        
        Common.clickButton(driver, Common.BTN_AUDIT);     
        Common.clickButton(driver, Common.BTN_CANCEL);
        
        Common.clickButton(driver, Common.BTN_EDIT);   
		Common.clickButton(driver, Common.BTN_BACK);
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.selectTab(driver, "Role Group");   
        Common.multipleSelect(driver, new Integer[] {2,3});   
        Common.clickButton(driver, Common.BTN_SAVE);
        
        Common.clickButton(driver, Common.BTN_EDIT);   
        Common.selectTab(driver, "Detail");   
		Common.setInput(driver, "form_description", searchText + "x");
        Common.clickButton(driver, Common.BTN_SAVE);
 
        Common.clickButton(driver, Common.BTN_SEARCH);      
        Common.clickRadioButton(driver, 0);     
        Common.clickButton(driver, Common.BTN_EXP);     

        Common.clickButton(driver, Common.BTN_SEARCH);    
        Common.clickRadioButton(driver, 0);        
        Common.clickButton(driver, Common.BTN_DELETE);     
        Common.clickButton(driver, Common.BTN_CONFIRM);         
        Common.clickButton(driver, Common.BTN_RESET);       
        
        /**INSIDE FUNCTION**/
        Common.logout(driver);
    }
    
    @After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
