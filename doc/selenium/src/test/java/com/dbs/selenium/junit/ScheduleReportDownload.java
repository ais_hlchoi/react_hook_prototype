package com.dbs.selenium.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.dbs.selenium.common.Common;
import com.dbs.selenium.env.EnvironmentManager;
import com.dbs.selenium.env.RunEnvironment;

public class ScheduleReportDownload {
	
    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }
    
    private final String MENU_1 = "Scheduled Report";
    private final String MENU_2 = "Download Reports";
    private String searchText = "order";

    @Test
    public void run() throws Exception {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get(Common.SIGN_IN_URL); 
        driver.manage().window().fullscreen();
        
        Common.login(driver, Common.MAK_USERNAME, Common.MAK_PWD);
        Common.selectMenu(driver, MENU_1, MENU_2);
        /**INSIDE FUNCTION**/		
        Common.clickButton(driver, Common.BTN_RESET);      
        Common.setSearchInput(driver, searchText);		 
        Common.clickButton(driver, Common.BTN_SEARCH);                      
        Common.clickRadioButton(driver, 0);
        Common.clickButton(driver, Common.BTN_EXP);    
        Common.clickButton(driver, Common.BTN_REVIEW);   
        Common.clickButton(driver, Common.BTN_EXP);    
        Common.selectTab(driver, "Summary");   
        Common.logout(driver);        
        /**INSIDE FUNCTION**/
    }
    

	@After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }	
	
}
