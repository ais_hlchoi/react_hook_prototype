Doc Repository:
https://dbs1bank.sharepoint.com/sites/hkiisupport/Shared Documents/Fidessa/

BitBucket
https://bitbucket.sgp.dbs.com:8443/dcifgit/projects/VTSP/

Jenkins
https://ci4.corp.dbs.com:8443/job/VTSP/

Nexus IQ Server Scan URL:
https://x01gdcifniq1a.vsi.sgp.dbs.com:8443/assets/index.html#/dashboard/violations

Sonar qube URL:
https://sonarqube.sgp.dbs.com:8443/projects

Fortify URL:
https://scancentral.corp.dbs.com:8443/ssc/html/ssc/dashboard/Issue%20Stats

UI (UAT) URL:
http://vtsp-core-ui.api.apps.ocpsit.uat.dbs.com/#/signin
=> UI Docker Image Log File Location: /var/log/httpd24
=> UI Docker Image Config File Location: /etc/httpd/conf

Standard Build Pipelines Reference:
https://confluence.sgp.dbs.com:8443/dcifcnfl/playbooks/jenkins/how-to-use-jenkins/pipelines-pp/standard-build-pipelines

JIRA Trigger Build
https://jira.sgp.dbs.com:8443/dcifjira/projects/VTSP/issues/VTSP-13?filter=allopenissues
1. Create Ticker
=> Project: VTSP
=> Issues Type: Deployment
=> CD Deployment Type: CD-PAAS
=> Summary: e.g VTSP_CORE_UI
=> Repository: e.g VTSP_CORE_UI
=> Branch Name: e.g sit
=> Service Name: e.g VTSP_CORE_UI
=> Build Type: Java

2. Assign to Me
3. Trigger Build

to encrypt the db password
=>go to any class
=>add below method
	public static void main(String[] args) {
		System.out.println(CipherUtil.encrypt("your password"));
	}
=>run

Install maria db
C:\Users\hoilokchoi\Desktop\fidessa\mariadb-10.5.6-winx64\bin>mysql_install_db.exe --datadir=C:\Users\hoilokchoi\Desktop\Software\mariadb_data --service=LocalMariaDB --password=dbs123

sc start LocalMariaDB  or  net start LocalMariaDB
net stop LocalMariaDB

Create database and users
=========================
CREATE DATABASE FCDB;
show databases;
CREATE USER 'fcdb' IDENTIFIED BY 'fcdb';
SELECT User FROM mysql.user;
GRANT ALL PRIVILEGES ON *.* TO 'fcdb' IDENTIFIED BY 'fcdb';
GRANT ALL PRIVILEGES ON FCDB.* TO 'fcdb';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'fcdb';

Physical Backup
Backup
mariabackup --backup --target-dir=C:\Users\hoilokchoi\Desktop\Software\mariadb_data_backup --user=root --password=dbs123

Restore
mariabackup --copy-back --target-dir=/backup/

Logical Backup
Export
mysqldump --user=root --password=dbs123 --triggers --routines --events --single-transaction --all-databases -r C:\Users\hoilokchoi\Desktop\Software\mariadb_data_logical\backup.sql

Import
mysql 
MariaDB> source dump.sql
		
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/doc.git doc	
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_api_gateway.git vtsp_api_gateway
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_auth_api.git vtsp_auth_api
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_core_api.git vtsp_core_api
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_main_api.git vtsp_main_api
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_sys_api.git vtsp_sys_api
git clone https://bitbucket.sgp.dbs.com:8443/dcifgit/scm/vtsp/vtsp_core_ui.git vtsp_core_ui

git init
git add --all
git commit -m "Initial Commit"
git push -u origin master

		
		
